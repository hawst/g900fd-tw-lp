.class public Ltv/peel/widget/i;
.super Ljava/lang/Object;

# interfaces
.implements Ltv/peel/widget/h;


# instance fields
.field private a:Landroid/widget/RemoteViews;

.field private b:Z

.field private c:Landroid/content/Context;

.field private d:Ltv/peel/widget/util/a;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/peel/widget/i;->b:Z

    return-void
.end method

.method private a(Landroid/widget/RemoteViews;[Lcom/peel/control/h;)V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/16 v7, 0x28

    const/16 v8, 0x20

    const/4 v10, 0x1

    const/4 v3, 0x0

    .line 258
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 261
    if-eqz p2, :cond_b

    array-length v0, p2

    if-lez v0, :cond_b

    .line 262
    array-length v1, p2

    move v0, v3

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v2, p2, v0

    .line 263
    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/data/g;->d()I

    move-result v4

    .line 265
    const/4 v5, 0x6

    if-ne v4, v5, :cond_0

    .line 262
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 268
    :cond_0
    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 270
    :cond_1
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    move v6, v0

    .line 273
    :goto_2
    sget v0, Lcom/peel/ui/fp;->btn1:I

    const-string/jumbo v1, "setBackgroundResource"

    sget v2, Lcom/peel/ui/fo;->lockscreen_power_button_stateful:I

    invoke-virtual {p1, v0, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 274
    sget v0, Lcom/peel/ui/fp;->btn2:I

    const-string/jumbo v1, "setBackgroundResource"

    sget v2, Lcom/peel/ui/fo;->lockscreen_power_button_stateful:I

    invoke-virtual {p1, v0, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 275
    sget v0, Lcom/peel/ui/fp;->btn3:I

    const-string/jumbo v1, "setBackgroundResource"

    sget v2, Lcom/peel/ui/fo;->lockscreen_power_button_stateful:I

    invoke-virtual {p1, v0, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 276
    sget v1, Lcom/peel/ui/fp;->btn1:I

    sget v2, Lcom/peel/ui/fo;->lock_qw_icon:I

    move-object v0, p1

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setTextViewCompoundDrawables(IIIII)V

    .line 277
    sget v1, Lcom/peel/ui/fp;->btn2:I

    sget v2, Lcom/peel/ui/fo;->lock_qw_icon:I

    move-object v0, p1

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setTextViewCompoundDrawables(IIIII)V

    .line 278
    sget v1, Lcom/peel/ui/fp;->btn3:I

    sget v2, Lcom/peel/ui/fo;->lock_qw_icon:I

    move-object v0, p1

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setTextViewCompoundDrawables(IIIII)V

    .line 280
    if-nez v6, :cond_2

    .line 281
    sget v0, Lcom/peel/ui/fp;->btn1:I

    const/16 v1, 0x8

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 282
    sget v0, Lcom/peel/ui/fp;->btn2:I

    const/16 v1, 0x8

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 283
    sget v0, Lcom/peel/ui/fp;->btn3:I

    const/16 v1, 0x8

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 309
    :goto_3
    return-void

    .line 284
    :cond_2
    if-ne v6, v10, :cond_4

    .line 285
    sget v0, Lcom/peel/ui/fp;->btn1:I

    const/4 v1, 0x4

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 286
    sget v0, Lcom/peel/ui/fp;->btn2:I

    invoke-virtual {p1, v0, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 287
    sget v0, Lcom/peel/ui/fp;->btn3:I

    const/4 v1, 0x4

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 288
    sget v1, Lcom/peel/ui/fp;->btn2:I

    iget-object v2, p0, Ltv/peel/widget/i;->c:Landroid/content/Context;

    invoke-interface {v9, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    invoke-static {v2, v0}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 289
    iget-object v1, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v2, Lcom/peel/ui/fp;->btn2:I

    invoke-interface {v9, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-static {v0}, Ltv/peel/widget/i;->a(Lcom/peel/control/h;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v7

    :goto_4
    invoke-virtual {v1, p1, v2, v0, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;III)V

    goto :goto_3

    :cond_3
    move v0, v8

    goto :goto_4

    .line 290
    :cond_4
    if-ne v6, v11, :cond_7

    .line 291
    sget v0, Lcom/peel/ui/fp;->btn1:I

    invoke-virtual {p1, v0, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 292
    sget v0, Lcom/peel/ui/fp;->btn2:I

    const/4 v1, 0x4

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 293
    sget v0, Lcom/peel/ui/fp;->btn3:I

    invoke-virtual {p1, v0, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 294
    sget v1, Lcom/peel/ui/fp;->btn1:I

    iget-object v2, p0, Ltv/peel/widget/i;->c:Landroid/content/Context;

    invoke-interface {v9, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    invoke-static {v2, v0}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 295
    sget v1, Lcom/peel/ui/fp;->btn3:I

    iget-object v2, p0, Ltv/peel/widget/i;->c:Landroid/content/Context;

    invoke-interface {v9, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    invoke-static {v2, v0}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 296
    iget-object v1, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v2, Lcom/peel/ui/fp;->btn1:I

    invoke-interface {v9, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-static {v0}, Ltv/peel/widget/i;->a(Lcom/peel/control/h;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v7

    :goto_5
    invoke-virtual {v1, p1, v2, v0, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;III)V

    .line 297
    iget-object v1, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v2, Lcom/peel/ui/fp;->btn3:I

    invoke-interface {v9, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-static {v0}, Ltv/peel/widget/i;->a(Lcom/peel/control/h;)Z

    move-result v0

    if-eqz v0, :cond_6

    :goto_6
    invoke-virtual {v1, p1, v2, v7, v10}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;III)V

    goto/16 :goto_3

    :cond_5
    move v0, v8

    .line 296
    goto :goto_5

    :cond_6
    move v7, v8

    .line 297
    goto :goto_6

    .line 299
    :cond_7
    sget v0, Lcom/peel/ui/fp;->btn1:I

    invoke-virtual {p1, v0, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 300
    sget v0, Lcom/peel/ui/fp;->btn2:I

    invoke-virtual {p1, v0, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 301
    sget v0, Lcom/peel/ui/fp;->btn3:I

    invoke-virtual {p1, v0, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 302
    sget v1, Lcom/peel/ui/fp;->btn1:I

    iget-object v2, p0, Ltv/peel/widget/i;->c:Landroid/content/Context;

    invoke-interface {v9, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    invoke-static {v2, v0}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 303
    sget v1, Lcom/peel/ui/fp;->btn2:I

    iget-object v2, p0, Ltv/peel/widget/i;->c:Landroid/content/Context;

    invoke-interface {v9, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    invoke-static {v2, v0}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 304
    sget v1, Lcom/peel/ui/fp;->btn3:I

    iget-object v2, p0, Ltv/peel/widget/i;->c:Landroid/content/Context;

    invoke-interface {v9, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    invoke-static {v2, v0}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 305
    iget-object v1, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v2, Lcom/peel/ui/fp;->btn1:I

    invoke-interface {v9, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-static {v0}, Ltv/peel/widget/i;->a(Lcom/peel/control/h;)Z

    move-result v0

    if-eqz v0, :cond_8

    move v0, v7

    :goto_7
    invoke-virtual {v1, p1, v2, v0, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;III)V

    .line 306
    iget-object v1, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v2, Lcom/peel/ui/fp;->btn2:I

    invoke-interface {v9, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-static {v0}, Ltv/peel/widget/i;->a(Lcom/peel/control/h;)Z

    move-result v0

    if-eqz v0, :cond_9

    move v0, v7

    :goto_8
    invoke-virtual {v1, p1, v2, v0, v10}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;III)V

    .line 307
    iget-object v1, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v2, Lcom/peel/ui/fp;->btn3:I

    invoke-interface {v9, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-static {v0}, Ltv/peel/widget/i;->a(Lcom/peel/control/h;)Z

    move-result v0

    if-eqz v0, :cond_a

    :goto_9
    invoke-virtual {v1, p1, v2, v7, v11}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;III)V

    goto/16 :goto_3

    :cond_8
    move v0, v8

    .line 305
    goto :goto_7

    :cond_9
    move v0, v8

    .line 306
    goto :goto_8

    :cond_a
    move v7, v8

    .line 307
    goto :goto_9

    :cond_b
    move v6, v3

    goto/16 :goto_2
.end method

.method private static a(Lcom/peel/control/h;)Z
    .locals 2

    .prologue
    .line 450
    invoke-virtual {p0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->a()Ljava/util/Map;

    move-result-object v0

    .line 452
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    if-lez v1, :cond_0

    const-string/jumbo v1, "Power"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 453
    const/4 v0, 0x0

    .line 455
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a()Ltv/peel/widget/h;
    .locals 7

    .prologue
    const/16 v2, 0x1e

    const/4 v1, 0x4

    .line 45
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Ltv/peel/widget/i;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v3, "is_setup_complete"

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 47
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    iget-object v4, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v5, Lcom/peel/ui/fp;->noti_main:I

    const/4 v6, 0x3

    invoke-virtual {v0, v4, v5, v6}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 48
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x15

    if-lt v0, v4, :cond_3

    .line 49
    iget-object v4, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    iget-object v5, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v6, Lcom/peel/ui/fp;->setup_button:I

    if-nez v3, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v4, v5, v6, v0}, Ltv/peel/widget/util/a;->c(Landroid/widget/RemoteViews;II)V

    .line 50
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    iget-object v4, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v5, Lcom/peel/ui/fp;->app_name_text:I

    if-nez v3, :cond_2

    :goto_1
    invoke-virtual {v0, v4, v5, v1}, Ltv/peel/widget/util/a;->c(Landroid/widget/RemoteViews;II)V

    .line 58
    :cond_0
    :goto_2
    return-object p0

    :cond_1
    move v0, v2

    .line 49
    goto :goto_0

    :cond_2
    move v1, v2

    .line 50
    goto :goto_1

    .line 52
    :cond_3
    iget-boolean v0, p0, Ltv/peel/widget/i;->b:Z

    if-nez v0, :cond_0

    .line 53
    iget-object v4, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    iget-object v5, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v6, Lcom/peel/ui/fp;->setup_button:I

    if-nez v3, :cond_4

    move v0, v1

    :goto_3
    invoke-virtual {v4, v5, v6, v0}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 54
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    iget-object v4, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v5, Lcom/peel/ui/fp;->app_name_text:I

    if-nez v3, :cond_5

    :goto_4
    invoke-virtual {v0, v4, v5, v1}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    goto :goto_2

    :cond_4
    move v0, v2

    .line 53
    goto :goto_3

    :cond_5
    move v1, v2

    .line 54
    goto :goto_4
.end method

.method public a(ILjava/lang/String;Ljava/lang/String;Z)Ltv/peel/widget/h;
    .locals 8

    .prologue
    const/16 v7, 0x64

    const/16 v6, 0x8

    const/4 v3, 0x0

    .line 460
    iget-boolean v0, p0, Ltv/peel/widget/i;->b:Z

    if-nez v0, :cond_3

    .line 461
    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Ltv/peel/widget/i;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 462
    sget v1, Lcom/peel/ui/fp;->btn16:I

    const-string/jumbo v2, "setBackgroundResource"

    sget v4, Lcom/peel/ui/fo;->lockscreen_power_button_stateful:I

    invoke-virtual {v0, v1, v2, v4}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 463
    sget v1, Lcom/peel/ui/fp;->btn16:I

    sget v2, Lcom/peel/ui/fo;->lock_qw_icon:I

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setTextViewCompoundDrawables(IIIII)V

    .line 464
    sget v1, Lcom/peel/ui/fp;->btn16:I

    iget-object v2, p0, Ltv/peel/widget/i;->c:Landroid/content/Context;

    sget v4, Lcom/peel/ui/ft;->DeviceType18_short:I

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 465
    sget v2, Lcom/peel/ui/fp;->btn17:I

    const-string/jumbo v4, "setBackgroundResource"

    sget-object v1, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v1}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v1

    sget-object v5, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v1, v5, :cond_4

    sget v1, Lcom/peel/ui/fo;->lockscreen_power_button_stateful:I

    :goto_0
    invoke-virtual {v0, v2, v4, v1}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 466
    sget v1, Lcom/peel/ui/fp;->btn17:I

    sget v2, Lcom/peel/ui/fo;->ac_mode_icon:I

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setTextViewCompoundDrawables(IIIII)V

    .line 467
    sget v1, Lcom/peel/ui/fp;->btn17:I

    iget-object v2, p0, Ltv/peel/widget/i;->c:Landroid/content/Context;

    sget v4, Lcom/peel/ui/ft;->button_mode:I

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 468
    sget v1, Lcom/peel/ui/fp;->btn4:I

    sget v2, Lcom/peel/ui/fo;->quick_vol_up_states:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 469
    sget v1, Lcom/peel/ui/fp;->btn5:I

    sget v2, Lcom/peel/ui/fo;->quick_vol_dn_states:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 470
    sget v1, Lcom/peel/ui/fp;->btn6:I

    sget v2, Lcom/peel/ui/fo;->quick_ch_up_states:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 471
    sget v1, Lcom/peel/ui/fp;->btn7:I

    sget v2, Lcom/peel/ui/fo;->quick_ch_dn_states:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 472
    sget v1, Lcom/peel/ui/fp;->textview1:I

    iget-object v2, p0, Ltv/peel/widget/i;->c:Landroid/content/Context;

    sget v4, Lcom/peel/ui/ft;->lockscreen_temp:I

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 473
    sget v1, Lcom/peel/ui/fp;->textview2:I

    iget-object v2, p0, Ltv/peel/widget/i;->c:Landroid/content/Context;

    sget v4, Lcom/peel/ui/ft;->lockscreen_fan_speed:I

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 474
    sget v2, Lcom/peel/ui/fp;->btn_peel_tv:I

    const-string/jumbo v4, "setBackgroundResource"

    sget-object v1, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v1}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v1

    sget-object v5, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v1, v5, :cond_5

    sget v1, Lcom/peel/ui/fo;->lockscreen_power_button_stateful:I

    :goto_1
    invoke-virtual {v0, v2, v4, v1}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 476
    sget-object v1, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v1}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v1

    sget-object v2, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v1, v2, :cond_0

    .line 477
    sget v1, Lcom/peel/ui/fp;->btn_peel_tv:I

    sget v2, Lcom/peel/ui/fo;->widget_expand_icon:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 478
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    .line 479
    sget v1, Lcom/peel/ui/fp;->peel_logo_layout:I

    invoke-virtual {v0, v1, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 483
    :cond_0
    iget-object v1, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    if-eqz v1, :cond_2

    .line 485
    iget-object v1, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v2, Lcom/peel/ui/fp;->btn_peel_tv:I

    const/16 v4, 0x14

    invoke-virtual {v1, v0, v2, v4}, Ltv/peel/widget/util/a;->c(Landroid/widget/RemoteViews;II)V

    .line 488
    iget-object v1, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v2, Lcom/peel/ui/fp;->btn16:I

    const/16 v4, 0x28

    const-string/jumbo v5, "PowerOn"

    invoke-virtual {v1, v0, v2, v4, v5}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 489
    iget-object v1, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v2, Lcom/peel/ui/fp;->btn17:I

    const-string/jumbo v4, "MODE"

    invoke-virtual {v1, v0, v2, v6, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 490
    iget-object v1, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v2, Lcom/peel/ui/fp;->btn4:I

    const-string/jumbo v4, "UP"

    invoke-virtual {v1, v0, v2, v6, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 491
    iget-object v1, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v2, Lcom/peel/ui/fp;->btn5:I

    const-string/jumbo v4, "Down"

    invoke-virtual {v1, v0, v2, v6, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 492
    iget-object v1, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v2, Lcom/peel/ui/fp;->btn6:I

    const-string/jumbo v4, "FAN_HIGH"

    invoke-virtual {v1, v0, v2, v6, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 493
    iget-object v1, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v2, Lcom/peel/ui/fp;->btn7:I

    const-string/jumbo v4, "FAN_LOW"

    invoke-virtual {v1, v0, v2, v6, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 494
    if-eqz p4, :cond_1

    .line 495
    sget v1, Lcom/peel/ui/fp;->btn17:I

    invoke-virtual {v0, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 496
    sget v1, Lcom/peel/ui/fp;->btn17:I

    invoke-virtual {v0, v1, p2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 500
    :cond_1
    iget-object v1, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v2, Lcom/peel/ui/fp;->btn4:I

    const-string/jumbo v3, "UP"

    invoke-virtual {v1, v0, v2, v7, v3}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 501
    iget-object v1, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v2, Lcom/peel/ui/fp;->btn5:I

    const-string/jumbo v3, "Down"

    invoke-virtual {v1, v0, v2, v7, v3}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 502
    iget-object v1, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v2, Lcom/peel/ui/fp;->btn6:I

    const-string/jumbo v3, "FAN_HIGH"

    invoke-virtual {v1, v0, v2, v7, v3}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 503
    iget-object v1, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v2, Lcom/peel/ui/fp;->btn7:I

    const-string/jumbo v3, "FAN_LOW"

    invoke-virtual {v1, v0, v2, v7, v3}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 505
    :cond_2
    iget-object v1, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v2, Lcom/peel/ui/fp;->controllerScreen:I

    invoke-virtual {v1, v2, v0}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    .line 507
    :cond_3
    return-object p0

    .line 465
    :cond_4
    sget v1, Lcom/peel/ui/fo;->lock_btn_normal:I

    goto/16 :goto_0

    .line 474
    :cond_5
    sget v1, Lcom/peel/ui/fo;->lockscreen_launch_button_states:I

    goto/16 :goto_1
.end method

.method public a(IZ[Lcom/peel/control/h;)Ltv/peel/widget/h;
    .locals 10

    .prologue
    const/16 v9, 0x14

    const/16 v8, 0x64

    const/4 v2, 0x0

    const/16 v7, 0x8

    .line 312
    iget-boolean v0, p0, Ltv/peel/widget/i;->b:Z

    if-nez v0, :cond_5

    .line 314
    new-instance v3, Landroid/widget/RemoteViews;

    iget-object v0, p0, Ltv/peel/widget/i;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0, p1}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 316
    array-length v4, p3

    move v1, v2

    move v0, v2

    :goto_0
    if-ge v1, v4, :cond_2

    aget-object v5, p3, v1

    .line 317
    invoke-virtual {v5}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/g;->d()I

    move-result v5

    .line 318
    const/4 v6, 0x2

    if-eq v5, v6, :cond_0

    if-ne v5, v9, :cond_1

    .line 319
    :cond_0
    const/4 v0, 0x1

    .line 316
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 323
    :cond_2
    if-eqz v0, :cond_6

    .line 324
    sget v0, Lcom/peel/ui/fp;->command_holder1:I

    invoke-virtual {v3, v0, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 325
    sget v0, Lcom/peel/ui/fp;->command_holder2:I

    invoke-virtual {v3, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 326
    sget v0, Lcom/peel/ui/fp;->btn8:I

    invoke-virtual {v3, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 334
    :goto_1
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v1, :cond_3

    .line 335
    sget v0, Lcom/peel/ui/fp;->btn_peel_tv:I

    sget v1, Lcom/peel/ui/fo;->widget_expand_icon:I

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 336
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_3

    .line 337
    sget v0, Lcom/peel/ui/fp;->peel_logo_layout:I

    invoke-virtual {v3, v0, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 340
    :cond_3
    sget v1, Lcom/peel/ui/fp;->btn_peel_tv:I

    const-string/jumbo v2, "setBackgroundResource"

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v4, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v4, :cond_7

    sget v0, Lcom/peel/ui/fo;->lockscreen_power_button_stateful:I

    :goto_2
    invoke-virtual {v3, v1, v2, v0}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 341
    sget v1, Lcom/peel/ui/fp;->btn8:I

    const-string/jumbo v2, "setBackgroundResource"

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v4, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v4, :cond_8

    sget v0, Lcom/peel/ui/fo;->lockscreen_power_button_stateful:I

    :goto_3
    invoke-virtual {v3, v1, v2, v0}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 342
    sget v1, Lcom/peel/ui/fp;->btn8:I

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v2, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v2, :cond_9

    sget v0, Lcom/peel/ui/fo;->widget_play_pause_icon:I

    :goto_4
    invoke-virtual {v3, v1, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 343
    sget v1, Lcom/peel/ui/fp;->btn9:I

    const-string/jumbo v2, "setBackgroundResource"

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v4, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v4, :cond_a

    sget v0, Lcom/peel/ui/fo;->lockscreen_power_button_stateful:I

    :goto_5
    invoke-virtual {v3, v1, v2, v0}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 344
    sget v1, Lcom/peel/ui/fp;->btn9:I

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v2, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v2, :cond_b

    sget v0, Lcom/peel/ui/fo;->widget_mute_icon:I

    :goto_6
    invoke-virtual {v3, v1, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 345
    sget v1, Lcom/peel/ui/fp;->btn10:I

    const-string/jumbo v2, "setBackgroundResource"

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v4, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v4, :cond_c

    sget v0, Lcom/peel/ui/fo;->lockscreen_power_button_stateful:I

    :goto_7
    invoke-virtual {v3, v1, v2, v0}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 346
    sget v1, Lcom/peel/ui/fp;->btn10:I

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v2, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v2, :cond_d

    sget v0, Lcom/peel/ui/fo;->widget_ff_icon:I

    :goto_8
    invoke-virtual {v3, v1, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 347
    sget v1, Lcom/peel/ui/fp;->btn11:I

    const-string/jumbo v2, "setBackgroundResource"

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v4, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v4, :cond_e

    sget v0, Lcom/peel/ui/fo;->lockscreen_power_button_stateful:I

    :goto_9
    invoke-virtual {v3, v1, v2, v0}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 348
    sget v1, Lcom/peel/ui/fp;->btn11:I

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v2, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v2, :cond_f

    sget v0, Lcom/peel/ui/fo;->widget_ch_up_icon:I

    :goto_a
    invoke-virtual {v3, v1, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 349
    sget v1, Lcom/peel/ui/fp;->btn12:I

    const-string/jumbo v2, "setBackgroundResource"

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v4, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v4, :cond_10

    sget v0, Lcom/peel/ui/fo;->lockscreen_power_button_stateful:I

    :goto_b
    invoke-virtual {v3, v1, v2, v0}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 350
    sget v1, Lcom/peel/ui/fp;->btn12:I

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v2, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v2, :cond_11

    sget v0, Lcom/peel/ui/fo;->widget_ch_down_icon:I

    :goto_c
    invoke-virtual {v3, v1, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 352
    sget v0, Lcom/peel/ui/fp;->btn4:I

    sget v1, Lcom/peel/ui/fo;->quick_vol_up_states:I

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 353
    sget v0, Lcom/peel/ui/fp;->btn5:I

    sget v1, Lcom/peel/ui/fo;->quick_vol_dn_states:I

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 354
    sget v0, Lcom/peel/ui/fp;->btn6:I

    sget v1, Lcom/peel/ui/fo;->quick_ch_up_states:I

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 355
    sget v0, Lcom/peel/ui/fp;->btn7:I

    sget v1, Lcom/peel/ui/fo;->quick_ch_dn_states:I

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 356
    sget v0, Lcom/peel/ui/fp;->textview1:I

    iget-object v1, p0, Ltv/peel/widget/i;->c:Landroid/content/Context;

    sget v2, Lcom/peel/ui/ft;->lock_btn_vol:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 357
    sget v0, Lcom/peel/ui/fp;->textview2:I

    iget-object v1, p0, Ltv/peel/widget/i;->c:Landroid/content/Context;

    sget v2, Lcom/peel/ui/ft;->lock_btn_ch:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 359
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    if-eqz v0, :cond_4

    .line 360
    invoke-direct {p0, v3, p3}, Ltv/peel/widget/i;->a(Landroid/widget/RemoteViews;[Lcom/peel/control/h;)V

    .line 363
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v1, Lcom/peel/ui/fp;->btn_peel_tv:I

    invoke-virtual {v0, v3, v1, v9}, Ltv/peel/widget/util/a;->c(Landroid/widget/RemoteViews;II)V

    .line 366
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v1, Lcom/peel/ui/fp;->btn11:I

    const-string/jumbo v2, "Channel_Up"

    invoke-virtual {v0, v3, v1, v7, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 367
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v1, Lcom/peel/ui/fp;->btn12:I

    const-string/jumbo v2, "Channel_Down"

    invoke-virtual {v0, v3, v1, v7, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 368
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v1, Lcom/peel/ui/fp;->btn10:I

    const-string/jumbo v2, "Fast_Forward"

    invoke-virtual {v0, v3, v1, v7, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 369
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v1, Lcom/peel/ui/fp;->btn9:I

    const-string/jumbo v2, "Mute"

    invoke-virtual {v0, v3, v1, v7, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 370
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v1, Lcom/peel/ui/fp;->btn8:I

    const-string/jumbo v2, "Play"

    invoke-virtual {v0, v3, v1, v7, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 371
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v1, Lcom/peel/ui/fp;->btn7:I

    const-string/jumbo v2, "Channel_Down"

    invoke-virtual {v0, v3, v1, v7, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 372
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v1, Lcom/peel/ui/fp;->btn6:I

    const-string/jumbo v2, "Channel_Up"

    invoke-virtual {v0, v3, v1, v7, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 373
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v1, Lcom/peel/ui/fp;->btn5:I

    const-string/jumbo v2, "Volume_Down"

    invoke-virtual {v0, v3, v1, v7, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 374
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v1, Lcom/peel/ui/fp;->btn4:I

    const-string/jumbo v2, "Volume_Up"

    invoke-virtual {v0, v3, v1, v7, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 377
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v1, Lcom/peel/ui/fp;->btn7:I

    const-string/jumbo v2, "Channel_Down"

    invoke-virtual {v0, v3, v1, v8, v2}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 378
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v1, Lcom/peel/ui/fp;->btn6:I

    const-string/jumbo v2, "Channel_Up"

    invoke-virtual {v0, v3, v1, v8, v2}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 379
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v1, Lcom/peel/ui/fp;->btn5:I

    const-string/jumbo v2, "Volume_Down"

    invoke-virtual {v0, v3, v1, v8, v2}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 380
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v1, Lcom/peel/ui/fp;->btn4:I

    const-string/jumbo v2, "Volume_Up"

    invoke-virtual {v0, v3, v1, v8, v2}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 382
    :cond_4
    iget-object v0, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v1, Lcom/peel/ui/fp;->controllerScreen:I

    invoke-virtual {v0, v1, v3}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    .line 385
    :cond_5
    return-object p0

    .line 328
    :cond_6
    sget v0, Lcom/peel/ui/fp;->command_holder1:I

    invoke-virtual {v3, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 329
    sget v0, Lcom/peel/ui/fp;->command_holder2:I

    invoke-virtual {v3, v0, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 330
    sget v0, Lcom/peel/ui/fp;->btn8:I

    const/4 v1, 0x4

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_1

    .line 340
    :cond_7
    sget v0, Lcom/peel/ui/fo;->lockscreen_launch_button_states:I

    goto/16 :goto_2

    .line 341
    :cond_8
    sget v0, Lcom/peel/ui/fo;->lock_btn_normal:I

    goto/16 :goto_3

    .line 342
    :cond_9
    sget v0, Lcom/peel/ui/fo;->lockscreen_playpause_states:I

    goto/16 :goto_4

    .line 343
    :cond_a
    sget v0, Lcom/peel/ui/fo;->lock_btn_normal:I

    goto/16 :goto_5

    .line 344
    :cond_b
    sget v0, Lcom/peel/ui/fo;->lockscreen_mute_states:I

    goto/16 :goto_6

    .line 345
    :cond_c
    sget v0, Lcom/peel/ui/fo;->lock_btn_normal:I

    goto/16 :goto_7

    .line 346
    :cond_d
    sget v0, Lcom/peel/ui/fo;->lockscreen_ff_states:I

    goto/16 :goto_8

    .line 347
    :cond_e
    sget v0, Lcom/peel/ui/fo;->lock_btn_normal:I

    goto/16 :goto_9

    .line 348
    :cond_f
    sget v0, Lcom/peel/ui/fo;->lockscreen_btn_ch_up_states:I

    goto/16 :goto_a

    .line 349
    :cond_10
    sget v0, Lcom/peel/ui/fo;->lock_btn_normal:I

    goto/16 :goto_b

    .line 350
    :cond_11
    sget v0, Lcom/peel/ui/fo;->lockscreen_btn_ch_dn_states:I

    goto/16 :goto_c
.end method

.method public a(IZ[Lcom/peel/control/h;Z)Ltv/peel/widget/h;
    .locals 8

    .prologue
    const/16 v7, 0x64

    const/4 v6, 0x4

    const/16 v5, 0x8

    .line 389
    iget-boolean v0, p0, Ltv/peel/widget/i;->b:Z

    if-nez v0, :cond_3

    .line 391
    new-instance v1, Landroid/widget/RemoteViews;

    iget-object v0, p0, Ltv/peel/widget/i;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0, p1}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 393
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v2, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v2, :cond_0

    .line 394
    sget v0, Lcom/peel/ui/fp;->btn_peel_tv:I

    sget v2, Lcom/peel/ui/fo;->widget_expand_icon:I

    invoke-virtual {v1, v0, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 395
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v0, v2, :cond_0

    .line 396
    sget v0, Lcom/peel/ui/fp;->peel_logo_layout:I

    invoke-virtual {v1, v0, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 399
    :cond_0
    sget v2, Lcom/peel/ui/fp;->btn_peel_tv:I

    const-string/jumbo v3, "setBackgroundResource"

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v4, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v4, :cond_4

    sget v0, Lcom/peel/ui/fo;->lockscreen_power_button_stateful:I

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 400
    sget v0, Lcom/peel/ui/fp;->btn8:I

    invoke-virtual {v1, v0, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 401
    sget v2, Lcom/peel/ui/fp;->btn8:I

    const-string/jumbo v3, "setBackgroundResource"

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v4, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v4, :cond_5

    sget v0, Lcom/peel/ui/fo;->lockscreen_power_button_stateful:I

    :goto_1
    invoke-virtual {v1, v2, v3, v0}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 402
    sget v2, Lcom/peel/ui/fp;->btn8:I

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v3, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v3, :cond_6

    sget v0, Lcom/peel/ui/fo;->widget_play_pause_icon:I

    :goto_2
    invoke-virtual {v1, v2, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 403
    sget v2, Lcom/peel/ui/fp;->btn9:I

    const-string/jumbo v3, "setBackgroundResource"

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v4, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v4, :cond_7

    sget v0, Lcom/peel/ui/fo;->lockscreen_power_button_stateful:I

    :goto_3
    invoke-virtual {v1, v2, v3, v0}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 404
    sget v2, Lcom/peel/ui/fp;->btn9:I

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v3, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v3, :cond_8

    sget v0, Lcom/peel/ui/fo;->widget_mute_icon:I

    :goto_4
    invoke-virtual {v1, v2, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 405
    sget v2, Lcom/peel/ui/fp;->btn13:I

    const-string/jumbo v3, "setBackgroundResource"

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v4, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v4, :cond_9

    sget v0, Lcom/peel/ui/fo;->lockscreen_power_button_stateful:I

    :goto_5
    invoke-virtual {v1, v2, v3, v0}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 406
    sget v2, Lcom/peel/ui/fp;->btn13:I

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v3, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v3, :cond_a

    sget v0, Lcom/peel/ui/fo;->widget_rewind_icon:I

    :goto_6
    invoke-virtual {v1, v2, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 407
    sget v2, Lcom/peel/ui/fp;->btn14:I

    const-string/jumbo v3, "setBackgroundResource"

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v4, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v4, :cond_b

    sget v0, Lcom/peel/ui/fo;->lockscreen_power_button_stateful:I

    :goto_7
    invoke-virtual {v1, v2, v3, v0}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 408
    sget v2, Lcom/peel/ui/fp;->btn14:I

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v3, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v3, :cond_c

    sget v0, Lcom/peel/ui/fo;->widget_ff_icon:I

    :goto_8
    invoke-virtual {v1, v2, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 409
    sget v2, Lcom/peel/ui/fp;->btn15:I

    const-string/jumbo v3, "setBackgroundResource"

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v4, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v4, :cond_d

    sget v0, Lcom/peel/ui/fo;->lockscreen_power_button_stateful:I

    :goto_9
    invoke-virtual {v1, v2, v3, v0}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 410
    sget v2, Lcom/peel/ui/fp;->btn15:I

    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v3, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v3, :cond_e

    sget v0, Lcom/peel/ui/fo;->widget_play_pause_icon:I

    :goto_a
    invoke-virtual {v1, v2, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 412
    sget v0, Lcom/peel/ui/fp;->btn4:I

    sget v2, Lcom/peel/ui/fo;->quick_vol_up_states:I

    invoke-virtual {v1, v0, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 413
    sget v0, Lcom/peel/ui/fp;->btn5:I

    sget v2, Lcom/peel/ui/fo;->quick_vol_dn_states:I

    invoke-virtual {v1, v0, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 414
    sget v0, Lcom/peel/ui/fp;->textview1:I

    iget-object v2, p0, Ltv/peel/widget/i;->c:Landroid/content/Context;

    sget v3, Lcom/peel/ui/ft;->lock_btn_vol:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 416
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    if-eqz v0, :cond_2

    .line 417
    invoke-direct {p0, v1, p3}, Ltv/peel/widget/i;->a(Landroid/widget/RemoteViews;[Lcom/peel/control/h;)V

    .line 419
    const-string/jumbo v0, "Play"

    .line 420
    if-eqz p4, :cond_1

    .line 421
    const-string/jumbo v0, "Select"

    .line 424
    :cond_1
    sget v2, Lcom/peel/ui/fp;->btn8:I

    invoke-virtual {v1, v2, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 428
    iget-object v2, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v3, Lcom/peel/ui/fp;->btn_peel_tv:I

    const/16 v4, 0x14

    invoke-virtual {v2, v1, v3, v4}, Ltv/peel/widget/util/a;->c(Landroid/widget/RemoteViews;II)V

    .line 431
    iget-object v2, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v3, Lcom/peel/ui/fp;->btn9:I

    const-string/jumbo v4, "Mute"

    invoke-virtual {v2, v1, v3, v5, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 432
    iget-object v2, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v3, Lcom/peel/ui/fp;->btn8:I

    invoke-virtual {v2, v1, v3, v5, v0}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 433
    iget-object v2, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v3, Lcom/peel/ui/fp;->btn15:I

    invoke-virtual {v2, v1, v3, v5, v0}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 434
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v2, Lcom/peel/ui/fp;->btn14:I

    const-string/jumbo v3, "Fast_Forward"

    invoke-virtual {v0, v1, v2, v5, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 435
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v2, Lcom/peel/ui/fp;->btn13:I

    const-string/jumbo v3, "Rewind"

    invoke-virtual {v0, v1, v2, v5, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 436
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v2, Lcom/peel/ui/fp;->btn5:I

    const-string/jumbo v3, "Volume_Down"

    invoke-virtual {v0, v1, v2, v5, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 437
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v2, Lcom/peel/ui/fp;->btn4:I

    const-string/jumbo v3, "Volume_Up"

    invoke-virtual {v0, v1, v2, v5, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 440
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v2, Lcom/peel/ui/fp;->btn5:I

    const-string/jumbo v3, "Volume_Down"

    invoke-virtual {v0, v1, v2, v7, v3}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 441
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v2, Lcom/peel/ui/fp;->btn4:I

    const-string/jumbo v3, "Volume_Up"

    invoke-virtual {v0, v1, v2, v7, v3}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 443
    :cond_2
    iget-object v0, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v2, Lcom/peel/ui/fp;->controllerScreen:I

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    .line 446
    :cond_3
    return-object p0

    .line 399
    :cond_4
    sget v0, Lcom/peel/ui/fo;->lockscreen_launch_button_states:I

    goto/16 :goto_0

    .line 401
    :cond_5
    sget v0, Lcom/peel/ui/fo;->lock_btn_normal:I

    goto/16 :goto_1

    .line 402
    :cond_6
    sget v0, Lcom/peel/ui/fo;->lockscreen_playpause_states:I

    goto/16 :goto_2

    .line 403
    :cond_7
    sget v0, Lcom/peel/ui/fo;->lock_btn_normal:I

    goto/16 :goto_3

    .line 404
    :cond_8
    sget v0, Lcom/peel/ui/fo;->lockscreen_mute_states:I

    goto/16 :goto_4

    .line 405
    :cond_9
    sget v0, Lcom/peel/ui/fo;->lock_btn_normal:I

    goto/16 :goto_5

    .line 406
    :cond_a
    sget v0, Lcom/peel/ui/fo;->lockscreen_rewind_states:I

    goto/16 :goto_6

    .line 407
    :cond_b
    sget v0, Lcom/peel/ui/fo;->lock_btn_normal:I

    goto/16 :goto_7

    .line 408
    :cond_c
    sget v0, Lcom/peel/ui/fo;->lockscreen_ff_states:I

    goto/16 :goto_8

    .line 409
    :cond_d
    sget v0, Lcom/peel/ui/fo;->lock_btn_normal:I

    goto/16 :goto_9

    .line 410
    :cond_e
    sget v0, Lcom/peel/ui/fo;->lockscreen_playpause_states:I

    goto/16 :goto_a
.end method

.method public a(Landroid/content/Context;ILtv/peel/widget/util/a;)Ltv/peel/widget/h;
    .locals 2

    .prologue
    .line 37
    iput-object p1, p0, Ltv/peel/widget/i;->c:Landroid/content/Context;

    .line 38
    iput-object p3, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    .line 39
    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Ltv/peel/widget/i;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    .line 40
    return-object p0
.end method

.method public a(Landroid/content/Context;ILtv/peel/widget/util/a;Ljava/lang/String;Z)Ltv/peel/widget/h;
    .locals 6

    .prologue
    const/16 v5, 0x8

    .line 63
    iput-object p1, p0, Ltv/peel/widget/i;->c:Landroid/content/Context;

    .line 64
    iput-object p3, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    .line 66
    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Ltv/peel/widget/i;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 68
    if-eqz p4, :cond_0

    .line 69
    sget v1, Lcom/peel/ui/fp;->activity_text:I

    invoke-virtual {v0, v1, p4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 71
    :cond_0
    iget-object v1, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v2, Lcom/peel/ui/fp;->noti_main:I

    const/4 v3, 0x3

    invoke-virtual {v1, v0, v2, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 73
    if-eqz p5, :cond_2

    .line 74
    sget v1, Lcom/peel/ui/fp;->btn20:I

    sget v2, Lcom/peel/ui/fo;->widget_temp_down_stateful:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 75
    sget v1, Lcom/peel/ui/fp;->btn21:I

    sget v2, Lcom/peel/ui/fo;->widget_temp_up_stateful:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 76
    iget-object v1, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    if-eqz v1, :cond_1

    .line 77
    iget-object v1, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v2, Lcom/peel/ui/fp;->btn21:I

    const-string/jumbo v3, "UP"

    invoke-virtual {v1, v0, v2, v5, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 78
    iget-object v1, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v2, Lcom/peel/ui/fp;->btn20:I

    const-string/jumbo v3, "Down"

    invoke-virtual {v1, v0, v2, v5, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 89
    :cond_1
    :goto_0
    iput-object v0, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    .line 91
    return-object p0

    .line 81
    :cond_2
    const-string/jumbo v1, "Play"

    .line 82
    sget v2, Lcom/peel/ui/fp;->btn20:I

    sget v3, Lcom/peel/ui/fo;->widget_mute_stateful:I

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 83
    sget v2, Lcom/peel/ui/fp;->btn21:I

    sget v3, Lcom/peel/ui/fo;->widget_play_pause_stateful:I

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 84
    iget-object v2, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    if-eqz v2, :cond_1

    .line 85
    iget-object v2, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v3, Lcom/peel/ui/fp;->btn20:I

    const-string/jumbo v4, "Mute"

    invoke-virtual {v2, v0, v3, v5, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 86
    iget-object v2, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    sget v3, Lcom/peel/ui/fp;->btn21:I

    invoke-virtual {v2, v0, v3, v5, v1}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;[Ljava/lang/String;Z)Ltv/peel/widget/h;
    .locals 9

    .prologue
    const/16 v8, 0x92

    const/16 v7, 0x2f

    const/16 v0, 0x8

    const/4 v1, 0x0

    const/16 v6, 0xf5

    .line 164
    if-eqz p2, :cond_0

    array-length v2, p2

    if-lez v2, :cond_0

    .line 165
    iget-object v2, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v3, Lcom/peel/ui/fp;->device_name:I

    invoke-virtual {v2, v3, p1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 166
    iget-object v2, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    iget-object v3, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v4, Lcom/peel/ui/fp;->noti_main:I

    const/4 v5, 0x3

    invoke-virtual {v2, v3, v4, v5}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 168
    array-length v2, p2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    .line 170
    iget-object v2, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v3, Lcom/peel/ui/fp;->device_button:I

    const-string/jumbo v4, "setEnabled"

    invoke-virtual {v2, v3, v4, v1}, Landroid/widget/RemoteViews;->setBoolean(ILjava/lang/String;Z)V

    .line 176
    :cond_0
    :goto_0
    iget-object v2, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v3, Lcom/peel/ui/fp;->devices_selector:I

    invoke-virtual {v2, v3, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 177
    iget-object v2, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v3, Lcom/peel/ui/fp;->device_0:I

    invoke-virtual {v2, v3, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 178
    iget-object v2, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v3, Lcom/peel/ui/fp;->device_1:I

    invoke-virtual {v2, v3, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 179
    iget-object v2, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v3, Lcom/peel/ui/fp;->device_2:I

    invoke-virtual {v2, v3, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 180
    iget-object v2, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v3, Lcom/peel/ui/fp;->device_3:I

    invoke-virtual {v2, v3, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 181
    iget-object v2, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v3, Lcom/peel/ui/fp;->device_4:I

    invoke-virtual {v2, v3, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 182
    iget-object v2, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v3, Lcom/peel/ui/fp;->device_5:I

    invoke-virtual {v2, v3, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 183
    iget-object v2, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v3, Lcom/peel/ui/fp;->device_6:I

    invoke-virtual {v2, v3, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 185
    if-eqz p3, :cond_2

    iget-object v2, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    if-eqz v2, :cond_2

    if-eqz p2, :cond_2

    array-length v2, p2

    const/4 v3, 0x1

    if-le v2, v3, :cond_2

    .line 186
    iget-object v2, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v3, Lcom/peel/ui/fp;->devices_selector:I

    if-eqz p3, :cond_1

    move v0, v1

    :cond_1
    invoke-virtual {v2, v3, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 187
    array-length v0, p2

    packed-switch v0, :pswitch_data_0

    .line 254
    :cond_2
    :goto_1
    return-object p0

    .line 171
    :cond_3
    array-length v2, p2

    const/4 v3, 0x1

    if-le v2, v3, :cond_0

    .line 172
    iget-object v2, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v3, Lcom/peel/ui/fp;->device_button:I

    const-string/jumbo v4, "setEnabled"

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v4, v5}, Landroid/widget/RemoteViews;->setBoolean(ILjava/lang/String;Z)V

    goto :goto_0

    .line 189
    :pswitch_0
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v3, Lcom/peel/ui/fp;->device_6:I

    const/16 v4, 0x1c

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 190
    iget-object v0, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v2, Lcom/peel/ui/fp;->device_6:I

    const/4 v3, 0x6

    aget-object v3, p2, v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 191
    iget-object v0, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v2, Lcom/peel/ui/fp;->device_6:I

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 192
    const/4 v0, 0x6

    aget-object v0, p2, v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 193
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v3, Lcom/peel/ui/fp;->device_6:I

    const/16 v4, 0xc9

    invoke-static {v7, v8, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;II)V

    .line 198
    :goto_2
    :pswitch_1
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v3, Lcom/peel/ui/fp;->device_5:I

    const/16 v4, 0x1b

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 199
    iget-object v0, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v2, Lcom/peel/ui/fp;->device_5:I

    const/4 v3, 0x5

    aget-object v3, p2, v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 200
    iget-object v0, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v2, Lcom/peel/ui/fp;->device_5:I

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 201
    const/4 v0, 0x5

    aget-object v0, p2, v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 202
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v3, Lcom/peel/ui/fp;->device_5:I

    const/16 v4, 0xc9

    invoke-static {v7, v8, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;II)V

    .line 207
    :goto_3
    :pswitch_2
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v3, Lcom/peel/ui/fp;->device_4:I

    const/16 v4, 0x1a

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 208
    iget-object v0, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v2, Lcom/peel/ui/fp;->device_4:I

    const/4 v3, 0x4

    aget-object v3, p2, v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 209
    iget-object v0, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v2, Lcom/peel/ui/fp;->device_4:I

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 210
    const/4 v0, 0x4

    aget-object v0, p2, v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 211
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v3, Lcom/peel/ui/fp;->device_4:I

    const/16 v4, 0xc9

    invoke-static {v7, v8, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;II)V

    .line 216
    :goto_4
    :pswitch_3
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v3, Lcom/peel/ui/fp;->device_3:I

    const/16 v4, 0x19

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 217
    iget-object v0, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v2, Lcom/peel/ui/fp;->device_3:I

    const/4 v3, 0x3

    aget-object v3, p2, v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 218
    iget-object v0, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v2, Lcom/peel/ui/fp;->device_3:I

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 219
    const/4 v0, 0x3

    aget-object v0, p2, v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 220
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v3, Lcom/peel/ui/fp;->device_3:I

    const/16 v4, 0xc9

    invoke-static {v7, v8, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;II)V

    .line 225
    :goto_5
    :pswitch_4
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v3, Lcom/peel/ui/fp;->device_2:I

    const/16 v4, 0x18

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 226
    iget-object v0, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v2, Lcom/peel/ui/fp;->device_2:I

    const/4 v3, 0x2

    aget-object v3, p2, v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 227
    iget-object v0, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v2, Lcom/peel/ui/fp;->device_2:I

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 228
    const/4 v0, 0x2

    aget-object v0, p2, v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 229
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v3, Lcom/peel/ui/fp;->device_2:I

    const/16 v4, 0xc9

    invoke-static {v7, v8, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;II)V

    .line 234
    :goto_6
    :pswitch_5
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v3, Lcom/peel/ui/fp;->device_1:I

    const/16 v4, 0x17

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 235
    iget-object v0, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v2, Lcom/peel/ui/fp;->device_1:I

    const/4 v3, 0x1

    aget-object v3, p2, v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 236
    iget-object v0, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v2, Lcom/peel/ui/fp;->device_1:I

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 237
    const/4 v0, 0x1

    aget-object v0, p2, v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 238
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v3, Lcom/peel/ui/fp;->device_1:I

    const/16 v4, 0xc9

    invoke-static {v7, v8, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;II)V

    .line 243
    :goto_7
    :pswitch_6
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v3, Lcom/peel/ui/fp;->device_0:I

    const/16 v4, 0x16

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 244
    iget-object v0, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v2, Lcom/peel/ui/fp;->device_0:I

    aget-object v3, p2, v1

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 245
    iget-object v0, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v2, Lcom/peel/ui/fp;->device_0:I

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 246
    aget-object v0, p2, v1

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 247
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    iget-object v1, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v2, Lcom/peel/ui/fp;->device_0:I

    const/16 v3, 0xc9

    invoke-static {v7, v8, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;II)V

    goto/16 :goto_1

    .line 195
    :cond_4
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v3, Lcom/peel/ui/fp;->device_6:I

    invoke-static {v6, v6, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;II)V

    goto/16 :goto_2

    .line 204
    :cond_5
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v3, Lcom/peel/ui/fp;->device_5:I

    invoke-static {v6, v6, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;II)V

    goto/16 :goto_3

    .line 213
    :cond_6
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v3, Lcom/peel/ui/fp;->device_4:I

    invoke-static {v6, v6, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;II)V

    goto/16 :goto_4

    .line 222
    :cond_7
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v3, Lcom/peel/ui/fp;->device_3:I

    invoke-static {v6, v6, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;II)V

    goto/16 :goto_5

    .line 231
    :cond_8
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v3, Lcom/peel/ui/fp;->device_2:I

    invoke-static {v6, v6, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;II)V

    goto/16 :goto_6

    .line 240
    :cond_9
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v3, Lcom/peel/ui/fp;->device_1:I

    invoke-static {v6, v6, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;II)V

    goto/16 :goto_7

    .line 249
    :cond_a
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    iget-object v1, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v2, Lcom/peel/ui/fp;->device_0:I

    invoke-static {v6, v6, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;II)V

    goto/16 :goto_1

    .line 187
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Z)Ltv/peel/widget/h;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 96
    iput-boolean p1, p0, Ltv/peel/widget/i;->b:Z

    .line 97
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Ltv/peel/widget/i;->c:Landroid/content/Context;

    const-string/jumbo v1, "widget_pref"

    invoke-virtual {v0, v1, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 100
    iget-boolean v1, p0, Ltv/peel/widget/i;->b:Z

    if-nez v1, :cond_3

    .line 101
    iget-object v1, p0, Ltv/peel/widget/i;->c:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v2, "is_setup_complete"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_1

    .line 102
    iget-object v1, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v2, Lcom/peel/ui/fp;->title:I

    iget-object v3, p0, Ltv/peel/widget/i;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/peel/ui/ft;->label_setup_now:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 107
    :goto_0
    const-string/jumbo v1, "clear_mode"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 108
    iget-object v0, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v1, Lcom/peel/ui/fp;->hide_button_text:I

    iget-object v2, p0, Ltv/peel/widget/i;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/peel/ui/ft;->label_clear:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 109
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    iget-object v1, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v2, Lcom/peel/ui/fp;->hide_button:I

    const/16 v3, 0x1f

    invoke-virtual {v0, v1, v2, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 128
    :cond_0
    :goto_1
    return-object p0

    .line 104
    :cond_1
    iget-object v1, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v2, Lcom/peel/ui/fp;->title:I

    iget-object v3, p0, Ltv/peel/widget/i;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/peel/ui/ft;->setup_device_now:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_0

    .line 111
    :cond_2
    iget-object v0, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v1, Lcom/peel/ui/fp;->hide_button_text:I

    iget-object v2, p0, Ltv/peel/widget/i;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/peel/ui/ft;->label_hide:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 112
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    iget-object v1, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v2, Lcom/peel/ui/fp;->hide_button:I

    invoke-virtual {v0, v1, v2, v5}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    goto :goto_1

    .line 115
    :cond_3
    iget-object v0, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v1, Lcom/peel/ui/fp;->title:I

    iget-object v2, p0, Ltv/peel/widget/i;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/peel/ui/ft;->text_hidden_text:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 116
    iget-object v0, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v1, Lcom/peel/ui/fp;->hide_button_text:I

    iget-object v2, p0, Ltv/peel/widget/i;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/peel/ui/ft;->label_show:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 117
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    iget-object v1, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v2, Lcom/peel/ui/fp;->hide_button:I

    invoke-virtual {v0, v1, v2, v5}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 118
    iget-object v0, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v1, Lcom/peel/ui/fp;->collapsable_part:I

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 120
    iget-object v0, p0, Ltv/peel/widget/i;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "is_setup_complete"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_4

    .line 121
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    iget-object v1, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v2, Lcom/peel/ui/fp;->title:I

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    goto/16 :goto_1

    .line 123
    :cond_4
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    iget-object v1, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v2, Lcom/peel/ui/fp;->title:I

    const/16 v3, 0x1e

    invoke-virtual {v0, v1, v2, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    goto/16 :goto_1
.end method

.method public b()Landroid/widget/RemoteViews;
    .locals 1

    .prologue
    .line 511
    iget-object v0, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    return-object v0
.end method

.method public b(Z)Ltv/peel/widget/h;
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 133
    iput-boolean p1, p0, Ltv/peel/widget/i;->b:Z

    .line 135
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    iget-object v1, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v2, Lcom/peel/ui/fp;->device_button:I

    const/4 v3, 0x5

    invoke-virtual {v0, v1, v2, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 139
    :cond_0
    iget-boolean v0, p0, Ltv/peel/widget/i;->b:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 141
    iget-object v0, p0, Ltv/peel/widget/i;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/peel/ui/ft;->button_show:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 142
    iget-object v1, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v2, Lcom/peel/ui/fp;->collapsable_part:I

    const/16 v3, 0x8

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 143
    iget-object v1, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v2, Lcom/peel/ui/fp;->hide_button_text:I

    invoke-virtual {v1, v2, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 144
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    iget-object v1, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v2, Lcom/peel/ui/fp;->hide_button:I

    invoke-virtual {v0, v1, v2, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 159
    :goto_0
    return-object p0

    .line 153
    :cond_1
    iget-object v0, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v1, Lcom/peel/ui/fp;->collapsable_part:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 154
    iget-object v0, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v1, Lcom/peel/ui/fp;->hide_button_text:I

    iget-object v2, p0, Ltv/peel/widget/i;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/peel/ui/ft;->label_hide:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 155
    iget-object v0, p0, Ltv/peel/widget/i;->d:Ltv/peel/widget/util/a;

    iget-object v1, p0, Ltv/peel/widget/i;->a:Landroid/widget/RemoteViews;

    sget v2, Lcom/peel/ui/fp;->hide_button:I

    invoke-virtual {v0, v1, v2, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    goto :goto_0
.end method
