.class Ltv/peel/widget/service/p;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Ltv/peel/widget/service/o;


# direct methods
.method constructor <init>(Ltv/peel/widget/service/o;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Ltv/peel/widget/service/p;->a:Ltv/peel/widget/service/o;

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 112
    iget-boolean v0, p0, Ltv/peel/widget/service/p;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/peel/widget/service/p;->j:Ljava/lang/Object;

    if-nez v0, :cond_1

    .line 113
    :cond_0
    iget-object v0, p0, Ltv/peel/widget/service/p;->a:Ltv/peel/widget/service/o;

    iget-object v0, v0, Ltv/peel/widget/service/o;->b:Ltv/peel/widget/service/n;

    iget-object v0, v0, Ltv/peel/widget/service/n;->d:Lcom/peel/util/t;

    invoke-virtual {v0, v6, v7, v7}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    .line 154
    :goto_0
    return-void

    .line 118
    :cond_1
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    iget-object v0, p0, Ltv/peel/widget/service/p;->j:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 119
    const-string/jumbo v0, "showquickpanel"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 120
    const-string/jumbo v2, "showmode"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 121
    iget-object v3, p0, Ltv/peel/widget/service/p;->a:Ltv/peel/widget/service/o;

    iget-object v3, v3, Ltv/peel/widget/service/o;->b:Ltv/peel/widget/service/n;

    iget-object v3, v3, Ltv/peel/widget/service/n;->a:Landroid/content/Context;

    const-string/jumbo v4, "widget_pref"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 123
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    const-string/jumbo v4, "clear"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 124
    const-string/jumbo v2, "noofreboots"

    const/4 v4, 0x3

    invoke-virtual {v1, v2, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v1

    .line 125
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string/jumbo v4, "clear_mode"

    const/4 v5, 0x1

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 126
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string/jumbo v4, "noofreboots"

    invoke-interface {v2, v4, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 135
    :cond_2
    :goto_1
    if-eqz v0, :cond_4

    const-string/jumbo v1, "yes"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 136
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "showquickpanel"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 137
    iget-object v0, p0, Ltv/peel/widget/service/p;->a:Ltv/peel/widget/service/o;

    iget-object v0, v0, Ltv/peel/widget/service/o;->b:Ltv/peel/widget/service/n;

    iget-object v0, v0, Ltv/peel/widget/service/n;->d:Lcom/peel/util/t;

    const/4 v1, 0x1

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 151
    :catch_0
    move-exception v0

    .line 152
    iget-object v0, p0, Ltv/peel/widget/service/p;->a:Ltv/peel/widget/service/o;

    iget-object v0, v0, Ltv/peel/widget/service/o;->b:Ltv/peel/widget/service/n;

    iget-object v0, v0, Ltv/peel/widget/service/n;->d:Lcom/peel/util/t;

    invoke-virtual {v0, v6, v7, v7}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 128
    :cond_3
    :try_start_1
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "clear_mode"

    const/4 v4, 0x0

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 130
    const-string/jumbo v1, "noofreboots"

    invoke-interface {v3, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 131
    iget-object v1, p0, Ltv/peel/widget/service/p;->a:Ltv/peel/widget/service/o;

    iget-object v1, v1, Ltv/peel/widget/service/o;->b:Ltv/peel/widget/service/n;

    iget-object v1, v1, Ltv/peel/widget/service/n;->a:Landroid/content/Context;

    const-string/jumbo v2, "widget_pref"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "noofreboots"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_1

    .line 139
    :cond_4
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "showquickpanel"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 140
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "boot_count"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 142
    const-string/jumbo v0, "clear_mode"

    invoke-interface {v3, v0}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 143
    iget-object v0, p0, Ltv/peel/widget/service/p;->a:Ltv/peel/widget/service/o;

    iget-object v0, v0, Ltv/peel/widget/service/o;->b:Ltv/peel/widget/service/n;

    iget-object v0, v0, Ltv/peel/widget/service/n;->a:Landroid/content/Context;

    const-string/jumbo v1, "widget_pref"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "clear_mode"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 146
    :cond_5
    const-string/jumbo v0, "noofreboots"

    invoke-interface {v3, v0}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 147
    iget-object v0, p0, Ltv/peel/widget/service/p;->a:Ltv/peel/widget/service/o;

    iget-object v0, v0, Ltv/peel/widget/service/o;->b:Ltv/peel/widget/service/n;

    iget-object v0, v0, Ltv/peel/widget/service/n;->a:Landroid/content/Context;

    const-string/jumbo v1, "widget_pref"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "noofreboots"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 149
    :cond_6
    iget-object v0, p0, Ltv/peel/widget/service/p;->a:Ltv/peel/widget/service/o;

    iget-object v0, v0, Ltv/peel/widget/service/o;->b:Ltv/peel/widget/service/n;

    iget-object v0, v0, Ltv/peel/widget/service/n;->d:Lcom/peel/util/t;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
