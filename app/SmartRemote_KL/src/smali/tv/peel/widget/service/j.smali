.class Ltv/peel/widget/service/j;
.super Ltv/peel/widget/service/f;


# instance fields
.field final synthetic a:Ltv/peel/widget/service/PeelService;


# direct methods
.method constructor <init>(Ltv/peel/widget/service/PeelService;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Ltv/peel/widget/service/j;->a:Ltv/peel/widget/service/PeelService;

    invoke-direct {p0}, Ltv/peel/widget/service/f;-><init>()V

    return-void
.end method

.method private a(ILjava/lang/String;Lcom/peel/control/h;)V
    .locals 8

    .prologue
    const/16 v2, 0x1388

    const/4 v5, -0x1

    .line 127
    if-eqz p3, :cond_0

    .line 128
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    invoke-virtual {p3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->h()I

    move-result v5

    invoke-virtual {p3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v6

    .line 129
    invoke-virtual {p3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->d()I

    move-result v7

    move v3, p1

    move-object v4, p2

    .line 128
    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    .line 134
    :goto_0
    return-void

    .line 131
    :cond_0
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    const-string/jumbo v6, "no device"

    move v3, p1

    move-object v4, p2

    move v7, v5

    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    goto :goto_0
.end method

.method private b(Ljava/lang/String;)Lcom/peel/control/a;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 206
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0, p1}, Lcom/peel/control/am;->a(Ljava/lang/String;)Lcom/peel/control/RoomControl;

    move-result-object v1

    .line 207
    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v4

    .line 208
    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->d()Lcom/peel/control/a;

    move-result-object v0

    .line 209
    if-eqz v0, :cond_1

    const/4 v3, 0x2

    invoke-virtual {v0}, Lcom/peel/control/a;->f()I

    move-result v5

    if-ne v3, v5, :cond_1

    .line 236
    :cond_0
    :goto_0
    return-object v0

    .line 215
    :cond_1
    iget-object v0, p0, Ltv/peel/widget/service/j;->a:Ltv/peel/widget/service/PeelService;

    invoke-virtual {v0}, Ltv/peel/widget/service/PeelService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v3, "last_activity"

    const/4 v5, 0x0

    invoke-interface {v0, v3, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 217
    if-eqz v3, :cond_2

    .line 219
    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v5

    array-length v6, v5

    move v1, v2

    :goto_1
    if-ge v1, v6, :cond_2

    aget-object v0, v5, v1

    .line 220
    invoke-virtual {v0}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 219
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 226
    :cond_2
    array-length v5, v4

    move v3, v2

    :goto_2
    if-ge v3, v5, :cond_5

    aget-object v0, v4, v3

    .line 227
    invoke-virtual {v0}, Lcom/peel/control/a;->d()[Ljava/lang/String;

    move-result-object v6

    .line 228
    if-nez v6, :cond_4

    .line 226
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 230
    :cond_4
    array-length v7, v6

    move v1, v2

    :goto_3
    if-ge v1, v7, :cond_3

    aget-object v8, v6, v1

    .line 231
    const-string/jumbo v9, "live"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 230
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 236
    :cond_5
    aget-object v0, v4, v2

    goto :goto_0
.end method

.method private n()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 320
    .line 322
    :try_start_0
    invoke-virtual {p0}, Ltv/peel/widget/service/j;->i()[Ltv/peel/widget/service/DeviceParcelable;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 327
    :goto_0
    if-eqz v1, :cond_0

    array-length v2, v1

    if-nez v2, :cond_1

    :cond_0
    :goto_1
    return-object v0

    .line 323
    :catch_0
    move-exception v1

    .line 324
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    move-object v1, v0

    goto :goto_0

    .line 327
    :cond_1
    const/4 v0, 0x0

    aget-object v0, v1, v0

    invoke-virtual {v0}, Ltv/peel/widget/service/DeviceParcelable;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 85
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1, p1}, Lcom/peel/control/am;->a(Ljava/lang/String;)Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/control/am;->b(Lcom/peel/control/RoomControl;)V

    .line 86
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 165
    iget-object v0, p0, Ltv/peel/widget/service/j;->a:Ltv/peel/widget/service/PeelService;

    invoke-virtual {v0}, Ltv/peel/widget/service/PeelService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "country"

    const-string/jumbo v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "Japan"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Ltv/peel/widget/service/j;->a:Ltv/peel/widget/service/PeelService;

    invoke-virtual {v0}, Ltv/peel/widget/service/PeelService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->d()Lcom/peel/control/a;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, p2, v2}, Lcom/peel/util/bx;->a(Landroid/content/Context;Lcom/peel/control/a;Ljava/lang/String;Lcom/peel/util/t;)V

    .line 171
    :goto_0
    return-void

    .line 168
    :cond_0
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0, p1}, Lcom/peel/control/am;->c(Ljava/lang/String;)Lcom/peel/control/h;

    move-result-object v0

    .line 169
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "live://channel/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/control/h;->a(Ljava/net/URI;)Z

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 118
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0, p2}, Lcom/peel/control/am;->c(Ljava/lang/String;)Lcom/peel/control/h;

    move-result-object v0

    .line 120
    if-eqz v0, :cond_0

    .line 121
    invoke-virtual {v0, p1}, Lcom/peel/control/h;->c(Ljava/lang/String;)Z

    .line 122
    invoke-direct {p0, p3, p1, v0}, Ltv/peel/widget/service/j;->a(ILjava/lang/String;Lcom/peel/control/h;)V

    .line 124
    :cond_0
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 241
    invoke-direct {p0, p2}, Ltv/peel/widget/service/j;->b(Ljava/lang/String;)Lcom/peel/control/a;

    move-result-object v1

    .line 242
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0, p1}, Lcom/peel/control/am;->c(Ljava/lang/String;)Lcom/peel/control/h;

    move-result-object v2

    .line 243
    invoke-virtual {v1, v2}, Lcom/peel/control/a;->b(Lcom/peel/control/h;)[Ljava/lang/Integer;

    move-result-object v0

    .line 246
    if-eqz v0, :cond_0

    array-length v0, v0

    if-nez v0, :cond_1

    .line 247
    :cond_0
    new-array v0, v5, [Ljava/lang/Integer;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v4

    .line 252
    :goto_0
    invoke-virtual {v1, v2}, Lcom/peel/control/a;->a(Lcom/peel/control/h;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Lcom/peel/control/a;->b(Lcom/peel/control/h;Ljava/lang/String;[Ljava/lang/Integer;)V

    .line 253
    return-void

    .line 249
    :cond_1
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v5

    goto :goto_0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 302
    if-nez p2, :cond_1

    .line 317
    :cond_0
    :goto_0
    return-void

    .line 305
    :cond_1
    const-string/jumbo v0, "Volume_Down"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "Volume_Up"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "Mute"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 306
    :cond_2
    invoke-direct {p0}, Ltv/peel/widget/service/j;->n()Ljava/lang/String;

    move-result-object v0

    .line 308
    if-eqz v0, :cond_3

    .line 309
    invoke-virtual {p0, p1, v0, p3}, Ltv/peel/widget/service/j;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 313
    :cond_3
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0, p2}, Lcom/peel/control/am;->e(Ljava/lang/String;)Lcom/peel/control/a;

    move-result-object v0

    .line 314
    if-eqz v0, :cond_0

    .line 315
    invoke-virtual {v0, p1}, Lcom/peel/control/a;->b(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public b()[Ltv/peel/widget/service/DeviceParcelable;
    .locals 9

    .prologue
    .line 51
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->e()[Lcom/peel/control/h;

    move-result-object v8

    .line 52
    if-nez v8, :cond_0

    .line 53
    const/4 v0, 0x0

    .line 66
    :goto_0
    return-object v0

    .line 55
    :cond_0
    array-length v0, v8

    new-array v7, v0, [Ltv/peel/widget/service/DeviceParcelable;

    .line 57
    const/4 v0, 0x0

    move v6, v0

    :goto_1
    array-length v0, v8

    if-ge v6, v0, :cond_1

    .line 58
    aget-object v0, v8, v6

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v2

    .line 59
    aget-object v0, v8, v6

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v1

    .line 60
    aget-object v0, v8, v6

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v4

    .line 62
    iget-object v0, p0, Ltv/peel/widget/service/j;->a:Ltv/peel/widget/service/PeelService;

    invoke-virtual {v0}, Ltv/peel/widget/service/PeelService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/peel/util/bx;->d(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    .line 63
    new-instance v0, Ltv/peel/widget/service/DeviceParcelable;

    invoke-static {v2}, Lcom/peel/util/bx;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct/range {v0 .. v5}, Ltv/peel/widget/service/DeviceParcelable;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v0, v7, v6

    .line 57
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1

    :cond_1
    move-object v0, v7

    .line 66
    goto :goto_0
.end method

.method public c()[Ltv/peel/widget/service/ContentsParcelable;
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 71
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->c()[Lcom/peel/control/RoomControl;

    move-result-object v3

    .line 72
    array-length v1, v3

    new-array v4, v1, [Ltv/peel/widget/service/ContentsParcelable;

    .line 75
    array-length v5, v3

    move v1, v0

    :goto_0
    if-ge v0, v5, :cond_0

    aget-object v2, v3, v0

    .line 76
    new-instance v6, Ltv/peel/widget/service/ContentsParcelable;

    invoke-virtual {v2}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v7

    invoke-virtual {v7}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/at;->a()Ljava/lang/String;

    move-result-object v2

    const/4 v8, 0x0

    invoke-direct {v6, v7, v2, v8}, Ltv/peel/widget/service/ContentsParcelable;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    .line 77
    add-int/lit8 v2, v1, 0x1

    aput-object v6, v4, v1

    .line 75
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_0

    .line 80
    :cond_0
    return-object v4
.end method

.method public d()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 90
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->e()[Lcom/peel/control/h;

    move-result-object v2

    .line 92
    if-eqz v2, :cond_1

    .line 93
    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 94
    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/data/g;->d()I

    move-result v4

    .line 95
    const/4 v5, 0x3

    if-eq v5, v4, :cond_0

    const/4 v5, 0x2

    if-eq v5, v4, :cond_0

    const/16 v5, 0x14

    if-ne v5, v4, :cond_2

    .line 96
    :cond_0
    const/4 v0, 0x1

    .line 101
    :cond_1
    return v0

    .line 93
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public e()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 106
    iget-object v0, p0, Ltv/peel/widget/service/j;->a:Ltv/peel/widget/service/PeelService;

    const-string/jumbo v1, "widget_pref"

    invoke-virtual {v0, v1, v2}, Ltv/peel/widget/service/PeelService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 107
    const-string/jumbo v1, "is_setup_complete"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public f()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 112
    iget-object v0, p0, Ltv/peel/widget/service/j;->a:Ltv/peel/widget/service/PeelService;

    const-string/jumbo v1, "widget_pref"

    invoke-virtual {v0, v1, v2}, Ltv/peel/widget/service/PeelService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 113
    const-string/jumbo v1, "notification"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public g()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ltv/peel/widget/service/ChannelParcelable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 138
    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v2

    .line 139
    const-string/jumbo v0, "live"

    invoke-static {v0}, Lcom/peel/content/a;->c(Ljava/lang/String;)Lcom/peel/content/library/Library;

    move-result-object v0

    check-cast v0, Lcom/peel/content/library/LiveLibrary;

    .line 140
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 142
    sget-object v3, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    if-nez v3, :cond_0

    const/4 v0, 0x0

    .line 160
    :goto_0
    return-object v0

    .line 144
    :cond_0
    sget-object v3, Lcom/peel/content/a;->f:Lcom/peel/content/user/User;

    invoke-virtual {v3}, Lcom/peel/content/user/User;->h()Landroid/os/Bundle;

    move-result-object v3

    .line 145
    if-eqz v3, :cond_2

    if-eqz v2, :cond_2

    if-eqz v0, :cond_2

    .line 146
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, "/"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 147
    if-eqz v3, :cond_2

    .line 148
    invoke-virtual {v0}, Lcom/peel/content/library/LiveLibrary;->i()[Lcom/peel/data/Channel;

    move-result-object v0

    check-cast v0, [Lcom/peel/data/Channel;

    .line 150
    if-eqz v0, :cond_2

    .line 151
    array-length v4, v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v5, v0, v2

    .line 152
    invoke-virtual {v5}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 153
    new-instance v6, Ltv/peel/widget/service/ChannelParcelable;

    invoke-virtual {v5}, Lcom/peel/data/Channel;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5}, Lcom/peel/data/Channel;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5}, Lcom/peel/data/Channel;->f()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5}, Lcom/peel/data/Channel;->m()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v6, v7, v8, v9, v5}, Ltv/peel/widget/service/ChannelParcelable;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 151
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 160
    goto :goto_0
.end method

.method public h()Z
    .locals 3

    .prologue
    .line 175
    iget-object v0, p0, Ltv/peel/widget/service/j;->a:Ltv/peel/widget/service/PeelService;

    invoke-virtual {v0}, Ltv/peel/widget/service/PeelService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "samsungid"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()[Ltv/peel/widget/service/DeviceParcelable;
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v5, 0x0

    const/4 v7, 0x0

    .line 180
    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v0

    .line 183
    if-eqz v0, :cond_3

    .line 184
    invoke-virtual {v0}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ltv/peel/widget/service/j;->b(Ljava/lang/String;)Lcom/peel/control/a;

    move-result-object v0

    .line 185
    invoke-virtual {v0, v7}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v4

    .line 186
    if-nez v4, :cond_4

    .line 187
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 189
    invoke-virtual {v0}, Lcom/peel/control/a;->e()[Lcom/peel/control/h;

    move-result-object v9

    array-length v10, v9

    move v6, v7

    :goto_0
    if-ge v6, v10, :cond_2

    aget-object v4, v9, v6

    .line 190
    const/16 v0, 0xd

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->d()I

    move-result v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x5

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->d()I

    move-result v1

    if-eq v0, v1, :cond_0

    const/16 v0, 0xe

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->d()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 191
    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    if-ne v11, v0, :cond_1

    .line 192
    :cond_0
    new-instance v0, Ltv/peel/widget/service/DeviceParcelable;

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->d()I

    move-result v2

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->d()I

    move-result v3

    invoke-static {v3}, Lcom/peel/util/bx;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Ltv/peel/widget/service/DeviceParcelable;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 189
    :cond_1
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 195
    :cond_2
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v5, v0, [Ltv/peel/widget/service/DeviceParcelable;

    .line 196
    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 202
    :cond_3
    :goto_1
    return-object v5

    .line 198
    :cond_4
    new-array v6, v11, [Ltv/peel/widget/service/DeviceParcelable;

    .line 199
    new-instance v0, Ltv/peel/widget/service/DeviceParcelable;

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->d()I

    move-result v2

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->d()I

    move-result v3

    invoke-static {v3}, Lcom/peel/util/bx;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Ltv/peel/widget/service/DeviceParcelable;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v0, v6, v7

    move-object v5, v6

    goto :goto_1
.end method

.method public j()Ljava/lang/String;
    .locals 3

    .prologue
    .line 257
    iget-object v0, p0, Ltv/peel/widget/service/j;->a:Ltv/peel/widget/service/PeelService;

    invoke-virtual {v0}, Ltv/peel/widget/service/PeelService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "country"

    const-string/jumbo v2, "United States Of America"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .locals 3

    .prologue
    .line 262
    iget-object v0, p0, Ltv/peel/widget/service/j;->a:Ltv/peel/widget/service/PeelService;

    invoke-virtual {v0}, Ltv/peel/widget/service/PeelService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "config_legacy"

    const-string/jumbo v2, "name|United States Of America|endpoint|usa|iso|US|type|5digitzip"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 263
    invoke-static {v0}, Lcom/peel/util/bx;->b(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 264
    const-string/jumbo v1, "endpoint"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public l()[Ltv/peel/widget/service/ContentsParcelable;
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 269
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    .line 271
    if-nez v0, :cond_0

    .line 272
    const/4 v0, 0x0

    .line 286
    :goto_0
    return-object v0

    .line 274
    :cond_0
    invoke-virtual {v0}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v3

    .line 276
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 277
    iget-object v0, p0, Ltv/peel/widget/service/j;->a:Ltv/peel/widget/service/PeelService;

    invoke-virtual {v0}, Ltv/peel/widget/service/PeelService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v5, "setup_type"

    invoke-interface {v0, v5, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-ne v1, v0, :cond_2

    move v0, v1

    .line 278
    :goto_1
    array-length v5, v3

    :goto_2
    if-ge v2, v5, :cond_4

    aget-object v6, v3, v2

    .line 279
    invoke-virtual {v6, v1}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v7

    .line 280
    if-eqz v7, :cond_1

    if-nez v0, :cond_3

    invoke-virtual {v7}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v7

    invoke-virtual {v7}, Lcom/peel/data/g;->d()I

    move-result v7

    const/4 v8, 0x5

    if-ne v7, v8, :cond_3

    .line 278
    :cond_1
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_2
    move v0, v2

    .line 277
    goto :goto_1

    .line 282
    :cond_3
    new-instance v7, Ltv/peel/widget/service/ContentsParcelable;

    invoke-virtual {v6}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6}, Lcom/peel/control/a;->d()[Ljava/lang/String;

    move-result-object v6

    invoke-direct {v7, v8, v9, v6}, Ltv/peel/widget/service/ContentsParcelable;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    .line 283
    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 286
    :cond_4
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ltv/peel/widget/service/ContentsParcelable;

    invoke-interface {v4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ltv/peel/widget/service/ContentsParcelable;

    goto :goto_0
.end method

.method public m()Ltv/peel/widget/service/ContentsParcelable;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 291
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    .line 294
    if-eqz v1, :cond_1

    .line 295
    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Ltv/peel/widget/service/j;->b(Ljava/lang/String;)Lcom/peel/control/a;

    move-result-object v1

    .line 297
    :goto_0
    if-nez v1, :cond_0

    :goto_1
    return-object v0

    :cond_0
    new-instance v0, Ltv/peel/widget/service/ContentsParcelable;

    invoke-virtual {v1}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/peel/control/a;->d()[Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v3, v1}, Ltv/peel/widget/service/ContentsParcelable;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method
