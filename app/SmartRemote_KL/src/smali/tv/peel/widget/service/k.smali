.class Ltv/peel/widget/service/k;
.super Ltv/peel/widget/service/i;


# instance fields
.field final synthetic a:Ltv/peel/widget/service/QuickConnectService;


# direct methods
.method constructor <init>(Ltv/peel/widget/service/QuickConnectService;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Ltv/peel/widget/service/k;->a:Ltv/peel/widget/service/QuickConnectService;

    invoke-direct {p0}, Ltv/peel/widget/service/i;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ltv/peel/widget/service/Device;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    if-nez p1, :cond_0

    .line 57
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->e()[Lcom/peel/control/h;

    move-result-object v0

    move-object v8, v0

    .line 62
    :goto_0
    if-nez v8, :cond_1

    .line 63
    const/4 v0, 0x0

    .line 77
    :goto_1
    return-object v0

    .line 59
    :cond_0
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0, p1}, Lcom/peel/control/am;->d(Ljava/lang/String;)[Lcom/peel/control/h;

    move-result-object v0

    move-object v8, v0

    goto :goto_0

    .line 65
    :cond_1
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 67
    const/4 v0, 0x0

    move v6, v0

    :goto_2
    array-length v0, v8

    if-ge v6, v0, :cond_3

    .line 68
    aget-object v0, v8, v6

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v2

    .line 69
    const/4 v0, 0x1

    if-ne v2, v0, :cond_2

    .line 70
    aget-object v0, v8, v6

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v1

    .line 71
    aget-object v0, v8, v6

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v4

    .line 72
    iget-object v0, p0, Ltv/peel/widget/service/k;->a:Ltv/peel/widget/service/QuickConnectService;

    invoke-virtual {v0}, Ltv/peel/widget/service/QuickConnectService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/peel/util/bx;->d(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    .line 73
    new-instance v0, Ltv/peel/widget/service/Device;

    invoke-static {v2}, Lcom/peel/util/bx;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct/range {v0 .. v5}, Ltv/peel/widget/service/Device;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    :cond_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_2

    :cond_3
    move-object v0, v7

    .line 77
    goto :goto_1
.end method

.method public a()Ltv/peel/widget/service/Room;
    .locals 3

    .prologue
    .line 44
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Ltv/peel/widget/service/k;->a:Ltv/peel/widget/service/QuickConnectService;

    invoke-static {v0}, Ltv/peel/widget/service/QuickConnectService;->a(Ltv/peel/widget/service/QuickConnectService;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "\n**** PeelControl.control.getCurrentRoom().getData().getId(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " -- "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/at;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    new-instance v0, Ltv/peel/widget/service/Room;

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/at;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ltv/peel/widget/service/Room;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    :goto_0
    return-object v0

    .line 48
    :cond_0
    iget-object v0, p0, Ltv/peel/widget/service/k;->a:Ltv/peel/widget/service/QuickConnectService;

    invoke-static {v0}, Ltv/peel/widget/service/QuickConnectService;->a(Ltv/peel/widget/service/QuickConnectService;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "\n**** current room is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()[Ltv/peel/widget/service/Room;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 82
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->c()[Lcom/peel/control/RoomControl;

    move-result-object v3

    .line 83
    array-length v1, v3

    new-array v4, v1, [Ltv/peel/widget/service/Room;

    .line 86
    array-length v5, v3

    move v1, v0

    :goto_0
    if-ge v0, v5, :cond_0

    aget-object v2, v3, v0

    .line 87
    new-instance v6, Ltv/peel/widget/service/Room;

    invoke-virtual {v2}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v7

    invoke-virtual {v7}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/at;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v6, v7, v2}, Ltv/peel/widget/service/Room;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    add-int/lit8 v2, v1, 0x1

    aput-object v6, v4, v1

    .line 86
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_0

    .line 91
    :cond_0
    return-object v4
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 101
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 102
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
