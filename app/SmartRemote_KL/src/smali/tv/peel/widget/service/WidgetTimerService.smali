.class public Ltv/peel/widget/service/WidgetTimerService;
.super Landroid/app/Service;


# instance fields
.field private final a:Ljava/util/Timer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 34
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Ltv/peel/widget/service/WidgetTimerService;->a:Ljava/util/Timer;

    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    const-wide/32 v4, 0x5265c00

    .line 168
    const-string/jumbo v0, "widget_pref"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "checked_time"

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 171
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v0

    cmp-long v6, v6, v4

    if-lez v6, :cond_0

    .line 177
    :goto_0
    new-instance v1, Ltv/peel/widget/service/q;

    invoke-direct {v1, p0, p1}, Ltv/peel/widget/service/q;-><init>(Ltv/peel/widget/service/WidgetTimerService;Landroid/content/Context;)V

    .line 222
    iget-object v0, p0, Ltv/peel/widget/service/WidgetTimerService;->a:Ljava/util/Timer;

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    .line 223
    return-void

    .line 174
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v0, v2, v0

    sub-long v2, v4, v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 81
    const-class v0, Lcom/peel/util/bx;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "get quickpanel feature"

    new-instance v2, Ltv/peel/widget/service/n;

    invoke-direct {v2, p3, p0, p1, p2}, Ltv/peel/widget/service/n;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 165
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Lcom/peel/util/t;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 53
    const-string/jumbo v0, "widget_pref"

    invoke-virtual {p0, v0, v5}, Ltv/peel/widget/service/WidgetTimerService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 54
    const-string/jumbo v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 55
    if-eqz v0, :cond_2

    .line 56
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkCountryIso()Ljava/lang/String;

    move-result-object v2

    .line 57
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v0

    .line 59
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 60
    const-string/jumbo v3, "&"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 61
    const-string/jumbo v3, "&"

    const-string/jumbo v4, ""

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 64
    :cond_0
    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ltv/peel/widget/service/m;

    invoke-direct {v3, p0, v1, p2}, Ltv/peel/widget/service/m;-><init>(Ltv/peel/widget/service/WidgetTimerService;Landroid/content/SharedPreferences;Lcom/peel/util/t;)V

    invoke-static {v2, v0, v3, p1}, Ltv/peel/widget/service/WidgetTimerService;->a(Ljava/lang/String;Ljava/lang/String;Lcom/peel/util/t;Landroid/content/Context;)V

    .line 77
    :goto_0
    return-void

    .line 72
    :cond_1
    invoke-virtual {p2, v5, v4, v4}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 75
    :cond_2
    invoke-virtual {p2, v5, v4, v4}, Lcom/peel/util/t;->a(ZLjava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p0}, Ltv/peel/widget/service/WidgetTimerService;->a(Landroid/content/Context;)V

    .line 49
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Ltv/peel/widget/service/WidgetTimerService;->a:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 228
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x1

    return v0
.end method
