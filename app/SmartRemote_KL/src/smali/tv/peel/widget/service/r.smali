.class Ltv/peel/widget/service/r;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Ltv/peel/widget/service/q;


# direct methods
.method constructor <init>(Ltv/peel/widget/service/q;Z)V
    .locals 0

    .prologue
    .line 189
    iput-object p1, p0, Ltv/peel/widget/service/r;->b:Ltv/peel/widget/service/q;

    iput-boolean p2, p0, Ltv/peel/widget/service/r;->a:Z

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 192
    iget-object v0, p0, Ltv/peel/widget/service/r;->b:Ltv/peel/widget/service/q;

    iget-object v0, v0, Ltv/peel/widget/service/q;->b:Ltv/peel/widget/service/WidgetTimerService;

    const-string/jumbo v1, "widget_pref"

    invoke-virtual {v0, v1, v4}, Ltv/peel/widget/service/WidgetTimerService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 193
    const-string/jumbo v1, "notification"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 195
    iget-boolean v2, p0, Ltv/peel/widget/service/r;->i:Z

    if-eqz v2, :cond_5

    .line 196
    iget-boolean v2, p0, Ltv/peel/widget/service/r;->a:Z

    if-nez v2, :cond_0

    .line 197
    const-string/jumbo v2, "showquickpanel"

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 198
    const-string/jumbo v3, "user_cleared"

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 199
    if-eqz v2, :cond_2

    if-nez v3, :cond_2

    if-nez v1, :cond_2

    .line 200
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "notification"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 201
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 202
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v2, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v2, :cond_1

    const-string/jumbo v0, "tv.peel.notification.EXPANDED"

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 203
    iget-object v0, p0, Ltv/peel/widget/service/r;->b:Ltv/peel/widget/service/q;

    iget-object v0, v0, Ltv/peel/widget/service/q;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 217
    :cond_0
    :goto_1
    return-void

    .line 202
    :cond_1
    const-string/jumbo v0, "tv.peel.samsung.notification.EXPANDED"

    goto :goto_0

    .line 204
    :cond_2
    if-eqz v1, :cond_0

    if-eqz v2, :cond_3

    if-eqz v3, :cond_0

    .line 205
    :cond_3
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "notification"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 206
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 207
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v2, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v2, :cond_4

    const-string/jumbo v0, "tv.peel.notification.COLLAPSED"

    :goto_2
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 208
    iget-object v0, p0, Ltv/peel/widget/service/r;->b:Ltv/peel/widget/service/q;

    iget-object v0, v0, Ltv/peel/widget/service/q;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_1

    .line 207
    :cond_4
    const-string/jumbo v0, "tv.peel.samsung.notification.COLLAPSED"

    goto :goto_2

    .line 211
    :cond_5
    if-eqz v1, :cond_0

    iget-boolean v1, p0, Ltv/peel/widget/service/r;->a:Z

    if-nez v1, :cond_0

    .line 212
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "notification"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 213
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 214
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v2, Lcom/peel/c/b;->c:Lcom/peel/c/b;

    if-ne v0, v2, :cond_6

    const-string/jumbo v0, "tv.peel.notification.COLLAPSED"

    :goto_3
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 215
    iget-object v0, p0, Ltv/peel/widget/service/r;->b:Ltv/peel/widget/service/q;

    iget-object v0, v0, Ltv/peel/widget/service/q;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_1

    .line 214
    :cond_6
    const-string/jumbo v0, "tv.peel.samsung.notification.COLLAPSED"

    goto :goto_3
.end method
