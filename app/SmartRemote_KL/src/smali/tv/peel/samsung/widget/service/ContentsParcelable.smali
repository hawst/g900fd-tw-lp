.class public Ltv/peel/samsung/widget/service/ContentsParcelable;
.super Ltv/peel/widget/service/ContentsParcelable;

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Ltv/peel/samsung/widget/service/ContentsParcelable;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field c:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    new-instance v0, Ltv/peel/samsung/widget/service/c;

    invoke-direct {v0}, Ltv/peel/samsung/widget/service/c;-><init>()V

    sput-object v0, Ltv/peel/samsung/widget/service/ContentsParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ltv/peel/widget/service/ContentsParcelable;-><init>()V

    .line 24
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/peel/samsung/widget/service/ContentsParcelable;->a:Ljava/lang/String;

    .line 25
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/peel/samsung/widget/service/ContentsParcelable;->b:Ljava/lang/String;

    .line 26
    iget-object v0, p0, Ltv/peel/samsung/widget/service/ContentsParcelable;->c:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringArray([Ljava/lang/String;)V

    .line 27
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ltv/peel/widget/service/ContentsParcelable;-><init>()V

    .line 30
    iput-object p1, p0, Ltv/peel/samsung/widget/service/ContentsParcelable;->a:Ljava/lang/String;

    .line 31
    iput-object p2, p0, Ltv/peel/samsung/widget/service/ContentsParcelable;->b:Ljava/lang/String;

    .line 32
    iput-object p3, p0, Ltv/peel/samsung/widget/service/ContentsParcelable;->c:[Ljava/lang/String;

    .line 33
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Ltv/peel/samsung/widget/service/ContentsParcelable;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 60
    iget-object v0, p0, Ltv/peel/samsung/widget/service/ContentsParcelable;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 61
    iget-object v0, p0, Ltv/peel/samsung/widget/service/ContentsParcelable;->c:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 62
    return-void
.end method
