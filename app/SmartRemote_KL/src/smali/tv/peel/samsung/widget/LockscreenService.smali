.class public Ltv/peel/samsung/widget/LockscreenService;
.super Ltv/peel/widget/WidgetService;


# static fields
.field private static final f:Ljava/lang/String;

.field private static g:Ltv/peel/samsung/widget/a;


# instance fields
.field private h:Z

.field private i:Ltv/peel/widget/util/c;

.field private j:Ljava/lang/String;

.field private k:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Ltv/peel/samsung/widget/LockscreenService;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ltv/peel/samsung/widget/LockscreenService;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 40
    invoke-direct {p0}, Ltv/peel/widget/WidgetService;-><init>()V

    .line 66
    iput-boolean v2, p0, Ltv/peel/samsung/widget/LockscreenService;->h:Z

    .line 67
    new-instance v0, Ltv/peel/samsung/widget/h;

    invoke-direct {v0, p0}, Ltv/peel/samsung/widget/h;-><init>(Ltv/peel/samsung/widget/LockscreenService;)V

    iput-object v0, p0, Ltv/peel/samsung/widget/LockscreenService;->i:Ltv/peel/widget/util/c;

    .line 398
    const-string/jumbo v0, "Play"

    iput-object v0, p0, Ltv/peel/samsung/widget/LockscreenService;->j:Ljava/lang/String;

    .line 399
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "PowerOn"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string/jumbo v2, "PowerOn"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "PowerOn"

    aput-object v2, v0, v1

    iput-object v0, p0, Ltv/peel/samsung/widget/LockscreenService;->k:[Ljava/lang/String;

    return-void
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Ltv/peel/samsung/widget/LockscreenService;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Ltv/peel/samsung/widget/a;)Ltv/peel/samsung/widget/a;
    .locals 0

    .prologue
    .line 40
    sput-object p0, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    return-object p0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 116
    invoke-static {p0}, Ltv/peel/widget/util/j;->a(Landroid/content/Context;)Ltv/peel/widget/util/j;

    move-result-object v1

    .line 117
    invoke-virtual {v1}, Ltv/peel/widget/util/j;->f()I

    move-result v0

    .line 118
    const v2, 0x7f0a0327

    if-ne p1, v2, :cond_1

    .line 119
    add-int/lit8 v0, v0, -0x1

    .line 124
    :cond_0
    :goto_0
    invoke-virtual {v1, v0}, Ltv/peel/widget/util/j;->b(I)V

    .line 125
    new-instance v0, Landroid/content/Intent;

    const-class v1, Ltv/peel/samsung/widget/LockscreenService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 126
    const-string/jumbo v1, "tv.peel.samsung.widget.lockscreen.action.UPDATE_WIDGETS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 127
    invoke-virtual {p0, v0}, Ltv/peel/samsung/widget/LockscreenService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 128
    return-void

    .line 120
    :cond_1
    const v2, 0x7f0a032c

    if-ne p1, v2, :cond_0

    .line 121
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private a(Landroid/content/Intent;)V
    .locals 11

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 260
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->b:Lcom/peel/c/b;

    if-eq v0, v1, :cond_1

    invoke-direct {p0}, Ltv/peel/samsung/widget/LockscreenService;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 261
    sget-object v0, Ltv/peel/samsung/widget/LockscreenService;->f:Ljava/lang/String;

    const-string/jumbo v1, "need to wait"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    :cond_0
    :goto_0
    return-void

    .line 265
    :cond_1
    invoke-static {p0}, Ltv/peel/widget/util/j;->a(Landroid/content/Context;)Ltv/peel/widget/util/j;

    move-result-object v6

    .line 266
    sget-object v0, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    invoke-virtual {v0}, Ltv/peel/samsung/widget/a;->k()Z

    move-result v1

    .line 267
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v5, Lcom/peel/c/b;->b:Lcom/peel/c/b;

    if-ne v0, v5, :cond_4

    const-string/jumbo v0, "widget_pref"

    .line 268
    invoke-virtual {p0, v0, v2}, Ltv/peel/samsung/widget/LockscreenService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v5, "is_setup_complete"

    invoke-interface {v0, v5, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 271
    :goto_1
    if-eqz v0, :cond_c

    .line 276
    :try_start_0
    sget-object v0, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    invoke-virtual {v0}, Ltv/peel/samsung/widget/a;->f()[Lcom/peel/control/a;

    move-result-object v1

    .line 277
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v5, Lcom/peel/c/b;->b:Lcom/peel/c/b;

    if-eq v0, v5, :cond_e

    .line 278
    if-nez v1, :cond_5

    move v0, v2

    :goto_2
    move v5, v0

    .line 281
    :goto_3
    invoke-virtual {v6, v1}, Ltv/peel/widget/util/j;->a([Lcom/peel/control/a;)V

    .line 283
    const-string/jumbo v0, "moving"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {v6}, Ltv/peel/widget/util/j;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 284
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v6}, Ltv/peel/widget/util/j;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/control/am;->e(Ljava/lang/String;)Lcom/peel/control/a;

    move-result-object v0

    .line 289
    :goto_4
    if-eqz v0, :cond_0

    .line 290
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v1

    .line 291
    if-eqz v1, :cond_0

    .line 294
    invoke-virtual {v6, v0}, Ltv/peel/widget/util/j;->a(Lcom/peel/control/a;)V

    .line 295
    sget-object v7, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    invoke-virtual {v7, v0}, Ltv/peel/samsung/widget/a;->a(Lcom/peel/control/a;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ltv/peel/widget/util/j;->a(Ljava/lang/String;)V

    .line 296
    invoke-virtual {v0}, Lcom/peel/control/a;->d()[Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ltv/peel/widget/util/j;->a([Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    const-string/jumbo v7, "tv.peel.samsung.widget.lockscreen.height"

    const/4 v8, 0x0

    invoke-virtual {p1, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    .line 298
    if-lez v7, :cond_2

    .line 299
    invoke-virtual {v6, v7}, Ltv/peel/widget/util/j;->a(I)V

    .line 301
    :cond_2
    invoke-virtual {v6, v1}, Ltv/peel/widget/util/j;->a(Lcom/peel/control/h;)V

    .line 302
    sget-object v1, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v1}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v1

    sget-object v7, Lcom/peel/c/b;->b:Lcom/peel/c/b;

    if-eq v1, v7, :cond_3

    .line 303
    sget-object v1, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    invoke-virtual {v1, v0}, Ltv/peel/samsung/widget/a;->a(Lcom/peel/control/a;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ltv/peel/samsung/widget/f;->a:Ljava/lang/String;

    .line 305
    :cond_3
    sget-object v0, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    invoke-virtual {v0}, Ltv/peel/samsung/widget/a;->n()[Ltv/peel/widget/service/DeviceParcelable;

    move-result-object v0

    invoke-virtual {v6, v0}, Ltv/peel/widget/util/j;->a([Ltv/peel/widget/service/DeviceParcelable;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 332
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->b:Lcom/peel/c/b;

    if-eq v0, v1, :cond_0

    .line 335
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 336
    new-instance v1, Landroid/content/ComponentName;

    const-class v7, Ltv/peel/samsung/widget/e;

    invoke-direct {v1, p0, v7}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v1

    .line 338
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 339
    array-length v8, v1

    move v0, v2

    :goto_5
    if-ge v0, v8, :cond_8

    aget v9, v1, v0

    .line 340
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 339
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 268
    :cond_4
    sget-object v0, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    .line 269
    invoke-virtual {v0}, Ltv/peel/samsung/widget/a;->j()Z

    move-result v0

    goto/16 :goto_1

    .line 278
    :cond_5
    :try_start_1
    array-length v0, v1

    goto/16 :goto_2

    .line 286
    :cond_6
    sget-object v0, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    invoke-virtual {v0}, Ltv/peel/samsung/widget/a;->g()Lcom/peel/control/a;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto/16 :goto_4

    .line 313
    :catch_0
    move-exception v0

    .line 314
    sget-object v1, Ltv/peel/samsung/widget/LockscreenService;->f:Ljava/lang/String;

    const-string/jumbo v3, "NPE, retrying..."

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 315
    iput-boolean v2, p0, Ltv/peel/samsung/widget/LockscreenService;->h:Z

    .line 317
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->b:Lcom/peel/c/b;

    if-eq v0, v1, :cond_0

    .line 318
    sget-object v0, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    if-eqz v0, :cond_7

    .line 319
    sget-object v0, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    invoke-virtual {v0}, Ltv/peel/samsung/widget/a;->h()V

    .line 320
    sput-object v4, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    .line 322
    :cond_7
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Ltv/peel/samsung/widget/l;

    invoke-direct {v1, p0}, Ltv/peel/samsung/widget/l;-><init>(Ltv/peel/samsung/widget/LockscreenService;)V

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 342
    :cond_8
    sget-object v0, Ltv/peel/samsung/widget/LockscreenService;->f:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "Updating widgets with appWidgetId(s): "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    invoke-virtual {v6}, Ltv/peel/widget/util/j;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ltv/peel/widget/util/j;->b(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    .line 345
    if-eqz v0, :cond_d

    .line 346
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_9
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 347
    const-string/jumbo v8, "live"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 353
    :goto_6
    invoke-virtual {v6}, Ltv/peel/widget/util/j;->e()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_b

    move-object v2, v4

    :goto_7
    if-eqz v3, :cond_a

    sget-object v0, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    invoke-virtual {v0}, Ltv/peel/samsung/widget/a;->l()Ljava/util/List;

    move-result-object v4

    :cond_a
    invoke-virtual {v6}, Ltv/peel/widget/util/j;->f()I

    move-result v6

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Ltv/peel/samsung/widget/f;->a(Landroid/content/Context;[I[Lcom/peel/control/h;ZLjava/util/List;II)V

    goto/16 :goto_0

    :cond_b
    sget-object v0, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    invoke-virtual {v0}, Ltv/peel/samsung/widget/a;->o()[Lcom/peel/control/h;

    move-result-object v2

    goto :goto_7

    .line 357
    :cond_c
    sget-object v0, Ltv/peel/samsung/widget/LockscreenService;->f:Ljava/lang/String;

    const-string/jumbo v2, "Controller is not setup yet"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    invoke-virtual {v6}, Ltv/peel/widget/util/j;->d()V

    .line 360
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v2, Lcom/peel/c/b;->b:Lcom/peel/c/b;

    if-eq v0, v2, :cond_0

    .line 361
    invoke-static {p0, v1}, Ltv/peel/samsung/widget/f;->a(Landroid/content/Context;Z)V

    goto/16 :goto_0

    :cond_d
    move v3, v2

    goto :goto_6

    :cond_e
    move v5, v2

    goto/16 :goto_3
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 415
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->b:Lcom/peel/c/b;

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Ltv/peel/samsung/widget/LockscreenService;->f()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 478
    :cond_1
    :goto_0
    return-void

    .line 418
    :cond_2
    sget-object v0, Ltv/peel/samsung/widget/LockscreenService;->f:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " button is pressed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    const-string/jumbo v0, "power"

    invoke-virtual {p0, v0}, Ltv/peel/samsung/widget/LockscreenService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 423
    const v1, 0x20000006

    const-string/jumbo v2, "Lockscreen"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    .line 425
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v2, Lcom/peel/c/b;->b:Lcom/peel/c/b;

    if-ne v0, v2, :cond_3

    .line 426
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 431
    :cond_3
    :try_start_0
    invoke-static {p0}, Lcom/peel/util/a/a;->a(Landroid/content/Context;)Lcom/peel/util/a/a;

    move-result-object v0

    const-string/jumbo v2, "lockpanelused"

    invoke-virtual {v0, v2}, Lcom/peel/util/a/a;->a(Ljava/lang/String;)V

    .line 432
    invoke-static {p0}, Ltv/peel/widget/util/j;->a(Landroid/content/Context;)Ltv/peel/widget/util/j;

    move-result-object v0

    .line 433
    const-string/jumbo v2, "Volume_Down"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string/jumbo v2, "Volume_Up"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string/jumbo v2, "Mute"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 434
    :cond_4
    sget-object v0, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    invoke-virtual {v0}, Ltv/peel/samsung/widget/a;->n()[Ltv/peel/widget/service/DeviceParcelable;

    move-result-object v0

    .line 435
    if-eqz v0, :cond_5

    array-length v2, v0

    if-lez v2, :cond_5

    .line 436
    sget-object v2, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    const/4 v3, 0x0

    aget-object v0, v0, v3

    invoke-virtual {v0}, Ltv/peel/widget/service/DeviceParcelable;->a()Ljava/lang/String;

    move-result-object v0

    const/16 v3, 0x1b58

    invoke-virtual {v2, p1, v0, v3}, Ltv/peel/samsung/widget/a;->a(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 470
    :cond_5
    :goto_1
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v2, Lcom/peel/c/b;->b:Lcom/peel/c/b;

    if-ne v0, v2, :cond_1

    .line 471
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v2, Ltv/peel/samsung/widget/m;

    invoke-direct {v2, p0, v1}, Ltv/peel/samsung/widget/m;-><init>(Ltv/peel/samsung/widget/LockscreenService;Landroid/os/PowerManager$WakeLock;)V

    const-wide/16 v4, 0x2710

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 438
    :cond_6
    :try_start_1
    const-string/jumbo v2, "Play|Pause"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 439
    invoke-virtual {v0}, Ltv/peel/widget/util/j;->a()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "Apple"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 440
    sget-object v2, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    const-string/jumbo v3, "Select"

    invoke-virtual {v0}, Ltv/peel/widget/util/j;->b()Ljava/lang/String;

    move-result-object v0

    const/16 v4, 0x1b58

    invoke-virtual {v2, v3, v0, v4}, Ltv/peel/samsung/widget/a;->a(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 466
    :catch_0
    move-exception v0

    .line 467
    sget-object v2, Ltv/peel/samsung/widget/LockscreenService;->f:Ljava/lang/String;

    const-string/jumbo v3, "NPE while trying to send IR command"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 442
    :cond_7
    :try_start_2
    sget-object v2, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    iget-object v3, p0, Ltv/peel/samsung/widget/LockscreenService;->j:Ljava/lang/String;

    invoke-virtual {v0}, Ltv/peel/widget/util/j;->b()Ljava/lang/String;

    move-result-object v0

    const/16 v4, 0x1b58

    invoke-virtual {v2, v3, v0, v4}, Ltv/peel/samsung/widget/a;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 443
    const-string/jumbo v0, "Play"

    iget-object v2, p0, Ltv/peel/samsung/widget/LockscreenService;->j:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string/jumbo v0, "Pause"

    :goto_2
    iput-object v0, p0, Ltv/peel/samsung/widget/LockscreenService;->j:Ljava/lang/String;

    goto :goto_1

    :cond_8
    const-string/jumbo v0, "Play"

    goto :goto_2

    .line 445
    :cond_9
    const-string/jumbo v2, "channel"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 446
    sget-object v2, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    invoke-virtual {v0}, Ltv/peel/widget/util/j;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0, p2}, Ltv/peel/samsung/widget/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 447
    :cond_a
    const-string/jumbo v2, "Power"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 448
    sget-object v0, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    const-string/jumbo v2, "Power"

    const/16 v3, 0x1b58

    invoke-virtual {v0, v2, p2, v3}, Ltv/peel/samsung/widget/a;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 449
    :cond_b
    const-string/jumbo v2, "PowerOn|PowerOff"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 450
    iget-object v0, p0, Ltv/peel/samsung/widget/LockscreenService;->k:[Ljava/lang/String;

    aget-object v0, v0, p3

    .line 451
    sget-object v2, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    const/16 v3, 0x1b58

    invoke-virtual {v2, v0, p2, v3}, Ltv/peel/samsung/widget/a;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 452
    iget-object v2, p0, Ltv/peel/samsung/widget/LockscreenService;->k:[Ljava/lang/String;

    const-string/jumbo v3, "PowerOn"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    const-string/jumbo v0, "PowerOff"

    :goto_3
    aput-object v0, v2, p3

    goto/16 :goto_1

    :cond_c
    const-string/jumbo v0, "PowerOn"

    goto :goto_3

    .line 454
    :cond_d
    sget-object v2, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    invoke-virtual {v0}, Ltv/peel/widget/util/j;->b()Ljava/lang/String;

    move-result-object v0

    const/16 v3, 0x1b58

    invoke-virtual {v2, p1, v0, v3}, Ltv/peel/samsung/widget/a;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 455
    const-string/jumbo v0, "MODE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_e

    const-string/jumbo v0, "Down"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_e

    const-string/jumbo v0, "UP"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 456
    :cond_e
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v2, Lcom/peel/c/b;->b:Lcom/peel/c/b;

    if-ne v0, v2, :cond_f

    .line 457
    invoke-virtual {p0}, Ltv/peel/samsung/widget/LockscreenService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Ltv/peel/samsung/widget/n;->a(Landroid/content/Context;)Landroid/widget/RemoteViews;

    .line 458
    invoke-virtual {p0}, Ltv/peel/samsung/widget/LockscreenService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/peel/util/bb;->d(Landroid/content/Context;)V

    goto/16 :goto_1

    .line 460
    :cond_f
    new-instance v0, Landroid/content/Intent;

    const-class v2, Ltv/peel/samsung/widget/LockscreenService;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 461
    const-string/jumbo v2, "tv.peel.samsung.widget.lockscreen.action.UPDATE_WIDGETS"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 462
    invoke-virtual {p0, v0}, Ltv/peel/samsung/widget/LockscreenService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_1
.end method

.method static synthetic a(Ltv/peel/samsung/widget/LockscreenService;)Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Ltv/peel/samsung/widget/LockscreenService;->h:Z

    return v0
.end method

.method static synthetic a(Ltv/peel/samsung/widget/LockscreenService;Z)Z
    .locals 0

    .prologue
    .line 40
    iput-boolean p1, p0, Ltv/peel/samsung/widget/LockscreenService;->h:Z

    return p1
.end method

.method static synthetic b(Ltv/peel/samsung/widget/LockscreenService;)Ltv/peel/widget/util/c;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Ltv/peel/samsung/widget/LockscreenService;->i:Ltv/peel/widget/util/c;

    return-object v0
.end method

.method private b(I)V
    .locals 12

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 132
    const-string/jumbo v0, "power"

    invoke-virtual {p0, v0}, Ltv/peel/samsung/widget/LockscreenService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 136
    const v4, 0x20000006

    const-string/jumbo v5, "Lockscreen"

    invoke-virtual {v0, v4, v5}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v5

    .line 138
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v4, Lcom/peel/c/b;->b:Lcom/peel/c/b;

    if-ne v0, v4, :cond_0

    .line 139
    invoke-virtual {v5}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 143
    :cond_0
    :try_start_0
    invoke-static {p0}, Lcom/peel/util/a/a;->a(Landroid/content/Context;)Lcom/peel/util/a/a;

    move-result-object v0

    const-string/jumbo v4, "lockpanelused"

    invoke-virtual {v0, v4}, Lcom/peel/util/a/a;->a(Ljava/lang/String;)V

    .line 144
    sget-object v0, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    invoke-virtual {v0}, Ltv/peel/samsung/widget/a;->f()[Lcom/peel/control/a;

    move-result-object v6

    .line 145
    invoke-static {p0}, Ltv/peel/widget/util/j;->a(Landroid/content/Context;)Ltv/peel/widget/util/j;

    move-result-object v7

    .line 146
    sget-object v0, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    invoke-virtual {v0}, Ltv/peel/samsung/widget/a;->g()Lcom/peel/control/a;

    move-result-object v8

    .line 147
    const v0, 0x7f0a0322

    if-ne p1, v0, :cond_8

    .line 151
    array-length v9, v6

    move v4, v2

    move v0, v2

    :goto_0
    if-ge v4, v9, :cond_e

    aget-object v2, v6, v4

    .line 152
    invoke-virtual {v8}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    move v0, v1

    .line 151
    :cond_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_0

    .line 156
    :cond_2
    if-eqz v0, :cond_1

    move-object v0, v2

    .line 161
    :goto_1
    if-nez v0, :cond_3

    .line 163
    const/4 v0, 0x0

    aget-object v0, v6, v0

    .line 165
    :cond_3
    if-eqz v0, :cond_5

    .line 166
    sget-object v1, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v1}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v1

    sget-object v2, Lcom/peel/c/b;->b:Lcom/peel/c/b;

    if-ne v1, v2, :cond_7

    .line 168
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "started activity"

    new-instance v3, Ltv/peel/samsung/widget/i;

    invoke-direct {v3, p0, v0}, Ltv/peel/samsung/widget/i;-><init>(Ltv/peel/samsung/widget/LockscreenService;Lcom/peel/control/a;)V

    const-wide/16 v8, 0x1f4

    invoke-static {v1, v2, v3, v8, v9}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)Ljava/lang/Runnable;

    .line 181
    :goto_2
    const/4 v1, 0x0

    invoke-virtual {v7, v1}, Ltv/peel/widget/util/j;->a(Z)V

    .line 182
    invoke-virtual {v7, v0}, Ltv/peel/widget/util/j;->a(Lcom/peel/control/a;)V

    .line 183
    sget-object v1, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    invoke-virtual {v1, v0}, Ltv/peel/samsung/widget/a;->a(Lcom/peel/control/a;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ltv/peel/widget/util/j;->a(Ljava/lang/String;)V

    .line 184
    invoke-virtual {v0}, Lcom/peel/control/a;->d()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v1, v2}, Ltv/peel/widget/util/j;->a([Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v0

    .line 187
    if-eqz v0, :cond_4

    .line 188
    invoke-virtual {v7, v0}, Ltv/peel/widget/util/j;->a(Lcom/peel/control/h;)V

    .line 190
    :cond_4
    new-instance v0, Landroid/content/Intent;

    const-class v1, Ltv/peel/samsung/widget/LockscreenService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 191
    const-string/jumbo v1, "moving"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 192
    const-string/jumbo v1, "tv.peel.samsung.widget.lockscreen.action.UPDATE_WIDGETS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 193
    invoke-virtual {p0, v0}, Ltv/peel/samsung/widget/LockscreenService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 246
    :cond_5
    :goto_3
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->b:Lcom/peel/c/b;

    if-ne v0, v1, :cond_6

    .line 247
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Ltv/peel/samsung/widget/k;

    invoke-direct {v1, p0, v5}, Ltv/peel/samsung/widget/k;-><init>(Ltv/peel/samsung/widget/LockscreenService;Landroid/os/PowerManager$WakeLock;)V

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 254
    :cond_6
    return-void

    .line 179
    :cond_7
    :try_start_1
    sget-object v1, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    invoke-virtual {v1, v0}, Ltv/peel/samsung/widget/a;->b(Lcom/peel/control/a;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 241
    :catch_0
    move-exception v0

    .line 242
    sget-object v1, Ltv/peel/samsung/widget/LockscreenService;->f:Ljava/lang/String;

    const-string/jumbo v2, "NPE, stop changing device"

    invoke-static {v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_3

    .line 195
    :cond_8
    const v0, 0x7f0a0323

    if-ne p1, v0, :cond_5

    .line 197
    :try_start_2
    array-length v4, v6

    move v1, v2

    move-object v0, v3

    :goto_4
    if-ge v1, v4, :cond_9

    aget-object v2, v6, v1

    .line 198
    invoke-virtual {v8}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 203
    :cond_9
    if-nez v0, :cond_a

    .line 206
    array-length v0, v6

    add-int/lit8 v0, v0, -0x1

    aget-object v0, v6, v0

    .line 208
    :cond_a
    if-eqz v0, :cond_5

    .line 210
    sget-object v1, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v1}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v1

    sget-object v2, Lcom/peel/c/b;->b:Lcom/peel/c/b;

    if-ne v1, v2, :cond_d

    .line 211
    const-class v1, Ltv/peel/samsung/widget/LockscreenService;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "started activity"

    new-instance v3, Ltv/peel/samsung/widget/j;

    invoke-direct {v3, p0, v0}, Ltv/peel/samsung/widget/j;-><init>(Ltv/peel/samsung/widget/LockscreenService;Lcom/peel/control/a;)V

    const-wide/16 v8, 0x1f4

    invoke-static {v1, v2, v3, v8, v9}, Lcom/peel/util/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;J)Ljava/lang/Runnable;

    .line 224
    :goto_5
    invoke-virtual {v7, v0}, Ltv/peel/widget/util/j;->a(Lcom/peel/control/a;)V

    .line 225
    sget-object v1, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    invoke-virtual {v1, v0}, Ltv/peel/samsung/widget/a;->a(Lcom/peel/control/a;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ltv/peel/widget/util/j;->a(Ljava/lang/String;)V

    .line 226
    invoke-virtual {v0}, Lcom/peel/control/a;->d()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v1, v2}, Ltv/peel/widget/util/j;->a([Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v0

    .line 230
    if-eqz v0, :cond_b

    .line 231
    invoke-virtual {v7, v0}, Ltv/peel/widget/util/j;->a(Lcom/peel/control/h;)V

    .line 232
    :cond_b
    const/4 v0, 0x0

    invoke-virtual {v7, v0}, Ltv/peel/widget/util/j;->a(Z)V

    .line 234
    new-instance v0, Landroid/content/Intent;

    const-class v1, Ltv/peel/samsung/widget/LockscreenService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 235
    const-string/jumbo v1, "moving"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 236
    const-string/jumbo v1, "tv.peel.samsung.widget.lockscreen.action.UPDATE_WIDGETS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 237
    invoke-virtual {p0, v0}, Ltv/peel/samsung/widget/LockscreenService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_3

    .line 197
    :cond_c
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move-object v0, v2

    goto :goto_4

    .line 222
    :cond_d
    sget-object v1, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    invoke-virtual {v1, v0}, Ltv/peel/samsung/widget/a;->b(Lcom/peel/control/a;)V
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_5

    :cond_e
    move-object v0, v3

    goto/16 :goto_1
.end method

.method private f()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 367
    sget-object v1, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    if-eqz v1, :cond_0

    sget-object v1, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    invoke-virtual {v1}, Ltv/peel/samsung/widget/a;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 378
    :goto_0
    return v0

    .line 371
    :cond_0
    iput-boolean v0, p0, Ltv/peel/samsung/widget/LockscreenService;->h:Z

    .line 373
    sget-object v0, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    if-nez v0, :cond_1

    .line 374
    sget-object v0, Ltv/peel/samsung/widget/LockscreenService;->f:Ljava/lang/String;

    const-string/jumbo v1, "The helper is null!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    new-instance v0, Ltv/peel/samsung/widget/a;

    iget-object v1, p0, Ltv/peel/samsung/widget/LockscreenService;->i:Ltv/peel/widget/util/c;

    invoke-direct {v0, p0, v1}, Ltv/peel/samsung/widget/a;-><init>(Landroid/content/Context;Ltv/peel/widget/util/c;)V

    sput-object v0, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    .line 378
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 395
    const/4 v0, 0x0

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 383
    invoke-super {p0}, Ltv/peel/widget/WidgetService;->onDestroy()V

    .line 384
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->b:Lcom/peel/c/b;

    if-eq v0, v1, :cond_1

    .line 385
    sget-object v0, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    if-eqz v0, :cond_0

    .line 386
    sget-object v0, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    invoke-virtual {v0}, Ltv/peel/samsung/widget/a;->h()V

    .line 387
    const/4 v0, 0x0

    sput-object v0, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    .line 389
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/peel/samsung/widget/LockscreenService;->h:Z

    .line 391
    :cond_1
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 90
    if-eqz p1, :cond_1

    .line 91
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 92
    sget-object v1, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v1}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v1

    sget-object v2, Lcom/peel/c/b;->b:Lcom/peel/c/b;

    if-ne v1, v2, :cond_0

    sget-object v1, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    if-nez v1, :cond_0

    .line 93
    new-instance v1, Ltv/peel/samsung/widget/a;

    invoke-direct {v1, p0}, Ltv/peel/samsung/widget/a;-><init>(Landroid/content/Context;)V

    sput-object v1, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    .line 95
    :cond_0
    const-string/jumbo v1, "tv.peel.samsung.widget.lockscreen.action.UPDATE_WIDGETS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 96
    sget-object v0, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    if-nez v0, :cond_2

    .line 97
    iput-boolean v4, p0, Ltv/peel/samsung/widget/LockscreenService;->h:Z

    .line 98
    new-instance v0, Ltv/peel/samsung/widget/a;

    iget-object v1, p0, Ltv/peel/samsung/widget/LockscreenService;->i:Ltv/peel/widget/util/c;

    invoke-direct {v0, p0, v1}, Ltv/peel/samsung/widget/a;-><init>(Landroid/content/Context;Ltv/peel/widget/util/c;)V

    sput-object v0, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    .line 99
    sget-object v0, Ltv/peel/samsung/widget/LockscreenService;->g:Ltv/peel/samsung/widget/a;

    sput-object v0, Ltv/peel/samsung/widget/f;->b:Ltv/peel/samsung/widget/a;

    .line 112
    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 101
    :cond_2
    invoke-direct {p0, p1}, Ltv/peel/samsung/widget/LockscreenService;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 103
    :cond_3
    const-string/jumbo v1, "tv.peel.samsung.widget.lockscreen.action.BUTTON_PRESSED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 104
    const-string/jumbo v0, "tv.peel.samsung.widget.lockscreen.extra.COMMAND"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "tv.peel.samsung.widget.lockscreen.field"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "tv.peel.samsung.widget.lockscreen.index"

    invoke-virtual {p1, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Ltv/peel/samsung/widget/LockscreenService;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 105
    :cond_4
    const-string/jumbo v1, "tv.peel.samsung.widget.lockscreen.action.CHANGE_DEVICE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 106
    const-string/jumbo v0, "tv.peel.samsung.widget.lockscreen.extra.COMMAND"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-direct {p0, v0}, Ltv/peel/samsung/widget/LockscreenService;->b(I)V

    goto :goto_0

    .line 107
    :cond_5
    const-string/jumbo v1, "tv.peel.samsung.widget.lockscreen.action.FAV_IDX_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 108
    const-string/jumbo v0, "tv.peel.samsung.widget.lockscreen.extra.COMMAND"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-direct {p0, v0}, Ltv/peel/samsung/widget/LockscreenService;->a(I)V

    goto :goto_0
.end method
