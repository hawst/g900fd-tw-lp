.class public Ltv/peel/samsung/widget/f;
.super Ljava/lang/Object;


# static fields
.field public static a:Ljava/lang/String;

.field public static b:Ltv/peel/samsung/widget/a;

.field private static final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Ltv/peel/samsung/widget/f;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ltv/peel/samsung/widget/f;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;ILjava/lang/String;Landroid/widget/RemoteViews;ZI)V
    .locals 3

    .prologue
    .line 166
    new-instance v0, Landroid/content/Intent;

    const-class v1, Ltv/peel/samsung/widget/LockscreenService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 167
    const-string/jumbo v1, "tv.peel.samsung.widget.lockscreen.action.BUTTON_PRESSED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 169
    if-eqz p4, :cond_0

    .line 170
    const-string/jumbo v1, "tv.peel.samsung.widget.lockscreen.index"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 171
    const-string/jumbo v1, "tv.peel.samsung.widget.lockscreen.extra.COMMAND"

    const-string/jumbo v2, "PowerOn|PowerOff"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 175
    :goto_0
    const-string/jumbo v1, "tv.peel.samsung.widget.lockscreen.field"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 176
    const/high16 v1, 0x8000000

    invoke-static {p0, p1, v0, v1}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p3, p1, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 177
    return-void

    .line 173
    :cond_0
    const-string/jumbo v1, "tv.peel.samsung.widget.lockscreen.extra.COMMAND"

    const-string/jumbo v2, "Power"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Landroid/widget/RemoteViews;)V
    .locals 8

    .prologue
    const/high16 v7, 0x8000000

    const/high16 v6, 0x200000

    const/4 v5, 0x3

    const v4, 0x7f0a0323

    const v3, 0x7f0a0322

    .line 329
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 330
    const-string/jumbo v1, "context_id"

    const/16 v2, 0x1b58

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 331
    const/high16 v1, 0x30000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 332
    const-string/jumbo v1, "from"

    const-string/jumbo v2, "LOCKSCREEN"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 334
    const v1, 0x7f0a018e

    invoke-static {p0, v5, v0, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 335
    const v1, 0x7f0a0324

    invoke-static {p0, v5, v0, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 337
    new-instance v0, Landroid/content/Intent;

    const-class v1, Ltv/peel/samsung/widget/LockscreenService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 338
    const-string/jumbo v1, "tv.peel.samsung.widget.lockscreen.action.CHANGE_DEVICE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 341
    const-string/jumbo v1, "tv.peel.samsung.widget.lockscreen.extra.COMMAND"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 342
    invoke-static {p0, v4, v0, v7}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {p1, v4, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 344
    const-string/jumbo v1, "tv.peel.samsung.widget.lockscreen.extra.COMMAND"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 345
    invoke-static {p0, v3, v0, v7}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 346
    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 180
    new-instance v0, Landroid/content/Intent;

    const-class v1, Ltv/peel/samsung/widget/LockscreenService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 181
    const-string/jumbo v1, "tv.peel.samsung.widget.lockscreen.action.BUTTON_PRESSED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 182
    const-string/jumbo v1, "tv.peel.samsung.widget.lockscreen.extra.COMMAND"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 183
    const/high16 v1, 0x8000000

    invoke-static {p0, p2, v0, v1}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 184
    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/widget/RemoteViews;IZ)V
    .locals 10

    .prologue
    const v9, 0x7f0a018d

    const v8, 0x7f0a0189

    const v7, 0x7f0a0188

    const v6, 0x7f020448

    const/4 v3, 0x0

    .line 188
    const v1, 0x7f0a01b7

    if-eqz p3, :cond_3

    const-string/jumbo v0, "Select"

    :goto_0
    invoke-static {p0, p1, v1, v0}, Ltv/peel/samsung/widget/f;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 189
    const v0, 0x7f0a01b8

    const-string/jumbo v1, "Mute"

    invoke-static {p0, p1, v0, v1}, Ltv/peel/samsung/widget/f;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 191
    const v0, 0x7f0a018e

    const-string/jumbo v1, "setBackgroundResource"

    const v2, 0x7f020475

    invoke-virtual {p1, v0, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 192
    const v0, 0x7f0a01b4

    const v1, 0x7f0d0262

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 193
    const v0, 0x7f0a01b5

    const v1, 0x7f0d0261

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 196
    const v0, 0x7f0a01b1

    const-string/jumbo v1, "setBackgroundResource"

    const v2, 0x7f020479

    invoke-virtual {p1, v0, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 197
    const v0, 0x7f0a01b2

    const-string/jumbo v1, "setBackgroundResource"

    const v2, 0x7f020479

    invoke-virtual {p1, v0, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 198
    const v0, 0x7f0a01b3

    const-string/jumbo v1, "setBackgroundResource"

    const v2, 0x7f020479

    invoke-virtual {p1, v0, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 199
    const v1, 0x7f0a01b1

    const v2, 0x7f020460

    move-object v0, p1

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setTextViewCompoundDrawables(IIIII)V

    .line 200
    const v1, 0x7f0a01b2

    const v2, 0x7f020460

    move-object v0, p1

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setTextViewCompoundDrawables(IIIII)V

    .line 201
    const v1, 0x7f0a01b3

    const v2, 0x7f020460

    move-object v0, p1

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setTextViewCompoundDrawables(IIIII)V

    .line 203
    const v0, 0x7f02047d

    invoke-virtual {p1, v7, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 204
    const v0, 0x7f02047c

    invoke-virtual {p1, v8, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 205
    const v0, 0x7f0a018a

    const v1, 0x7f020472

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 206
    const v0, 0x7f0a018b

    const v1, 0x7f020471

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 208
    const v0, 0x7f0a01b7

    const-string/jumbo v1, "setBackgroundResource"

    invoke-virtual {p1, v0, v1, v6}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 209
    const v0, 0x7f0a01b7

    const v1, 0x7f020478

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 210
    const v0, 0x7f0a01b8

    const-string/jumbo v1, "setBackgroundResource"

    invoke-virtual {p1, v0, v1, v6}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 211
    const v0, 0x7f0a01b8

    const v1, 0x7f020477

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 214
    const v0, 0x7f03008f

    if-ne p2, v0, :cond_0

    .line 215
    const-string/jumbo v0, "Volume_Up"

    invoke-static {p0, p1, v7, v0}, Ltv/peel/samsung/widget/f;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 216
    const-string/jumbo v0, "Volume_Down"

    invoke-static {p0, p1, v8, v0}, Ltv/peel/samsung/widget/f;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 217
    const v0, 0x7f0a018a

    const-string/jumbo v1, "Channel_Up"

    invoke-static {p0, p1, v0, v1}, Ltv/peel/samsung/widget/f;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 218
    const v0, 0x7f0a018b

    const-string/jumbo v1, "Channel_Down"

    invoke-static {p0, p1, v0, v1}, Ltv/peel/samsung/widget/f;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 219
    const v0, 0x7f0a0182

    const-string/jumbo v1, "Fast_Forward"

    invoke-static {p0, p1, v0, v1}, Ltv/peel/samsung/widget/f;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 220
    const v0, 0x7f0a0183

    const-string/jumbo v1, "Channel_Up"

    invoke-static {p0, p1, v0, v1}, Ltv/peel/samsung/widget/f;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 221
    const v0, 0x7f0a0184

    const-string/jumbo v1, "Channel_Down"

    invoke-static {p0, p1, v0, v1}, Ltv/peel/samsung/widget/f;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 223
    const v0, 0x7f0a0182

    const-string/jumbo v1, "setBackgroundResource"

    invoke-virtual {p1, v0, v1, v6}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 224
    const v0, 0x7f0a0182

    const v1, 0x7f020474

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 225
    const v0, 0x7f0a0183

    const-string/jumbo v1, "setBackgroundResource"

    invoke-virtual {p1, v0, v1, v6}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 226
    const v0, 0x7f0a0183

    const v1, 0x7f020470

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 227
    const v0, 0x7f0a0184

    const-string/jumbo v1, "setBackgroundResource"

    invoke-virtual {p1, v0, v1, v6}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 228
    const v0, 0x7f0a0184

    const v1, 0x7f02046f

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 231
    :cond_0
    const v0, 0x7f030090

    if-ne p2, v0, :cond_1

    .line 232
    const-string/jumbo v0, "Volume_Up"

    invoke-static {p0, p1, v7, v0}, Ltv/peel/samsung/widget/f;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 233
    const-string/jumbo v0, "Volume_Down"

    invoke-static {p0, p1, v8, v0}, Ltv/peel/samsung/widget/f;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 234
    const v0, 0x7f0a0185

    const-string/jumbo v1, "Rewind"

    invoke-static {p0, p1, v0, v1}, Ltv/peel/samsung/widget/f;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 235
    const v0, 0x7f0a0186

    const-string/jumbo v1, "Fast_Forward"

    invoke-static {p0, p1, v0, v1}, Ltv/peel/samsung/widget/f;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 236
    const v1, 0x7f0a0187

    if-eqz p3, :cond_4

    const-string/jumbo v0, "Select"

    :goto_1
    invoke-static {p0, p1, v1, v0}, Ltv/peel/samsung/widget/f;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 238
    const v0, 0x7f0a0185

    const-string/jumbo v1, "setBackgroundResource"

    invoke-virtual {p1, v0, v1, v6}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 239
    const v0, 0x7f0a0185

    const v1, 0x7f02047b

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 240
    const v0, 0x7f0a0186

    const-string/jumbo v1, "setBackgroundResource"

    invoke-virtual {p1, v0, v1, v6}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 241
    const v0, 0x7f0a0186

    const v1, 0x7f020474

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 242
    const v0, 0x7f0a0187

    const-string/jumbo v1, "setBackgroundResource"

    invoke-virtual {p1, v0, v1, v6}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 243
    const v0, 0x7f0a0187

    const v1, 0x7f020478

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 246
    :cond_1
    const v0, 0x7f030091

    if-ne p2, v0, :cond_2

    .line 247
    const v0, 0x7f0a01b4

    const v1, 0x7f0d0265

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 248
    const v0, 0x7f0a01b5

    const v1, 0x7f0d0263

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 250
    const v2, 0x7f02005b

    move-object v0, p1

    move v1, v9

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setTextViewCompoundDrawables(IIIII)V

    .line 251
    const-string/jumbo v0, "setBackgroundResource"

    invoke-virtual {p1, v9, v0, v6}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 252
    const-string/jumbo v0, "UP"

    invoke-static {p0, p1, v7, v0}, Ltv/peel/samsung/widget/f;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 253
    const-string/jumbo v0, "Down"

    invoke-static {p0, p1, v8, v0}, Ltv/peel/samsung/widget/f;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 254
    const v0, 0x7f0a018a

    const-string/jumbo v1, "FAN_HIGH"

    invoke-static {p0, p1, v0, v1}, Ltv/peel/samsung/widget/f;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 255
    const v0, 0x7f0a018b

    const-string/jumbo v1, "FAN_LOW"

    invoke-static {p0, p1, v0, v1}, Ltv/peel/samsung/widget/f;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 256
    const-string/jumbo v0, "MODE"

    invoke-static {p0, p1, v9, v0}, Ltv/peel/samsung/widget/f;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 258
    const-string/jumbo v0, "setBackgroundResource"

    invoke-virtual {p1, v9, v0, v6}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 260
    :cond_2
    return-void

    .line 188
    :cond_3
    const-string/jumbo v0, "Play|Pause"

    goto/16 :goto_0

    .line 236
    :cond_4
    const-string/jumbo v0, "Play|Pause"

    goto/16 :goto_1
.end method

.method public static a(Landroid/content/Context;Z)V
    .locals 8

    .prologue
    const/high16 v7, 0x30000000

    const/high16 v6, 0x200000

    const/16 v5, 0x1b58

    .line 289
    new-instance v1, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f03012b

    invoke-direct {v1, v0, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 290
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    .line 293
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v3, "is_setup_complete"

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_2

    .line 294
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 295
    const-string/jumbo v3, "context_id"

    invoke-virtual {v0, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 296
    invoke-virtual {v0, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 297
    const-string/jumbo v3, "from"

    const-string/jumbo v4, "LOCKSCREEN"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 298
    const/4 v3, 0x3

    invoke-static {p0, v3, v0, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 309
    :goto_0
    const v3, 0x7f0a004c

    invoke-virtual {v1, v3, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 310
    const v3, 0x7f0a0331

    invoke-virtual {v1, v3, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 311
    const v3, 0x7f0a0332

    invoke-virtual {v1, v3, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 312
    const v3, 0x7f0a0333

    invoke-virtual {v1, v3, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 314
    const-string/jumbo v0, "connectivity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 315
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 316
    if-eqz v0, :cond_1

    .line 317
    new-instance v0, Ltv/peel/widget/util/d;

    invoke-direct {v0, p0}, Ltv/peel/widget/util/d;-><init>(Landroid/content/Context;)V

    new-instance v3, Ltv/peel/samsung/widget/g;

    const/4 v4, 0x1

    invoke-direct {v3, v4, v2, p0, v1}, Ltv/peel/samsung/widget/g;-><init>(ILandroid/appwidget/AppWidgetManager;Landroid/content/Context;Landroid/widget/RemoteViews;)V

    invoke-virtual {v0, v3, v1}, Ltv/peel/widget/util/d;->a(Lcom/peel/util/t;Landroid/widget/RemoteViews;)V

    .line 324
    :cond_1
    new-instance v0, Landroid/content/ComponentName;

    const-class v3, Ltv/peel/samsung/widget/e;

    invoke-direct {v0, p0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v0, v1}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(Landroid/content/ComponentName;Landroid/widget/RemoteViews;)V

    .line 325
    return-void

    .line 300
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v3, "android.intent.action.MAIN"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 301
    const-string/jumbo v3, "tv.peel.samsung.app"

    const-string/jumbo v4, "com.peel.main.Home"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 302
    const-string/jumbo v3, "context_id"

    invoke-virtual {v0, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 303
    invoke-virtual {v0, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 304
    const-string/jumbo v3, "from"

    const-string/jumbo v4, "LOCKSCREEN"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 305
    const-string/jumbo v3, "peel://remote"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 306
    const/4 v3, 0x4

    invoke-static {p0, v3, v0, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;[I[Lcom/peel/control/h;ZLjava/util/List;II)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "[I[",
            "Lcom/peel/control/h;",
            "Z",
            "Ljava/util/List",
            "<",
            "Ltv/peel/widget/service/ChannelParcelable;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 58
    .line 59
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v2, "is_setup_complete"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 60
    move/from16 v0, p3

    invoke-static {p0, v0}, Ltv/peel/samsung/widget/f;->b(Landroid/content/Context;Z)I

    move-result v9

    .line 62
    new-instance v4, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1, v9}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 63
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v10

    .line 64
    const/4 v7, 0x0

    .line 66
    move-object/from16 v0, p1

    array-length v11, v0

    const/4 v1, 0x0

    move v8, v1

    :goto_0
    if-ge v8, v11, :cond_c

    aget v12, p1, v8

    .line 67
    const-string/jumbo v1, "layout_inflater"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 68
    const/4 v2, 0x0

    invoke-virtual {v1, v9, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 69
    new-instance v1, Landroid/content/Intent;

    const-class v2, Ltv/peel/samsung/widget/LockscreenService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 70
    const-string/jumbo v2, "tv.peel.samsung.widget.lockscreen.action.BUTTON_PRESSED"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 71
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 73
    const/4 v2, 0x0

    .line 74
    move-object/from16 v0, p2

    array-length v5, v0

    const/4 v1, 0x0

    move v3, v1

    move v1, v2

    :goto_1
    if-ge v3, v5, :cond_5

    aget-object v2, p2, v3

    .line 75
    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v6

    invoke-virtual {v6}, Lcom/peel/data/g;->d()I

    move-result v6

    .line 77
    const/4 v14, 0x6

    if-ne v6, v14, :cond_0

    .line 78
    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v2

    .line 79
    if-eqz v2, :cond_2

    const-string/jumbo v6, "Apple"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 80
    const/4 v2, 0x1

    .line 74
    :goto_2
    add-int/lit8 v3, v3, 0x1

    move v7, v2

    goto :goto_1

    .line 83
    :cond_0
    const/16 v14, 0x12

    if-ne v6, v14, :cond_3

    .line 84
    sget-object v6, Ltv/peel/samsung/widget/f;->b:Ltv/peel/samsung/widget/a;

    if-eqz v6, :cond_1

    sget-object v6, Ltv/peel/samsung/widget/f;->b:Ltv/peel/samsung/widget/a;

    invoke-virtual {v6}, Ltv/peel/samsung/widget/a;->b()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 85
    const v6, 0x7f0a018d

    const/4 v14, 0x0

    invoke-virtual {v4, v6, v14}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 86
    const v6, 0x7f0a018d

    sget-object v14, Ltv/peel/samsung/widget/f;->b:Ltv/peel/samsung/widget/a;

    invoke-virtual {v14}, Ltv/peel/samsung/widget/a;->d()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v4, v6, v14}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 92
    :cond_1
    :goto_3
    invoke-interface {v13, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    move v2, v7

    goto :goto_2

    .line 89
    :cond_3
    const/4 v14, 0x2

    if-eq v6, v14, :cond_4

    const/16 v14, 0x14

    if-ne v6, v14, :cond_1

    .line 90
    :cond_4
    const/4 v1, 0x1

    goto :goto_3

    .line 96
    :cond_5
    const/4 v2, 0x2

    move/from16 v0, p5

    if-ge v0, v2, :cond_6

    .line 97
    const v2, 0x7f0a0323

    const/4 v3, 0x4

    invoke-virtual {v4, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 98
    const v2, 0x7f0a0322

    const/4 v3, 0x4

    invoke-virtual {v4, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 101
    :cond_6
    invoke-static {p0, v4}, Ltv/peel/samsung/widget/f;->a(Landroid/content/Context;Landroid/widget/RemoteViews;)V

    .line 102
    const v2, 0x7f0a01af

    sget-object v3, Ltv/peel/samsung/widget/f;->a:Ljava/lang/String;

    invoke-virtual {v4, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 103
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v2

    .line 105
    if-eqz v1, :cond_7

    const v3, 0x7f03008f

    if-ne v9, v3, :cond_7

    .line 106
    const v1, 0x7f0a0180

    const/16 v3, 0x8

    invoke-virtual {v4, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 107
    const v1, 0x7f0a0181

    const/4 v3, 0x0

    invoke-virtual {v4, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 108
    const v1, 0x7f0a01b7

    const/4 v3, 0x0

    invoke-virtual {v4, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 119
    :goto_4
    if-nez v2, :cond_9

    .line 120
    const v1, 0x7f0a01b1

    const/16 v2, 0x8

    invoke-virtual {v4, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 121
    const v1, 0x7f0a01b2

    const/16 v2, 0x8

    invoke-virtual {v4, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 122
    const v1, 0x7f0a01b3

    const/16 v2, 0x8

    invoke-virtual {v4, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 149
    :goto_5
    invoke-static {p0, v4, v9, v7}, Ltv/peel/samsung/widget/f;->a(Landroid/content/Context;Landroid/widget/RemoteViews;IZ)V

    .line 150
    invoke-virtual {v10, v12, v4}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 66
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    goto/16 :goto_0

    .line 109
    :cond_7
    if-nez v1, :cond_8

    const v1, 0x7f03008f

    if-ne v9, v1, :cond_8

    .line 110
    const v1, 0x7f0a0180

    const/4 v3, 0x0

    invoke-virtual {v4, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 111
    const v1, 0x7f0a0181

    const/16 v3, 0x8

    invoke-virtual {v4, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 112
    const v1, 0x7f0a01b7

    const/4 v3, 0x4

    invoke-virtual {v4, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_4

    .line 114
    :cond_8
    const v1, 0x7f0a01b7

    const/4 v3, 0x4

    invoke-virtual {v4, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_4

    .line 123
    :cond_9
    const/4 v1, 0x1

    if-ne v2, v1, :cond_a

    .line 124
    const v1, 0x7f0a01b1

    const/4 v2, 0x4

    invoke-virtual {v4, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 125
    const v1, 0x7f0a01b2

    const/4 v2, 0x0

    invoke-virtual {v4, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 126
    const v1, 0x7f0a01b3

    const/4 v2, 0x4

    invoke-virtual {v4, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 127
    const v2, 0x7f0a01b2

    const/4 v1, 0x0

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->d()I

    move-result v1

    invoke-static {p0, v1}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v2, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 128
    const v2, 0x7f0a01b2

    const/4 v1, 0x0

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v3

    const/4 v1, 0x0

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    invoke-static {v1}, Ltv/peel/samsung/widget/f;->a(Lcom/peel/control/h;)Z

    move-result v5

    const/4 v6, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v6}, Ltv/peel/samsung/widget/f;->a(Landroid/content/Context;ILjava/lang/String;Landroid/widget/RemoteViews;ZI)V

    goto/16 :goto_5

    .line 129
    :cond_a
    const/4 v1, 0x2

    if-ne v2, v1, :cond_b

    .line 130
    const v1, 0x7f0a01b1

    const/4 v2, 0x0

    invoke-virtual {v4, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 131
    const v1, 0x7f0a01b3

    const/4 v2, 0x0

    invoke-virtual {v4, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 132
    const v1, 0x7f0a01b2

    const/4 v2, 0x4

    invoke-virtual {v4, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 133
    const v2, 0x7f0a01b1

    const/4 v1, 0x0

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->d()I

    move-result v1

    invoke-static {p0, v1}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v2, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 134
    const v2, 0x7f0a01b3

    const/4 v1, 0x1

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->d()I

    move-result v1

    invoke-static {p0, v1}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v2, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 135
    const v2, 0x7f0a01b1

    const/4 v1, 0x0

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v3

    const/4 v1, 0x0

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    invoke-static {v1}, Ltv/peel/samsung/widget/f;->a(Lcom/peel/control/h;)Z

    move-result v5

    const/4 v6, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v6}, Ltv/peel/samsung/widget/f;->a(Landroid/content/Context;ILjava/lang/String;Landroid/widget/RemoteViews;ZI)V

    .line 136
    const v2, 0x7f0a01b3

    const/4 v1, 0x1

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v3

    const/4 v1, 0x1

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    invoke-static {v1}, Ltv/peel/samsung/widget/f;->a(Lcom/peel/control/h;)Z

    move-result v5

    const/4 v6, 0x1

    move-object v1, p0

    invoke-static/range {v1 .. v6}, Ltv/peel/samsung/widget/f;->a(Landroid/content/Context;ILjava/lang/String;Landroid/widget/RemoteViews;ZI)V

    goto/16 :goto_5

    .line 138
    :cond_b
    const v1, 0x7f0a01b1

    const/4 v2, 0x0

    invoke-virtual {v4, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 139
    const v1, 0x7f0a01b2

    const/4 v2, 0x0

    invoke-virtual {v4, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 140
    const v1, 0x7f0a01b3

    const/4 v2, 0x0

    invoke-virtual {v4, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 141
    const v2, 0x7f0a01b1

    const/4 v1, 0x0

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->d()I

    move-result v1

    invoke-static {p0, v1}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v2, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 142
    const v2, 0x7f0a01b2

    const/4 v1, 0x1

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->d()I

    move-result v1

    invoke-static {p0, v1}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v2, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 143
    const v2, 0x7f0a01b3

    const/4 v1, 0x2

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->d()I

    move-result v1

    invoke-static {p0, v1}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v2, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 144
    const v2, 0x7f0a01b1

    const/4 v1, 0x0

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v3

    const/4 v1, 0x0

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    invoke-static {v1}, Ltv/peel/samsung/widget/f;->a(Lcom/peel/control/h;)Z

    move-result v5

    const/4 v6, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v6}, Ltv/peel/samsung/widget/f;->a(Landroid/content/Context;ILjava/lang/String;Landroid/widget/RemoteViews;ZI)V

    .line 145
    const v2, 0x7f0a01b2

    const/4 v1, 0x1

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v3

    const/4 v1, 0x1

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    invoke-static {v1}, Ltv/peel/samsung/widget/f;->a(Lcom/peel/control/h;)Z

    move-result v5

    const/4 v6, 0x1

    move-object v1, p0

    invoke-static/range {v1 .. v6}, Ltv/peel/samsung/widget/f;->a(Landroid/content/Context;ILjava/lang/String;Landroid/widget/RemoteViews;ZI)V

    .line 146
    const v2, 0x7f0a01b3

    const/4 v1, 0x2

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v3

    const/4 v1, 0x2

    invoke-interface {v13, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/peel/control/h;

    invoke-static {v1}, Ltv/peel/samsung/widget/f;->a(Lcom/peel/control/h;)Z

    move-result v5

    const/4 v6, 0x2

    move-object v1, p0

    invoke-static/range {v1 .. v6}, Ltv/peel/samsung/widget/f;->a(Landroid/content/Context;ILjava/lang/String;Landroid/widget/RemoteViews;ZI)V

    goto/16 :goto_5

    .line 153
    :cond_c
    return-void
.end method

.method private static a(Lcom/peel/control/h;)Z
    .locals 2

    .prologue
    .line 156
    invoke-virtual {p0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->a()Ljava/util/Map;

    move-result-object v0

    .line 158
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    if-lez v1, :cond_0

    const-string/jumbo v1, "Power"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    const/4 v0, 0x0

    .line 161
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;Z)I
    .locals 2

    .prologue
    const v0, 0x7f03008f

    .line 349
    invoke-static {p0}, Ltv/peel/widget/util/j;->a(Landroid/content/Context;)Ltv/peel/widget/util/j;

    move-result-object v1

    .line 350
    invoke-virtual {v1}, Ltv/peel/widget/util/j;->c()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 356
    if-eqz p1, :cond_0

    .line 359
    :goto_0
    :sswitch_0
    return v0

    .line 354
    :sswitch_1
    const v0, 0x7f030091

    goto :goto_0

    .line 359
    :cond_0
    const v0, 0x7f030090

    goto :goto_0

    .line 350
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x12 -> :sswitch_1
    .end sparse-switch
.end method
