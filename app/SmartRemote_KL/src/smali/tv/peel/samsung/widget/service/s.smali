.class Ltv/peel/samsung/widget/service/s;
.super Lcom/peel/util/t;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Ltv/peel/samsung/widget/service/r;


# direct methods
.method constructor <init>(Ltv/peel/samsung/widget/service/r;Z)V
    .locals 0

    .prologue
    .line 187
    iput-object p1, p0, Ltv/peel/samsung/widget/service/s;->b:Ltv/peel/samsung/widget/service/r;

    iput-boolean p2, p0, Ltv/peel/samsung/widget/service/s;->a:Z

    invoke-direct {p0}, Lcom/peel/util/t;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 190
    iget-object v0, p0, Ltv/peel/samsung/widget/service/s;->b:Ltv/peel/samsung/widget/service/r;

    iget-object v0, v0, Ltv/peel/samsung/widget/service/r;->b:Ltv/peel/samsung/widget/service/WidgetTimerService;

    const-string/jumbo v1, "widget_pref"

    invoke-virtual {v0, v1, v4}, Ltv/peel/samsung/widget/service/WidgetTimerService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 191
    const-string/jumbo v1, "notification"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 193
    iget-boolean v2, p0, Ltv/peel/samsung/widget/service/s;->i:Z

    if-eqz v2, :cond_3

    .line 194
    iget-boolean v2, p0, Ltv/peel/samsung/widget/service/s;->a:Z

    if-nez v2, :cond_0

    .line 195
    const-string/jumbo v2, "showquickpanel"

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 196
    const-string/jumbo v3, "user_cleared"

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 197
    if-eqz v2, :cond_1

    if-nez v3, :cond_1

    if-nez v1, :cond_1

    .line 198
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "notification"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 199
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 200
    const-string/jumbo v1, "tv.peel.samsung.notification.EXPANDED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 201
    iget-object v1, p0, Ltv/peel/samsung/widget/service/s;->b:Ltv/peel/samsung/widget/service/r;

    iget-object v1, v1, Ltv/peel/samsung/widget/service/r;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 215
    :cond_0
    :goto_0
    return-void

    .line 202
    :cond_1
    if-eqz v1, :cond_0

    if-eqz v2, :cond_2

    if-eqz v3, :cond_0

    .line 203
    :cond_2
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "notification"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 204
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 205
    const-string/jumbo v1, "tv.peel.samsung.notification.COLLAPSED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 206
    iget-object v1, p0, Ltv/peel/samsung/widget/service/s;->b:Ltv/peel/samsung/widget/service/r;

    iget-object v1, v1, Ltv/peel/samsung/widget/service/r;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 209
    :cond_3
    if-eqz v1, :cond_0

    iget-boolean v1, p0, Ltv/peel/samsung/widget/service/s;->a:Z

    if-nez v1, :cond_0

    .line 210
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "notification"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 211
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 212
    const-string/jumbo v1, "tv.peel.samsung.notification.COLLAPSED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 213
    iget-object v1, p0, Ltv/peel/samsung/widget/service/s;->b:Ltv/peel/samsung/widget/service/r;

    iget-object v1, v1, Ltv/peel/samsung/widget/service/r;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method
