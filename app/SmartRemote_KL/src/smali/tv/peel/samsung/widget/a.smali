.class public Ltv/peel/samsung/widget/a;
.super Ltv/peel/widget/b;


# static fields
.field private static final a:Ljava/lang/String;

.field private static f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static j:I

.field private static k:I

.field private static l:I

.field private static q:Z


# instance fields
.field private b:Ltv/peel/samsung/widget/service/f;

.field private c:Landroid/content/Context;

.field private d:Ltv/peel/widget/util/c;

.field private e:I

.field private i:Landroid/content/SharedPreferences;

.field private m:Z

.field private n:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private r:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private s:Landroid/content/ServiceConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 47
    const-class v0, Ltv/peel/samsung/widget/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ltv/peel/samsung/widget/a;->a:Ljava/lang/String;

    .line 55
    sput v1, Ltv/peel/samsung/widget/a;->j:I

    sput v1, Ltv/peel/samsung/widget/a;->k:I

    sput v1, Ltv/peel/samsung/widget/a;->l:I

    .line 60
    sput-boolean v1, Ltv/peel/samsung/widget/a;->q:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Ltv/peel/widget/b;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/peel/samsung/widget/a;->b:Ltv/peel/samsung/widget/service/f;

    .line 52
    const/4 v0, -0x1

    iput v0, p0, Ltv/peel/samsung/widget/a;->e:I

    .line 56
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/peel/samsung/widget/a;->m:Z

    .line 309
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ltv/peel/samsung/widget/a;->r:Ljava/util/Map;

    .line 527
    new-instance v0, Ltv/peel/samsung/widget/b;

    invoke-direct {v0, p0}, Ltv/peel/samsung/widget/b;-><init>(Ltv/peel/samsung/widget/a;)V

    iput-object v0, p0, Ltv/peel/samsung/widget/a;->s:Landroid/content/ServiceConnection;

    .line 63
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Ltv/peel/samsung/widget/a;->i:Landroid/content/SharedPreferences;

    .line 64
    iput-object p1, p0, Ltv/peel/samsung/widget/a;->c:Landroid/content/Context;

    .line 65
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ltv/peel/widget/util/c;)V
    .locals 4

    .prologue
    .line 67
    invoke-direct {p0}, Ltv/peel/widget/b;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/peel/samsung/widget/a;->b:Ltv/peel/samsung/widget/service/f;

    .line 52
    const/4 v0, -0x1

    iput v0, p0, Ltv/peel/samsung/widget/a;->e:I

    .line 56
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/peel/samsung/widget/a;->m:Z

    .line 309
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ltv/peel/samsung/widget/a;->r:Ljava/util/Map;

    .line 527
    new-instance v0, Ltv/peel/samsung/widget/b;

    invoke-direct {v0, p0}, Ltv/peel/samsung/widget/b;-><init>(Ltv/peel/samsung/widget/a;)V

    iput-object v0, p0, Ltv/peel/samsung/widget/a;->s:Landroid/content/ServiceConnection;

    .line 68
    iput-object p1, p0, Ltv/peel/samsung/widget/a;->c:Landroid/content/Context;

    .line 69
    iput-object p2, p0, Ltv/peel/samsung/widget/a;->d:Ltv/peel/widget/util/c;

    .line 71
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Ltv/peel/samsung/widget/a;->i:Landroid/content/SharedPreferences;

    .line 73
    iget-object v0, p0, Ltv/peel/samsung/widget/a;->b:Ltv/peel/samsung/widget/service/f;

    if-nez v0, :cond_0

    .line 74
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "tv.peel.samsung.widget.service.IPeelService"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 75
    iget-object v1, p0, Ltv/peel/samsung/widget/a;->s:Landroid/content/ServiceConnection;

    if-eqz v1, :cond_1

    .line 76
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Ltv/peel/samsung/widget/a;->s:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 81
    :cond_0
    :goto_0
    return-void

    .line 78
    :cond_1
    sget-object v0, Ltv/peel/samsung/widget/a;->a:Ljava/lang/String;

    const-string/jumbo v1, "ServiceConnection is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic a(Ltv/peel/samsung/widget/a;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Ltv/peel/samsung/widget/a;->c:Landroid/content/Context;

    return-object v0
.end method

.method private a(Ljava/lang/String;)Lcom/peel/control/a;
    .locals 11

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 461
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1, p1}, Lcom/peel/control/am;->a(Ljava/lang/String;)Lcom/peel/control/RoomControl;

    move-result-object v2

    .line 462
    invoke-virtual {v2}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v5

    .line 463
    invoke-virtual {v2}, Lcom/peel/control/RoomControl;->d()Lcom/peel/control/a;

    move-result-object v1

    .line 464
    if-eqz v1, :cond_1

    const/4 v4, 0x2

    invoke-virtual {v1}, Lcom/peel/control/a;->f()I

    move-result v6

    if-ne v4, v6, :cond_1

    move-object v0, v1

    .line 494
    :cond_0
    :goto_0
    return-object v0

    .line 468
    :cond_1
    iget-object v1, p0, Ltv/peel/samsung/widget/a;->c:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v4, "last_activity"

    invoke-interface {v1, v4, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 469
    if-eqz v4, :cond_3

    .line 472
    :try_start_0
    invoke-virtual {v2}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v6

    array-length v7, v6

    move v2, v3

    :goto_1
    if-ge v2, v7, :cond_3

    aget-object v1, v6, v2

    .line 473
    invoke-virtual {v1}, Lcom/peel/control/a;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    if-eqz v8, :cond_2

    move-object v0, v1

    .line 474
    goto :goto_0

    .line 472
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 477
    :catch_0
    move-exception v1

    .line 482
    :cond_3
    if-eqz v5, :cond_7

    .line 483
    array-length v6, v5

    move v4, v3

    :goto_2
    if-ge v4, v6, :cond_7

    aget-object v1, v5, v4

    .line 484
    invoke-virtual {v1}, Lcom/peel/control/a;->d()[Ljava/lang/String;

    move-result-object v7

    .line 485
    if-nez v7, :cond_5

    .line 483
    :cond_4
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_2

    .line 487
    :cond_5
    array-length v8, v7

    move v2, v3

    :goto_3
    if-ge v2, v8, :cond_4

    aget-object v9, v7, v2

    .line 488
    const-string/jumbo v10, "live"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    move-object v0, v1

    .line 489
    goto :goto_0

    .line 487
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 494
    :cond_7
    if-eqz v5, :cond_0

    aget-object v0, v5, v3

    goto :goto_0
.end method

.method private a(Lcom/peel/control/h;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 194
    const-string/jumbo v0, "UP"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 195
    sget-object v0, Ltv/peel/samsung/widget/a;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 197
    sget-object v0, Ltv/peel/samsung/widget/a;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "No Temperature commands for AC "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " -- code set: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, p2

    .line 213
    :goto_0
    const-string/jumbo v1, "T"

    iput-object v1, p0, Ltv/peel/samsung/widget/a;->p:Ljava/lang/String;

    move-object p2, v0

    .line 287
    :cond_0
    :goto_1
    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    const-string/jumbo v1, "VANE"

    invoke-virtual {v0, v1}, Lcom/peel/data/g;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 288
    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->a()Ljava/util/Map;

    move-result-object v0

    const-string/jumbo v1, "VANE"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    const-string/jumbo v1, "type"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ltv/peel/samsung/widget/a;->o:Ljava/lang/String;

    .line 294
    :goto_2
    iget-object v0, p0, Ltv/peel/samsung/widget/a;->o:Ljava/lang/String;

    const-string/jumbo v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    iput-boolean v0, p0, Ltv/peel/samsung/widget/a;->m:Z

    .line 295
    sget-object v0, Ltv/peel/samsung/widget/a;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "\ncombo code rule: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Ltv/peel/samsung/widget/a;->o:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " -- use combo codes: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Ltv/peel/samsung/widget/a;->m:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " --use bruteforce: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Ltv/peel/samsung/widget/a;->q:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    iget-boolean v0, p0, Ltv/peel/samsung/widget/a;->m:Z

    if-eqz v0, :cond_16

    .line 297
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ltv/peel/samsung/widget/a;->n:Ljava/util/Map;

    .line 298
    iget-object v0, p0, Ltv/peel/samsung/widget/a;->p:Ljava/lang/String;

    invoke-direct {p0, p2, v0, p1}, Ltv/peel/samsung/widget/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/peel/control/h;)V

    .line 306
    :cond_1
    :goto_3
    return-object p2

    .line 198
    :cond_2
    sget-object v0, Ltv/peel/samsung/widget/a;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_3

    .line 199
    sget-object v0, Ltv/peel/samsung/widget/a;->f:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 200
    sget-object v2, Ltv/peel/samsung/widget/a;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sending the only 1 temperature command: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v1, Ltv/peel/samsung/widget/a;->f:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 202
    :cond_3
    sget v0, Ltv/peel/samsung/widget/a;->j:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Ltv/peel/samsung/widget/a;->j:I

    .line 203
    sget v0, Ltv/peel/samsung/widget/a;->j:I

    sget-object v1, Ltv/peel/samsung/widget/a;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_4

    .line 204
    sget-object v0, Ltv/peel/samsung/widget/a;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    sput v0, Ltv/peel/samsung/widget/a;->j:I

    .line 205
    :cond_4
    sget-object v0, Ltv/peel/samsung/widget/a;->f:Ljava/util/List;

    sget v1, Ltv/peel/samsung/widget/a;->j:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 207
    :try_start_0
    iget-object v1, p0, Ltv/peel/samsung/widget/a;->i:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v3}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "ac_last_temp_idx"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget v3, Ltv/peel/samsung/widget/a;->j:I

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 211
    :goto_4
    sget-object v2, Ltv/peel/samsung/widget/a;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sending the Temperature command (idx: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v3, Ltv/peel/samsung/widget/a;->j:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "): "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v1, Ltv/peel/samsung/widget/a;->f:Ljava/util/List;

    sget v4, Ltv/peel/samsung/widget/a;->j:I

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 208
    :catch_0
    move-exception v1

    .line 209
    sget-object v2, Ltv/peel/samsung/widget/a;->a:Ljava/lang/String;

    sget-object v3, Ltv/peel/samsung/widget/a;->a:Ljava/lang/String;

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    .line 214
    :cond_5
    const-string/jumbo v0, "Down"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 215
    sget-object v0, Ltv/peel/samsung/widget/a;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_6

    .line 217
    sget-object v0, Ltv/peel/samsung/widget/a;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "No Temperature commands for AC "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " -- code set: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, p2

    .line 233
    :goto_5
    const-string/jumbo v1, "T"

    iput-object v1, p0, Ltv/peel/samsung/widget/a;->p:Ljava/lang/String;

    move-object p2, v0

    goto/16 :goto_1

    .line 218
    :cond_6
    sget-object v0, Ltv/peel/samsung/widget/a;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_7

    .line 219
    sget-object v0, Ltv/peel/samsung/widget/a;->f:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 220
    sget-object v2, Ltv/peel/samsung/widget/a;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sending the only 1 temperature command: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v1, Ltv/peel/samsung/widget/a;->f:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 222
    :cond_7
    sget v0, Ltv/peel/samsung/widget/a;->j:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Ltv/peel/samsung/widget/a;->j:I

    .line 223
    sget v0, Ltv/peel/samsung/widget/a;->j:I

    if-gez v0, :cond_8

    .line 224
    sput v4, Ltv/peel/samsung/widget/a;->j:I

    .line 225
    :cond_8
    sget-object v0, Ltv/peel/samsung/widget/a;->f:Ljava/util/List;

    sget v1, Ltv/peel/samsung/widget/a;->j:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 227
    :try_start_1
    iget-object v1, p0, Ltv/peel/samsung/widget/a;->i:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v3}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "ac_last_temp_idx"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget v3, Ltv/peel/samsung/widget/a;->j:I

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 231
    :goto_6
    sget-object v2, Ltv/peel/samsung/widget/a;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sending the Temperature command (idx: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v3, Ltv/peel/samsung/widget/a;->j:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "): "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v1, Ltv/peel/samsung/widget/a;->f:Ljava/util/List;

    sget v4, Ltv/peel/samsung/widget/a;->j:I

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 228
    :catch_1
    move-exception v1

    .line 229
    sget-object v2, Ltv/peel/samsung/widget/a;->a:Ljava/lang/String;

    sget-object v3, Ltv/peel/samsung/widget/a;->a:Ljava/lang/String;

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_6

    .line 234
    :cond_9
    const-string/jumbo v0, "FAN_HIGH"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 235
    sget-object v0, Ltv/peel/samsung/widget/a;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_a

    .line 237
    sget-object v0, Ltv/peel/samsung/widget/a;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "No Fan Speed commands for AC: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " -- code set: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, p2

    .line 249
    :goto_7
    const-string/jumbo v1, "F"

    iput-object v1, p0, Ltv/peel/samsung/widget/a;->p:Ljava/lang/String;

    move-object p2, v0

    goto/16 :goto_1

    .line 238
    :cond_a
    sget-object v0, Ltv/peel/samsung/widget/a;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_b

    .line 239
    sget-object v0, Ltv/peel/samsung/widget/a;->g:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 240
    sget-object v2, Ltv/peel/samsung/widget/a;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sending the only 1 Fan command: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v1, Ltv/peel/samsung/widget/a;->g:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    .line 242
    :cond_b
    sget v0, Ltv/peel/samsung/widget/a;->k:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Ltv/peel/samsung/widget/a;->k:I

    .line 243
    sget v0, Ltv/peel/samsung/widget/a;->k:I

    sget-object v1, Ltv/peel/samsung/widget/a;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_c

    .line 244
    sget-object v0, Ltv/peel/samsung/widget/a;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    sput v0, Ltv/peel/samsung/widget/a;->k:I

    .line 245
    :cond_c
    sget-object v0, Ltv/peel/samsung/widget/a;->g:Ljava/util/List;

    sget v1, Ltv/peel/samsung/widget/a;->k:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 246
    iget-object v1, p0, Ltv/peel/samsung/widget/a;->i:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v3}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "ac_last_fanspeed_idx"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget v3, Ltv/peel/samsung/widget/a;->k:I

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 247
    sget-object v2, Ltv/peel/samsung/widget/a;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sending the Fan command (idx: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v3, Ltv/peel/samsung/widget/a;->k:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "): "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v1, Ltv/peel/samsung/widget/a;->g:Ljava/util/List;

    sget v4, Ltv/peel/samsung/widget/a;->k:I

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_7

    .line 250
    :cond_d
    const-string/jumbo v0, "FAN_LOW"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 251
    sget-object v0, Ltv/peel/samsung/widget/a;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_e

    .line 253
    sget-object v0, Ltv/peel/samsung/widget/a;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "No Fan Speed commands for AC: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " -- code set: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, p2

    .line 265
    :goto_8
    const-string/jumbo v1, "F"

    iput-object v1, p0, Ltv/peel/samsung/widget/a;->p:Ljava/lang/String;

    move-object p2, v0

    goto/16 :goto_1

    .line 254
    :cond_e
    sget-object v0, Ltv/peel/samsung/widget/a;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_f

    .line 255
    sget-object v0, Ltv/peel/samsung/widget/a;->g:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 256
    sget-object v2, Ltv/peel/samsung/widget/a;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sending the only 1 Fan command: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v1, Ltv/peel/samsung/widget/a;->g:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8

    .line 258
    :cond_f
    sget v0, Ltv/peel/samsung/widget/a;->k:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Ltv/peel/samsung/widget/a;->k:I

    .line 259
    sget v0, Ltv/peel/samsung/widget/a;->k:I

    if-gez v0, :cond_10

    .line 260
    sput v4, Ltv/peel/samsung/widget/a;->k:I

    .line 261
    :cond_10
    sget-object v0, Ltv/peel/samsung/widget/a;->g:Ljava/util/List;

    sget v1, Ltv/peel/samsung/widget/a;->k:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 262
    iget-object v1, p0, Ltv/peel/samsung/widget/a;->i:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v3}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "ac_last_fanspeed_idx"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget v3, Ltv/peel/samsung/widget/a;->k:I

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 263
    sget-object v2, Ltv/peel/samsung/widget/a;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sending the Fan command (idx: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v3, Ltv/peel/samsung/widget/a;->k:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "): "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v1, Ltv/peel/samsung/widget/a;->g:Ljava/util/List;

    sget v4, Ltv/peel/samsung/widget/a;->k:I

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_8

    .line 266
    :cond_11
    const-string/jumbo v0, "MODE"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 270
    sget-object v0, Ltv/peel/samsung/widget/a;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_12

    .line 272
    sget-object v0, Ltv/peel/samsung/widget/a;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "No Mode commands for AC: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " -- code set: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, p2

    .line 284
    :goto_9
    const-string/jumbo v1, "M"

    iput-object v1, p0, Ltv/peel/samsung/widget/a;->p:Ljava/lang/String;

    move-object p2, v0

    goto/16 :goto_1

    .line 273
    :cond_12
    sget-object v0, Ltv/peel/samsung/widget/a;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_13

    .line 274
    sget-object v0, Ltv/peel/samsung/widget/a;->h:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 275
    sget-object v2, Ltv/peel/samsung/widget/a;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sending the only 1 direction command: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v1, Ltv/peel/samsung/widget/a;->h:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9

    .line 277
    :cond_13
    sget v0, Ltv/peel/samsung/widget/a;->l:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Ltv/peel/samsung/widget/a;->l:I

    .line 278
    sget v0, Ltv/peel/samsung/widget/a;->l:I

    sget-object v1, Ltv/peel/samsung/widget/a;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    rem-int/2addr v0, v1

    sput v0, Ltv/peel/samsung/widget/a;->l:I

    .line 279
    sget-object v0, Ltv/peel/samsung/widget/a;->h:Ljava/util/List;

    sget v1, Ltv/peel/samsung/widget/a;->l:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 280
    iget-object v1, p0, Ltv/peel/samsung/widget/a;->i:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v3}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "ac_last_mode_idx"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget v3, Ltv/peel/samsung/widget/a;->l:I

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 281
    sget-object v2, Ltv/peel/samsung/widget/a;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sending the direction command (idx: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v3, Ltv/peel/samsung/widget/a;->l:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "): "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v1, Ltv/peel/samsung/widget/a;->h:Ljava/util/List;

    sget v4, Ltv/peel/samsung/widget/a;->l:I

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_9

    .line 289
    :cond_14
    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    const-string/jumbo v1, "T_22"

    invoke-virtual {v0, v1}, Lcom/peel/data/g;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 290
    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->a()Ljava/util/Map;

    move-result-object v0

    const-string/jumbo v1, "T_22"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    const-string/jumbo v1, "type"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ltv/peel/samsung/widget/a;->o:Ljava/lang/String;

    goto/16 :goto_2

    .line 292
    :cond_15
    const-string/jumbo v0, ""

    iput-object v0, p0, Ltv/peel/samsung/widget/a;->o:Ljava/lang/String;

    goto/16 :goto_2

    .line 299
    :cond_16
    sget-boolean v0, Ltv/peel/samsung/widget/a;->q:Z

    if-eqz v0, :cond_1

    .line 300
    iget-object v0, p0, Ltv/peel/samsung/widget/a;->n:Ljava/util/Map;

    if-nez v0, :cond_17

    .line 301
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ltv/peel/samsung/widget/a;->n:Ljava/util/Map;

    .line 303
    :cond_17
    iget-object v0, p0, Ltv/peel/samsung/widget/a;->n:Ljava/util/Map;

    iget-object v1, p0, Ltv/peel/samsung/widget/a;->p:Ljava/lang/String;

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 304
    iget-object v0, p0, Ltv/peel/samsung/widget/a;->n:Ljava/util/Map;

    invoke-static {v0}, Lcom/peel/util/ay;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_3
.end method

.method static synthetic a(Ltv/peel/samsung/widget/a;Ltv/peel/samsung/widget/service/f;)Ltv/peel/samsung/widget/service/f;
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Ltv/peel/samsung/widget/a;->b:Ltv/peel/samsung/widget/service/f;

    return-object p1
.end method

.method private a(ILjava/lang/String;Lcom/peel/control/h;)V
    .locals 8

    .prologue
    const/16 v2, 0x1388

    const/4 v5, -0x1

    .line 637
    if-eqz p3, :cond_0

    .line 638
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    invoke-virtual {p3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->h()I

    move-result v5

    invoke-virtual {p3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v6

    .line 639
    invoke-virtual {p3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->d()I

    move-result v7

    move v3, p1

    move-object v4, p2

    .line 638
    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    .line 643
    :goto_0
    return-void

    .line 641
    :cond_0
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->f()I

    move-result v1

    const-string/jumbo v6, "no device"

    move v3, p1

    move-object v4, p2

    move v7, v5

    invoke-virtual/range {v0 .. v7}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;ILjava/lang/String;I)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lcom/peel/control/h;)V
    .locals 7

    .prologue
    .line 312
    iget-boolean v0, p0, Ltv/peel/samsung/widget/a;->m:Z

    if-eqz v0, :cond_2

    .line 316
    invoke-virtual {p3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->a()Ljava/util/Map;

    move-result-object v3

    .line 318
    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 322
    const-string/jumbo v1, "PowerOn"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string/jumbo v1, "PowerOff"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 323
    iget-object v1, p0, Ltv/peel/samsung/widget/a;->n:Ljava/util/Map;

    invoke-interface {v1, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 326
    iget-object v1, p0, Ltv/peel/samsung/widget/a;->r:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 327
    iget-object v1, p0, Ltv/peel/samsung/widget/a;->r:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 328
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    .line 329
    const-string/jumbo v5, "ir"

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v2, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 330
    invoke-virtual {p3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v5

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v5, v1, v2}, Lcom/peel/data/g;->a(Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_0

    .line 334
    :cond_0
    iget-object v1, p0, Ltv/peel/samsung/widget/a;->r:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 336
    iget-object v1, p0, Ltv/peel/samsung/widget/a;->r:Ljava/util/Map;

    const-string/jumbo v2, "ir"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 342
    :cond_1
    iget-object v1, p0, Ltv/peel/samsung/widget/a;->o:Ljava/lang/String;

    iget-object v2, p0, Ltv/peel/samsung/widget/a;->n:Ljava/util/Map;

    invoke-static {v1, v3, v2}, Lcom/peel/util/ay;->b(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v1

    .line 344
    const-string/jumbo v2, "ir"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 346
    invoke-virtual {p3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/peel/data/g;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 349
    :cond_2
    return-void
.end method

.method static synthetic b(Ltv/peel/samsung/widget/a;)I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Ltv/peel/samsung/widget/a;->e:I

    return v0
.end method

.method static synthetic c(Ltv/peel/samsung/widget/a;)Ltv/peel/widget/util/c;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Ltv/peel/samsung/widget/a;->d:Ltv/peel/widget/util/c;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/peel/control/a;)Ljava/lang/String;
    .locals 7

    .prologue
    const v6, 0x7f0d045e

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 404
    const/4 v0, 0x0

    .line 405
    if-eqz p1, :cond_0

    .line 406
    invoke-virtual {p1, v4}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v0

    .line 408
    if-eqz v0, :cond_2

    .line 409
    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v1

    .line 411
    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 429
    iget-object v0, p0, Ltv/peel/samsung/widget/a;->c:Landroid/content/Context;

    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/peel/control/a;->c()Lcom/peel/data/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/d;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 437
    :cond_0
    :goto_0
    return-object v0

    .line 413
    :sswitch_0
    iget-object v0, p0, Ltv/peel/samsung/widget/a;->c:Landroid/content/Context;

    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/peel/control/a;->c()Lcom/peel/data/d;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/d;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 414
    const-string/jumbo v2, "directv"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string/jumbo v2, "tivo"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 415
    :cond_1
    iget-object v0, p0, Ltv/peel/samsung/widget/a;->c:Landroid/content/Context;

    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v1, v2, v5

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 419
    :sswitch_1
    iget-object v0, p0, Ltv/peel/samsung/widget/a;->c:Landroid/content/Context;

    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v1, v2, v5

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 423
    :sswitch_2
    invoke-virtual {p1}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 433
    :cond_2
    iget-object v0, p0, Ltv/peel/samsung/widget/a;->c:Landroid/content/Context;

    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/peel/control/a;->c()Lcom/peel/data/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/d;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 411
    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_2
        0x6 -> :sswitch_1
        0x12 -> :sswitch_2
        0x14 -> :sswitch_0
    .end sparse-switch
.end method

.method public a(Lcom/peel/control/h;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 90
    invoke-virtual {p1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->a()Ljava/util/Map;

    move-result-object v1

    .line 92
    const-string/jumbo v0, "16_F_A_C"

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    const/4 v0, 0x1

    sput-boolean v0, Ltv/peel/samsung/widget/a;->q:Z

    .line 97
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Ltv/peel/samsung/widget/a;->f:Ljava/util/List;

    .line 98
    const/16 v0, 0x10

    :goto_0
    const/16 v2, 0x1e

    if-gt v0, v2, :cond_3

    .line 99
    sget-boolean v2, Ltv/peel/samsung/widget/a;->q:Z

    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "T_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 100
    :cond_1
    sget-object v2, Ltv/peel/samsung/widget/a;->f:Ljava/util/List;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "T_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 106
    :cond_3
    sget-object v0, Ltv/peel/samsung/widget/a;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_5

    .line 107
    const-string/jumbo v0, "UP"

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 108
    sget-object v0, Ltv/peel/samsung/widget/a;->f:Ljava/util/List;

    const-string/jumbo v2, "UP"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    :cond_4
    const-string/jumbo v0, "Down"

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 110
    sget-object v0, Ltv/peel/samsung/widget/a;->f:Ljava/util/List;

    const-string/jumbo v2, "Down"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 114
    :cond_5
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Ltv/peel/samsung/widget/a;->g:Ljava/util/List;

    .line 115
    sget-boolean v0, Ltv/peel/samsung/widget/a;->q:Z

    if-nez v0, :cond_6

    const-string/jumbo v0, "FAN_LOW"

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 116
    :cond_6
    sget-object v0, Ltv/peel/samsung/widget/a;->g:Ljava/util/List;

    const-string/jumbo v2, "FAN_LOW"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 117
    :cond_7
    sget-boolean v0, Ltv/peel/samsung/widget/a;->q:Z

    if-nez v0, :cond_8

    const-string/jumbo v0, "FAN_MED"

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 118
    :cond_8
    sget-object v0, Ltv/peel/samsung/widget/a;->g:Ljava/util/List;

    const-string/jumbo v2, "FAN_MED"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 119
    :cond_9
    sget-boolean v0, Ltv/peel/samsung/widget/a;->q:Z

    if-nez v0, :cond_a

    const-string/jumbo v0, "FAN_HIGH"

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 120
    :cond_a
    sget-object v0, Ltv/peel/samsung/widget/a;->g:Ljava/util/List;

    const-string/jumbo v2, "FAN_HIGH"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 122
    :cond_b
    sget-boolean v0, Ltv/peel/samsung/widget/a;->q:Z

    if-eqz v0, :cond_c

    sget-object v0, Ltv/peel/samsung/widget/a;->g:Ljava/util/List;

    const-string/jumbo v2, "FAN_AUTO"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 125
    :cond_c
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Ltv/peel/samsung/widget/a;->h:Ljava/util/List;

    .line 126
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 127
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_d
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 128
    const-string/jumbo v2, "Mode_"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 129
    sget-object v2, Ltv/peel/samsung/widget/a;->h:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 131
    :cond_e
    sget-boolean v0, Ltv/peel/samsung/widget/a;->q:Z

    if-eqz v0, :cond_f

    .line 132
    sget-object v0, Ltv/peel/samsung/widget/a;->h:Ljava/util/List;

    const-string/jumbo v1, "Mode_Cool"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 133
    sget-object v0, Ltv/peel/samsung/widget/a;->h:Ljava/util/List;

    const-string/jumbo v1, "Mode_Heat"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 136
    :cond_f
    iget-object v0, p0, Ltv/peel/samsung/widget/a;->i:Landroid/content/SharedPreferences;

    if-nez v0, :cond_13

    const-string/jumbo v0, "prefs null"

    .line 137
    :goto_2
    sget-object v1, Ltv/peel/samsung/widget/a;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    if-nez v0, :cond_14

    const-string/jumbo v0, "PeelControl.control null"

    .line 139
    :goto_3
    sget-object v1, Ltv/peel/samsung/widget/a;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-nez v0, :cond_15

    const-string/jumbo v0, "PeelControl.control.getCurrentRoom() null"

    .line 141
    :goto_4
    sget-object v1, Ltv/peel/samsung/widget/a;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    iget-object v0, p0, Ltv/peel/samsung/widget/a;->i:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "ac_last_temp_idx"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Ltv/peel/samsung/widget/a;->j:I

    .line 146
    sget v0, Ltv/peel/samsung/widget/a;->j:I

    sget-object v1, Ltv/peel/samsung/widget/a;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_10

    .line 147
    sput v5, Ltv/peel/samsung/widget/a;->j:I

    .line 149
    :cond_10
    iget-object v0, p0, Ltv/peel/samsung/widget/a;->i:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "ac_last_fanspeed_idx"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Ltv/peel/samsung/widget/a;->k:I

    .line 151
    sget v0, Ltv/peel/samsung/widget/a;->k:I

    sget-object v1, Ltv/peel/samsung/widget/a;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_11

    .line 152
    sput v5, Ltv/peel/samsung/widget/a;->k:I

    .line 154
    :cond_11
    iget-object v0, p0, Ltv/peel/samsung/widget/a;->i:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "ac_last_mode_idx"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Ltv/peel/samsung/widget/a;->l:I

    .line 156
    sget v0, Ltv/peel/samsung/widget/a;->l:I

    sget-object v1, Ltv/peel/samsung/widget/a;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_12

    .line 157
    sput v5, Ltv/peel/samsung/widget/a;->l:I

    .line 159
    :cond_12
    return-void

    .line 136
    :cond_13
    const-string/jumbo v0, "prefs not null"

    goto/16 :goto_2

    .line 138
    :cond_14
    const-string/jumbo v0, "PeelControl.control not null"

    goto/16 :goto_3

    .line 140
    :cond_15
    const-string/jumbo v0, "PeelControl.control.getCurrentRoom() not null"

    goto/16 :goto_4
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 688
    if-eqz p1, :cond_0

    .line 689
    :try_start_0
    iget-object v0, p0, Ltv/peel/samsung/widget/a;->b:Ltv/peel/samsung/widget/service/f;

    invoke-interface {v0, p1, p2}, Ltv/peel/samsung/widget/service/f;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 693
    :cond_0
    :goto_0
    return-void

    .line 690
    :catch_0
    move-exception v0

    .line 691
    sget-object v1, Ltv/peel/samsung/widget/a;->a:Ljava/lang/String;

    sget-object v2, Ltv/peel/samsung/widget/a;->a:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 648
    if-nez p2, :cond_1

    .line 673
    :cond_0
    :goto_0
    return-void

    .line 651
    :cond_1
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0, p2}, Lcom/peel/control/am;->c(Ljava/lang/String;)Lcom/peel/control/h;

    move-result-object v0

    .line 653
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->d()I

    move-result v1

    const/16 v2, 0x12

    if-ne v1, v2, :cond_2

    .line 654
    const-string/jumbo v1, "PowerOn"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string/jumbo v1, "PowerOff"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 655
    invoke-direct {p0, v0, p1}, Ltv/peel/samsung/widget/a;->a(Lcom/peel/control/h;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 658
    :cond_2
    if-eqz v0, :cond_0

    .line 660
    invoke-static {}, Lcom/peel/util/i;->c()Z

    move-result v1

    if-nez v1, :cond_3

    .line 661
    invoke-virtual {v0, p1}, Lcom/peel/control/h;->c(Ljava/lang/String;)Z

    .line 671
    :goto_1
    invoke-direct {p0, p3, p1, v0}, Ltv/peel/samsung/widget/a;->a(ILjava/lang/String;Lcom/peel/control/h;)V

    goto :goto_0

    .line 663
    :cond_3
    const-class v1, Ltv/peel/samsung/widget/a;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "sendCommand"

    new-instance v3, Ltv/peel/samsung/widget/c;

    invoke-direct {v3, p0, v0, p1}, Ltv/peel/samsung/widget/c;-><init>(Ltv/peel/samsung/widget/a;Lcom/peel/control/h;Ljava/lang/String;)V

    invoke-static {v1, v2, v3}, Lcom/peel/util/i;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_1
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Ltv/peel/samsung/widget/a;->b:Ltv/peel/samsung/widget/service/f;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a([Lcom/peel/control/a;)[Ljava/lang/String;
    .locals 3

    .prologue
    .line 390
    const/4 v0, 0x0

    .line 391
    if-eqz p1, :cond_1

    array-length v1, p1

    if-lez v1, :cond_1

    .line 392
    array-length v0, p1

    new-array v1, v0, [Ljava/lang/String;

    .line 394
    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 395
    aget-object v2, p1, v0

    invoke-virtual {p0, v2}, Ltv/peel/samsung/widget/a;->a(Lcom/peel/control/a;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 394
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 399
    :cond_1
    return-object v0
.end method

.method public b(Lcom/peel/control/a;)V
    .locals 3

    .prologue
    .line 707
    const-class v0, Ltv/peel/samsung/widget/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "started activity"

    new-instance v2, Ltv/peel/samsung/widget/d;

    invoke-direct {v2, p0, p1}, Ltv/peel/samsung/widget/d;-><init>(Ltv/peel/samsung/widget/a;Lcom/peel/control/a;)V

    invoke-static {v0, v1, v2}, Lcom/peel/util/i;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 720
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 163
    sget-object v0, Ltv/peel/samsung/widget/a;->h:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-object v0, Ltv/peel/samsung/widget/a;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 164
    const/4 v0, 0x1

    .line 167
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 3

    .prologue
    .line 172
    sget-object v0, Ltv/peel/samsung/widget/a;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-object v0, Ltv/peel/samsung/widget/a;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 173
    sget-object v0, Ltv/peel/samsung/widget/a;->f:Ljava/util/List;

    sget v1, Ltv/peel/samsung/widget/a;->j:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string/jumbo v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 174
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x1

    aget-object v0, v0, v2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0xb0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "C"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 176
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 181
    sget-object v0, Ltv/peel/samsung/widget/a;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 190
    :goto_0
    return-object v0

    .line 185
    :cond_0
    sget-object v0, Ltv/peel/samsung/widget/a;->h:Ljava/util/List;

    sget v2, Ltv/peel/samsung/widget/a;->l:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 187
    if-eqz v0, :cond_1

    .line 188
    const/16 v1, 0x5f

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 190
    goto :goto_0
.end method

.method public e()[Ltv/peel/widget/service/DeviceParcelable;
    .locals 3

    .prologue
    .line 354
    :try_start_0
    iget-object v0, p0, Ltv/peel/samsung/widget/a;->b:Ltv/peel/samsung/widget/service/f;

    invoke-interface {v0}, Ltv/peel/samsung/widget/service/f;->b()[Ltv/peel/widget/service/DeviceParcelable;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 359
    :goto_0
    return-object v0

    .line 355
    :catch_0
    move-exception v0

    .line 356
    sget-object v1, Ltv/peel/samsung/widget/a;->a:Ljava/lang/String;

    sget-object v2, Ltv/peel/samsung/widget/a;->a:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 359
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()[Lcom/peel/control/a;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 364
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    .line 366
    if-nez v1, :cond_1

    .line 385
    :cond_0
    :goto_0
    return-object v0

    .line 370
    :cond_1
    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->c()[Lcom/peel/control/a;

    move-result-object v1

    .line 371
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 373
    if-eqz v1, :cond_0

    .line 374
    array-length v3, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_3

    aget-object v4, v1, v0

    .line 375
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v5

    .line 376
    invoke-virtual {p0}, Ltv/peel/samsung/widget/a;->k()Z

    move-result v6

    if-nez v6, :cond_2

    if-eqz v5, :cond_2

    invoke-virtual {v5}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/g;->d()I

    move-result v5

    const/4 v6, 0x5

    if-ne v5, v6, :cond_2

    .line 374
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 379
    :cond_2
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 381
    :cond_3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/peel/control/a;

    .line 382
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    goto :goto_0
.end method

.method public g()Lcom/peel/control/a;
    .locals 4

    .prologue
    .line 442
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    .line 443
    const/4 v0, 0x0

    .line 445
    if-eqz v1, :cond_1

    .line 446
    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ltv/peel/samsung/widget/a;->a(Ljava/lang/String;)Lcom/peel/control/a;

    move-result-object v0

    .line 447
    if-eqz v0, :cond_0

    .line 448
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v1

    .line 450
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->d()I

    move-result v2

    const/16 v3, 0x12

    if-ne v2, v3, :cond_0

    .line 451
    invoke-virtual {p0, v1}, Ltv/peel/samsung/widget/a;->a(Lcom/peel/control/h;)V

    .line 457
    :cond_0
    :goto_0
    return-object v0

    .line 455
    :cond_1
    sget-object v1, Ltv/peel/samsung/widget/a;->a:Ljava/lang/String;

    const-string/jumbo v2, "room is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public h()V
    .locals 2

    .prologue
    .line 521
    iget-object v0, p0, Ltv/peel/samsung/widget/a;->b:Ltv/peel/samsung/widget/service/f;

    if-eqz v0, :cond_0

    .line 522
    iget-object v0, p0, Ltv/peel/samsung/widget/a;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Ltv/peel/samsung/widget/a;->s:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 523
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/peel/samsung/widget/a;->b:Ltv/peel/samsung/widget/service/f;

    .line 525
    :cond_0
    return-void
.end method

.method public i()Z
    .locals 3

    .prologue
    .line 557
    :try_start_0
    iget-object v0, p0, Ltv/peel/samsung/widget/a;->b:Ltv/peel/samsung/widget/service/f;

    invoke-interface {v0}, Ltv/peel/samsung/widget/service/f;->d()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 562
    :goto_0
    return v0

    .line 558
    :catch_0
    move-exception v0

    .line 559
    sget-object v1, Ltv/peel/samsung/widget/a;->a:Ljava/lang/String;

    sget-object v2, Ltv/peel/samsung/widget/a;->a:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 562
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 567
    iget-object v0, p0, Ltv/peel/samsung/widget/a;->c:Landroid/content/Context;

    const-string/jumbo v1, "widget_pref"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "is_setup_complete"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public k()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 572
    iget-object v2, p0, Ltv/peel/samsung/widget/a;->c:Landroid/content/Context;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string/jumbo v3, "setup_type"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 573
    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public l()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ltv/peel/widget/service/ChannelParcelable;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 678
    :try_start_0
    iget-object v1, p0, Ltv/peel/samsung/widget/a;->b:Ltv/peel/samsung/widget/service/f;

    if-nez v1, :cond_0

    .line 682
    :goto_0
    return-object v0

    .line 678
    :cond_0
    iget-object v1, p0, Ltv/peel/samsung/widget/a;->b:Ltv/peel/samsung/widget/service/f;

    invoke-interface {v1}, Ltv/peel/samsung/widget/service/f;->g()Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 679
    :catch_0
    move-exception v1

    .line 680
    sget-object v2, Ltv/peel/samsung/widget/a;->a:Ljava/lang/String;

    sget-object v3, Ltv/peel/samsung/widget/a;->a:Ljava/lang/String;

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public m()Z
    .locals 3

    .prologue
    .line 698
    :try_start_0
    iget-object v0, p0, Ltv/peel/samsung/widget/a;->b:Ltv/peel/samsung/widget/service/f;

    invoke-interface {v0}, Ltv/peel/samsung/widget/service/f;->f()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 702
    :goto_0
    return v0

    .line 699
    :catch_0
    move-exception v0

    .line 700
    sget-object v1, Ltv/peel/samsung/widget/a;->a:Ljava/lang/String;

    sget-object v2, Ltv/peel/samsung/widget/a;->a:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 702
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()[Ltv/peel/widget/service/DeviceParcelable;
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v5, 0x0

    const/4 v7, 0x0

    .line 724
    invoke-static {}, Lcom/peel/content/a;->a()Lcom/peel/data/ContentRoom;

    move-result-object v0

    .line 727
    if-eqz v0, :cond_3

    .line 728
    invoke-virtual {v0}, Lcom/peel/data/ContentRoom;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ltv/peel/samsung/widget/a;->a(Ljava/lang/String;)Lcom/peel/control/a;

    move-result-object v0

    .line 729
    invoke-virtual {v0, v7}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v4

    .line 730
    if-nez v4, :cond_4

    .line 731
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 733
    invoke-virtual {v0}, Lcom/peel/control/a;->e()[Lcom/peel/control/h;

    move-result-object v9

    array-length v10, v9

    move v6, v7

    :goto_0
    if-ge v6, v10, :cond_2

    aget-object v4, v9, v6

    .line 734
    const/16 v0, 0xd

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->d()I

    move-result v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x5

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->d()I

    move-result v1

    if-eq v0, v1, :cond_0

    const/16 v0, 0xe

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->d()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 735
    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    if-ne v11, v0, :cond_1

    .line 736
    :cond_0
    new-instance v0, Ltv/peel/widget/service/DeviceParcelable;

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->d()I

    move-result v2

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->d()I

    move-result v3

    invoke-static {v3}, Lcom/peel/util/bx;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Ltv/peel/widget/service/DeviceParcelable;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 733
    :cond_1
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 739
    :cond_2
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v5, v0, [Ltv/peel/widget/service/DeviceParcelable;

    .line 740
    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 746
    :cond_3
    :goto_1
    return-object v5

    .line 742
    :cond_4
    new-array v6, v11, [Ltv/peel/widget/service/DeviceParcelable;

    .line 743
    new-instance v0, Ltv/peel/widget/service/DeviceParcelable;

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->d()I

    move-result v2

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->d()I

    move-result v3

    invoke-static {v3}, Lcom/peel/util/bx;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Ltv/peel/widget/service/DeviceParcelable;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v0, v6, v7

    move-object v5, v6

    goto :goto_1
.end method

.method public o()[Lcom/peel/control/h;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 751
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    .line 754
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Ltv/peel/samsung/widget/a;->a(Ljava/lang/String;)Lcom/peel/control/a;

    move-result-object v1

    .line 756
    :goto_0
    if-nez v1, :cond_0

    .line 757
    :goto_1
    return-object v0

    :cond_0
    invoke-virtual {v1}, Lcom/peel/control/a;->e()[Lcom/peel/control/h;

    move-result-object v0

    goto :goto_1

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method
