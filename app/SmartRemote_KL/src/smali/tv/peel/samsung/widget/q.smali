.class public Ltv/peel/samsung/widget/q;
.super Ljava/lang/Object;

# interfaces
.implements Ltv/peel/widget/h;


# instance fields
.field private a:Landroid/widget/RemoteViews;

.field private b:Z

.field private c:Landroid/content/Context;

.field private d:Ltv/peel/widget/util/a;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/peel/samsung/widget/q;->b:Z

    return-void
.end method

.method private a(Landroid/widget/RemoteViews;[Lcom/peel/control/h;)V
    .locals 8

    .prologue
    .line 223
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 224
    const/4 v0, 0x0

    .line 226
    if-eqz p2, :cond_b

    array-length v1, p2

    if-lez v1, :cond_b

    .line 227
    array-length v1, p2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v2, p2, v0

    .line 228
    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/g;->d()I

    move-result v3

    .line 230
    const/4 v4, 0x6

    if-ne v3, v4, :cond_0

    .line 227
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 233
    :cond_0
    invoke-interface {v7, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 235
    :cond_1
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    move v6, v0

    .line 238
    :goto_2
    const v0, 0x7f0a01b1

    const-string/jumbo v1, "setBackgroundResource"

    const v2, 0x7f020479

    invoke-virtual {p1, v0, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 239
    const v0, 0x7f0a01b2

    const-string/jumbo v1, "setBackgroundResource"

    const v2, 0x7f020479

    invoke-virtual {p1, v0, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 240
    const v0, 0x7f0a01b3

    const-string/jumbo v1, "setBackgroundResource"

    const v2, 0x7f020479

    invoke-virtual {p1, v0, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 241
    const v1, 0x7f0a01b1

    const v2, 0x7f020460

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setTextViewCompoundDrawables(IIIII)V

    .line 242
    const v1, 0x7f0a01b2

    const v2, 0x7f020460

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setTextViewCompoundDrawables(IIIII)V

    .line 243
    const v1, 0x7f0a01b3

    const v2, 0x7f020460

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setTextViewCompoundDrawables(IIIII)V

    .line 245
    if-nez v6, :cond_2

    .line 246
    const v0, 0x7f0a01b1

    const/16 v1, 0x8

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 247
    const v0, 0x7f0a01b2

    const/16 v1, 0x8

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 248
    const v0, 0x7f0a01b3

    const/16 v1, 0x8

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 274
    :goto_3
    return-void

    .line 249
    :cond_2
    const/4 v0, 0x1

    if-ne v6, v0, :cond_4

    .line 250
    const v0, 0x7f0a01b1

    const/4 v1, 0x4

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 251
    const v0, 0x7f0a01b2

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 252
    const v0, 0x7f0a01b3

    const/4 v1, 0x4

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 253
    const v1, 0x7f0a01b2

    iget-object v2, p0, Ltv/peel/samsung/widget/q;->c:Landroid/content/Context;

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    invoke-static {v2, v0}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 254
    iget-object v1, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a01b2

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-static {v0}, Ltv/peel/samsung/widget/q;->a(Lcom/peel/control/h;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/16 v0, 0x28

    :goto_4
    const/4 v3, 0x0

    invoke-virtual {v1, p1, v2, v0, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;III)V

    goto :goto_3

    :cond_3
    const/16 v0, 0x20

    goto :goto_4

    .line 255
    :cond_4
    const/4 v0, 0x2

    if-ne v6, v0, :cond_7

    .line 256
    const v0, 0x7f0a01b1

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 257
    const v0, 0x7f0a01b2

    const/4 v1, 0x4

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 258
    const v0, 0x7f0a01b3

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 259
    const v1, 0x7f0a01b1

    iget-object v2, p0, Ltv/peel/samsung/widget/q;->c:Landroid/content/Context;

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    invoke-static {v2, v0}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 260
    const v1, 0x7f0a01b3

    iget-object v2, p0, Ltv/peel/samsung/widget/q;->c:Landroid/content/Context;

    const/4 v0, 0x1

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    invoke-static {v2, v0}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 261
    iget-object v1, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a01b1

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-static {v0}, Ltv/peel/samsung/widget/q;->a(Lcom/peel/control/h;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/16 v0, 0x28

    :goto_5
    const/4 v3, 0x0

    invoke-virtual {v1, p1, v2, v0, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;III)V

    .line 262
    iget-object v1, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a01b3

    const/4 v0, 0x1

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-static {v0}, Ltv/peel/samsung/widget/q;->a(Lcom/peel/control/h;)Z

    move-result v0

    if-eqz v0, :cond_6

    const/16 v0, 0x28

    :goto_6
    const/4 v3, 0x1

    invoke-virtual {v1, p1, v2, v0, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;III)V

    goto/16 :goto_3

    .line 261
    :cond_5
    const/16 v0, 0x20

    goto :goto_5

    .line 262
    :cond_6
    const/16 v0, 0x20

    goto :goto_6

    .line 264
    :cond_7
    const v0, 0x7f0a01b1

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 265
    const v0, 0x7f0a01b2

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 266
    const v0, 0x7f0a01b3

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 267
    const v1, 0x7f0a01b1

    iget-object v2, p0, Ltv/peel/samsung/widget/q;->c:Landroid/content/Context;

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    invoke-static {v2, v0}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 268
    const v1, 0x7f0a01b2

    iget-object v2, p0, Ltv/peel/samsung/widget/q;->c:Landroid/content/Context;

    const/4 v0, 0x1

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    invoke-static {v2, v0}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 269
    const v1, 0x7f0a01b3

    iget-object v2, p0, Ltv/peel/samsung/widget/q;->c:Landroid/content/Context;

    const/4 v0, 0x2

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    invoke-static {v2, v0}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 270
    iget-object v1, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a01b1

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-static {v0}, Ltv/peel/samsung/widget/q;->a(Lcom/peel/control/h;)Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0x28

    :goto_7
    const/4 v3, 0x0

    invoke-virtual {v1, p1, v2, v0, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;III)V

    .line 271
    iget-object v1, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a01b2

    const/4 v0, 0x1

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-static {v0}, Ltv/peel/samsung/widget/q;->a(Lcom/peel/control/h;)Z

    move-result v0

    if-eqz v0, :cond_9

    const/16 v0, 0x28

    :goto_8
    const/4 v3, 0x1

    invoke-virtual {v1, p1, v2, v0, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;III)V

    .line 272
    iget-object v1, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a01b3

    const/4 v0, 0x2

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-static {v0}, Ltv/peel/samsung/widget/q;->a(Lcom/peel/control/h;)Z

    move-result v0

    if-eqz v0, :cond_a

    const/16 v0, 0x28

    :goto_9
    const/4 v3, 0x2

    invoke-virtual {v1, p1, v2, v0, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;III)V

    goto/16 :goto_3

    .line 270
    :cond_8
    const/16 v0, 0x20

    goto :goto_7

    .line 271
    :cond_9
    const/16 v0, 0x20

    goto :goto_8

    .line 272
    :cond_a
    const/16 v0, 0x20

    goto :goto_9

    :cond_b
    move v6, v0

    goto/16 :goto_2
.end method

.method private static a(Lcom/peel/control/h;)Z
    .locals 2

    .prologue
    .line 406
    invoke-virtual {p0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->a()Ljava/util/Map;

    move-result-object v0

    .line 408
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    if-lez v1, :cond_0

    const-string/jumbo v1, "Power"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 409
    const/4 v0, 0x0

    .line 411
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a()Ltv/peel/widget/h;
    .locals 5

    .prologue
    const v4, 0x7f0a01f1

    .line 48
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ltv/peel/samsung/widget/q;->b:Z

    if-nez v0, :cond_0

    .line 49
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    iget-object v1, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01ce

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 50
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "is_setup_complete"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 51
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    iget-object v1, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v4, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 56
    :cond_0
    :goto_0
    return-object p0

    .line 53
    :cond_1
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    iget-object v1, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const/16 v2, 0x1e

    invoke-virtual {v0, v1, v4, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    goto :goto_0
.end method

.method public a(ILjava/lang/String;Ljava/lang/String;Z)Ltv/peel/widget/h;
    .locals 6

    .prologue
    .line 417
    iget-boolean v0, p0, Ltv/peel/samsung/widget/q;->b:Z

    if-nez v0, :cond_2

    .line 418
    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Ltv/peel/samsung/widget/q;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 420
    const v1, 0x7f0a018e

    const-string/jumbo v2, "setBackgroundResource"

    const v3, 0x7f020475

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 421
    const v1, 0x7f0a01d7

    const-string/jumbo v2, "setBackgroundResource"

    const v3, 0x7f020479

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 422
    const v1, 0x7f0a01d7

    iget-object v2, p0, Ltv/peel/samsung/widget/q;->c:Landroid/content/Context;

    const v3, 0x7f0d000a

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 423
    const v1, 0x7f0a01d7

    const v2, 0x7f020460

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setTextViewCompoundDrawables(IIIII)V

    .line 424
    const v1, 0x7f0a018d

    const-string/jumbo v2, "setBackgroundResource"

    const v3, 0x7f020448

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 425
    const v1, 0x7f0a018d

    const v2, 0x7f02005b

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setTextViewCompoundDrawables(IIIII)V

    .line 426
    const v1, 0x7f0a018d

    iget-object v2, p0, Ltv/peel/samsung/widget/q;->c:Landroid/content/Context;

    const v3, 0x7f0d0098

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 428
    const v1, 0x7f0a0188

    const v2, 0x7f020549

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 429
    const v1, 0x7f0a0189

    const v2, 0x7f020548

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 430
    const v1, 0x7f0a018a

    const v2, 0x7f020544

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 431
    const v1, 0x7f0a018b

    const v2, 0x7f020543

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 432
    const v1, 0x7f0a01b4

    iget-object v2, p0, Ltv/peel/samsung/widget/q;->c:Landroid/content/Context;

    const v3, 0x7f0d0265

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 433
    const v1, 0x7f0a01b5

    iget-object v2, p0, Ltv/peel/samsung/widget/q;->c:Landroid/content/Context;

    const v3, 0x7f0d0263

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 435
    iget-object v1, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    if-eqz v1, :cond_1

    .line 437
    iget-object v1, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a018e

    const/16 v3, 0x14

    invoke-virtual {v1, v0, v2, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 439
    iget-object v1, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a01d7

    const/16 v3, 0x28

    const-string/jumbo v4, "PowerOn"

    invoke-virtual {v1, v0, v2, v3, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 440
    iget-object v1, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a018d

    const/16 v3, 0x8

    const-string/jumbo v4, "MODE"

    invoke-virtual {v1, v0, v2, v3, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 441
    iget-object v1, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a0188

    const/16 v3, 0x8

    const-string/jumbo v4, "UP"

    invoke-virtual {v1, v0, v2, v3, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 442
    iget-object v1, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a0189

    const/16 v3, 0x8

    const-string/jumbo v4, "Down"

    invoke-virtual {v1, v0, v2, v3, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 443
    iget-object v1, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a018a

    const/16 v3, 0x8

    const-string/jumbo v4, "FAN_HIGH"

    invoke-virtual {v1, v0, v2, v3, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 444
    iget-object v1, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a018b

    const/16 v3, 0x8

    const-string/jumbo v4, "FAN_LOW"

    invoke-virtual {v1, v0, v2, v3, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 445
    if-eqz p4, :cond_0

    .line 446
    const v1, 0x7f0a018d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 447
    const v1, 0x7f0a018d

    invoke-virtual {v0, v1, p2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 451
    :cond_0
    iget-object v1, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a0188

    const/16 v3, 0x64

    const-string/jumbo v4, "UP"

    invoke-virtual {v1, v0, v2, v3, v4}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 452
    iget-object v1, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a0189

    const/16 v3, 0x64

    const-string/jumbo v4, "Down"

    invoke-virtual {v1, v0, v2, v3, v4}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 453
    iget-object v1, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a018a

    const/16 v3, 0x64

    const-string/jumbo v4, "FAN_HIGH"

    invoke-virtual {v1, v0, v2, v3, v4}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 454
    iget-object v1, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a018b

    const/16 v3, 0x64

    const-string/jumbo v4, "FAN_LOW"

    invoke-virtual {v1, v0, v2, v3, v4}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 456
    :cond_1
    iget-object v1, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01ec

    invoke-virtual {v1, v2, v0}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    .line 458
    :cond_2
    return-object p0
.end method

.method public a(IZ[Lcom/peel/control/h;)Ltv/peel/widget/h;
    .locals 11

    .prologue
    const/16 v10, 0x64

    const v9, 0x7f0a01b7

    const v8, 0x7f020448

    const/4 v2, 0x0

    const/16 v7, 0x8

    .line 279
    iget-boolean v0, p0, Ltv/peel/samsung/widget/q;->b:Z

    if-nez v0, :cond_4

    .line 281
    new-instance v3, Landroid/widget/RemoteViews;

    iget-object v0, p0, Ltv/peel/samsung/widget/q;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0, p1}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 284
    array-length v4, p3

    move v1, v2

    move v0, v2

    :goto_0
    if-ge v1, v4, :cond_2

    aget-object v5, p3, v1

    .line 285
    invoke-virtual {v5}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/g;->d()I

    move-result v5

    .line 286
    const/4 v6, 0x2

    if-eq v5, v6, :cond_0

    const/16 v6, 0x14

    if-ne v5, v6, :cond_1

    .line 287
    :cond_0
    const/4 v0, 0x1

    .line 284
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 291
    :cond_2
    if-eqz v0, :cond_5

    .line 292
    const v0, 0x7f0a0180

    invoke-virtual {v3, v0, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 293
    const v0, 0x7f0a0181

    invoke-virtual {v3, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 294
    invoke-virtual {v3, v9, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 301
    :goto_1
    const v0, 0x7f0a018e

    const-string/jumbo v1, "setBackgroundResource"

    const v2, 0x7f020475

    invoke-virtual {v3, v0, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 302
    const-string/jumbo v0, "setBackgroundResource"

    invoke-virtual {v3, v9, v0, v8}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 303
    const v0, 0x7f020478

    invoke-virtual {v3, v9, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 304
    const v0, 0x7f0a01b8

    const-string/jumbo v1, "setBackgroundResource"

    invoke-virtual {v3, v0, v1, v8}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 305
    const v0, 0x7f0a01b8

    const v1, 0x7f020477

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 306
    const v0, 0x7f0a0182

    const-string/jumbo v1, "setBackgroundResource"

    invoke-virtual {v3, v0, v1, v8}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 307
    const v0, 0x7f0a0182

    const v1, 0x7f020474

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 308
    const v0, 0x7f0a0183

    const-string/jumbo v1, "setBackgroundResource"

    invoke-virtual {v3, v0, v1, v8}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 309
    const v0, 0x7f0a0183

    const v1, 0x7f020470

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 310
    const v0, 0x7f0a0184

    const-string/jumbo v1, "setBackgroundResource"

    invoke-virtual {v3, v0, v1, v8}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 311
    const v0, 0x7f0a0184

    const v1, 0x7f02046f

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 313
    const v0, 0x7f0a0188

    const v1, 0x7f020549

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 314
    const v0, 0x7f0a0189

    const v1, 0x7f020548

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 315
    const v0, 0x7f0a018a

    const v1, 0x7f020544

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 316
    const v0, 0x7f0a018b

    const v1, 0x7f020543

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 317
    const v0, 0x7f0a01b4

    iget-object v1, p0, Ltv/peel/samsung/widget/q;->c:Landroid/content/Context;

    const v2, 0x7f0d0262

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 318
    const v0, 0x7f0a01b5

    iget-object v1, p0, Ltv/peel/samsung/widget/q;->c:Landroid/content/Context;

    const v2, 0x7f0d0261

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 320
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    if-eqz v0, :cond_3

    .line 321
    invoke-direct {p0, v3, p3}, Ltv/peel/samsung/widget/q;->a(Landroid/widget/RemoteViews;[Lcom/peel/control/h;)V

    .line 324
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const v1, 0x7f0a018e

    const/16 v2, 0x14

    const-string/jumbo v4, "launchApp"

    invoke-virtual {v0, v3, v1, v2, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 328
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const v1, 0x7f0a0183

    const-string/jumbo v2, "Channel_Up"

    invoke-virtual {v0, v3, v1, v7, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 329
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const v1, 0x7f0a0184

    const-string/jumbo v2, "Channel_Down"

    invoke-virtual {v0, v3, v1, v7, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 330
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const v1, 0x7f0a0182

    const-string/jumbo v2, "Fast_Forward"

    invoke-virtual {v0, v3, v1, v7, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 331
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const v1, 0x7f0a01b8

    const-string/jumbo v2, "Mute"

    invoke-virtual {v0, v3, v1, v7, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 332
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const-string/jumbo v1, "Play"

    invoke-virtual {v0, v3, v9, v7, v1}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 333
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const v1, 0x7f0a018b

    const-string/jumbo v2, "Channel_Down"

    invoke-virtual {v0, v3, v1, v7, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 334
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const v1, 0x7f0a018a

    const-string/jumbo v2, "Channel_Up"

    invoke-virtual {v0, v3, v1, v7, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 335
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const v1, 0x7f0a0189

    const-string/jumbo v2, "Volume_Down"

    invoke-virtual {v0, v3, v1, v7, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 336
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const v1, 0x7f0a0188

    const-string/jumbo v2, "Volume_Up"

    invoke-virtual {v0, v3, v1, v7, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 339
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const v1, 0x7f0a018b

    const-string/jumbo v2, "Channel_Down"

    invoke-virtual {v0, v3, v1, v10, v2}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 340
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const v1, 0x7f0a018a

    const-string/jumbo v2, "Channel_Up"

    invoke-virtual {v0, v3, v1, v10, v2}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 341
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const v1, 0x7f0a0189

    const-string/jumbo v2, "Volume_Down"

    invoke-virtual {v0, v3, v1, v10, v2}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 342
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const v1, 0x7f0a0188

    const-string/jumbo v2, "Volume_Up"

    invoke-virtual {v0, v3, v1, v10, v2}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 344
    :cond_3
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v1, 0x7f0a01ec

    invoke-virtual {v0, v1, v3}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    .line 347
    :cond_4
    return-object p0

    .line 296
    :cond_5
    const v0, 0x7f0a0180

    invoke-virtual {v3, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 297
    const v0, 0x7f0a0181

    invoke-virtual {v3, v0, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 298
    const/4 v0, 0x4

    invoke-virtual {v3, v9, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_1
.end method

.method public a(IZ[Lcom/peel/control/h;Z)Ltv/peel/widget/h;
    .locals 9

    .prologue
    const v8, 0x7f0a0186

    const v7, 0x7f0a0185

    const v6, 0x7f0a01b7

    const v4, 0x7f020448

    const/16 v5, 0x8

    .line 353
    iget-boolean v0, p0, Ltv/peel/samsung/widget/q;->b:Z

    if-nez v0, :cond_2

    .line 355
    new-instance v1, Landroid/widget/RemoteViews;

    iget-object v0, p0, Ltv/peel/samsung/widget/q;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0, p1}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 356
    const/4 v0, 0x4

    invoke-virtual {v1, v6, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 357
    const v0, 0x7f0a018e

    const-string/jumbo v2, "setBackgroundResource"

    const v3, 0x7f020475

    invoke-virtual {v1, v0, v2, v3}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 358
    const-string/jumbo v0, "setBackgroundResource"

    invoke-virtual {v1, v6, v0, v4}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 359
    const v0, 0x7f020478

    invoke-virtual {v1, v6, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 360
    const v0, 0x7f0a01b8

    const-string/jumbo v2, "setBackgroundResource"

    invoke-virtual {v1, v0, v2, v4}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 361
    const v0, 0x7f0a01b8

    const v2, 0x7f020477

    invoke-virtual {v1, v0, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 362
    const-string/jumbo v0, "setBackgroundResource"

    invoke-virtual {v1, v7, v0, v4}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 363
    const v0, 0x7f02047b

    invoke-virtual {v1, v7, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 364
    const-string/jumbo v0, "setBackgroundResource"

    invoke-virtual {v1, v8, v0, v4}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 365
    const v0, 0x7f020474

    invoke-virtual {v1, v8, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 366
    const v0, 0x7f0a0187

    const-string/jumbo v2, "setBackgroundResource"

    invoke-virtual {v1, v0, v2, v4}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 367
    const v0, 0x7f0a0187

    const v2, 0x7f020478

    invoke-virtual {v1, v0, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 369
    const v0, 0x7f0a0188

    const v2, 0x7f020549

    invoke-virtual {v1, v0, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 370
    const v0, 0x7f0a0189

    const v2, 0x7f020548

    invoke-virtual {v1, v0, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 371
    const v0, 0x7f0a01b4

    iget-object v2, p0, Ltv/peel/samsung/widget/q;->c:Landroid/content/Context;

    const v3, 0x7f0d0262

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 373
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    if-eqz v0, :cond_1

    .line 374
    invoke-direct {p0, v1, p3}, Ltv/peel/samsung/widget/q;->a(Landroid/widget/RemoteViews;[Lcom/peel/control/h;)V

    .line 376
    const-string/jumbo v0, "Play"

    .line 377
    if-eqz p4, :cond_0

    .line 378
    const-string/jumbo v0, "Select"

    .line 384
    :cond_0
    iget-object v2, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const v3, 0x7f0a018e

    const/16 v4, 0x14

    invoke-virtual {v2, v1, v3, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 387
    iget-object v2, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const v3, 0x7f0a01b8

    const-string/jumbo v4, "Mute"

    invoke-virtual {v2, v1, v3, v5, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 388
    iget-object v2, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    invoke-virtual {v2, v1, v6, v5, v0}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 389
    iget-object v2, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const v3, 0x7f0a0187

    invoke-virtual {v2, v1, v3, v5, v0}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 390
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const-string/jumbo v2, "Fast_Forward"

    invoke-virtual {v0, v1, v8, v5, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 391
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const-string/jumbo v2, "Rewind"

    invoke-virtual {v0, v1, v7, v5, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 392
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a0189

    const-string/jumbo v3, "Volume_Down"

    invoke-virtual {v0, v1, v2, v5, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 393
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a0188

    const-string/jumbo v3, "Volume_Up"

    invoke-virtual {v0, v1, v2, v5, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 396
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a0189

    const/16 v3, 0x64

    const-string/jumbo v4, "Volume_Down"

    invoke-virtual {v0, v1, v2, v3, v4}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 397
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a0188

    const/16 v3, 0x64

    const-string/jumbo v4, "Volume_Up"

    invoke-virtual {v0, v1, v2, v3, v4}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 399
    :cond_1
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01ec

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    .line 402
    :cond_2
    return-object p0
.end method

.method public a(Landroid/content/Context;ILtv/peel/widget/util/a;)Ltv/peel/widget/h;
    .locals 2

    .prologue
    .line 39
    iput-object p1, p0, Ltv/peel/samsung/widget/q;->c:Landroid/content/Context;

    .line 40
    iput-object p3, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    .line 41
    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Ltv/peel/samsung/widget/q;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    .line 42
    return-object p0
.end method

.method public a(Landroid/content/Context;ILtv/peel/widget/util/a;Ljava/lang/String;Z)Ltv/peel/widget/h;
    .locals 0

    .prologue
    .line 61
    return-object p0
.end method

.method public a(Ljava/lang/String;[Ljava/lang/String;Z)Ltv/peel/widget/h;
    .locals 9

    .prologue
    const/16 v8, 0xc9

    const/16 v7, 0x2f

    const/16 v6, 0x92

    const/4 v1, 0x0

    const/16 v5, 0xf5

    .line 139
    if-eqz p2, :cond_0

    array-length v0, p2

    if-lez v0, :cond_0

    .line 140
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01ee

    invoke-virtual {v0, v2, p1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 141
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v3, 0x7f0a01ce

    const/4 v4, 0x3

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 143
    array-length v0, p2

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 145
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01ed

    const-string/jumbo v3, "setEnabled"

    invoke-virtual {v0, v2, v3, v1}, Landroid/widget/RemoteViews;->setBoolean(ILjava/lang/String;Z)V

    .line 146
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01ee

    const/16 v3, 0x7f

    const/16 v4, 0x89

    invoke-static {v3, v4, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 150
    :cond_0
    if-eqz p3, :cond_1

    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    array-length v0, p2

    const/4 v2, 0x1

    if-le v0, v2, :cond_1

    .line 151
    iget-object v2, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v3, 0x7f0a01f3

    if-eqz p3, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v2, v3, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 152
    array-length v0, p2

    packed-switch v0, :pswitch_data_0

    .line 219
    :cond_1
    :goto_1
    return-object p0

    .line 151
    :cond_2
    const/16 v0, 0x8

    goto :goto_0

    .line 154
    :pswitch_0
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v3, 0x7f0a01fa

    const/16 v4, 0x1c

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 155
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01fa

    const/4 v3, 0x6

    aget-object v3, p2, v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 156
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01fa

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 157
    const/4 v0, 0x6

    aget-object v0, p2, v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 158
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v3, 0x7f0a01fa

    invoke-static {v7, v6, v8}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;II)V

    .line 163
    :goto_2
    :pswitch_1
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v3, 0x7f0a01f9

    const/16 v4, 0x1b

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 164
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01f9

    const/4 v3, 0x5

    aget-object v3, p2, v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 165
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01f9

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 166
    const/4 v0, 0x5

    aget-object v0, p2, v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 167
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v3, 0x7f0a01f9

    invoke-static {v7, v6, v8}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;II)V

    .line 172
    :goto_3
    :pswitch_2
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v3, 0x7f0a01f8

    const/16 v4, 0x1a

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 173
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01f8

    const/4 v3, 0x4

    aget-object v3, p2, v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 174
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01f8

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 175
    const/4 v0, 0x4

    aget-object v0, p2, v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 176
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v3, 0x7f0a01f8

    invoke-static {v7, v6, v8}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;II)V

    .line 181
    :goto_4
    :pswitch_3
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v3, 0x7f0a01f7

    const/16 v4, 0x19

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 182
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01f7

    const/4 v3, 0x3

    aget-object v3, p2, v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 183
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01f7

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 184
    const/4 v0, 0x3

    aget-object v0, p2, v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 185
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v3, 0x7f0a01f7

    invoke-static {v7, v6, v8}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;II)V

    .line 190
    :goto_5
    :pswitch_4
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v3, 0x7f0a01f6

    const/16 v4, 0x18

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 191
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01f6

    const/4 v3, 0x2

    aget-object v3, p2, v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 192
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01f6

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 193
    const/4 v0, 0x2

    aget-object v0, p2, v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 194
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v3, 0x7f0a01f6

    invoke-static {v7, v6, v8}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;II)V

    .line 199
    :goto_6
    :pswitch_5
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v3, 0x7f0a01f5

    const/16 v4, 0x17

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 200
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01f5

    const/4 v3, 0x1

    aget-object v3, p2, v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 201
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01f5

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 202
    const/4 v0, 0x1

    aget-object v0, p2, v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 203
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v3, 0x7f0a01f5

    invoke-static {v7, v6, v8}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;II)V

    .line 208
    :goto_7
    :pswitch_6
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v3, 0x7f0a01f4

    const/16 v4, 0x16

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 209
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01f4

    aget-object v3, p2, v1

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 210
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01f4

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 211
    aget-object v0, p2, v1

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 212
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    iget-object v1, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01f4

    invoke-static {v7, v6, v8}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;II)V

    goto/16 :goto_1

    .line 160
    :cond_3
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v3, 0x7f0a01fa

    invoke-static {v5, v5, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;II)V

    goto/16 :goto_2

    .line 169
    :cond_4
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v3, 0x7f0a01f9

    invoke-static {v5, v5, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;II)V

    goto/16 :goto_3

    .line 178
    :cond_5
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v3, 0x7f0a01f8

    invoke-static {v5, v5, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;II)V

    goto/16 :goto_4

    .line 187
    :cond_6
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v3, 0x7f0a01f7

    invoke-static {v5, v5, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;II)V

    goto/16 :goto_5

    .line 196
    :cond_7
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v3, 0x7f0a01f6

    invoke-static {v5, v5, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;II)V

    goto/16 :goto_6

    .line 205
    :cond_8
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v3, 0x7f0a01f5

    invoke-static {v5, v5, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;II)V

    goto/16 :goto_7

    .line 214
    :cond_9
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    iget-object v1, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01f4

    invoke-static {v5, v5, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;II)V

    goto/16 :goto_1

    .line 152
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Z)Ltv/peel/widget/h;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const v6, 0x7f0a01f0

    const v5, 0x7f0a01ef

    const/4 v3, 0x0

    const v4, 0x7f0a004d

    .line 67
    iput-boolean p1, p0, Ltv/peel/samsung/widget/q;->b:Z

    .line 68
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->c:Landroid/content/Context;

    const-string/jumbo v1, "widget_pref"

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 71
    iget-boolean v1, p0, Ltv/peel/samsung/widget/q;->b:Z

    if-nez v1, :cond_3

    .line 72
    iget-object v1, p0, Ltv/peel/samsung/widget/q;->c:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v2, "is_setup_complete"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_1

    .line 73
    iget-object v1, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    iget-object v2, p0, Ltv/peel/samsung/widget/q;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0236

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v4, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 78
    :goto_0
    const-string/jumbo v1, "clear_mode"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 79
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    iget-object v1, p0, Ltv/peel/samsung/widget/q;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d020f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v6, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 80
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    iget-object v1, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const/16 v2, 0x1f

    invoke-virtual {v0, v1, v5, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 99
    :cond_0
    :goto_1
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    iget-object v1, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01ed

    const/4 v3, 0x5

    invoke-virtual {v0, v1, v2, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 102
    return-object p0

    .line 75
    :cond_1
    iget-object v1, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    iget-object v2, p0, Ltv/peel/samsung/widget/q;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d038e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v4, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_0

    .line 82
    :cond_2
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    iget-object v1, p0, Ltv/peel/samsung/widget/q;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d021d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v6, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 83
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    iget-object v1, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    invoke-virtual {v0, v1, v5, v7}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    goto :goto_1

    .line 86
    :cond_3
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    iget-object v1, p0, Ltv/peel/samsung/widget/q;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d03ee

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 87
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    iget-object v1, p0, Ltv/peel/samsung/widget/q;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0238

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v6, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 88
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    iget-object v1, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    invoke-virtual {v0, v1, v5, v7}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 89
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v1, 0x7f0a01eb

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 91
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "is_setup_complete"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_4

    .line 92
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    iget-object v1, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v4, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    goto/16 :goto_1

    .line 94
    :cond_4
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    iget-object v1, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const/16 v2, 0x1e

    invoke-virtual {v0, v1, v4, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    goto/16 :goto_1
.end method

.method public b()Landroid/widget/RemoteViews;
    .locals 1

    .prologue
    .line 463
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    return-object v0
.end method

.method public b(Z)Ltv/peel/widget/h;
    .locals 7

    .prologue
    const v6, 0x7f0a01f0

    const v5, 0x7f0a01ef

    const/4 v4, 0x2

    .line 108
    iput-boolean p1, p0, Ltv/peel/samsung/widget/q;->b:Z

    .line 110
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    iget-object v1, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01ed

    const/4 v3, 0x5

    invoke-virtual {v0, v1, v2, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 114
    :cond_0
    iget-boolean v0, p0, Ltv/peel/samsung/widget/q;->b:Z

    if-eqz v0, :cond_1

    .line 116
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00a7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 117
    iget-object v1, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01eb

    const/16 v3, 0x8

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 118
    iget-object v1, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    invoke-virtual {v1, v6, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 119
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    iget-object v1, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    invoke-virtual {v0, v1, v5, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 133
    :goto_0
    return-object p0

    .line 128
    :cond_1
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    iget-object v1, p0, Ltv/peel/samsung/widget/q;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d021d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v6, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 129
    iget-object v0, p0, Ltv/peel/samsung/widget/q;->d:Ltv/peel/widget/util/a;

    iget-object v1, p0, Ltv/peel/samsung/widget/q;->a:Landroid/widget/RemoteViews;

    invoke-virtual {v0, v1, v5, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    goto :goto_0
.end method
