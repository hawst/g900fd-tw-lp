.class Ltv/peel/samsung/widget/service/l;
.super Ltv/peel/samsung/widget/service/j;


# instance fields
.field final synthetic a:Ltv/peel/samsung/widget/service/QuickConnectService;


# direct methods
.method constructor <init>(Ltv/peel/samsung/widget/service/QuickConnectService;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Ltv/peel/samsung/widget/service/l;->a:Ltv/peel/samsung/widget/service/QuickConnectService;

    invoke-direct {p0}, Ltv/peel/samsung/widget/service/j;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/util/List;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ltv/peel/samsung/widget/service/Device;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    if-nez p1, :cond_0

    .line 57
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->e()[Lcom/peel/control/h;

    move-result-object v1

    move-object v12, v1

    .line 62
    :goto_0
    if-nez v12, :cond_1

    .line 63
    const/4 v1, 0x0

    .line 98
    :goto_1
    return-object v1

    .line 59
    :cond_0
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/peel/control/am;->d(Ljava/lang/String;)[Lcom/peel/control/h;

    move-result-object v1

    move-object v12, v1

    goto :goto_0

    .line 65
    :cond_1
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 67
    const/4 v1, 0x0

    move v10, v1

    :goto_2
    array-length v1, v12

    if-ge v10, v1, :cond_5

    .line 68
    aget-object v1, v12, v10

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->d()I

    move-result v13

    .line 69
    const/4 v1, 0x1

    if-ne v13, v1, :cond_4

    .line 70
    aget-object v1, v12, v10

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v14

    .line 71
    aget-object v1, v12, v10

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v15

    .line 72
    move-object/from16 v0, p0

    iget-object v1, v0, Ltv/peel/samsung/widget/service/l;->a:Ltv/peel/samsung/widget/service/QuickConnectService;

    invoke-virtual {v1}, Ltv/peel/samsung/widget/service/QuickConnectService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v13}, Lcom/peel/util/bx;->d(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v16

    .line 74
    aget-object v1, v12, v10

    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->a()Ljava/util/Map;

    move-result-object v1

    .line 75
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 76
    aget-object v2, v12, v10

    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->h()I

    move-result v4

    .line 77
    aget-object v2, v12, v10

    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->d()I

    move-result v5

    .line 78
    aget-object v2, v12, v10

    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v7

    .line 80
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :goto_3
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Ljava/util/Map$Entry;

    .line 81
    new-instance v1, Ltv/peel/samsung/widget/service/Command;

    .line 82
    invoke-interface {v8}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 83
    invoke-interface {v8}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    const-string/jumbo v6, "rank"

    invoke-interface {v3, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    const/4 v6, -0x1

    .line 88
    invoke-interface {v8}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Map;

    const-string/jumbo v9, "is_input"

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_2

    const/4 v8, 0x1

    :goto_4
    const/4 v9, 0x0

    invoke-direct/range {v1 .. v9}, Ltv/peel/samsung/widget/service/Command;-><init>(Ljava/lang/String;IIIILjava/lang/String;ZI)V

    .line 81
    move-object/from16 v0, v17

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 88
    :cond_2
    const/4 v8, 0x0

    goto :goto_4

    .line 94
    :cond_3
    new-instance v1, Ltv/peel/samsung/widget/service/Device;

    invoke-static {v13}, Lcom/peel/util/bx;->a(I)Ljava/lang/String;

    move-result-object v4

    move-object v2, v14

    move v3, v13

    move-object v5, v15

    move-object/from16 v6, v16

    move-object/from16 v7, v17

    invoke-direct/range {v1 .. v7}, Ltv/peel/samsung/widget/service/Device;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    :cond_4
    add-int/lit8 v1, v10, 0x1

    move v10, v1

    goto/16 :goto_2

    :cond_5
    move-object v1, v11

    .line 98
    goto/16 :goto_1
.end method

.method public a()Ltv/peel/samsung/widget/service/Room;
    .locals 3

    .prologue
    .line 44
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Ltv/peel/samsung/widget/service/l;->a:Ltv/peel/samsung/widget/service/QuickConnectService;

    invoke-static {v0}, Ltv/peel/samsung/widget/service/QuickConnectService;->a(Ltv/peel/samsung/widget/service/QuickConnectService;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "\n**** PeelControl.control.getCurrentRoom().getData().getId(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " -- "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/at;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    new-instance v0, Ltv/peel/samsung/widget/service/Room;

    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v2}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/at;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ltv/peel/samsung/widget/service/Room;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    :goto_0
    return-object v0

    .line 48
    :cond_0
    iget-object v0, p0, Ltv/peel/samsung/widget/service/l;->a:Ltv/peel/samsung/widget/service/QuickConnectService;

    invoke-static {v0}, Ltv/peel/samsung/widget/service/QuickConnectService;->a(Ltv/peel/samsung/widget/service/QuickConnectService;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "\n**** current room is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ltv/peel/samsung/widget/service/Device;Ltv/peel/samsung/widget/service/Command;)V
    .locals 4

    .prologue
    .line 117
    iget-object v0, p0, Ltv/peel/samsung/widget/service/l;->a:Ltv/peel/samsung/widget/service/QuickConnectService;

    invoke-static {v0}, Ltv/peel/samsung/widget/service/QuickConnectService;->a(Ltv/peel/samsung/widget/service/QuickConnectService;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "sendRemocon: peelcontrol.control is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    if-eqz v0, :cond_1

    .line 121
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {p2}, Ltv/peel/samsung/widget/service/Device;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/control/am;->c(Ljava/lang/String;)Lcom/peel/control/h;

    move-result-object v0

    .line 123
    if-eqz v0, :cond_0

    .line 124
    invoke-virtual {p3}, Ltv/peel/samsung/widget/service/Command;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/peel/control/h;->c(Ljava/lang/String;)Z

    move-result v0

    .line 125
    iget-object v1, p0, Ltv/peel/samsung/widget/service/l;->a:Ltv/peel/samsung/widget/service/QuickConnectService;

    invoke-static {v1}, Ltv/peel/samsung/widget/service/QuickConnectService;->a(Ltv/peel/samsung/widget/service/QuickConnectService;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "IR Sent: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " for device: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Ltv/peel/samsung/widget/service/Device;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " w/ cmd: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Ltv/peel/samsung/widget/service/Command;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    :goto_0
    return-void

    .line 127
    :cond_0
    iget-object v0, p0, Ltv/peel/samsung/widget/service/l;->a:Ltv/peel/samsung/widget/service/QuickConnectService;

    invoke-static {v0}, Ltv/peel/samsung/widget/service/QuickConnectService;->a(Ltv/peel/samsung/widget/service/QuickConnectService;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "sendRemocon: not able to find device by room: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " -- device: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Ltv/peel/samsung/widget/service/Device;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 130
    :cond_1
    iget-object v0, p0, Ltv/peel/samsung/widget/service/l;->a:Ltv/peel/samsung/widget/service/QuickConnectService;

    invoke-static {v0}, Ltv/peel/samsung/widget/service/QuickConnectService;->a(Ltv/peel/samsung/widget/service/QuickConnectService;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "sendRemocon: peelcontrol.control is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public b()[Ltv/peel/samsung/widget/service/Room;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 103
    sget-object v1, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v1}, Lcom/peel/control/am;->c()[Lcom/peel/control/RoomControl;

    move-result-object v3

    .line 104
    array-length v1, v3

    new-array v4, v1, [Ltv/peel/samsung/widget/service/Room;

    .line 107
    array-length v5, v3

    move v1, v0

    :goto_0
    if-ge v0, v5, :cond_0

    aget-object v2, v3, v0

    .line 108
    new-instance v6, Ltv/peel/samsung/widget/service/Room;

    invoke-virtual {v2}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v7

    invoke-virtual {v7}, Lcom/peel/data/at;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2}, Lcom/peel/control/RoomControl;->a()Lcom/peel/data/at;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/at;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v6, v7, v2}, Ltv/peel/samsung/widget/service/Room;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    add-int/lit8 v2, v1, 0x1

    aput-object v6, v4, v1

    .line 107
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_0

    .line 112
    :cond_0
    return-object v4
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 141
    sget-object v0, Lcom/peel/control/am;->b:Lcom/peel/control/am;

    invoke-virtual {v0}, Lcom/peel/control/am;->d()Lcom/peel/control/RoomControl;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 142
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
