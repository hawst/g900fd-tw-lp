.class Ltv/peel/samsung/widget/v;
.super Ljava/lang/Object;

# interfaces
.implements Ltv/peel/widget/util/c;


# instance fields
.field final synthetic a:Ltv/peel/samsung/widget/NotiRemoteService;

.field private b:Z


# direct methods
.method public constructor <init>(Ltv/peel/samsung/widget/NotiRemoteService;Z)V
    .locals 1

    .prologue
    .line 116
    iput-object p1, p0, Ltv/peel/samsung/widget/v;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/peel/samsung/widget/v;->b:Z

    .line 117
    iput-boolean p2, p0, Ltv/peel/samsung/widget/v;->b:Z

    .line 118
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;I)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 122
    invoke-static {}, Ltv/peel/samsung/widget/NotiRemoteService;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onServiceOn() mServiceState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Ltv/peel/samsung/widget/v;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v2}, Ltv/peel/samsung/widget/NotiRemoteService;->a(Ltv/peel/samsung/widget/NotiRemoteService;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    iget-object v0, p0, Ltv/peel/samsung/widget/v;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v0}, Ltv/peel/samsung/widget/NotiRemoteService;->a(Ltv/peel/samsung/widget/NotiRemoteService;)I

    move-result v0

    if-eq v0, v3, :cond_0

    .line 125
    iget-object v0, p0, Ltv/peel/samsung/widget/v;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v0, v3}, Ltv/peel/samsung/widget/NotiRemoteService;->a(Ltv/peel/samsung/widget/NotiRemoteService;I)I

    .line 127
    iget-boolean v0, p0, Ltv/peel/samsung/widget/v;->b:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 128
    iget-object v0, p0, Ltv/peel/samsung/widget/v;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v0, v3}, Ltv/peel/samsung/widget/NotiRemoteService;->a(Ltv/peel/samsung/widget/NotiRemoteService;I)I

    .line 129
    iget-object v0, p0, Ltv/peel/samsung/widget/v;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    const-string/jumbo v1, "com.peel.widget.notification.SERVICE_BINDED"

    invoke-static {v0, p1, v1}, Ltv/peel/samsung/widget/NotiRemoteService;->a(Ltv/peel/samsung/widget/NotiRemoteService;Landroid/content/Context;Ljava/lang/String;)V

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 131
    :cond_1
    iget-object v0, p0, Ltv/peel/samsung/widget/v;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Ltv/peel/samsung/widget/NotiRemoteService;->a(Ltv/peel/samsung/widget/NotiRemoteService;I)I

    goto :goto_0
.end method
