.class Ltv/peel/samsung/widget/t;
.super Ljava/lang/Object;

# interfaces
.implements Ltv/peel/widget/util/b;


# instance fields
.field final synthetic a:Ltv/peel/samsung/widget/NotiRemoteService;


# direct methods
.method private constructor <init>(Ltv/peel/samsung/widget/NotiRemoteService;)V
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Ltv/peel/samsung/widget/NotiRemoteService;Ltv/peel/samsung/widget/s;)V
    .locals 0

    .prologue
    .line 176
    invoke-direct {p0, p1}, Ltv/peel/samsung/widget/t;-><init>(Ltv/peel/samsung/widget/NotiRemoteService;)V

    return-void
.end method

.method private a(Ljava/util/TreeMap;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 309
    const-string/jumbo v1, "Play"

    .line 310
    iget-object v0, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v0}, Ltv/peel/samsung/widget/NotiRemoteService;->f(Ltv/peel/samsung/widget/NotiRemoteService;)Ltv/peel/widget/g;

    move-result-object v0

    if-eqz v0, :cond_2

    if-eqz p1, :cond_2

    .line 311
    iget-object v0, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v0}, Ltv/peel/samsung/widget/NotiRemoteService;->h(Ltv/peel/samsung/widget/NotiRemoteService;)Lcom/peel/control/a;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v0

    .line 312
    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v2

    .line 313
    const-string/jumbo v0, "Play"

    .line 314
    if-eqz v2, :cond_2

    .line 315
    iget-object v1, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v1}, Ltv/peel/samsung/widget/NotiRemoteService;->i(Ltv/peel/samsung/widget/NotiRemoteService;)Ljava/util/TreeMap;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/util/TreeMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 316
    iget-object v0, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v0}, Ltv/peel/samsung/widget/NotiRemoteService;->i(Ltv/peel/samsung/widget/NotiRemoteService;)Ljava/util/TreeMap;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 318
    :cond_0
    const-string/jumbo v1, "Play"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 319
    const-string/jumbo v0, "Pause"

    .line 323
    :goto_0
    iget-object v1, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v1}, Ltv/peel/samsung/widget/NotiRemoteService;->i(Ltv/peel/samsung/widget/NotiRemoteService;)Ljava/util/TreeMap;

    move-result-object v1

    invoke-virtual {v1, v2, v0}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 326
    :goto_1
    return-object v0

    .line 321
    :cond_1
    const-string/jumbo v0, "Play"

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method public a(Landroid/content/Intent;ILjava/lang/String;I)V
    .locals 7

    .prologue
    const/16 v4, 0x15

    const/4 v5, -0x1

    const/16 v3, 0x1770

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 179
    .line 181
    iget-object v2, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v2}, Ltv/peel/samsung/widget/NotiRemoteService;->b(Ltv/peel/samsung/widget/NotiRemoteService;)Z

    move-result v2

    if-ne v2, v1, :cond_a

    const/4 v2, 0x5

    if-eq p2, v2, :cond_a

    .line 182
    iget-object v2, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v2, v0}, Ltv/peel/samsung/widget/NotiRemoteService;->a(Ltv/peel/samsung/widget/NotiRemoteService;Z)Z

    move v6, v1

    .line 186
    :goto_0
    packed-switch p2, :pswitch_data_0

    .line 283
    :pswitch_0
    iget-object v0, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v0}, Ltv/peel/samsung/widget/NotiRemoteService;->f(Ltv/peel/samsung/widget/NotiRemoteService;)Ltv/peel/widget/g;

    move-result-object v0

    invoke-static {}, Ltv/peel/samsung/widget/NotiRemoteService;->b()Ltv/peel/samsung/widget/a;

    move-result-object v2

    iget-object v3, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v3}, Ltv/peel/samsung/widget/NotiRemoteService;->h(Ltv/peel/samsung/widget/NotiRemoteService;)Lcom/peel/control/a;

    move-result-object v3

    invoke-virtual {v0, v2, p3, v3}, Ltv/peel/widget/g;->a(Ltv/peel/widget/b;Ljava/lang/String;Lcom/peel/control/a;)V

    .line 287
    :cond_0
    :goto_1
    if-ne v6, v1, :cond_1

    .line 288
    iget-object v0, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v0}, Ltv/peel/samsung/widget/NotiRemoteService;->l(Ltv/peel/samsung/widget/NotiRemoteService;)V

    .line 290
    :cond_1
    return-void

    .line 189
    :pswitch_1
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    const/16 v2, 0x440

    const-string/jumbo v4, "NOTIFICATION"

    invoke-virtual/range {v0 .. v5}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;I)V

    .line 190
    iget-object v0, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v0}, Ltv/peel/samsung/widget/NotiRemoteService;->c(Ltv/peel/samsung/widget/NotiRemoteService;)V

    .line 193
    iget-object v0, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-virtual {v0}, Ltv/peel/samsung/widget/NotiRemoteService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v2, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-virtual {v2}, Ltv/peel/samsung/widget/NotiRemoteService;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 194
    const/high16 v2, 0x30000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 195
    const-string/jumbo v2, "context_id"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 196
    const-string/jumbo v2, "from"

    const-string/jumbo v3, "NOTIFICATION"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 197
    iget-object v2, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-virtual {v2, v0}, Ltv/peel/samsung/widget/NotiRemoteService;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 201
    :pswitch_2
    iget-object v2, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    iget-object v3, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v3}, Ltv/peel/samsung/widget/NotiRemoteService;->d(Ltv/peel/samsung/widget/NotiRemoteService;)Z

    move-result v3

    if-nez v3, :cond_2

    move v0, v1

    :cond_2
    invoke-static {v2, v0}, Ltv/peel/samsung/widget/NotiRemoteService;->b(Ltv/peel/samsung/widget/NotiRemoteService;Z)V

    move v6, v1

    .line 203
    goto :goto_1

    .line 205
    :pswitch_3
    iget-object v2, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v2}, Ltv/peel/samsung/widget/NotiRemoteService;->e(Ltv/peel/samsung/widget/NotiRemoteService;)V

    .line 206
    iget-object v2, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    const-string/jumbo v3, "widget_pref"

    invoke-virtual {v2, v3, v0}, Ltv/peel/samsung/widget/NotiRemoteService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string/jumbo v3, "notification"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 207
    iget-object v2, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    const-string/jumbo v3, "widget_pref"

    invoke-virtual {v2, v3, v0}, Ltv/peel/samsung/widget/NotiRemoteService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string/jumbo v3, "user_cleared"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 208
    iget-object v2, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    const-string/jumbo v3, "widget_pref"

    invoke-virtual {v2, v3, v0}, Ltv/peel/samsung/widget/NotiRemoteService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string/jumbo v3, "boot_count"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_1

    .line 211
    :pswitch_4
    iget-object v3, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    iget-object v2, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v2}, Ltv/peel/samsung/widget/NotiRemoteService;->b(Ltv/peel/samsung/widget/NotiRemoteService;)Z

    move-result v2

    if-nez v2, :cond_3

    move v2, v1

    :goto_2
    invoke-static {v3, v2}, Ltv/peel/samsung/widget/NotiRemoteService;->a(Ltv/peel/samsung/widget/NotiRemoteService;Z)Z

    .line 212
    iget-object v2, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v2, v0}, Ltv/peel/samsung/widget/NotiRemoteService;->b(Ltv/peel/samsung/widget/NotiRemoteService;Z)V

    move v6, v1

    .line 214
    goto/16 :goto_1

    :cond_3
    move v2, v0

    .line 211
    goto :goto_2

    .line 216
    :pswitch_5
    sget-object v2, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v2}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v2

    sget-object v3, Lcom/peel/c/b;->a:Lcom/peel/c/b;

    if-ne v2, v3, :cond_0

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v2, v4, :cond_0

    .line 217
    iget-object v2, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v2, v0}, Ltv/peel/samsung/widget/NotiRemoteService;->c(Ltv/peel/samsung/widget/NotiRemoteService;Z)Z

    move v6, v1

    .line 218
    goto/16 :goto_1

    .line 222
    :pswitch_6
    iget-object v0, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v0}, Ltv/peel/samsung/widget/NotiRemoteService;->f(Ltv/peel/samsung/widget/NotiRemoteService;)Ltv/peel/widget/g;

    move-result-object v0

    iput p4, v0, Ltv/peel/widget/g;->h:I

    .line 223
    iget-object v0, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v0}, Ltv/peel/samsung/widget/NotiRemoteService;->g(Ltv/peel/samsung/widget/NotiRemoteService;)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, p4

    .line 224
    iget-object v2, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v2}, Ltv/peel/samsung/widget/NotiRemoteService;->f(Ltv/peel/samsung/widget/NotiRemoteService;)Ltv/peel/widget/g;

    move-result-object v2

    invoke-static {}, Ltv/peel/samsung/widget/NotiRemoteService;->b()Ltv/peel/samsung/widget/a;

    move-result-object v3

    iget-object v4, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v4}, Ltv/peel/samsung/widget/NotiRemoteService;->h(Ltv/peel/samsung/widget/NotiRemoteService;)Lcom/peel/control/a;

    move-result-object v4

    invoke-virtual {v2, v3, v0, v4}, Ltv/peel/widget/g;->a(Ltv/peel/widget/b;Ljava/lang/String;Lcom/peel/control/a;)V

    .line 225
    iget-object v2, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v2}, Ltv/peel/samsung/widget/NotiRemoteService;->g(Ltv/peel/samsung/widget/NotiRemoteService;)[Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "PowerOn"

    if-ne v0, v3, :cond_4

    const-string/jumbo v0, "PowerOff"

    :goto_3
    aput-object v0, v2, p4

    goto/16 :goto_1

    :cond_4
    const-string/jumbo v0, "PowerOn"

    goto :goto_3

    .line 234
    :pswitch_7
    sget-object v2, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v2}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v2

    sget-object v3, Lcom/peel/c/b;->a:Lcom/peel/c/b;

    if-ne v2, v3, :cond_5

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v2, v4, :cond_5

    .line 235
    iget-object v2, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v2, v0}, Ltv/peel/samsung/widget/NotiRemoteService;->c(Ltv/peel/samsung/widget/NotiRemoteService;Z)Z

    .line 237
    :cond_5
    iget-object v0, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    iget-object v2, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v2}, Ltv/peel/samsung/widget/NotiRemoteService;->f(Ltv/peel/samsung/widget/NotiRemoteService;)Ltv/peel/widget/g;

    move-result-object v2

    iget-object v2, v2, Ltv/peel/widget/g;->e:[Lcom/peel/control/a;

    add-int/lit8 v3, p2, -0x16

    aget-object v2, v2, v3

    invoke-static {v0, v2}, Ltv/peel/samsung/widget/NotiRemoteService;->a(Ltv/peel/samsung/widget/NotiRemoteService;Lcom/peel/control/a;)Lcom/peel/control/a;

    .line 238
    invoke-static {}, Ltv/peel/samsung/widget/NotiRemoteService;->b()Ltv/peel/samsung/widget/a;

    move-result-object v0

    iget-object v2, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v2}, Ltv/peel/samsung/widget/NotiRemoteService;->h(Ltv/peel/samsung/widget/NotiRemoteService;)Lcom/peel/control/a;

    move-result-object v2

    invoke-virtual {v0, v2}, Ltv/peel/samsung/widget/a;->b(Lcom/peel/control/a;)V

    .line 239
    iget-object v0, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v0}, Ltv/peel/samsung/widget/NotiRemoteService;->h(Ltv/peel/samsung/widget/NotiRemoteService;)Lcom/peel/control/a;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v0

    .line 241
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->d()I

    move-result v2

    const/16 v3, 0x12

    if-ne v2, v3, :cond_6

    .line 242
    invoke-static {}, Ltv/peel/samsung/widget/NotiRemoteService;->b()Ltv/peel/samsung/widget/a;

    move-result-object v2

    invoke-virtual {v2, v0}, Ltv/peel/samsung/widget/a;->a(Lcom/peel/control/h;)V

    :cond_6
    move v6, v1

    .line 246
    goto/16 :goto_1

    .line 248
    :pswitch_8
    iget-object v0, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v0}, Ltv/peel/samsung/widget/NotiRemoteService;->f(Ltv/peel/samsung/widget/NotiRemoteService;)Ltv/peel/widget/g;

    move-result-object v0

    iput p4, v0, Ltv/peel/widget/g;->h:I

    .line 249
    iget-object v0, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v0}, Ltv/peel/samsung/widget/NotiRemoteService;->f(Ltv/peel/samsung/widget/NotiRemoteService;)Ltv/peel/widget/g;

    move-result-object v0

    invoke-static {}, Ltv/peel/samsung/widget/NotiRemoteService;->b()Ltv/peel/samsung/widget/a;

    move-result-object v2

    const-string/jumbo v3, "Power"

    iget-object v4, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v4}, Ltv/peel/samsung/widget/NotiRemoteService;->h(Ltv/peel/samsung/widget/NotiRemoteService;)Lcom/peel/control/a;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/g;->a(Ltv/peel/widget/b;Ljava/lang/String;Lcom/peel/control/a;)V

    goto/16 :goto_1

    .line 254
    :pswitch_9
    iget-object v0, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v0}, Ltv/peel/samsung/widget/NotiRemoteService;->f(Ltv/peel/samsung/widget/NotiRemoteService;)Ltv/peel/widget/g;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 256
    const-string/jumbo v0, "Play"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 257
    iget-object v0, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v0}, Ltv/peel/samsung/widget/NotiRemoteService;->i(Ltv/peel/samsung/widget/NotiRemoteService;)Ljava/util/TreeMap;

    move-result-object v0

    invoke-direct {p0, v0}, Ltv/peel/samsung/widget/t;->a(Ljava/util/TreeMap;)Ljava/lang/String;

    move-result-object p3

    move v0, v6

    .line 261
    :goto_4
    iget-object v2, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v2}, Ltv/peel/samsung/widget/NotiRemoteService;->f(Ltv/peel/samsung/widget/NotiRemoteService;)Ltv/peel/widget/g;

    move-result-object v2

    invoke-static {}, Ltv/peel/samsung/widget/NotiRemoteService;->b()Ltv/peel/samsung/widget/a;

    move-result-object v3

    iget-object v4, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v4}, Ltv/peel/samsung/widget/NotiRemoteService;->h(Ltv/peel/samsung/widget/NotiRemoteService;)Lcom/peel/control/a;

    move-result-object v4

    invoke-virtual {v2, v3, p3, v4}, Ltv/peel/widget/g;->a(Ltv/peel/widget/b;Ljava/lang/String;Lcom/peel/control/a;)V

    move v6, v0

    goto/16 :goto_1

    .line 258
    :cond_7
    const-string/jumbo v0, "UP"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    const-string/jumbo v0, "Down"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    const-string/jumbo v0, "mode"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_8
    move v0, v1

    .line 259
    goto :goto_4

    .line 267
    :pswitch_a
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    const-string/jumbo v3, "http://peel.com/partners"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 268
    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 269
    iget-object v2, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-virtual {v2, v0}, Ltv/peel/samsung/widget/NotiRemoteService;->startActivity(Landroid/content/Intent;)V

    .line 270
    iget-object v0, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v0}, Ltv/peel/samsung/widget/NotiRemoteService;->c(Ltv/peel/samsung/widget/NotiRemoteService;)V

    goto/16 :goto_1

    .line 273
    :pswitch_b
    invoke-static {}, Lcom/peel/util/a/f;->a()Lcom/peel/util/a/f;

    move-result-object v0

    const/16 v2, 0x440

    const-string/jumbo v4, "NOTIFICATION"

    invoke-virtual/range {v0 .. v5}, Lcom/peel/util/a/f;->a(IIILjava/lang/String;I)V

    .line 274
    iget-object v0, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v0}, Ltv/peel/samsung/widget/NotiRemoteService;->c(Ltv/peel/samsung/widget/NotiRemoteService;)V

    .line 275
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v2}, Ltv/peel/samsung/widget/NotiRemoteService;->j(Ltv/peel/samsung/widget/NotiRemoteService;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 276
    iget-object v2, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v2}, Ltv/peel/samsung/widget/NotiRemoteService;->k(Ltv/peel/samsung/widget/NotiRemoteService;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "com.peel.main.Home"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 277
    const/high16 v2, 0x30000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 278
    const-string/jumbo v2, "peel://remote"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 279
    iget-object v2, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-virtual {v2, v0}, Ltv/peel/samsung/widget/NotiRemoteService;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    :cond_9
    move v0, v6

    goto/16 :goto_4

    :cond_a
    move v6, v0

    goto/16 :goto_0

    .line 186
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_a
        :pswitch_b
        :pswitch_3
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method

.method public b(Landroid/content/Intent;ILjava/lang/String;I)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 294
    if-ne p4, v1, :cond_2

    .line 295
    iget-object v0, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v0, p3}, Ltv/peel/samsung/widget/NotiRemoteService;->a(Ltv/peel/samsung/widget/NotiRemoteService;Ljava/lang/String;)Ljava/lang/String;

    .line 296
    const-string/jumbo v0, "UP"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "Down"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "mode"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 297
    :cond_0
    iget-object v0, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v0, v1}, Ltv/peel/samsung/widget/NotiRemoteService;->d(Ltv/peel/samsung/widget/NotiRemoteService;Z)Z

    .line 301
    :goto_0
    iget-object v0, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    invoke-static {v0, v1}, Ltv/peel/samsung/widget/NotiRemoteService;->a(Ltv/peel/samsung/widget/NotiRemoteService;Ljava/util/Timer;)Ljava/util/Timer;

    .line 302
    iget-object v0, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-static {v0}, Ltv/peel/samsung/widget/NotiRemoteService;->m(Ltv/peel/samsung/widget/NotiRemoteService;)Ljava/util/Timer;

    move-result-object v0

    new-instance v1, Ltv/peel/samsung/widget/u;

    iget-object v2, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-direct {v1, v2}, Ltv/peel/samsung/widget/u;-><init>(Ltv/peel/samsung/widget/NotiRemoteService;)V

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 306
    :goto_1
    return-void

    .line 299
    :cond_1
    iget-object v0, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ltv/peel/samsung/widget/NotiRemoteService;->d(Ltv/peel/samsung/widget/NotiRemoteService;Z)Z

    goto :goto_0

    .line 304
    :cond_2
    iget-object v0, p0, Ltv/peel/samsung/widget/t;->a:Ltv/peel/samsung/widget/NotiRemoteService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ltv/peel/samsung/widget/NotiRemoteService;->a(Ltv/peel/samsung/widget/NotiRemoteService;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1
.end method
