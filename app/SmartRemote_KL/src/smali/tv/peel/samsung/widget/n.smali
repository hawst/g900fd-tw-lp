.class public Ltv/peel/samsung/widget/n;
.super Ljava/lang/Object;


# static fields
.field public static a:Ltv/peel/samsung/widget/a;

.field private static final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Ltv/peel/samsung/widget/n;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ltv/peel/samsung/widget/n;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/peel/control/h;Landroid/content/Context;Z)I
    .locals 2

    .prologue
    const v0, 0x7f03008f

    .line 342
    invoke-virtual {p0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->d()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 348
    if-eqz p2, :cond_0

    .line 351
    :goto_0
    :sswitch_0
    return v0

    .line 346
    :sswitch_1
    const v0, 0x7f030091

    goto :goto_0

    .line 351
    :cond_0
    const v0, 0x7f030090

    goto :goto_0

    .line 342
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x12 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(Landroid/content/Context;)Landroid/widget/RemoteViews;
    .locals 12

    .prologue
    .line 40
    const/4 v3, 0x0

    .line 42
    sget-object v0, Ltv/peel/samsung/widget/n;->a:Ltv/peel/samsung/widget/a;

    if-nez v0, :cond_0

    .line 43
    new-instance v0, Ltv/peel/samsung/widget/a;

    invoke-direct {v0, p0}, Ltv/peel/samsung/widget/a;-><init>(Landroid/content/Context;)V

    sput-object v0, Ltv/peel/samsung/widget/n;->a:Ltv/peel/samsung/widget/a;

    .line 45
    :cond_0
    sget-object v0, Ltv/peel/samsung/widget/n;->a:Ltv/peel/samsung/widget/a;

    invoke-virtual {v0}, Ltv/peel/samsung/widget/a;->g()Lcom/peel/control/a;

    move-result-object v4

    .line 46
    const/4 v0, 0x0

    .line 47
    const/4 v6, 0x0

    .line 49
    if-eqz v4, :cond_9

    .line 50
    const/4 v1, 0x1

    invoke-virtual {v4, v1}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v2

    .line 51
    invoke-virtual {v4}, Lcom/peel/control/a;->d()[Ljava/lang/String;

    move-result-object v5

    .line 52
    if-eqz v5, :cond_1

    .line 53
    array-length v7, v5

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v7, :cond_1

    aget-object v8, v5, v1

    .line 54
    const-string/jumbo v9, "live"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 55
    const/4 v0, 0x1

    .line 61
    :cond_1
    if-eqz v2, :cond_9

    .line 62
    invoke-virtual {v4}, Lcom/peel/control/a;->e()[Lcom/peel/control/h;

    move-result-object v5

    .line 64
    invoke-static {p0}, Ltv/peel/widget/util/j;->a(Landroid/content/Context;)Ltv/peel/widget/util/j;

    move-result-object v1

    invoke-virtual {v1, v2}, Ltv/peel/widget/util/j;->a(Lcom/peel/control/h;)V

    .line 65
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v7, "is_setup_complete"

    const/4 v8, 0x0

    invoke-interface {v1, v7, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 66
    invoke-static {v2, p0, v0}, Ltv/peel/samsung/widget/n;->a(Lcom/peel/control/h;Landroid/content/Context;Z)I

    move-result v7

    .line 68
    new-instance v3, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0, v7}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 70
    const-string/jumbo v0, "layout_inflater"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 71
    const/4 v1, 0x0

    invoke-virtual {v0, v7, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 72
    new-instance v0, Landroid/content/Intent;

    const-class v1, Ltv/peel/samsung/widget/LockscreenService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 73
    const-string/jumbo v1, "tv.peel.samsung.widget.lockscreen.action.BUTTON_PRESSED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 74
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 76
    const/4 v1, 0x0

    .line 77
    array-length v9, v5

    const/4 v0, 0x0

    move v2, v0

    move v0, v1

    :goto_1
    if-ge v2, v9, :cond_8

    aget-object v1, v5, v2

    .line 78
    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v10

    invoke-virtual {v10}, Lcom/peel/data/g;->d()I

    move-result v10

    .line 80
    const/4 v11, 0x6

    if-ne v10, v11, :cond_3

    .line 81
    invoke-virtual {v1}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v1

    .line 82
    if-eqz v1, :cond_5

    const-string/jumbo v10, "Apple"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 83
    const/4 v1, 0x1

    .line 77
    :goto_2
    add-int/lit8 v2, v2, 0x1

    move v6, v1

    goto :goto_1

    .line 53
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 86
    :cond_3
    const/16 v11, 0x12

    if-ne v10, v11, :cond_6

    .line 87
    sget-object v10, Ltv/peel/samsung/widget/n;->a:Ltv/peel/samsung/widget/a;

    if-eqz v10, :cond_4

    sget-object v10, Ltv/peel/samsung/widget/n;->a:Ltv/peel/samsung/widget/a;

    invoke-virtual {v10}, Ltv/peel/samsung/widget/a;->b()Z

    move-result v10

    if-eqz v10, :cond_4

    .line 88
    const v10, 0x7f0a018d

    const/4 v11, 0x0

    invoke-virtual {v3, v10, v11}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 89
    const v10, 0x7f0a018d

    sget-object v11, Ltv/peel/samsung/widget/n;->a:Ltv/peel/samsung/widget/a;

    invoke-virtual {v11}, Ltv/peel/samsung/widget/a;->d()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v3, v10, v11}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 95
    :cond_4
    :goto_3
    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    move v1, v6

    goto :goto_2

    .line 92
    :cond_6
    const/4 v11, 0x2

    if-eq v10, v11, :cond_7

    const/16 v11, 0x14

    if-ne v10, v11, :cond_4

    .line 93
    :cond_7
    const/4 v0, 0x1

    goto :goto_3

    .line 98
    :cond_8
    invoke-static {p0, v3}, Ltv/peel/samsung/widget/n;->a(Landroid/content/Context;Landroid/widget/RemoteViews;)V

    .line 99
    const v1, 0x7f0a01af

    invoke-static {v4, p0}, Ltv/peel/samsung/widget/n;->a(Lcom/peel/control/a;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 100
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v1

    .line 102
    if-eqz v0, :cond_a

    const v2, 0x7f03008f

    if-ne v7, v2, :cond_a

    .line 103
    const v0, 0x7f0a0180

    const/16 v2, 0x8

    invoke-virtual {v3, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 104
    const v0, 0x7f0a0181

    const/4 v2, 0x0

    invoke-virtual {v3, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 105
    const v0, 0x7f0a01b7

    const/4 v2, 0x0

    invoke-virtual {v3, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 114
    :goto_4
    if-nez v1, :cond_c

    .line 115
    const v0, 0x7f0a01b1

    const/16 v1, 0x8

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 116
    const v0, 0x7f0a01b2

    const/16 v1, 0x8

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 117
    const v0, 0x7f0a01b3

    const/16 v1, 0x8

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 144
    :goto_5
    invoke-static {p0, v3, v7, v6}, Ltv/peel/samsung/widget/n;->a(Landroid/content/Context;Landroid/widget/RemoteViews;IZ)V

    .line 148
    :cond_9
    return-object v3

    .line 106
    :cond_a
    if-nez v0, :cond_b

    const v0, 0x7f03008f

    if-ne v7, v0, :cond_b

    .line 107
    const v0, 0x7f0a0180

    const/4 v2, 0x0

    invoke-virtual {v3, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 108
    const v0, 0x7f0a0181

    const/16 v2, 0x8

    invoke-virtual {v3, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 109
    const v0, 0x7f0a01b7

    const/4 v2, 0x4

    invoke-virtual {v3, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_4

    .line 111
    :cond_b
    const v0, 0x7f0a01b7

    const/4 v2, 0x4

    invoke-virtual {v3, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_4

    .line 118
    :cond_c
    const/4 v0, 0x1

    if-ne v1, v0, :cond_d

    .line 119
    const v0, 0x7f0a01b1

    const/4 v1, 0x4

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 120
    const v0, 0x7f0a01b2

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 121
    const v0, 0x7f0a01b3

    const/4 v1, 0x4

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 122
    const v1, 0x7f0a01b2

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    invoke-static {p0, v0}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 123
    const v1, 0x7f0a01b2

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-static {v0}, Ltv/peel/samsung/widget/n;->a(Lcom/peel/control/h;)Z

    move-result v4

    const/4 v5, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Ltv/peel/samsung/widget/n;->a(Landroid/content/Context;ILjava/lang/String;Landroid/widget/RemoteViews;ZI)V

    goto :goto_5

    .line 124
    :cond_d
    const/4 v0, 0x2

    if-ne v1, v0, :cond_e

    .line 125
    const v0, 0x7f0a01b1

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 126
    const v0, 0x7f0a01b2

    const/4 v1, 0x4

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 127
    const v0, 0x7f0a01b3

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 128
    const v1, 0x7f0a01b1

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    invoke-static {p0, v0}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 129
    const v1, 0x7f0a01b3

    const/4 v0, 0x1

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    invoke-static {p0, v0}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 130
    const v1, 0x7f0a01b1

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-static {v0}, Ltv/peel/samsung/widget/n;->a(Lcom/peel/control/h;)Z

    move-result v4

    const/4 v5, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Ltv/peel/samsung/widget/n;->a(Landroid/content/Context;ILjava/lang/String;Landroid/widget/RemoteViews;ZI)V

    .line 131
    const v1, 0x7f0a01b3

    const/4 v0, 0x1

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x1

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-static {v0}, Ltv/peel/samsung/widget/n;->a(Lcom/peel/control/h;)Z

    move-result v4

    const/4 v5, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Ltv/peel/samsung/widget/n;->a(Landroid/content/Context;ILjava/lang/String;Landroid/widget/RemoteViews;ZI)V

    goto/16 :goto_5

    .line 133
    :cond_e
    const v0, 0x7f0a01b1

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 134
    const v0, 0x7f0a01b2

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 135
    const v0, 0x7f0a01b3

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 136
    const v1, 0x7f0a01b1

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    invoke-static {p0, v0}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 137
    const v1, 0x7f0a01b2

    const/4 v0, 0x1

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    invoke-static {p0, v0}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 138
    const v1, 0x7f0a01b3

    const/4 v0, 0x2

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    invoke-static {p0, v0}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 139
    const v1, 0x7f0a01b1

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-static {v0}, Ltv/peel/samsung/widget/n;->a(Lcom/peel/control/h;)Z

    move-result v4

    const/4 v5, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Ltv/peel/samsung/widget/n;->a(Landroid/content/Context;ILjava/lang/String;Landroid/widget/RemoteViews;ZI)V

    .line 140
    const v1, 0x7f0a01b2

    const/4 v0, 0x1

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x1

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-static {v0}, Ltv/peel/samsung/widget/n;->a(Lcom/peel/control/h;)Z

    move-result v4

    const/4 v5, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Ltv/peel/samsung/widget/n;->a(Landroid/content/Context;ILjava/lang/String;Landroid/widget/RemoteViews;ZI)V

    .line 141
    const v1, 0x7f0a01b3

    const/4 v0, 0x2

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x2

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/peel/control/h;

    invoke-static {v0}, Ltv/peel/samsung/widget/n;->a(Lcom/peel/control/h;)Z

    move-result v4

    const/4 v5, 0x2

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Ltv/peel/samsung/widget/n;->a(Landroid/content/Context;ILjava/lang/String;Landroid/widget/RemoteViews;ZI)V

    goto/16 :goto_5
.end method

.method public static a(Lcom/peel/control/a;Landroid/content/Context;)Ljava/lang/String;
    .locals 7

    .prologue
    const v6, 0x7f0d045e

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 356
    const/4 v0, 0x0

    .line 357
    if-eqz p0, :cond_0

    .line 358
    invoke-virtual {p0, v4}, Lcom/peel/control/a;->a(I)Lcom/peel/control/h;

    move-result-object v0

    .line 360
    if-eqz v0, :cond_2

    .line 361
    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/peel/data/g;->f()Ljava/lang/String;

    move-result-object v1

    .line 363
    invoke-virtual {v0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->d()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 381
    invoke-virtual {p1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/peel/control/a;->c()Lcom/peel/data/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/d;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 389
    :cond_0
    :goto_0
    return-object v0

    .line 365
    :sswitch_0
    invoke-virtual {p1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/peel/control/a;->c()Lcom/peel/data/d;

    move-result-object v3

    invoke-virtual {v3}, Lcom/peel/data/d;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 366
    const-string/jumbo v2, "directv"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string/jumbo v2, "tivo"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 367
    :cond_1
    invoke-virtual {p1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v1, v2, v5

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 371
    :sswitch_1
    invoke-virtual {p1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v1, v2, v5

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 375
    :sswitch_2
    invoke-virtual {p0}, Lcom/peel/control/a;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 385
    :cond_2
    invoke-virtual {p1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/peel/control/a;->c()Lcom/peel/data/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/d;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 363
    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_2
        0x6 -> :sswitch_1
        0x12 -> :sswitch_2
        0x14 -> :sswitch_0
    .end sparse-switch
.end method

.method private static a(Landroid/content/Context;ILjava/lang/String;Landroid/widget/RemoteViews;ZI)V
    .locals 3

    .prologue
    .line 162
    new-instance v0, Landroid/content/Intent;

    const-class v1, Ltv/peel/samsung/widget/LockscreenService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 163
    const-string/jumbo v1, "tv.peel.samsung.widget.lockscreen.action.BUTTON_PRESSED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 165
    if-eqz p4, :cond_0

    .line 166
    const-string/jumbo v1, "tv.peel.samsung.widget.lockscreen.index"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 167
    const-string/jumbo v1, "tv.peel.samsung.widget.lockscreen.extra.COMMAND"

    const-string/jumbo v2, "PowerOn|PowerOff"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 171
    :goto_0
    const-string/jumbo v1, "tv.peel.samsung.widget.lockscreen.field"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 172
    const/high16 v1, 0x8000000

    invoke-static {p0, p1, v0, v1}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p3, p1, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 173
    return-void

    .line 169
    :cond_0
    const-string/jumbo v1, "tv.peel.samsung.widget.lockscreen.extra.COMMAND"

    const-string/jumbo v2, "Power"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Landroid/widget/RemoteViews;)V
    .locals 8

    .prologue
    const/high16 v7, 0x8000000

    const/high16 v6, 0x200000

    const/4 v5, 0x3

    const v4, 0x7f0a0323

    const v3, 0x7f0a0322

    .line 323
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 324
    const-string/jumbo v1, "context_id"

    const/16 v2, 0x1b58

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 325
    const/high16 v1, 0x30000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 326
    const-string/jumbo v1, "from"

    const-string/jumbo v2, "LOCKSCREEN"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 328
    const v1, 0x7f0a018e

    invoke-static {p0, v5, v0, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 329
    const v1, 0x7f0a01af

    invoke-static {p0, v5, v0, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 331
    new-instance v0, Landroid/content/Intent;

    const-class v1, Ltv/peel/samsung/widget/LockscreenService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 332
    const-string/jumbo v1, "tv.peel.samsung.widget.lockscreen.action.CHANGE_DEVICE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 334
    const-string/jumbo v1, "tv.peel.samsung.widget.lockscreen.extra.COMMAND"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 335
    invoke-static {p0, v4, v0, v7}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {p1, v4, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 337
    const-string/jumbo v1, "tv.peel.samsung.widget.lockscreen.extra.COMMAND"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 338
    invoke-static {p0, v3, v0, v7}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 339
    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 176
    new-instance v0, Landroid/content/Intent;

    const-class v1, Ltv/peel/samsung/widget/LockscreenService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 177
    const-string/jumbo v1, "tv.peel.samsung.widget.lockscreen.action.BUTTON_PRESSED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 178
    const-string/jumbo v1, "tv.peel.samsung.widget.lockscreen.extra.COMMAND"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 179
    const/high16 v1, 0x8000000

    invoke-static {p0, p2, v0, v1}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 180
    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/widget/RemoteViews;IZ)V
    .locals 10

    .prologue
    const v9, 0x7f0a018d

    const v8, 0x7f0a0189

    const v7, 0x7f0a0188

    const v6, 0x7f020448

    const/4 v3, 0x0

    .line 184
    const v1, 0x7f0a01b7

    if-eqz p3, :cond_3

    const-string/jumbo v0, "Select"

    :goto_0
    invoke-static {p0, p1, v1, v0}, Ltv/peel/samsung/widget/n;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 185
    const v0, 0x7f0a01b8

    const-string/jumbo v1, "Mute"

    invoke-static {p0, p1, v0, v1}, Ltv/peel/samsung/widget/n;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 187
    const v0, 0x7f0a018e

    const-string/jumbo v1, "setBackgroundResource"

    const v2, 0x7f020475

    invoke-virtual {p1, v0, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 189
    const v0, 0x7f0a01b1

    const-string/jumbo v1, "setBackgroundResource"

    const v2, 0x7f020479

    invoke-virtual {p1, v0, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 190
    const v0, 0x7f0a01b2

    const-string/jumbo v1, "setBackgroundResource"

    const v2, 0x7f020479

    invoke-virtual {p1, v0, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 191
    const v0, 0x7f0a01b3

    const-string/jumbo v1, "setBackgroundResource"

    const v2, 0x7f020479

    invoke-virtual {p1, v0, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 192
    const v1, 0x7f0a01b1

    const v2, 0x7f020460

    move-object v0, p1

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setTextViewCompoundDrawables(IIIII)V

    .line 193
    const v1, 0x7f0a01b2

    const v2, 0x7f020460

    move-object v0, p1

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setTextViewCompoundDrawables(IIIII)V

    .line 194
    const v1, 0x7f0a01b3

    const v2, 0x7f020460

    move-object v0, p1

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setTextViewCompoundDrawables(IIIII)V

    .line 196
    const v0, 0x7f02047d

    invoke-virtual {p1, v7, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 197
    const v0, 0x7f02047c

    invoke-virtual {p1, v8, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 198
    const v0, 0x7f0a018a

    const v1, 0x7f020472

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 199
    const v0, 0x7f0a018b

    const v1, 0x7f020471

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 201
    const v0, 0x7f0a01b4

    const v1, 0x7f0d0262

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 202
    const v0, 0x7f0a01b5

    const v1, 0x7f0d0261

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 204
    const v0, 0x7f0a01b7

    const-string/jumbo v1, "setBackgroundResource"

    invoke-virtual {p1, v0, v1, v6}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 205
    const v0, 0x7f0a01b7

    const v1, 0x7f020478

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 206
    const v0, 0x7f0a01b8

    const-string/jumbo v1, "setBackgroundResource"

    invoke-virtual {p1, v0, v1, v6}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 207
    const v0, 0x7f0a01b8

    const v1, 0x7f020477

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 210
    const v0, 0x7f03008f

    if-ne p2, v0, :cond_0

    .line 211
    const-string/jumbo v0, "Volume_Up"

    invoke-static {p0, p1, v7, v0}, Ltv/peel/samsung/widget/n;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 212
    const-string/jumbo v0, "Volume_Down"

    invoke-static {p0, p1, v8, v0}, Ltv/peel/samsung/widget/n;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 213
    const v0, 0x7f0a018a

    const-string/jumbo v1, "Channel_Up"

    invoke-static {p0, p1, v0, v1}, Ltv/peel/samsung/widget/n;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 214
    const v0, 0x7f0a018b

    const-string/jumbo v1, "Channel_Down"

    invoke-static {p0, p1, v0, v1}, Ltv/peel/samsung/widget/n;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 215
    const v0, 0x7f0a0182

    const-string/jumbo v1, "Fast_Forward"

    invoke-static {p0, p1, v0, v1}, Ltv/peel/samsung/widget/n;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 216
    const v0, 0x7f0a0183

    const-string/jumbo v1, "Channel_Up"

    invoke-static {p0, p1, v0, v1}, Ltv/peel/samsung/widget/n;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 217
    const v0, 0x7f0a0184

    const-string/jumbo v1, "Channel_Down"

    invoke-static {p0, p1, v0, v1}, Ltv/peel/samsung/widget/n;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 219
    const v0, 0x7f0a0182

    const-string/jumbo v1, "setBackgroundResource"

    invoke-virtual {p1, v0, v1, v6}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 220
    const v0, 0x7f0a0182

    const v1, 0x7f020474

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 221
    const v0, 0x7f0a0183

    const-string/jumbo v1, "setBackgroundResource"

    invoke-virtual {p1, v0, v1, v6}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 222
    const v0, 0x7f0a0183

    const v1, 0x7f020470

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 223
    const v0, 0x7f0a0184

    const-string/jumbo v1, "setBackgroundResource"

    invoke-virtual {p1, v0, v1, v6}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 224
    const v0, 0x7f0a0184

    const v1, 0x7f02046f

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 227
    :cond_0
    const v0, 0x7f030090

    if-ne p2, v0, :cond_1

    .line 228
    const-string/jumbo v0, "Volume_Up"

    invoke-static {p0, p1, v7, v0}, Ltv/peel/samsung/widget/n;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 229
    const-string/jumbo v0, "Volume_Down"

    invoke-static {p0, p1, v8, v0}, Ltv/peel/samsung/widget/n;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 230
    const v0, 0x7f0a0185

    const-string/jumbo v1, "Rewind"

    invoke-static {p0, p1, v0, v1}, Ltv/peel/samsung/widget/n;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 231
    const v0, 0x7f0a0186

    const-string/jumbo v1, "Fast_Forward"

    invoke-static {p0, p1, v0, v1}, Ltv/peel/samsung/widget/n;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 232
    const v1, 0x7f0a0187

    if-eqz p3, :cond_4

    const-string/jumbo v0, "Select"

    :goto_1
    invoke-static {p0, p1, v1, v0}, Ltv/peel/samsung/widget/n;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 234
    const v0, 0x7f0a0185

    const-string/jumbo v1, "setBackgroundResource"

    invoke-virtual {p1, v0, v1, v6}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 235
    const v0, 0x7f0a0185

    const v1, 0x7f02047b

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 236
    const v0, 0x7f0a0186

    const-string/jumbo v1, "setBackgroundResource"

    invoke-virtual {p1, v0, v1, v6}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 237
    const v0, 0x7f0a0186

    const v1, 0x7f020474

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 238
    const v0, 0x7f0a0187

    const-string/jumbo v1, "setBackgroundResource"

    invoke-virtual {p1, v0, v1, v6}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 239
    const v0, 0x7f0a0187

    const v1, 0x7f020478

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 242
    :cond_1
    const v0, 0x7f030091

    if-ne p2, v0, :cond_2

    .line 243
    const v0, 0x7f0a01b4

    const v1, 0x7f0d0265

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 244
    const v0, 0x7f0a01b5

    const v1, 0x7f0d0263

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 246
    const v2, 0x7f02005b

    move-object v0, p1

    move v1, v9

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setTextViewCompoundDrawables(IIIII)V

    .line 247
    const-string/jumbo v0, "setBackgroundResource"

    invoke-virtual {p1, v9, v0, v6}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 248
    const-string/jumbo v0, "UP"

    invoke-static {p0, p1, v7, v0}, Ltv/peel/samsung/widget/n;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 249
    const-string/jumbo v0, "Down"

    invoke-static {p0, p1, v8, v0}, Ltv/peel/samsung/widget/n;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 250
    const v0, 0x7f0a018a

    const-string/jumbo v1, "FAN_HIGH"

    invoke-static {p0, p1, v0, v1}, Ltv/peel/samsung/widget/n;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 251
    const v0, 0x7f0a018b

    const-string/jumbo v1, "FAN_LOW"

    invoke-static {p0, p1, v0, v1}, Ltv/peel/samsung/widget/n;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 252
    const-string/jumbo v0, "MODE"

    invoke-static {p0, p1, v9, v0}, Ltv/peel/samsung/widget/n;->a(Landroid/content/Context;Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 254
    const-string/jumbo v0, "setBackgroundResource"

    invoke-virtual {p1, v9, v0, v6}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 256
    :cond_2
    return-void

    .line 184
    :cond_3
    const-string/jumbo v0, "Play|Pause"

    goto/16 :goto_0

    .line 232
    :cond_4
    const-string/jumbo v0, "Play|Pause"

    goto/16 :goto_1
.end method

.method private static a(Lcom/peel/control/h;)Z
    .locals 2

    .prologue
    .line 152
    invoke-virtual {p0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->a()Ljava/util/Map;

    move-result-object v0

    .line 154
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    if-lez v1, :cond_0

    const-string/jumbo v1, "Power"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    const/4 v0, 0x0

    .line 157
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
