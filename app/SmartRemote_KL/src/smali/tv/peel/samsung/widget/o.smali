.class public Ltv/peel/samsung/widget/o;
.super Ljava/lang/Object;

# interfaces
.implements Ltv/peel/widget/h;


# instance fields
.field private a:Landroid/widget/RemoteViews;

.field private b:Z

.field private c:Landroid/content/Context;

.field private d:Ltv/peel/widget/util/a;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Ltv/peel/samsung/widget/o;->b:Z

    return-void
.end method

.method private a(Landroid/widget/RemoteViews;[Lcom/peel/control/h;)V
    .locals 7

    .prologue
    .line 184
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 185
    const/4 v0, 0x0

    .line 187
    if-eqz p2, :cond_b

    array-length v2, p2

    if-lez v2, :cond_b

    .line 188
    array-length v2, p2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, p2, v0

    .line 189
    invoke-virtual {v3}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v4

    invoke-virtual {v4}, Lcom/peel/data/g;->d()I

    move-result v4

    .line 191
    const/4 v5, 0x6

    if-ne v4, v5, :cond_0

    .line 188
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 194
    :cond_0
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 196
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    move v6, v0

    .line 199
    :goto_2
    const v0, 0x7f0a01b1

    const-string/jumbo v1, "setBackgroundResource"

    const v2, 0x7f020479

    invoke-virtual {p1, v0, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 200
    const v0, 0x7f0a01b2

    const-string/jumbo v1, "setBackgroundResource"

    const v2, 0x7f020479

    invoke-virtual {p1, v0, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 201
    const v0, 0x7f0a01b3

    const-string/jumbo v1, "setBackgroundResource"

    const v2, 0x7f020479

    invoke-virtual {p1, v0, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 202
    const v1, 0x7f0a01b1

    const v2, 0x7f020460

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setTextViewCompoundDrawables(IIIII)V

    .line 203
    const v1, 0x7f0a01b2

    const v2, 0x7f020460

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setTextViewCompoundDrawables(IIIII)V

    .line 204
    const v1, 0x7f0a01b3

    const v2, 0x7f020460

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setTextViewCompoundDrawables(IIIII)V

    .line 206
    if-nez v6, :cond_2

    .line 207
    const v0, 0x7f0a01b1

    const/16 v1, 0x8

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 208
    const v0, 0x7f0a01b2

    const/16 v1, 0x8

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 209
    const v0, 0x7f0a01b3

    const/16 v1, 0x8

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 235
    :goto_3
    return-void

    .line 210
    :cond_2
    const/4 v0, 0x1

    if-ne v6, v0, :cond_4

    .line 211
    const v0, 0x7f0a01b1

    const/4 v1, 0x4

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 212
    const v0, 0x7f0a01b2

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 213
    const v0, 0x7f0a01b3

    const/4 v1, 0x4

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 214
    const v0, 0x7f0a01b2

    iget-object v1, p0, Ltv/peel/samsung/widget/o;->c:Landroid/content/Context;

    const/4 v2, 0x0

    aget-object v2, p2, v2

    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->d()I

    move-result v2

    invoke-static {v1, v2}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 215
    iget-object v1, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a01b2

    const/4 v0, 0x0

    aget-object v0, p2, v0

    invoke-static {v0}, Ltv/peel/samsung/widget/o;->a(Lcom/peel/control/h;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/16 v0, 0x28

    :goto_4
    const/4 v3, 0x0

    invoke-virtual {v1, p1, v2, v0, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;III)V

    goto :goto_3

    :cond_3
    const/16 v0, 0x20

    goto :goto_4

    .line 217
    :cond_4
    const/4 v0, 0x2

    if-ne v6, v0, :cond_7

    .line 218
    const v0, 0x7f0a01b1

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 219
    const v0, 0x7f0a01b2

    const/4 v1, 0x4

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 220
    const v0, 0x7f0a01b3

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 221
    const v0, 0x7f0a01b1

    iget-object v1, p0, Ltv/peel/samsung/widget/o;->c:Landroid/content/Context;

    const/4 v2, 0x0

    aget-object v2, p2, v2

    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->d()I

    move-result v2

    invoke-static {v1, v2}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 222
    const v0, 0x7f0a01b3

    iget-object v1, p0, Ltv/peel/samsung/widget/o;->c:Landroid/content/Context;

    const/4 v2, 0x1

    aget-object v2, p2, v2

    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->d()I

    move-result v2

    invoke-static {v1, v2}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 223
    iget-object v1, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a01b1

    const/4 v0, 0x0

    aget-object v0, p2, v0

    invoke-static {v0}, Ltv/peel/samsung/widget/o;->a(Lcom/peel/control/h;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/16 v0, 0x28

    :goto_5
    const/4 v3, 0x0

    invoke-virtual {v1, p1, v2, v0, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;III)V

    .line 224
    iget-object v1, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a01b3

    const/4 v0, 0x1

    aget-object v0, p2, v0

    invoke-static {v0}, Ltv/peel/samsung/widget/o;->a(Lcom/peel/control/h;)Z

    move-result v0

    if-eqz v0, :cond_6

    const/16 v0, 0x28

    :goto_6
    const/4 v3, 0x1

    invoke-virtual {v1, p1, v2, v0, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;III)V

    goto/16 :goto_3

    .line 223
    :cond_5
    const/16 v0, 0x20

    goto :goto_5

    .line 224
    :cond_6
    const/16 v0, 0x20

    goto :goto_6

    .line 226
    :cond_7
    const v0, 0x7f0a01b1

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 227
    const v0, 0x7f0a01b2

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 228
    const v0, 0x7f0a01b3

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 229
    const v0, 0x7f0a01b1

    iget-object v1, p0, Ltv/peel/samsung/widget/o;->c:Landroid/content/Context;

    const/4 v2, 0x0

    aget-object v2, p2, v2

    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->d()I

    move-result v2

    invoke-static {v1, v2}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 230
    const v0, 0x7f0a01b2

    iget-object v1, p0, Ltv/peel/samsung/widget/o;->c:Landroid/content/Context;

    const/4 v2, 0x1

    aget-object v2, p2, v2

    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->d()I

    move-result v2

    invoke-static {v1, v2}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 231
    const v0, 0x7f0a01b3

    iget-object v1, p0, Ltv/peel/samsung/widget/o;->c:Landroid/content/Context;

    const/4 v2, 0x2

    aget-object v2, p2, v2

    invoke-virtual {v2}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/peel/data/g;->d()I

    move-result v2

    invoke-static {v1, v2}, Lcom/peel/util/bx;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 232
    iget-object v1, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a01b1

    const/4 v0, 0x0

    aget-object v0, p2, v0

    invoke-static {v0}, Ltv/peel/samsung/widget/o;->a(Lcom/peel/control/h;)Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0x28

    :goto_7
    const/4 v3, 0x0

    invoke-virtual {v1, p1, v2, v0, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;III)V

    .line 233
    iget-object v1, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a01b2

    const/4 v0, 0x1

    aget-object v0, p2, v0

    invoke-static {v0}, Ltv/peel/samsung/widget/o;->a(Lcom/peel/control/h;)Z

    move-result v0

    if-eqz v0, :cond_9

    const/16 v0, 0x28

    :goto_8
    const/4 v3, 0x1

    invoke-virtual {v1, p1, v2, v0, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;III)V

    .line 234
    iget-object v1, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a01b3

    const/4 v0, 0x2

    aget-object v0, p2, v0

    invoke-static {v0}, Ltv/peel/samsung/widget/o;->a(Lcom/peel/control/h;)Z

    move-result v0

    if-eqz v0, :cond_a

    const/16 v0, 0x28

    :goto_9
    const/4 v3, 0x2

    invoke-virtual {v1, p1, v2, v0, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;III)V

    goto/16 :goto_3

    .line 232
    :cond_8
    const/16 v0, 0x20

    goto :goto_7

    .line 233
    :cond_9
    const/16 v0, 0x20

    goto :goto_8

    .line 234
    :cond_a
    const/16 v0, 0x20

    goto :goto_9

    :cond_b
    move v6, v0

    goto/16 :goto_2
.end method

.method private static a(Lcom/peel/control/h;)Z
    .locals 2

    .prologue
    .line 311
    invoke-virtual {p0}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/peel/data/g;->a()Ljava/util/Map;

    move-result-object v0

    .line 313
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    if-lez v1, :cond_0

    const-string/jumbo v1, "Power"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 314
    const/4 v0, 0x0

    .line 316
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a()Ltv/peel/widget/h;
    .locals 6

    .prologue
    const v5, 0x7f0a01f1

    const/4 v4, 0x0

    .line 47
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ltv/peel/samsung/widget/o;->b:Z

    if-nez v0, :cond_0

    .line 48
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    iget-object v1, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01ce

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 49
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "is_setup_complete"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 50
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    iget-object v1, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v5, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 55
    :cond_0
    :goto_0
    return-object p0

    .line 51
    :cond_1
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->c:Landroid/content/Context;

    const-string/jumbo v1, "widget_pref"

    invoke-virtual {v0, v1, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "is_setup_complete"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 52
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    iget-object v1, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    const/16 v2, 0x1e

    invoke-virtual {v0, v1, v5, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    goto :goto_0
.end method

.method public a(ILjava/lang/String;Ljava/lang/String;Z)Ltv/peel/widget/h;
    .locals 6

    .prologue
    .line 377
    iget-boolean v0, p0, Ltv/peel/samsung/widget/o;->b:Z

    if-nez v0, :cond_2

    .line 378
    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Ltv/peel/samsung/widget/o;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 380
    const v1, 0x7f0a018e

    const-string/jumbo v2, "setBackgroundResource"

    const v3, 0x7f020475

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 381
    const v1, 0x7f0a01d7

    const-string/jumbo v2, "setBackgroundResource"

    const v3, 0x7f020479

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 382
    const v1, 0x7f0a01d7

    iget-object v2, p0, Ltv/peel/samsung/widget/o;->c:Landroid/content/Context;

    const v3, 0x7f0d000a

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 383
    const v1, 0x7f0a01d7

    const v2, 0x7f020460

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setTextViewCompoundDrawables(IIIII)V

    .line 384
    const v1, 0x7f0a018d

    const-string/jumbo v2, "setBackgroundResource"

    const v3, 0x7f020448

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 385
    const v1, 0x7f0a018d

    const v2, 0x7f02005b

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setTextViewCompoundDrawables(IIIII)V

    .line 386
    const v1, 0x7f0a018d

    iget-object v2, p0, Ltv/peel/samsung/widget/o;->c:Landroid/content/Context;

    const v3, 0x7f0d0098

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 388
    const v1, 0x7f0a0188

    const v2, 0x7f020549

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 389
    const v1, 0x7f0a0189

    const v2, 0x7f020548

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 390
    const v1, 0x7f0a018a

    const v2, 0x7f020544

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 391
    const v1, 0x7f0a018b

    const v2, 0x7f020543

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 392
    const v1, 0x7f0a01b4

    iget-object v2, p0, Ltv/peel/samsung/widget/o;->c:Landroid/content/Context;

    const v3, 0x7f0d0265

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 393
    const v1, 0x7f0a01b5

    iget-object v2, p0, Ltv/peel/samsung/widget/o;->c:Landroid/content/Context;

    const v3, 0x7f0d0263

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 395
    iget-object v1, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    if-eqz v1, :cond_1

    .line 397
    iget-object v1, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a018e

    const/16 v3, 0x14

    invoke-virtual {v1, v0, v2, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 399
    iget-object v1, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a01d7

    const/16 v3, 0x28

    const-string/jumbo v4, "PowerOn"

    invoke-virtual {v1, v0, v2, v3, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 400
    iget-object v1, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a018d

    const/16 v3, 0x8

    const-string/jumbo v4, "MODE"

    invoke-virtual {v1, v0, v2, v3, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 401
    iget-object v1, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a0188

    const/16 v3, 0x8

    const-string/jumbo v4, "UP"

    invoke-virtual {v1, v0, v2, v3, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 402
    iget-object v1, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a0189

    const/16 v3, 0x8

    const-string/jumbo v4, "Down"

    invoke-virtual {v1, v0, v2, v3, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 403
    iget-object v1, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a018a

    const/16 v3, 0x8

    const-string/jumbo v4, "FAN_HIGH"

    invoke-virtual {v1, v0, v2, v3, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 404
    iget-object v1, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a018b

    const/16 v3, 0x8

    const-string/jumbo v4, "FAN_LOW"

    invoke-virtual {v1, v0, v2, v3, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 405
    if-eqz p4, :cond_0

    .line 406
    const v1, 0x7f0a018d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 407
    const v1, 0x7f0a018d

    invoke-virtual {v0, v1, p2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 411
    :cond_0
    iget-object v1, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a0188

    const/16 v3, 0x64

    const-string/jumbo v4, "UP"

    invoke-virtual {v1, v0, v2, v3, v4}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 412
    iget-object v1, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a0189

    const/16 v3, 0x64

    const-string/jumbo v4, "Down"

    invoke-virtual {v1, v0, v2, v3, v4}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 413
    iget-object v1, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a018a

    const/16 v3, 0x64

    const-string/jumbo v4, "FAN_HIGH"

    invoke-virtual {v1, v0, v2, v3, v4}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 414
    iget-object v1, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a018b

    const/16 v3, 0x64

    const-string/jumbo v4, "FAN_LOW"

    invoke-virtual {v1, v0, v2, v3, v4}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 416
    :cond_1
    iget-object v1, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01ec

    invoke-virtual {v1, v2, v0}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    .line 418
    :cond_2
    return-object p0
.end method

.method public a(IZ[Lcom/peel/control/h;)Ltv/peel/widget/h;
    .locals 11

    .prologue
    const/16 v10, 0x64

    const v9, 0x7f0a01b7

    const v8, 0x7f020448

    const/4 v2, 0x0

    const/16 v7, 0x8

    .line 239
    iget-boolean v0, p0, Ltv/peel/samsung/widget/o;->b:Z

    if-nez v0, :cond_4

    .line 241
    new-instance v3, Landroid/widget/RemoteViews;

    iget-object v0, p0, Ltv/peel/samsung/widget/o;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0, p1}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 244
    array-length v4, p3

    move v1, v2

    move v0, v2

    :goto_0
    if-ge v1, v4, :cond_2

    aget-object v5, p3, v1

    .line 245
    invoke-virtual {v5}, Lcom/peel/control/h;->l()Lcom/peel/data/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/peel/data/g;->d()I

    move-result v5

    .line 246
    const/4 v6, 0x2

    if-eq v5, v6, :cond_0

    const/16 v6, 0x14

    if-ne v5, v6, :cond_1

    .line 247
    :cond_0
    const/4 v0, 0x1

    .line 244
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 251
    :cond_2
    if-eqz v0, :cond_5

    .line 252
    const v0, 0x7f0a0180

    invoke-virtual {v3, v0, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 253
    const v0, 0x7f0a0181

    invoke-virtual {v3, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 254
    invoke-virtual {v3, v9, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 262
    :goto_1
    const v0, 0x7f0a018e

    const-string/jumbo v1, "setBackgroundResource"

    const v2, 0x7f020475

    invoke-virtual {v3, v0, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 263
    const-string/jumbo v0, "setBackgroundResource"

    invoke-virtual {v3, v9, v0, v8}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 264
    const v0, 0x7f020478

    invoke-virtual {v3, v9, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 265
    const v0, 0x7f0a01b8

    const-string/jumbo v1, "setBackgroundResource"

    invoke-virtual {v3, v0, v1, v8}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 266
    const v0, 0x7f0a01b8

    const v1, 0x7f020477

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 267
    const v0, 0x7f0a0182

    const-string/jumbo v1, "setBackgroundResource"

    invoke-virtual {v3, v0, v1, v8}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 268
    const v0, 0x7f0a0182

    const v1, 0x7f020474

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 269
    const v0, 0x7f0a0183

    const-string/jumbo v1, "setBackgroundResource"

    invoke-virtual {v3, v0, v1, v8}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 270
    const v0, 0x7f0a0183

    const v1, 0x7f020470

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 271
    const v0, 0x7f0a0184

    const-string/jumbo v1, "setBackgroundResource"

    invoke-virtual {v3, v0, v1, v8}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 272
    const v0, 0x7f0a0184

    const v1, 0x7f02046f

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 274
    const v0, 0x7f0a0188

    const v1, 0x7f020549

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 275
    const v0, 0x7f0a0189

    const v1, 0x7f020548

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 276
    const v0, 0x7f0a018a

    const v1, 0x7f020544

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 277
    const v0, 0x7f0a018b

    const v1, 0x7f020543

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 278
    const v0, 0x7f0a01b4

    iget-object v1, p0, Ltv/peel/samsung/widget/o;->c:Landroid/content/Context;

    const v2, 0x7f0d0262

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 279
    const v0, 0x7f0a01b5

    iget-object v1, p0, Ltv/peel/samsung/widget/o;->c:Landroid/content/Context;

    const v2, 0x7f0d0261

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 281
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    if-eqz v0, :cond_3

    .line 282
    invoke-direct {p0, v3, p3}, Ltv/peel/samsung/widget/o;->a(Landroid/widget/RemoteViews;[Lcom/peel/control/h;)V

    .line 285
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const v1, 0x7f0a018e

    const/16 v2, 0x14

    invoke-virtual {v0, v3, v1, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 288
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const v1, 0x7f0a0183

    const-string/jumbo v2, "Channel_Up"

    invoke-virtual {v0, v3, v1, v7, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 289
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const v1, 0x7f0a0184

    const-string/jumbo v2, "Channel_Down"

    invoke-virtual {v0, v3, v1, v7, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 290
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const v1, 0x7f0a0182

    const-string/jumbo v2, "Fast_Forward"

    invoke-virtual {v0, v3, v1, v7, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 291
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const v1, 0x7f0a01b8

    const-string/jumbo v2, "Mute"

    invoke-virtual {v0, v3, v1, v7, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 292
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const-string/jumbo v1, "Play"

    invoke-virtual {v0, v3, v9, v7, v1}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 293
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const v1, 0x7f0a018b

    const-string/jumbo v2, "Channel_Down"

    invoke-virtual {v0, v3, v1, v7, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 294
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const v1, 0x7f0a018a

    const-string/jumbo v2, "Channel_Up"

    invoke-virtual {v0, v3, v1, v7, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 295
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const v1, 0x7f0a0189

    const-string/jumbo v2, "Volume_Down"

    invoke-virtual {v0, v3, v1, v7, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 296
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const v1, 0x7f0a0188

    const-string/jumbo v2, "Volume_Up"

    invoke-virtual {v0, v3, v1, v7, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 299
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const v1, 0x7f0a018b

    const-string/jumbo v2, "Channel_Down"

    invoke-virtual {v0, v3, v1, v10, v2}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 300
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const v1, 0x7f0a018a

    const-string/jumbo v2, "Channel_Up"

    invoke-virtual {v0, v3, v1, v10, v2}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 301
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const v1, 0x7f0a0189

    const-string/jumbo v2, "Volume_Down"

    invoke-virtual {v0, v3, v1, v10, v2}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 302
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const v1, 0x7f0a0188

    const-string/jumbo v2, "Volume_Up"

    invoke-virtual {v0, v3, v1, v10, v2}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 304
    :cond_3
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    const v1, 0x7f0a01ec

    invoke-virtual {v0, v1, v3}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    .line 307
    :cond_4
    return-object p0

    .line 257
    :cond_5
    const v0, 0x7f0a0180

    invoke-virtual {v3, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 258
    const v0, 0x7f0a0181

    invoke-virtual {v3, v0, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 259
    const/4 v0, 0x4

    invoke-virtual {v3, v9, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_1
.end method

.method public a(IZ[Lcom/peel/control/h;Z)Ltv/peel/widget/h;
    .locals 9

    .prologue
    const v8, 0x7f0a0186

    const v7, 0x7f0a0185

    const v6, 0x7f0a01b7

    const v4, 0x7f020448

    const/16 v5, 0x8

    .line 322
    iget-boolean v0, p0, Ltv/peel/samsung/widget/o;->b:Z

    if-nez v0, :cond_2

    .line 324
    new-instance v1, Landroid/widget/RemoteViews;

    iget-object v0, p0, Ltv/peel/samsung/widget/o;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0, p1}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 325
    const/4 v0, 0x4

    invoke-virtual {v1, v6, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 326
    const v0, 0x7f0a018e

    const-string/jumbo v2, "setBackgroundResource"

    const v3, 0x7f020475

    invoke-virtual {v1, v0, v2, v3}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 327
    const-string/jumbo v0, "setBackgroundResource"

    invoke-virtual {v1, v6, v0, v4}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 328
    const v0, 0x7f020478

    invoke-virtual {v1, v6, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 329
    const v0, 0x7f0a01b8

    const-string/jumbo v2, "setBackgroundResource"

    invoke-virtual {v1, v0, v2, v4}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 330
    const v0, 0x7f0a01b8

    const v2, 0x7f020477

    invoke-virtual {v1, v0, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 331
    const-string/jumbo v0, "setBackgroundResource"

    invoke-virtual {v1, v7, v0, v4}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 332
    const v0, 0x7f02047b

    invoke-virtual {v1, v7, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 333
    const-string/jumbo v0, "setBackgroundResource"

    invoke-virtual {v1, v8, v0, v4}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 334
    const v0, 0x7f020474

    invoke-virtual {v1, v8, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 335
    const v0, 0x7f0a0187

    const-string/jumbo v2, "setBackgroundResource"

    invoke-virtual {v1, v0, v2, v4}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 336
    const v0, 0x7f0a0187

    const v2, 0x7f020478

    invoke-virtual {v1, v0, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 338
    const v0, 0x7f0a0188

    const v2, 0x7f020549

    invoke-virtual {v1, v0, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 339
    const v0, 0x7f0a0189

    const v2, 0x7f020548

    invoke-virtual {v1, v0, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 340
    const v0, 0x7f0a01b4

    iget-object v2, p0, Ltv/peel/samsung/widget/o;->c:Landroid/content/Context;

    const v3, 0x7f0d0262

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 342
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    if-eqz v0, :cond_1

    .line 343
    invoke-direct {p0, v1, p3}, Ltv/peel/samsung/widget/o;->a(Landroid/widget/RemoteViews;[Lcom/peel/control/h;)V

    .line 345
    const-string/jumbo v0, "Play"

    .line 346
    if-eqz p4, :cond_0

    .line 347
    const-string/jumbo v0, "Select"

    .line 354
    :cond_0
    iget-object v2, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const v3, 0x7f0a018e

    const/16 v4, 0x14

    invoke-virtual {v2, v1, v3, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 357
    iget-object v2, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const v3, 0x7f0a01b8

    const-string/jumbo v4, "Mute"

    invoke-virtual {v2, v1, v3, v5, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 358
    iget-object v2, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    invoke-virtual {v2, v1, v6, v5, v0}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 359
    iget-object v2, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const v3, 0x7f0a0187

    invoke-virtual {v2, v1, v3, v5, v0}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 360
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const-string/jumbo v2, "Fast_Forward"

    invoke-virtual {v0, v1, v8, v5, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 361
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const-string/jumbo v2, "Rewind"

    invoke-virtual {v0, v1, v7, v5, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 362
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a0189

    const-string/jumbo v3, "Volume_Down"

    invoke-virtual {v0, v1, v2, v5, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 363
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a0188

    const-string/jumbo v3, "Volume_Up"

    invoke-virtual {v0, v1, v2, v5, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 366
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a0189

    const/16 v3, 0x64

    const-string/jumbo v4, "Volume_Down"

    invoke-virtual {v0, v1, v2, v3, v4}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 367
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    const v2, 0x7f0a0188

    const/16 v3, 0x64

    const-string/jumbo v4, "Volume_Up"

    invoke-virtual {v0, v1, v2, v3, v4}, Ltv/peel/widget/util/a;->b(Landroid/widget/RemoteViews;IILjava/lang/String;)V

    .line 369
    :cond_1
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01ec

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    .line 372
    :cond_2
    return-object p0
.end method

.method public a(Landroid/content/Context;ILtv/peel/widget/util/a;)Ltv/peel/widget/h;
    .locals 2

    .prologue
    .line 37
    iput-object p1, p0, Ltv/peel/samsung/widget/o;->c:Landroid/content/Context;

    .line 38
    iput-object p3, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    .line 39
    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Ltv/peel/samsung/widget/o;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    .line 40
    return-object p0
.end method

.method public a(Landroid/content/Context;ILtv/peel/widget/util/a;Ljava/lang/String;Z)Ltv/peel/widget/h;
    .locals 0

    .prologue
    .line 60
    return-object p0
.end method

.method public a(Ljava/lang/String;[Ljava/lang/String;Z)Ltv/peel/widget/h;
    .locals 10

    .prologue
    const v9, 0x7f0a01f6

    const v8, 0x7f0a01f5

    const v7, 0x7f0a01f4

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 135
    if-eqz p2, :cond_0

    array-length v0, p2

    if-lez v0, :cond_0

    .line 136
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01ee

    invoke-virtual {v0, v2, p1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 137
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    const v3, 0x7f0a01ce

    const/4 v4, 0x3

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 139
    array-length v0, p2

    if-ne v0, v6, :cond_0

    .line 141
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01ed

    const-string/jumbo v3, "setEnabled"

    invoke-virtual {v0, v2, v3, v1}, Landroid/widget/RemoteViews;->setBoolean(ILjava/lang/String;Z)V

    .line 142
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01ee

    const/16 v3, 0x7f

    const/16 v4, 0x89

    const/16 v5, 0x92

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 146
    :cond_0
    if-eqz p3, :cond_1

    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    array-length v0, p2

    if-le v0, v6, :cond_1

    .line 147
    iget-object v2, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    const v3, 0x7f0a01f3

    if-eqz p3, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v2, v3, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 148
    array-length v0, p2

    packed-switch v0, :pswitch_data_0

    .line 180
    :cond_1
    :goto_1
    return-object p0

    .line 147
    :cond_2
    const/16 v0, 0x8

    goto :goto_0

    .line 150
    :pswitch_0
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    const v3, 0x7f0a01fa

    const/16 v4, 0x1c

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 151
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01fa

    const/4 v3, 0x6

    aget-object v3, p2, v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 152
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01fa

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 154
    :pswitch_1
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    const v3, 0x7f0a01f9

    const/16 v4, 0x1b

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 155
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01f9

    const/4 v3, 0x5

    aget-object v3, p2, v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 156
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01f9

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 158
    :pswitch_2
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    const v3, 0x7f0a01f8

    const/16 v4, 0x1a

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 159
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01f8

    const/4 v3, 0x4

    aget-object v3, p2, v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 160
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01f8

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 162
    :pswitch_3
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    const v3, 0x7f0a01f7

    const/16 v4, 0x19

    invoke-virtual {v0, v2, v3, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 163
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01f7

    const/4 v3, 0x3

    aget-object v3, p2, v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 164
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01f7

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 166
    :pswitch_4
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    const/16 v3, 0x18

    invoke-virtual {v0, v2, v9, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 167
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    const/4 v2, 0x2

    aget-object v2, p2, v2

    invoke-virtual {v0, v9, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 168
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    invoke-virtual {v0, v9, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 170
    :pswitch_5
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    const/16 v3, 0x17

    invoke-virtual {v0, v2, v8, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 171
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    aget-object v2, p2, v6

    invoke-virtual {v0, v8, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 172
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    invoke-virtual {v0, v8, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 174
    :pswitch_6
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    const/16 v3, 0x16

    invoke-virtual {v0, v2, v7, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 175
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    aget-object v2, p2, v1

    invoke-virtual {v0, v7, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 176
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    invoke-virtual {v0, v7, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_1

    .line 148
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Z)Ltv/peel/widget/h;
    .locals 9

    .prologue
    const/4 v8, 0x2

    const v7, 0x7f0a01f0

    const v6, 0x7f0a01ef

    const v5, 0x7f0a004d

    const/4 v4, 0x0

    .line 66
    iput-boolean p1, p0, Ltv/peel/samsung/widget/o;->b:Z

    .line 67
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    if-eqz v0, :cond_1

    .line 68
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->c:Landroid/content/Context;

    const-string/jumbo v1, "widget_pref"

    invoke-virtual {v0, v1, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 70
    iget-boolean v1, p0, Ltv/peel/samsung/widget/o;->b:Z

    if-nez v1, :cond_4

    .line 71
    iget-object v1, p0, Ltv/peel/samsung/widget/o;->c:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v2, "is_setup_complete"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_2

    .line 72
    iget-object v1, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    iget-object v2, p0, Ltv/peel/samsung/widget/o;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0236

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v5, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 77
    :cond_0
    :goto_0
    const-string/jumbo v1, "clear_mode"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 78
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    iget-object v1, p0, Ltv/peel/samsung/widget/o;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d020f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v7, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 79
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    iget-object v1, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    const/16 v2, 0x1f

    invoke-virtual {v0, v1, v6, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 98
    :cond_1
    :goto_1
    return-object p0

    .line 73
    :cond_2
    const-string/jumbo v1, "is_setup_complete"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 74
    iget-object v1, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    iget-object v2, p0, Ltv/peel/samsung/widget/o;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d038e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v5, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_0

    .line 81
    :cond_3
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    iget-object v1, p0, Ltv/peel/samsung/widget/o;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d021d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v7, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 82
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    iget-object v1, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    invoke-virtual {v0, v1, v6, v8}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    goto :goto_1

    .line 85
    :cond_4
    iget-object v1, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    iget-object v2, p0, Ltv/peel/samsung/widget/o;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d03ee

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v5, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 86
    iget-object v1, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    iget-object v2, p0, Ltv/peel/samsung/widget/o;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0238

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v7, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 87
    iget-object v1, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    iget-object v2, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    invoke-virtual {v1, v2, v6, v8}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 88
    iget-object v1, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01eb

    const/16 v3, 0x8

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 90
    iget-object v1, p0, Ltv/peel/samsung/widget/o;->c:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v2, "is_setup_complete"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_5

    .line 91
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    iget-object v1, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v5, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    goto/16 :goto_1

    .line 92
    :cond_5
    const-string/jumbo v1, "is_setup_complete"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 93
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    iget-object v1, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    const/16 v2, 0x1e

    invoke-virtual {v0, v1, v5, v2}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    goto/16 :goto_1
.end method

.method public b()Landroid/widget/RemoteViews;
    .locals 1

    .prologue
    .line 423
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    return-object v0
.end method

.method public b(Z)Ltv/peel/widget/h;
    .locals 7

    .prologue
    const v6, 0x7f0a01f0

    const v5, 0x7f0a01ef

    const/4 v4, 0x2

    .line 104
    iput-boolean p1, p0, Ltv/peel/samsung/widget/o;->b:Z

    .line 106
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    iget-object v1, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01ed

    const/4 v3, 0x5

    invoke-virtual {v0, v1, v2, v3}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 110
    :cond_0
    iget-boolean v0, p0, Ltv/peel/samsung/widget/o;->b:Z

    if-eqz v0, :cond_1

    .line 112
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00a7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 113
    iget-object v1, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    const v2, 0x7f0a01eb

    const/16 v3, 0x8

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 114
    iget-object v1, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    invoke-virtual {v1, v6, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 115
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    iget-object v1, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    invoke-virtual {v0, v1, v5, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    .line 129
    :goto_0
    return-object p0

    .line 124
    :cond_1
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    iget-object v1, p0, Ltv/peel/samsung/widget/o;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d021d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v6, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 125
    iget-object v0, p0, Ltv/peel/samsung/widget/o;->d:Ltv/peel/widget/util/a;

    iget-object v1, p0, Ltv/peel/samsung/widget/o;->a:Landroid/widget/RemoteViews;

    invoke-virtual {v0, v1, v5, v4}, Ltv/peel/widget/util/a;->a(Landroid/widget/RemoteViews;II)V

    goto :goto_0
.end method
