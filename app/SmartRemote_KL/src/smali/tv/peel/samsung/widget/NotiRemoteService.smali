.class public Ltv/peel/samsung/widget/NotiRemoteService;
.super Landroid/app/Service;


# static fields
.field private static final b:Ljava/lang/String;

.field private static f:Ltv/peel/samsung/widget/a;


# instance fields
.field public a:Landroid/content/BroadcastReceiver;

.field private c:I

.field private d:Ltv/peel/widget/util/a;

.field private e:Ltv/peel/widget/util/b;

.field private g:Z

.field private h:Z

.field private final i:[Ljava/lang/String;

.field private j:Ljava/util/Timer;

.field private k:I

.field private l:Ljava/lang/String;

.field private m:Z

.field private n:Ltv/peel/widget/g;

.field private o:Lcom/peel/control/a;

.field private final p:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final q:Ljava/lang/String;

.field private final r:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-class v0, Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ltv/peel/samsung/widget/NotiRemoteService;->b:Ljava/lang/String;

    .line 80
    const/4 v0, 0x0

    sput-object v0, Ltv/peel/samsung/widget/NotiRemoteService;->f:Ltv/peel/samsung/widget/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 50
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 77
    iput v3, p0, Ltv/peel/samsung/widget/NotiRemoteService;->c:I

    .line 81
    iput-boolean v3, p0, Ltv/peel/samsung/widget/NotiRemoteService;->g:Z

    .line 82
    iput-boolean v3, p0, Ltv/peel/samsung/widget/NotiRemoteService;->h:Z

    .line 83
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "PowerOn"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string/jumbo v2, "PowerOn"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "PowerOn"

    aput-object v2, v0, v1

    iput-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->i:[Ljava/lang/String;

    .line 85
    iput v3, p0, Ltv/peel/samsung/widget/NotiRemoteService;->k:I

    .line 86
    const/4 v0, 0x0

    iput-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->l:Ljava/lang/String;

    .line 87
    iput-boolean v3, p0, Ltv/peel/samsung/widget/NotiRemoteService;->m:Z

    .line 93
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->p:Ljava/util/TreeMap;

    .line 95
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->a:Lcom/peel/c/b;

    if-ne v0, v1, :cond_0

    const-string/jumbo v0, "tv.peel.samsung.app"

    :goto_0
    iput-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->q:Ljava/lang/String;

    .line 98
    sget-object v0, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v0}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/peel/c/b;->a:Lcom/peel/c/b;

    if-ne v0, v1, :cond_1

    const-string/jumbo v0, "android.intent.action.VIEW"

    :goto_1
    iput-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->r:Ljava/lang/String;

    .line 350
    new-instance v0, Ltv/peel/samsung/widget/s;

    invoke-direct {v0, p0}, Ltv/peel/samsung/widget/s;-><init>(Ltv/peel/samsung/widget/NotiRemoteService;)V

    iput-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->a:Landroid/content/BroadcastReceiver;

    return-void

    .line 95
    :cond_0
    const-string/jumbo v0, "tv.peel.smartremote"

    goto :goto_0

    .line 98
    :cond_1
    const-string/jumbo v0, "android.intent.action.MAIN"

    goto :goto_1
.end method

.method static synthetic a(Ltv/peel/samsung/widget/NotiRemoteService;)I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->c:I

    return v0
.end method

.method static synthetic a(Ltv/peel/samsung/widget/NotiRemoteService;I)I
    .locals 0

    .prologue
    .line 50
    iput p1, p0, Ltv/peel/samsung/widget/NotiRemoteService;->c:I

    return p1
.end method

.method static synthetic a(Ltv/peel/samsung/widget/NotiRemoteService;Lcom/peel/control/a;)Lcom/peel/control/a;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Ltv/peel/samsung/widget/NotiRemoteService;->o:Lcom/peel/control/a;

    return-object p1
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Ltv/peel/samsung/widget/NotiRemoteService;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Ltv/peel/samsung/widget/NotiRemoteService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Ltv/peel/samsung/widget/NotiRemoteService;->l:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Ltv/peel/samsung/widget/NotiRemoteService;Ljava/util/Timer;)Ljava/util/Timer;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Ltv/peel/samsung/widget/NotiRemoteService;->j:Ljava/util/Timer;

    return-object p1
.end method

.method private a(Landroid/content/Context;Ltv/peel/samsung/widget/v;)Ltv/peel/samsung/widget/a;
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 556
    sget-object v0, Ltv/peel/samsung/widget/NotiRemoteService;->f:Ltv/peel/samsung/widget/a;

    .line 557
    sget-object v1, Ltv/peel/samsung/widget/NotiRemoteService;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "connectToPeelService "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Ltv/peel/samsung/widget/NotiRemoteService;->c:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 559
    iget v1, p0, Ltv/peel/samsung/widget/NotiRemoteService;->c:I

    if-eq v1, v4, :cond_0

    iget v1, p0, Ltv/peel/samsung/widget/NotiRemoteService;->c:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    :cond_0
    sget-object v1, Ltv/peel/samsung/widget/NotiRemoteService;->f:Ltv/peel/samsung/widget/a;

    if-nez v1, :cond_2

    .line 560
    :cond_1
    const/4 v0, 0x1

    iput v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->c:I

    .line 561
    new-instance v0, Ltv/peel/samsung/widget/a;

    invoke-direct {v0, p1, p2}, Ltv/peel/samsung/widget/a;-><init>(Landroid/content/Context;Ltv/peel/widget/util/c;)V

    .line 566
    :goto_0
    return-object v0

    .line 563
    :cond_2
    iput v4, p0, Ltv/peel/samsung/widget/NotiRemoteService;->c:I

    .line 564
    const-string/jumbo v1, "com.peel.widget.notification.SERVICE_BINDED"

    invoke-direct {p0, p1, v1}, Ltv/peel/samsung/widget/NotiRemoteService;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Z)Ltv/peel/widget/h;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 617
    invoke-static {}, Ltv/peel/samsung/widget/r;->a()Ltv/peel/widget/h;

    move-result-object v0

    const v2, 0x7f0300a1

    iget-object v3, p0, Ltv/peel/samsung/widget/NotiRemoteService;->d:Ltv/peel/widget/util/a;

    sget-object v1, Ltv/peel/samsung/widget/NotiRemoteService;->f:Ltv/peel/samsung/widget/a;

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/peel/control/a;

    iget-object v5, p0, Ltv/peel/samsung/widget/NotiRemoteService;->o:Lcom/peel/control/a;

    aput-object v5, v4, v6

    .line 618
    invoke-virtual {v1, v4}, Ltv/peel/samsung/widget/a;->a([Lcom/peel/control/a;)[Ljava/lang/String;

    move-result-object v1

    aget-object v4, v1, v6

    move-object v1, p0

    move v5, p1

    .line 617
    invoke-interface/range {v0 .. v5}, Ltv/peel/widget/h;->a(Landroid/content/Context;ILtv/peel/widget/util/a;Ljava/lang/String;Z)Ltv/peel/widget/h;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 659
    sget-object v0, Ltv/peel/samsung/widget/NotiRemoteService;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Send action to self "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 660
    new-instance v0, Landroid/content/Intent;

    const-class v1, Ltv/peel/samsung/widget/NotiRemoteService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 661
    invoke-virtual {v0, p2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 662
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 663
    return-void
.end method

.method private a(Landroid/widget/RemoteViews;Landroid/widget/RemoteViews;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v5, 0x1

    const/16 v4, 0x15

    .line 481
    .line 483
    if-eqz p1, :cond_3

    .line 484
    new-instance v0, Landroid/app/Notification$Builder;

    invoke-direct {v0, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 486
    sget-object v1, Lcom/peel/c/a;->b:Lcom/peel/c/e;

    invoke-static {v1}, Lcom/peel/c/a;->a(Lcom/peel/c/e;)Ljava/lang/Object;

    move-result-object v1

    sget-object v2, Lcom/peel/c/b;->a:Lcom/peel/c/b;

    if-ne v1, v2, :cond_4

    .line 487
    const v1, 0x7f0204ec

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v1

    const-string/jumbo v2, "WatchOn"

    .line 488
    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    .line 489
    invoke-virtual {v1, v3}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    .line 496
    :goto_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v4, :cond_0

    .line 497
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    .line 500
    invoke-virtual {v0, v5}, Landroid/app/Notification$Builder;->setVisibility(I)Landroid/app/Notification$Builder;

    .line 503
    :cond_0
    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    .line 504
    iget v0, v2, Landroid/app/Notification;->flags:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v2, Landroid/app/Notification;->flags:I

    .line 511
    const/4 v1, 0x0

    .line 513
    :try_start_0
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string/jumbo v3, "twQuickPanelEvent"

    invoke-virtual {v0, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 517
    :goto_1
    if-eqz v0, :cond_5

    .line 519
    const/16 v1, 0x101

    :try_start_1
    invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V

    .line 520
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v4, :cond_1

    .line 521
    iput-object p1, v2, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2

    .line 534
    :cond_1
    :goto_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v4, :cond_2

    .line 535
    iput-object p2, v2, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 536
    iput-object p1, v2, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    .line 539
    :cond_2
    const-string/jumbo v0, "notification"

    invoke-virtual {p0, v0}, Ltv/peel/samsung/widget/NotiRemoteService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 542
    const/16 v1, 0x7c2

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 543
    sget-object v1, Lcom/peel/util/v;->a:Lcom/peel/util/v;

    invoke-virtual {v1}, Lcom/peel/util/v;->a()V

    .line 545
    invoke-virtual {v0, v5, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 546
    invoke-virtual {p0, v5, v2}, Ltv/peel/samsung/widget/NotiRemoteService;->startForeground(ILandroid/app/Notification;)V

    .line 548
    :cond_3
    return-void

    .line 491
    :cond_4
    const v1, 0x7f020624

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v1

    .line 492
    invoke-virtual {v1, v3}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    move-result-object v1

    const-string/jumbo v2, "Smart Remote"

    .line 493
    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    goto :goto_0

    .line 514
    :catch_0
    move-exception v0

    .line 515
    sget-object v3, Ltv/peel/samsung/widget/NotiRemoteService;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/NoSuchFieldException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    goto :goto_1

    .line 523
    :catch_1
    move-exception v0

    .line 524
    sget-object v1, Ltv/peel/samsung/widget/NotiRemoteService;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 525
    :catch_2
    move-exception v0

    .line 526
    sget-object v1, Ltv/peel/samsung/widget/NotiRemoteService;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 529
    :cond_5
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v4, :cond_1

    .line 530
    iput-object p1, v2, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    goto :goto_2
.end method

.method static synthetic a(Ltv/peel/samsung/widget/NotiRemoteService;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Ltv/peel/samsung/widget/NotiRemoteService;->a(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Ltv/peel/samsung/widget/NotiRemoteService;Z)Z
    .locals 0

    .prologue
    .line 50
    iput-boolean p1, p0, Ltv/peel/samsung/widget/NotiRemoteService;->g:Z

    return p1
.end method

.method static synthetic b()Ltv/peel/samsung/widget/a;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Ltv/peel/samsung/widget/NotiRemoteService;->f:Ltv/peel/samsung/widget/a;

    return-object v0
.end method

.method static synthetic b(Ltv/peel/samsung/widget/NotiRemoteService;Z)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0, p1}, Ltv/peel/samsung/widget/NotiRemoteService;->b(Z)V

    return-void
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 627
    const-string/jumbo v0, "widget_pref"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Ltv/peel/samsung/widget/NotiRemoteService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 628
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 629
    const-string/jumbo v1, "notification_controller_collapsed"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 630
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 631
    return-void
.end method

.method static synthetic b(Ltv/peel/samsung/widget/NotiRemoteService;)Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->g:Z

    return v0
.end method

.method private c()V
    .locals 6

    .prologue
    const/16 v5, 0x15

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 159
    invoke-static {}, Ltv/peel/samsung/widget/r;->a()Ltv/peel/widget/h;

    move-result-object v2

    .line 161
    const-string/jumbo v3, "widget_pref"

    invoke-virtual {p0, v3, v1}, Ltv/peel/samsung/widget/NotiRemoteService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string/jumbo v4, "clear_mode"

    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-direct {p0}, Ltv/peel/samsung/widget/NotiRemoteService;->i()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 162
    invoke-direct {p0}, Ltv/peel/samsung/widget/NotiRemoteService;->i()Z

    move-result v3

    if-nez v3, :cond_1

    :goto_0
    invoke-direct {p0, v0}, Ltv/peel/samsung/widget/NotiRemoteService;->b(Z)V

    .line 164
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v5, :cond_2

    const v0, 0x7f0300b3

    :goto_1
    iget-object v1, p0, Ltv/peel/samsung/widget/NotiRemoteService;->d:Ltv/peel/widget/util/a;

    invoke-interface {v2, p0, v0, v1}, Ltv/peel/widget/h;->a(Landroid/content/Context;ILtv/peel/widget/util/a;)Ltv/peel/widget/h;

    move-result-object v0

    .line 166
    invoke-direct {p0}, Ltv/peel/samsung/widget/NotiRemoteService;->i()Z

    move-result v1

    invoke-interface {v0, v1}, Ltv/peel/widget/h;->a(Z)Ltv/peel/widget/h;

    move-result-object v0

    invoke-interface {v0}, Ltv/peel/widget/h;->a()Ltv/peel/widget/h;

    move-result-object v0

    invoke-interface {v0}, Ltv/peel/widget/h;->b()Landroid/widget/RemoteViews;

    move-result-object v1

    .line 167
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v5, :cond_3

    const v0, 0x7f0300a2

    iget-object v3, p0, Ltv/peel/samsung/widget/NotiRemoteService;->d:Ltv/peel/widget/util/a;

    invoke-interface {v2, p0, v0, v3}, Ltv/peel/widget/h;->a(Landroid/content/Context;ILtv/peel/widget/util/a;)Ltv/peel/widget/h;

    move-result-object v0

    invoke-interface {v0}, Ltv/peel/widget/h;->a()Ltv/peel/widget/h;

    move-result-object v0

    invoke-interface {v0}, Ltv/peel/widget/h;->b()Landroid/widget/RemoteViews;

    move-result-object v0

    .line 168
    :goto_2
    invoke-direct {p0, v1, v0}, Ltv/peel/samsung/widget/NotiRemoteService;->a(Landroid/widget/RemoteViews;Landroid/widget/RemoteViews;)V

    .line 169
    return-void

    :cond_1
    move v0, v1

    .line 162
    goto :goto_0

    .line 164
    :cond_2
    const v0, 0x7f0300b1

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 167
    goto :goto_2
.end method

.method static synthetic c(Ltv/peel/samsung/widget/NotiRemoteService;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ltv/peel/samsung/widget/NotiRemoteService;->n()V

    return-void
.end method

.method static synthetic c(Ltv/peel/samsung/widget/NotiRemoteService;Z)Z
    .locals 0

    .prologue
    .line 50
    iput-boolean p1, p0, Ltv/peel/samsung/widget/NotiRemoteService;->h:Z

    return p1
.end method

.method private d()I
    .locals 2

    .prologue
    .line 364
    invoke-virtual {p0}, Ltv/peel/samsung/widget/NotiRemoteService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 365
    const/4 v0, 0x0

    .line 366
    if-eqz v1, :cond_0

    .line 367
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 369
    :cond_0
    return v0
.end method

.method static synthetic d(Ltv/peel/samsung/widget/NotiRemoteService;)Z
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Ltv/peel/samsung/widget/NotiRemoteService;->i()Z

    move-result v0

    return v0
.end method

.method static synthetic d(Ltv/peel/samsung/widget/NotiRemoteService;Z)Z
    .locals 0

    .prologue
    .line 50
    iput-boolean p1, p0, Ltv/peel/samsung/widget/NotiRemoteService;->m:Z

    return p1
.end method

.method private e()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x3

    .line 440
    iget-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->e:Ltv/peel/widget/util/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->d:Ltv/peel/widget/util/a;

    if-nez v0, :cond_1

    .line 441
    :cond_0
    new-instance v0, Ltv/peel/samsung/widget/t;

    invoke-direct {v0, p0, v5}, Ltv/peel/samsung/widget/t;-><init>(Ltv/peel/samsung/widget/NotiRemoteService;Ltv/peel/samsung/widget/s;)V

    iput-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->e:Ltv/peel/widget/util/b;

    .line 442
    new-instance v0, Ltv/peel/widget/util/a;

    const-string/jumbo v1, "com.peel.widget.notification.BUTTON_PRESSED"

    const-class v2, Ltv/peel/samsung/widget/NotiRemoteService;

    iget-object v3, p0, Ltv/peel/samsung/widget/NotiRemoteService;->e:Ltv/peel/widget/util/b;

    invoke-direct {v0, v1, v2, v3, p0}, Ltv/peel/widget/util/a;-><init>(Ljava/lang/String;Ljava/lang/Class;Ltv/peel/widget/util/b;Landroid/content/Context;)V

    iput-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->d:Ltv/peel/widget/util/a;

    .line 445
    :cond_1
    sget-object v0, Ltv/peel/samsung/widget/NotiRemoteService;->f:Ltv/peel/samsung/widget/a;

    invoke-static {v0}, Ltv/peel/widget/g;->a(Ltv/peel/widget/b;)Ltv/peel/widget/g;

    move-result-object v0

    iput-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->n:Ltv/peel/widget/g;

    .line 447
    iget-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->n:Ltv/peel/widget/g;

    if-nez v0, :cond_5

    .line 453
    iget v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->k:I

    const/16 v1, 0x32

    if-ge v0, v1, :cond_4

    .line 454
    sget-object v0, Ltv/peel/samsung/widget/NotiRemoteService;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "sHelper.isBind() is null. Try to connect again : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Ltv/peel/samsung/widget/NotiRemoteService;->k:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 455
    sget-object v0, Ltv/peel/samsung/widget/NotiRemoteService;->f:Ltv/peel/samsung/widget/a;

    if-eqz v0, :cond_2

    .line 456
    sget-object v0, Ltv/peel/samsung/widget/NotiRemoteService;->f:Ltv/peel/samsung/widget/a;

    invoke-virtual {v0}, Ltv/peel/samsung/widget/a;->h()V

    .line 457
    sput-object v5, Ltv/peel/samsung/widget/NotiRemoteService;->f:Ltv/peel/samsung/widget/a;

    .line 460
    :cond_2
    const/4 v0, 0x1

    iput v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->c:I

    .line 461
    const-string/jumbo v0, "com.peel.widget.notification.DISPLAY_UI"

    invoke-direct {p0, p0, v0}, Ltv/peel/samsung/widget/NotiRemoteService;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 467
    :goto_0
    iget v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->k:I

    .line 477
    :cond_3
    :goto_1
    return-void

    .line 463
    :cond_4
    sget-object v0, Ltv/peel/samsung/widget/NotiRemoteService;->b:Ljava/lang/String;

    const-string/jumbo v1, "Failed to bind to PeelService. Stop self"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    iput v4, p0, Ltv/peel/samsung/widget/NotiRemoteService;->c:I

    .line 465
    const-string/jumbo v0, "com.peel.widget.notification.STOP_PROCESS"

    invoke-direct {p0, p0, v0}, Ltv/peel/samsung/widget/NotiRemoteService;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 469
    :cond_5
    const/4 v0, 0x0

    iput v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->k:I

    .line 470
    iget-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->n:Ltv/peel/widget/g;

    iget-boolean v0, v0, Ltv/peel/widget/g;->d:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->n:Ltv/peel/widget/g;

    iget-boolean v0, v0, Ltv/peel/widget/g;->a:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->n:Ltv/peel/widget/g;

    iget-boolean v0, v0, Ltv/peel/widget/g;->g:Z

    if-eqz v0, :cond_3

    :cond_6
    iget-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->n:Ltv/peel/widget/g;

    iget-object v0, v0, Ltv/peel/widget/g;->f:Lcom/peel/control/a;

    if-nez v0, :cond_3

    .line 472
    :cond_7
    sget-object v0, Ltv/peel/samsung/widget/NotiRemoteService;->b:Ljava/lang/String;

    const-string/jumbo v1, "feature is Off. Stop service"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 473
    iput v4, p0, Ltv/peel/samsung/widget/NotiRemoteService;->c:I

    .line 474
    const-string/jumbo v0, "com.peel.widget.notification.STOP_PROCESS"

    invoke-direct {p0, p0, v0}, Ltv/peel/samsung/widget/NotiRemoteService;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic e(Ltv/peel/samsung/widget/NotiRemoteService;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ltv/peel/samsung/widget/NotiRemoteService;->f()V

    return-void
.end method

.method static synthetic f(Ltv/peel/samsung/widget/NotiRemoteService;)Ltv/peel/widget/g;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->n:Ltv/peel/widget/g;

    return-object v0
.end method

.method private f()V
    .locals 1

    .prologue
    .line 552
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ltv/peel/samsung/widget/NotiRemoteService;->stopForeground(Z)V

    .line 553
    return-void
.end method

.method private g()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 570
    iget v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->c:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->n:Ltv/peel/widget/g;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->n:Ltv/peel/widget/g;

    iget-boolean v0, v0, Ltv/peel/widget/g;->d:Z

    if-ne v0, v3, :cond_3

    .line 571
    iget-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->o:Lcom/peel/control/a;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->n:Ltv/peel/widget/g;

    iget-boolean v0, v0, Ltv/peel/widget/g;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->n:Ltv/peel/widget/g;

    iget-boolean v0, v0, Ltv/peel/widget/g;->g:Z

    if-eqz v0, :cond_2

    .line 572
    :cond_0
    invoke-direct {p0}, Ltv/peel/samsung/widget/NotiRemoteService;->m()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 583
    sget-object v0, Ltv/peel/samsung/widget/NotiRemoteService;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateUI() - Unknown screen layout :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Ltv/peel/samsung/widget/NotiRemoteService;->m()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 598
    :cond_1
    :goto_0
    return-void

    .line 574
    :sswitch_0
    invoke-direct {p0}, Ltv/peel/samsung/widget/NotiRemoteService;->j()V

    goto :goto_0

    .line 577
    :sswitch_1
    invoke-direct {p0}, Ltv/peel/samsung/widget/NotiRemoteService;->l()V

    goto :goto_0

    .line 580
    :sswitch_2
    invoke-direct {p0}, Ltv/peel/samsung/widget/NotiRemoteService;->k()V

    goto :goto_0

    .line 586
    :cond_2
    iget-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->n:Ltv/peel/widget/g;

    iget-boolean v0, v0, Ltv/peel/widget/g;->a:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->n:Ltv/peel/widget/g;

    iget-boolean v0, v0, Ltv/peel/widget/g;->g:Z

    if-nez v0, :cond_1

    .line 587
    invoke-direct {p0}, Ltv/peel/samsung/widget/NotiRemoteService;->c()V

    goto :goto_0

    .line 590
    :cond_3
    sget-object v0, Ltv/peel/samsung/widget/NotiRemoteService;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Wrong controller state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Ltv/peel/samsung/widget/NotiRemoteService;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 591
    iget-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->n:Ltv/peel/widget/g;

    if-eqz v0, :cond_4

    .line 592
    sget-object v0, Ltv/peel/samsung/widget/NotiRemoteService;->b:Ljava/lang/String;

    iget-object v1, p0, Ltv/peel/samsung/widget/NotiRemoteService;->n:Ltv/peel/widget/g;

    invoke-virtual {v1}, Ltv/peel/widget/g;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 596
    :goto_1
    new-instance v0, Ltv/peel/samsung/widget/v;

    invoke-direct {v0, p0, v3}, Ltv/peel/samsung/widget/v;-><init>(Ltv/peel/samsung/widget/NotiRemoteService;Z)V

    invoke-direct {p0, p0, v0}, Ltv/peel/samsung/widget/NotiRemoteService;->a(Landroid/content/Context;Ltv/peel/samsung/widget/v;)Ltv/peel/samsung/widget/a;

    goto :goto_0

    .line 594
    :cond_4
    sget-object v0, Ltv/peel/samsung/widget/NotiRemoteService;->b:Ljava/lang/String;

    const-string/jumbo v1, "mRemoteControlSettings is NULL"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 572
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5 -> :sswitch_1
        0x6 -> :sswitch_2
    .end sparse-switch
.end method

.method static synthetic g(Ltv/peel/samsung/widget/NotiRemoteService;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->i:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Ltv/peel/samsung/widget/NotiRemoteService;)Lcom/peel/control/a;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->o:Lcom/peel/control/a;

    return-object v0
.end method

.method private h()Ltv/peel/widget/h;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 607
    invoke-static {}, Ltv/peel/samsung/widget/r;->a()Ltv/peel/widget/h;

    move-result-object v1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v0, v2, :cond_0

    const v0, 0x7f0300af

    :goto_0
    iget-object v2, p0, Ltv/peel/samsung/widget/NotiRemoteService;->d:Ltv/peel/widget/util/a;

    invoke-interface {v1, p0, v0, v2}, Ltv/peel/widget/h;->a(Landroid/content/Context;ILtv/peel/widget/util/a;)Ltv/peel/widget/h;

    move-result-object v0

    invoke-direct {p0}, Ltv/peel/samsung/widget/NotiRemoteService;->i()Z

    move-result v1

    invoke-interface {v0, v1}, Ltv/peel/widget/h;->b(Z)Ltv/peel/widget/h;

    move-result-object v0

    sget-object v1, Ltv/peel/samsung/widget/NotiRemoteService;->f:Ltv/peel/samsung/widget/a;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/peel/control/a;

    iget-object v3, p0, Ltv/peel/samsung/widget/NotiRemoteService;->o:Lcom/peel/control/a;

    aput-object v3, v2, v4

    .line 608
    invoke-virtual {v1, v2}, Ltv/peel/samsung/widget/a;->a([Lcom/peel/control/a;)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v4

    sget-object v2, Ltv/peel/samsung/widget/NotiRemoteService;->f:Ltv/peel/samsung/widget/a;

    iget-object v3, p0, Ltv/peel/samsung/widget/NotiRemoteService;->n:Ltv/peel/widget/g;

    iget-object v3, v3, Ltv/peel/widget/g;->e:[Lcom/peel/control/a;

    invoke-virtual {v2, v3}, Ltv/peel/samsung/widget/a;->a([Lcom/peel/control/a;)[Ljava/lang/String;

    move-result-object v2

    iget-boolean v3, p0, Ltv/peel/samsung/widget/NotiRemoteService;->g:Z

    invoke-interface {v0, v1, v2, v3}, Ltv/peel/widget/h;->a(Ljava/lang/String;[Ljava/lang/String;Z)Ltv/peel/widget/h;

    move-result-object v0

    return-object v0

    .line 607
    :cond_0
    const v0, 0x7f0300ac

    goto :goto_0
.end method

.method static synthetic i(Ltv/peel/samsung/widget/NotiRemoteService;)Ljava/util/TreeMap;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->p:Ljava/util/TreeMap;

    return-object v0
.end method

.method private i()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 622
    const-string/jumbo v0, "widget_pref"

    invoke-virtual {p0, v0, v2}, Ltv/peel/samsung/widget/NotiRemoteService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 623
    const-string/jumbo v1, "notification_controller_collapsed"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method static synthetic j(Ltv/peel/samsung/widget/NotiRemoteService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->r:Ljava/lang/String;

    return-object v0
.end method

.method private j()V
    .locals 5

    .prologue
    const/16 v3, 0x15

    .line 634
    invoke-direct {p0}, Ltv/peel/samsung/widget/NotiRemoteService;->h()Ltv/peel/widget/h;

    move-result-object v2

    .line 635
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v3, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ltv/peel/samsung/widget/NotiRemoteService;->a(Z)Ltv/peel/widget/h;

    move-result-object v0

    invoke-interface {v0}, Ltv/peel/widget/h;->b()Landroid/widget/RemoteViews;

    move-result-object v0

    .line 636
    :goto_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v3, :cond_1

    const v1, 0x7f0300a4

    :goto_1
    iget-boolean v3, p0, Ltv/peel/samsung/widget/NotiRemoteService;->h:Z

    iget-object v4, p0, Ltv/peel/samsung/widget/NotiRemoteService;->o:Lcom/peel/control/a;

    invoke-virtual {v4}, Lcom/peel/control/a;->e()[Lcom/peel/control/h;

    move-result-object v4

    invoke-interface {v2, v1, v3, v4}, Ltv/peel/widget/h;->a(IZ[Lcom/peel/control/h;)Ltv/peel/widget/h;

    move-result-object v1

    invoke-interface {v1}, Ltv/peel/widget/h;->b()Landroid/widget/RemoteViews;

    move-result-object v1

    .line 637
    invoke-direct {p0, v1, v0}, Ltv/peel/samsung/widget/NotiRemoteService;->a(Landroid/widget/RemoteViews;Landroid/widget/RemoteViews;)V

    .line 638
    return-void

    .line 635
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 636
    :cond_1
    const v1, 0x7f0300a3

    goto :goto_1
.end method

.method static synthetic k(Ltv/peel/samsung/widget/NotiRemoteService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->q:Ljava/lang/String;

    return-object v0
.end method

.method private k()V
    .locals 6

    .prologue
    const/16 v4, 0x15

    const/4 v1, 0x0

    .line 641
    invoke-direct {p0}, Ltv/peel/samsung/widget/NotiRemoteService;->h()Ltv/peel/widget/h;

    move-result-object v3

    .line 643
    iget-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->n:Ltv/peel/widget/g;

    if-eqz v0, :cond_2

    .line 644
    iget-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->n:Ltv/peel/widget/g;

    iget-object v2, p0, Ltv/peel/samsung/widget/NotiRemoteService;->o:Lcom/peel/control/a;

    invoke-virtual {v0, v2}, Ltv/peel/widget/g;->b(Lcom/peel/control/a;)Z

    move-result v0

    .line 646
    :goto_0
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v2, v4, :cond_0

    invoke-direct {p0, v1}, Ltv/peel/samsung/widget/NotiRemoteService;->a(Z)Ltv/peel/widget/h;

    move-result-object v1

    invoke-interface {v1}, Ltv/peel/widget/h;->b()Landroid/widget/RemoteViews;

    move-result-object v1

    .line 647
    :goto_1
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v2, v4, :cond_1

    const v2, 0x7f0300a6

    :goto_2
    iget-boolean v4, p0, Ltv/peel/samsung/widget/NotiRemoteService;->h:Z

    iget-object v5, p0, Ltv/peel/samsung/widget/NotiRemoteService;->o:Lcom/peel/control/a;

    invoke-virtual {v5}, Lcom/peel/control/a;->e()[Lcom/peel/control/h;

    move-result-object v5

    invoke-interface {v3, v2, v4, v5, v0}, Ltv/peel/widget/h;->a(IZ[Lcom/peel/control/h;Z)Ltv/peel/widget/h;

    move-result-object v0

    invoke-interface {v0}, Ltv/peel/widget/h;->b()Landroid/widget/RemoteViews;

    move-result-object v0

    .line 648
    invoke-direct {p0, v0, v1}, Ltv/peel/samsung/widget/NotiRemoteService;->a(Landroid/widget/RemoteViews;Landroid/widget/RemoteViews;)V

    .line 649
    return-void

    .line 646
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 647
    :cond_1
    const v2, 0x7f0300a5

    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method private l()V
    .locals 6

    .prologue
    const/16 v3, 0x15

    .line 652
    invoke-direct {p0}, Ltv/peel/samsung/widget/NotiRemoteService;->h()Ltv/peel/widget/h;

    move-result-object v2

    .line 653
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v3, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Ltv/peel/samsung/widget/NotiRemoteService;->a(Z)Ltv/peel/widget/h;

    move-result-object v0

    invoke-interface {v0}, Ltv/peel/widget/h;->b()Landroid/widget/RemoteViews;

    move-result-object v0

    .line 654
    :goto_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v3, :cond_1

    const v1, 0x7f0300a8

    :goto_1
    sget-object v3, Ltv/peel/samsung/widget/NotiRemoteService;->f:Ltv/peel/samsung/widget/a;

    invoke-virtual {v3}, Ltv/peel/samsung/widget/a;->d()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ltv/peel/samsung/widget/NotiRemoteService;->f:Ltv/peel/samsung/widget/a;

    invoke-virtual {v4}, Ltv/peel/samsung/widget/a;->c()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Ltv/peel/samsung/widget/NotiRemoteService;->f:Ltv/peel/samsung/widget/a;

    invoke-virtual {v5}, Ltv/peel/samsung/widget/a;->b()Z

    move-result v5

    invoke-interface {v2, v1, v3, v4, v5}, Ltv/peel/widget/h;->a(ILjava/lang/String;Ljava/lang/String;Z)Ltv/peel/widget/h;

    move-result-object v1

    invoke-interface {v1}, Ltv/peel/widget/h;->b()Landroid/widget/RemoteViews;

    move-result-object v1

    .line 655
    invoke-direct {p0, v1, v0}, Ltv/peel/samsung/widget/NotiRemoteService;->a(Landroid/widget/RemoteViews;Landroid/widget/RemoteViews;)V

    .line 656
    return-void

    .line 653
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 654
    :cond_1
    const v1, 0x7f0300a7

    goto :goto_1
.end method

.method static synthetic l(Ltv/peel/samsung/widget/NotiRemoteService;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ltv/peel/samsung/widget/NotiRemoteService;->g()V

    return-void
.end method

.method private m()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 666
    .line 667
    iget-object v1, p0, Ltv/peel/samsung/widget/NotiRemoteService;->n:Ltv/peel/widget/g;

    if-eqz v1, :cond_0

    .line 668
    iget-object v1, p0, Ltv/peel/samsung/widget/NotiRemoteService;->n:Ltv/peel/widget/g;

    iget-object v2, p0, Ltv/peel/samsung/widget/NotiRemoteService;->o:Lcom/peel/control/a;

    invoke-virtual {v1, v2}, Ltv/peel/widget/g;->a(Lcom/peel/control/a;)I

    move-result v1

    .line 670
    sparse-switch v1, :sswitch_data_0

    .line 681
    const/4 v0, 0x6

    .line 682
    sget-object v2, Ltv/peel/samsung/widget/NotiRemoteService;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "getScreenLayoutType() - Unknown device type :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 687
    :cond_0
    :goto_0
    :sswitch_0
    return v0

    .line 678
    :sswitch_1
    const/4 v0, 0x5

    .line 679
    goto :goto_0

    .line 670
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0xa -> :sswitch_0
        0x12 -> :sswitch_1
        0x14 -> :sswitch_0
    .end sparse-switch
.end method

.method static synthetic m(Ltv/peel/samsung/widget/NotiRemoteService;)Ljava/util/Timer;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->j:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic n(Ltv/peel/samsung/widget/NotiRemoteService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->l:Ljava/lang/String;

    return-object v0
.end method

.method private n()V
    .locals 5

    .prologue
    .line 692
    const-string/jumbo v0, "statusbar"

    .line 693
    const-string/jumbo v0, "statusbar"

    invoke-virtual {p0, v0}, Ltv/peel/samsung/widget/NotiRemoteService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 695
    if-eqz v2, :cond_0

    .line 696
    const/4 v1, 0x0

    .line 698
    :try_start_0
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string/jumbo v3, "collapsePanels"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 702
    :goto_0
    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    .line 704
    const/4 v1, 0x0

    :try_start_1
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_3

    .line 714
    :cond_0
    :goto_1
    return-void

    .line 699
    :catch_0
    move-exception v0

    .line 700
    sget-object v3, Ltv/peel/samsung/widget/NotiRemoteService;->b:Ljava/lang/String;

    sget-object v4, Ltv/peel/samsung/widget/NotiRemoteService;->b:Ljava/lang/String;

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    goto :goto_0

    .line 705
    :catch_1
    move-exception v0

    .line 706
    sget-object v1, Ltv/peel/samsung/widget/NotiRemoteService;->b:Ljava/lang/String;

    sget-object v2, Ltv/peel/samsung/widget/NotiRemoteService;->b:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 707
    :catch_2
    move-exception v0

    .line 708
    sget-object v1, Ltv/peel/samsung/widget/NotiRemoteService;->b:Ljava/lang/String;

    sget-object v2, Ltv/peel/samsung/widget/NotiRemoteService;->b:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 709
    :catch_3
    move-exception v0

    .line 710
    sget-object v1, Ltv/peel/samsung/widget/NotiRemoteService;->b:Ljava/lang/String;

    sget-object v2, Ltv/peel/samsung/widget/NotiRemoteService;->b:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method static synthetic o(Ltv/peel/samsung/widget/NotiRemoteService;)Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->m:Z

    return v0
.end method

.method static synthetic p(Ltv/peel/samsung/widget/NotiRemoteService;)I
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Ltv/peel/samsung/widget/NotiRemoteService;->d()I

    move-result v0

    return v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 173
    const/4 v0, 0x0

    return-object v0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 139
    sget-object v0, Ltv/peel/samsung/widget/NotiRemoteService;->b:Ljava/lang/String;

    const-string/jumbo v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    invoke-direct {p0}, Ltv/peel/samsung/widget/NotiRemoteService;->f()V

    .line 143
    iget-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->a:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 145
    :try_start_0
    iget-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->a:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Ltv/peel/samsung/widget/NotiRemoteService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 149
    :goto_0
    iput-object v2, p0, Ltv/peel/samsung/widget/NotiRemoteService;->a:Landroid/content/BroadcastReceiver;

    .line 152
    :cond_0
    sget-object v0, Ltv/peel/samsung/widget/NotiRemoteService;->f:Ltv/peel/samsung/widget/a;

    if-eqz v0, :cond_1

    .line 153
    sget-object v0, Ltv/peel/samsung/widget/NotiRemoteService;->f:Ltv/peel/samsung/widget/a;

    invoke-virtual {v0}, Ltv/peel/samsung/widget/a;->h()V

    .line 154
    sput-object v2, Ltv/peel/samsung/widget/NotiRemoteService;->f:Ltv/peel/samsung/widget/a;

    .line 156
    :cond_1
    return-void

    .line 146
    :catch_0
    move-exception v0

    .line 147
    sget-object v0, Ltv/peel/samsung/widget/NotiRemoteService;->b:Ljava/lang/String;

    const-string/jumbo v1, "mOrientationChangeReceier was not registered"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 374
    const-string/jumbo v0, ""

    .line 377
    if-eqz p1, :cond_0

    .line 378
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 380
    :cond_0
    sget-object v1, Ltv/peel/samsung/widget/NotiRemoteService;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "action "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    const-string/jumbo v1, "com.peel.widget.notification.SETUP_SERVICE_CONNECTION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 383
    iget v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->c:I

    if-nez v0, :cond_1

    .line 391
    new-instance v0, Ltv/peel/samsung/widget/v;

    invoke-direct {v0, p0, v4}, Ltv/peel/samsung/widget/v;-><init>(Ltv/peel/samsung/widget/NotiRemoteService;Z)V

    invoke-direct {p0, p0, v0}, Ltv/peel/samsung/widget/NotiRemoteService;->a(Landroid/content/Context;Ltv/peel/samsung/widget/v;)Ltv/peel/samsung/widget/a;

    move-result-object v0

    sput-object v0, Ltv/peel/samsung/widget/NotiRemoteService;->f:Ltv/peel/samsung/widget/a;

    .line 435
    :cond_1
    :goto_0
    return v4

    .line 394
    :cond_2
    const-string/jumbo v1, "com.peel.widget.notification.DISPLAY_UI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 398
    iput-boolean v5, p0, Ltv/peel/samsung/widget/NotiRemoteService;->g:Z

    .line 399
    new-instance v0, Ltv/peel/samsung/widget/v;

    invoke-direct {v0, p0, v4}, Ltv/peel/samsung/widget/v;-><init>(Ltv/peel/samsung/widget/NotiRemoteService;Z)V

    invoke-direct {p0, p0, v0}, Ltv/peel/samsung/widget/NotiRemoteService;->a(Landroid/content/Context;Ltv/peel/samsung/widget/v;)Ltv/peel/samsung/widget/a;

    move-result-object v0

    sput-object v0, Ltv/peel/samsung/widget/NotiRemoteService;->f:Ltv/peel/samsung/widget/a;

    goto :goto_0

    .line 401
    :cond_3
    const-string/jumbo v1, "com.peel.widget.notification.HIDE_UI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 402
    iput-boolean v5, p0, Ltv/peel/samsung/widget/NotiRemoteService;->g:Z

    goto :goto_0

    .line 405
    :cond_4
    const-string/jumbo v1, "com.peel.widget.notification.SERVICE_BINDED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 406
    invoke-direct {p0}, Ltv/peel/samsung/widget/NotiRemoteService;->e()V

    .line 407
    iget v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->c:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->n:Ltv/peel/widget/g;

    if-eqz v0, :cond_1

    .line 408
    sget-object v0, Ltv/peel/samsung/widget/NotiRemoteService;->b:Ljava/lang/String;

    iget-object v1, p0, Ltv/peel/samsung/widget/NotiRemoteService;->n:Ltv/peel/widget/g;

    invoke-virtual {v1}, Ltv/peel/widget/g;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 412
    iget-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->n:Ltv/peel/widget/g;

    iget-object v0, v0, Ltv/peel/widget/g;->f:Lcom/peel/control/a;

    if-eqz v0, :cond_5

    .line 413
    iget-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->n:Ltv/peel/widget/g;

    iget-object v0, v0, Ltv/peel/widget/g;->f:Lcom/peel/control/a;

    iput-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->o:Lcom/peel/control/a;

    .line 415
    iget-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->p:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->clear()V

    .line 418
    :cond_5
    iget-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->a:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string/jumbo v2, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Ltv/peel/samsung/widget/NotiRemoteService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 420
    invoke-direct {p0}, Ltv/peel/samsung/widget/NotiRemoteService;->d()I

    move-result v0

    if-ne v0, v4, :cond_1

    .line 421
    invoke-direct {p0}, Ltv/peel/samsung/widget/NotiRemoteService;->g()V

    goto :goto_0

    .line 425
    :cond_6
    const-string/jumbo v1, "com.peel.widget.notification.STOP_PROCESS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 426
    const/4 v0, 0x3

    iput v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->c:I

    .line 427
    invoke-virtual {p0}, Ltv/peel/samsung/widget/NotiRemoteService;->stopSelf()V

    goto :goto_0

    .line 429
    :cond_7
    const-string/jumbo v1, "com.peel.widget.notification.BUTTON_PRESSED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 430
    iget-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->d:Ltv/peel/widget/util/a;

    if-eqz v0, :cond_1

    .line 431
    iget-object v0, p0, Ltv/peel/samsung/widget/NotiRemoteService;->d:Ltv/peel/widget/util/a;

    invoke-virtual {v0, p1}, Ltv/peel/widget/util/a;->a(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method
