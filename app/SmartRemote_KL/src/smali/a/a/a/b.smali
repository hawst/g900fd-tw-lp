.class public La/a/a/b;
.super La/a/b;


# instance fields
.field private transient a:Lorg/apache/http/client/HttpClient;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, La/a/b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    new-instance v0, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v0}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    iput-object v0, p0, La/a/a/b;->a:Lorg/apache/http/client/HttpClient;

    .line 44
    return-void
.end method


# virtual methods
.method protected a(Ljava/lang/String;)La/a/c/b;
    .locals 2

    .prologue
    .line 58
    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v0, p1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 59
    new-instance v1, La/a/a/c;

    invoke-direct {v1, v0}, La/a/a/c;-><init>(Lorg/apache/http/client/methods/HttpUriRequest;)V

    return-object v1
.end method

.method protected a(La/a/c/b;)La/a/c/c;
    .locals 2

    .prologue
    .line 64
    iget-object v1, p0, La/a/a/b;->a:Lorg/apache/http/client/HttpClient;

    invoke-interface {p1}, La/a/c/b;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v1, v0}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 65
    new-instance v1, La/a/a/d;

    invoke-direct {v1, v0}, La/a/a/d;-><init>(Lorg/apache/http/HttpResponse;)V

    return-object v1
.end method

.method protected a(La/a/c/b;La/a/c/c;)V
    .locals 1

    .prologue
    .line 71
    if-eqz p2, :cond_0

    .line 72
    invoke-interface {p2}, La/a/c/c;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/HttpResponse;

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 73
    if-eqz v0, :cond_0

    .line 76
    :try_start_0
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 77
    :catch_0
    move-exception v0

    .line 79
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method
