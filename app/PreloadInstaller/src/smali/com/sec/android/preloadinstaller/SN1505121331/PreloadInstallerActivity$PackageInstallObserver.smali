.class Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver;
.super Landroid/content/pm/IPackageInstallObserver$Stub;
.source "PreloadInstallerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PackageInstallObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;)V
    .locals 0

    .prologue
    .line 539
    iput-object p1, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver;->this$0:Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;

    invoke-direct {p0}, Landroid/content/pm/IPackageInstallObserver$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public packageInstalled(Ljava/lang/String;I)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "returnCode"    # I

    .prologue
    .line 541
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " installed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    .line 542
    const/4 v1, 0x1

    if-ne p2, v1, :cond_0

    .line 543
    iget-object v1, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver;->this$0:Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;

    invoke-virtual {v1, p1}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->putInstallHistory(Ljava/lang/String;)V

    .line 544
    :cond_0
    iget-object v1, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver;->this$0:Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;

    # operator++ for: Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstalledApkCnt:I
    invoke-static {v1}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->access$208(Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;)I

    .line 547
    iget-object v1, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver;->this$0:Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;

    # getter for: Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstalledApkCnt:I
    invoke-static {v1}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->access$200(Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;)I

    move-result v1

    rem-int/lit8 v0, v1, 0x2

    .line 548
    .local v0, "idx":I
    iget-object v1, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver;->this$0:Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;

    iget-object v1, v1, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mImgView:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    new-instance v2, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver$1;

    invoke-direct {v2, p0, p1, v0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver$1;-><init>(Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver;Ljava/lang/String;I)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->post(Ljava/lang/Runnable;)Z

    .line 566
    iget-object v1, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver;->this$0:Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;

    iget-object v1, v1, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mProgressBar:Landroid/widget/ProgressBar;

    new-instance v2, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver$2;

    invoke-direct {v2, p0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver$2;-><init>(Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver;)V

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->post(Ljava/lang/Runnable;)Z

    .line 573
    const-wide/16 v2, 0x258

    invoke-static {v2, v3}, Landroid/os/SystemClock;->sleep(J)V

    .line 576
    iget-object v1, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver;->this$0:Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;

    # getter for: Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstalledApkCnt:I
    invoke-static {v1}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->access$200(Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver;->this$0:Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;

    # getter for: Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstallPackageList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->access$300(Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 577
    const-string v1, "completed"

    invoke-static {v1}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    .line 578
    iget-object v1, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver;->this$0:Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;

    # invokes: Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->setDisabled()V
    invoke-static {v1}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->access$400(Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;)V

    .line 580
    :cond_1
    return-void
.end method
