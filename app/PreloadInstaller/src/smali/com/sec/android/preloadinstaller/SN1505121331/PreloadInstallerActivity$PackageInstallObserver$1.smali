.class Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver$1;
.super Ljava/lang/Object;
.source "PreloadInstallerActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver;->packageInstalled(Ljava/lang/String;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver;

.field final synthetic val$idx:I

.field final synthetic val$packageName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 548
    iput-object p1, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver$1;->this$1:Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver;

    iput-object p2, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver$1;->val$packageName:Ljava/lang/String;

    iput p3, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver$1;->val$idx:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 550
    const/4 v0, 0x0

    .line 552
    .local v0, "appIcon":Landroid/graphics/drawable/Drawable;
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver$1;->this$1:Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver;

    iget-object v3, v3, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver;->this$0:Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;

    invoke-virtual {v3}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver$1;->val$packageName:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v4, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver$1;->this$1:Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver;

    iget-object v4, v4, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver;->this$0:Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;

    invoke-virtual {v4}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 557
    :goto_0
    iget-object v3, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver$1;->this$1:Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver;

    iget-object v3, v3, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver;->this$0:Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;

    invoke-virtual {v3}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const/high16 v4, 0x7f030000

    invoke-static {v3, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    .line 559
    .local v2, "fadeIn":Landroid/view/animation/Animation;
    iget-object v3, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver$1;->this$1:Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver;

    iget-object v3, v3, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver;->this$0:Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;

    iget-object v3, v3, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mImgView:Ljava/util/LinkedList;

    iget v4, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver$1;->val$idx:I

    invoke-virtual {v3, v4}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 560
    iget-object v3, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver$1;->this$1:Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver;

    iget-object v3, v3, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver;->this$0:Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;

    iget-object v3, v3, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mImgView:Ljava/util/LinkedList;

    iget v4, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver$1;->val$idx:I

    invoke-virtual {v3, v4}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->bringToFront()V

    .line 561
    iget-object v3, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver$1;->this$1:Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver;

    iget-object v3, v3, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver;->this$0:Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;

    iget-object v3, v3, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mImgView:Ljava/util/LinkedList;

    iget v4, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver$1;->val$idx:I

    invoke-virtual {v3, v4}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 562
    return-void

    .line 554
    .end local v2    # "fadeIn":Landroid/view/animation/Animation;
    :catch_0
    move-exception v1

    .line 555
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v3, "Can\'t load applicaiton icon"

    invoke-static {v3}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    goto :goto_0
.end method
