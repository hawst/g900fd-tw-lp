.class public Lcom/sec/android/preloadinstaller/LogMsg;
.super Ljava/lang/Object;
.source "LogMsg.java"


# static fields
.field private static TAG:Ljava/lang/String;

.field private static sLogMsg:Lcom/sec/android/preloadinstaller/LogMsg;


# instance fields
.field private outputContents:Ljava/lang/StringBuffer;

.field private sdfNow:Ljava/text/SimpleDateFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-string v0, "PreloadInstaller"

    sput-object v0, Lcom/sec/android/preloadinstaller/LogMsg;->TAG:Ljava/lang/String;

    .line 14
    new-instance v0, Lcom/sec/android/preloadinstaller/LogMsg;

    invoke-direct {v0}, Lcom/sec/android/preloadinstaller/LogMsg;-><init>()V

    sput-object v0, Lcom/sec/android/preloadinstaller/LogMsg;->sLogMsg:Lcom/sec/android/preloadinstaller/LogMsg;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/preloadinstaller/LogMsg;->outputContents:Ljava/lang/StringBuffer;

    .line 30
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MM-dd HH:mm:ss.SSS"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/preloadinstaller/LogMsg;->sdfNow:Ljava/text/SimpleDateFormat;

    .line 33
    sget-object v0, Lcom/sec/android/preloadinstaller/LogMsg;->TAG:Ljava/lang/String;

    const-string v1, ""

    invoke-direct {p0, v0, v1}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    return-void
.end method

.method private exists()Z
    .locals 2

    .prologue
    .line 77
    new-instance v0, Ljava/io/File;

    const-string v1, "/data/log/PreloadInstaller.txt"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 78
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    return v1
.end method

.method private flush()V
    .locals 8

    .prologue
    .line 52
    const/4 v1, 0x0

    .line 53
    .local v1, "file":Ljava/io/File;
    const/4 v3, 0x0

    .line 55
    .local v3, "fos":Ljava/io/OutputStreamWriter;
    :try_start_0
    new-instance v2, Ljava/io/File;

    const-string v5, "/data/log/PreloadInstaller.txt"

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 56
    .end local v1    # "file":Ljava/io/File;
    .local v2, "file":Ljava/io/File;
    :try_start_1
    new-instance v4, Ljava/io/OutputStreamWriter;

    new-instance v5, Ljava/io/FileOutputStream;

    const/4 v6, 0x1

    invoke-direct {v5, v2, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    const-string v6, "UTF-8"

    invoke-direct {v4, v5, v6}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 58
    .end local v3    # "fos":Ljava/io/OutputStreamWriter;
    .local v4, "fos":Ljava/io/OutputStreamWriter;
    :try_start_2
    iget-object v6, p0, Lcom/sec/android/preloadinstaller/LogMsg;->outputContents:Ljava/lang/StringBuffer;

    monitor-enter v6
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 59
    :try_start_3
    iget-object v5, p0, Lcom/sec/android/preloadinstaller/LogMsg;->outputContents:Ljava/lang/StringBuffer;

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V

    .line 60
    iget-object v5, p0, Lcom/sec/android/preloadinstaller/LogMsg;->outputContents:Ljava/lang/StringBuffer;

    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 61
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 67
    if-eqz v4, :cond_0

    :try_start_4
    invoke-virtual {v4}, Ljava/io/OutputStreamWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    :cond_0
    move-object v3, v4

    .end local v4    # "fos":Ljava/io/OutputStreamWriter;
    .restart local v3    # "fos":Ljava/io/OutputStreamWriter;
    move-object v1, v2

    .line 74
    .end local v2    # "file":Ljava/io/File;
    .restart local v1    # "file":Ljava/io/File;
    :cond_1
    :goto_0
    return-void

    .line 61
    .end local v1    # "file":Ljava/io/File;
    .end local v3    # "fos":Ljava/io/OutputStreamWriter;
    .restart local v2    # "file":Ljava/io/File;
    .restart local v4    # "fos":Ljava/io/OutputStreamWriter;
    :catchall_0
    move-exception v5

    :try_start_5
    monitor-exit v6
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v5
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 62
    :catch_0
    move-exception v0

    move-object v3, v4

    .end local v4    # "fos":Ljava/io/OutputStreamWriter;
    .restart local v3    # "fos":Ljava/io/OutputStreamWriter;
    move-object v1, v2

    .line 63
    .end local v2    # "file":Ljava/io/File;
    .local v0, "e":Ljava/lang/Exception;
    .restart local v1    # "file":Ljava/io/File;
    :goto_1
    :try_start_7
    sget-object v5, Lcom/sec/android/preloadinstaller/LogMsg;->TAG:Ljava/lang/String;

    invoke-static {v5, v0}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 67
    if-eqz v3, :cond_1

    :try_start_8
    invoke-virtual {v3}, Ljava/io/OutputStreamWriter;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1

    goto :goto_0

    .line 69
    :catch_1
    move-exception v0

    .line 71
    .local v0, "e":Ljava/io/IOException;
    sget-object v5, Lcom/sec/android/preloadinstaller/LogMsg;->TAG:Ljava/lang/String;

    invoke-static {v5, v0}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 69
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "file":Ljava/io/File;
    .end local v3    # "fos":Ljava/io/OutputStreamWriter;
    .restart local v2    # "file":Ljava/io/File;
    .restart local v4    # "fos":Ljava/io/OutputStreamWriter;
    :catch_2
    move-exception v0

    .line 71
    .restart local v0    # "e":Ljava/io/IOException;
    sget-object v5, Lcom/sec/android/preloadinstaller/LogMsg;->TAG:Ljava/lang/String;

    invoke-static {v5, v0}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v3, v4

    .end local v4    # "fos":Ljava/io/OutputStreamWriter;
    .restart local v3    # "fos":Ljava/io/OutputStreamWriter;
    move-object v1, v2

    .line 73
    .end local v2    # "file":Ljava/io/File;
    .restart local v1    # "file":Ljava/io/File;
    goto :goto_0

    .line 65
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v5

    .line 67
    :goto_2
    if-eqz v3, :cond_2

    :try_start_9
    invoke-virtual {v3}, Ljava/io/OutputStreamWriter;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    .line 72
    :cond_2
    :goto_3
    throw v5

    .line 69
    :catch_3
    move-exception v0

    .line 71
    .restart local v0    # "e":Ljava/io/IOException;
    sget-object v6, Lcom/sec/android/preloadinstaller/LogMsg;->TAG:Ljava/lang/String;

    invoke-static {v6, v0}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    .line 65
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "file":Ljava/io/File;
    .restart local v2    # "file":Ljava/io/File;
    :catchall_2
    move-exception v5

    move-object v1, v2

    .end local v2    # "file":Ljava/io/File;
    .restart local v1    # "file":Ljava/io/File;
    goto :goto_2

    .end local v1    # "file":Ljava/io/File;
    .end local v3    # "fos":Ljava/io/OutputStreamWriter;
    .restart local v2    # "file":Ljava/io/File;
    .restart local v4    # "fos":Ljava/io/OutputStreamWriter;
    :catchall_3
    move-exception v5

    move-object v3, v4

    .end local v4    # "fos":Ljava/io/OutputStreamWriter;
    .restart local v3    # "fos":Ljava/io/OutputStreamWriter;
    move-object v1, v2

    .end local v2    # "file":Ljava/io/File;
    .restart local v1    # "file":Ljava/io/File;
    goto :goto_2

    .line 62
    :catch_4
    move-exception v0

    goto :goto_1

    .end local v1    # "file":Ljava/io/File;
    .restart local v2    # "file":Ljava/io/File;
    :catch_5
    move-exception v0

    move-object v1, v2

    .end local v2    # "file":Ljava/io/File;
    .restart local v1    # "file":Ljava/io/File;
    goto :goto_1
.end method

.method public static isExists()Z
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/sec/android/preloadinstaller/LogMsg;->sLogMsg:Lcom/sec/android/preloadinstaller/LogMsg;

    invoke-direct {v0}, Lcom/sec/android/preloadinstaller/LogMsg;->exists()Z

    move-result v0

    return v0
.end method

.method public static out(Ljava/lang/String;)V
    .locals 2
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 18
    sget-object v0, Lcom/sec/android/preloadinstaller/LogMsg;->sLogMsg:Lcom/sec/android/preloadinstaller/LogMsg;

    sget-object v1, Lcom/sec/android/preloadinstaller/LogMsg;->TAG:Ljava/lang/String;

    invoke-direct {v0, v1, p0}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    return-void
.end method

.method private out(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 37
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 38
    .local v0, "now":J
    iget-object v3, p0, Lcom/sec/android/preloadinstaller/LogMsg;->sdfNow:Ljava/text/SimpleDateFormat;

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 40
    .local v2, "strNow":Ljava/lang/String;
    invoke-static {p1, p2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    iget-object v4, p0, Lcom/sec/android/preloadinstaller/LogMsg;->outputContents:Ljava/lang/StringBuffer;

    monitor-enter v4

    .line 42
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/preloadinstaller/LogMsg;->outputContents:Ljava/lang/StringBuffer;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 43
    monitor-exit v4

    .line 44
    return-void

    .line 43
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public static write(Ljava/lang/String;)V
    .locals 2
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 22
    sget-object v0, Lcom/sec/android/preloadinstaller/LogMsg;->sLogMsg:Lcom/sec/android/preloadinstaller/LogMsg;

    sget-object v1, Lcom/sec/android/preloadinstaller/LogMsg;->TAG:Ljava/lang/String;

    invoke-direct {v0, v1, p0}, Lcom/sec/android/preloadinstaller/LogMsg;->write(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    return-void
.end method

.method private write(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    invoke-direct {p0}, Lcom/sec/android/preloadinstaller/LogMsg;->flush()V

    .line 49
    return-void
.end method
