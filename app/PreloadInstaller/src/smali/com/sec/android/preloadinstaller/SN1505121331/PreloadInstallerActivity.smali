.class public Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;
.super Landroid/app/Activity;
.source "PreloadInstallerActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver;
    }
.end annotation


# instance fields
.field mApkFilter:Ljava/io/FilenameFilter;

.field private mCscFeatureAddedAPKList:Ljava/lang/String;

.field private mCscFeatureReInstallAPKList:Ljava/lang/String;

.field private mCscFeatureUninstallPKGList:Ljava/lang/String;

.field private mForceEnable:Z

.field mImgView:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field mInstallHistory:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mInstallPackageList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private mInstalledApkCnt:I

.field private mIsFota:Z

.field private mIsVerifier:Z

.field private mKeepDataApp:Z

.field mPref:Landroid/content/SharedPreferences;

.field private mPreviousVerifierValue:I

.field mProgressBar:Landroid/widget/ProgressBar;

.field private unInstallPackageList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 63
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 83
    iput v1, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstalledApkCnt:I

    .line 85
    iput-boolean v1, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mKeepDataApp:Z

    .line 87
    iput-boolean v1, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mForceEnable:Z

    .line 88
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mCscFeatureAddedAPKList:Ljava/lang/String;

    .line 89
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mCscFeatureReInstallAPKList:Ljava/lang/String;

    .line 90
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mCscFeatureUninstallPKGList:Ljava/lang/String;

    .line 93
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->unInstallPackageList:Ljava/util/ArrayList;

    .line 95
    new-instance v0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$1;-><init>(Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;)V

    iput-object v0, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mApkFilter:Ljava/io/FilenameFilter;

    .line 105
    iput-boolean v1, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mIsVerifier:Z

    .line 107
    iput v1, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mPreviousVerifierValue:I

    .line 109
    invoke-static {}, Lcom/sec/android/preloadinstaller/LogMsg;->isExists()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mIsFota:Z

    .line 816
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstallHistory:Ljava/util/Collection;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->startPreloadInstaller()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->unInstallPackageList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;

    .prologue
    .line 63
    iget v0, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstalledApkCnt:I

    return v0
.end method

.method static synthetic access$208(Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;

    .prologue
    .line 63
    iget v0, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstalledApkCnt:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstalledApkCnt:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstallPackageList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->setDisabled()V

    return-void
.end method

.method private addPackageLocation(Ljava/lang/String;)V
    .locals 9
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 370
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 371
    .local v1, "dir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 372
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " exist"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    .line 373
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v6

    .line 375
    .local v6, "paths":[Ljava/io/File;
    if-nez v6, :cond_1

    .line 413
    .end local v6    # "paths":[Ljava/io/File;
    :cond_0
    return-void

    .line 379
    .restart local v6    # "paths":[Ljava/io/File;
    :cond_1
    move-object v0, v6

    .local v0, "arr$":[Ljava/io/File;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_3

    aget-object v2, v0, v4

    .line 380
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 382
    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->addPackageLocation(Ljava/lang/String;)V

    .line 379
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 387
    .end local v2    # "file":Ljava/io/File;
    :cond_3
    iget-object v7, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mApkFilter:Ljava/io/FilenameFilter;

    if-eqz v7, :cond_0

    .line 388
    iget-object v7, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mApkFilter:Ljava/io/FilenameFilter;

    invoke-virtual {v1, v7}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v3

    .line 389
    .local v3, "files":[Ljava/io/File;
    if-eqz v3, :cond_0

    .line 393
    move-object v0, v3

    array-length v5, v0

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v5, :cond_0

    aget-object v2, v0, v4

    .line 395
    .restart local v2    # "file":Ljava/io/File;
    iget-boolean v7, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mForceEnable:Z

    if-eqz v7, :cond_6

    .line 396
    iget-object v7, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mCscFeatureAddedAPKList:Ljava/lang/String;

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mCscFeatureAddedAPKList:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_4

    .line 397
    iget-object v7, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mCscFeatureAddedAPKList:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-direct {p0, v2}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->isInstalled(Ljava/io/File;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 398
    iget-object v7, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstallPackageList:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 401
    :cond_4
    iget-object v7, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mCscFeatureReInstallAPKList:Ljava/lang/String;

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mCscFeatureReInstallAPKList:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_5

    .line 402
    iget-object v7, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mCscFeatureReInstallAPKList:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-direct {p0, v2}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->isInstalled(Ljava/io/File;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 403
    iget-object v7, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstallPackageList:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 393
    :cond_5
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 408
    :cond_6
    iget-object v7, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstallPackageList:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method private disableNotifications()V
    .locals 3

    .prologue
    .line 285
    const-string v2, "statusbar"

    invoke-virtual {p0, v2}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/StatusBarManager;

    .line 286
    .local v0, "mStatusBarManager":Landroid/app/StatusBarManager;
    if-eqz v0, :cond_0

    .line 287
    const-string v2, "disableNotifications"

    invoke-static {v2}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    .line 289
    const/high16 v1, 0x1a70000

    .line 295
    .local v1, "statusBarFlag":I
    invoke-virtual {v0, v1}, Landroid/app/StatusBarManager;->disable(I)V

    .line 297
    .end local v1    # "statusBarFlag":I
    :cond_0
    return-void
.end method

.method private enableNotifications()V
    .locals 2

    .prologue
    .line 277
    const-string v1, "statusbar"

    invoke-virtual {p0, v1}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/StatusBarManager;

    .line 278
    .local v0, "mStatusBarManager":Landroid/app/StatusBarManager;
    if-eqz v0, :cond_0

    .line 279
    const-string v1, "enableNotifications"

    invoke-static {v1}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    .line 280
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/StatusBarManager;->disable(I)V

    .line 282
    :cond_0
    return-void
.end method

.method private installPackage(Ljava/util/ArrayList;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "packageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    const/16 v11, 0x12

    const/4 v10, 0x0

    .line 416
    invoke-virtual {p0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 417
    .local v6, "pm":Landroid/content/pm/PackageManager;
    new-instance v5, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver;

    invoke-direct {v5, p0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver;-><init>(Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;)V

    .line 419
    .local v5, "observer":Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$PackageInstallObserver;
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    .line 420
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8, v10}, Landroid/content/pm/PackageManager;->getPackageArchiveInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v7

    .line 421
    .local v7, "updatePkg":Landroid/content/pm/PackageInfo;
    if-eqz v7, :cond_3

    .line 422
    iget-object v8, v7, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const-string v9, "kr.co.rightbrain.ScreenSaver.GalaxyNote_10_1"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 424
    const/4 v4, 0x1

    .line 426
    .local v4, "isScreenSaverInstalled":Z
    :try_start_0
    const-string v8, "kr.co.rightbrain.ScreenSaver.GalaxyNote_10_1"

    const/4 v9, 0x0

    invoke-virtual {v6, v8, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 430
    :goto_1
    if-eqz v4, :cond_1

    .line 431
    iget v8, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstalledApkCnt:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstalledApkCnt:I

    .line 432
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, v7, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " Skip..."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    .line 433
    iget v8, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstalledApkCnt:I

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ne v8, v9, :cond_0

    .line 434
    const-string v8, "installPackage() : completed"

    invoke-static {v8}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    .line 435
    invoke-direct {p0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->setDisabled()V

    goto :goto_0

    .line 427
    :catch_0
    move-exception v0

    .line 428
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v4, 0x0

    goto :goto_1

    .line 442
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v4    # "isScreenSaverInstalled":Z
    :cond_1
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v8

    const-string v9, "CscFeature_Common_ConfigLocalSecurityPolicy"

    invoke-virtual {v8, v9}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 443
    .local v3, "isChinaNal":Ljava/lang/String;
    const-string v8, "ChinaNalSecurity"

    invoke-virtual {v8, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 444
    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v8

    const-string v9, "com.sec.android.preloadinstaller"

    invoke-virtual {v6, v8, v5, v11, v9}, Landroid/content/pm/PackageManager;->installPackage(Landroid/net/Uri;Landroid/content/pm/IPackageInstallObserver;ILjava/lang/String;)V

    .line 451
    :goto_2
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, v7, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " install..."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 447
    :cond_2
    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v6, v8, v5, v11, v9}, Landroid/content/pm/PackageManager;->installPackage(Landroid/net/Uri;Landroid/content/pm/IPackageInstallObserver;ILjava/lang/String;)V

    goto :goto_2

    .line 453
    .end local v3    # "isChinaNal":Ljava/lang/String;
    :cond_3
    iget v8, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstalledApkCnt:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstalledApkCnt:I

    .line 454
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " Skip..."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    .line 455
    iget v8, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstalledApkCnt:I

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ne v8, v9, :cond_0

    .line 456
    const-string v8, "installPackage() : completed"

    invoke-static {v8}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    .line 457
    invoke-direct {p0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->setDisabled()V

    goto/16 :goto_0

    .line 462
    .end local v1    # "file":Ljava/io/File;
    .end local v7    # "updatePkg":Landroid/content/pm/PackageInfo;
    :cond_4
    return-void
.end method

.method public static isChinaModel()Z
    .locals 2

    .prologue
    .line 728
    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 729
    .local v0, "salesCode":Ljava/lang/String;
    if-eqz v0, :cond_1

    const-string v1, "CHZ"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "CHN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "CHM"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "CHU"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "CTC"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "CHC"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isFactoryMode()Z
    .locals 5

    .prologue
    .line 631
    const/4 v1, 0x0

    .line 633
    .local v1, "userMode":Ljava/lang/String;
    :try_start_0
    new-instance v2, Ljava/io/File;

    const-string v3, "/efs/FactoryApp/factorymode"

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/16 v3, 0x20

    const/4 v4, 0x0

    invoke-virtual {p0, v2, v3, v4}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 638
    :goto_0
    if-eqz v1, :cond_0

    const-string v2, "ON"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 639
    const-string v2, "User Mode"

    invoke-static {v2}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    .line 640
    const/4 v2, 0x0

    .line 643
    :goto_1
    return v2

    .line 634
    :catch_0
    move-exception v0

    .line 635
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "OFF"

    .line 636
    const-string v2, "cannot open FactoryMode flag file"

    invoke-static {v2}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    goto :goto_0

    .line 642
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const-string v2, "Factory Mode"

    invoke-static {v2}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    .line 643
    const/4 v2, 0x1

    goto :goto_1
.end method

.method private isFromFactoryReset()Z
    .locals 2

    .prologue
    .line 620
    new-instance v0, Ljava/io/File;

    const-string v1, "/efs/.currentlyFactoryReset"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    return v0
.end method

.method private isInstalled(Ljava/io/File;)Z
    .locals 6
    .param p1, "file"    # Ljava/io/File;

    .prologue
    const/4 v3, 0x0

    .line 735
    invoke-virtual {p0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 736
    .local v2, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4, v3}, Landroid/content/pm/PackageManager;->getPackageArchiveInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 737
    .local v1, "pkgInfo":Landroid/content/pm/PackageInfo;
    if-nez v1, :cond_0

    .line 738
    const-string v4, "pkgInfo is null"

    invoke-static {v4}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    .line 746
    :goto_0
    return v3

    .line 742
    :cond_0
    :try_start_0
    iget-object v4, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 746
    const/4 v3, 0x1

    goto :goto_0

    .line 743
    :catch_0
    move-exception v0

    .line 744
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_0
.end method

.method private isKeepDataApp()Z
    .locals 5

    .prologue
    .line 692
    :try_start_0
    new-instance v2, Ljava/io/File;

    const-string v3, "/data/factory_reset.txt"

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 693
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v3

    if-eqz v3, :cond_1

    .line 694
    const/4 v1, 0x0

    .line 696
    .local v1, "fatoryReset":Ljava/lang/String;
    const/16 v3, 0x20

    const/4 v4, 0x0

    :try_start_1
    invoke-virtual {p0, v2, v3, v4}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 698
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "fatoryReset text = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    .line 699
    if-eqz v1, :cond_0

    const-string v3, "/data/app"

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 700
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mKeepDataApp:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 705
    :cond_0
    :goto_0
    :try_start_2
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-result v3

    if-nez v3, :cond_1

    .line 706
    const-string v3, "failed to delete file"

    invoke-static {v3}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 712
    .end local v1    # "fatoryReset":Ljava/lang/String;
    .end local v2    # "file":Ljava/io/File;
    :cond_1
    :goto_1
    iget-boolean v3, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mKeepDataApp:Z

    return v3

    .line 702
    .restart local v1    # "fatoryReset":Ljava/lang/String;
    .restart local v2    # "file":Ljava/io/File;
    :catch_0
    move-exception v0

    .line 703
    .local v0, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v3, "cannot read factory_reset file"

    invoke-static {v3}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 709
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "fatoryReset":Ljava/lang/String;
    .end local v2    # "file":Ljava/io/File;
    :catch_1
    move-exception v0

    .line 710
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v3, "cannot open factory_reset file"

    invoke-static {v3}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static isOwner()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 717
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v1

    .line 718
    .local v1, "userMode":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ActivityManager.getCurrentUser() = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 719
    if-nez v1, :cond_0

    .line 721
    :goto_0
    return v2

    .line 719
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 720
    :catch_0
    move-exception v0

    .line 721
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method private isVerifierInstalled()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 680
    invoke-virtual {p0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 681
    .local v0, "pm":Landroid/content/pm/PackageManager;
    new-instance v2, Landroid/content/Intent;

    const-string v5, "android.intent.action.PACKAGE_NEEDS_VERIFICATION"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 682
    .local v2, "verification":Landroid/content/Intent;
    const-string v5, "application/vnd.android.package-archive"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 683
    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 684
    invoke-virtual {v0, v2, v4}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 685
    .local v1, "receivers":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_0

    :goto_0
    return v3

    :cond_0
    move v3, v4

    goto :goto_0
.end method

.method private makeinstallPackageList()V
    .locals 15

    .prologue
    const/4 v14, 0x0

    .line 300
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    iput-object v10, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstallPackageList:Ljava/util/ArrayList;

    .line 301
    invoke-virtual {p0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->getCscCode()Ljava/lang/String;

    move-result-object v0

    .line 303
    .local v0, "cscCode":Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "cscCode : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    .line 304
    const-string v10, "/preload/Common_app"

    invoke-direct {p0, v10}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->addPackageLocation(Ljava/lang/String;)V

    .line 305
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "/preload/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/hidden_app"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v10}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->addPackageLocation(Ljava/lang/String;)V

    .line 306
    const-string v10, "/system/preload"

    invoke-direct {p0, v10}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->addPackageLocation(Ljava/lang/String;)V

    .line 307
    const-string v10, "/system/hidden/Common_app/"

    invoke-direct {p0, v10}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->addPackageLocation(Ljava/lang/String;)V

    .line 308
    const-string v10, "/system/etc/vpl"

    invoke-direct {p0, v10}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->addPackageLocation(Ljava/lang/String;)V

    .line 309
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "/system/hidden/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/hidden_app"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v10}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->addPackageLocation(Ljava/lang/String;)V

    .line 314
    invoke-direct {p0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->isFromFactoryReset()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 315
    invoke-direct {p0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->isFactoryMode()Z

    move-result v10

    if-nez v10, :cond_0

    .line 316
    const-string v10, "/system/preloadFactoryResetOnly"

    invoke-direct {p0, v10}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->addPackageLocation(Ljava/lang/String;)V

    .line 319
    :cond_0
    new-instance v7, Ljava/util/ArrayList;

    iget-object v10, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstallPackageList:Ljava/util/ArrayList;

    invoke-direct {v7, v10}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 320
    .local v7, "packageListClone":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/io/File;

    .line 321
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v10

    const-wide/16 v12, 0x0

    cmp-long v10, v10, v12

    if-nez v10, :cond_1

    .line 322
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    .line 323
    .local v1, "delFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 324
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "remove : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    .line 325
    iget-object v10, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstallPackageList:Ljava/util/ArrayList;

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 332
    .end local v1    # "delFile":Ljava/io/File;
    .end local v3    # "file":Ljava/io/File;
    .end local v5    # "i$":Ljava/util/Iterator;
    :cond_3
    iget-boolean v10, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mIsFota:Z

    if-eqz v10, :cond_9

    .line 333
    invoke-virtual {p0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    .line 334
    .local v8, "pm":Landroid/content/pm/PackageManager;
    new-instance v7, Ljava/util/ArrayList;

    .end local v7    # "packageListClone":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    iget-object v10, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstallPackageList:Ljava/util/ArrayList;

    invoke-direct {v7, v10}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 335
    .restart local v7    # "packageListClone":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/io/File;

    .line 336
    .restart local v3    # "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10, v14}, Landroid/content/pm/PackageManager;->getPackageArchiveInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v9

    .line 337
    .local v9, "updatePkg":Landroid/content/pm/PackageInfo;
    const/4 v6, 0x0

    .line 338
    .local v6, "installedPkg":Landroid/content/pm/PackageInfo;
    if-eqz v9, :cond_4

    .line 340
    :try_start_0
    iget-object v10, v9, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual {v8, v10, v11}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v6

    .line 341
    if-eqz v6, :cond_7

    .line 342
    iget v10, v9, Landroid/content/pm/PackageInfo;->versionCode:I

    iget v11, v6, Landroid/content/pm/PackageInfo;->versionCode:I

    if-gt v10, v11, :cond_5

    iget-object v10, v6, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    iget-object v11, v9, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v10

    const/4 v11, 0x1

    if-eq v10, v11, :cond_6

    :cond_5
    iget-object v10, v6, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    iget-object v11, v9, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v10

    if-nez v10, :cond_7

    .line 343
    :cond_6
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "exists : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    .line 344
    iget-object v10, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstallPackageList:Ljava/util/ArrayList;

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 345
    iget-object v10, v9, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p0, v10}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->putInstallHistory(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 352
    :cond_7
    if-nez v6, :cond_4

    .line 353
    iget-object v10, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstallHistory:Ljava/util/Collection;

    iget-object v11, v9, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-interface {v10, v11}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 354
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "removed by user : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    .line 355
    iget-object v10, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstallPackageList:Ljava/util/ArrayList;

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 348
    :catch_0
    move-exception v2

    .line 349
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_1
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "new : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 352
    if-nez v6, :cond_4

    .line 353
    iget-object v10, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstallHistory:Ljava/util/Collection;

    iget-object v11, v9, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-interface {v10, v11}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 354
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "removed by user : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    .line 355
    iget-object v10, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstallPackageList:Ljava/util/ArrayList;

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 352
    .end local v2    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catchall_0
    move-exception v10

    if-nez v6, :cond_8

    .line 353
    iget-object v11, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstallHistory:Ljava/util/Collection;

    iget-object v12, v9, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-interface {v11, v12}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 354
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "removed by user : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    .line 355
    iget-object v11, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstallPackageList:Ljava/util/ArrayList;

    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_8
    throw v10

    .line 364
    .end local v3    # "file":Ljava/io/File;
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v6    # "installedPkg":Landroid/content/pm/PackageInfo;
    .end local v8    # "pm":Landroid/content/pm/PackageManager;
    .end local v9    # "updatePkg":Landroid/content/pm/PackageInfo;
    :cond_9
    iget-object v10, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstallPackageList:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .restart local v4    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/io/File;

    .line 365
    .restart local v3    # "file":Ljava/io/File;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "To Install : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    goto :goto_2

    .line 367
    .end local v3    # "file":Ljava/io/File;
    :cond_a
    return-void
.end method

.method private removeFactoryResetFlag()Z
    .locals 2

    .prologue
    .line 624
    new-instance v0, Ljava/io/File;

    const-string v1, "/efs/.currentlyFactoryReset"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 625
    .local v0, "flagFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private setDisabled()V
    .locals 5

    .prologue
    .line 480
    const-string v1, "persist.sys.storage_preload"

    const-string v2, "0"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    iget-boolean v1, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mIsVerifier:Z

    if-eqz v1, :cond_0

    .line 483
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Verify App]set to Verifier Enabled Value to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mPreviousVerifierValue:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    .line 484
    iget v1, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mPreviousVerifierValue:I

    if-eqz v1, :cond_0

    .line 485
    invoke-virtual {p0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "package_verifier_enable"

    iget v3, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mPreviousVerifierValue:I

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 491
    :cond_0
    const-string v1, "Set package state to disabled"

    invoke-static {v1}, Lcom/sec/android/preloadinstaller/LogMsg;->write(Ljava/lang/String;)V

    .line 494
    iget-object v1, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstallPackageList:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstallPackageList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_1

    .line 499
    const-string v1, "Waiting for cache flush..."

    invoke-static {v1}, Lcom/sec/android/preloadinstaller/LogMsg;->write(Ljava/lang/String;)V

    .line 500
    const-wide/16 v2, 0x1388

    invoke-static {v2, v3}, Landroid/os/SystemClock;->sleep(J)V

    .line 503
    :cond_1
    const-string v1, "persist.sys.storage_preload"

    const-string v2, "2"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    iget-boolean v1, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mKeepDataApp:Z

    if-nez v1, :cond_2

    .line 507
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PREINSTALLER_FINISH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 508
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 512
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->isFromFactoryReset()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 513
    invoke-direct {p0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->isFactoryMode()Z

    move-result v1

    if-nez v1, :cond_3

    .line 514
    invoke-direct {p0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->removeFactoryResetFlag()Z

    .line 517
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    new-instance v2, Landroid/content/ComponentName;

    const-class v3, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v3, 0x2

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 520
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MAIN"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v2, "android.intent.category.HOME"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x10200000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->startActivity(Landroid/content/Intent;)V

    .line 525
    const-string v1, "Intent.CATEGORY_HOME is called."

    invoke-static {v1}, Lcom/sec/android/preloadinstaller/LogMsg;->write(Ljava/lang/String;)V

    .line 527
    invoke-direct {p0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->enableNotifications()V

    .line 531
    invoke-virtual {p0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->finish()V

    .line 532
    const-string v1, "finish() is called."

    invoke-static {v1}, Lcom/sec/android/preloadinstaller/LogMsg;->write(Ljava/lang/String;)V

    .line 533
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    invoke-static {v1}, Landroid/os/Process;->killProcess(I)V

    .line 534
    return-void
.end method

.method private startPreloadInstaller()V
    .locals 2

    .prologue
    .line 246
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PreloadInstaller Start : Fota="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mIsFota:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    .line 247
    const v0, 0x7f050004

    invoke-virtual {p0, v0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mProgressBar:Landroid/widget/ProgressBar;

    .line 248
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mImgView:Ljava/util/LinkedList;

    .line 249
    iget-object v1, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mImgView:Ljava/util/LinkedList;

    const v0, 0x7f050001

    invoke-virtual {p0, v0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 250
    iget-object v1, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mImgView:Ljava/util/LinkedList;

    const v0, 0x7f050002

    invoke-virtual {p0, v0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 252
    invoke-direct {p0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->unInstallPackage()V

    .line 254
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v1, "CscFeature_Setting_ConfigTypeFactoryReset"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->isKeepDataApp()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-static {}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->isOwner()Z

    move-result v0

    if-nez v0, :cond_2

    .line 259
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->setDisabled()V

    .line 274
    :goto_0
    return-void

    .line 264
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->loadInstallHistory()V

    .line 265
    invoke-direct {p0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->makeinstallPackageList()V

    .line 266
    iget-object v0, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstallPackageList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 267
    const-string v0, "apk count is 0. call  setDisabled();"

    invoke-static {v0}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    .line 268
    invoke-direct {p0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->setDisabled()V

    goto :goto_0

    .line 272
    :cond_3
    iget-object v0, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mProgressBar:Landroid/widget/ProgressBar;

    iget-object v1, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstallPackageList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 273
    iget-object v0, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstallPackageList:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->installPackage(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private unInstallPackage()V
    .locals 9

    .prologue
    .line 647
    const/4 v3, 0x0

    .line 650
    .local v3, "reader":Ljava/io/BufferedReader;
    new-instance v1, Ljava/io/File;

    const-string v6, "/system/etc/uninstall_preloadpkg.lst"

    invoke-direct {v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 651
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 653
    :try_start_0
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/InputStreamReader;

    new-instance v7, Ljava/io/FileInputStream;

    invoke-direct {v7, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const-string v8, "UTF-8"

    invoke-direct {v6, v7, v8}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v4, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 657
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .local v4, "reader":Ljava/io/BufferedReader;
    :cond_0
    :goto_0
    :try_start_1
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    .local v5, "s":Ljava/lang/String;
    if-eqz v5, :cond_2

    .line 658
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_0

    const-string v6, "#"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->unInstallPackageList:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 659
    iget-object v6, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->unInstallPackageList:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 662
    .end local v5    # "s":Ljava/lang/String;
    :catch_0
    move-exception v0

    move-object v3, v4

    .line 663
    .end local v4    # "reader":Ljava/io/BufferedReader;
    .local v0, "e":Ljava/lang/Exception;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :goto_1
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 666
    if-eqz v3, :cond_1

    .line 667
    :try_start_3
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 673
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_2
    iget-object v6, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->unInstallPackageList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 674
    .restart local v5    # "s":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "deletePackage :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    .line 675
    invoke-virtual {p0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v6, v5, v7, v8}, Landroid/content/pm/PackageManager;->deletePackage(Ljava/lang/String;Landroid/content/pm/IPackageDeleteObserver;I)V

    goto :goto_3

    .line 666
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :cond_2
    if-eqz v4, :cond_3

    .line 667
    :try_start_4
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    :cond_3
    move-object v3, v4

    .line 669
    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 668
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :catch_1
    move-exception v6

    move-object v3, v4

    .line 670
    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 665
    .end local v5    # "s":Ljava/lang/String;
    :catchall_0
    move-exception v6

    .line 666
    :goto_4
    if-eqz v3, :cond_4

    .line 667
    :try_start_5
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    .line 669
    :cond_4
    :goto_5
    throw v6

    .line 668
    .restart local v0    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v6

    goto :goto_2

    .end local v0    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v7

    goto :goto_5

    .line 677
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_5
    return-void

    .line 665
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v6

    move-object v3, v4

    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    goto :goto_4

    .line 662
    :catch_4
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 466
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 467
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 468
    const-string v0, "Ignore KeyEvent"

    invoke-static {v0}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    .line 469
    const/4 v0, 0x1

    .line 472
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCscCode()Ljava/lang/String;
    .locals 12

    .prologue
    .line 584
    const/4 v3, 0x0

    .line 585
    .local v3, "inFile":Ljava/io/File;
    const/4 v5, 0x0

    .line 586
    .local v5, "inStream":Ljava/io/FileInputStream;
    const/4 v2, 0x0

    .line 587
    .local v2, "inChannel":Ljava/nio/channels/FileChannel;
    const/4 v0, 0x0

    .line 588
    .local v0, "bBuffer":Ljava/nio/ByteBuffer;
    const/4 v7, 0x0

    .line 591
    .local v7, "strBuffer":Ljava/lang/String;
    :try_start_0
    new-instance v4, Ljava/io/File;

    const-string v9, "system/csc/sales_code.dat"

    invoke-direct {v4, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 592
    .end local v3    # "inFile":Ljava/io/File;
    .local v4, "inFile":Ljava/io/File;
    :try_start_1
    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_6

    .line 597
    .end local v5    # "inStream":Ljava/io/FileInputStream;
    .local v6, "inStream":Ljava/io/FileInputStream;
    invoke-virtual {v6}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v2

    .line 599
    :try_start_2
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v10

    long-to-int v9, v10

    add-int/lit8 v9, v9, -0x1

    invoke-static {v9}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 600
    invoke-virtual {v2, v0}, Ljava/nio/channels/FileChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v9

    if-nez v9, :cond_0

    .line 601
    const-string v9, "read size is 0"

    invoke-static {v9}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    .line 602
    :cond_0
    new-instance v8, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    move-result-object v9

    check-cast v9, Ljava/nio/ByteBuffer;

    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/String;-><init>([B)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 603
    .end local v7    # "strBuffer":Ljava/lang/String;
    .local v8, "strBuffer":Ljava/lang/String;
    :try_start_3
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "sales_code is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 608
    if-eqz v6, :cond_1

    .line 609
    :try_start_4
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V

    .line 610
    :cond_1
    if-eqz v2, :cond_2

    .line 611
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    :cond_2
    move-object v7, v8

    .end local v8    # "strBuffer":Ljava/lang/String;
    .restart local v7    # "strBuffer":Ljava/lang/String;
    :cond_3
    :goto_0
    move-object v5, v6

    .end local v6    # "inStream":Ljava/io/FileInputStream;
    .restart local v5    # "inStream":Ljava/io/FileInputStream;
    move-object v3, v4

    .end local v4    # "inFile":Ljava/io/File;
    .restart local v3    # "inFile":Ljava/io/File;
    move-object v9, v7

    .line 616
    :goto_1
    return-object v9

    .line 593
    :catch_0
    move-exception v1

    .line 594
    .local v1, "e":Ljava/io/FileNotFoundException;
    :goto_2
    const-string v9, "There is no mps_code.dat file"

    invoke-static {v9}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    .line 595
    const/4 v9, 0x0

    goto :goto_1

    .line 612
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    .end local v3    # "inFile":Ljava/io/File;
    .end local v5    # "inStream":Ljava/io/FileInputStream;
    .end local v7    # "strBuffer":Ljava/lang/String;
    .restart local v4    # "inFile":Ljava/io/File;
    .restart local v6    # "inStream":Ljava/io/FileInputStream;
    .restart local v8    # "strBuffer":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 613
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    move-object v7, v8

    .line 615
    .end local v8    # "strBuffer":Ljava/lang/String;
    .restart local v7    # "strBuffer":Ljava/lang/String;
    goto :goto_0

    .line 604
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v1

    .line 605
    .local v1, "e":Ljava/io/IOException;
    :goto_3
    :try_start_5
    const-string v9, "sales_code reading failed"

    invoke-static {v9}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 608
    if-eqz v6, :cond_4

    .line 609
    :try_start_6
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V

    .line 610
    :cond_4
    if-eqz v2, :cond_3

    .line 611
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_0

    .line 612
    :catch_3
    move-exception v1

    .line 613
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 607
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v9

    .line 608
    :goto_4
    if-eqz v6, :cond_5

    .line 609
    :try_start_7
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V

    .line 610
    :cond_5
    if-eqz v2, :cond_6

    .line 611
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4

    .line 614
    :cond_6
    :goto_5
    throw v9

    .line 612
    :catch_4
    move-exception v1

    .line 613
    .restart local v1    # "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5

    .line 607
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v7    # "strBuffer":Ljava/lang/String;
    .restart local v8    # "strBuffer":Ljava/lang/String;
    :catchall_1
    move-exception v9

    move-object v7, v8

    .end local v8    # "strBuffer":Ljava/lang/String;
    .restart local v7    # "strBuffer":Ljava/lang/String;
    goto :goto_4

    .line 604
    .end local v7    # "strBuffer":Ljava/lang/String;
    .restart local v8    # "strBuffer":Ljava/lang/String;
    :catch_5
    move-exception v1

    move-object v7, v8

    .end local v8    # "strBuffer":Ljava/lang/String;
    .restart local v7    # "strBuffer":Ljava/lang/String;
    goto :goto_3

    .line 593
    .end local v6    # "inStream":Ljava/io/FileInputStream;
    .restart local v5    # "inStream":Ljava/io/FileInputStream;
    :catch_6
    move-exception v1

    move-object v3, v4

    .end local v4    # "inFile":Ljava/io/File;
    .restart local v3    # "inFile":Ljava/io/File;
    goto :goto_2
.end method

.method public loadInstallHistory()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 833
    const-string v2, "install_history"

    invoke-virtual {p0, v2, v4}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mPref:Landroid/content/SharedPreferences;

    .line 834
    iget-object v2, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstallHistory:Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->clear()V

    .line 835
    iget-object v2, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mPref:Landroid/content/SharedPreferences;

    const-string v3, "count"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 836
    .local v0, "count":I
    if-lez v0, :cond_0

    .line 837
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 838
    iget-object v2, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstallHistory:Ljava/util/Collection;

    iget-object v3, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mPref:Landroid/content/SharedPreferences;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 837
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 841
    .end local v1    # "i":I
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 23
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 113
    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 114
    const/high16 v20, 0x7f020000

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setContentView(I)V

    .line 122
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v20

    const/high16 v21, 0x480000

    invoke-virtual/range {v20 .. v21}, Landroid/view/Window;->addFlags(I)V

    .line 125
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->disableNotifications()V

    .line 128
    const-string v20, "ro.build.characteristics"

    invoke-static/range {v20 .. v20}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 129
    .local v10, "deviceType":Ljava/lang/String;
    const/4 v15, 0x0

    .line 130
    .local v15, "isTablet":Z
    if-eqz v10, :cond_0

    .line 131
    const-string v20, "tablet"

    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    .line 133
    :cond_0
    if-nez v15, :cond_1

    .line 134
    const/16 v20, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 136
    :cond_1
    const-string v20, "persist.sys.storage_preload"

    invoke-static/range {v20 .. v20}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 137
    .local v17, "mStoragePreload":Ljava/lang/String;
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "persist.sys.storage_preload : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    .line 138
    if-eqz v17, :cond_8

    const-string v20, "2"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_8

    .line 139
    invoke-static {}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->isChinaModel()Z

    move-result v20

    if-eqz v20, :cond_d

    .line 140
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v20

    const-string v21, "CscFeature_Setting_ConfigActivationModeForPreloadInstaller"

    invoke-virtual/range {v20 .. v21}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v20

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mForceEnable:Z

    .line 141
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mForceEnable:Z

    move/from16 v20, v0

    if-eqz v20, :cond_c

    .line 142
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v20

    const-string v21, "CscFeature_Setting_ConfigApkListForPreloadInstaller"

    invoke-virtual/range {v20 .. v21}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 143
    .local v5, "apkListToParse":Ljava/lang/String;
    const-string v20, " "

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 144
    .local v6, "apks":[Ljava/lang/String;
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_0
    array-length v0, v6

    move/from16 v20, v0

    move/from16 v0, v20

    if-ge v13, v0, :cond_4

    .line 145
    aget-object v20, v6, v13

    const-string v21, "+"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 146
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mCscFeatureAddedAPKList:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    aget-object v21, v6, v13

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mCscFeatureAddedAPKList:Ljava/lang/String;

    .line 144
    :goto_1
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 148
    :cond_2
    aget-object v20, v6, v13

    const-string v21, "-"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_3

    .line 149
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mCscFeatureUninstallPKGList:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    aget-object v21, v6, v13

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mCscFeatureUninstallPKGList:Ljava/lang/String;

    goto :goto_1

    .line 152
    :cond_3
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mCscFeatureReInstallAPKList:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    aget-object v21, v6, v13

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mCscFeatureReInstallAPKList:Ljava/lang/String;

    goto :goto_1

    .line 155
    :cond_4
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "mCscFeatureAddedAPKList = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mCscFeatureAddedAPKList:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    .line 156
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "mCscFeatureUninstallPKGList = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mCscFeatureUninstallPKGList:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    .line 157
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "mCscFeatureReInstallAPKList = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mCscFeatureReInstallAPKList:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    .line 158
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mCscFeatureAddedAPKList:Ljava/lang/String;

    move-object/from16 v20, v0

    if-eqz v20, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mCscFeatureAddedAPKList:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v20

    if-gtz v20, :cond_7

    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mCscFeatureReInstallAPKList:Ljava/lang/String;

    move-object/from16 v20, v0

    if-eqz v20, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mCscFeatureReInstallAPKList:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v20

    if-gtz v20, :cond_7

    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mCscFeatureUninstallPKGList:Ljava/lang/String;

    move-object/from16 v20, v0

    if-eqz v20, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mCscFeatureUninstallPKGList:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v20

    if-lez v20, :cond_b

    .line 161
    :cond_7
    const-string v20, "mForceEnable is true."

    invoke-static/range {v20 .. v20}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    .line 162
    const-string v20, "persist.sys.storage_preload"

    const-string v21, "1"

    invoke-static/range {v20 .. v21}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    const-wide/16 v20, 0x3e8

    invoke-static/range {v20 .. v21}, Landroid/os/SystemClock;->sleep(J)V

    .line 183
    .end local v5    # "apkListToParse":Ljava/lang/String;
    .end local v6    # "apks":[Ljava/lang/String;
    .end local v13    # "i":I
    :cond_8
    :goto_2
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->isVerifierInstalled()Z

    move-result v20

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mIsVerifier:Z

    .line 184
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "[Verify App]Is Verifier Installed : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mIsVerifier:Z

    move/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    .line 185
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mIsVerifier:Z

    move/from16 v20, v0

    if-eqz v20, :cond_9

    .line 186
    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v20

    const-string v21, "package_verifier_enable"

    const/16 v22, 0x1

    invoke-static/range {v20 .. v22}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v20

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mPreviousVerifierValue:I

    .line 189
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "[Verify App]current value is "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mPreviousVerifierValue:I

    move/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", set to 0"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    .line 190
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mPreviousVerifierValue:I

    move/from16 v20, v0

    if-eqz v20, :cond_9

    .line 191
    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v20

    const-string v21, "package_verifier_enable"

    const/16 v22, 0x0

    invoke-static/range {v20 .. v22}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 196
    :cond_9
    invoke-static {}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->isChinaModel()Z

    move-result v20

    if-eqz v20, :cond_10

    .line 197
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mCscFeatureUninstallPKGList:Ljava/lang/String;

    move-object/from16 v20, v0

    const-string v21, "-"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v19

    .line 198
    .local v19, "strArrayUninstallPkgList":[Ljava/lang/String;
    const-string v7, ""

    .line 201
    .local v7, "appNames":Ljava/lang/String;
    move-object/from16 v8, v19

    .local v8, "arr$":[Ljava/lang/String;
    array-length v0, v8

    move/from16 v16, v0

    .local v16, "len$":I
    const/4 v14, 0x0

    .local v14, "i$":I
    :goto_3
    move/from16 v0, v16

    if-ge v14, v0, :cond_e

    aget-object v18, v8, v14

    .line 203
    .local v18, "s":Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v20

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 208
    .local v4, "ai":Landroid/content/pm/ApplicationInfo;
    :goto_4
    if-eqz v4, :cond_a

    .line 209
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "\n"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v20

    check-cast v20, Ljava/lang/String;

    nop

    nop

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 201
    :cond_a
    add-int/lit8 v14, v14, 0x1

    goto :goto_3

    .line 166
    .end local v4    # "ai":Landroid/content/pm/ApplicationInfo;
    .end local v7    # "appNames":Ljava/lang/String;
    .end local v8    # "arr$":[Ljava/lang/String;
    .end local v14    # "i$":I
    .end local v16    # "len$":I
    .end local v18    # "s":Ljava/lang/String;
    .end local v19    # "strArrayUninstallPkgList":[Ljava/lang/String;
    .restart local v5    # "apkListToParse":Ljava/lang/String;
    .restart local v6    # "apks":[Ljava/lang/String;
    .restart local v13    # "i":I
    :cond_b
    const-string v20, "mForceEnable is ture. But empty apk list. call setDisabled"

    invoke-static/range {v20 .. v20}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    .line 167
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->setDisabled()V

    .line 243
    .end local v5    # "apkListToParse":Ljava/lang/String;
    .end local v6    # "apks":[Ljava/lang/String;
    .end local v13    # "i":I
    :goto_5
    return-void

    .line 172
    :cond_c
    const-string v20, "mForceEnable is false. call setDisabled"

    invoke-static/range {v20 .. v20}, Lcom/sec/android/preloadinstaller/LogMsg;->out(Ljava/lang/String;)V

    .line 173
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->setDisabled()V

    goto :goto_5

    .line 178
    :cond_d
    const-string v20, "persist.sys.storage_preload"

    const-string v21, "1"

    invoke-static/range {v20 .. v21}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    const-wide/16 v20, 0x3e8

    invoke-static/range {v20 .. v21}, Landroid/os/SystemClock;->sleep(J)V

    goto/16 :goto_2

    .line 204
    .restart local v7    # "appNames":Ljava/lang/String;
    .restart local v8    # "arr$":[Ljava/lang/String;
    .restart local v14    # "i$":I
    .restart local v16    # "len$":I
    .restart local v18    # "s":Ljava/lang/String;
    .restart local v19    # "strArrayUninstallPkgList":[Ljava/lang/String;
    :catch_0
    move-exception v12

    .line 205
    .local v12, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v4, 0x0

    .restart local v4    # "ai":Landroid/content/pm/ApplicationInfo;
    goto :goto_4

    .line 213
    .end local v4    # "ai":Landroid/content/pm/ApplicationInfo;
    .end local v12    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v18    # "s":Ljava/lang/String;
    :cond_e
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mIsFota:Z

    move/from16 v20, v0

    if-eqz v20, :cond_f

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mForceEnable:Z

    move/from16 v20, v0

    if-eqz v20, :cond_f

    if-eqz v7, :cond_f

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v20

    if-lez v20, :cond_f

    .line 214
    new-instance v9, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 215
    .local v9, "builder":Landroid/app/AlertDialog$Builder;
    const v20, 0x7f040002

    move/from16 v0, v20

    invoke-virtual {v9, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v20

    const v21, 0x7f040003

    new-instance v22, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$3;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$3;-><init>(Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;[Ljava/lang/String;)V

    invoke-virtual/range {v20 .. v22}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v20

    const v21, 0x7f040004

    new-instance v22, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$2;

    invoke-direct/range {v22 .. v23}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity$2;-><init>(Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;)V

    invoke-virtual/range {v20 .. v22}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 233
    invoke-virtual {v9}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v11

    .line 234
    .local v11, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v11}, Landroid/app/Dialog;->show()V

    goto :goto_5

    .line 237
    .end local v9    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v11    # "dialog":Landroid/app/AlertDialog;
    :cond_f
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->startPreloadInstaller()V

    goto :goto_5

    .line 241
    .end local v7    # "appNames":Ljava/lang/String;
    .end local v8    # "arr$":[Ljava/lang/String;
    .end local v14    # "i$":I
    .end local v16    # "len$":I
    .end local v19    # "strArrayUninstallPkgList":[Ljava/lang/String;
    :cond_10
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->startPreloadInstaller()V

    goto :goto_5
.end method

.method public putInstallHistory(Ljava/lang/String;)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 819
    iget-object v2, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mPref:Landroid/content/SharedPreferences;

    if-nez v2, :cond_0

    .line 820
    invoke-virtual {p0}, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->loadInstallHistory()V

    .line 821
    :cond_0
    iget-object v2, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstallHistory:Ljava/util/Collection;

    invoke-interface {v2, p1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 822
    iget-object v2, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstallHistory:Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v1

    .line 823
    .local v1, "index":I
    iget-object v2, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mInstallHistory:Ljava/util/Collection;

    invoke-interface {v2, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 824
    iget-object v2, p0, Lcom/sec/android/preloadinstaller/SN1505121331/PreloadInstallerActivity;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 825
    .local v0, "edit":Landroid/content/SharedPreferences$Editor;
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 826
    const-string v2, "count"

    add-int/lit8 v3, v1, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 827
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 828
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "saveInstallHistory() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/preloadinstaller/LogMsg;->write(Ljava/lang/String;)V

    .line 830
    .end local v0    # "edit":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "index":I
    :cond_1
    return-void
.end method

.method public readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;
    .locals 15
    .param p1, "file"    # Ljava/io/File;
    .param p2, "max"    # I
    .param p3, "ellipsis"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 750
    new-instance v4, Ljava/io/FileInputStream;

    move-object/from16 v0, p1

    invoke-direct {v4, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 752
    .local v4, "input":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v10

    .line 753
    .local v10, "size":J
    if-gtz p2, :cond_0

    const-wide/16 v12, 0x0

    cmp-long v12, v10, v12

    if-lez v12, :cond_6

    if-nez p2, :cond_6

    .line 755
    :cond_0
    const-wide/16 v12, 0x0

    cmp-long v12, v10, v12

    if-lez v12, :cond_2

    if-eqz p2, :cond_1

    move/from16 v0, p2

    int-to-long v12, v0

    cmp-long v12, v10, v12

    if-gez v12, :cond_2

    .line 756
    :cond_1
    long-to-int v0, v10

    move/from16 p2, v0

    .line 757
    :cond_2
    add-int/lit8 v12, p2, 0x1

    new-array v3, v12, [B

    .line 758
    .local v3, "data":[B
    invoke-virtual {v4, v3}, Ljava/io/InputStream;->read([B)I

    move-result v7

    .line 759
    .local v7, "length":I
    if-gtz v7, :cond_3

    .line 760
    const-string v12, ""
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 806
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .end local v7    # "length":I
    :goto_0
    return-object v12

    .line 761
    .restart local v7    # "length":I
    :cond_3
    move/from16 v0, p2

    if-gt v7, v0, :cond_4

    .line 762
    :try_start_1
    new-instance v12, Ljava/lang/String;

    const/4 v13, 0x0

    invoke-direct {v12, v3, v13, v7}, Ljava/lang/String;-><init>([BII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 806
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    goto :goto_0

    .line 763
    :cond_4
    if-nez p3, :cond_5

    .line 764
    :try_start_2
    new-instance v12, Ljava/lang/String;

    const/4 v13, 0x0

    move/from16 v0, p2

    invoke-direct {v12, v3, v13, v0}, Ljava/lang/String;-><init>([BII)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 806
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    goto :goto_0

    .line 765
    :cond_5
    :try_start_3
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v13, Ljava/lang/String;

    const/4 v14, 0x0

    move/from16 v0, p2

    invoke-direct {v13, v3, v14, v0}, Ljava/lang/String;-><init>([BII)V

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p3

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v12

    .line 806
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    goto :goto_0

    .line 766
    .end local v3    # "data":[B
    .end local v7    # "length":I
    :cond_6
    if-gez p2, :cond_f

    .line 768
    const/4 v8, 0x0

    .line 769
    .local v8, "rolled":Z
    const/4 v5, 0x0

    .local v5, "last":[B
    const/4 v3, 0x0

    .line 771
    .restart local v3    # "data":[B
    :cond_7
    if-eqz v5, :cond_8

    .line 772
    const/4 v8, 0x1

    .line 773
    :cond_8
    move-object v9, v5

    .line 774
    .local v9, "tmp":[B
    move-object v5, v3

    .line 775
    move-object v3, v9

    .line 776
    if-nez v3, :cond_9

    .line 777
    move/from16 v0, p2

    neg-int v12, v0

    :try_start_4
    new-array v3, v12, [B

    .line 778
    :cond_9
    invoke-virtual {v4, v3}, Ljava/io/InputStream;->read([B)I

    move-result v6

    .line 779
    .local v6, "len":I
    array-length v12, v3

    if-eq v6, v12, :cond_7

    .line 781
    if-nez v5, :cond_a

    if-gtz v6, :cond_a

    .line 782
    const-string v12, ""
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 806
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    goto :goto_0

    .line 783
    :cond_a
    if-nez v5, :cond_b

    .line 784
    :try_start_5
    new-instance v12, Ljava/lang/String;

    const/4 v13, 0x0

    invoke-direct {v12, v3, v13, v6}, Ljava/lang/String;-><init>([BII)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 806
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    goto :goto_0

    .line 785
    :cond_b
    if-lez v6, :cond_c

    .line 786
    const/4 v8, 0x1

    .line 787
    const/4 v12, 0x0

    :try_start_6
    array-length v13, v5

    sub-int/2addr v13, v6

    invoke-static {v5, v6, v5, v12, v13}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 788
    const/4 v12, 0x0

    array-length v13, v5

    sub-int/2addr v13, v6

    invoke-static {v3, v12, v5, v13, v6}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 790
    :cond_c
    if-eqz p3, :cond_d

    if-nez v8, :cond_e

    .line 791
    :cond_d
    new-instance v12, Ljava/lang/String;

    invoke-direct {v12, v5}, Ljava/lang/String;-><init>([B)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 806
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    goto/16 :goto_0

    .line 792
    :cond_e
    :try_start_7
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p3

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    new-instance v13, Ljava/lang/String;

    invoke-direct {v13, v5}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result-object v12

    .line 806
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    goto/16 :goto_0

    .line 795
    .end local v3    # "data":[B
    .end local v5    # "last":[B
    .end local v6    # "len":I
    .end local v8    # "rolled":Z
    .end local v9    # "tmp":[B
    :cond_f
    :try_start_8
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 797
    .local v2, "contents":Ljava/io/ByteArrayOutputStream;
    const/16 v12, 0x400

    new-array v3, v12, [B

    .line 799
    .restart local v3    # "data":[B
    :cond_10
    invoke-virtual {v4, v3}, Ljava/io/InputStream;->read([B)I

    move-result v6

    .line 800
    .restart local v6    # "len":I
    if-lez v6, :cond_11

    .line 801
    const/4 v12, 0x0

    invoke-virtual {v2, v3, v12, v6}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 802
    :cond_11
    array-length v12, v3

    if-eq v6, v12, :cond_10

    .line 803
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-result-object v12

    .line 806
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    goto/16 :goto_0

    .end local v2    # "contents":Ljava/io/ByteArrayOutputStream;
    .end local v3    # "data":[B
    .end local v6    # "len":I
    .end local v10    # "size":J
    :catchall_0
    move-exception v12

    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    throw v12
.end method
