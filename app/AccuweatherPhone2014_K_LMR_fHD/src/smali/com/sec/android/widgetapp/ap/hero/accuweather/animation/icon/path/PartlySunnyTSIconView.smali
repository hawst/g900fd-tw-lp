.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;
.super Landroid/view/View;
.source "PartlySunnyTSIconView.java"

# interfaces
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;


# instance fields
.field private CloudTranslate:Landroid/animation/ValueAnimator;

.field private DropCenterT1:Landroid/animation/ValueAnimator;

.field private DropCenterT2:Landroid/animation/ValueAnimator;

.field private DropCenterT4:Landroid/animation/ValueAnimator;

.field private DropCenterT5:Landroid/animation/ValueAnimator;

.field private DropThunderT3:Landroid/animation/ValueAnimator;

.field private SunRotation:Landroid/animation/ValueAnimator;

.field private SunScale1:Landroid/animation/ValueAnimator;

.field private SunScale2:Landroid/animation/ValueAnimator;

.field isStop:Z

.field l:Landroid/animation/Animator$AnimatorListener;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mCloud:Landroid/graphics/Path;

.field private mIsActiveAnimationThread:Z

.field private mMasking:Landroid/graphics/Path;

.field private mPaint:Landroid/graphics/Paint;

.field private mPaintColor:I

.field public mPreFixT1:Ljava/lang/String;

.field public mPreFixT2:Ljava/lang/String;

.field public mPreFixT3:Ljava/lang/String;

.field public mPreFixT4:Ljava/lang/String;

.field public mPreFixT5:Ljava/lang/String;

.field private mRainLine1:Landroid/graphics/Path;

.field private mRainLine2:Landroid/graphics/Path;

.field private mRainLine4:Landroid/graphics/Path;

.field private mRainLine5:Landroid/graphics/Path;

.field private mScale:F

.field private mSun:Landroid/graphics/Path;

.field private mSunOuter:Landroid/graphics/Path;

.field private mThunder:Landroid/graphics/Path;

.field rotateOffset:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 92
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 93
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 88
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 89
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 83
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    .line 31
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunRotation:Landroid/animation/ValueAnimator;

    .line 33
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunScale1:Landroid/animation/ValueAnimator;

    .line 35
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunScale2:Landroid/animation/ValueAnimator;

    .line 37
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    .line 39
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    .line 41
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropThunderT3:Landroid/animation/ValueAnimator;

    .line 43
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT4:Landroid/animation/ValueAnimator;

    .line 45
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT5:Landroid/animation/ValueAnimator;

    .line 47
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mIsActiveAnimationThread:Z

    .line 49
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPaint:Landroid/graphics/Paint;

    .line 51
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mMasking:Landroid/graphics/Path;

    .line 53
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    .line 55
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mSun:Landroid/graphics/Path;

    .line 57
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mSunOuter:Landroid/graphics/Path;

    .line 59
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine1:Landroid/graphics/Path;

    .line 61
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine2:Landroid/graphics/Path;

    .line 63
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mThunder:Landroid/graphics/Path;

    .line 65
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine4:Landroid/graphics/Path;

    .line 67
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine5:Landroid/graphics/Path;

    .line 69
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    .line 72
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mBitmap:Landroid/graphics/Bitmap;

    .line 76
    const v0, -0xa0a0b

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPaintColor:I

    .line 543
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->rotateOffset:F

    .line 544
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->l:Landroid/animation/Animator$AnimatorListener;

    .line 591
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPreFixT1:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPreFixT2:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPreFixT3:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPreFixT4:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPreFixT5:Ljava/lang/String;

    .line 783
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->isStop:Z

    .line 84
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->init()V

    .line 85
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunRotation:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunScale1:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunScale2:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropThunderT3:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT4:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT5:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mIsActiveAnimationThread:Z

    return v0
.end method

.method private drawCloud(Landroid/graphics/Canvas;Landroid/graphics/Paint;FF)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "paint"    # Landroid/graphics/Paint;
    .param p3, "dx"    # F
    .param p4, "dy"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 329
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPaintColor:I

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 330
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 331
    neg-float v0, p3

    const/high16 v1, 0x40a00000    # 5.0f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    add-float/2addr v1, p4

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 332
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    invoke-virtual {p1, v0, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 333
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 334
    return-void
.end method

.method private drawDrop(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/high16 v13, -0x3db80000    # -50.0f

    .line 390
    const/4 v0, 0x0

    .line 391
    .local v0, "dcenterx1":F
    const/4 v4, 0x0

    .line 393
    .local v4, "dcentery1":F
    const/16 v9, 0xff

    .line 394
    .local v9, "thunderA":I
    const/4 v6, 0x0

    .line 396
    .local v6, "dcentery3":F
    const/4 v1, 0x0

    .line 397
    .local v1, "dcenterx2":F
    const/4 v5, 0x0

    .line 399
    .local v5, "dcentery2":F
    const/4 v2, 0x0

    .line 400
    .local v2, "dcenterx4":F
    const/4 v7, 0x0

    .line 402
    .local v7, "dcentery4":F
    const/4 v3, 0x0

    .line 403
    .local v3, "dcenterx5":F
    const/4 v8, 0x0

    .line 404
    .local v8, "dcentery5":F
    iget-boolean v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mIsActiveAnimationThread:Z

    if-eqz v10, :cond_4

    .line 408
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    invoke-virtual {v10}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v10

    if-eqz v10, :cond_5

    .line 409
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPreFixT1:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "x"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Float;

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 410
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPreFixT1:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "y"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Float;

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v4

    .line 417
    :cond_0
    :goto_0
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    invoke-virtual {v10}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v10

    if-eqz v10, :cond_6

    .line 418
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPreFixT2:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "x"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Float;

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 419
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPreFixT2:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "y"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Float;

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v5

    .line 426
    :cond_1
    :goto_1
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropThunderT3:Landroid/animation/ValueAnimator;

    invoke-virtual {v10}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v10

    if-eqz v10, :cond_7

    .line 427
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropThunderT3:Landroid/animation/ValueAnimator;

    const-string v11, "t"

    invoke-virtual {v10, v11}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Float;

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v6

    .line 428
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropThunderT3:Landroid/animation/ValueAnimator;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPreFixT3:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "a"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 436
    :cond_2
    :goto_2
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT4:Landroid/animation/ValueAnimator;

    invoke-virtual {v10}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v10

    if-eqz v10, :cond_8

    .line 437
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT4:Landroid/animation/ValueAnimator;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPreFixT4:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "x"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Float;

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v2

    .line 438
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT4:Landroid/animation/ValueAnimator;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPreFixT4:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "y"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Float;

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v7

    .line 445
    :cond_3
    :goto_3
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT5:Landroid/animation/ValueAnimator;

    invoke-virtual {v10}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v10

    if-eqz v10, :cond_9

    .line 446
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT5:Landroid/animation/ValueAnimator;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPreFixT5:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "x"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Float;

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v3

    .line 447
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT5:Landroid/animation/ValueAnimator;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPreFixT5:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "y"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Float;

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v8

    .line 454
    :cond_4
    :goto_4
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 455
    invoke-virtual {p1, v0, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 456
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine1:Landroid/graphics/Path;

    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v10, v11}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 457
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 459
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 460
    invoke-virtual {p1, v2, v7}, Landroid/graphics/Canvas;->translate(FF)V

    .line 461
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine2:Landroid/graphics/Path;

    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v10, v11}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 462
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 464
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 465
    const/4 v10, 0x0

    invoke-virtual {p1, v10, v6}, Landroid/graphics/Canvas;->translate(FF)V

    .line 466
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v10, v9}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 467
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mThunder:Landroid/graphics/Path;

    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v10, v11}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 468
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPaint:Landroid/graphics/Paint;

    const/16 v11, 0xff

    invoke-virtual {v10, v11}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 469
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 471
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 472
    invoke-virtual {p1, v1, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 473
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine4:Landroid/graphics/Path;

    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v10, v11}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 474
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 476
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 477
    invoke-virtual {p1, v3, v8}, Landroid/graphics/Canvas;->translate(FF)V

    .line 478
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine5:Landroid/graphics/Path;

    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v10, v11}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 479
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 481
    return-void

    .line 412
    :cond_5
    iget-boolean v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->isStop:Z

    if-nez v10, :cond_0

    .line 413
    const/high16 v10, -0x3e380000    # -25.0f

    iget v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float v4, v10, v11

    goto/16 :goto_0

    .line 421
    :cond_6
    iget-boolean v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->isStop:Z

    if-nez v10, :cond_1

    .line 422
    const/high16 v10, -0x3dcc0000    # -45.0f

    iget v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float v5, v10, v11

    goto/16 :goto_1

    .line 430
    :cond_7
    iget-boolean v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->isStop:Z

    if-nez v10, :cond_2

    .line 431
    iget v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float v6, v13, v10

    .line 432
    const/16 v9, 0xff

    goto/16 :goto_2

    .line 440
    :cond_8
    iget-boolean v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->isStop:Z

    if-nez v10, :cond_3

    .line 441
    iget v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float v7, v13, v10

    goto/16 :goto_3

    .line 449
    :cond_9
    iget-boolean v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->isStop:Z

    if-nez v10, :cond_4

    .line 450
    iget v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float v8, v13, v10

    goto/16 :goto_4
.end method

.method private drawSun(Landroid/graphics/Canvas;Landroid/graphics/Paint;FFF)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "paint"    # Landroid/graphics/Paint;
    .param p3, "rotation"    # F
    .param p4, "s1"    # F
    .param p5, "s2"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const v4, 0x42a6dc29    # 83.43f

    const v3, 0x42a3999a    # 81.8f

    .line 338
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 339
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v0, v3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v4

    invoke-virtual {p1, p4, p4, v0, v1}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 340
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mSun:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 342
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 343
    const/4 v0, 0x0

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v4

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 344
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mSunOuter:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 345
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 347
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 348
    const/high16 v0, 0x42b40000    # 90.0f

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v4

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 349
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mSunOuter:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 350
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 352
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 353
    const/high16 v0, 0x43340000    # 180.0f

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v4

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 354
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mSunOuter:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 355
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 357
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 358
    const/high16 v0, 0x43870000    # 270.0f

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v4

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 359
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mSunOuter:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 360
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 361
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 363
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 364
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v0, v3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v4

    invoke-virtual {p1, p5, p5, v0, v1}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 366
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 367
    const/high16 v0, 0x42340000    # 45.0f

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v4

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 368
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mSunOuter:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 369
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 371
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 372
    const/high16 v0, 0x43070000    # 135.0f

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v4

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 373
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mSunOuter:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 374
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 376
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 377
    const/high16 v0, 0x43610000    # 225.0f

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v4

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 378
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mSunOuter:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 379
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 381
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 382
    const v0, 0x439d8000    # 315.0f

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v4

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 383
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mSunOuter:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 384
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 386
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 387
    return-void
.end method

.method private drawTransparentArea(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "paint"    # Landroid/graphics/Paint;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 323
    new-instance v0, Landroid/graphics/PorterDuffXfermode;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v1}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 324
    invoke-virtual {p1, p2, v2, v2, p3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 325
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 326
    return-void
.end method

.method private init()V
    .locals 4

    .prologue
    const/high16 v3, 0x43960000    # 300.0f

    .line 96
    invoke-virtual {p0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 97
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPaint:Landroid/graphics/Paint;

    .line 98
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->initPath()V

    .line 99
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPaintColor:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 100
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 101
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 102
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x40a00000    # 5.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 104
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mBitmap:Landroid/graphics/Bitmap;

    .line 106
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 107
    .local v0, "mMaskingCanvas":Landroid/graphics/Canvas;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mMasking:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 108
    return-void
.end method

.method private initPath()V
    .locals 13

    .prologue
    const v12, 0x428e1eb8    # 71.06f

    const v11, 0x4274b333    # 61.175f

    const v10, 0x43358fdf

    const v9, 0x43135df4

    const v8, 0x42a3e560

    .line 111
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mMasking:Landroid/graphics/Path;

    .line 112
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x433613f8

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42c44312

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 113
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x43435917

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42c4cfdf

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x434e045a

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42dad1ec    # 109.41f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x434e045a

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42f57ae1    # 122.74f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 115
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x434e045a

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43083e77

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x434311ec    # 195.07f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v9

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v10

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v9

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 117
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x42932f1b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v9

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 118
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x42857852    # 66.735f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v9

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v11

    const v4, 0x430da312

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v11

    const v6, 0x4306c7f0

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 120
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mMasking:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v11

    const v2, 0x42ffd917

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42857852    # 66.735f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42f48ed9    # 122.279f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42932f1b

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42f48ed9    # 122.279f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 122
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x4293ef1b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42f48ed9    # 122.279f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4294ac08

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42f4849c

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x429567f0

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42f494fe    # 122.291f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 124
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x4294d99a

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42f224dd

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42948c4a

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42ef91ec

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42948c4a

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42ecf5c3    # 118.48f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x42948c4a

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42da2c8b

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42a3c831

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42caea7f    # 101.458f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42b693f8

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42caea7f    # 101.458f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 128
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x42b6ab02    # 91.334f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42caea7f    # 101.458f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42b6c083    # 91.376f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42caeb02    # 101.459f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42b6d78d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42caeb02    # 101.459f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 130
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x42ce8fdf

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42b413f8

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 131
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x42d8b0a4

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a42b02    # 82.084f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42e8276d

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4298020c

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42fa6e98

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4292170a

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 133
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x4300dc6a

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x428fcdd3    # 71.902f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43052ed9

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v12

    const v5, 0x4309d2f2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v12

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 135
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x4318ad50

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v12

    const v3, 0x4326c625

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x429f52f2

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x432dbae1    # 173.73f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42b8b2b0    # 92.349f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 137
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mMasking:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 138
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x3f68ec71

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x434f526b

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 139
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x435cdfdd

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x434f526b

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 140
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x435d6160

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43941fc2

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 141
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mMasking:Landroid/graphics/Path;

    const v1, -0x40317b92

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43951fe0

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 142
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x3f68ec71

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x434f526b

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 144
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    .line 145
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v10

    const v2, 0x431a2f9e

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 146
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x4346ce56    # 198.806f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x431a2f9e

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4354d5c3

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x430bfc29    # 139.985f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4354d5c3

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42f57b64

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 148
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x4354d5c3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x42d30000    # 105.5f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4346ce56    # 198.806f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42b6f3b6

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v10

    const v6, 0x42b6f3b6

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 150
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x433554fe    # 181.332f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42b6f3b6

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43351a5e

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42b6c937

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4334df3b

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42b6cb44

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 152
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x432cfe77

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4296028f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431c51ec    # 156.32f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x428092f2

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4309d2f2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x428092f2

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 154
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x430776c9

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x428092f2

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43051e35

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4280e7f0

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4302cd0e

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4281999a    # 64.8f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 156
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42f04106

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42847efa

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42dd072b    # 110.514f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x428d8a3d    # 70.77f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42ce61cb

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x429d5810

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 158
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42c77cee

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a4c937

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42c1e354    # 96.944f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42ad4dd3    # 86.652f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42bda666

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42b691ec

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 160
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42bc8f5c    # 94.28f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42b8d375

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42bb87ae    # 93.765f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42bb1a1d

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42ba9db2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42bd7021

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 162
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42b9472b    # 92.639f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42bd53f8

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42b7ee98

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42bd4419

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42b693f8

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42bd4419

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 164
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x429db74c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42bd4419

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42893d71    # 68.62f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42d064dd

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42871893

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42e8b3b6

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 166
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x426f9375

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42edc7ae    # 118.89f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42596e98

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42fc91ec

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42596e98

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4306c873

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 168
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42596e98

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43116354    # 145.388f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x427bf2b0    # 62.987f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x431a2fdf

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42932f9e

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x431a2fdf

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 170
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v10

    const v2, 0x431a2fdf

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 171
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v10

    const v2, 0x431a2f9e

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 172
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 173
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v10

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v9

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 174
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42932f1b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v9

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 175
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42857852    # 66.735f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v9

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v11

    const v4, 0x430da312

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v11

    const v6, 0x4306c7f0

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 177
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v11

    const v2, 0x42ffd917

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42857852    # 66.735f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42f48ed9    # 122.279f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42932f1b

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42f48ed9    # 122.279f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 179
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x4293ef1b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42f48ed9    # 122.279f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4294ac08

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42f4849c

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x429567f0

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42f494fe    # 122.291f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 181
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x4294d99a

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42f224dd

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42948c4a

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42ef91ec

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42948c4a

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42ecf5c3    # 118.48f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 183
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42948c4a

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42da2c8b

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42a3c831

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42caea7f    # 101.458f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42b693f8

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42caea7f    # 101.458f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 185
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42b6ab02    # 91.334f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42caea7f    # 101.458f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42b6c083    # 91.376f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42caeb02    # 101.459f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42b6d78d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42caeb02    # 101.459f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 187
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42b52b85    # 90.585f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42d27be7

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42b44ccd    # 90.15f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42da578d

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42b44ccd    # 90.15f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42e26042

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 189
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42b44ccd    # 90.15f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42e6245a    # 115.071f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42b75a1d

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42e93021

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42bb1db2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42e93021

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 191
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42bee1cb

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42e93021

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42c1ef1b

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42e6224e

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42c1ef1b

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42e25e35

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 193
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42c1ef1b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42d0d4fe    # 104.416f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42c688b4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42c1170a

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42ce8fdf

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42b413f8

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 195
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42d8b0a4

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a42b02    # 82.084f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42e8276d

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4298020c

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42fa6e98

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4292170a

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 197
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x4300dc6a

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x428fcdd3    # 71.902f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43052ed9

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v12

    const v5, 0x4309d2f2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v12

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 199
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x4318ad50

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v12

    const v3, 0x4326c625

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x429f52f2

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x432dbae1    # 173.73f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42b8b2b0    # 92.349f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 201
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x4325d021

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42bcbbe7

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431ee20c

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42c6a45a    # 99.321f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431a5e35

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42d4deb8

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 203
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x43195ba6

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42d80c4a

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4319d3b6

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42dc428f    # 110.13f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431b6ac1

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42de4625

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 205
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x431bfc29    # 155.985f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42defdf4

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431c9db2

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42df5604    # 111.668f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431d3df4

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42df5604    # 111.668f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 207
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x431e5eb8    # 158.37f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42df5604    # 111.668f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431f7917

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42de374c

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43201f7d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42dc2d91

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 209
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x4324dd2f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42cd3c6a

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x432bf581    # 171.959f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42c44312

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x433613f8

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42c44312

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 211
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x43435917

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42c4cfdf

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x434e045a

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42dad1ec    # 109.41f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x434e045a

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42f57ae1    # 122.74f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 213
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x434e045a

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43083e77

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x434311ec    # 195.07f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v9

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v10

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v9

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 215
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 217
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mSunOuter:Landroid/graphics/Path;

    .line 218
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mSunOuter:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v8

    const v2, 0x41fc76c9    # 31.558f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 219
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mSunOuter:Landroid/graphics/Path;

    const v1, 0x42a01aa0

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41fc76c9    # 31.558f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x429d07ae    # 78.515f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x41f02b02    # 30.021f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x429d07ae    # 78.515f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x41e1020c    # 28.126f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 221
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mSunOuter:Landroid/graphics/Path;

    const v1, 0x429d07ae    # 78.515f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x413f78d5    # 11.967f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 222
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mSunOuter:Landroid/graphics/Path;

    const v1, 0x429d07ae    # 78.515f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x412122d1    # 10.071f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42a01aa0

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x41088b44    # 8.534f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v8

    const v6, 0x41088b44    # 8.534f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 224
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mSunOuter:Landroid/graphics/Path;

    const v1, 0x42a7b021

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41088b44    # 8.534f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42aac312

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x412122d1    # 10.071f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42aac312

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x413f78d5    # 11.967f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 226
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mSunOuter:Landroid/graphics/Path;

    const v1, 0x42aac312

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41e1020c    # 28.126f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 227
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mSunOuter:Landroid/graphics/Path;

    const v1, 0x42aac28f    # 85.38f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41f02b02    # 30.021f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42a7af9e

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x41fc76c9    # 31.558f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v8

    const v6, 0x41fc76c9    # 31.558f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 230
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mSun:Landroid/graphics/Path;

    .line 231
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mSun:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v8

    const v2, 0x42f5353f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 232
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mSun:Landroid/graphics/Path;

    const v1, 0x4271c8b4    # 60.446f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42f5353f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x422bced9    # 42.952f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42d23a5e

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x422bced9    # 42.952f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42a73cee

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 234
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mSun:Landroid/graphics/Path;

    const v1, 0x422bced9    # 42.952f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x427877cf    # 62.117f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4271c7ae    # 60.445f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42327be7

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v8

    const v6, 0x42327be7

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 236
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mSun:Landroid/graphics/Path;

    const v1, 0x42cee6e9

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42327be7

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42f1e979

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x427875c3

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42f1e979

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42a73cee

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 238
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mSun:Landroid/graphics/Path;

    const v1, 0x42f1e979

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42d23ae1

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42cee6e9

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42f5353f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v8

    const v6, 0x42f5353f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 240
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mSun:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 241
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mSun:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v8

    const v2, 0x424df1aa    # 51.486f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 242
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mSun:Landroid/graphics/Path;

    const v1, 0x42807646

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x424df1aa    # 51.486f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4247449c

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4283cd50    # 65.901f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4247449c

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42a73c6a

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 244
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mSun:Landroid/graphics/Path;

    const v1, 0x4247449c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42caa76d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42807646

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42e77ae1    # 115.74f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v8

    const v6, 0x42e77ae1    # 115.74f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 246
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mSun:Landroid/graphics/Path;

    const v1, 0x42c753f8

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42e77ae1    # 115.74f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42e42e14    # 114.09f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42caa7f0

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42e42e14    # 114.09f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42a73c6a

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 248
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mSun:Landroid/graphics/Path;

    const v1, 0x42e42e14    # 114.09f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4283cd50    # 65.901f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42c753f8

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x424df1aa    # 51.486f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v8

    const v6, 0x424df1aa    # 51.486f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 251
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine5:Landroid/graphics/Path;

    .line 252
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine5:Landroid/graphics/Path;

    const v1, 0x4308326f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4334f375

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 253
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine5:Landroid/graphics/Path;

    const v1, 0x43066c08

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4333a28f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4303ea7f    # 131.916f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4334028f    # 180.01f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x430299db

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4335c873

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 255
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine5:Landroid/graphics/Path;

    const v1, 0x42f4e8f6

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4340c76d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 256
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine5:Landroid/graphics/Path;

    const v1, 0x42f248b4

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43428dd3    # 194.554f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42f307ae    # 121.515f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43450f5c    # 197.06f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42f693f8

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43466000    # 198.375f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 258
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine5:Landroid/graphics/Path;

    const v1, 0x42f8020c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4346e7f0

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42f9ae14    # 124.84f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43472979

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42fb54fe    # 125.666f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43472979

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 260
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine5:Landroid/graphics/Path;

    const v1, 0x42fdc8b4

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43472979

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x430019db

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x434699db

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4300e20c

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43458a7f    # 197.541f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 262
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine5:Landroid/graphics/Path;

    const v1, 0x4309076d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x433a8b85

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 263
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine5:Landroid/graphics/Path;

    const v1, 0x430a5893

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4338c51f    # 184.77f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4309f917

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43364419

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4308326f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4334f375

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 266
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine4:Landroid/graphics/Path;

    .line 267
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine4:Landroid/graphics/Path;

    const v1, 0x42dea2d1    # 111.318f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432d2396

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 268
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine4:Landroid/graphics/Path;

    const v1, 0x42db170a

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432bd375

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42d613f8

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x432c322d    # 172.196f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42d371aa    # 105.722f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x432df917

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 270
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine4:Landroid/graphics/Path;

    const v1, 0x42b79375

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4340c7ae    # 192.78f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 271
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine4:Landroid/graphics/Path;

    const v1, 0x42b4f2b0    # 90.474f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43428e14

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42b5b1aa    # 90.847f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43450f9e

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42b93e77

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43466083

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 273
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine4:Landroid/graphics/Path;

    const v1, 0x42baac8b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4346e831

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42bc578d

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x434729fc

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42bdff7d    # 94.999f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x434729fc

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 275
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine4:Landroid/graphics/Path;

    const v1, 0x42c072b0    # 96.224f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x434729fc

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42c2ddb2

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43469a1d

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42c46e98

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43458b02    # 197.543f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 277
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine4:Landroid/graphics/Path;

    const v1, 0x42e04d50    # 112.151f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4332bc29    # 178.735f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 278
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine4:Landroid/graphics/Path;

    const v1, 0x42e2ef1b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4330f581    # 176.959f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42e23021

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x432e73f8

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42dea2d1    # 111.318f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x432d2396

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 281
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mThunder:Landroid/graphics/Path;

    .line 282
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mThunder:Landroid/graphics/Path;

    const v1, 0x431d6a7f    # 157.416f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43212666    # 161.15f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 283
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mThunder:Landroid/graphics/Path;

    const v1, 0x430d1646

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43342666    # 180.15f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 284
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mThunder:Landroid/graphics/Path;

    const v1, 0x4319da1d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43342666    # 180.15f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 285
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mThunder:Landroid/graphics/Path;

    const v1, 0x430e6d0e

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x434a4396

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 286
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mThunder:Landroid/graphics/Path;

    const v1, 0x432fd893

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432c2666    # 172.15f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 287
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mThunder:Landroid/graphics/Path;

    const v1, 0x43218a7f    # 161.541f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432c2666    # 172.15f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 288
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mThunder:Landroid/graphics/Path;

    const v1, 0x432b4666

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43212666    # 161.15f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 290
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine2:Landroid/graphics/Path;

    .line 291
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine2:Landroid/graphics/Path;

    const v1, 0x431cffeb    # 156.99968f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42f773e9

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 292
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine2:Landroid/graphics/Path;

    const v1, 0x431b5195

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42f49531

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4318cb70

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42f4fb14

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43175c14

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42f85637

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 294
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine2:Landroid/graphics/Path;

    const v1, 0x42eb7a6e

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43250653

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 295
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine2:Landroid/graphics/Path;

    const v1, 0x42e8a2e1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4326b6f7

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42e911fc

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43293d9f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42ec723d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x432aa9a7

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 297
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine2:Landroid/graphics/Path;

    const v1, 0x42edcee9

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432b3c99

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42ef71ba

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x432b8aef

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42f118a4

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x432b9880

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 299
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine2:Landroid/graphics/Path;

    const v1, 0x42f38b54

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432bab72

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42f606b8

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x432b2fcc

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42f7b75c

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x432a2d3d

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 301
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine2:Landroid/graphics/Path;

    const v1, 0x431d7112

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43015d8a

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 302
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine2:Landroid/graphics/Path;

    const v1, 0x431ee0b0

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42ff5f6f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431eae00

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42fa52a2

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431cffeb    # 156.99968f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42f773e9

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 305
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine1:Landroid/graphics/Path;

    .line 306
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine1:Landroid/graphics/Path;

    const v1, 0x42d135c3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43155e35

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 307
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine1:Landroid/graphics/Path;

    const v1, 0x42cda8f6    # 102.83f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43140d50

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42c8a5e3

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43146d50

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42c6051f    # 99.01f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43163333    # 150.2f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 309
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine1:Landroid/graphics/Path;

    const v1, 0x42a7ac8b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432a6189

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 310
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine1:Landroid/graphics/Path;

    const v1, 0x42a50419

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432c251f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42a5b7cf

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x432ea76d

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42a93df4

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x432ffc29    # 175.985f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 312
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine1:Landroid/graphics/Path;

    const v1, 0x42aaa979

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x433085e3

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42ac53f8

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4330c937

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42adfb64

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4330cb02    # 176.793f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 314
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine1:Landroid/graphics/Path;

    const v1, 0x42b06e98

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4330cdd3    # 176.804f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42b2dc29    # 89.43f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43304148

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42b47127    # 90.221f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x432f3375

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 316
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine1:Landroid/graphics/Path;

    const v1, 0x42d2dfbe

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x431af687

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 317
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine1:Landroid/graphics/Path;

    const v1, 0x42d58189

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43192fdf

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42d4c28f    # 106.38f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4316aed9

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42d135c3

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43155e35

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 320
    return-void
.end method


# virtual methods
.method public cancelAnimation()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 741
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 742
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 745
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunRotation:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunRotation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 746
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunRotation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 748
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunScale1:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunScale1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 749
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunScale1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 751
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunScale2:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunScale2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 752
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunScale2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 755
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 756
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 760
    :cond_4
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 761
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 766
    :cond_5
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropThunderT3:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropThunderT3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 767
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropThunderT3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 770
    :cond_6
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT4:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT4:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 771
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT4:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 776
    :cond_7
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT5:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT5:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 777
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT5:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 780
    :cond_8
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mIsActiveAnimationThread:Z

    .line 781
    return-void
.end method

.method public getScale()F
    .locals 1

    .prologue
    .line 818
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 539
    return-object p0
.end method

.method public isRunning()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 789
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunRotation:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunScale1:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunScale2:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropThunderT3:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT4:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT5:Landroid/animation/ValueAnimator;

    if-nez v1, :cond_1

    .line 809
    :cond_0
    :goto_0
    return v0

    .line 801
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunRotation:Landroid/animation/ValueAnimator;

    .line 802
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunScale1:Landroid/animation/ValueAnimator;

    .line 803
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunScale2:Landroid/animation/ValueAnimator;

    .line 804
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    .line 805
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    .line 806
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropThunderT3:Landroid/animation/ValueAnimator;

    .line 807
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT4:Landroid/animation/ValueAnimator;

    .line 808
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT5:Landroid/animation/ValueAnimator;

    .line 809
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v4, 0x43960000    # 300.0f

    const/4 v2, 0x0

    .line 485
    :try_start_0
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 487
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    cmpg-float v0, v0, v2

    if-gtz v0, :cond_1

    .line 488
    const-string v0, ""

    const-string v1, "scale is less then 0"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    :cond_0
    :goto_0
    return-void

    .line 492
    :cond_1
    const/4 v7, 0x0

    .line 493
    .local v7, "dx":F
    const/4 v8, 0x0

    .line 494
    .local v8, "dy":F
    const/4 v10, 0x0

    .line 495
    .local v10, "rotation":F
    const/high16 v11, 0x3f800000    # 1.0f

    .line 496
    .local v11, "scale1":F
    const/high16 v12, 0x3f800000    # 1.0f

    .line 498
    .local v12, "scale2":F
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mIsActiveAnimationThread:Z

    if-eqz v0, :cond_2

    .line 499
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    const-string v1, "x"

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v7

    .line 500
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    const-string v1, "y"

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v8

    .line 502
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunRotation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->rotateOffset:F

    add-float v10, v0, v1

    .line 504
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunScale1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v11

    .line 505
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunScale2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 506
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunScale2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v12

    .line 510
    :cond_2
    const/16 v6, 0x1f

    .line 512
    .local v6, "saveFlag":I
    const/16 v6, 0xf

    .line 515
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float v1, v2, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v2, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float v3, v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v4, v0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->saveLayer(FFFFLandroid/graphics/Paint;I)I

    move-result v13

    .line 518
    .local v13, "transparentSaveLevel":I
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPaint:Landroid/graphics/Paint;

    move-object v0, p0

    move-object v1, p1

    move v3, v10

    move v4, v11

    move v5, v12

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->drawSun(Landroid/graphics/Canvas;Landroid/graphics/Paint;FFF)V

    .line 520
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->drawDrop(Landroid/graphics/Canvas;)V

    .line 522
    neg-float v0, v7

    const/high16 v1, 0x40a00000    # 5.0f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    mul-float/2addr v1, v2

    add-float/2addr v1, v8

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 523
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPaint:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->drawTransparentArea(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V

    .line 525
    invoke-virtual {p1, v13}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 527
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPaint:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0, v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->drawCloud(Landroid/graphics/Canvas;Landroid/graphics/Paint;FF)V

    .line 528
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 529
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->cancelAnimation()V

    .line 530
    const-string v0, ""

    const-string v1, "cancelA"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 533
    .end local v6    # "saveFlag":I
    .end local v7    # "dx":F
    .end local v8    # "dy":F
    .end local v10    # "rotation":F
    .end local v11    # "scale1":F
    .end local v12    # "scale2":F
    .end local v13    # "transparentSaveLevel":I
    :catch_0
    move-exception v9

    .line 534
    .local v9, "e":Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0
.end method

.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 904
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 827
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onViewDetachedFromWindow : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 828
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 829
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 830
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 833
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunRotation:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_1

    .line 834
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunRotation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 835
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunRotation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 837
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunScale1:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_2

    .line 838
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunScale1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 839
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunScale1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 841
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunScale2:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_3

    .line 842
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunScale2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 843
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunScale2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 846
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_4

    .line 847
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 848
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 852
    :cond_4
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_5

    .line 853
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 854
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 859
    :cond_5
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropThunderT3:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_6

    .line 860
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropThunderT3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 861
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropThunderT3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 864
    :cond_6
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT4:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_7

    .line 865
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT4:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 866
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT4:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 871
    :cond_7
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT5:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_8

    .line 872
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT5:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 873
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT5:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 877
    :cond_8
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    .line 878
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunRotation:Landroid/animation/ValueAnimator;

    .line 879
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunScale1:Landroid/animation/ValueAnimator;

    .line 880
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunScale2:Landroid/animation/ValueAnimator;

    .line 881
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    .line 882
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    .line 883
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropThunderT3:Landroid/animation/ValueAnimator;

    .line 884
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT4:Landroid/animation/ValueAnimator;

    .line 885
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT5:Landroid/animation/ValueAnimator;

    .line 886
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPaint:Landroid/graphics/Paint;

    .line 887
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mMasking:Landroid/graphics/Path;

    .line 888
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mCloud:Landroid/graphics/Path;

    .line 889
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mSun:Landroid/graphics/Path;

    .line 890
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mSunOuter:Landroid/graphics/Path;

    .line 891
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine1:Landroid/graphics/Path;

    .line 892
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine2:Landroid/graphics/Path;

    .line 893
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mThunder:Landroid/graphics/Path;

    .line 894
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine4:Landroid/graphics/Path;

    .line 895
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mRainLine5:Landroid/graphics/Path;

    .line 896
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_9

    .line 897
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 899
    :cond_9
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mBitmap:Landroid/graphics/Bitmap;

    .line 900
    return-void
.end method

.method public setPaintColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 79
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPaintColor:I

    .line 80
    return-void
.end method

.method public setScale(F)V
    .locals 0
    .param p1, "scale"    # F

    .prologue
    .line 813
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    .line 814
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->init()V

    .line 815
    return-void
.end method

.method public setWidthAndHeight(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 822
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, p1, p2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 823
    .local v0, "l":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 825
    return-void
.end method

.method public startAnimation()V
    .locals 28

    .prologue
    .line 600
    const/16 v21, 0x0

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->isStop:Z

    .line 601
    const/16 v21, 0x0

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->rotateOffset:F

    .line 602
    const-string v21, ""

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPreFixT1:Ljava/lang/String;

    .line 603
    const-string v21, ""

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPreFixT2:Ljava/lang/String;

    .line 604
    const-string v21, ""

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPreFixT3:Ljava/lang/String;

    .line 605
    const-string v21, ""

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPreFixT4:Ljava/lang/String;

    .line 606
    const-string v21, ""

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mPreFixT5:Ljava/lang/String;

    .line 607
    const-wide/16 v12, 0x3e8

    .line 608
    .local v12, "duration":J
    const-wide/16 v21, 0x3

    div-long v10, v12, v21

    .line 610
    .local v10, "delayGap":J
    const/16 v21, 0x4

    move/from16 v0, v21

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "y"

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [F

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const/high16 v26, -0x3d900000    # -60.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    move/from16 v27, v0

    mul-float v26, v26, v27

    aput v26, v24, v25

    const/16 v25, 0x1

    const/high16 v26, 0x428c0000    # 70.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    move/from16 v27, v0

    mul-float v26, v26, v27

    aput v26, v24, v25

    .line 611
    invoke-static/range {v23 .. v24}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "x"

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [F

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const/high16 v26, 0x41a00000    # 20.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    move/from16 v27, v0

    mul-float v26, v26, v27

    aput v26, v24, v25

    const/16 v25, 0x1

    const/high16 v26, -0x3de00000    # -40.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    move/from16 v27, v0

    mul-float v26, v26, v27

    aput v26, v24, v25

    .line 612
    invoke-static/range {v23 .. v24}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "e_y"

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [F

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const/high16 v26, -0x3d900000    # -60.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    move/from16 v27, v0

    mul-float v26, v26, v27

    aput v26, v24, v25

    const/16 v25, 0x1

    const/16 v26, 0x0

    aput v26, v24, v25

    .line 613
    invoke-static/range {v23 .. v24}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x3

    const-string v23, "e_x"

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [F

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const/high16 v26, 0x41a00000    # 20.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    move/from16 v27, v0

    mul-float v26, v26, v27

    aput v26, v24, v25

    const/16 v25, 0x1

    const/16 v26, 0x0

    aput v26, v24, v25

    .line 614
    invoke-static/range {v23 .. v24}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v23

    aput-object v23, v21, v22

    .line 610
    invoke-static/range {v21 .. v21}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    .line 616
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    const/16 v22, -0x1

    invoke-virtual/range {v21 .. v22}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 617
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v12, v13}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 618
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    new-instance v22, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct/range {v22 .. v22}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual/range {v21 .. v22}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 619
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 620
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/animation/ValueAnimator;->start()V

    .line 623
    const/16 v21, 0x4

    move/from16 v0, v21

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "y"

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [F

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const/high16 v26, -0x3dcc0000    # -45.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    move/from16 v27, v0

    mul-float v26, v26, v27

    aput v26, v24, v25

    const/16 v25, 0x1

    const/high16 v26, 0x42960000    # 75.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    move/from16 v27, v0

    mul-float v26, v26, v27

    aput v26, v24, v25

    .line 624
    invoke-static/range {v23 .. v24}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "x"

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [F

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const/high16 v26, 0x41a00000    # 20.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    move/from16 v27, v0

    mul-float v26, v26, v27

    aput v26, v24, v25

    const/16 v25, 0x1

    const/high16 v26, -0x3e100000    # -30.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    move/from16 v27, v0

    mul-float v26, v26, v27

    aput v26, v24, v25

    .line 625
    invoke-static/range {v23 .. v24}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "e_y"

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [F

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const/high16 v26, -0x3dcc0000    # -45.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    move/from16 v27, v0

    mul-float v26, v26, v27

    aput v26, v24, v25

    const/16 v25, 0x1

    const/16 v26, 0x0

    aput v26, v24, v25

    .line 626
    invoke-static/range {v23 .. v24}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x3

    const-string v23, "e_x"

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [F

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const/high16 v26, 0x41a00000    # 20.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    move/from16 v27, v0

    mul-float v26, v26, v27

    aput v26, v24, v25

    const/16 v25, 0x1

    const/16 v26, 0x0

    aput v26, v24, v25

    .line 627
    invoke-static/range {v23 .. v24}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v23

    aput-object v23, v21, v22

    .line 623
    invoke-static/range {v21 .. v21}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    .line 629
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    const/16 v22, -0x1

    invoke-virtual/range {v21 .. v22}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 630
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v12, v13}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 631
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v10, v11}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 632
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    new-instance v22, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct/range {v22 .. v22}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual/range {v21 .. v22}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 633
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 634
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/animation/ValueAnimator;->start()V

    .line 636
    const/16 v21, 0x0

    const/high16 v22, -0x3db80000    # -50.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    move/from16 v23, v0

    mul-float v22, v22, v23

    invoke-static/range {v21 .. v22}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v14

    .line 637
    .local v14, "key1":Landroid/animation/Keyframe;
    const v21, 0x3dcccccd    # 0.1f

    const/16 v22, 0x0

    invoke-static/range {v21 .. v22}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v15

    .line 638
    .local v15, "key2":Landroid/animation/Keyframe;
    const v21, 0x3f19999a    # 0.6f

    const/16 v22, 0x0

    invoke-static/range {v21 .. v22}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v16

    .line 639
    .local v16, "key3":Landroid/animation/Keyframe;
    const v21, 0x3f666666    # 0.9f

    const/16 v22, 0x0

    invoke-static/range {v21 .. v22}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v17

    .line 640
    .local v17, "key4":Landroid/animation/Keyframe;
    const v21, 0x3f733333    # 0.95f

    const/16 v22, 0x0

    invoke-static/range {v21 .. v22}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v18

    .line 641
    .local v18, "key5":Landroid/animation/Keyframe;
    const/high16 v21, 0x3f800000    # 1.0f

    const/16 v22, 0x0

    invoke-static/range {v21 .. v22}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v19

    .line 643
    .local v19, "key6":Landroid/animation/Keyframe;
    const/16 v21, 0x0

    const/16 v22, 0xff

    invoke-static/range {v21 .. v22}, Landroid/animation/Keyframe;->ofInt(FI)Landroid/animation/Keyframe;

    move-result-object v2

    .line 644
    .local v2, "akey1":Landroid/animation/Keyframe;
    const v21, 0x3dcccccd    # 0.1f

    const/16 v22, 0xff

    invoke-static/range {v21 .. v22}, Landroid/animation/Keyframe;->ofInt(FI)Landroid/animation/Keyframe;

    move-result-object v3

    .line 645
    .local v3, "akey2":Landroid/animation/Keyframe;
    const v21, 0x3e4ccccd    # 0.2f

    const/16 v22, 0xff

    invoke-static/range {v21 .. v22}, Landroid/animation/Keyframe;->ofInt(FI)Landroid/animation/Keyframe;

    move-result-object v4

    .line 646
    .local v4, "akey3":Landroid/animation/Keyframe;
    const v21, 0x3e99999a    # 0.3f

    const/16 v22, 0x0

    invoke-static/range {v21 .. v22}, Landroid/animation/Keyframe;->ofInt(FI)Landroid/animation/Keyframe;

    move-result-object v5

    .line 647
    .local v5, "akey4":Landroid/animation/Keyframe;
    const v21, 0x3ecccccd    # 0.4f

    const/16 v22, 0xff

    invoke-static/range {v21 .. v22}, Landroid/animation/Keyframe;->ofInt(FI)Landroid/animation/Keyframe;

    move-result-object v6

    .line 648
    .local v6, "akey5":Landroid/animation/Keyframe;
    const/high16 v21, 0x3f000000    # 0.5f

    const/16 v22, 0x0

    invoke-static/range {v21 .. v22}, Landroid/animation/Keyframe;->ofInt(FI)Landroid/animation/Keyframe;

    move-result-object v7

    .line 649
    .local v7, "akey6":Landroid/animation/Keyframe;
    const v21, 0x3f19999a    # 0.6f

    const/16 v22, 0xff

    invoke-static/range {v21 .. v22}, Landroid/animation/Keyframe;->ofInt(FI)Landroid/animation/Keyframe;

    move-result-object v8

    .line 650
    .local v8, "akey7":Landroid/animation/Keyframe;
    const/high16 v21, 0x3f800000    # 1.0f

    const/16 v22, 0xff

    invoke-static/range {v21 .. v22}, Landroid/animation/Keyframe;->ofInt(FI)Landroid/animation/Keyframe;

    move-result-object v9

    .line 651
    .local v9, "akey8":Landroid/animation/Keyframe;
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "t"

    const/16 v24, 0x6

    move/from16 v0, v24

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object v14, v24, v25

    const/16 v25, 0x1

    aput-object v15, v24, v25

    const/16 v25, 0x2

    aput-object v16, v24, v25

    const/16 v25, 0x3

    aput-object v17, v24, v25

    const/16 v25, 0x4

    aput-object v18, v24, v25

    const/16 v25, 0x5

    aput-object v19, v24, v25

    .line 652
    invoke-static/range {v23 .. v24}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "a"

    const/16 v24, 0x8

    move/from16 v0, v24

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object v2, v24, v25

    const/16 v25, 0x1

    aput-object v3, v24, v25

    const/16 v25, 0x2

    aput-object v4, v24, v25

    const/16 v25, 0x3

    aput-object v5, v24, v25

    const/16 v25, 0x4

    aput-object v6, v24, v25

    const/16 v25, 0x5

    aput-object v7, v24, v25

    const/16 v25, 0x6

    aput-object v8, v24, v25

    const/16 v25, 0x7

    aput-object v9, v24, v25

    .line 654
    invoke-static/range {v23 .. v24}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "e_a"

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object v2, v24, v25

    const/16 v25, 0x1

    aput-object v3, v24, v25

    .line 655
    invoke-static/range {v23 .. v24}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v23

    aput-object v23, v21, v22

    .line 651
    invoke-static/range {v21 .. v21}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropThunderT3:Landroid/animation/ValueAnimator;

    .line 657
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropThunderT3:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    const/16 v22, -0x1

    invoke-virtual/range {v21 .. v22}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 658
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropThunderT3:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    const-wide/16 v22, 0x9c4

    invoke-virtual/range {v21 .. v23}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 659
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropThunderT3:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v10, v11}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 660
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropThunderT3:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    new-instance v22, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct/range {v22 .. v22}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual/range {v21 .. v22}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 661
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropThunderT3:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 662
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropThunderT3:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/animation/ValueAnimator;->start()V

    .line 664
    const/16 v21, 0x4

    move/from16 v0, v21

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "y"

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [F

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const/high16 v26, -0x3e380000    # -25.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    move/from16 v27, v0

    mul-float v26, v26, v27

    aput v26, v24, v25

    const/16 v25, 0x1

    const/high16 v26, 0x42c80000    # 100.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    move/from16 v27, v0

    mul-float v26, v26, v27

    aput v26, v24, v25

    .line 665
    invoke-static/range {v23 .. v24}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "x"

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [F

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const/high16 v26, 0x41200000    # 10.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    move/from16 v27, v0

    mul-float v26, v26, v27

    aput v26, v24, v25

    const/16 v25, 0x1

    const/high16 v26, -0x3de00000    # -40.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    move/from16 v27, v0

    mul-float v26, v26, v27

    aput v26, v24, v25

    .line 666
    invoke-static/range {v23 .. v24}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "e_y"

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [F

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const/high16 v26, -0x3e380000    # -25.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    move/from16 v27, v0

    mul-float v26, v26, v27

    aput v26, v24, v25

    const/16 v25, 0x1

    const/16 v26, 0x0

    aput v26, v24, v25

    .line 667
    invoke-static/range {v23 .. v24}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x3

    const-string v23, "e_x"

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [F

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const/high16 v26, 0x41200000    # 10.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    move/from16 v27, v0

    mul-float v26, v26, v27

    aput v26, v24, v25

    const/16 v25, 0x1

    const/16 v26, 0x0

    aput v26, v24, v25

    .line 668
    invoke-static/range {v23 .. v24}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v23

    aput-object v23, v21, v22

    .line 664
    invoke-static/range {v21 .. v21}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT4:Landroid/animation/ValueAnimator;

    .line 670
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT4:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    const/16 v22, -0x1

    invoke-virtual/range {v21 .. v22}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 671
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT4:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v12, v13}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 672
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT4:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    const-wide/16 v22, 0x3

    mul-long v22, v22, v10

    invoke-virtual/range {v21 .. v23}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 673
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT4:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    new-instance v22, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct/range {v22 .. v22}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual/range {v21 .. v22}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 674
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT4:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 675
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT4:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/animation/ValueAnimator;->start()V

    .line 677
    const/16 v21, 0x4

    move/from16 v0, v21

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "y"

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [F

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const/high16 v26, -0x3db80000    # -50.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    move/from16 v27, v0

    mul-float v26, v26, v27

    aput v26, v24, v25

    const/16 v25, 0x1

    const/high16 v26, 0x42a00000    # 80.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    move/from16 v27, v0

    mul-float v26, v26, v27

    aput v26, v24, v25

    .line 678
    invoke-static/range {v23 .. v24}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "x"

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [F

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const/high16 v26, 0x41200000    # 10.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    move/from16 v27, v0

    mul-float v26, v26, v27

    aput v26, v24, v25

    const/16 v25, 0x1

    const/high16 v26, -0x3de00000    # -40.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    move/from16 v27, v0

    mul-float v26, v26, v27

    aput v26, v24, v25

    .line 679
    invoke-static/range {v23 .. v24}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "e_y"

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [F

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const/high16 v26, -0x3db80000    # -50.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    move/from16 v27, v0

    mul-float v26, v26, v27

    aput v26, v24, v25

    const/16 v25, 0x1

    const/16 v26, 0x0

    aput v26, v24, v25

    .line 680
    invoke-static/range {v23 .. v24}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x3

    const-string v23, "e_x"

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [F

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const/high16 v26, 0x41200000    # 10.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    move/from16 v27, v0

    mul-float v26, v26, v27

    aput v26, v24, v25

    const/16 v25, 0x1

    const/16 v26, 0x0

    aput v26, v24, v25

    .line 681
    invoke-static/range {v23 .. v24}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v23

    aput-object v23, v21, v22

    .line 677
    invoke-static/range {v21 .. v21}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT5:Landroid/animation/ValueAnimator;

    .line 683
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT5:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    const/16 v22, -0x1

    invoke-virtual/range {v21 .. v22}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 684
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT5:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v12, v13}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 685
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT5:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    const-wide/16 v22, 0x5

    mul-long v22, v22, v10

    invoke-virtual/range {v21 .. v23}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 686
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT5:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 687
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT5:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    new-instance v22, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct/range {v22 .. v22}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual/range {v21 .. v22}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 688
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->DropCenterT5:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/animation/ValueAnimator;->start()V

    .line 691
    const/16 v21, 0x2

    move/from16 v0, v21

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "y"

    const/16 v24, 0x3

    move/from16 v0, v24

    new-array v0, v0, [F

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const/16 v26, 0x0

    aput v26, v24, v25

    const/16 v25, 0x1

    const v26, -0x3fd33333    # -2.7f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    move/from16 v27, v0

    mul-float v26, v26, v27

    aput v26, v24, v25

    const/16 v25, 0x2

    const/16 v26, 0x0

    aput v26, v24, v25

    .line 692
    invoke-static/range {v23 .. v24}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "x"

    const/16 v24, 0x5

    move/from16 v0, v24

    new-array v0, v0, [F

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const/16 v26, 0x0

    aput v26, v24, v25

    const/16 v25, 0x1

    const v26, -0x3f933333    # -3.7f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    move/from16 v27, v0

    mul-float v26, v26, v27

    aput v26, v24, v25

    const/16 v25, 0x2

    const/16 v26, 0x0

    aput v26, v24, v25

    const/16 v25, 0x3

    const/high16 v26, 0x40c00000    # 6.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mScale:F

    move/from16 v27, v0

    mul-float v26, v26, v27

    aput v26, v24, v25

    const/16 v25, 0x4

    const/16 v26, 0x0

    aput v26, v24, v25

    .line 693
    invoke-static/range {v23 .. v24}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v23

    aput-object v23, v21, v22

    .line 691
    invoke-static/range {v21 .. v21}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    .line 695
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    const/16 v22, -0x1

    invoke-virtual/range {v21 .. v22}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 696
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    const-wide/16 v22, 0x7d0

    invoke-virtual/range {v21 .. v23}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 697
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 698
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/animation/ValueAnimator;->start()V

    .line 700
    const/16 v21, 0x2

    move/from16 v0, v21

    new-array v0, v0, [F

    move-object/from16 v21, v0

    fill-array-data v21, :array_0

    invoke-static/range {v21 .. v21}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunRotation:Landroid/animation/ValueAnimator;

    .line 701
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunRotation:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    const/16 v22, -0x1

    invoke-virtual/range {v21 .. v22}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 702
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunRotation:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    const-wide/16 v22, 0x7d0

    invoke-virtual/range {v21 .. v23}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 703
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunRotation:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 704
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunRotation:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    new-instance v22, Landroid/view/animation/LinearInterpolator;

    invoke-direct/range {v22 .. v22}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual/range {v21 .. v22}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 705
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunRotation:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/animation/ValueAnimator;->start()V

    .line 707
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [F

    move-object/from16 v21, v0

    fill-array-data v21, :array_1

    invoke-static/range {v21 .. v21}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunScale1:Landroid/animation/ValueAnimator;

    .line 708
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunScale1:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    const/16 v22, -0x1

    invoke-virtual/range {v21 .. v22}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 709
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunScale1:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    const-wide/16 v22, 0x514

    invoke-virtual/range {v21 .. v23}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 710
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunScale1:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 711
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunScale1:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    new-instance v22, Landroid/view/animation/LinearInterpolator;

    invoke-direct/range {v22 .. v22}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual/range {v21 .. v22}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 712
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunScale1:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/animation/ValueAnimator;->start()V

    .line 714
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [F

    move-object/from16 v21, v0

    fill-array-data v21, :array_2

    invoke-static/range {v21 .. v21}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunScale2:Landroid/animation/ValueAnimator;

    .line 715
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunScale2:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    const/16 v22, -0x1

    invoke-virtual/range {v21 .. v22}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 716
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunScale2:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    const-wide/16 v22, 0x514

    invoke-virtual/range {v21 .. v23}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 717
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunScale2:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    const-wide/16 v22, 0x190

    invoke-virtual/range {v21 .. v23}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 718
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunScale2:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    new-instance v22, Landroid/view/animation/LinearInterpolator;

    invoke-direct/range {v22 .. v22}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual/range {v21 .. v22}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 719
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunScale2:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 720
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->SunScale2:Landroid/animation/ValueAnimator;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/animation/ValueAnimator;->start()V

    .line 722
    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->mIsActiveAnimationThread:Z

    .line 723
    new-instance v20, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView$2;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;)V

    .line 736
    .local v20, "t":Ljava/lang/Thread;
    const/16 v21, 0xa

    invoke-virtual/range {v20 .. v21}, Ljava/lang/Thread;->setPriority(I)V

    .line 737
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Thread;->start()V

    .line 738
    return-void

    .line 700
    nop

    :array_0
    .array-data 4
        0x0
        0x42340000    # 45.0f
    .end array-data

    .line 707
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x3f8ccccd    # 1.1f
        0x3f800000    # 1.0f
    .end array-data

    .line 714
    :array_2
    .array-data 4
        0x3f800000    # 1.0f
        0x3f8ccccd    # 1.1f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public stopAnimation()V
    .locals 1

    .prologue
    .line 785
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyTSIconView;->isStop:Z

    .line 786
    return-void
.end method
