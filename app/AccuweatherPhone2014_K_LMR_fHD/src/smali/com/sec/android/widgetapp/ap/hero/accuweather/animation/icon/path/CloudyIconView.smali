.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;
.super Landroid/view/View;
.source "CloudyIconView.java"

# interfaces
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;


# instance fields
.field private TranslateAni:Landroid/animation/ValueAnimator;

.field private TranslateAni2:Landroid/animation/ValueAnimator;

.field cloudyX:F

.field isStop:Z

.field l:Landroid/animation/Animator$AnimatorListener;

.field private mCloudPaint1:Landroid/graphics/Paint;

.field private mCloudPath_left:Landroid/graphics/Path;

.field private mCloudPath_right:Landroid/graphics/Path;

.field private mCloudPath_top:Landroid/graphics/Path;

.field private mIsActiveAnimationThread:Z

.field private mPaintColor:I

.field private mScaleFactor:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attributeset"    # Landroid/util/AttributeSet;

    .prologue
    .line 27
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attributeset"    # Landroid/util/AttributeSet;
    .param p3, "i"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 31
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    .line 45
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mIsActiveAnimationThread:Z

    .line 47
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    .line 49
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    .line 51
    const v0, -0xa0a0b

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mPaintColor:I

    .line 197
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->isStop:Z

    .line 202
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->l:Landroid/animation/Animator$AnimatorListener;

    .line 256
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->cloudyX:F

    .line 32
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->init()V

    .line 33
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;

    .prologue
    .line 21
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mIsActiveAnimationThread:Z

    return v0
.end method

.method private init()V
    .locals 13

    .prologue
    const v12, 0x41cab021    # 25.336f

    const v11, 0x418ab439    # 17.338f

    const v10, 0x42b3d47b    # 89.915f

    const v9, 0x43235db2

    const v8, 0x431b66a8    # 155.401f

    .line 58
    invoke-virtual {p0, p0}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 59
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->initPaint()V

    .line 61
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_left:Landroid/graphics/Path;

    .line 62
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_left:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 63
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x421f9fbe

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 64
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x41fee148    # 31.86f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v8

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v3, v12

    const v4, 0x4314b4bc

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v5, v12

    const v6, 0x430cb1ec

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 67
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_left:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v12

    const v2, 0x4304af9e

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x41fee148    # 31.86f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42fc2e98

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x421f9fbe

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42fc2e98

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 70
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x42216148

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42fc2e98

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x42231db2    # 40.779f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42fc22d1    # 126.068f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x4224d604    # 41.209f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42fc36c9

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 73
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x4223872b    # 40.882f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42f95d2f    # 124.682f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x4222d1ec

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42f65b23

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x4222d1ec

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42f34e56    # 121.653f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 76
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x4222d1ec

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42dd5b23

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x42469168

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42cb872b    # 101.764f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x4272ae14    # 60.67f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42cb872b    # 101.764f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 79
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x4272e354    # 60.722f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42cb872b    # 101.764f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x4282e148    # 65.44f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42cca2d1    # 102.318f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x4282e148    # 65.44f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42cca2d1    # 102.318f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 82
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x4286ad0e

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42bc7b64

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 83
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x42880ccd

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42bc73b6

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x4275ced9    # 61.452f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42bb947b    # 93.79f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x4272af1b

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42bb947b    # 93.79f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 86
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x4238570a    # 46.085f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42bb947b    # 93.79f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x42084ac1

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42d1ec8b

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x42034396

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42ee53f8

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 89
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x41beae14    # 23.835f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42f44189

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v3, v11

    const v4, 0x4302c51f    # 130.77f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v5, v11

    const v6, 0x430cb1aa    # 140.694f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 92
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_left:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v11

    const v2, 0x431915c3

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x41dbb22d    # 27.462f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v4, v9

    const v5, 0x421fa1cb    # 39.908f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v6, v9

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 95
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x431c374c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v9

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 96
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x431c374c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x431b6666    # 155.4f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 97
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x421f9fbe

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x431b6666    # 155.4f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 99
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_right:Landroid/graphics/Path;

    .line 100
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_right:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 101
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x43267ba6

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v10

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 102
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x43263687

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v10

    const v3, 0x4321d5c3

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42b3ed0e

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x431d3aa0

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42b65f3b

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 105
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x43140b02    # 148.043f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42bb3f7d    # 93.624f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x430bfc6a

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42c6a9fc    # 99.332f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x4306bae1    # 134.73f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42d727f0

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 108
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x43058b44

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42dadeb8

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x43061852

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42dfcb44

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x4307f5c3    # 135.96f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42e22666

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 111
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x4308a083

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42e2fdf4

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x43095df4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42e364dd

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x430a19db

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42e364dd

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 114
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x430b6ccd

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42e364dd

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x430cb810

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42e21581    # 113.042f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x430d7be7

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42dfb1aa    # 111.847f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 117
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x43130c08

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42ce3c6a

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x431b5efa

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42c3c189

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x43273e35

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42c3c189

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 120
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x4336cfdf

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42c465e3

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x4343553f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42de1aa0

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x4343553f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42fd428f    # 126.63f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 123
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x4343553f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x430e67f0

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x43367cee

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v4, v8

    const v5, 0x4326a354    # 166.638f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v6, v8

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x42775a1d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 127
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x42775a1d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x43235df4

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 128
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x4326a354    # 166.638f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x43235df4

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 129
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x4326a354    # 166.638f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v9

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 130
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x433adf3b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v9

    const v3, 0x434b54bc

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x4312c6a8    # 146.776f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x434b54bc

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42fd428f    # 126.63f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 133
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x434b54bc

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42d4f7cf

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x433ab78d

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v4, v10

    const v5, 0x43267ba6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v6, v10

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 137
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_top:Landroid/graphics/Path;

    .line 139
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_top:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 142
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x42733646

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42cbbbe7

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 143
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x426f5a1d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42d48312

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x426d52f2

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42dd9f3b

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x426d52f2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42e6f0a4    # 115.47f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 146
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x426d52f2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42eb570a    # 117.67f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x42747df4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42eee666    # 119.45f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x427d52f2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42eee666    # 119.45f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 149
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x4283147b    # 65.54f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42eee666    # 119.45f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x4286a979

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42eb54fe    # 117.666f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x4286a979

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42e6ee98

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 152
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x4286a979

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42d270a4    # 105.22f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x428c0f5c    # 70.03f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42c00d50    # 96.026f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x42957a5e

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42b0d99a

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 155
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x42a15d2f    # 80.682f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x429e4ed9    # 79.154f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x42b3820c

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42901a1d

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x42c8f021

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x4289322d    # 68.598f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 158
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x42d18831

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x4286851f    # 67.26f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x42dbb958    # 109.862f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42848d50    # 66.276f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x42e6a2d1    # 115.318f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42848d50    # 66.276f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 161
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x4304bf3b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42848d50    # 66.276f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x431549ba

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x4298a76d

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x431d72f2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42b64000    # 91.125f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 164
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x431d6000    # 157.375f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42b649ba

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x431efd2f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42bd9fbe

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x431eea3d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42bda979

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 167
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x43277e35

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42bb6d0e

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 168
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x4325c042

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42b3b852    # 89.86f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 169
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x431c79db

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x428da45a    # 70.821f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x4308f687

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x426973b6

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x42e6a2d1    # 115.318f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x426973b6

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 172
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x42e11810

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x426973b6

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x42db92f2

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x426a3a5e    # 58.557f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x42d61c29    # 107.055f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x426bdb23    # 58.964f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 175
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x42bcfbe7

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42729eb8

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x42a67333

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x4283e042

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x42954419

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x4296570a    # 75.17f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 178
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x428d2d0e

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x429f051f    # 79.51f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x42869b23

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42a8ee98

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x4281a24e

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42b3c28f    # 89.88f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 181
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x42805b23

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42b663d7    # 91.195f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x427c0419

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42bbc083    # 93.876f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const/high16 v5, 0x427c0000    # 63.0f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42bbc6a8    # 93.888f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 184
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x4277ee98

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42c1f6c9

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x42733646

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42cbbbe7

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x42733646

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42cbbbe7

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 187
    return-void
.end method

.method private initPaint()V
    .locals 3

    .prologue
    .line 190
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPaint1:Landroid/graphics/Paint;

    .line 191
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPaint1:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 192
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPaint1:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 193
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPaint1:Landroid/graphics/Paint;

    const/high16 v1, 0x41400000    # 12.0f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 194
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPaint1:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mPaintColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 195
    return-void
.end method


# virtual methods
.method public cancelAnimation()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 293
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mIsActiveAnimationThread:Z

    .line 295
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->cloudyX:F

    .line 298
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 301
    :cond_0
    return-void
.end method

.method public getIsPalyAnimation()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 249
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    if-nez v0, :cond_0

    .line 250
    const/4 v0, 0x0

    .line 252
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    goto :goto_0
.end method

.method public getScale()F
    .locals 1

    .prologue
    .line 313
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 289
    return-object p0
.end method

.method public isRunning()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 304
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->getIsPalyAnimation()Z

    move-result v0

    return v0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v2, 0x0

    .line 259
    :try_start_0
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_1

    .line 260
    const-string v1, ""

    const-string v2, "scale is less then 0"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    :cond_0
    :goto_0
    return-void

    .line 264
    :cond_1
    iget-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mIsActiveAnimationThread:Z

    if-eqz v1, :cond_2

    .line 266
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->cloudyX:F

    .line 269
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 270
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->cloudyX:F

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 272
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_left:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPaint1:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 273
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_top:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPaint1:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 274
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_right:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPaint1:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 277
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 280
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->getIsPalyAnimation()Z

    move-result v1

    if-nez v1, :cond_0

    .line 281
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->cancelAnimation()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 283
    :catch_0
    move-exception v0

    .line 284
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 341
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 322
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onViewDetachedFromWindow : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPaint1:Landroid/graphics/Paint;

    .line 324
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_left:Landroid/graphics/Path;

    .line 325
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_right:Landroid/graphics/Path;

    .line 326
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mCloudPath_top:Landroid/graphics/Path;

    .line 329
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 330
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 331
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 336
    :cond_0
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    .line 337
    return-void
.end method

.method public setPaintColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 54
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mPaintColor:I

    .line 55
    return-void
.end method

.method public setScale(F)V
    .locals 0
    .param p1, "scale"    # F

    .prologue
    .line 308
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    .line 309
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->init()V

    .line 310
    return-void
.end method

.method public setWidthAndHeight(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 317
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, p1, p2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 318
    .local v0, "l":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 319
    return-void
.end method

.method public startAnimation()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 220
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->isStop:Z

    .line 223
    const/4 v1, 0x5

    new-array v1, v1, [F

    aput v5, v1, v2

    const/high16 v2, -0x3ef80000    # -8.5f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    aput v2, v1, v6

    const/4 v2, 0x2

    aput v5, v1, v2

    const/4 v2, 0x3

    const/high16 v3, 0x41080000    # 8.5f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    aput v3, v1, v2

    const/4 v2, 0x4

    aput v5, v1, v2

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    .line 224
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 225
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 226
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    .line 227
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 229
    iput-boolean v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->mIsActiveAnimationThread:Z

    .line 230
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;)V

    .line 244
    .local v0, "t":Ljava/lang/Thread;
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 245
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 246
    return-void
.end method

.method public stopAnimation()V
    .locals 1

    .prologue
    .line 199
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->isStop:Z

    .line 200
    return-void
.end method
