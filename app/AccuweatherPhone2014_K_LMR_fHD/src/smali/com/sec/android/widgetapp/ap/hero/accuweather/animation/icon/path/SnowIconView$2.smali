.class Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView$2;
.super Ljava/lang/Object;
.source "SnowIconView.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;

    .prologue
    .line 687
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 710
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->isStart2ndAni:Z
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->access$202(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;Z)Z

    .line 711
    invoke-virtual {p1}, Landroid/animation/Animator;->removeAllListeners()V

    .line 712
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 705
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->isStart2ndAni:Z
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->access$202(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;Z)Z

    .line 706
    invoke-virtual {p1}, Landroid/animation/Animator;->removeAllListeners()V

    .line 707
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 3
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    const/16 v2, 0x25

    .line 693
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mAnimators:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;)Ljava/util/HashMap;

    move-result-object v0

    if-nez v0, :cond_1

    .line 702
    :cond_0
    :goto_0
    return-void

    .line 696
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->isStop:Z

    if-eqz v0, :cond_0

    .line 697
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mAnimators:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 698
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mAnimators:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    .line 699
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->isStart2ndAni:Z
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->access$202(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;Z)Z

    goto :goto_0
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 689
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->isStart2ndAni:Z
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->access$202(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;Z)Z

    .line 690
    return-void
.end method
