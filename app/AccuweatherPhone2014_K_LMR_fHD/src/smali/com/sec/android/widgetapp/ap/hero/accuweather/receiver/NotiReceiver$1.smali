.class Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$1;
.super Landroid/os/Handler;
.source "NotiReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;

    .prologue
    .line 825
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 827
    iget v8, p1, Landroid/os/Message;->what:I

    const/16 v9, 0x3f2

    if-ne v8, v9, :cond_1

    .line 828
    const-string v8, ""

    const-string v9, "NotiReceiver GET_DATA_NETWORK_ERROR"

    invoke-static {v8, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 877
    :cond_0
    :goto_0
    return-void

    .line 830
    :cond_1
    iget v8, p1, Landroid/os/Message;->what:I

    const/16 v9, 0xc8

    if-ne v8, v9, :cond_7

    .line 831
    const-string v8, ""

    const-string v9, "NotiReceiver GET_CURRENT_LOCATION_OK"

    invoke-static {v8, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 832
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;)Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getCheckCurrentCityLocation(Landroid/content/Context;)I

    move-result v8

    if-eqz v8, :cond_0

    .line 834
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "cityinfo"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    .line 836
    .local v2, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    invoke-direct {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;-><init>()V

    .line 837
    .local v0, "dinfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "today"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v8

    check-cast v8, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    invoke-virtual {v0, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->setTodayWeatherInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;)V

    .line 840
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    const/4 v8, 0x7

    if-ge v1, v8, :cond_3

    .line 841
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v8

    if-eqz v8, :cond_2

    .line 842
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    .line 843
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v8

    check-cast v8, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    .line 842
    invoke-virtual {v0, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addForecastInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;)V

    .line 840
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 847
    :cond_3
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "detailinfo"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v8

    check-cast v8, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    .line 848
    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getHourSize()I

    move-result v4

    .line 849
    .local v4, "num":I
    if-eqz v4, :cond_4

    .line 850
    const/4 v7, 0x0

    .local v7, "z":I
    :goto_2
    if-ge v7, v4, :cond_4

    .line 851
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "detailinfo"

    .line 852
    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v8

    check-cast v8, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    invoke-virtual {v8, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getHourInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;

    move-result-object v8

    .line 851
    invoke-virtual {v0, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addHourInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;)V

    .line 850
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 856
    .end local v7    # "z":I
    :cond_4
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "cityxmlinfo"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 858
    .local v6, "xmlInfo":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "detailinfo"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v8

    check-cast v8, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    .line 859
    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfoSize()I

    move-result v5

    .line 860
    .local v5, "size":I
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_3
    if-ge v3, v5, :cond_6

    .line 861
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v8

    if-eqz v8, :cond_5

    .line 862
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "detailinfo"

    .line 863
    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v8

    check-cast v8, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    invoke-virtual {v8, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;

    move-result-object v8

    .line 862
    invoke-virtual {v0, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addPhotosInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;)V

    .line 860
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 868
    :cond_6
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->saveData(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)V
    invoke-static {v8, v2, v0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)V

    .line 870
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->displayNotification()V

    goto/16 :goto_0

    .line 873
    .end local v0    # "dinfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .end local v1    # "i":I
    .end local v2    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .end local v3    # "j":I
    .end local v4    # "num":I
    .end local v5    # "size":I
    .end local v6    # "xmlInfo":Ljava/lang/String;
    :cond_7
    iget v8, p1, Landroid/os/Message;->what:I

    const/16 v9, 0xc9

    if-ne v8, v9, :cond_0

    .line 874
    const-string v8, ""

    const-string v9, "NotiReceiver GET_CURRENT_LOCATION_PROVIDER_ERR"

    invoke-static {v8, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
