.class Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl$1;
.super Ljava/lang/Object;
.source "MenuSettingsBGTransparencyControl.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->mTransparencyProgressVal:I
    invoke-static {v0, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->access$002(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;I)I

    .line 77
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->mBGImageView:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-static {p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->calAlphaVal(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageAlpha(I)V

    .line 78
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "arg0"    # Landroid/widget/SeekBar;

    .prologue
    .line 82
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "arg0"    # Landroid/widget/SeekBar;

    .prologue
    .line 86
    return-void
.end method
