.class Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;
.super Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;
.source "MenuAdd.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->performGetData(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

.field final synthetic val$cityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

.field final synthetic val$location:Ljava/lang/String;

.field final synthetic val$tempScaleSetting:I


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;ILcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 1695
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    iput p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->val$tempScaleSetting:I

    iput-object p3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->val$cityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    iput-object p4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->val$location:Ljava/lang/String;

    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(IILjava/lang/String;Ljava/lang/String;J)V
    .locals 21
    .param p1, "position"    # I
    .param p2, "responseCode"    # I
    .param p3, "responseStatus"    # Ljava/lang/String;
    .param p4, "responseBody"    # Ljava/lang/String;
    .param p5, "stamp"    # J

    .prologue
    .line 1698
    invoke-super/range {p0 .. p6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;->onReceive(IILjava/lang/String;Ljava/lang/String;J)V

    .line 1701
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mRequestCanceled:Z
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Z

    move-result v18

    if-nez v18, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-wide v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->syncstamp:J

    move-wide/from16 v18, v0

    cmp-long v18, v18, p5

    if-eqz v18, :cond_1

    .line 1934
    :cond_0
    :goto_0
    return-void

    .line 1704
    :cond_1
    const/16 v18, 0xc8

    move/from16 v0, p2

    move/from16 v1, v18

    if-ne v0, v1, :cond_14

    .line 1705
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getTimestamp()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    .line 1706
    .local v17, "updateDate":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->parser:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$3400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v19, v0

    .line 1707
    move-object/from16 v0, v18

    move-object/from16 v1, p4

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->parseDetailWeatherLocationCity(Ljava/lang/String;Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    move-result-object v10

    .line 1708
    .local v10, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->parser:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$3400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->val$tempScaleSetting:I

    move/from16 v19, v0

    move-object/from16 v0, v18

    move/from16 v1, v19

    move-object/from16 v2, p4

    move-object/from16 v3, v17

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->parseDetailWeather(ILjava/lang/String;Ljava/lang/String;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    move-result-object v8

    .line 1711
    .local v8, "detailInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    if-eqz v10, :cond_2

    .line 1712
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->val$cityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    move-object/from16 v18, v0

    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLatitude()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setLatitude(Ljava/lang/String;)V

    .line 1713
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->val$cityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    move-object/from16 v18, v0

    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLongitude()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setLongitude(Ljava/lang/String;)V

    .line 1714
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->val$cityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    move-object/from16 v18, v0

    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getState()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setState(Ljava/lang/String;)V

    .line 1717
    :cond_2
    if-eqz v8, :cond_13

    .line 1718
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mRequestCanceled:Z
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Z

    move-result v18

    if-nez v18, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-wide v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->syncstamp:J

    move-wide/from16 v18, v0

    cmp-long v18, v18, p5

    if-nez v18, :cond_0

    .line 1721
    const/4 v13, 0x1

    .line 1723
    .local v13, "result":I
    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v16

    .line 1724
    .local v16, "todayInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->val$tempScaleSetting:I

    move/from16 v18, v0

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setTempScale(I)V

    .line 1726
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getApplicationContext()Landroid/content/Context;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getAllCityListCount(Landroid/content/Context;)I

    move-result v4

    .line 1727
    .local v4, "CityListCount":I
    const-string v18, ""

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "MA@CityListCount : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1729
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->val$location:Ljava/lang/String;

    .line 1730
    .local v15, "tempLocation":Ljava/lang/String;
    const/4 v11, 0x0

    .line 1731
    .local v11, "isResultByGPS":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    const v19, 0x7f080112

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 1733
    .local v5, "addRowState":Landroid/widget/TextView;
    if-eqz v5, :cond_4

    .line 1735
    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v18

    .line 1736
    invoke-interface/range {v18 .. v18}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v19, v0

    .line 1737
    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0d0033

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 1739
    const/4 v11, 0x1

    .line 1742
    :cond_3
    if-eqz v11, :cond_4

    .line 1745
    const-string v15, "cityId:current"

    .line 1746
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->val$cityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->val$cityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setRealLocation(Ljava/lang/String;)V

    .line 1747
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->val$cityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setLocation(Ljava/lang/String;)V

    .line 1751
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/content/Context;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->isMaxCityListAdded(Landroid/content/Context;)Z

    move-result v18

    if-eqz v18, :cond_5

    if-nez v11, :cond_5

    .line 1752
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getDataHandler:Landroid/os/Handler;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$4100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/os/Handler;

    move-result-object v18

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34$1;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;)V

    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 1760
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->val$cityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    move-object/from16 v18, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSummerTime()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setSummerTime(Ljava/lang/String;)V

    .line 1762
    const-string v12, ""

    .line 1763
    .local v12, "loc":Ljava/lang/String;
    if-eqz v11, :cond_7

    .line 1764
    const-string v12, "cityId:current"

    .line 1768
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/content/Context;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v0, v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->isRegisteredToDetailHourInfo(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_8

    .line 1769
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/content/Context;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v0, v12, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateDetailHourInfo(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)I

    .line 1774
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/content/Context;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v0, v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->isRegisteredToCityList(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_9

    .line 1775
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/content/Context;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->val$cityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, p4

    invoke-static {v0, v1, v8, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->insertCity(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)I

    move-result v13

    .line 1778
    if-eqz v11, :cond_6

    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v13, v0, :cond_6

    .line 1779
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/content/Context;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getCheckCurrentCityLocation(Landroid/content/Context;)I

    move-result v18

    if-nez v18, :cond_6

    .line 1780
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/content/Context;

    move-result-object v18

    const/16 v19, 0x1

    invoke-static/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateCheckCurrentCityLocation(Landroid/content/Context;I)I

    .line 1784
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->checkResultCode(I)I
    invoke-static {v0, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$4200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;I)I

    move-result v14

    .line 1787
    .local v14, "resultCode":I
    const/16 v18, 0x3e7

    move/from16 v0, v18

    if-ne v0, v14, :cond_e

    .line 1788
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getDataHandler:Landroid/os/Handler;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$4100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/os/Handler;

    move-result-object v18

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34$2;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;)V

    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 1766
    .end local v14    # "resultCode":I
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->val$cityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_1

    .line 1771
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/content/Context;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v0, v12, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->insertDetailHourInfo(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)I

    goto/16 :goto_2

    .line 1796
    :cond_9
    if-eqz v11, :cond_e

    .line 1797
    sget-boolean v18, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isCurCityAtTopInCityList:Z

    if-nez v18, :cond_a

    sget-boolean v18, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isCurCityAtTopInCityList:Z

    if-nez v18, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    .line 1799
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/content/Context;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getCheckCurrentCityLocation(Landroid/content/Context;)I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_d

    .line 1800
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/content/Context;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->val$cityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, p4

    invoke-static {v0, v15, v1, v8, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateCity(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)I

    move-result v13

    .line 1803
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/content/Context;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getCheckCurrentCityLocation(Landroid/content/Context;)I

    move-result v18

    if-nez v18, :cond_b

    .line 1804
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/content/Context;

    move-result-object v18

    const/16 v19, 0x1

    invoke-static/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateCheckCurrentCityLocation(Landroid/content/Context;I)I

    .line 1823
    :cond_b
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->checkResultCode(I)I
    invoke-static {v0, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$4200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;I)I

    move-result v14

    .line 1824
    .restart local v14    # "resultCode":I
    if-nez v14, :cond_c

    .line 1826
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/content/Context;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/BManager;->isCheckSupportClock(Landroid/content/Context;)Z

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_c

    .line 1827
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/content/Context;

    move-result-object v18

    const/16 v19, 0x1

    const-string v20, "PGD"

    invoke-static/range {v18 .. v20}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/BManager;->sendBroadCastToProvider(Landroid/content/Context;ZLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1833
    :cond_c
    :goto_4
    const/16 v18, 0x3e7

    move/from16 v0, v18

    if-ne v0, v14, :cond_e

    .line 1834
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getDataHandler:Landroid/os/Handler;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$4100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/os/Handler;

    move-result-object v18

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34$3;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34$3;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;)V

    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 1807
    .end local v14    # "resultCode":I
    :cond_d
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1808
    .local v7, "deleteList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v7, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1809
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/content/Context;

    move-result-object v18

    const/16 v19, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-static {v0, v7, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->deleteCitys(Landroid/content/Context;Ljava/util/ArrayList;Z)I

    move-result v13

    .line 1811
    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v13, v0, :cond_b

    .line 1812
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/content/Context;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->deletePhotosInfo(Landroid/content/Context;Ljava/util/ArrayList;)I

    .line 1814
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/content/Context;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->val$cityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, p4

    invoke-static {v0, v1, v8, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->insertCity(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)I

    move-result v13

    .line 1817
    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v13, v0, :cond_b

    .line 1818
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/content/Context;

    move-result-object v18

    const/16 v19, 0x1

    invoke-static/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateCheckCurrentCityLocation(Landroid/content/Context;I)I

    goto/16 :goto_3

    .line 1829
    .end local v7    # "deleteList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v14    # "resultCode":I
    :catch_0
    move-exception v9

    .line 1830
    .local v9, "e":Ljava/lang/Exception;
    const-string v18, ""

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "db : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v9}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 1843
    .end local v9    # "e":Ljava/lang/Exception;
    .end local v14    # "resultCode":I
    :cond_e
    if-nez v4, :cond_f

    .line 1844
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/content/Context;

    move-result-object v18

    const/16 v19, 0x1

    const/16 v20, 0x1

    invoke-static/range {v18 .. v20}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;->setWidgetAtDaemon(Landroid/content/Context;ZZ)V

    .line 1846
    :cond_f
    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v18

    if-eqz v18, :cond_11

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfoSize()I

    move-result v18

    if-lez v18, :cond_11

    .line 1847
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v8, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;->getCityId()Ljava/lang/String;

    move-result-object v6

    .line 1848
    .local v6, "cityId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/content/Context;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getExistPhotos(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_10

    .line 1849
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/content/Context;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->deletePhotosInfo(Landroid/content/Context;Ljava/lang/String;)I

    .line 1851
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/content/Context;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v0, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->insertPhotosInfo(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)I

    .line 1855
    .end local v6    # "cityId":Ljava/lang/String;
    :cond_11
    const-string v18, ""

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "LC : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->val$location:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " -> "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1856
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getApplicationContext()Landroid/content/Context;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v0, v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateLastSelectedLocation(Landroid/content/Context;Ljava/lang/String;)I

    .line 1859
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getDataHandler:Landroid/os/Handler;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$4100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/os/Handler;

    move-result-object v18

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34$4;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34$4;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;)V

    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1933
    .end local v4    # "CityListCount":I
    .end local v5    # "addRowState":Landroid/widget/TextView;
    .end local v8    # "detailInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .end local v10    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .end local v11    # "isResultByGPS":Z
    .end local v12    # "loc":Ljava/lang/String;
    .end local v13    # "result":I
    .end local v15    # "tempLocation":Ljava/lang/String;
    .end local v16    # "todayInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    .end local v17    # "updateDate":Ljava/lang/String;
    :cond_12
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->deleteMe(I)V

    goto/16 :goto_0

    .line 1907
    .restart local v8    # "detailInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .restart local v10    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .restart local v17    # "updateDate":Ljava/lang/String;
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->isActivityVisible()Z

    move-result v18

    if-eqz v18, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mRequestCanceled:Z
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Z

    move-result v18

    if-nez v18, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-wide v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->syncstamp:J

    move-wide/from16 v18, v0

    cmp-long v18, v18, p5

    if-nez v18, :cond_12

    .line 1909
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getDataHandler:Landroid/os/Handler;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$4100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/os/Handler;

    move-result-object v18

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34$5;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34$5;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;)V

    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_5

    .line 1921
    .end local v8    # "detailInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .end local v10    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .end local v17    # "updateDate":Ljava/lang/String;
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->isActivityVisible()Z

    move-result v18

    if-eqz v18, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mRequestCanceled:Z
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Z

    move-result v18

    if-nez v18, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-wide v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->syncstamp:J

    move-wide/from16 v18, v0

    cmp-long v18, v18, p5

    if-nez v18, :cond_12

    .line 1922
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getDataHandler:Landroid/os/Handler;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$4100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/os/Handler;

    move-result-object v18

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34$6;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34$6;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;)V

    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_5
.end method
