.class Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$10;
.super Ljava/lang/Object;
.source "MapsActivityNew.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->setEventListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 837
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$10;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    .line 839
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$10;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$2700(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 840
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$10;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$2700(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setFocusableInTouchMode(Z)V

    .line 841
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$10;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$2700(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setCursorVisible(Z)V

    .line 842
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$10;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$2700(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setFocusable(Z)V

    .line 844
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$10;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$2800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 845
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$10;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$2800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$10;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a006d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHintTextColor(I)V

    .line 847
    :cond_1
    const/4 v0, 0x0

    return v0
.end method
