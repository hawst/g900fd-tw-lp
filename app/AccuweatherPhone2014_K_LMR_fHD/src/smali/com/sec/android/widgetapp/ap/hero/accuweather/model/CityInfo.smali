.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
.super Ljava/lang/Object;
.source "CityInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field mCity:Ljava/lang/String;

.field mLatitude:Ljava/lang/String;

.field mLocation:Ljava/lang/String;

.field mLongitude:Ljava/lang/String;

.field mProvider:I

.field mRealLocation:Ljava/lang/String;

.field mState:Ljava/lang/String;

.field mSummerTime:Ljava/lang/String;

.field zoomlevel:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 176
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo$1;

    invoke-direct {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo$1;-><init>()V

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mCity:Ljava/lang/String;

    .line 147
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mState:Ljava/lang/String;

    .line 148
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mLocation:Ljava/lang/String;

    .line 149
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mLatitude:Ljava/lang/String;

    .line 150
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mLongitude:Ljava/lang/String;

    .line 151
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mProvider:I

    .line 152
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->zoomlevel:I

    .line 153
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mRealLocation:Ljava/lang/String;

    .line 154
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mSummerTime:Ljava/lang/String;

    .line 155
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1, "city"    # Ljava/lang/String;
    .param p2, "state"    # Ljava/lang/String;
    .param p3, "location"    # Ljava/lang/String;
    .param p4, "latitude"    # Ljava/lang/String;
    .param p5, "longitude"    # Ljava/lang/String;
    .param p6, "provider"    # I

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mCity:Ljava/lang/String;

    .line 21
    iput-object p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mState:Ljava/lang/String;

    .line 22
    iput-object p3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mLocation:Ljava/lang/String;

    .line 23
    iput-object p4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mLatitude:Ljava/lang/String;

    .line 24
    iput-object p5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mLongitude:Ljava/lang/String;

    .line 25
    iput p6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mProvider:I

    .line 26
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 0
    .param p1, "mCity"    # Ljava/lang/String;
    .param p2, "mState"    # Ljava/lang/String;
    .param p3, "mLocation"    # Ljava/lang/String;
    .param p4, "mLatitude"    # Ljava/lang/String;
    .param p5, "mLongitude"    # Ljava/lang/String;
    .param p6, "mProvider"    # I
    .param p7, "zoomlevel"    # I

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mCity:Ljava/lang/String;

    .line 32
    iput-object p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mState:Ljava/lang/String;

    .line 33
    iput-object p3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mLocation:Ljava/lang/String;

    .line 34
    iput-object p4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mLatitude:Ljava/lang/String;

    .line 35
    iput-object p5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mLongitude:Ljava/lang/String;

    .line 36
    iput p6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mProvider:I

    .line 37
    iput p7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->zoomlevel:I

    .line 38
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 0
    .param p1, "city"    # Ljava/lang/String;
    .param p2, "state"    # Ljava/lang/String;
    .param p3, "location"    # Ljava/lang/String;
    .param p4, "latitude"    # Ljava/lang/String;
    .param p5, "longitude"    # Ljava/lang/String;
    .param p6, "summerTime"    # Ljava/lang/String;
    .param p7, "provider"    # I
    .param p8, "zoomlevel"    # I

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mCity:Ljava/lang/String;

    .line 44
    iput-object p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mState:Ljava/lang/String;

    .line 45
    iput-object p3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mLocation:Ljava/lang/String;

    .line 46
    iput-object p4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mLatitude:Ljava/lang/String;

    .line 47
    iput-object p5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mLongitude:Ljava/lang/String;

    .line 48
    iput p7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mProvider:I

    .line 49
    iput p8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->zoomlevel:I

    .line 50
    iput-object p6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mSummerTime:Ljava/lang/String;

    .line 52
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 130
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public getCity()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mCity:Ljava/lang/String;

    return-object v0
.end method

.method public getLatitude()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mLatitude:Ljava/lang/String;

    return-object v0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mLocation:Ljava/lang/String;

    return-object v0
.end method

.method public getLongitude()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mLongitude:Ljava/lang/String;

    return-object v0
.end method

.method public getProvider()I
    .locals 1

    .prologue
    .line 74
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mProvider:I

    return v0
.end method

.method public getRealLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mRealLocation:Ljava/lang/String;

    return-object v0
.end method

.method public getState()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mState:Ljava/lang/String;

    return-object v0
.end method

.method public getSummerTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mSummerTime:Ljava/lang/String;

    return-object v0
.end method

.method public getZoomlevel()I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->zoomlevel:I

    return v0
.end method

.method public setCity(Ljava/lang/String;)V
    .locals 0
    .param p1, "mCity"    # Ljava/lang/String;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mCity:Ljava/lang/String;

    .line 107
    return-void
.end method

.method public setLatitude(Ljava/lang/String;)V
    .locals 0
    .param p1, "mLatitude"    # Ljava/lang/String;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mLatitude:Ljava/lang/String;

    .line 123
    return-void
.end method

.method public setLocation(Ljava/lang/String;)V
    .locals 0
    .param p1, "mLocation"    # Ljava/lang/String;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mLocation:Ljava/lang/String;

    .line 115
    return-void
.end method

.method public setLongitude(Ljava/lang/String;)V
    .locals 0
    .param p1, "mLongitude"    # Ljava/lang/String;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mLongitude:Ljava/lang/String;

    .line 127
    return-void
.end method

.method public setProvider(I)V
    .locals 0
    .param p1, "mProvider"    # I

    .prologue
    .line 78
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mProvider:I

    .line 79
    return-void
.end method

.method public setRealLocation(Ljava/lang/String;)V
    .locals 0
    .param p1, "realLocation"    # Ljava/lang/String;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mRealLocation:Ljava/lang/String;

    .line 60
    return-void
.end method

.method public setState(Ljava/lang/String;)V
    .locals 0
    .param p1, "mState"    # Ljava/lang/String;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mState:Ljava/lang/String;

    .line 111
    return-void
.end method

.method public setSummerTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "mSummerTime"    # Ljava/lang/String;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mSummerTime:Ljava/lang/String;

    .line 119
    return-void
.end method

.method public setZoomlevel(I)V
    .locals 0
    .param p1, "zoomlevel"    # I

    .prologue
    .line 67
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->zoomlevel:I

    .line 68
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 158
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 159
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "CityInfo >> \n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cityInfo.getProvider() = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mProvider:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cityInfo.zoomlevel() = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->zoomlevel:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 170
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cityInfo.getRealLocation() ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mRealLocation:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cityInfo.getSummerTime() ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mSummerTime:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mCity:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mState:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 136
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mLocation:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 137
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mLatitude:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 138
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mLongitude:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 139
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mProvider:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 140
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->zoomlevel:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 141
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mRealLocation:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 142
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->mSummerTime:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 143
    return-void
.end method
