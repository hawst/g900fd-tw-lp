.class Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$3;
.super Landroid/os/Handler;
.source "SurfaceWidgetWeather.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v6, 0x100

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 145
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mRefreshUIHandler : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x20

    if-ne v1, v2, :cond_2

    .line 147
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->stopRefreshIcon()V
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;)V

    .line 148
    invoke-virtual {p0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$3;->hasMessages(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 149
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->updateView(Z)V
    invoke-static {v1, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;Z)V

    .line 152
    :cond_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mIsOnRefreshing:Z
    invoke-static {v1, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->access$1002(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;Z)Z

    .line 154
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090012

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    .line 155
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090013

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 156
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.widgetapp.ap.hero.accuweather.widget.action.APPWIDGET_UPDATE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 157
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "FROMSURFACE"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 158
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    invoke-virtual {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->sendBroadcast(Landroid/content/Intent;)V

    .line 163
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 164
    return-void

    .line 160
    :cond_2
    iget v1, p1, Landroid/os/Message;->what:I

    if-ne v1, v6, :cond_1

    .line 161
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->updateView(Z)V
    invoke-static {v1, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;Z)V

    goto :goto_0
.end method
