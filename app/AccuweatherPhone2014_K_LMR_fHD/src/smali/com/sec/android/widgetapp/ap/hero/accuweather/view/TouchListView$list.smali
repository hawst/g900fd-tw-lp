.class Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView$list;
.super Ljava/lang/Object;
.source "TouchListView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "list"
.end annotation


# instance fields
.field listener:Landroid/widget/AdapterView$OnItemClickListener;

.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;


# direct methods
.method public constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;
    .param p2, "listener"    # Landroid/widget/AdapterView$OnItemClickListener;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView$list;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iput-object p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView$list;->listener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 86
    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v0, 0x0

    .line 74
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView$list;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;

    iget-boolean v1, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;->holding:Z

    if-eqz v1, :cond_1

    .line 75
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView$list;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;->layoutChildren()V

    .line 76
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView$list;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;

    iput-boolean v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;->holding:Z

    .line 77
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView$list;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView$list;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;

    invoke-virtual {v2, p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;->isItemChecked(I)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v1, p3, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;->setItemChecked(IZ)V

    .line 82
    :goto_0
    return-void

    .line 81
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView$list;->listener:Landroid/widget/AdapterView$OnItemClickListener;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemClickListener;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    goto :goto_0
.end method
