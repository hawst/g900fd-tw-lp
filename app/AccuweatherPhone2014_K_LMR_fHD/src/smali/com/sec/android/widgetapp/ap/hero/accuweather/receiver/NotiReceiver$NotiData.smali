.class Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData;
.super Ljava/lang/Object;
.source "NotiReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "NotiData"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;
    }
.end annotation


# instance fields
.field private mIconNum:I

.field private mNotiMessage:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 224
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;->LINE_NIGHT:Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;->ordinal()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData;->mNotiMessage:[Ljava/lang/String;

    .line 225
    return-void
.end method


# virtual methods
.method public getIconNum()I
    .locals 1

    .prologue
    .line 244
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData;->mIconNum:I

    return v0
.end method

.method public getNotiMessage()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData;->mNotiMessage:[Ljava/lang/String;

    return-object v0
.end method

.method public setIconNum(I)V
    .locals 0
    .param p1, "iconNum"    # I

    .prologue
    .line 248
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData;->mIconNum:I

    .line 249
    return-void
.end method

.method public setNotiMessage(Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;Ljava/lang/String;)V
    .locals 2
    .param p1, "lineNum"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 232
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData;->mNotiMessage:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;->ordinal()I

    move-result v1

    aput-object p2, v0, v1

    .line 233
    return-void
.end method
