.class Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$1;
.super Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;
.source "RefreshManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->makeHttpClient(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;ZZI)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

.field final synthetic val$bool:Z

.field final synthetic val$detailWeatherInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

.field final synthetic val$needRefresh:Z

.field final synthetic val$tempScaleSetting:I

.field final synthetic val$visibleError:I


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;IZIZ)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    .prologue
    .line 786
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    iput-object p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$1;->val$detailWeatherInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    iput p3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$1;->val$tempScaleSetting:I

    iput-boolean p4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$1;->val$bool:Z

    iput p5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$1;->val$visibleError:I

    iput-boolean p6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$1;->val$needRefresh:Z

    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(IILjava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "position"    # I
    .param p2, "responseCode"    # I
    .param p3, "responseStatus"    # Ljava/lang/String;
    .param p4, "responseBody"    # Ljava/lang/String;

    .prologue
    .line 789
    invoke-super {p0, p1, p2, p3, p4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;->onReceive(IILjava/lang/String;Ljava/lang/String;)V

    .line 790
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " [mhc]responseCode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 791
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCanceled:Z
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$1100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 792
    const-string v2, ""

    const-string v3, " [mhc]rfh ccl!!!"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 817
    :goto_0
    return-void

    .line 795
    :cond_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->httpHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$1200(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/os/Handler;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 796
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 797
    .local v1, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 798
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "RESPONSE_CODE"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 799
    const-string v2, "RESPONSE_BODY"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 800
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$1;->val$detailWeatherInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getLocation()Ljava/lang/String;

    move-result-object v2

    const-string v3, "cityId:current"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 802
    const-string v2, "LOCATION_NAME"

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$1;->val$detailWeatherInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    .line 803
    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getRealLocation()Ljava/lang/String;

    move-result-object v3

    .line 802
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 807
    :goto_1
    const-string v2, "SCALAE_SETTING"

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$1;->val$tempScaleSetting:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 808
    const-string v2, "BOOL"

    iget-boolean v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$1;->val$bool:Z

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 809
    const-string v2, "VISIBLE_ERROR"

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$1;->val$visibleError:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 810
    const-string v2, "NEED_REFRESH"

    iget-boolean v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$1;->val$needRefresh:Z

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 811
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 812
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->httpHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$1200(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 816
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "msg":Landroid/os/Message;
    :goto_2
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    invoke-virtual {v2, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->deleteMe(I)V

    goto :goto_0

    .line 805
    .restart local v0    # "bundle":Landroid/os/Bundle;
    .restart local v1    # "msg":Landroid/os/Message;
    :cond_1
    const-string v2, "LOCATION_NAME"

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$1;->val$detailWeatherInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getLocation()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 814
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "msg":Landroid/os/Message;
    :cond_2
    const-string v2, ""

    const-string v3, "dataHandler is cleared"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
