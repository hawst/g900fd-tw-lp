.class Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2$1;
.super Ljava/lang/Object;
.source "MenuSettingsUseCurrentLocation.java"

# interfaces
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;->onCheckedChanged(Landroid/widget/CompoundButton;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;)V
    .locals 0
    .param p1, "this$1"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;

    .prologue
    .line 242
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public click(I)V
    .locals 3
    .param p1, "buttonType"    # I

    .prologue
    .line 244
    const/16 v0, 0xa

    if-ne p1, v0, :cond_2

    .line 245
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getLocationCheck(Landroid/content/Context;)I

    move-result v0

    const/16 v1, 0xbbb

    if-ne v0, v1, :cond_1

    .line 246
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;

    iget-object v1, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;

    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2$1$1;

    invoke-direct {v2, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2$1$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2$1;)V

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->showGPSoffDialog(Landroid/app/Activity;Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)Landroid/app/Dialog;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mPopupDialog:Landroid/app/Dialog;

    .line 265
    :cond_0
    :goto_0
    return-void

    .line 257
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;

    .line 258
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getCheckCurrentCityLocation(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->getCurrentLocation()V
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;)V

    goto :goto_0

    .line 261
    :cond_2
    const/16 v0, 0xb

    if-ne p1, v0, :cond_0

    .line 262
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->hideAllDialog()V
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;)V

    .line 263
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->setSwitchStatus()V

    goto :goto_0
.end method
