.class Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$3;
.super Ljava/lang/Object;
.source "MenuChangeOrdersList.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->setEventListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    .prologue
    .line 349
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    .line 351
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mOrgItemList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mOrgItemList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 362
    :cond_0
    :goto_0
    return-void

    .line 353
    :cond_1
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mOrgItemList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getLocation()Ljava/lang/String;

    move-result-object v1

    .line 354
    .local v1, "realCityId":Ljava/lang/String;
    if-eqz v1, :cond_2

    const-string v3, "cityId:current"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 355
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mOrgItemList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getRealLocation()Ljava/lang/String;

    move-result-object v1

    .line 358
    :cond_2
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCtx:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)Landroid/content/Context;

    move-result-object v4

    const-string v5, "DETAIL_HOME"

    invoke-virtual {v3, v4, v5, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->setViewUri(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 359
    .local v2, "uri":Landroid/net/Uri;
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 360
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 361
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    invoke-virtual {v3, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
