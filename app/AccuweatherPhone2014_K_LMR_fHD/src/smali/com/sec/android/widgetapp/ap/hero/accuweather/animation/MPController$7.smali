.class Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$7;
.super Landroid/os/Handler;
.source "MPController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    .prologue
    .line 746
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$7;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 749
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 750
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$7;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 751
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$7;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPResUri:Landroid/net/Uri;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 752
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$7;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPView:Landroid/widget/VideoView;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)Landroid/widget/VideoView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$7;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mIsVisibleActivity:Z
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 753
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$7;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->stop()V

    .line 766
    :goto_0
    return-void

    .line 756
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$7;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPView:Landroid/widget/VideoView;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)Landroid/widget/VideoView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setWFDTcpDisable(Z)V

    .line 757
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$7;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPView:Landroid/widget/VideoView;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)Landroid/widget/VideoView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$7;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPCompleteListener:Landroid/media/MediaPlayer$OnCompletionListener;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)Landroid/media/MediaPlayer$OnCompletionListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 758
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$7;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPView:Landroid/widget/VideoView;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)Landroid/widget/VideoView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$7;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPPrepareListener:Landroid/media/MediaPlayer$OnPreparedListener;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$800(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)Landroid/media/MediaPlayer$OnPreparedListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 759
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$7;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPView:Landroid/widget/VideoView;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)Landroid/widget/VideoView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$7;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$900(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)Landroid/media/MediaPlayer$OnErrorListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 760
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$7;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPView:Landroid/widget/VideoView;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)Landroid/widget/VideoView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$7;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPResUri:Landroid/net/Uri;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setVideoURI(Landroid/net/Uri;)V

    .line 761
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$7;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPView:Landroid/widget/VideoView;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)Landroid/widget/VideoView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/VideoView;->requestFocus()Z

    .line 762
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$7;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPView:Landroid/widget/VideoView;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)Landroid/widget/VideoView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setBackgroundColor(I)V

    goto :goto_0

    .line 764
    :cond_2
    const-string v0, ""

    const-string v1, "mp4 res uri null!!"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
