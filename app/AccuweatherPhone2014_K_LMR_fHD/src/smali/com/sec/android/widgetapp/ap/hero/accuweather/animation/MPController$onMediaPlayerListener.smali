.class public interface abstract Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;
.super Ljava/lang/Object;
.source "MPController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "onMediaPlayerListener"
.end annotation


# virtual methods
.method public abstract getCubeViewController()Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;
.end method

.method public abstract getCurrentBGView()Landroid/widget/ImageView;
.end method

.method public abstract getViEffectSettinginfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;
.end method

.method public abstract isConfigurationChanged()Z
.end method

.method public abstract isCreateActivity()Z
.end method

.method public abstract notifyEndAnimation(I)V
.end method

.method public abstract onPrepareComplete(Landroid/media/MediaPlayer;)V
.end method

.method public abstract setBGAlphaListener(Z)V
.end method

.method public abstract setBGVisibilityListener(Z)V
.end method
