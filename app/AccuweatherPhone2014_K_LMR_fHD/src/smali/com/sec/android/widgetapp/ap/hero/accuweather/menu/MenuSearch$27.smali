.class Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27;
.super Landroid/os/Handler;
.source "MenuSearch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 1105
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    const/4 v7, 0x0

    const/16 v6, 0x3e7

    .line 1107
    iget v4, p1, Landroid/os/Message;->what:I

    const/16 v5, 0x3f3

    if-ne v4, v5, :cond_1

    .line 1108
    const/4 v3, 0x0

    .line 1109
    .local v3, "intent":Landroid/content/Intent;
    new-instance v3, Landroid/content/Intent;

    .end local v3    # "intent":Landroid/content/Intent;
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    const-class v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1111
    .restart local v3    # "intent":Landroid/content/Intent;
    const-string v4, "flags"

    const/16 v5, -0x7530

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1112
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->hideLoadingDialog()V
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$2000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    .line 1113
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    invoke-virtual {v4, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->startActivity(Landroid/content/Intent;)V

    .line 1114
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->finish()V

    .line 1192
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 1125
    :cond_1
    iget v4, p1, Landroid/os/Message;->what:I

    const/16 v5, 0xca

    if-eq v4, v5, :cond_2

    iget v4, p1, Landroid/os/Message;->what:I

    const/16 v5, 0x3f2

    if-ne v4, v5, :cond_3

    .line 1128
    :cond_2
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->hideLoadingDialog()V
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$2000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    .line 1129
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->isActivityVisible()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1130
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    const/16 v6, 0x3f7

    new-instance v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27$1;

    invoke-direct {v7, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27;)V

    invoke-static {v5, v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->showDialog(Landroid/content/Context;ILcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)Landroid/app/Dialog;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mPopupDialog:Landroid/app/Dialog;

    goto :goto_0

    .line 1143
    :cond_3
    iget v4, p1, Landroid/os/Message;->what:I

    const/16 v5, 0xc8

    if-ne v4, v5, :cond_8

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->isActivityVisible()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1144
    const-string v4, ""

    const-string v5, "GET_CURRENT_LOCATION_OK"

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1145
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->hideAllDialog()V
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$3100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    .line 1146
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "cityinfo"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    .line 1147
    .local v2, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    invoke-direct {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;-><init>()V

    .line 1148
    .local v0, "dinfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "today"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    invoke-virtual {v0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->setTodayWeatherInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;)V

    .line 1150
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    const/4 v4, 0x7

    if-ge v1, v4, :cond_5

    .line 1151
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 1152
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    .line 1153
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    .line 1152
    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    invoke-virtual {v0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addForecastInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;)V

    .line 1150
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1157
    :cond_5
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->lstCity:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$3200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 1158
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->lstCity:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$3200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1160
    const-string v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->lstCity:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$3200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1162
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mNoSearchText:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$1700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Landroid/widget/TextView;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 1163
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mNoSearchText:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$1700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Landroid/widget/TextView;

    move-result-object v4

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1170
    :cond_6
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Landroid/widget/AutoCompleteTextView;

    move-result-object v4

    if-eqz v4, :cond_7

    .line 1171
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Landroid/widget/AutoCompleteTextView;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1174
    :cond_7
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    const v7, 0x7f030033

    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .line 1175
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->lstCity:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$3200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-direct {v5, v6, v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    .line 1174
    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->cityListAdapter2:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;
    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$3302(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;)Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;

    .line 1176
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getListView()Landroid/widget/ListView;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->cityListAdapter2:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;
    invoke-static {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$3300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1177
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getListView()Landroid/widget/ListView;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 1178
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getListView()Landroid/widget/ListView;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setVisibility(I)V

    .line 1179
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->cityListAdapter2:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$3300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;->notifyDataSetChanged()V

    goto/16 :goto_0

    .line 1180
    .end local v0    # "dinfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .end local v1    # "i":I
    .end local v2    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    :cond_8
    iget v4, p1, Landroid/os/Message;->what:I

    const/16 v5, 0xc9

    if-ne v4, v5, :cond_9

    .line 1181
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->hideAllDialog()V
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$3100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    .line 1183
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->isActivityVisible()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1184
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    invoke-static {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->showGPSoffDialog(Landroid/app/Activity;)Landroid/app/Dialog;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mPopupDialog:Landroid/app/Dialog;

    goto/16 :goto_0

    .line 1187
    :cond_9
    iget v4, p1, Landroid/os/Message;->what:I

    if-ne v4, v6, :cond_0

    .line 1188
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->hideAllDialog()V
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$3100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    .line 1189
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    invoke-virtual {v4, v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->setResult(ILandroid/content/Intent;)V

    .line 1190
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->finish()V

    goto/16 :goto_0
.end method
