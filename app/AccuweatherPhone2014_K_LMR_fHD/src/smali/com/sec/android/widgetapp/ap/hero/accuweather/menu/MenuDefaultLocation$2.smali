.class Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$2;
.super Ljava/lang/Object;
.source "MenuDefaultLocation.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 124
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->mInfoList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v0

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->defaultLocation:Ljava/lang/String;
    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->access$002(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;Ljava/lang/String;)Ljava/lang/String;

    .line 125
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->mDefaultLocationAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$WeatherClockListAdapter;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;)Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$WeatherClockListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$WeatherClockListAdapter;->notifyDataSetChanged()V

    .line 126
    return-void
.end method
