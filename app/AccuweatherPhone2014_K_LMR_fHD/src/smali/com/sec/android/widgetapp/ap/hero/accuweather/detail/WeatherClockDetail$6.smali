.class Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$6;
.super Ljava/lang/Object;
.source "WeatherClockDetail.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setEventListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 922
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$6;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 925
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$6;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$6;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "DETAIL_HOME"

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setViewUri(Landroid/content/Context;Ljava/lang/String;I)Landroid/net/Uri;

    move-result-object v2

    .line 926
    .local v2, "uri":Landroid/net/Uri;
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 927
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 928
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$6;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-virtual {v3, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 933
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "uri":Landroid/net/Uri;
    :goto_0
    return-void

    .line 929
    :catch_0
    move-exception v0

    .line 930
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v3, ""

    const-string v4, "ANFE home"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 931
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method
