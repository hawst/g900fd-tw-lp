.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultTwMultiSelectListener;
.super Ljava/lang/Object;
.source "DeviceDefaultTwMultiSelectListener.java"

# interfaces
.implements Lcom/sec/android/touchwiz/widget/TwAdapterView$OnItemSelectedListener;


# instance fields
.field public final mUseSPenMultiSelect:Z

.field public final mUseSpinnerSelectAll:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultTwMultiSelectListener;->mUseSpinnerSelectAll:Z

    .line 13
    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultTwMultiSelectListener;->mUseSPenMultiSelect:Z

    return-void
.end method


# virtual methods
.method public onItemSelected(Lcom/sec/android/touchwiz/widget/TwAdapterView;Landroid/view/View;IJ)V
    .locals 0
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/touchwiz/widget/TwAdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 20
    .local p1, "arg0":Lcom/sec/android/touchwiz/widget/TwAdapterView;, "Lcom/sec/android/touchwiz/widget/TwAdapterView<*>;"
    return-void
.end method

.method public onNothingSelected(Lcom/sec/android/touchwiz/widget/TwAdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/touchwiz/widget/TwAdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 26
    .local p1, "arg0":Lcom/sec/android/touchwiz/widget/TwAdapterView;, "Lcom/sec/android/touchwiz/widget/TwAdapterView<*>;"
    return-void
.end method
