.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;
.super Landroid/view/View;
.source "HotIconView.java"

# interfaces
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;


# static fields
.field private static final ANI_TYPE_SUN_ROTATION:I = 0x21

.field private static final ANI_TYPE_SUN_SCALE1:I = 0x22

.field private static final ANI_TYPE_SUN_SCALE2:I = 0x23

.field private static final ANI_TYPE_THERMOMETER_SCALE_Y:I = 0x11


# instance fields
.field isStop:Z

.field l:Landroid/animation/Animator$AnimatorListener;

.field private mAnimators:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/animation/ValueAnimator;",
            ">;"
        }
    .end annotation
.end field

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mIsActiveAnimationThread:Z

.field private mMarkingPaint:Landroid/graphics/Paint;

.field private mPaint:Landroid/graphics/Paint;

.field private mPaintColor:I

.field private mScale:F

.field private mSunCenterPath:Landroid/graphics/Path;

.field private mSunOutPath:Landroid/graphics/Path;

.field private mSunRotation:Landroid/animation/ValueAnimator;

.field private mSunScale1:Landroid/animation/ValueAnimator;

.field private mSunScale2:Landroid/animation/ValueAnimator;

.field private mThermometerInnerPath:Landroid/graphics/Path;

.field private mThermometerMarkingAniPath:Landroid/graphics/Path;

.field private mThermometerMarkingPath:Landroid/graphics/Path;

.field private mThermometerOutPath:Landroid/graphics/Path;

.field private mThermometerScaleYAni:Landroid/animation/ValueAnimator;

.field rotateOffset:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 76
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 77
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 85
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 86
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 80
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mAnimators:Ljava/util/HashMap;

    .line 43
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mIsActiveAnimationThread:Z

    .line 45
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mPaint:Landroid/graphics/Paint;

    .line 46
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mMarkingPaint:Landroid/graphics/Paint;

    .line 48
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    .line 50
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunCenterPath:Landroid/graphics/Path;

    .line 52
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunOutPath:Landroid/graphics/Path;

    .line 54
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mThermometerInnerPath:Landroid/graphics/Path;

    .line 56
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mThermometerOutPath:Landroid/graphics/Path;

    .line 58
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mThermometerMarkingPath:Landroid/graphics/Path;

    .line 60
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mThermometerMarkingAniPath:Landroid/graphics/Path;

    .line 62
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mBitmap:Landroid/graphics/Bitmap;

    .line 64
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mThermometerScaleYAni:Landroid/animation/ValueAnimator;

    .line 65
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunRotation:Landroid/animation/ValueAnimator;

    .line 66
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunScale1:Landroid/animation/ValueAnimator;

    .line 67
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunScale2:Landroid/animation/ValueAnimator;

    .line 69
    const v0, -0xa0a0b

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mPaintColor:I

    .line 495
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->l:Landroid/animation/Animator$AnimatorListener;

    .line 528
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->rotateOffset:F

    .line 529
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->isStop:Z

    .line 81
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->init()V

    .line 82
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mIsActiveAnimationThread:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mAnimators:Ljava/util/HashMap;

    return-object v0
.end method

.method private drawMasking(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "path"    # Landroid/graphics/Path;
    .param p3, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 321
    const/high16 v0, -0x10000

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 322
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 323
    invoke-virtual {p1, p2, p3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 324
    return-void
.end method

.method private drawSun(Landroid/graphics/Canvas;Landroid/graphics/Paint;FFF)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "paint"    # Landroid/graphics/Paint;
    .param p3, "rotation"    # F
    .param p4, "scale1"    # F
    .param p5, "scale2"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/high16 v4, 0x42dc0000    # 110.0f

    const/high16 v3, 0x42d10000    # 104.5f

    .line 259
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 260
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v0, v3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v4

    invoke-virtual {p1, p4, p4, v0, v1}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 261
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunCenterPath:Landroid/graphics/Path;

    invoke-virtual {p1, v0, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 263
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 264
    const/4 v0, 0x0

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v2, v4

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 265
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunOutPath:Landroid/graphics/Path;

    invoke-virtual {p1, v0, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 266
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 268
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 269
    const/high16 v0, 0x42b40000    # 90.0f

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v2, v4

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 270
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunOutPath:Landroid/graphics/Path;

    invoke-virtual {p1, v0, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 271
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 273
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 274
    const/high16 v0, 0x43340000    # 180.0f

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v2, v4

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 275
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunOutPath:Landroid/graphics/Path;

    invoke-virtual {p1, v0, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 276
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 278
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 279
    const/high16 v0, 0x43870000    # 270.0f

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v2, v4

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 280
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunOutPath:Landroid/graphics/Path;

    invoke-virtual {p1, v0, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 281
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 282
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 284
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 285
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v0, v3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v4

    invoke-virtual {p1, p5, p5, v0, v1}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 287
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 288
    const/high16 v0, 0x42340000    # 45.0f

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v2, v4

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 289
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunOutPath:Landroid/graphics/Path;

    invoke-virtual {p1, v0, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 290
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 292
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 293
    const/high16 v0, 0x43070000    # 135.0f

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v2, v4

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 294
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunOutPath:Landroid/graphics/Path;

    invoke-virtual {p1, v0, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 295
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 297
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 298
    const/high16 v0, 0x43610000    # 225.0f

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v2, v4

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 299
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunOutPath:Landroid/graphics/Path;

    invoke-virtual {p1, v0, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 300
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 302
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 303
    const v0, 0x439d8000    # 315.0f

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v2, v4

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 304
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunOutPath:Landroid/graphics/Path;

    invoke-virtual {p1, v0, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 305
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 307
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 308
    return-void
.end method

.method private drawThermometer(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "path"    # Landroid/graphics/Path;
    .param p3, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 311
    invoke-virtual {p1, p2, p3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 313
    return-void
.end method

.method private drawThermometerMarking(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "path"    # Landroid/graphics/Path;
    .param p3, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 317
    invoke-virtual {p1, p2, p3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 318
    return-void
.end method

.method private drawTransparentArea(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "paint"    # Landroid/graphics/Paint;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 327
    new-instance v0, Landroid/graphics/PorterDuffXfermode;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v1}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 328
    invoke-virtual {p1, p2, v2, v2, p3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 329
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 330
    return-void
.end method

.method private getSunCenterPath(Landroid/graphics/Path;)V
    .locals 12
    .param p1, "path"    # Landroid/graphics/Path;

    .prologue
    const v11, 0x426db021    # 59.422f

    const v10, 0x43127c29    # 146.485f

    const v9, 0x42da147b    # 109.04f

    const v8, 0x427e5f3b

    const v7, 0x42d1bbe7

    .line 129
    invoke-virtual {p1}, Landroid/graphics/Path;->reset()V

    .line 130
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v0, v7

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v8

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 131
    const v0, 0x429f9db2

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v2, v8, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v3, v11, v0

    const v0, 0x42a7f646

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v5, v11, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v6, v9, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 133
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v1, v11, v0

    const v0, 0x43061958    # 134.099f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x429f9e35

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x431a7c29    # 154.485f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v5, v7, v0

    const v0, 0x431a7c29    # 154.485f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 135
    const v0, 0x4301eccd

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x431a7c29    # 154.485f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x43164f1b

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x4306199a    # 134.1f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x43164f1b

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v5, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v6, v9, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 137
    const v0, 0x43164f5c    # 150.31f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x42a7f5c3    # 83.98f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x4301eccd

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v4, v8, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v5, v7, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v6, v8, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 139
    invoke-virtual {p1}, Landroid/graphics/Path;->close()V

    .line 140
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v0, v7

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v10

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 141
    const v0, 0x42a87127    # 84.221f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v2, v10, v0

    const v0, 0x4286d917

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x4301af9e

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x4286d917

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v5, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v6, v9, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 143
    const v0, 0x4286d917

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x42b0c8b4

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42a87127    # 84.221f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x428f2f9e

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v5, v7, v0

    const v0, 0x428f2f9e

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 145
    const v0, 0x42fb06a8    # 125.513f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x428f2f9e

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x430e4f1b

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x42b0c8b4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x430e4f1b

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v5, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v6, v9, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 147
    const v0, 0x430e4f5c    # 142.31f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x4301afdf

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42fb06a8    # 125.513f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v4, v10, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v5, v7, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v6, v10, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 149
    invoke-virtual {p1}, Landroid/graphics/Path;->close()V

    .line 151
    sget-object v0, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {p1, v0}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 152
    return-void
.end method

.method private getSunOutPath(Landroid/graphics/Path;)V
    .locals 12
    .param p1, "path"    # Landroid/graphics/Path;

    .prologue
    const v11, 0x42d1bbe7

    const v10, 0x41ac4fdf    # 21.539f

    const v9, 0x42d9bbe7

    const v8, 0x42c9bbe7

    const v7, 0x42417ae1    # 48.37f

    .line 155
    invoke-virtual {p1}, Landroid/graphics/Path;->reset()V

    .line 156
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v0, v11

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v7

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 157
    const v0, 0x42cd50e5

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v2, v7, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v3, v8, v0

    const v0, 0x423a50e5

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v5, v8, v0

    const v0, 0x42317ae1    # 44.37f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 159
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v0, v8

    const v1, 0x41cc4fdf    # 25.539f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 160
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v1, v8, v0

    const v0, 0x41baa3d7    # 23.33f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42cd50e5

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v4, v10, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v5, v11, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v6, v10, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 162
    const v0, 0x42d626e9

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v2, v10, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v3, v9, v0

    const v0, 0x41baa3d7    # 23.33f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v5, v9, v0

    const v0, 0x41cc4fdf    # 25.539f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 164
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v0, v9

    const v1, 0x42317ae1    # 44.37f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 165
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v1, v9, v0

    const v0, 0x423a50e5

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42d626e9

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v4, v7, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v5, v11, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v6, v7, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 167
    invoke-virtual {p1}, Landroid/graphics/Path;->close()V

    .line 169
    sget-object v0, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {p1, v0}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 170
    return-void
.end method

.method private getThermometerInnerLinePath(Landroid/graphics/Path;)V
    .locals 12
    .param p1, "path"    # Landroid/graphics/Path;

    .prologue
    const v11, 0x428d0fdf

    const v10, 0x426c0106    # 59.001f

    const v9, 0x422c0106    # 43.001f

    const v8, 0x4203a7f0

    const v7, 0x43407604

    .line 173
    invoke-virtual {p1}, Landroid/graphics/Path;->reset()V

    .line 174
    const v0, 0x424f3127    # 51.798f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v7

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 175
    const v0, 0x42255f3b

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v2, v7, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v3, v8, v0

    const v0, 0x4337fbe7

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v5, v8, v0

    const v0, 0x432d876d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 177
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v1, v8, v0

    const v0, 0x43291f7d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42088f5c    # 34.14f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x432512b0

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x4212a8f6    # 36.665f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x4321dae1

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 179
    const v0, 0x4219b958    # 38.431f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x431f9ae1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42200106    # 40.001f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x431dc3d7    # 157.765f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v5, v9, v0

    const v0, 0x431c8042

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 181
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v0, v9

    const v1, 0x42bc2979

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 182
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v1, v9, v0

    const v0, 0x42b305a2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x4239b958    # 46.431f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x42ab9c29    # 85.805f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x424c0106    # 51.001f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x42ab9c29    # 85.805f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 184
    const v0, 0x425e48b4    # 55.571f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x42ab9c29    # 85.805f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v2, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v3, v10, v0

    const v0, 0x42b305a2

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v5, v10, v0

    const v0, 0x42bc2979

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 186
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v0, v10

    const v1, 0x431c8042

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 187
    const/high16 v0, 0x427c0000    # 63.0f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x431dc3d7    # 157.765f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x4280e0c5

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x431f9ae1

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x428468f6

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x4321dae1

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 189
    const v0, 0x4289753f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x432512b0

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v2, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v3, v11, v0

    const v0, 0x43291f7d

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v5, v11, v0

    const v0, 0x432d876d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 191
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v1, v11, v0

    const v0, 0x4337fbe7

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42790312

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v4, v7, v0

    const v0, 0x424f3127    # 51.798f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v5, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v6, v7, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 193
    invoke-virtual {p1}, Landroid/graphics/Path;->close()V

    .line 194
    return-void
.end method

.method private getThermometerMarkingAniPath(Landroid/graphics/Path;)V
    .locals 12
    .param p1, "path"    # Landroid/graphics/Path;

    .prologue
    const v11, 0x4329ab02    # 169.668f

    const/high16 v10, 0x424c0000    # 51.0f

    const v9, 0x42b9daa0

    const/high16 v8, 0x42580000    # 54.0f

    const/high16 v7, 0x42400000    # 48.0f

    .line 237
    invoke-virtual {p1}, Landroid/graphics/Path;->reset()V

    .line 238
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v0, v10

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v9

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 239
    const v0, 0x42455f3b

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v2, v9, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v3, v7, v0

    const v0, 0x42bc8a3d    # 94.27f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v5, v7, v0

    const v0, 0x42bfdaa0

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 241
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v0, v7

    const v1, 0x42d6daa0

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 243
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v0, v7

    const v1, 0x431b2b02    # 155.168f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 244
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v0, v7

    const v1, 0x4326ab02    # 166.668f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 245
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v1, v7, v0

    const v0, 0x432852f2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42455f3b

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v4, v11, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v5, v10, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v6, v11, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 247
    const v0, 0x4252a0c5

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v2, v11, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v3, v8, v0

    const v0, 0x432852f2

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v5, v8, v0

    const v0, 0x4326ab02    # 166.668f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 249
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v0, v8

    const v1, 0x431b2b02    # 155.168f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 251
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v0, v8

    const v1, 0x42d6daa0

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 252
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v0, v8

    const v1, 0x42bfdaa0

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 253
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v1, v8, v0

    const v0, 0x42bc8a3d    # 94.27f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x4252a1cb    # 52.658f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v4, v9, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v5, v10, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v6, v9, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 255
    invoke-virtual {p1}, Landroid/graphics/Path;->close()V

    .line 256
    return-void
.end method

.method private getThermometerMarkingPath(Landroid/graphics/Path;)V
    .locals 12
    .param p1, "path"    # Landroid/graphics/Path;

    .prologue
    const v11, 0x4339d581    # 185.834f

    const v10, 0x432daac1

    const v9, 0x43218001    # 161.50002f

    const/high16 v8, 0x421c0000    # 39.0f

    const v7, 0x427d5604    # 63.334f

    .line 223
    invoke-virtual {p1}, Landroid/graphics/Path;->reset()V

    .line 224
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v0, v7

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v10

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 225
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v1, v7, v0

    const v0, 0x433462fc

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42678bee

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v4, v11, v0

    const v0, 0x424cab02    # 51.167f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v5, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v6, v11, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 227
    const v0, 0x4231ca16

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v2, v11, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v3, v8, v0

    const v0, 0x433462fc

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v5, v8, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v6, v10, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 229
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v1, v8, v0

    const v0, 0x4326f286

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x4231ca16

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v4, v9, v0

    const v0, 0x424cab02    # 51.167f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v5, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v6, v9, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 231
    const v0, 0x42678bee

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v2, v9, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v3, v7, v0

    const v0, 0x4326f286

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v5, v7, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v6, v10, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 233
    invoke-virtual {p1}, Landroid/graphics/Path;->close()V

    .line 234
    return-void
.end method

.method private getThermometerOutLinePath(Landroid/graphics/Path;)V
    .locals 12
    .param p1, "path"    # Landroid/graphics/Path;

    .prologue
    const v11, 0x4282df3b

    const v10, 0x4282c9ba

    const v9, 0x4212e979    # 36.728f

    const v8, 0x41cfd2f2    # 25.978f

    const v7, 0x42bd0083    # 94.501f

    .line 197
    invoke-virtual {p1}, Landroid/graphics/Path;->reset()V

    .line 198
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v0, v11

    const v1, 0x431908b4

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 199
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v0, v11

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v7

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 200
    const v0, 0x4282c106

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v7

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 201
    const v0, 0x4282c28f    # 65.38f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x42bcc72b    # 94.389f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v2, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v3, v10, v0

    const v0, 0x42bc8f5c    # 94.28f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v5, v10, v0

    const v0, 0x42bc5581    # 94.167f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 203
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v1, v10, v0

    const v0, 0x42ac8083    # 86.251f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x426be873

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x429fab02    # 79.834f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x424c3e77    # 51.061f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x429fab02    # 79.834f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 205
    const v0, 0x422c947b    # 43.145f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x429fab02    # 79.834f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v2, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v3, v9, v0

    const v0, 0x42ac8083    # 86.251f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v5, v9, v0

    const v0, 0x42bc5581    # 94.167f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 207
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v1, v9, v0

    const v0, 0x42bc8f5c    # 94.28f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x4212f7cf    # 36.742f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x42bcc72b    # 94.389f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x4212fae1

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v5, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v6, v7, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 209
    const v0, 0x4212be77    # 36.686f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v7

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 210
    const v0, 0x4212be77    # 36.686f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x431908b4

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 211
    const v0, 0x41f1b646    # 30.214f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x431d91ec    # 157.57f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v2, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v3, v8, v0

    const v0, 0x4325147b    # 165.08f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v5, v8, v0

    const v0, 0x432d9581    # 173.584f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 213
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v1, v8, v0

    const v0, 0x433b6fdf

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x4214d4fe    # 37.208f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x4346aac1

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x424c3e77    # 51.061f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x4346aac1

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 215
    const v0, 0x4281d3f8

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x4346aac1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x429849ba

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x433b6fdf

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x429849ba

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x432d9581    # 173.584f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 217
    const v0, 0x429849ba

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x4325147b    # 165.08f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x428fd0e5

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x431d91ec    # 157.57f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float v5, v11, v0

    const v0, 0x431908b4

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 219
    invoke-virtual {p1}, Landroid/graphics/Path;->close()V

    .line 220
    return-void
.end method

.method private init()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/high16 v4, 0x43960000    # 300.0f

    const/high16 v3, 0x40a00000    # 5.0f

    .line 89
    invoke-virtual {p0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 90
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mPaint:Landroid/graphics/Paint;

    .line 91
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mMarkingPaint:Landroid/graphics/Paint;

    .line 92
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunCenterPath:Landroid/graphics/Path;

    .line 93
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunOutPath:Landroid/graphics/Path;

    .line 94
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mThermometerInnerPath:Landroid/graphics/Path;

    .line 95
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mThermometerOutPath:Landroid/graphics/Path;

    .line 96
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mThermometerMarkingPath:Landroid/graphics/Path;

    .line 97
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mThermometerMarkingAniPath:Landroid/graphics/Path;

    .line 99
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mPaintColor:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 100
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 101
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 102
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 103
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 105
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mMarkingPaint:Landroid/graphics/Paint;

    const v2, -0xff9f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 106
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mMarkingPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 107
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mMarkingPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 108
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mMarkingPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 109
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mMarkingPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 111
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->initPath()V

    .line 113
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v2, v4

    float-to-int v2, v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mBitmap:Landroid/graphics/Bitmap;

    .line 115
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 116
    .local v0, "mMaskingCanvas":Landroid/graphics/Canvas;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mThermometerInnerPath:Landroid/graphics/Path;

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->drawMasking(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 117
    return-void
.end method

.method private initPath()V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mThermometerInnerPath:Landroid/graphics/Path;

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->getThermometerInnerLinePath(Landroid/graphics/Path;)V

    .line 121
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mThermometerOutPath:Landroid/graphics/Path;

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->getThermometerOutLinePath(Landroid/graphics/Path;)V

    .line 122
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mThermometerMarkingPath:Landroid/graphics/Path;

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->getThermometerMarkingPath(Landroid/graphics/Path;)V

    .line 123
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mThermometerMarkingAniPath:Landroid/graphics/Path;

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->getThermometerMarkingAniPath(Landroid/graphics/Path;)V

    .line 124
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunCenterPath:Landroid/graphics/Path;

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->getSunCenterPath(Landroid/graphics/Path;)V

    .line 125
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunOutPath:Landroid/graphics/Path;

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->getSunOutPath(Landroid/graphics/Path;)V

    .line 126
    return-void
.end method


# virtual methods
.method public cancelAnimation()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 470
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mAnimators:Ljava/util/HashMap;

    if-eqz v3, :cond_1

    .line 471
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 472
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 473
    .local v2, "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 474
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 475
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    .line 476
    .local v0, "ani":Landroid/animation/ValueAnimator;
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 477
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    goto :goto_0

    .line 482
    .end local v0    # "ani":Landroid/animation/ValueAnimator;
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    .end local v2    # "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_1
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunRotation:Landroid/animation/ValueAnimator;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunRotation:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 483
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunRotation:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->cancel()V

    .line 485
    :cond_2
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunScale1:Landroid/animation/ValueAnimator;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunScale1:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 486
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunScale1:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->cancel()V

    .line 488
    :cond_3
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunScale2:Landroid/animation/ValueAnimator;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunScale2:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 489
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunScale2:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->cancel()V

    .line 492
    :cond_4
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mIsActiveAnimationThread:Z

    .line 493
    return-void
.end method

.method public getScale()F
    .locals 1

    .prologue
    .line 558
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 401
    return-object p0
.end method

.method public isRunning()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 535
    const/4 v3, 0x0

    .line 536
    .local v3, "ret":Z
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mAnimators:Ljava/util/HashMap;

    if-eqz v4, :cond_1

    .line 537
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 538
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 539
    .local v2, "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 540
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 541
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    .line 542
    .local v0, "ani":Landroid/animation/ValueAnimator;
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 543
    const/4 v3, 0x1

    .line 549
    .end local v0    # "ani":Landroid/animation/ValueAnimator;
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    .end local v2    # "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_1
    return v3
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 17
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 334
    :try_start_0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    const/4 v3, 0x0

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_1

    .line 335
    const-string v2, ""

    const-string v3, "scale is less then 0"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    :cond_0
    :goto_0
    return-void

    .line 338
    :cond_1
    const/high16 v15, 0x3f800000    # 1.0f

    .line 339
    .local v15, "thermometerScaleY":F
    const/4 v10, 0x0

    .line 340
    .local v10, "rotation":F
    const/high16 v11, 0x3f800000    # 1.0f

    .line 341
    .local v11, "scale1":F
    const/high16 v12, 0x3f800000    # 1.0f

    .line 343
    .local v12, "scale2":F
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mIsActiveAnimationThread:Z

    if-eqz v2, :cond_5

    .line 344
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mThermometerScaleYAni:Landroid/animation/ValueAnimator;

    if-eqz v2, :cond_2

    .line 345
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mThermometerScaleYAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v15

    .line 348
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunRotation:Landroid/animation/ValueAnimator;

    if-eqz v2, :cond_3

    .line 349
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunRotation:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->rotateOffset:F

    add-float v10, v2, v3

    .line 352
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunScale1:Landroid/animation/ValueAnimator;

    if-eqz v2, :cond_4

    .line 353
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunScale1:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v11

    .line 356
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunScale2:Landroid/animation/ValueAnimator;

    if-eqz v2, :cond_5

    .line 357
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunScale2:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v12

    .line 358
    const/high16 v2, 0x3f800000    # 1.0f

    cmpg-float v2, v12, v2

    if-gez v2, :cond_5

    .line 359
    const/high16 v12, 0x3f800000    # 1.0f

    .line 364
    :cond_5
    const/16 v8, 0x1f

    .line 366
    .local v8, "saveFlag":I
    const/16 v8, 0xf

    .line 369
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v3, v2

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v4, v2

    const/high16 v2, 0x43960000    # 300.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v5, v2

    const/high16 v2, 0x43960000    # 300.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v6, v2

    const/4 v7, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Canvas;->saveLayer(FFFFLandroid/graphics/Paint;I)I

    move-result v16

    .line 372
    .local v16, "transparentSaveLevel":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move v5, v10

    move v6, v11

    move v7, v12

    invoke-direct/range {v2 .. v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->drawSun(Landroid/graphics/Canvas;Landroid/graphics/Paint;FFF)V

    .line 373
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mThermometerOutPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 374
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->drawTransparentArea(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V

    .line 376
    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 378
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v14

    .line 380
    .local v14, "thermometerSaveLavel":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mThermometerMarkingPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mMarkingPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 381
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v13

    .line 383
    .local v13, "thermometerAniLavel":I
    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v3, 0x424c0000    # 51.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4329ab02    # 169.668f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    mul-float/2addr v4, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15, v3, v4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 384
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mThermometerMarkingAniPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mMarkingPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 386
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 388
    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 390
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->isRunning()Z

    move-result v2

    if-nez v2, :cond_0

    .line 391
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->cancelAnimation()V

    .line 392
    const-string v2, ""

    const-string v3, "cancelA"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 395
    .end local v8    # "saveFlag":I
    .end local v10    # "rotation":F
    .end local v11    # "scale1":F
    .end local v12    # "scale2":F
    .end local v13    # "thermometerAniLavel":I
    .end local v14    # "thermometerSaveLavel":I
    .end local v15    # "thermometerScaleY":F
    .end local v16    # "transparentSaveLevel":I
    :catch_0
    move-exception v9

    .line 396
    .local v9, "e":Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0
.end method

.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 617
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x0

    .line 566
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onViewDetachedFromWindow : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 567
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mAnimators:Ljava/util/HashMap;

    if-eqz v3, :cond_2

    .line 568
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 569
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 570
    .local v2, "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 571
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 572
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    .line 573
    .local v0, "ani":Landroid/animation/ValueAnimator;
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 574
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 575
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    goto :goto_0

    .line 578
    .end local v0    # "ani":Landroid/animation/ValueAnimator;
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    :cond_1
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    .line 581
    .end local v2    # "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_2
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mAnimators:Ljava/util/HashMap;

    .line 582
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mPaint:Landroid/graphics/Paint;

    .line 583
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mMarkingPaint:Landroid/graphics/Paint;

    .line 584
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunCenterPath:Landroid/graphics/Path;

    .line 585
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunOutPath:Landroid/graphics/Path;

    .line 586
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mThermometerInnerPath:Landroid/graphics/Path;

    .line 587
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mThermometerOutPath:Landroid/graphics/Path;

    .line 588
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mThermometerMarkingPath:Landroid/graphics/Path;

    .line 589
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mThermometerMarkingAniPath:Landroid/graphics/Path;

    .line 590
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mThermometerScaleYAni:Landroid/animation/ValueAnimator;

    .line 592
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunRotation:Landroid/animation/ValueAnimator;

    if-eqz v3, :cond_3

    .line 593
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunRotation:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 594
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunRotation:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->cancel()V

    .line 596
    :cond_3
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunScale1:Landroid/animation/ValueAnimator;

    if-eqz v3, :cond_4

    .line 597
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunScale1:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 598
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunScale1:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->cancel()V

    .line 600
    :cond_4
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunScale2:Landroid/animation/ValueAnimator;

    if-eqz v3, :cond_5

    .line 601
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunScale2:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 602
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunScale2:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->cancel()V

    .line 606
    :cond_5
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunRotation:Landroid/animation/ValueAnimator;

    .line 607
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunScale1:Landroid/animation/ValueAnimator;

    .line 608
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunScale2:Landroid/animation/ValueAnimator;

    .line 610
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_6

    .line 611
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 613
    :cond_6
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mBitmap:Landroid/graphics/Bitmap;

    .line 614
    return-void
.end method

.method public setPaintColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 72
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mPaintColor:I

    .line 73
    return-void
.end method

.method public setScale(F)V
    .locals 0
    .param p1, "scale"    # F

    .prologue
    .line 553
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mScale:F

    .line 554
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->init()V

    .line 555
    return-void
.end method

.method public setWidthAndHeight(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 562
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, p1, p2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 563
    .local v0, "l":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 564
    return-void
.end method

.method public startAnimation()V
    .locals 11

    .prologue
    const-wide/16 v9, 0x708

    const/4 v8, 0x3

    const/4 v7, -0x1

    .line 405
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->isStop:Z

    .line 406
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 407
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 408
    .local v2, "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 409
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 410
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    .line 411
    .local v0, "ani":Landroid/animation/ValueAnimator;
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 412
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    goto :goto_0

    .line 415
    .end local v0    # "ani":Landroid/animation/ValueAnimator;
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    :cond_1
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    .line 417
    .end local v2    # "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_2
    const/4 v4, 0x0

    iput v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->rotateOffset:F

    .line 419
    new-array v4, v8, [F

    fill-array-data v4, :array_0

    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mThermometerScaleYAni:Landroid/animation/ValueAnimator;

    .line 420
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mThermometerScaleYAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v4, v7}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 421
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mThermometerScaleYAni:Landroid/animation/ValueAnimator;

    const-wide/16 v5, 0x7d0

    invoke-virtual {v4, v5, v6}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 422
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mThermometerScaleYAni:Landroid/animation/ValueAnimator;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 423
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mThermometerScaleYAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v4}, Landroid/animation/ValueAnimator;->start()V

    .line 424
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mAnimators:Ljava/util/HashMap;

    const/16 v5, 0x11

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mThermometerScaleYAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 426
    const/4 v4, 0x2

    new-array v4, v4, [F

    fill-array-data v4, :array_1

    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunRotation:Landroid/animation/ValueAnimator;

    .line 427
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunRotation:Landroid/animation/ValueAnimator;

    invoke-virtual {v4, v7}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 428
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunRotation:Landroid/animation/ValueAnimator;

    const-wide/16 v5, 0x3e8

    invoke-virtual {v4, v5, v6}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 429
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunRotation:Landroid/animation/ValueAnimator;

    new-instance v5, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v5}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 430
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunRotation:Landroid/animation/ValueAnimator;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 431
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunRotation:Landroid/animation/ValueAnimator;

    invoke-virtual {v4}, Landroid/animation/ValueAnimator;->start()V

    .line 432
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mAnimators:Ljava/util/HashMap;

    const/16 v5, 0x21

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunRotation:Landroid/animation/ValueAnimator;

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 434
    new-array v4, v8, [F

    fill-array-data v4, :array_2

    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunScale1:Landroid/animation/ValueAnimator;

    .line 435
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunScale1:Landroid/animation/ValueAnimator;

    invoke-virtual {v4, v7}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 436
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunScale1:Landroid/animation/ValueAnimator;

    invoke-virtual {v4, v9, v10}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 437
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunScale1:Landroid/animation/ValueAnimator;

    new-instance v5, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v5}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 438
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunScale1:Landroid/animation/ValueAnimator;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 439
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunScale1:Landroid/animation/ValueAnimator;

    invoke-virtual {v4}, Landroid/animation/ValueAnimator;->start()V

    .line 440
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mAnimators:Ljava/util/HashMap;

    const/16 v5, 0x22

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunScale1:Landroid/animation/ValueAnimator;

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 442
    new-array v4, v8, [F

    fill-array-data v4, :array_3

    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunScale2:Landroid/animation/ValueAnimator;

    .line 443
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunScale2:Landroid/animation/ValueAnimator;

    invoke-virtual {v4, v7}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 444
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunScale2:Landroid/animation/ValueAnimator;

    invoke-virtual {v4, v9, v10}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 445
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunScale2:Landroid/animation/ValueAnimator;

    const-wide/16 v5, 0x258

    invoke-virtual {v4, v5, v6}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 446
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunScale2:Landroid/animation/ValueAnimator;

    new-instance v5, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v5}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 447
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunScale2:Landroid/animation/ValueAnimator;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 448
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunScale2:Landroid/animation/ValueAnimator;

    invoke-virtual {v4}, Landroid/animation/ValueAnimator;->start()V

    .line 449
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mAnimators:Ljava/util/HashMap;

    const/16 v5, 0x23

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mSunScale2:Landroid/animation/ValueAnimator;

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 451
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->mIsActiveAnimationThread:Z

    .line 452
    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView$1;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;)V

    .line 465
    .local v3, "t":Ljava/lang/Thread;
    const/16 v4, 0xa

    invoke-virtual {v3, v4}, Ljava/lang/Thread;->setPriority(I)V

    .line 466
    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    .line 467
    return-void

    .line 419
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3f4ccccd    # 0.8f
        0x3f800000    # 1.0f
    .end array-data

    .line 426
    :array_1
    .array-data 4
        0x0
        0x42340000    # 45.0f
    .end array-data

    .line 434
    :array_2
    .array-data 4
        0x3f800000    # 1.0f
        0x3f8ccccd    # 1.1f
        0x3f800000    # 1.0f
    .end array-data

    .line 442
    :array_3
    .array-data 4
        0x3f800000    # 1.0f
        0x3f8ccccd    # 1.1f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public stopAnimation()V
    .locals 1

    .prologue
    .line 531
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/HotIconView;->isStop:Z

    .line 532
    return-void
.end method
