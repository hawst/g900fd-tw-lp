.class Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$5$1;
.super Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;
.source "MyCurrentLocation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$5;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$5;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$5;)V
    .locals 0
    .param p1, "this$1"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$5;

    .prologue
    .line 508
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$5$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$5;

    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(IILjava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "position"    # I
    .param p2, "responseCode"    # I
    .param p3, "responseStatus"    # Ljava/lang/String;
    .param p4, "responseBody"    # Ljava/lang/String;

    .prologue
    const/16 v6, 0xc8

    .line 511
    invoke-super {p0, p1, p2, p3, p4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;->onReceive(IILjava/lang/String;Ljava/lang/String;)V

    .line 513
    if-ne p2, v6, :cond_1

    .line 514
    const/4 v1, 0x0

    .line 515
    .local v1, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;

    invoke-direct {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;-><init>()V

    .line 516
    .local v3, "parser":Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$5$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$5;

    iget-object v4, v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->ctx:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, p4, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->parseDetailWeatherLocation(Ljava/lang/String;Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    move-result-object v1

    .line 518
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getCity()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 519
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$5$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$5;

    iget-object v4, v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$5;->val$temp_cityinfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getCity()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setCity(Ljava/lang/String;)V

    .line 526
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$5$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$5;

    iget-object v4, v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$5$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$5;

    iget-object v5, v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$5;->val$temp_cityinfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$5$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$5;

    iget-object v6, v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$5;->val$locationHandler:Landroid/os/Handler;

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->addCityWeatherFromDetail(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Landroid/os/Handler;)V

    .line 544
    .end local v1    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .end local v3    # "parser":Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;
    :goto_0
    return-void

    .line 529
    .restart local v1    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .restart local v3    # "parser":Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;
    :cond_0
    const/4 p2, -0x2

    .line 532
    .end local v1    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .end local v3    # "parser":Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;
    :cond_1
    if-eq p2, v6, :cond_2

    .line 533
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    .line 534
    .local v2, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 535
    .local v0, "data":Landroid/os/Bundle;
    invoke-virtual {v2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 536
    const/16 v4, 0x3f2

    iput v4, v2, Landroid/os/Message;->what:I

    .line 537
    const-string v4, "provider"

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$5$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$5;

    iget-object v5, v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mProvider:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 538
    const-string v4, "responseCode"

    invoke-virtual {v0, v4, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 539
    invoke-virtual {v2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 540
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$5$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$5;

    iget-object v4, v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$5;->val$locationHandler:Landroid/os/Handler;

    invoke-virtual {v4, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 543
    .end local v0    # "data":Landroid/os/Bundle;
    .end local v2    # "msg":Landroid/os/Message;
    :cond_2
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$5$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$5;

    iget-object v4, v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->stopHttpThread()V

    goto :goto_0
.end method
