.class Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$5;
.super Landroid/content/BroadcastReceiver;
.source "SurfaceWidgetWeather.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->registerTTReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    .prologue
    .line 518
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 521
    if-nez p2, :cond_1

    .line 522
    :try_start_0
    const-string v2, ""

    const-string v3, "==============action : intent = null"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    :cond_0
    :goto_0
    return-void

    .line 525
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 527
    .local v0, "action":Ljava/lang/String;
    const-string v2, "android.intent.action.TIME_TICK"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 528
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "=============== BR act = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 529
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->doChangeDayOrNight()Z
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 530
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    const/4 v3, 0x0

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->updateClock(Z)V
    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 533
    .end local v0    # "action":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 534
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
