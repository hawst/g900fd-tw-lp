.class Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$6;
.super Ljava/lang/Object;
.source "MenuSettings.java"

# interfaces
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

.field final synthetic val$newValue:Ljava/lang/Object;

.field final synthetic val$preference:Landroid/preference/Preference;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;Ljava/lang/Object;Landroid/preference/Preference;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    .prologue
    .line 1447
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$6;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    iput-object p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$6;->val$newValue:Ljava/lang/Object;

    iput-object p3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$6;->val$preference:Landroid/preference/Preference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public click(I)V
    .locals 6
    .param p1, "buttonType"    # I

    .prologue
    .line 1449
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "bTy"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1450
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$6;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    invoke-virtual {v3, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->setButtenTypeCheck(I)V

    .line 1451
    const/16 v3, 0xa

    if-ne p1, v3, :cond_0

    .line 1452
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$6;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$6;->val$newValue:Ljava/lang/Object;

    .line 1453
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 1452
    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateAutoRefreshTime(Landroid/content/Context;I)I

    move-result v2

    .line 1455
    .local v2, "result":I
    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    .line 1456
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$6;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    invoke-virtual {v3, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->setResult(I)V

    .line 1457
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$6;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->finish()V

    .line 1471
    .end local v2    # "result":I
    :cond_0
    :goto_0
    return-void

    .line 1459
    .restart local v2    # "result":I
    :cond_1
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$6;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    .line 1460
    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1459
    invoke-static {v3, v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getNextTime(Landroid/content/Context;ZZ)J

    move-result-wide v0

    .line 1461
    .local v0, "nextTime":J
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$6;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateNextTime(Landroid/content/Context;J)I

    .line 1462
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$6;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->init()V
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->access$800(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V

    .line 1464
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$6;->val$preference:Landroid/preference/Preference;

    check-cast v3, Landroid/preference/ListPreference;

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$6;->val$newValue:Ljava/lang/Object;

    .line 1465
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 1464
    invoke-virtual {v3, v4}, Landroid/preference/ListPreference;->setValueIndex(I)V

    .line 1466
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$6;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$6;->val$preference:Landroid/preference/Preference;

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;->checkSameDaemonCityIdAtStting(Landroid/content/Context;Landroid/preference/Preference;)V

    .line 1468
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$6;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->setValues()V
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->access$900(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V

    goto :goto_0
.end method
