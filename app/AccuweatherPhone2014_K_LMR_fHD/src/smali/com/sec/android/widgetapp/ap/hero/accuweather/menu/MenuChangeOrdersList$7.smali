.class Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$7;
.super Ljava/lang/Object;
.source "MenuChangeOrdersList.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->showRefreshDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    .prologue
    .line 515
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$7;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 517
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$7;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mRefreshDialog:Landroid/app/Dialog;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->access$902(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;Landroid/app/Dialog;)Landroid/app/Dialog;

    .line 518
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$7;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 519
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$7;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->cancleRefresh()V

    .line 520
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$7;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->setRefreshCanceled(Z)V

    .line 526
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$7;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->stopRefreshAnimation()V
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->access$1000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)V

    .line 527
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$7;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 528
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$7;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->refreshListView()V

    .line 529
    return-void
.end method
