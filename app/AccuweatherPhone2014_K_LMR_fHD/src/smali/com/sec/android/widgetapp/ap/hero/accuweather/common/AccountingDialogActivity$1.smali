.class Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity$1;
.super Ljava/lang/Object;
.source "AccountingDialogActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;

.field final synthetic val$ctx:Landroid/content/Context;

.field final synthetic val$intent:Landroid/content/Intent;

.field final synthetic val$mActivity:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;Landroid/app/Activity;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;

    iput-object p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity$1;->val$mActivity:Landroid/app/Activity;

    iput-object p3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity$1;->val$ctx:Landroid/content/Context;

    iput-object p4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity$1;->val$intent:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 90
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setShowChargerPopup(Landroid/content/Context;Z)V

    .line 92
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;

    iget-object v1, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mDialogFrom:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 93
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onDismiss : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;

    iget-object v3, v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mDialogFrom:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;

    iget-object v1, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mDialogFrom:Ljava/lang/String;

    const-string v2, "FROM_REFRESH"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 95
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.android.widgetapp.ap.hero.accuweather.action.REFRESH_FROM_ACCOUNTING_DIALOG"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 96
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity$1;->val$mActivity:Landroid/app/Activity;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->activityfinish(Landroid/app/Activity;)V
    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;Landroid/app/Activity;)V

    .line 127
    :cond_0
    :goto_0
    return-void

    .line 97
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;

    iget-object v1, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mDialogFrom:Ljava/lang/String;

    const-string v2, "FROM_TOUCH_WIDGET"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 99
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;

    iget-object v1, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mStartActivity:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 100
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;

    iget-object v1, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mStartActivity:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 101
    .local v0, "sintent":Landroid/content/Intent;
    const-string v1, "flags"

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;

    iget v2, v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mActivityFlags:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 102
    const-string v1, "widget_mode"

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;

    iget v2, v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mWidgetMode:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 103
    const-string v1, "searchlocation"

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;

    iget-object v2, v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mLocation:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 104
    const-string v1, "where_from_to_details"

    const/16 v2, 0x101

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 106
    const v1, 0x10008000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 109
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->startActivity(Landroid/content/Intent;)V

    .line 111
    .end local v0    # "sintent":Landroid/content/Intent;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity$1;->val$mActivity:Landroid/app/Activity;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->activityfinish(Landroid/app/Activity;)V
    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;Landroid/app/Activity;)V

    goto :goto_0

    .line 112
    :cond_3
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;

    iget-object v1, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mDialogFrom:Ljava/lang/String;

    const-string v2, "FROM_TOUCH_WIDGET_EASY"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 113
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity$1;->val$ctx:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090011

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-nez v1, :cond_4

    .line 114
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity$1;->val$mActivity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity$1;->val$ctx:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity$1;->val$intent:Landroid/content/Intent;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->showUseCurrentLocationPopup(Landroid/app/Activity;Landroid/content/Context;Landroid/content/Intent;)V
    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;Landroid/app/Activity;Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    .line 116
    :cond_4
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.widgetapp.ap.hero.accuweather.easy.widget.action.ACTIVITY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 117
    .restart local v0    # "sintent":Landroid/content/Intent;
    const-string v1, "cls"

    const-string v2, "com.samsung.sec.android.widgetapp.intent.action.MENU_DETAIL_GL"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 119
    const-string v1, "flags"

    const/16 v2, -0x7530

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 120
    const-string v1, "searchlocation"

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;

    iget-object v2, v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->mSelectedIndex_easy:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 121
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 122
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity$1;->val$mActivity:Landroid/app/Activity;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->activityfinish(Landroid/app/Activity;)V
    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;Landroid/app/Activity;)V

    goto/16 :goto_0
.end method
