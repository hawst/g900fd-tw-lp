.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;
.super Ljava/lang/Object;
.source "SnowParticleObj.java"

# interfaces
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/ParticleObj;


# instance fields
.field private final FADEIN_SPEED:I

.field private final MAX_ALPHA:I

.field private final MIN_RADIUS:F

.field private final MOVE_Y_SPEED:F

.field mAlpha:I

.field mAnimX:F

.field mAnimY:F

.field private mDelayCnt:I

.field private mInterval:I

.field mRadius:F

.field mSX:F

.field mSY:F


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/16 v4, 0xff

    const/4 v3, 0x0

    const/high16 v2, 0x40400000    # 3.0f

    const/4 v1, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->FADEIN_SPEED:I

    .line 10
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->MIN_RADIUS:F

    .line 12
    iput v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->MAX_ALPHA:I

    .line 14
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->MOVE_Y_SPEED:F

    .line 16
    iput v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->mAlpha:I

    .line 18
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->mSX:F

    .line 20
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->mSY:F

    .line 22
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->mAnimX:F

    .line 24
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->mAnimY:F

    .line 26
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->mRadius:F

    .line 44
    iput v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->mInterval:I

    .line 50
    iput v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->mDelayCnt:I

    .line 30
    return-void
.end method


# virtual methods
.method public Animate(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "paint"    # Landroid/graphics/Paint;

    .prologue
    const/4 v2, 0x0

    .line 53
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->mDelayCnt:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->mDelayCnt:I

    .line 55
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->mDelayCnt:I

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->mInterval:I

    if-ge v0, v1, :cond_1

    .line 73
    :cond_0
    :goto_0
    return-void

    .line 58
    :cond_1
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->mAlpha:I

    const/4 v1, 0x6

    if-le v0, v1, :cond_2

    .line 59
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->mAlpha:I

    add-int/lit8 v0, v0, -0x6

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->mAlpha:I

    .line 63
    :goto_1
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->mAlpha:I

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 64
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->mAnimY:F

    const/high16 v1, 0x40400000    # 3.0f

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->mAnimY:F

    .line 65
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    .line 67
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->mAlpha:I

    if-gtz v0, :cond_0

    .line 68
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->mDelayCnt:I

    .line 69
    const/16 v0, 0xff

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->mAlpha:I

    .line 70
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->mSX:F

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->mAnimX:F

    .line 71
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->mSY:F

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->mAnimY:F

    goto :goto_0

    .line 61
    :cond_2
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->mAlpha:I

    goto :goto_1
.end method

.method public draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 76
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->mAnimX:F

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->mAnimY:F

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->mRadius:F

    invoke-virtual {p1, v0, v1, v2, p2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 77
    return-void
.end method

.method public setInterval(I)V
    .locals 0
    .param p1, "interval"    # I

    .prologue
    .line 47
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->mInterval:I

    .line 48
    return-void
.end method

.method public setPos(FFF)V
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "val"    # F

    .prologue
    .line 33
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->mSX:F

    .line 34
    iput p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->mSY:F

    .line 35
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->mSX:F

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->mAnimX:F

    .line 36
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->mSY:F

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->mAnimY:F

    .line 37
    const/4 v0, 0x0

    cmpl-float v0, p3, v0

    if-nez v0, :cond_0

    .line 38
    const/high16 v0, 0x40400000    # 3.0f

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->mRadius:F

    .line 42
    :goto_0
    return-void

    .line 40
    :cond_0
    iput p3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->mRadius:F

    goto :goto_0
.end method
