.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;
.super Ljava/lang/Object;
.source "CubeViewController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;
    }
.end annotation


# static fields
.field private static mScrollAniType:I


# instance fields
.field protected mAnimationListener:Landroid/view/animation/Animation$AnimationListener;

.field protected mCtx:Landroid/content/Context;

.field protected mDataIndex:I

.field protected mDataSize:I

.field protected mDepthZ:F

.field protected mDgree:F

.field protected mDuration:J

.field protected mHeight:I

.field protected mIsRunningAni:Z

.field protected mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;

.field private mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

.field public mMPDelayHandler:Landroid/os/Handler;

.field private mScrollInAnim:Landroid/view/animation/Animation;

.field private mScrollOutAnim:Landroid/view/animation/Animation;

.field private mTimer:Ljava/util/Timer;

.field private mViEffectSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

.field protected mViewArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation
.end field

.field protected mViewIndex:I

.field protected mWidth:I

.field protected mWidthOffset:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 442
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollAniType:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .locals 4
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "dataSize"    # I
    .param p3, "dataPos"    # I

    .prologue
    const/16 v1, 0x12c

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const v0, 0x44a28000    # 1300.0f

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mDepthZ:F

    .line 28
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViewIndex:I

    .line 30
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mDataSize:I

    .line 32
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mDataIndex:I

    .line 34
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViewArray:Ljava/util/ArrayList;

    .line 36
    const/high16 v0, 0x43960000    # 300.0f

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mWidthOffset:F

    .line 38
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mCtx:Landroid/content/Context;

    .line 40
    const/high16 v0, 0x42820000    # 65.0f

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mDgree:F

    .line 42
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mWidth:I

    .line 44
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mHeight:I

    .line 46
    const-wide/16 v0, 0x320

    iput-wide v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mDuration:J

    .line 48
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;

    .line 50
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mIsRunningAni:Z

    .line 55
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViEffectSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    .line 57
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    .line 79
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$1;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 108
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$2;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mMPDelayHandler:Landroid/os/Handler;

    .line 394
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollOutAnim:Landroid/view/animation/Animation;

    .line 395
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollInAnim:Landroid/view/animation/Animation;

    .line 131
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mCtx:Landroid/content/Context;

    .line 132
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mWidthOffset:F

    .line 133
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViewIndex:I

    .line 134
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mDataSize:I

    .line 135
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mDataIndex:I

    .line 136
    invoke-virtual {p0, p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->setDataIndex(I)V

    .line 137
    invoke-virtual {p0, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->setDataSize(I)Z

    .line 138
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    return-object v0
.end method

.method private setScrollLeftInAnimation()V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 467
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    sget v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollAniType:I

    const/high16 v2, -0x40800000    # -1.0f

    sget v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollAniType:I

    sget v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollAniType:I

    sget v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollAniType:I

    move v6, v4

    move v8, v4

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 470
    .local v0, "scrollLeftIn":Landroid/view/animation/Animation;
    iget-wide v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mDuration:J

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 471
    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 472
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 473
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 474
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollInAnim:Landroid/view/animation/Animation;

    .line 475
    return-void
.end method

.method private setScrollLeftOutAnimation()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 457
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    sget v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollAniType:I

    sget v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollAniType:I

    const/high16 v4, -0x40800000    # -1.0f

    sget v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollAniType:I

    sget v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollAniType:I

    move v6, v2

    move v8, v2

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 460
    .local v0, "scrollLeftOut":Landroid/view/animation/Animation;
    iget-wide v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mDuration:J

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 461
    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 462
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 463
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollOutAnim:Landroid/view/animation/Animation;

    .line 464
    return-void
.end method

.method private setScrollRightInAnimation()V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 446
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    sget v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollAniType:I

    const/high16 v2, 0x3f800000    # 1.0f

    sget v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollAniType:I

    sget v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollAniType:I

    sget v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollAniType:I

    move v6, v4

    move v8, v4

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 449
    .local v0, "scrollRightIn":Landroid/view/animation/Animation;
    iget-wide v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mDuration:J

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 450
    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 451
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 452
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 453
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollInAnim:Landroid/view/animation/Animation;

    .line 454
    return-void
.end method

.method private setScrollRightOutAnimation()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 478
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    sget v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollAniType:I

    sget v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollAniType:I

    const/high16 v4, 0x3f800000    # 1.0f

    sget v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollAniType:I

    sget v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollAniType:I

    move v6, v2

    move v8, v2

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 481
    .local v0, "scrollRightOut":Landroid/view/animation/Animation;
    iget-wide v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mDuration:J

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 482
    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 483
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 484
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollOutAnim:Landroid/view/animation/Animation;

    .line 485
    return-void
.end method

.method private turnLeft(Landroid/view/View;Landroid/view/View;Ljava/lang/Boolean;)V
    .locals 4
    .param p1, "scrollInView"    # Landroid/view/View;
    .param p2, "scrollOutView"    # Landroid/view/View;
    .param p3, "isEntering"    # Ljava/lang/Boolean;

    .prologue
    const/4 v3, 0x1

    .line 420
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    .line 421
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->prepareView()V

    .line 423
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->bringToFront()V

    .line 424
    const/4 v0, 0x0

    .line 426
    .local v0, "centerX":F
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mCtx:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getOrientation(Landroid/content/Context;)I

    move-result v1

    if-ne v1, v3, :cond_2

    .line 427
    const/4 v1, 0x0

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mWidthOffset:F

    add-float v0, v1, v2

    .line 432
    :goto_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViEffectSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getCityChangeScrollAnimation()I

    move-result v1

    if-ne v1, v3, :cond_1

    .line 434
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->setScrollLeftOutAnimation()V

    .line 435
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->setScrollRightInAnimation()V

    .line 436
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollInAnim:Landroid/view/animation/Animation;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 437
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollOutAnim:Landroid/view/animation/Animation;

    invoke-virtual {p2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 438
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollInAnim:Landroid/view/animation/Animation;

    invoke-virtual {p1, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 440
    :cond_1
    return-void

    .line 429
    :cond_2
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mWidthOffset:F

    neg-float v1, v1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    add-float v0, v1, v2

    goto :goto_0
.end method

.method private turnRight(Landroid/view/View;Landroid/view/View;)V
    .locals 4
    .param p1, "scrollInView"    # Landroid/view/View;
    .param p2, "scrollOutView"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    .line 398
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->prepareView()V

    .line 399
    invoke-virtual {p1}, Landroid/view/View;->bringToFront()V

    .line 401
    const/4 v0, 0x0

    .line 403
    .local v0, "centerX":F
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mCtx:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getOrientation(Landroid/content/Context;)I

    move-result v1

    if-ne v1, v3, :cond_1

    .line 404
    const/4 v1, 0x0

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mWidthOffset:F

    add-float v0, v1, v2

    .line 409
    :goto_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViEffectSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getCityChangeScrollAnimation()I

    move-result v1

    if-ne v1, v3, :cond_0

    .line 410
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->setScrollRightOutAnimation()V

    .line 411
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->setScrollLeftInAnimation()V

    .line 412
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollInAnim:Landroid/view/animation/Animation;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 413
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollOutAnim:Landroid/view/animation/Animation;

    invoke-virtual {p2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 414
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollInAnim:Landroid/view/animation/Animation;

    invoke-virtual {p1, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 417
    :cond_0
    return-void

    .line 406
    :cond_1
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mWidthOffset:F

    neg-float v1, v1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    add-float v0, v1, v2

    goto :goto_0
.end method


# virtual methods
.method public getCurrentDataIndex()I
    .locals 1

    .prologue
    .line 245
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mDataIndex:I

    return v0
.end method

.method public getCurrentView()Landroid/view/ViewGroup;
    .locals 2

    .prologue
    .line 284
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViewArray:Ljava/util/ArrayList;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViewIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method public getDataSize()I
    .locals 1

    .prologue
    .line 280
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mDataSize:I

    return v0
.end method

.method public getLayoutInController(II)Landroid/view/ViewGroup;
    .locals 5
    .param p1, "position"    # I
    .param p2, "resID"    # I

    .prologue
    .line 293
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getLayoutInAdapter : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViewArray:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 296
    .local v1, "root":Landroid/view/View;
    invoke-virtual {v1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 299
    .end local v1    # "root":Landroid/view/View;
    :goto_0
    return-object v2

    .line 297
    :catch_0
    move-exception v0

    .line 298
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 299
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getLayoutInController(Landroid/view/View;I)Landroid/view/ViewGroup;
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "resID"    # I

    .prologue
    .line 305
    move-object v1, p1

    .line 306
    .local v1, "root":Landroid/view/View;
    :try_start_0
    invoke-virtual {v1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 309
    :goto_0
    return-object v2

    .line 307
    :catch_0
    move-exception v0

    .line 308
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 309
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getMPDelayHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mMPDelayHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public getNextDataIndex()I
    .locals 3

    .prologue
    .line 258
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mDataIndex:I

    .line 259
    .local v0, "currentDataIndex":I
    add-int/lit8 v1, v0, 0x1

    .line 260
    .local v1, "nextDataIndex":I
    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mDataSize:I

    if-lt v1, v2, :cond_0

    .line 261
    const/4 v1, 0x0

    .line 263
    :cond_0
    return v1
.end method

.method public getNextView()Landroid/view/ViewGroup;
    .locals 2

    .prologue
    .line 288
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->getNextViewIndex()I

    move-result v0

    .line 289
    .local v0, "nextViewIndex":I
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViewArray:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    return-object v1
.end method

.method public getNextViewIndex()I
    .locals 2

    .prologue
    .line 228
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViewIndex:I

    add-int/lit8 v0, v1, 0x1

    .line 229
    .local v0, "pos":I
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViewArray:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 230
    const/4 v0, 0x0

    .line 233
    :cond_0
    return v0
.end method

.method public getPreDataIndex()I
    .locals 3

    .prologue
    .line 249
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mDataIndex:I

    .line 250
    .local v0, "currentDataIndex":I
    add-int/lit8 v1, v0, -0x1

    .line 251
    .local v1, "preDataIndex":I
    if-gez v1, :cond_0

    .line 252
    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mDataSize:I

    add-int/lit8 v1, v2, -0x1

    .line 254
    :cond_0
    return v1
.end method

.method public getPreViewIndex()I
    .locals 2

    .prologue
    .line 237
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViewIndex:I

    add-int/lit8 v0, v1, -0x1

    .line 238
    .local v0, "pos":I
    if-gez v0, :cond_0

    .line 239
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViewArray:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .line 241
    :cond_0
    return v0
.end method

.method public getViewIndex()I
    .locals 1

    .prologue
    .line 224
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViewIndex:I

    return v0
.end method

.method public initRotationViewController()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 181
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;

    if-eqz v1, :cond_2

    .line 182
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViewArray:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 183
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViewArray:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 184
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViewArray:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 186
    :cond_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViewArray:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;

    invoke-interface {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;->initView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 187
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViewArray:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 182
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 189
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;

    invoke-interface {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;->setCurrentView()V

    .line 190
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViewArray:Ljava/util/ArrayList;

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViewIndex:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 191
    const/4 v1, 0x1

    .line 197
    .end local v0    # "i":I
    :goto_1
    return v1

    .line 194
    :cond_2
    const-string v1, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "initRotationViewController : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViewIndex:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViewArray:Ljava/util/ArrayList;

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViewIndex:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    move v1, v2

    .line 197
    goto :goto_1
.end method

.method public isRunningAni()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mIsRunningAni:Z

    return v0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 150
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViewArray:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 151
    .local v1, "viewIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/view/ViewGroup;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 152
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 153
    .local v0, "v":Landroid/view/ViewGroup;
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    goto :goto_0

    .line 156
    .end local v0    # "v":Landroid/view/ViewGroup;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViewArray:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 157
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViewArray:Ljava/util/ArrayList;

    .line 158
    return-void
.end method

.method protected prepareView()V
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;

    invoke-interface {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;->setOnPageChangeListener()V

    .line 176
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;

    invoke-interface {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;->setCurrentView()V

    .line 178
    return-void
.end method

.method public resetData(II)Z
    .locals 4
    .param p1, "dataSize"    # I
    .param p2, "dataPos"    # I

    .prologue
    .line 141
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "resetData : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    const/4 v0, 0x0

    .line 143
    .local v0, "isSetView":Z
    invoke-virtual {p0, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->setDataIndex(I)V

    .line 144
    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->setDataSize(I)Z

    move-result v0

    .line 145
    return v0
.end method

.method public setDataIndex(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 267
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mDataIndex:I

    .line 268
    return-void
.end method

.method public setDataSize(I)Z
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 271
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mDataSize:I

    .line 272
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;

    invoke-interface {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;->setCurrentView()V

    .line 274
    const/4 v0, 0x1

    .line 276
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setIsRunningAni(Z)V
    .locals 1
    .param p1, "mIsRunningAni"    # Z

    .prologue
    .line 72
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViEffectSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    .line 73
    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getCityChangeScrollAnimation()I

    move-result v0

    if-nez v0, :cond_1

    .line 77
    :cond_0
    :goto_0
    return-void

    .line 76
    :cond_1
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mIsRunningAni:Z

    goto :goto_0
.end method

.method public setListener(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;)V
    .locals 0
    .param p1, "mListener"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;

    .prologue
    .line 204
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;

    .line 205
    return-void
.end method

.method public setMPController(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)V
    .locals 0
    .param p1, "mpController"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    .line 61
    return-void
.end method

.method public setViEffectSetting(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;)V
    .locals 0
    .param p1, "viEffectSetting"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViEffectSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    .line 65
    return-void
.end method

.method public setViewArray(Landroid/view/ViewGroup;Landroid/view/ViewGroup;)V
    .locals 1
    .param p1, "v1"    # Landroid/view/ViewGroup;
    .param p2, "v2"    # Landroid/view/ViewGroup;

    .prologue
    .line 208
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViewArray:Ljava/util/ArrayList;

    .line 209
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViewArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 210
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViewArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 211
    return-void
.end method

.method public setViewSize(III)V
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "offset"    # I

    .prologue
    .line 214
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mWidth:I

    .line 215
    iput p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mHeight:I

    .line 216
    int-to-float v0, p3

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mWidthOffset:F

    .line 217
    return-void
.end method

.method public setZ(F)V
    .locals 0
    .param p1, "z"    # F

    .prologue
    .line 220
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mDepthZ:F

    .line 221
    return-void
.end method

.method public turnLeft()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 354
    iget-boolean v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mIsRunningAni:Z

    if-nez v4, :cond_0

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mDataSize:I

    const/4 v5, 0x2

    if-ge v4, v5, :cond_1

    .line 392
    :cond_0
    :goto_0
    return-void

    .line 357
    :cond_1
    invoke-virtual {p0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->setIsRunningAni(Z)V

    .line 358
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollInAnim:Landroid/view/animation/Animation;

    if-eqz v4, :cond_2

    .line 359
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollInAnim:Landroid/view/animation/Animation;

    invoke-virtual {v4}, Landroid/view/animation/Animation;->cancel()V

    .line 360
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollInAnim:Landroid/view/animation/Animation;

    invoke-virtual {v4, v7}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 363
    :cond_2
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollOutAnim:Landroid/view/animation/Animation;

    if-eqz v4, :cond_3

    .line 364
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollOutAnim:Landroid/view/animation/Animation;

    invoke-virtual {v4}, Landroid/view/animation/Animation;->cancel()V

    .line 366
    :cond_3
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mMPDelayHandler:Landroid/os/Handler;

    if-eqz v4, :cond_4

    .line 367
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mMPDelayHandler:Landroid/os/Handler;

    invoke-virtual {v4, v7}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 369
    :cond_4
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViewIndex:I

    .line 370
    .local v1, "currentViewIndex":I
    add-int/lit8 v3, v1, 0x1

    .line 371
    .local v3, "nextViewIndex":I
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViewArray:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v3, v4, :cond_5

    .line 372
    const/4 v3, 0x0

    .line 375
    :cond_5
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mDataIndex:I

    .line 376
    .local v0, "currentDataIndex":I
    add-int/lit8 v2, v0, 0x1

    .line 377
    .local v2, "nextDataIndex":I
    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mDataSize:I

    if-lt v2, v4, :cond_6

    .line 378
    const/4 v2, 0x0

    .line 381
    :cond_6
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mDataIndex:I

    .line 382
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViEffectSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getCityChangeScrollAnimation()I

    move-result v4

    if-ne v4, v6, :cond_7

    .line 383
    iput v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViewIndex:I

    .line 384
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViewArray:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViewArray:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-direct {p0, v4, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->turnLeft(Landroid/view/View;Landroid/view/View;Ljava/lang/Boolean;)V

    goto :goto_0

    .line 386
    :cond_7
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;

    invoke-interface {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;->setOnPageChangeListener()V

    .line 387
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;

    invoke-interface {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;->setCurrentView()V

    .line 388
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->setMPState(I)V

    .line 389
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->startAfterFlick()V

    goto :goto_0
.end method

.method public turnRight()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 314
    iget-boolean v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mIsRunningAni:Z

    if-nez v4, :cond_0

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mDataSize:I

    const/4 v5, 0x2

    if-ge v4, v5, :cond_1

    .line 351
    :cond_0
    :goto_0
    return-void

    .line 317
    :cond_1
    invoke-virtual {p0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->setIsRunningAni(Z)V

    .line 318
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollInAnim:Landroid/view/animation/Animation;

    if-eqz v4, :cond_2

    .line 319
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollInAnim:Landroid/view/animation/Animation;

    invoke-virtual {v4}, Landroid/view/animation/Animation;->cancel()V

    .line 320
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollInAnim:Landroid/view/animation/Animation;

    invoke-virtual {v4, v7}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 323
    :cond_2
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollOutAnim:Landroid/view/animation/Animation;

    if-eqz v4, :cond_3

    .line 324
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mScrollOutAnim:Landroid/view/animation/Animation;

    invoke-virtual {v4}, Landroid/view/animation/Animation;->cancel()V

    .line 326
    :cond_3
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mMPDelayHandler:Landroid/os/Handler;

    if-eqz v4, :cond_4

    .line 327
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mMPDelayHandler:Landroid/os/Handler;

    invoke-virtual {v4, v7}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 329
    :cond_4
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViewIndex:I

    .line 330
    .local v1, "currentViewIndex":I
    add-int/lit8 v3, v1, -0x1

    .line 331
    .local v3, "preViewIndex":I
    if-gez v3, :cond_5

    .line 332
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViewArray:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v3, v4, -0x1

    .line 335
    :cond_5
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mDataIndex:I

    .line 336
    .local v0, "currentDataIndex":I
    add-int/lit8 v2, v0, -0x1

    .line 337
    .local v2, "preDataIndex":I
    if-gez v2, :cond_6

    .line 338
    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mDataSize:I

    add-int/lit8 v2, v4, -0x1

    .line 341
    :cond_6
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mDataIndex:I

    .line 342
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViEffectSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getCityChangeScrollAnimation()I

    move-result v4

    if-ne v4, v6, :cond_7

    .line 343
    iput v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViewIndex:I

    .line 344
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViewArray:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mViewArray:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    invoke-direct {p0, v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->turnRight(Landroid/view/View;Landroid/view/View;)V

    goto :goto_0

    .line 346
    :cond_7
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;

    invoke-interface {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;->setOnPageChangeListener()V

    .line 347
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;

    invoke-interface {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;->setCurrentView()V

    .line 348
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->setMPState(I)V

    .line 349
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->startAfterFlick()V

    goto :goto_0
.end method
