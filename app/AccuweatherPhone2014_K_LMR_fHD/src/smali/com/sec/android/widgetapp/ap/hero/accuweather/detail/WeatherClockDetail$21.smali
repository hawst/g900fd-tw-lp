.class Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;
.super Ljava/lang/Object;
.source "WeatherClockDetail.java"

# interfaces
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mLayoutInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V
    .locals 1
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 2980
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2981
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->mLayoutInflater:Landroid/view/LayoutInflater;

    return-void
.end method

.method private initView(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 3374
    if-nez p1, :cond_0

    .line 3375
    const-string v0, ""

    const-string v1, "initView v is n"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3433
    :goto_0
    return-void

    .line 3379
    :cond_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const v0, 0x7f0800a5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTopCityInfo:Landroid/widget/LinearLayout;
    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6502(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 3380
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const v0, 0x7f0800a6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityNameAndStateLayuout:Landroid/widget/LinearLayout;
    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$2602(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 3381
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const v0, 0x7f0800a7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityNameLayuout:Landroid/widget/LinearLayout;
    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6602(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 3382
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const v0, 0x7f0800a8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherCity:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$3802(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 3383
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const v0, 0x7f080075

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCurrentLocationIcon:Landroid/widget/ImageView;
    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4202(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 3384
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const v0, 0x7f0800aa

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityTimeAndDateLayout:Landroid/widget/LinearLayout;
    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6702(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 3385
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const v0, 0x7f0800ab

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherDate:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4102(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 3388
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const v0, 0x7f0800a9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCenterCityInfo:Landroid/widget/LinearLayout;
    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6802(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 3391
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const v0, 0x7f0800af

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityTempLayout:Landroid/widget/LinearLayout;
    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6902(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 3392
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const v0, 0x7f0800b0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherTemp:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4502(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 3393
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const v0, 0x7f08002c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherHighTemp:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4602(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 3394
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const v0, 0x7f08002d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherLowTemp:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4702(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 3395
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const v0, 0x7f0800b1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherTemper:Landroid/widget/ImageView;
    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4402(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 3396
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const v0, 0x7f0800ad

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherIconLayout:Landroid/widget/RelativeLayout;
    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4802(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/RelativeLayout;)Landroid/widget/RelativeLayout;

    .line 3397
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const v0, 0x7f0800ae

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherIcon:Landroid/widget/ImageView;
    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4902(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 3398
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const v0, 0x7f0800b2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityWeatherTextLayout:Landroid/widget/LinearLayout;
    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$7002(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 3399
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const v0, 0x7f0800b3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherText:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5202(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 3402
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const v0, 0x7f0800b4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mBottomCityInfo:Landroid/widget/LinearLayout;
    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$7102(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 3404
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const v0, 0x7f0800b5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityOtherInfoLayout:Landroid/widget/LinearLayout;
    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$7202(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 3406
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const v0, 0x7f0800b6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityOtherValueLayout:Landroid/widget/LinearLayout;
    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$7302(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 3407
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const v0, 0x7f0800b9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRealfeelValue:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5302(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 3408
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const v0, 0x7f0800b8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRealfeel:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$7402(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 3410
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const v0, 0x7f0800bb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionOneLineLayout:Landroid/widget/LinearLayout;
    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$3002(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 3411
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const v0, 0x7f0800be

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionTwoLineLayout:Landroid/widget/LinearLayout;
    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$3102(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 3413
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionOneLineLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$3000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 3414
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionTwoLineLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$3100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/LinearLayout;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 3415
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionTwoLineLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$3100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 3418
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const v0, 0x7f0800bc

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionOneLine:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$2902(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 3419
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const v0, 0x7f0800bd

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionOneLineValue:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5402(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 3421
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const v0, 0x7f0800bf

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionTwoLine:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5502(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 3422
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const v0, 0x7f0800c0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionTwoLineValue:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5602(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 3424
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const v0, 0x7f0800c3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUVIndexValue:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5902(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 3425
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const v0, 0x7f0800c2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUVIndex:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5802(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 3426
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const v0, 0x7f0800c5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSunState:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6002(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 3427
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const v0, 0x7f0800c6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTimeValue:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6102(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 3429
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->isIncludeWeeklyInfoInViewPager:Z
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3430
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->initWeatherClockDetailView(Landroid/view/View;)V
    invoke-static {v0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$7500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/view/View;)V

    .line 3432
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setStrokeAndShadow()V

    goto/16 :goto_0
.end method


# virtual methods
.method public clearView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3436
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTopCityInfo:Landroid/widget/LinearLayout;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6502(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 3437
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityNameAndStateLayuout:Landroid/widget/LinearLayout;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$2602(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 3438
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityNameLayuout:Landroid/widget/LinearLayout;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6602(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 3439
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherCity:Landroid/widget/TextView;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$3802(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 3440
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCurrentLocationIcon:Landroid/widget/ImageView;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4202(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 3442
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCenterCityInfo:Landroid/widget/LinearLayout;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6802(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 3443
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityTempLayout:Landroid/widget/LinearLayout;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6902(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 3444
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherTemp:Landroid/widget/TextView;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4502(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 3445
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherHighTemp:Landroid/widget/TextView;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4602(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 3446
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherLowTemp:Landroid/widget/TextView;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4702(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 3447
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherTemper:Landroid/widget/ImageView;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4402(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 3448
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherIconLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4802(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/RelativeLayout;)Landroid/widget/RelativeLayout;

    .line 3449
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherIcon:Landroid/widget/ImageView;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4902(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 3451
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityWeatherTextLayout:Landroid/widget/LinearLayout;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$7002(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 3452
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherText:Landroid/widget/TextView;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5202(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 3454
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mBottomCityInfo:Landroid/widget/LinearLayout;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$7102(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 3455
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityOtherInfoLayout:Landroid/widget/LinearLayout;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$7202(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 3456
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityOtherValueLayout:Landroid/widget/LinearLayout;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$7302(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 3457
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRealfeelValue:Landroid/widget/TextView;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5302(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 3458
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRealfeel:Landroid/widget/TextView;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$7402(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 3460
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionOneLineLayout:Landroid/widget/LinearLayout;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$3002(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 3461
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionTwoLineLayout:Landroid/widget/LinearLayout;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$3102(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 3463
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionOneLine:Landroid/widget/TextView;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$2902(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 3464
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionOneLineValue:Landroid/widget/TextView;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5402(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 3466
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionTwoLine:Landroid/widget/TextView;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5502(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 3467
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionTwoLineValue:Landroid/widget/TextView;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5602(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 3469
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUVIndexValue:Landroid/widget/TextView;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5902(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 3470
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUVIndex:Landroid/widget/TextView;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5802(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 3471
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSunState:Landroid/widget/TextView;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6002(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 3472
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTimeValue:Landroid/widget/TextView;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6102(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 3473
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 3475
    return-void
.end method

.method public initView()Landroid/view/View;
    .locals 7

    .prologue
    const v6, 0x7f0800c9

    .line 3006
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->mLayoutInflater:Landroid/view/LayoutInflater;

    if-nez v3, :cond_0

    .line 3007
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 3009
    :cond_0
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v4, 0x7f03002b

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 3012
    .local v0, "layout":Landroid/widget/RelativeLayout;
    const v3, 0x7f080090

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 3014
    .local v2, "weeklyLayout":Landroid/view/View;
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v1, v3, Landroid/content/res/Configuration;->orientation:I

    .line 3016
    .local v1, "orientation":I
    const/4 v3, 0x1

    if-ne v1, v3, :cond_1

    .line 3017
    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 3022
    :goto_0
    return-object v0

    .line 3019
    :cond_1
    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public onFinshAnimation()V
    .locals 2

    .prologue
    .line 2990
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->getNextView()Landroid/view/ViewGroup;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2991
    return-void
.end method

.method public onStartAnimation()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2983
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->resetTouchTimer()V
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    .line 2984
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setTouchTimer(I)V
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;I)V

    .line 2985
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->getCurrentView()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2986
    return-void
.end method

.method public setContentCubeViewPager(ILandroid/view/ViewGroup;)V
    .locals 20
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/ViewGroup;

    .prologue
    .line 3034
    const-string v13, ""

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "setContentCubeViewPager mSelLoc = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;
    invoke-static {v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$1100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "/"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3035
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityList:Ljava/util/ArrayList;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$1000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Ljava/util/ArrayList;

    move-result-object v13

    if-nez v13, :cond_1

    .line 3036
    const-string v13, ""

    const-string v14, "sCCVP city list is n"

    invoke-static {v13, v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3217
    :cond_0
    :goto_0
    return-void

    .line 3041
    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityList:Ljava/util/ArrayList;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$1000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Ljava/util/ArrayList;

    move-result-object v13

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-lez v13, :cond_2

    .line 3042
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherCity:Landroid/widget/TextView;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$3800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityList:Ljava/util/ArrayList;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$1000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Ljava/util/ArrayList;

    move-result-object v13

    move/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getCityName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v14, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3045
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityNameAndStateLayuout:Landroid/widget/LinearLayout;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$2600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/LinearLayout;

    move-result-object v13

    new-instance v14, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21$1;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;)V

    invoke-virtual {v13, v14}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3058
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityNameAndStateLayuout:Landroid/widget/LinearLayout;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$2600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/LinearLayout;

    move-result-object v13

    new-instance v14, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21$2;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;)V

    invoke-virtual {v13, v14}, Landroid/widget/LinearLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 3068
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .line 3069
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityList:Ljava/util/ArrayList;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$1000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Ljava/util/ArrayList;

    move-result-object v13

    move/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v13

    .line 3068
    invoke-static {v14, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getDetailWeatherInfo(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    move-result-object v5

    .line 3070
    .local v5, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    const/4 v11, 0x0

    .line 3072
    .local v11, "today_Url_Info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    if-eqz v5, :cond_3

    .line 3073
    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v11

    .line 3075
    if-nez v11, :cond_4

    .line 3076
    const-string v13, ""

    const-string v14, "updateCubeViewPager TodayWeatherInfo is null"

    invoke-static {v13, v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3077
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->finish()V

    goto/16 :goto_0

    .line 3081
    :cond_3
    const-string v13, ""

    const-string v14, "updateCubeViewPager DetailWeatherInfo is null"

    invoke-static {v13, v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3082
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->finish()V

    goto/16 :goto_0

    .line 3087
    :cond_4
    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v10

    .line 3088
    .local v10, "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v14

    .line 3089
    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v15

    .line 3088
    invoke-static {v13, v14, v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkDayOrNight(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    .line 3091
    .local v6, "isDay":Z
    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_9

    .line 3092
    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v13

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTz:Ljava/util/TimeZone;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$3902(Ljava/util/TimeZone;)Ljava/util/TimeZone;

    .line 3097
    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->cal_b:Ljava/util/Calendar;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Ljava/util/Calendar;

    move-result-object v13

    if-nez v13, :cond_5

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v14

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->cal_b:Ljava/util/Calendar;
    invoke-static {v13, v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4002(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Ljava/util/Calendar;)Ljava/util/Calendar;

    .line 3098
    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->cal_b:Ljava/util/Calendar;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Ljava/util/Calendar;

    move-result-object v13

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTz:Ljava/util/TimeZone;
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$3900()Ljava/util/TimeZone;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 3099
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->cal_b:Ljava/util/Calendar;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Ljava/util/Calendar;

    move-result-object v13

    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getUpdateDate()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    invoke-virtual {v13, v14, v15}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 3101
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherDate:Landroid/widget/TextView;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->cal_b:Ljava/util/Calendar;
    invoke-static {v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Ljava/util/Calendar;

    move-result-object v15

    const/16 v16, 0x1

    const/16 v17, 0x0

    invoke-static/range {v14 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->dateFormat(Landroid/content/Context;Ljava/util/Calendar;ZZ)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3108
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityList:Ljava/util/ArrayList;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$1000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Ljava/util/ArrayList;

    move-result-object v13

    move/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v13

    const-string v14, "cityId:current"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_a

    .line 3109
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCurrentLocationIcon:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/ImageView;

    move-result-object v13

    if-eqz v13, :cond_6

    .line 3110
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCurrentLocationIcon:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/ImageView;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3121
    :cond_6
    :goto_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)I

    move-result v13

    const/4 v14, 0x1

    if-ne v13, v14, :cond_b

    .line 3123
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherTemper:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/ImageView;

    move-result-object v13

    const v14, 0x7f0200d0

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 3129
    :goto_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherTemp:Landroid/widget/TextView;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v13

    const-string v14, "%d"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)I

    move-result v18

    .line 3130
    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getCurrentTemp()F

    move-result v19

    .line 3129
    invoke-static/range {v17 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3132
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherHighTemp:Landroid/widget/TextView;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v13

    const-string v14, "%d"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    move-object/from16 v18, v0

    .line 3133
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)I

    move-result v18

    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getHighTemp()F

    move-result v19

    .line 3132
    invoke-static/range {v17 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3134
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherLowTemp:Landroid/widget/TextView;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4700(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v13

    const-string v14, "%d"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    move-object/from16 v18, v0

    .line 3135
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)I

    move-result v18

    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getLowTemp()F

    move-result v19

    .line 3134
    invoke-static/range {v17 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3137
    sget-object v13, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->SamsungNumber3L:Landroid/graphics/Typeface;

    if-eqz v13, :cond_7

    .line 3138
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherTemp:Landroid/widget/TextView;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v13

    sget-object v14, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->SamsungNumber3L:Landroid/graphics/Typeface;

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 3139
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherHighTemp:Landroid/widget/TextView;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v13

    sget-object v14, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->SamsungNumber3L:Landroid/graphics/Typeface;

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 3140
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherLowTemp:Landroid/widget/TextView;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4700(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v13

    sget-object v14, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->SamsungNumber3L:Landroid/graphics/Typeface;

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 3143
    :cond_7
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    move-result-object v13

    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getIconAnimation()I

    move-result v13

    if-nez v13, :cond_c

    .line 3144
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherIconLayout:Landroid/widget/RelativeLayout;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/RelativeLayout;

    move-result-object v13

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 3145
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherIcon:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4900(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/ImageView;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3146
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherIcon:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4900(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/ImageView;

    move-result-object v13

    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getIconNum()I

    move-result v14

    const/4 v15, 0x1

    invoke-static {v14, v15, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setTodayWeatherIcon(IIZ)I

    move-result v14

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 3168
    :goto_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherText:Landroid/widget/TextView;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v13

    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getWeatherText()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3171
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRealfeelValue:Landroid/widget/TextView;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v13

    const-string v14, "%d"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    move-object/from16 v18, v0

    .line 3172
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I
    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)I

    move-result v18

    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getRealFeel()F

    move-result v19

    .line 3171
    invoke-static/range {v17 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3180
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const/4 v14, 0x0

    invoke-static {v13, v10, v14, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setPrecipitaionString(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;Z)Ljava/lang/String;

    move-result-object v8

    .line 3181
    .local v8, "precipitaionString":Ljava/lang/String;
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "%d"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v10, v1, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setPrecipitaionValue(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;Z)I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "%"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 3183
    .local v9, "precipitaionValue":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionOneLine:Landroid/widget/TextView;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$2900(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v13

    invoke-virtual {v13, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3184
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionOneLineValue:Landroid/widget/TextView;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v13

    invoke-virtual {v13, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3186
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionTwoLine:Landroid/widget/TextView;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v13

    if-eqz v13, :cond_8

    .line 3187
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionTwoLine:Landroid/widget/TextView;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v13

    invoke-virtual {v13, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3188
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionTwoLineValue:Landroid/widget/TextView;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v13

    invoke-virtual {v13, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3190
    :cond_8
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setVisibilePrecipitationLayout()V
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5700(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    .line 3192
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUVIndex:Landroid/widget/TextView;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-static {v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getUVIndexText(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3193
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUVIndexValue:Landroid/widget/TextView;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5900(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v13

    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getUVIndexText()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3194
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getBaseContext()Landroid/content/Context;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v13

    iget-object v13, v13, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v13}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v7

    .line 3196
    .local v7, "locale":Ljava/lang/String;
    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkSunriseOrSunset(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 3198
    .local v3, "checkSun":I
    const/4 v13, 0x2

    if-ne v3, v13, :cond_f

    .line 3199
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSunState:Landroid/widget/TextView;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v13

    const v14, 0x7f0d00b7

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(I)V

    .line 3200
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTimeValue:Landroid/widget/TextView;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .line 3201
    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v15

    .line 3200
    invoke-static {v14, v15, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getSunRiseAndSunSetByFormat(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3212
    :goto_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->isIncludeWeeklyInfoInViewPager:Z
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 3213
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityList:Ljava/util/ArrayList;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$1000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Ljava/util/ArrayList;

    move-result-object v13

    move/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;
    invoke-static {v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$1100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 3214
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-virtual {v13, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setUiDeatilView(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)V

    goto/16 :goto_0

    .line 3094
    .end local v3    # "checkSun":I
    .end local v7    # "locale":Ljava/lang/String;
    .end local v8    # "precipitaionString":Ljava/lang/String;
    .end local v9    # "precipitaionValue":Ljava/lang/String;
    :cond_9
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v13

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTz:Ljava/util/TimeZone;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$3902(Ljava/util/TimeZone;)Ljava/util/TimeZone;

    goto/16 :goto_1

    .line 3113
    :cond_a
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCurrentLocationIcon:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/ImageView;

    move-result-object v13

    if-eqz v13, :cond_6

    .line 3114
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCurrentLocationIcon:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/ImageView;

    move-result-object v13

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    .line 3126
    :cond_b
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherTemper:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/ImageView;

    move-result-object v13

    const v14, 0x7f0200d1

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_3

    .line 3149
    :cond_c
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherIconLayout:Landroid/widget/RelativeLayout;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/RelativeLayout;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 3150
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherIcon:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4900(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/ImageView;

    move-result-object v13

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3152
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0b0009

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v13

    invoke-static {v13}, Ljava/lang/Math;->round(F)I

    move-result v12

    .line 3154
    .local v12, "width":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0b000a

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v13

    invoke-static {v13}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 3156
    .local v4, "height":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIconManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;

    move-result-object v13

    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getIconNum()I

    move-result v14

    invoke-virtual {v13, v14, v6, v12, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->getIconAnimation(IZII)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 3158
    .local v2, "ani":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherIconLayout:Landroid/widget/RelativeLayout;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/RelativeLayout;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/RelativeLayout;->getChildCount()I

    move-result v13

    if-lez v13, :cond_e

    .line 3159
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIconManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherIconLayout:Landroid/widget/RelativeLayout;
    invoke-static {v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/RelativeLayout;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->cancelIconAnimation(Landroid/view/ViewGroup;)V

    .line 3160
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIconStopHanler:Landroid/os/Handler;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/os/Handler;

    move-result-object v13

    if-eqz v13, :cond_d

    .line 3161
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIconStopHanler:Landroid/os/Handler;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/os/Handler;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 3163
    :cond_d
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherIconLayout:Landroid/widget/RelativeLayout;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/RelativeLayout;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 3165
    :cond_e
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherIconLayout:Landroid/widget/RelativeLayout;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/RelativeLayout;

    move-result-object v13

    invoke-virtual {v13, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_4

    .line 3202
    .end local v2    # "ani":Landroid/view/View;
    .end local v4    # "height":I
    .end local v12    # "width":I
    .restart local v3    # "checkSun":I
    .restart local v7    # "locale":Ljava/lang/String;
    .restart local v8    # "precipitaionString":Ljava/lang/String;
    .restart local v9    # "precipitaionValue":Ljava/lang/String;
    :cond_f
    const/4 v13, 0x3

    if-ne v3, v13, :cond_10

    .line 3203
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSunState:Landroid/widget/TextView;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v13

    const v14, 0x7f0d00b6

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(I)V

    .line 3204
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTimeValue:Landroid/widget/TextView;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .line 3205
    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v15

    .line 3204
    invoke-static {v14, v15, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getSunRiseAndSunSetByFormat(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 3207
    :cond_10
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSunState:Landroid/widget/TextView;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v13

    const v14, 0x7f0d00b6

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(I)V

    .line 3208
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTimeValue:Landroid/widget/TextView;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .line 3209
    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v15

    .line 3208
    invoke-static {v14, v15, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getSunRiseAndSunSetByFormat(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5
.end method

.method public setContentSearchModeCubeViewPager(ILandroid/view/ViewGroup;)V
    .locals 16
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/ViewGroup;

    .prologue
    .line 3220
    const-string v9, ""

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "setContentSearchModeCubeViewPager mSelLoc = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;
    invoke-static {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$1100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, p1

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3223
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchCityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    move-result-object v9

    if-eqz v9, :cond_0

    .line 3224
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherCity:Landroid/widget/TextView;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$3800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchCityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    invoke-static {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getCity()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3226
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityNameAndStateLayuout:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$2600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/LinearLayout;

    move-result-object v9

    new-instance v10, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21$3;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21$3;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;)V

    invoke-virtual {v9, v10}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3241
    :cond_0
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    move-result-object v9

    if-nez v9, :cond_2

    .line 3242
    const-string v9, ""

    const-string v10, "updateCubeViewPager searchTodayInfo is null"

    invoke-static {v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3243
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-virtual {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->finish()V

    .line 3371
    :cond_1
    :goto_0
    return-void

    .line 3248
    :cond_2
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .line 3249
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;
    invoke-static {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;
    invoke-static {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v11

    .line 3248
    invoke-static {v9, v10, v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkDayOrNight(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    .line 3251
    .local v4, "isDay":Z
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_7

    .line 3252
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v9

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTz:Ljava/util/TimeZone;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$3902(Ljava/util/TimeZone;)Ljava/util/TimeZone;

    .line 3257
    :goto_1
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->cal_b:Ljava/util/Calendar;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Ljava/util/Calendar;

    move-result-object v9

    if-nez v9, :cond_3

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v10

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->cal_b:Ljava/util/Calendar;
    invoke-static {v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4002(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Ljava/util/Calendar;)Ljava/util/Calendar;

    .line 3258
    :cond_3
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->cal_b:Ljava/util/Calendar;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Ljava/util/Calendar;

    move-result-object v9

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTz:Ljava/util/TimeZone;
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$3900()Ljava/util/TimeZone;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 3259
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->cal_b:Ljava/util/Calendar;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Ljava/util/Calendar;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;
    invoke-static {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getUpdateDate()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 3261
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherDate:Landroid/widget/TextView;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->cal_b:Ljava/util/Calendar;
    invoke-static {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Ljava/util/Calendar;

    move-result-object v11

    const/4 v12, 0x1

    const/4 v13, 0x0

    invoke-static {v10, v11, v12, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->dateFormat(Landroid/content/Context;Ljava/util/Calendar;ZZ)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3268
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchCityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v9

    const-string v10, "cityId:current"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 3269
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCurrentLocationIcon:Landroid/widget/ImageView;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/ImageView;

    move-result-object v9

    if-eqz v9, :cond_4

    .line 3270
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCurrentLocationIcon:Landroid/widget/ImageView;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/ImageView;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3279
    :cond_4
    :goto_2
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-static {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getTempScaleSetting(Landroid/content/Context;)I

    move-result v10

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I
    invoke-static {v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4302(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;I)I

    .line 3281
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_9

    .line 3283
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherTemper:Landroid/widget/ImageView;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/ImageView;

    move-result-object v9

    const v10, 0x7f0200d0

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 3289
    :goto_3
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherTemp:Landroid/widget/TextView;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v9

    const-string v10, "%d"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    move-result-object v13

    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTempScale()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .line 3290
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I
    invoke-static {v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)I

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;
    invoke-static {v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    move-result-object v15

    invoke-virtual {v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getCurrentTemp()F

    move-result v15

    .line 3289
    invoke-static {v13, v14, v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3292
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherHighTemp:Landroid/widget/TextView;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v9

    const-string v10, "%d"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .line 3293
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    move-result-object v13

    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTempScale()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I
    invoke-static {v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)I

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;
    invoke-static {v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    move-result-object v15

    invoke-virtual {v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getHighTemp()F

    move-result v15

    .line 3292
    invoke-static {v13, v14, v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3294
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherLowTemp:Landroid/widget/TextView;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4700(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v9

    const-string v10, "%d"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .line 3295
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    move-result-object v13

    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTempScale()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I
    invoke-static {v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)I

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;
    invoke-static {v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    move-result-object v15

    invoke-virtual {v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getLowTemp()F

    move-result v15

    .line 3294
    invoke-static {v13, v14, v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3297
    sget-object v9, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->SamsungNumber3L:Landroid/graphics/Typeface;

    if-eqz v9, :cond_5

    .line 3298
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherTemp:Landroid/widget/TextView;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v9

    sget-object v10, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->SamsungNumber3L:Landroid/graphics/Typeface;

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 3299
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherHighTemp:Landroid/widget/TextView;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v9

    sget-object v10, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->SamsungNumber3L:Landroid/graphics/Typeface;

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 3300
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherLowTemp:Landroid/widget/TextView;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4700(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v9

    sget-object v10, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->SamsungNumber3L:Landroid/graphics/Typeface;

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 3303
    :cond_5
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getIconAnimation()I

    move-result v9

    if-nez v9, :cond_a

    .line 3304
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherIconLayout:Landroid/widget/RelativeLayout;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/RelativeLayout;

    move-result-object v9

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 3305
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherIcon:Landroid/widget/ImageView;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4900(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/ImageView;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3306
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherIcon:Landroid/widget/ImageView;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4900(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/ImageView;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .line 3307
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;
    invoke-static {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getIconNum()I

    move-result v10

    const/4 v11, 0x1

    .line 3306
    invoke-static {v10, v11, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setTodayWeatherIcon(IIZ)I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 3329
    :goto_4
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherText:Landroid/widget/TextView;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;
    invoke-static {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getWeatherText()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3332
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRealfeelValue:Landroid/widget/TextView;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v9

    const-string v10, "%d"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    move-result-object v13

    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTempScale()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .line 3333
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I
    invoke-static {v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)I

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;
    invoke-static {v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    move-result-object v15

    invoke-virtual {v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getRealFeel()F

    move-result v15

    .line 3332
    invoke-static {v13, v14, v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3335
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;
    invoke-static {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    move-result-object v11

    invoke-static {v9, v10, v11, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setPrecipitaionString(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;Z)Ljava/lang/String;

    move-result-object v6

    .line 3336
    .local v6, "precipitaionString":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "%d"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;
    invoke-static {v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    move-result-object v15

    invoke-static {v13, v14, v15, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setPrecipitaionValue(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;Z)I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "%"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 3338
    .local v7, "precipitaionValue":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionOneLine:Landroid/widget/TextView;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$2900(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v9

    invoke-virtual {v9, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3339
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionOneLineValue:Landroid/widget/TextView;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v9

    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3341
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionTwoLine:Landroid/widget/TextView;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v9

    if-eqz v9, :cond_6

    .line 3342
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionTwoLine:Landroid/widget/TextView;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v9

    invoke-virtual {v9, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3343
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionTwoLineValue:Landroid/widget/TextView;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v9

    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3345
    :cond_6
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setVisibilePrecipitationLayout()V
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5700(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    .line 3347
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUVIndex:Landroid/widget/TextView;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-static {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getUVIndexText(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3348
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUVIndexValue:Landroid/widget/TextView;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5900(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;
    invoke-static {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getUVIndexText()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3349
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-virtual {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getBaseContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v9

    iget-object v9, v9, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v9}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    .line 3351
    .local v5, "locale":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;
    invoke-static {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .line 3352
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;
    invoke-static {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v11

    .line 3351
    invoke-static {v9, v10, v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkSunriseOrSunset(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 3354
    .local v2, "checkSun":I
    const/4 v9, 0x2

    if-ne v2, v9, :cond_d

    .line 3355
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSunState:Landroid/widget/TextView;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v9

    const v10, 0x7f0d00b7

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(I)V

    .line 3356
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTimeValue:Landroid/widget/TextView;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .line 3357
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;
    invoke-static {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v11

    .line 3356
    invoke-static {v10, v11, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getSunRiseAndSunSetByFormat(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3368
    :goto_5
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->isIncludeWeeklyInfoInViewPager:Z
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 3369
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-virtual {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setUiSearchDeatilView()V

    goto/16 :goto_0

    .line 3254
    .end local v2    # "checkSun":I
    .end local v5    # "locale":Ljava/lang/String;
    .end local v6    # "precipitaionString":Ljava/lang/String;
    .end local v7    # "precipitaionValue":Ljava/lang/String;
    :cond_7
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v9

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTz:Ljava/util/TimeZone;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$3902(Ljava/util/TimeZone;)Ljava/util/TimeZone;

    goto/16 :goto_1

    .line 3273
    :cond_8
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCurrentLocationIcon:Landroid/widget/ImageView;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/ImageView;

    move-result-object v9

    if-eqz v9, :cond_4

    .line 3274
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCurrentLocationIcon:Landroid/widget/ImageView;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/ImageView;

    move-result-object v9

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    .line 3286
    :cond_9
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherTemper:Landroid/widget/ImageView;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/ImageView;

    move-result-object v9

    const v10, 0x7f0200d1

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_3

    .line 3309
    :cond_a
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherIconLayout:Landroid/widget/RelativeLayout;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/RelativeLayout;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 3310
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherIcon:Landroid/widget/ImageView;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4900(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/ImageView;

    move-result-object v9

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3312
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-virtual {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0b0009

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v9

    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    move-result v8

    .line 3314
    .local v8, "width":I
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-virtual {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0b000a

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v9

    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 3317
    .local v3, "height":I
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIconManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .line 3318
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;
    invoke-static {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getIconNum()I

    move-result v10

    invoke-virtual {v9, v10, v4, v8, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->getIconAnimation(IZII)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 3319
    .local v1, "ani":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherIconLayout:Landroid/widget/RelativeLayout;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/RelativeLayout;

    move-result-object v9

    invoke-virtual {v9}, Landroid/widget/RelativeLayout;->getChildCount()I

    move-result v9

    if-lez v9, :cond_c

    .line 3320
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIconManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherIconLayout:Landroid/widget/RelativeLayout;
    invoke-static {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/RelativeLayout;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->cancelIconAnimation(Landroid/view/ViewGroup;)V

    .line 3321
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIconStopHanler:Landroid/os/Handler;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/os/Handler;

    move-result-object v9

    if-eqz v9, :cond_b

    .line 3322
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIconStopHanler:Landroid/os/Handler;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$5100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/os/Handler;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 3324
    :cond_b
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherIconLayout:Landroid/widget/RelativeLayout;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/RelativeLayout;

    move-result-object v9

    invoke-virtual {v9}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 3326
    :cond_c
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherIconLayout:Landroid/widget/RelativeLayout;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/RelativeLayout;

    move-result-object v9

    invoke-virtual {v9, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_4

    .line 3358
    .end local v1    # "ani":Landroid/view/View;
    .end local v3    # "height":I
    .end local v8    # "width":I
    .restart local v2    # "checkSun":I
    .restart local v5    # "locale":Ljava/lang/String;
    .restart local v6    # "precipitaionString":Ljava/lang/String;
    .restart local v7    # "precipitaionValue":Ljava/lang/String;
    :cond_d
    const/4 v9, 0x3

    if-ne v2, v9, :cond_e

    .line 3359
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSunState:Landroid/widget/TextView;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v9

    const v10, 0x7f0d00b6

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(I)V

    .line 3360
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTimeValue:Landroid/widget/TextView;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .line 3361
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;
    invoke-static {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v11

    .line 3360
    invoke-static {v10, v11, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getSunRiseAndSunSetByFormat(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 3363
    :cond_e
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSunState:Landroid/widget/TextView;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v9

    const v10, 0x7f0d00b6

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(I)V

    .line 3364
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTimeValue:Landroid/widget/TextView;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .line 3365
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;
    invoke-static {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$6400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v11

    .line 3364
    invoke-static {v10, v11, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getSunRiseAndSunSetByFormat(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5
.end method

.method public setCurrentView()V
    .locals 2

    .prologue
    .line 2994
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->getCurrentView()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->initView(Landroid/view/View;)V

    .line 2995
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getSearchMode()Z
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$1500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2996
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->getCurrentDataIndex()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .line 2997
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->getCurrentView()Landroid/view/ViewGroup;

    move-result-object v1

    .line 2996
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->setContentSearchModeCubeViewPager(ILandroid/view/ViewGroup;)V

    .line 3002
    :goto_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIsChangeOrientation:Z

    .line 3003
    return-void

    .line 2999
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->getCurrentDataIndex()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .line 3000
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->getCurrentView()Landroid/view/ViewGroup;

    move-result-object v1

    .line 2999
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->setContentCubeViewPager(ILandroid/view/ViewGroup;)V

    goto :goto_0
.end method

.method public setOnPageChangeListener()V
    .locals 3

    .prologue
    .line 3026
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->getCurrentDataIndex()I

    move-result v0

    .line 3027
    .local v0, "position":I
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$1000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$1100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3028
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$1000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;
    invoke-static {v2, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$1102(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Ljava/lang/String;)Ljava/lang/String;

    .line 3031
    :cond_0
    return-void
.end method
