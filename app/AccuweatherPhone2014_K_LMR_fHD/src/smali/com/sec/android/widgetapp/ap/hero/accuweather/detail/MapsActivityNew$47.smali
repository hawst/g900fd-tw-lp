.class Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$47;
.super Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;
.source "MapsActivityNew.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->makeDetail(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 1898
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$47;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(IILjava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "position"    # I
    .param p2, "responseCode"    # I
    .param p3, "responseStatus"    # Ljava/lang/String;
    .param p4, "responseBody"    # Ljava/lang/String;

    .prologue
    .line 1901
    invoke-super {p0, p1, p2, p3, p4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;->onReceive(IILjava/lang/String;Ljava/lang/String;)V

    .line 1903
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 1904
    .local v1, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1905
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "RESPONSE_CODE"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1906
    const-string v2, "RESPONSE_BODY"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1907
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1909
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$47;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    iget-object v2, v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->detailHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1910
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$47;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-virtual {v2, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->deleteMe(I)V

    .line 1911
    return-void
.end method
