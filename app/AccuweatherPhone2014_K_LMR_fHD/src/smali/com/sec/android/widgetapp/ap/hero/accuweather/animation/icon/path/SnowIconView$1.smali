.class Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView$1;
.super Ljava/lang/Object;
.source "SnowIconView.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;

    .prologue
    .line 655
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 662
    invoke-virtual {p1}, Landroid/animation/Animator;->removeAllListeners()V

    .line 663
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 658
    invoke-virtual {p1}, Landroid/animation/Animator;->removeAllListeners()V

    .line 659
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 5
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    const/16 v4, 0x21

    const/16 v3, 0x11

    const/16 v2, 0x22

    .line 666
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mAnimators:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;)Ljava/util/HashMap;

    move-result-object v0

    if-nez v0, :cond_1

    .line 684
    :cond_0
    :goto_0
    return-void

    .line 669
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mAnimators:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 670
    const-wide/16 v0, 0x5dc

    invoke-virtual {p1, v0, v1}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 672
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->isStop:Z

    if-eqz v0, :cond_0

    .line 673
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mAnimators:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 674
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mAnimators:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 675
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;

    const-string v1, "e_"

    iput-object v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPreFixProperty1:Ljava/lang/String;

    goto :goto_0

    .line 676
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mAnimators:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 677
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mAnimators:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    goto :goto_0

    .line 678
    :cond_4
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mAnimators:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 679
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mAnimators:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    goto/16 :goto_0

    .line 680
    :cond_5
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->TranslateAni:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 681
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->TranslateAni:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    goto/16 :goto_0
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 656
    return-void
.end method
