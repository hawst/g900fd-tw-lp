.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
.super Ljava/lang/Object;
.source "GeneralWeatherInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mConverterWeatehrIconNum:I

.field mDayHailAmount:Ljava/lang/Double;

.field mDayHailProbability:I

.field mDayPrecipitationAmount:Ljava/lang/Double;

.field mDayPrecipitationProbability:I

.field mDayRainAmount:Ljava/lang/Double;

.field mDayRainProbability:I

.field mDaySnowAmount:Ljava/lang/Double;

.field mDaySnowProbability:I

.field mHighTemp:F

.field mIconNum:I

.field mLowTemp:F

.field mNightHailAmount:Ljava/lang/Double;

.field mNightHailProbability:I

.field mNightPrecipitationAmount:Ljava/lang/Double;

.field mNightPrecipitationProbability:I

.field mNightRainAmount:Ljava/lang/Double;

.field mNightRainProbability:I

.field mNightSnowAmount:Ljava/lang/Double;

.field mNightSnowProbability:I

.field mOnedayDayIcon:I

.field mOnedayNightIcon:I

.field mSummerTime:Ljava/lang/String;

.field private mSunRiseTime:Ljava/lang/String;

.field private mSunSetTime:Ljava/lang/String;

.field mTempScale:I

.field mTimestamp:Ljava/lang/String;

.field mUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 400
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo$1;

    invoke-direct {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo$1;-><init>()V

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    return-void
.end method

.method public constructor <init>(IFFILjava/lang/String;)V
    .locals 0
    .param p1, "tempScale"    # I
    .param p2, "lowTemp"    # F
    .param p3, "highTemp"    # F
    .param p4, "iconNum"    # I
    .param p5, "url"    # Ljava/lang/String;

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mTempScale:I

    .line 72
    iput p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mLowTemp:F

    .line 73
    iput p3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mHighTemp:F

    .line 74
    iput p4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mIconNum:I

    .line 75
    iput-object p5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mUrl:Ljava/lang/String;

    .line 76
    return-void
.end method

.method public constructor <init>(IFFILjava/lang/String;IIIILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "tempScale"    # I
    .param p2, "lowTemp"    # F
    .param p3, "highTemp"    # F
    .param p4, "iconNum"    # I
    .param p5, "url"    # Ljava/lang/String;
    .param p6, "rainProb"    # I
    .param p7, "snowProb"    # I
    .param p8, "hailProb"    # I
    .param p9, "precipitation"    # I
    .param p10, "sunRise"    # Ljava/lang/String;
    .param p11, "sunSet"    # Ljava/lang/String;

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mTempScale:I

    .line 91
    iput p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mLowTemp:F

    .line 92
    iput p3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mHighTemp:F

    .line 93
    iput p4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mIconNum:I

    .line 94
    iput-object p5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mUrl:Ljava/lang/String;

    .line 96
    iput p6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mDayRainProbability:I

    .line 97
    iput p7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mDaySnowProbability:I

    .line 98
    iput p8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mDayHailProbability:I

    .line 99
    iput p9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mDayPrecipitationProbability:I

    .line 100
    iput-object p10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mSunRiseTime:Ljava/lang/String;

    .line 101
    iput-object p11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mSunSetTime:Ljava/lang/String;

    .line 102
    return-void
.end method

.method public constructor <init>(IFFILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "tempScale"    # I
    .param p2, "lowTemp"    # F
    .param p3, "highTemp"    # F
    .param p4, "iconNum"    # I
    .param p5, "url"    # Ljava/lang/String;
    .param p6, "summertime"    # Ljava/lang/String;

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mTempScale:I

    .line 81
    iput p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mLowTemp:F

    .line 82
    iput p3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mHighTemp:F

    .line 83
    iput p4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mIconNum:I

    .line 84
    iput-object p5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mUrl:Ljava/lang/String;

    .line 85
    iput-object p6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mSummerTime:Ljava/lang/String;

    .line 86
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 369
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 370
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mTimestamp:Ljava/lang/String;

    .line 371
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mTempScale:I

    .line 372
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mIconNum:I

    .line 373
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mHighTemp:F

    .line 374
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mLowTemp:F

    .line 375
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mUrl:Ljava/lang/String;

    .line 376
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mSummerTime:Ljava/lang/String;

    .line 378
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mConverterWeatehrIconNum:I

    .line 380
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mDayRainProbability:I

    .line 381
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mDaySnowProbability:I

    .line 382
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mDayHailProbability:I

    .line 383
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mDayPrecipitationProbability:I

    .line 384
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mDayRainAmount:Ljava/lang/Double;

    .line 385
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mDaySnowAmount:Ljava/lang/Double;

    .line 386
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mDayHailAmount:Ljava/lang/Double;

    .line 387
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mDayPrecipitationAmount:Ljava/lang/Double;

    .line 388
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mNightRainProbability:I

    .line 389
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mNightSnowProbability:I

    .line 390
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mNightHailProbability:I

    .line 391
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mNightPrecipitationProbability:I

    .line 392
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mNightRainAmount:Ljava/lang/Double;

    .line 393
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mNightSnowAmount:Ljava/lang/Double;

    .line 394
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mNightHailAmount:Ljava/lang/Double;

    .line 395
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mNightPrecipitationAmount:Ljava/lang/Double;

    .line 396
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mSunRiseTime:Ljava/lang/String;

    .line 397
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mSunSetTime:Ljava/lang/String;

    .line 398
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 164
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public getConverterWeatherIconNum()I
    .locals 1

    .prologue
    .line 330
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mConverterWeatehrIconNum:I

    return v0
.end method

.method public getDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mTimestamp:Ljava/lang/String;

    return-object v0
.end method

.method public getDayHailAmount()Ljava/lang/Double;
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mDayHailAmount:Ljava/lang/Double;

    return-object v0
.end method

.method public getDayHailProbability()I
    .locals 1

    .prologue
    .line 253
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mDayHailProbability:I

    return v0
.end method

.method public getDayPrecipitationAmount()Ljava/lang/Double;
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mDayPrecipitationAmount:Ljava/lang/Double;

    return-object v0
.end method

.method public getDayPrecipitationProbability()I
    .locals 1

    .prologue
    .line 261
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mDayPrecipitationProbability:I

    return v0
.end method

.method public getDayRainAmount()Ljava/lang/Double;
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mDayRainAmount:Ljava/lang/Double;

    return-object v0
.end method

.method public getDayRainProbability()I
    .locals 1

    .prologue
    .line 237
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mDayRainProbability:I

    return v0
.end method

.method public getDaySnowAmount()Ljava/lang/Double;
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mDaySnowAmount:Ljava/lang/Double;

    return-object v0
.end method

.method public getDaySnowProbability()I
    .locals 1

    .prologue
    .line 245
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mDaySnowProbability:I

    return v0
.end method

.method public getHighTemp()F
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mHighTemp:F

    return v0
.end method

.method public getIconNum()I
    .locals 1

    .prologue
    .line 137
    sget-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isAutoCaptureToolSupportMode:Z

    if-eqz v0, :cond_0

    .line 138
    const/4 v0, 0x1

    .line 140
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mIconNum:I

    goto :goto_0
.end method

.method public getLowTemp()F
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mLowTemp:F

    return v0
.end method

.method public getNightHailAmount()Ljava/lang/Double;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mNightHailAmount:Ljava/lang/Double;

    return-object v0
.end method

.method public getNightHailProbability()I
    .locals 1

    .prologue
    .line 189
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mNightHailProbability:I

    return v0
.end method

.method public getNightPrecipitationAmount()Ljava/lang/Double;
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mNightPrecipitationAmount:Ljava/lang/Double;

    return-object v0
.end method

.method public getNightPrecipitationProbability()I
    .locals 1

    .prologue
    .line 197
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mNightPrecipitationProbability:I

    return v0
.end method

.method public getNightRainAmount()Ljava/lang/Double;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mNightRainAmount:Ljava/lang/Double;

    return-object v0
.end method

.method public getNightRainProbability()I
    .locals 1

    .prologue
    .line 173
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mNightRainProbability:I

    return v0
.end method

.method public getNightSnowAmount()Ljava/lang/Double;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mNightSnowAmount:Ljava/lang/Double;

    return-object v0
.end method

.method public getNightSnowProbability()I
    .locals 1

    .prologue
    .line 181
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mNightSnowProbability:I

    return v0
.end method

.method public getOnedayDayIcon()I
    .locals 1

    .prologue
    .line 297
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mOnedayDayIcon:I

    return v0
.end method

.method public getOnedayNightIcon()I
    .locals 1

    .prologue
    .line 305
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mOnedayNightIcon:I

    return v0
.end method

.method public getSummerTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mSummerTime:Ljava/lang/String;

    return-object v0
.end method

.method public getSunRiseTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mSunRiseTime:Ljava/lang/String;

    return-object v0
.end method

.method public getSunSetTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 321
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mSunSetTime:Ljava/lang/String;

    return-object v0
.end method

.method public getTempScale()I
    .locals 1

    .prologue
    .line 156
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mTempScale:I

    return v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public setConverterWeatherIconNum(I)V
    .locals 0
    .param p1, "iconNum"    # I

    .prologue
    .line 335
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mConverterWeatehrIconNum:I

    .line 336
    return-void
.end method

.method public setDate(Ljava/lang/String;)V
    .locals 0
    .param p1, "timestamp"    # Ljava/lang/String;

    .prologue
    .line 152
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mTimestamp:Ljava/lang/String;

    .line 153
    return-void
.end method

.method public setDayHailAmount(D)V
    .locals 1
    .param p1, "hailamount"    # D

    .prologue
    .line 281
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mDayHailAmount:Ljava/lang/Double;

    .line 282
    return-void
.end method

.method public setDayHailProbability(I)V
    .locals 0
    .param p1, "hailprob"    # I

    .prologue
    .line 249
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mDayHailProbability:I

    .line 250
    return-void
.end method

.method public setDayPrecipitationAmount(D)V
    .locals 1
    .param p1, "precipitationamount"    # D

    .prologue
    .line 289
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mDayPrecipitationAmount:Ljava/lang/Double;

    .line 290
    return-void
.end method

.method public setDayPrecipitationProbability(I)V
    .locals 0
    .param p1, "precipitationprob"    # I

    .prologue
    .line 257
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mDayPrecipitationProbability:I

    .line 258
    return-void
.end method

.method public setDayRainAmount(D)V
    .locals 1
    .param p1, "rainamount"    # D

    .prologue
    .line 265
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mDayRainAmount:Ljava/lang/Double;

    .line 266
    return-void
.end method

.method public setDayRainProbability(I)V
    .locals 0
    .param p1, "rainprob"    # I

    .prologue
    .line 233
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mDayRainProbability:I

    .line 234
    return-void
.end method

.method public setDaySnowAmount(D)V
    .locals 1
    .param p1, "snowamount"    # D

    .prologue
    .line 273
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mDaySnowAmount:Ljava/lang/Double;

    .line 274
    return-void
.end method

.method public setDaySnowProbability(I)V
    .locals 0
    .param p1, "snowprob"    # I

    .prologue
    .line 241
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mDaySnowProbability:I

    .line 242
    return-void
.end method

.method public setHighTemp(F)V
    .locals 0
    .param p1, "temperature"    # F

    .prologue
    .line 133
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mHighTemp:F

    .line 134
    return-void
.end method

.method public setIconNum(I)V
    .locals 0
    .param p1, "iconNum"    # I

    .prologue
    .line 144
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mIconNum:I

    .line 145
    return-void
.end method

.method public setLowTemp(F)V
    .locals 0
    .param p1, "temperature"    # F

    .prologue
    .line 125
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mLowTemp:F

    .line 126
    return-void
.end method

.method public setNightHailAmount(D)V
    .locals 1
    .param p1, "hailamount"    # D

    .prologue
    .line 217
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mNightHailAmount:Ljava/lang/Double;

    .line 218
    return-void
.end method

.method public setNightHailProbability(I)V
    .locals 0
    .param p1, "hailprob"    # I

    .prologue
    .line 185
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mNightHailProbability:I

    .line 186
    return-void
.end method

.method public setNightPrecipitationAmount(D)V
    .locals 1
    .param p1, "precipitationamount"    # D

    .prologue
    .line 225
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mNightPrecipitationAmount:Ljava/lang/Double;

    .line 226
    return-void
.end method

.method public setNightPrecipitationProbability(I)V
    .locals 0
    .param p1, "precipitationprob"    # I

    .prologue
    .line 193
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mNightPrecipitationProbability:I

    .line 194
    return-void
.end method

.method public setNightRainAmount(D)V
    .locals 1
    .param p1, "rainamount"    # D

    .prologue
    .line 201
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mNightRainAmount:Ljava/lang/Double;

    .line 202
    return-void
.end method

.method public setNightRainProbability(I)V
    .locals 0
    .param p1, "rainprob"    # I

    .prologue
    .line 169
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mNightRainProbability:I

    .line 170
    return-void
.end method

.method public setNightSnowAmount(D)V
    .locals 1
    .param p1, "snowamount"    # D

    .prologue
    .line 209
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mNightSnowAmount:Ljava/lang/Double;

    .line 210
    return-void
.end method

.method public setNightSnowProbability(I)V
    .locals 0
    .param p1, "snowprob"    # I

    .prologue
    .line 177
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mNightSnowProbability:I

    .line 178
    return-void
.end method

.method public setOnedayDayIcon(I)V
    .locals 0
    .param p1, "mOnedayDayIcon"    # I

    .prologue
    .line 301
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mOnedayDayIcon:I

    .line 302
    return-void
.end method

.method public setOnedayNightIcon(I)V
    .locals 0
    .param p1, "mOnedayNightIcon"    # I

    .prologue
    .line 309
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mOnedayNightIcon:I

    .line 310
    return-void
.end method

.method public setSummerTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "summertime"    # Ljava/lang/String;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mSummerTime:Ljava/lang/String;

    .line 110
    return-void
.end method

.method public setSunRiseTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "time"    # Ljava/lang/String;

    .prologue
    .line 317
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mSunRiseTime:Ljava/lang/String;

    .line 318
    return-void
.end method

.method public setSunSetTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "time"    # Ljava/lang/String;

    .prologue
    .line 325
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mSunSetTime:Ljava/lang/String;

    .line 326
    return-void
.end method

.method public setTempScale(I)V
    .locals 0
    .param p1, "scale"    # I

    .prologue
    .line 160
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mTempScale:I

    .line 161
    return-void
.end method

.method public setUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mUrl:Ljava/lang/String;

    .line 118
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 339
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mTimestamp:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 340
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mTempScale:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 341
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mIconNum:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 342
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mHighTemp:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 343
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mLowTemp:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 344
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 345
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mSummerTime:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 347
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mConverterWeatehrIconNum:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 349
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mDayRainProbability:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 350
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mDaySnowProbability:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 351
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mDayHailProbability:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 352
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mDayPrecipitationProbability:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 353
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mDayRainAmount:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 354
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mDaySnowAmount:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 355
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mDayHailAmount:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 356
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mDayPrecipitationAmount:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 357
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mNightRainProbability:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 358
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mNightSnowProbability:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 359
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mNightHailProbability:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 360
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mNightPrecipitationProbability:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 361
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mNightRainAmount:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 362
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mNightSnowAmount:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 363
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mNightHailAmount:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 364
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mNightPrecipitationAmount:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 365
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mSunRiseTime:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 366
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->mSunSetTime:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 367
    return-void
.end method
