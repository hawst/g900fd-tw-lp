.class public final enum Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;
.super Ljava/lang/Enum;
.source "NotiReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "GetLineNum"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;

.field public static final enum LINE_DATE:Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;

.field public static final enum LINE_DAY:Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;

.field public static final enum LINE_NIGHT:Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 213
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;

    const-string v1, "LINE_DATE"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;->LINE_DATE:Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;

    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;

    const-string v1, "LINE_DAY"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;->LINE_DAY:Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;

    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;

    const-string v1, "LINE_NIGHT"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;->LINE_NIGHT:Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;

    .line 212
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;

    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;->LINE_DATE:Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;->LINE_DAY:Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;->LINE_NIGHT:Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;->$VALUES:[Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 212
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 212
    const-class v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;
    .locals 1

    .prologue
    .line 212
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;->$VALUES:[Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;

    invoke-virtual {v0}, [Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;

    return-object v0
.end method
