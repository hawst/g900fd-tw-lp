.class Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$1;
.super Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;
.source "GetMapInfoManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->getMapCityInfo(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    .prologue
    .line 190
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(IILjava/lang/String;Ljava/lang/String;J)V
    .locals 9
    .param p1, "position"    # I
    .param p2, "responseCode"    # I
    .param p3, "responseStatus"    # Ljava/lang/String;
    .param p4, "responseBody"    # Ljava/lang/String;
    .param p5, "stamp"    # J

    .prologue
    .line 193
    invoke-super {p0, p1, p2, p3, p4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;->onReceive(IILjava/lang/String;Ljava/lang/String;)V

    .line 195
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mPerformMapCancled:Z
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->access$000()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 196
    const-string v5, ""

    const-string v6, "performMapCancled"

    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->deleteHttpThreadAll()V

    .line 316
    :cond_0
    :goto_0
    return-void

    .line 200
    :cond_1
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->syncstamp:J
    invoke-static {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;)J

    move-result-wide v5

    cmp-long v5, v5, p5

    if-eqz v5, :cond_2

    .line 201
    const-string v5, ""

    const-string v6, "syncstamp != stamp #1"

    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->deleteHttpThreadAll()V

    goto :goto_0

    .line 206
    :cond_2
    const/4 v0, 0x0

    .line 208
    .local v0, "curUrlType":I
    const-string v5, ""

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mHT p : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    iget-object v5, v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mHttpThreads:Ljava/util/ArrayList;

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    iget-object v5, v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v5, p1, :cond_6

    .line 211
    const-string v5, ""

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mHT : s: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    iget-object v7, v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    iget-object v5, v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Thread;

    .line 213
    .local v4, "thread":Ljava/lang/Thread;
    if-eqz v4, :cond_5

    .line 214
    invoke-virtual {v4}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 225
    const-string v5, ""

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "curUrlType : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    const/16 v5, 0xc8

    if-ne p2, v5, :cond_c

    .line 229
    :try_start_0
    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;

    invoke-direct {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;-><init>()V

    .line 230
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mTempScale:I
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->access$200()I

    move-result v6

    .line 231
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    .line 230
    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    .line 229
    invoke-virtual {v5, v6, v7, p4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->parseDetailWeather_LocCities(ILjava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 233
    .local v3, "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;>;"
    const-class v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    monitor-enter v6
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 234
    packed-switch v0, :pswitch_data_0

    .line 266
    :cond_3
    :goto_1
    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 268
    :try_start_2
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->postProcess()V
    invoke-static {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 270
    .end local v3    # "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;>;"
    :catch_0
    move-exception v1

    .line 271
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 273
    const/4 v5, 0x5

    if-ne v0, v5, :cond_a

    .line 274
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    const/4 v6, 0x5

    const/4 v7, 0x0

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->setGetMapInfoResult(IZ)V
    invoke-static {v5, v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;IZ)V

    .line 281
    :cond_4
    :goto_2
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->deleteHttpThreadAll()V

    .line 283
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mActivity:Landroid/app/Activity;
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->access$800()Landroid/app/Activity;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 284
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mActivity:Landroid/app/Activity;
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->access$800()Landroid/app/Activity;

    move-result-object v5

    new-instance v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$1$1;

    invoke-direct {v6, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$1$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$1;)V

    invoke-virtual {v5, v6}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 216
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_5
    const-string v5, ""

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "thread n : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->deleteHttpThreadAll()V

    goto/16 :goto_0

    .line 221
    .end local v4    # "thread":Ljava/lang/Thread;
    :cond_6
    const-string v5, ""

    const-string v6, "o f i"

    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->deleteHttpThreadAll()V

    goto/16 :goto_0

    .line 236
    .restart local v3    # "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;>;"
    .restart local v4    # "thread":Ljava/lang/Thread;
    :pswitch_0
    if-eqz v3, :cond_3

    .line 237
    :try_start_3
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    .line 238
    .local v2, "i":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    const/4 v7, 0x5

    invoke-virtual {v2, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->setZoomlevel(I)V

    .line 239
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList5:Ljava/util/ArrayList;
    invoke-static {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 266
    .end local v2    # "i":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v5
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 241
    :cond_7
    :try_start_5
    const-string v5, ""

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "url5 "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList5:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " cities"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    const/4 v7, 0x5

    const/4 v8, 0x1

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->setGetMapInfoResult(IZ)V
    invoke-static {v5, v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;IZ)V

    goto/16 :goto_1

    .line 246
    :pswitch_1
    if-eqz v3, :cond_3

    .line 247
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    .line 248
    .restart local v2    # "i":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    const/4 v7, 0x5

    invoke-virtual {v2, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->setZoomlevel(I)V

    .line 249
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList6:Ljava/util/ArrayList;
    invoke-static {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 251
    .end local v2    # "i":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    :cond_8
    const-string v5, ""

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "url6 "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList6:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " cities"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    const/4 v7, 0x6

    const/4 v8, 0x1

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->setGetMapInfoResult(IZ)V
    invoke-static {v5, v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;IZ)V

    goto/16 :goto_1

    .line 256
    :pswitch_2
    if-eqz v3, :cond_3

    .line 257
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_9

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    .line 258
    .restart local v2    # "i":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    const/4 v7, 0x5

    invoke-virtual {v2, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->setZoomlevel(I)V

    .line 259
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList7:Ljava/util/ArrayList;
    invoke-static {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 261
    .end local v2    # "i":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    :cond_9
    const-string v5, ""

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "url7 "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList7:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " cities"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    const/4 v7, 0x7

    const/4 v8, 0x1

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->setGetMapInfoResult(IZ)V
    invoke-static {v5, v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;IZ)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1

    .line 275
    .end local v3    # "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;>;"
    .restart local v1    # "e":Ljava/lang/Exception;
    :cond_a
    const/4 v5, 0x6

    if-ne v0, v5, :cond_b

    .line 276
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    const/4 v6, 0x6

    const/4 v7, 0x0

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->setGetMapInfoResult(IZ)V
    invoke-static {v5, v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;IZ)V

    goto/16 :goto_2

    .line 277
    :cond_b
    const/4 v5, 0x7

    if-ne v0, v5, :cond_4

    .line 278
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    const/4 v6, 0x7

    const/4 v7, 0x0

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->setGetMapInfoResult(IZ)V
    invoke-static {v5, v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;IZ)V

    goto/16 :goto_2

    .line 294
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_c
    const-string v5, ""

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "response : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    const/4 v5, 0x5

    if-ne v0, v5, :cond_e

    .line 297
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    const/4 v6, 0x5

    const/4 v7, 0x0

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->setGetMapInfoResult(IZ)V
    invoke-static {v5, v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;IZ)V

    .line 304
    :cond_d
    :goto_6
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->deleteHttpThreadAll()V

    .line 306
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mActivity:Landroid/app/Activity;
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->access$800()Landroid/app/Activity;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 307
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mActivity:Landroid/app/Activity;
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->access$800()Landroid/app/Activity;

    move-result-object v5

    new-instance v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$1$2;

    invoke-direct {v6, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$1$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$1;)V

    invoke-virtual {v5, v6}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 298
    :cond_e
    const/4 v5, 0x6

    if-ne v0, v5, :cond_f

    .line 299
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    const/4 v6, 0x6

    const/4 v7, 0x0

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->setGetMapInfoResult(IZ)V
    invoke-static {v5, v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;IZ)V

    goto :goto_6

    .line 300
    :cond_f
    const/4 v5, 0x7

    if-ne v0, v5, :cond_d

    .line 301
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    const/4 v6, 0x7

    const/4 v7, 0x0

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->setGetMapInfoResult(IZ)V
    invoke-static {v5, v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;IZ)V

    goto :goto_6

    .line 234
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
