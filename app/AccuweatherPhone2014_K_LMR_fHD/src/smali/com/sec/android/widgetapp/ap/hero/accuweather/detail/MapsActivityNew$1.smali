.class Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;
.super Landroid/os/Handler;
.source "MapsActivityNew.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 270
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x6

    const/4 v1, 0x5

    .line 272
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 351
    :cond_0
    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mRunning:Z
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$802(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Z)Z

    .line 352
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->invalidate()V

    .line 353
    return-void

    .line 282
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->setupMapView()V
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    goto :goto_0

    .line 288
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->removeAllMapList()V
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    goto :goto_0

    .line 292
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->createMapItems(I)V
    invoke-static {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;I)V

    goto :goto_0

    .line 296
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->removeAllMapList()V
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    .line 297
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->createMapItems(I)V
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;I)V

    .line 298
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->createMapItems(I)V
    invoke-static {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;I)V

    .line 301
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItemsList5:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 302
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getOverlays()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItemsList5:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 304
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 305
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getOverlays()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 310
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->removeAllMapList()V
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    .line 311
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->createMapItems(I)V
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;I)V

    .line 312
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->createMapItems(I)V
    invoke-static {v0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;I)V

    .line 314
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItemsList6:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 315
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getOverlays()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItemsList6:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 318
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItemsList5:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 319
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getOverlays()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItemsList5:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 322
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 323
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getOverlays()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 328
    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->removeAllMapList()V
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    .line 329
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->createMapItems(I)V
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;I)V

    .line 330
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->createMapItems(I)V
    invoke-static {v0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;I)V

    .line 331
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    const/4 v1, 0x7

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->createMapItems(I)V
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;I)V

    .line 333
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItemsList7:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 334
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getOverlays()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItemsList7:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 337
    :cond_4
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItemsList6:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 338
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getOverlays()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItemsList6:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 341
    :cond_5
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItemsList5:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 342
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getOverlays()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItemsList5:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 345
    :cond_6
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 346
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getOverlays()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 272
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_3
    .end packed-switch
.end method
