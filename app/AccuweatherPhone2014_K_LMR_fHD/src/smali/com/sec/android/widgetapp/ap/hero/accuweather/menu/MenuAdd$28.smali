.class Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$28;
.super Ljava/lang/Object;
.source "MenuAdd.java"

# interfaces
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getLocationInfo()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 1274
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public click(I)V
    .locals 3
    .param p1, "buttonType"    # I

    .prologue
    const/16 v2, 0xa

    .line 1276
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->isActivityVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1277
    if-ne p1, v2, :cond_1

    .line 1278
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->gps_btn:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/widget/RelativeLayout;

    move-result-object v1

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->performClickGpsBtn(Landroid/view/View;)V
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;Landroid/view/View;)V

    .line 1279
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setIsAgreeUseLocation(Landroid/content/Context;Z)V

    .line 1280
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mPushDialogButtonType:I
    invoke-static {v0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$1902(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;I)I

    .line 1287
    :cond_0
    :goto_0
    return-void

    .line 1282
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->enableEditField()V
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$1300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    .line 1283
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->requestFocus()Z

    .line 1284
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    goto :goto_0
.end method
