.class Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$29;
.super Ljava/lang/Object;
.source "MenuSearch.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 1266
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$29;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 12
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    const/16 v5, 0x8

    const/4 v2, 0x0

    .line 1275
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$29;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkLanguage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    .line 1276
    .local v6, "language":Ljava/lang/String;
    const/4 v7, 0x1

    .line 1282
    .local v7, "result":Z
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$29;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1284
    .local v8, "strCityName":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$29;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v0}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1285
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_6

    .line 1286
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$29;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setGravity(I)V

    .line 1293
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$29;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mClearTextBtn:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$3700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1294
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$29;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mClearTextBtn:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$3700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_7

    .line 1295
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_1

    .line 1296
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$29;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mClearTextBtn:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$3700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1303
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$29;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mSearchLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Landroid/widget/LinearLayout;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1304
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$29;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mSearchLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_8

    .line 1305
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 1306
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$29;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mSearchLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1313
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$29;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->isActivityVisible()Z

    move-result v0

    if-eqz v0, :cond_5

    if-eqz v7, :cond_5

    .line 1315
    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    .line 1317
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_5

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x1f

    if-gt v0, v1, :cond_5

    .line 1318
    if-eqz p1, :cond_3

    .line 1319
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$29;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->dismissDropDown()V

    .line 1320
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$29;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$29;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    const v5, 0x7f030024

    const v10, 0x1020014

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v1, v2, v5, v10, v11}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    .line 1321
    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1325
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$29;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$3800(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeHeader()Lorg/apache/http/message/HeaderGroup;

    move-result-object v4

    .line 1326
    .local v4, "headerGroup":Lorg/apache/http/message/HeaderGroup;
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$29;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$3800(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    move-result-object v0

    invoke-virtual {v0, v8, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeUrlForGetAutoComplete(Ljava/lang/String;Ljava/lang/String;)Ljava/net/URL;

    move-result-object v3

    .line 1328
    .local v3, "url":Ljava/net/URL;
    if-eqz v3, :cond_5

    .line 1333
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$29;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mHttpThreads:Ljava/util/ArrayList;

    if-nez v0, :cond_4

    .line 1334
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$29;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mHttpThreads:Ljava/util/ArrayList;

    .line 1336
    :cond_4
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$29;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->httpClient:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$4000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$29;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    iget-object v1, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$29;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$29$1;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$29$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$29;)V

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->get(ILandroid/content/Context;Ljava/net/URL;Lorg/apache/http/message/HeaderGroup;Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;)Ljava/lang/Thread;

    move-result-object v9

    .line 1377
    .local v9, "t":Ljava/lang/Thread;
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$29;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1381
    .end local v3    # "url":Ljava/net/URL;
    .end local v4    # "headerGroup":Lorg/apache/http/message/HeaderGroup;
    .end local v9    # "t":Ljava/lang/Thread;
    :cond_5
    return-void

    .line 1289
    :cond_6
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$29;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    const/16 v1, 0x15

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setGravity(I)V

    goto/16 :goto_0

    .line 1298
    :cond_7
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 1299
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$29;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mClearTextBtn:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$3700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1

    .line 1308
    :cond_8
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_2

    .line 1309
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$29;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mSearchLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_2
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 1272
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 1269
    return-void
.end method
