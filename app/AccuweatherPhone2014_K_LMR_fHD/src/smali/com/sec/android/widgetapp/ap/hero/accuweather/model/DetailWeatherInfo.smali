.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
.super Ljava/lang/Object;
.source "DetailWeatherInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field Location:Ljava/lang/String;

.field cityName:Ljava/lang/String;

.field forecasts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;",
            ">;"
        }
    .end annotation
.end field

.field hourInfo:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;",
            ">;"
        }
    .end annotation
.end field

.field mRealLocation:Ljava/lang/String;

.field mState:Ljava/lang/String;

.field mTemperature:Ljava/lang/String;

.field photosInfo:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;",
            ">;"
        }
    .end annotation
.end field

.field todayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 137
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo$1;

    invoke-direct {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo$1;-><init>()V

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->hourInfo:Ljava/util/ArrayList;

    .line 12
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->forecasts:Ljava/util/ArrayList;

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->photosInfo:Ljava/util/ArrayList;

    .line 88
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x0

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->hourInfo:Ljava/util/ArrayList;

    .line 12
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->forecasts:Ljava/util/ArrayList;

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->photosInfo:Ljava/util/ArrayList;

    .line 82
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->todayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    .line 83
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->forecasts:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 84
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->hourInfo:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 85
    return-void
.end method


# virtual methods
.method public addForecastInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;)V
    .locals 1
    .param p1, "info"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->forecasts:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 57
    return-void
.end method

.method public addHourInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;)V
    .locals 1
    .param p1, "info"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->hourInfo:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 49
    return-void
.end method

.method public addPhotosInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;)V
    .locals 1
    .param p1, "info"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->photosInfo:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 65
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 72
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public getCityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->cityName:Ljava/lang/String;

    return-object v0
.end method

.method public getForcastSize()I
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->forecasts:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->forecasts:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    return-object v0
.end method

.method public getHourInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->hourInfo:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;

    return-object v0
.end method

.method public getHourInfo()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->hourInfo:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getHourSize()I
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->hourInfo:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->Location:Ljava/lang/String;

    return-object v0
.end method

.method public getPhotosInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->photosInfo:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;

    return-object v0
.end method

.method public getPhotosInfoSize()I
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->photosInfo:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getRealLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->mRealLocation:Ljava/lang/String;

    return-object v0
.end method

.method public getState()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->mState:Ljava/lang/String;

    return-object v0
.end method

.method public getTemperature()Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->mTemperature:Ljava/lang/String;

    return-object v0
.end method

.method public getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->todayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    return-object v0
.end method

.method public setCityName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->cityName:Ljava/lang/String;

    .line 92
    return-void
.end method

.method public setHourInfo(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 131
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;>;"
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->hourInfo:Ljava/util/ArrayList;

    .line 132
    return-void
.end method

.method public setLocation(Ljava/lang/String;)V
    .locals 0
    .param p1, "cityid"    # Ljava/lang/String;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->Location:Ljava/lang/String;

    .line 108
    return-void
.end method

.method public setRealLocation(Ljava/lang/String;)V
    .locals 0
    .param p1, "realLocation"    # Ljava/lang/String;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->mRealLocation:Ljava/lang/String;

    .line 120
    return-void
.end method

.method public setState(Ljava/lang/String;)V
    .locals 0
    .param p1, "state"    # Ljava/lang/String;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->mState:Ljava/lang/String;

    .line 100
    return-void
.end method

.method public setTemperature(Ljava/lang/String;)V
    .locals 0
    .param p1, "temp"    # Ljava/lang/String;

    .prologue
    .line 123
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->mTemperature:Ljava/lang/String;

    .line 124
    return-void
.end method

.method public setTodayWeatherInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;)V
    .locals 0
    .param p1, "todayInfo"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->todayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    .line 41
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->todayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 77
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->forecasts:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 78
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->hourInfo:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 79
    return-void
.end method
