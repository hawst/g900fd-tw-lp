.class Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$5;
.super Landroid/os/Handler;
.source "MenuChangeOrdersList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    .prologue
    .line 435
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 437
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "RESPONSE_CODE"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 438
    .local v1, "responseCode":I
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "LOCATION_NAME"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 440
    .local v0, "locationType":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 441
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mUH cnt = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getRefreshCnt()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", rCode = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", loc = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 447
    :goto_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getRefreshCnt()I

    move-result v2

    if-gtz v2, :cond_0

    .line 448
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->hideRefreshDialog()V
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->access$800(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)V

    .line 451
    :cond_0
    const/16 v2, 0xc8

    if-eq v1, v2, :cond_2

    .line 452
    if-eqz v0, :cond_1

    const-string v2, "cityId:current"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 453
    :cond_1
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->hideRefreshDialog()V
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->access$800(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)V

    .line 454
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    const v3, 0x7f0d001f

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    .line 458
    :cond_2
    return-void

    .line 444
    :cond_3
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mRM is n, rCode = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", loc = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
