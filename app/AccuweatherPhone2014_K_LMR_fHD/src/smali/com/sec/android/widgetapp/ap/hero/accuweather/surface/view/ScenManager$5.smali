.class Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$5;
.super Ljava/lang/Object;
.source "ScenManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->startIconAnimation(Landroid/view/ViewGroup;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    .prologue
    .line 1202
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 1204
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurrentCityInfoLayout:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->access$800(Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;)Landroid/view/ViewGroup;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1205
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurrentCityInfoLayout:Landroid/view/ViewGroup;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->access$800(Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;)Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->stopIconAnimation(Landroid/view/ViewGroup;)V

    .line 1207
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mNextCityInfoLayout:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->access$900(Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;)Landroid/view/ViewGroup;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1208
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mNextCityInfoLayout:Landroid/view/ViewGroup;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->access$900(Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;)Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->stopIconAnimation(Landroid/view/ViewGroup;)V

    .line 1210
    :cond_1
    return-void
.end method
