.class Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$2;
.super Landroid/os/Handler;
.source "CubeViewController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    .prologue
    .line 108
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 111
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 112
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mMPDelayHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 113
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mIsRunningAni:Z

    if-eqz v0, :cond_1

    .line 123
    :cond_0
    :goto_0
    return-void

    .line 116
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    .line 117
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->getMPState()I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    .line 118
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->getMPState()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 119
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->playBGFadeIn()V

    .line 120
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->setMPState(I)V

    .line 121
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->startAfterFlick()V

    goto :goto_0
.end method
