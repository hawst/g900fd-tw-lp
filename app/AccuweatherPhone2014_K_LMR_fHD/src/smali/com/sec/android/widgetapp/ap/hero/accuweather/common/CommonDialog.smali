.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;
.super Ljava/lang/Object;
.source "CommonDialog.java"


# static fields
.field public static final ALREADY_REGISTERED_MESSAGE:I = 0x3eb

.field public static final BUTTON_CHECK:I = 0xc

.field public static final BUTTON_NEGATIVE:I = 0xb

.field public static final BUTTON_POSITIVE:I = 0xa

.field public static final CHANGED_MESSAGE:I = 0x3f1

.field public static final CONFIRM_ALL_DELETE:I = 0x3fb

.field public static final CONFIRM_AUTO_REFRESH:I = 0x3f4

.field public static final CONFIRM_DATA_ROAMING:I = 0x3f9

.field public static final CONFIRM_DELETE:I = 0x3fa

.field public static final CONFIRM_DST:I = 0x3f3

.field public static final CONFIRM_GET_CURRENT_LOCATION:I = 0x402

.field public static final CONFIRM_GOTO_SAMSUNGAPPS:I = 0x421

.field public static final CONFIRM_GPS_LOCATING:I = 0x3f5

.field public static final CONFIRM_SENSOR:I = 0x3fd

.field public static final CONFIRM_SENSOR_SETTING:I = 0x3fe

.field public static final CONFIRM_TOUCH_WIDGET:I = 0x401

.field public static final CURRENT_LOCATION_EMPTY:I = 0x3fc

.field public static final DELETED_MESSAGE:I = 0x3f2

.field public static final ERROR_LOCATION_MESSAGE:I = 0x3f7

.field public static final ERROR_NETWORK_ACCESS_CHECK:I = 0x3ea

.field public static final ERROR_TEXT_INPUT_FAILED:I = 0x3e9

.field public static final LOADING_MESSAGE:I = 0x3ef

.field public static final LOCATING_MESSAGE:I = 0x3f8

.field public static final MAXIMUM_ITEM_SAVED_MESSAGE:I = 0x3ec

.field public static final NETWORK_CONNECTION_ERROR:I = 0x3f0

.field public static final NEW_VERSION_SOFTWARE:I = 0x423

.field private static final NORMAL_SHOW_TIME:I = 0x7d0

.field public static final NOTICE_GPS_ONLY:I = 0x400

.field public static final NOTICE_GPS_TURNED_OFF:I = 0x3f6

.field public static final NOTIFICATION_INFO_MESSAGE:I = 0x3ff

.field public static final NO_NEW_SOFTWARE:I = 0x422

.field public static final REFRESH_MESSAGE:I = 0x3ee

.field public static final SOFTWARE_UPDATE_NOTI:I = 0x424

.field public static handler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 85
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->handler:Landroid/os/Handler;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    return-void
.end method

.method private static hide(Landroid/app/AlertDialog;ILcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogClosedInteraction;)Landroid/app/AlertDialog;
    .locals 4
    .param p0, "dialog"    # Landroid/app/AlertDialog;
    .param p1, "time"    # I
    .param p2, "interaction"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogClosedInteraction;

    .prologue
    .line 110
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$2;

    invoke-direct {v1, p0, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$2;-><init>(Landroid/app/AlertDialog;Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogClosedInteraction;)V

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 124
    return-object p0
.end method

.method private static makeBasicDialog(Landroid/content/Context;II)Landroid/app/AlertDialog;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "stringID"    # I
    .param p2, "dialogID"    # I

    .prologue
    .line 133
    invoke-static {p0, p1, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->makeBasicDialogBuilder(Landroid/content/Context;II)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 134
    .local v0, "dialog":Landroid/app/AlertDialog;
    return-object v0
.end method

.method private static makeBasicDialog(Landroid/content/Context;Ljava/lang/String;I)Landroid/app/AlertDialog;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "dialogID"    # I

    .prologue
    .line 138
    invoke-static {p0, p1, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->makeBasicDialogBuilder(Landroid/content/Context;Ljava/lang/String;I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 139
    .local v0, "dialog":Landroid/app/AlertDialog;
    return-object v0
.end method

.method public static makeBasicDialogBuilder(Landroid/content/Context;I)Landroid/app/AlertDialog$Builder;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dialogID"    # I

    .prologue
    .line 161
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 163
    .local v0, "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    return-object v0
.end method

.method public static makeBasicDialogBuilder(Landroid/content/Context;II)Landroid/app/AlertDialog$Builder;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "stringID"    # I
    .param p2, "dialogID"    # I

    .prologue
    .line 144
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 146
    .local v0, "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 148
    return-object v0
.end method

.method public static makeBasicDialogBuilder(Landroid/content/Context;Ljava/lang/String;I)Landroid/app/AlertDialog$Builder;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "dialogID"    # I

    .prologue
    .line 153
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 155
    .local v0, "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 157
    return-object v0
.end method

.method private static show(Landroid/app/AlertDialog$Builder;Z)Landroid/app/AlertDialog;
    .locals 2
    .param p0, "builder"    # Landroid/app/AlertDialog$Builder;
    .param p1, "isShow"    # Z

    .prologue
    .line 128
    invoke-virtual {p0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 129
    .local v0, "dialog":Landroid/app/AlertDialog;
    invoke-static {v0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->show(Landroid/app/AlertDialog;Z)Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1
.end method

.method private static show(Landroid/app/AlertDialog;Z)Landroid/app/AlertDialog;
    .locals 2
    .param p0, "dialog"    # Landroid/app/AlertDialog;
    .param p1, "isShow"    # Z

    .prologue
    .line 92
    if-eqz p1, :cond_0

    .line 93
    if-eqz p0, :cond_0

    .line 94
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$1;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$1;-><init>(Landroid/app/AlertDialog;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 105
    :cond_0
    return-object p0
.end method

.method public static showDialog(Landroid/content/Context;I)Landroid/app/Dialog;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dialogID"    # I

    .prologue
    const/4 v1, 0x0

    .line 167
    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, v1

    move v3, p1

    invoke-static/range {v0 .. v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->showDialog(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogClosedInteraction;IZI)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public static showDialog(Landroid/content/Context;IILcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)Landroid/app/Dialog;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dialogID"    # I
    .param p2, "num"    # I
    .param p3, "interaction"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;

    .prologue
    .line 191
    const/4 v2, 0x0

    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p3

    move v3, p1

    move v5, p2

    invoke-static/range {v0 .. v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->showDialog(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogClosedInteraction;IZI)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public static showDialog(Landroid/content/Context;ILcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogClosedInteraction;)Landroid/app/Dialog;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dialogID"    # I
    .param p2, "interaction"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogClosedInteraction;

    .prologue
    .line 182
    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p2

    move v3, p1

    invoke-static/range {v0 .. v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->showDialog(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogClosedInteraction;IZI)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public static showDialog(Landroid/content/Context;ILcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)Landroid/app/Dialog;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dialogID"    # I
    .param p2, "interaction"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;

    .prologue
    .line 172
    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p2

    move v3, p1

    invoke-static/range {v0 .. v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->showDialog(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogClosedInteraction;IZI)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public static showDialog(Landroid/content/Context;ILcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;I)Landroid/app/Dialog;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dialogID"    # I
    .param p2, "interaction"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;
    .param p3, "num"    # I

    .prologue
    .line 177
    const/4 v2, 0x0

    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p2

    move v3, p1

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->showDialog(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogClosedInteraction;IZI)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public static showDialog(Landroid/content/Context;IZ)Landroid/app/Dialog;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dialogID"    # I
    .param p2, "isShow"    # Z

    .prologue
    const/4 v1, 0x0

    .line 186
    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, v1

    move v3, p1

    move v4, p2

    invoke-static/range {v0 .. v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->showDialog(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogClosedInteraction;IZI)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public static showDialog(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogClosedInteraction;IZI)Landroid/app/Dialog;
    .locals 22
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "interaction"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;
    .param p2, "closedInteraction"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogClosedInteraction;
    .param p3, "dialogID"    # I
    .param p4, "isShow"    # Z
    .param p5, "num"    # I

    .prologue
    .line 197
    const/4 v9, 0x0

    .line 200
    .local v9, "dialog":Landroid/app/AlertDialog;
    packed-switch p3, :pswitch_data_0

    .line 898
    :pswitch_0
    const/4 v12, 0x0

    :goto_0
    return-object v12

    .line 202
    :pswitch_1
    const v18, 0x7f0d0069

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->makeBasicDialog(Landroid/content/Context;II)Landroid/app/AlertDialog;

    move-result-object v9

    .line 203
    move/from16 v0, p4

    invoke-static {v9, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->show(Landroid/app/AlertDialog;Z)Landroid/app/AlertDialog;

    .line 204
    const/16 v18, 0x7d0

    move/from16 v0, v18

    move-object/from16 v1, p2

    invoke-static {v9, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->hide(Landroid/app/AlertDialog;ILcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogClosedInteraction;)Landroid/app/AlertDialog;

    move-object v12, v9

    .line 205
    goto :goto_0

    .line 208
    :pswitch_2
    const v18, 0x7f0d006a

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->makeBasicDialog(Landroid/content/Context;II)Landroid/app/AlertDialog;

    move-result-object v9

    .line 209
    move/from16 v0, p4

    invoke-static {v9, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->show(Landroid/app/AlertDialog;Z)Landroid/app/AlertDialog;

    .line 210
    const/16 v18, 0x7d0

    move/from16 v0, v18

    move-object/from16 v1, p2

    invoke-static {v9, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->hide(Landroid/app/AlertDialog;ILcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogClosedInteraction;)Landroid/app/AlertDialog;

    move-object v12, v9

    .line 211
    goto :goto_0

    .line 214
    :pswitch_3
    const v18, 0x7f0d0022

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->makeBasicDialog(Landroid/content/Context;II)Landroid/app/AlertDialog;

    move-result-object v9

    .line 215
    move/from16 v0, p4

    invoke-static {v9, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->show(Landroid/app/AlertDialog;Z)Landroid/app/AlertDialog;

    .line 216
    const/16 v18, 0x7d0

    move/from16 v0, v18

    move-object/from16 v1, p2

    invoke-static {v9, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->hide(Landroid/app/AlertDialog;ILcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogClosedInteraction;)Landroid/app/AlertDialog;

    move-object v12, v9

    .line 217
    goto :goto_0

    .line 220
    :pswitch_4
    const v18, 0x7f0d0023

    .line 221
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v18

    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    const/16 v21, 0xa

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    aput-object v21, v19, v20

    .line 220
    invoke-static/range {v18 .. v19}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->makeBasicDialog(Landroid/content/Context;Ljava/lang/String;I)Landroid/app/AlertDialog;

    move-result-object v9

    .line 223
    move/from16 v0, p4

    invoke-static {v9, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->show(Landroid/app/AlertDialog;Z)Landroid/app/AlertDialog;

    .line 224
    const/16 v18, 0x7d0

    move/from16 v0, v18

    move-object/from16 v1, p2

    invoke-static {v9, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->hide(Landroid/app/AlertDialog;ILcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogClosedInteraction;)Landroid/app/AlertDialog;

    move-object v12, v9

    .line 225
    goto/16 :goto_0

    .line 228
    :pswitch_5
    const v18, 0x7f0d001d

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->makeBasicDialog(Landroid/content/Context;II)Landroid/app/AlertDialog;

    move-result-object v9

    .line 229
    move/from16 v0, p4

    invoke-static {v9, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->show(Landroid/app/AlertDialog;Z)Landroid/app/AlertDialog;

    .line 230
    const/16 v18, 0x7d0

    move/from16 v0, v18

    move-object/from16 v1, p2

    invoke-static {v9, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->hide(Landroid/app/AlertDialog;ILcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogClosedInteraction;)Landroid/app/AlertDialog;

    move-object v12, v9

    .line 231
    goto/16 :goto_0

    .line 234
    :pswitch_6
    const v18, 0x7f0d001c

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->makeBasicDialog(Landroid/content/Context;II)Landroid/app/AlertDialog;

    move-result-object v9

    .line 235
    move/from16 v0, p4

    invoke-static {v9, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->show(Landroid/app/AlertDialog;Z)Landroid/app/AlertDialog;

    .line 236
    const/16 v18, 0x7d0

    move/from16 v0, v18

    move-object/from16 v1, p2

    invoke-static {v9, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->hide(Landroid/app/AlertDialog;ILcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogClosedInteraction;)Landroid/app/AlertDialog;

    move-object v12, v9

    .line 237
    goto/16 :goto_0

    .line 240
    :pswitch_7
    const v18, 0x7f0d006b

    const/16 v19, 0x3ee

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->makeBasicDialogBuilder(Landroid/content/Context;II)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 242
    .local v3, "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    const v18, 0x7f0d0009

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$3;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$3;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 250
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v12

    goto/16 :goto_0

    .line 253
    .end local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    :pswitch_8
    new-instance v12, Landroid/app/ProgressDialog;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 254
    .local v12, "loadingdialog":Landroid/app/ProgressDialog;
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v12, v0}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 255
    const v18, 0x7f0d001e

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 256
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v12, v0}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 257
    new-instance v18, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$4;

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$4;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)V

    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    goto/16 :goto_0

    .line 267
    .end local v12    # "loadingdialog":Landroid/app/ProgressDialog;
    :pswitch_9
    const v18, 0x7f0d001f

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->makeBasicDialogBuilder(Landroid/content/Context;II)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 269
    .restart local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    const v18, 0x7f0d006c

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$5;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$5;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 278
    const v18, 0x7f0d0009

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$6;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$6;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 287
    move/from16 v0, p4

    invoke-static {v3, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->show(Landroid/app/AlertDialog$Builder;Z)Landroid/app/AlertDialog;

    move-result-object v12

    goto/16 :goto_0

    .line 290
    .end local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    :pswitch_a
    const v18, 0x7f0d0067

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->makeBasicDialogBuilder(Landroid/content/Context;II)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 292
    .restart local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    const v18, 0x7f030014

    const/16 v19, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    .line 294
    .local v10, "dst_view":Landroid/view/View;
    const v18, 0x7f08003b

    move/from16 v0, v18

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/LinearLayout;

    check-cast v18, Landroid/widget/LinearLayout;

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$7;

    invoke-direct/range {v19 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$7;-><init>()V

    .line 295
    invoke-virtual/range {v18 .. v19}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 302
    const v18, 0x7f08003c

    move/from16 v0, v18

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultCheckBox;

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$8;

    invoke-direct/range {v19 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$8;-><init>()V

    .line 303
    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultCheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 308
    invoke-virtual {v3, v10}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 310
    const v18, 0x7f0d0067

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$9;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$9;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 319
    const v18, 0x7f0d0068

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$10;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$10;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 328
    move/from16 v0, p4

    invoke-static {v3, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->show(Landroid/app/AlertDialog$Builder;Z)Landroid/app/AlertDialog;

    move-result-object v12

    goto/16 :goto_0

    .line 331
    .end local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    .end local v10    # "dst_view":Landroid/view/View;
    :pswitch_b
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->makeBasicDialogBuilder(Landroid/content/Context;I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 333
    .restart local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    const v18, 0x7f0d0032

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 335
    const v18, 0x7f030014

    const/16 v19, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 337
    .local v7, "confirm_view":Landroid/view/View;
    const v18, 0x7f08003a

    move/from16 v0, v18

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    check-cast v18, Landroid/widget/TextView;

    const v19, 0x7f0d0030

    .line 338
    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setText(I)V

    .line 340
    const v18, 0x7f08003b

    move/from16 v0, v18

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/LinearLayout;

    check-cast v18, Landroid/widget/LinearLayout;

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$11;

    invoke-direct/range {v19 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$11;-><init>()V

    .line 341
    invoke-virtual/range {v18 .. v19}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 348
    const v18, 0x7f08003c

    move/from16 v0, v18

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultCheckBox;

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$12;

    invoke-direct/range {v19 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$12;-><init>()V

    .line 349
    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultCheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 354
    invoke-virtual {v3, v7}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 356
    const v18, 0x7f0d0008

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$13;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$13;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 365
    const v18, 0x7f0d0009

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$14;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$14;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 374
    move/from16 v0, p4

    invoke-static {v3, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->show(Landroid/app/AlertDialog$Builder;Z)Landroid/app/AlertDialog;

    move-result-object v12

    goto/16 :goto_0

    .line 377
    .end local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    .end local v7    # "confirm_view":Landroid/view/View;
    :pswitch_c
    const v18, 0x7f0d002a

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->makeBasicDialogBuilder(Landroid/content/Context;II)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 379
    .restart local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    const v18, 0x7f0d0029

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 380
    const v18, 0x7f0d000b

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$15;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$15;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 389
    const v18, 0x7f0d0009

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$16;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$16;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 398
    move/from16 v0, p4

    invoke-static {v3, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->show(Landroid/app/AlertDialog$Builder;Z)Landroid/app/AlertDialog;

    move-result-object v12

    goto/16 :goto_0

    .line 401
    .end local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    :pswitch_d
    sget-boolean v18, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isVerizon:Z

    if-eqz v18, :cond_0

    .line 402
    const v18, 0x7f0d002e

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->makeBasicDialogBuilder(Landroid/content/Context;II)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 409
    .restart local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    :goto_1
    const v18, 0x7f0d0033

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 411
    const v18, 0x7f0d0006

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$17;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$17;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 420
    const v18, 0x7f0d0009

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$18;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$18;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 429
    move/from16 v0, p4

    invoke-static {v3, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->show(Landroid/app/AlertDialog$Builder;Z)Landroid/app/AlertDialog;

    move-result-object v12

    goto/16 :goto_0

    .line 405
    .end local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    :cond_0
    const v18, 0x7f0d002d

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->makeBasicDialogBuilder(Landroid/content/Context;II)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .restart local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    goto :goto_1

    .line 432
    .end local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    :pswitch_e
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->makeBasicDialogBuilder(Landroid/content/Context;I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 433
    .restart local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    const v18, 0x7f0d0033

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 435
    const v18, 0x7f030014

    const/16 v19, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 436
    .local v6, "confirm_dialog":Landroid/view/View;
    const v18, 0x7f08003a

    move/from16 v0, v18

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    const v19, 0x7f0d002c

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setText(I)V

    .line 438
    const v18, 0x7f08003c

    move/from16 v0, v18

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultCheckBox;

    .line 439
    .local v8, "dcb":Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultCheckBox;
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v8, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultCheckBox;->setChecked(Z)V

    .line 440
    new-instance v18, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$19;

    invoke-direct/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$19;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v8, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultCheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 446
    const v18, 0x7f08003b

    move/from16 v0, v18

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/LinearLayout;

    check-cast v18, Landroid/widget/LinearLayout;

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$20;

    move-object/from16 v0, v19

    invoke-direct {v0, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$20;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultCheckBox;)V

    .line 447
    invoke-virtual/range {v18 .. v19}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 452
    invoke-virtual {v3, v6}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 453
    const v18, 0x7f0d000e

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$21;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$21;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultCheckBox;)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 465
    const v18, 0x7f0d0006

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$22;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$22;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 474
    move/from16 v0, p4

    invoke-static {v3, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->show(Landroid/app/AlertDialog$Builder;Z)Landroid/app/AlertDialog;

    move-result-object v12

    goto/16 :goto_0

    .line 478
    .end local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    .end local v6    # "confirm_dialog":Landroid/view/View;
    .end local v8    # "dcb":Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultCheckBox;
    :pswitch_f
    const v18, 0x7f0d002f

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->makeBasicDialogBuilder(Landroid/content/Context;II)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 481
    .restart local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    const v18, 0x7f0d002b

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 483
    const v18, 0x7f0d006c

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$23;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$23;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 492
    const v18, 0x7f0d000e

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$24;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$24;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 501
    move/from16 v0, p4

    invoke-static {v3, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->show(Landroid/app/AlertDialog$Builder;Z)Landroid/app/AlertDialog;

    move-result-object v12

    goto/16 :goto_0

    .line 503
    .end local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    :pswitch_10
    const v18, 0x7f0d0037

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->makeBasicDialogBuilder(Landroid/content/Context;II)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 505
    .restart local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    const v18, 0x7f0d0036

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 506
    const v18, 0x7f0d0067

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$25;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$25;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 515
    const v18, 0x7f0d0068

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$26;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$26;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 524
    move/from16 v0, p4

    invoke-static {v3, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->show(Landroid/app/AlertDialog$Builder;Z)Landroid/app/AlertDialog;

    move-result-object v12

    goto/16 :goto_0

    .line 526
    .end local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    :pswitch_11
    const/16 v18, 0x1

    move/from16 v0, p5

    move/from16 v1, v18

    if-le v0, v1, :cond_1

    .line 527
    const v18, 0x7f0d0077

    .line 528
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v18

    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    aput-object v21, v19, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    .line 527
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->makeBasicDialogBuilder(Landroid/content/Context;Ljava/lang/String;I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 530
    .restart local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    const v18, 0x7f0d00e8

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 537
    :goto_2
    const v18, 0x7f0d0002

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$27;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$27;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 545
    const v18, 0x7f0d0009

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$28;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$28;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 553
    move/from16 v0, p4

    invoke-static {v3, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->show(Landroid/app/AlertDialog$Builder;Z)Landroid/app/AlertDialog;

    move-result-object v12

    goto/16 :goto_0

    .line 532
    .end local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    :cond_1
    const v18, 0x7f0d0028

    .line 533
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 532
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->makeBasicDialogBuilder(Landroid/content/Context;Ljava/lang/String;I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 534
    .restart local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    const v18, 0x7f0d00e7

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    goto :goto_2

    .line 556
    .end local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    :pswitch_12
    const v18, 0x7f0d0078

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->makeBasicDialogBuilder(Landroid/content/Context;II)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 558
    .restart local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    const/16 v18, 0x1

    move/from16 v0, p5

    move/from16 v1, v18

    if-ne v0, v1, :cond_2

    .line 559
    const v18, 0x7f0d00e7

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 563
    :goto_3
    const v18, 0x7f0d000d

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$29;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$29;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 571
    const v18, 0x7f0d0009

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$30;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$30;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 579
    move/from16 v0, p4

    invoke-static {v3, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->show(Landroid/app/AlertDialog$Builder;Z)Landroid/app/AlertDialog;

    move-result-object v12

    goto/16 :goto_0

    .line 561
    :cond_2
    const v18, 0x7f0d00e8

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    goto :goto_3

    .line 582
    .end local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    :pswitch_13
    const/4 v15, -0x1

    .line 583
    .local v15, "strId":I
    const/16 v16, -0x1

    .line 584
    .local v16, "titleId":I
    const/4 v13, -0x1

    .line 585
    .local v13, "postiveBtnId":I
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f090011

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 586
    const v15, 0x7f0d0079

    .line 587
    const v16, 0x7f0d0033

    .line 588
    const v13, 0x7f0d000c

    .line 594
    :goto_4
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-static {v0, v15, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->makeBasicDialogBuilder(Landroid/content/Context;II)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 596
    .restart local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    move/from16 v0, v16

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 597
    new-instance v18, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$31;

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$31;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)V

    move-object/from16 v0, v18

    invoke-virtual {v3, v13, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 606
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f090011

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 607
    const v18, 0x7f0d0009

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$32;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$32;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 615
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 616
    new-instance v18, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$33;

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$33;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)V

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 625
    :cond_3
    move/from16 v0, p4

    invoke-static {v3, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->show(Landroid/app/AlertDialog$Builder;Z)Landroid/app/AlertDialog;

    move-result-object v12

    goto/16 :goto_0

    .line 590
    .end local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    :cond_4
    const v15, 0x7f0d00e2

    .line 591
    const v16, 0x7f0d00e4

    .line 592
    const v13, 0x7f0d0008

    goto :goto_4

    .line 627
    .end local v13    # "postiveBtnId":I
    .end local v15    # "strId":I
    .end local v16    # "titleId":I
    :pswitch_14
    const v18, 0x7f0d00bd

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->makeBasicDialogBuilder(Landroid/content/Context;II)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 629
    .restart local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    const v18, 0x7f0d00bc

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 631
    const v18, 0x7f0d000e

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$34;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$34;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 639
    const v18, 0x7f0d00be

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$35;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$35;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 647
    move/from16 v0, p4

    invoke-static {v3, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->show(Landroid/app/AlertDialog$Builder;Z)Landroid/app/AlertDialog;

    move-result-object v12

    goto/16 :goto_0

    .line 650
    .end local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    :pswitch_15
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->makeBasicDialogBuilder(Landroid/content/Context;I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 651
    .restart local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    const v18, 0x7f0d00bc

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 652
    const v18, 0x7f030014

    const/16 v19, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v17

    .line 653
    .local v17, "view":Landroid/view/View;
    const v18, 0x7f08003a

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    const v19, 0x7f0d00c1

    .line 654
    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setText(I)V

    .line 655
    const v18, 0x7f08003c

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultCheckBox;

    .line 656
    .local v5, "cb":Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultCheckBox;
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v5, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultCheckBox;->setChecked(Z)V

    .line 657
    new-instance v18, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$36;

    invoke-direct/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$36;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultCheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 661
    const v18, 0x7f08003b

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/LinearLayout;

    check-cast v18, Landroid/widget/LinearLayout;

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$37;

    move-object/from16 v0, v19

    invoke-direct {v0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$37;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultCheckBox;)V

    .line 662
    invoke-virtual/range {v18 .. v19}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 668
    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 669
    const v18, 0x7f0d0009

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$38;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$38;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultCheckBox;)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 681
    const v18, 0x7f0d0008

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$39;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$39;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultCheckBox;)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 692
    move/from16 v0, p4

    invoke-static {v3, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->show(Landroid/app/AlertDialog$Builder;Z)Landroid/app/AlertDialog;

    move-result-object v12

    goto/16 :goto_0

    .line 733
    .end local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    .end local v5    # "cb":Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultCheckBox;
    .end local v17    # "view":Landroid/view/View;
    :pswitch_16
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->makeBasicDialogBuilder(Landroid/content/Context;I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 735
    .restart local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    const v18, 0x7f0d00c3

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 736
    const v18, 0x7f030016

    const/16 v19, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v11

    .line 738
    .local v11, "goto_apps":Landroid/view/View;
    const v18, 0x7f08003a

    move/from16 v0, v18

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    check-cast v18, Landroid/widget/TextView;

    const v19, 0x7f0d00c4

    .line 739
    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setText(I)V

    .line 759
    invoke-virtual {v3, v11}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 761
    const v18, 0x7f0d00c5

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$40;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$40;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 770
    const v18, 0x7f0d00c6

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$41;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$41;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 779
    move/from16 v0, p4

    invoke-static {v3, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->show(Landroid/app/AlertDialog$Builder;Z)Landroid/app/AlertDialog;

    move-result-object v12

    goto/16 :goto_0

    .line 783
    .end local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    .end local v11    # "goto_apps":Landroid/view/View;
    :pswitch_17
    const v18, 0x7f0d00d5

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->makeBasicDialogBuilder(Landroid/content/Context;II)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 785
    .restart local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    const v18, 0x7f0d00a4

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 786
    const v18, 0x7f0d0008

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$42;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$42;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 795
    move/from16 v0, p4

    invoke-static {v3, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->show(Landroid/app/AlertDialog$Builder;Z)Landroid/app/AlertDialog;

    move-result-object v12

    goto/16 :goto_0

    .line 798
    .end local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    :pswitch_18
    const v18, 0x7f0d00d6

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->makeBasicDialogBuilder(Landroid/content/Context;II)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 800
    .restart local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    const v18, 0x7f0d00a4

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 801
    const v18, 0x7f0d00d8

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$43;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$43;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 810
    const v18, 0x7f0d0009

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$44;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$44;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 819
    move/from16 v0, p4

    invoke-static {v3, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->show(Landroid/app/AlertDialog$Builder;Z)Landroid/app/AlertDialog;

    move-result-object v12

    goto/16 :goto_0

    .line 823
    .end local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    :pswitch_19
    const v18, 0x7f0d00d7

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->makeBasicDialogBuilder(Landroid/content/Context;II)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 825
    .restart local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    const v18, 0x7f0d00d9

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 826
    const v18, 0x7f0d0008

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$45;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$45;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 835
    const v18, 0x7f0d0009

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$46;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$46;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 844
    move/from16 v0, p4

    invoke-static {v3, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->show(Landroid/app/AlertDialog$Builder;Z)Landroid/app/AlertDialog;

    move-result-object v12

    goto/16 :goto_0

    .line 847
    .end local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    :pswitch_1a
    const/4 v4, 0x0

    .line 848
    .local v4, "autorefresh_time":Ljava/lang/String;
    const/16 v18, 0x6

    move/from16 v0, v18

    new-array v14, v0, [I

    fill-array-data v14, :array_0

    .line 851
    .local v14, "refreshTimes":[I
    const/16 v18, 0x1

    move/from16 v0, p5

    move/from16 v1, v18

    if-ne v0, v1, :cond_5

    .line 852
    const v18, 0x7f0d00cc

    .line 853
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 858
    :goto_5
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->makeBasicDialogBuilder(Landroid/content/Context;I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 860
    .restart local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    const v18, 0x7f0d0032

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 861
    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 863
    const v18, 0x7f0d0008

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$47;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$47;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 871
    move/from16 v0, p4

    invoke-static {v3, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->show(Landroid/app/AlertDialog$Builder;Z)Landroid/app/AlertDialog;

    move-result-object v12

    goto/16 :goto_0

    .line 855
    .end local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    :cond_5
    const v18, 0x7f0d00cb

    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aget v21, v14, p5

    .line 856
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    aput-object v21, v19, v20

    .line 855
    move-object/from16 v0, p0

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_5

    .line 874
    .end local v4    # "autorefresh_time":Ljava/lang/String;
    .end local v14    # "refreshTimes":[I
    :pswitch_1b
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->makeBasicDialogBuilder(Landroid/content/Context;I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 875
    .restart local v3    # "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    const v18, 0x7f0d0004

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 876
    const v18, 0x7f0d00e1

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 877
    const v18, 0x7f0d00df

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$48;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$48;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 886
    const v18, 0x7f0d00e0

    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$49;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog$49;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)V

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 895
    move/from16 v0, p4

    invoke-static {v3, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->show(Landroid/app/AlertDialog$Builder;Z)Landroid/app/AlertDialog;

    move-result-object v12

    goto/16 :goto_0

    .line 200
    nop

    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_5
        :pswitch_6
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_f
        :pswitch_0
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_0
        :pswitch_e
        :pswitch_1a
        :pswitch_1b
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
    .end packed-switch

    .line 848
    :array_0
    .array-data 4
        0x0
        0x1
        0x3
        0x6
        0xc
        0x18
    .end array-data
.end method
