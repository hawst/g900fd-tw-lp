.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;
.super Landroid/view/View;
.source "ColdIconView.java"

# interfaces
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;


# static fields
.field private static final ANI_TYPE_CIRCLE_ALPHA_0:I = 0x14

.field private static final ANI_TYPE_CIRCLE_ALPHA_1:I = 0x15

.field private static final ANI_TYPE_CIRCLE_ALPHA_2:I = 0x16

.field private static final ANI_TYPE_CIRCLE_ALPHA_3:I = 0x17

.field private static final ANI_TYPE_CIRCLE_ALPHA_4:I = 0x18

.field private static final ANI_TYPE_CIRCLE_ALPHA_5:I = 0x19

.field private static final ANI_TYPE_CIRCLE_TRANSLATE:I = 0x13

.field private static final ANI_TYPE_ICE_ROTATE:I = 0x12

.field private static final ANI_TYPE_ICE_TRANSLATE:I = 0x20

.field private static final ANI_TYPE_THERMOMETER_SCALE_Y:I = 0x11


# instance fields
.field isStop:Z

.field l:Landroid/animation/Animator$AnimatorListener;

.field private mAnimators:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/animation/ValueAnimator;",
            ">;"
        }
    .end annotation
.end field

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mCircleCubicPos:[[[F

.field private mCircleMovePos:[[F

.field private mIsActiveAnimationThread:Z

.field private mPaint:Landroid/graphics/Paint;

.field private mPaintColor:I

.field private mPath:Landroid/graphics/Path;

.field private mScale:F

.field rotateOffset:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 83
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 84
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 79
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x6

    .line 87
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mPaint:Landroid/graphics/Paint;

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mPath:Landroid/graphics/Path;

    .line 38
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    .line 62
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mAnimators:Ljava/util/HashMap;

    .line 64
    iput-boolean v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mIsActiveAnimationThread:Z

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mBitmap:Landroid/graphics/Bitmap;

    .line 72
    const v0, -0xa0a0b

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mPaintColor:I

    .line 211
    new-array v0, v4, [[[F

    const/4 v1, 0x4

    new-array v1, v1, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_0

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_1

    aput-object v2, v1, v7

    new-array v2, v4, [F

    fill-array-data v2, :array_2

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_3

    aput-object v2, v1, v8

    aput-object v1, v0, v6

    const/4 v1, 0x4

    new-array v1, v1, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_4

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_5

    aput-object v2, v1, v7

    new-array v2, v4, [F

    fill-array-data v2, :array_6

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_7

    aput-object v2, v1, v8

    aput-object v1, v0, v7

    const/4 v1, 0x4

    new-array v1, v1, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_8

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_9

    aput-object v2, v1, v7

    new-array v2, v4, [F

    fill-array-data v2, :array_a

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_b

    aput-object v2, v1, v8

    aput-object v1, v0, v5

    const/4 v1, 0x4

    new-array v1, v1, [[F

    new-array v2, v4, [F

    fill-array-data v2, :array_c

    aput-object v2, v1, v6

    new-array v2, v4, [F

    fill-array-data v2, :array_d

    aput-object v2, v1, v7

    new-array v2, v4, [F

    fill-array-data v2, :array_e

    aput-object v2, v1, v5

    new-array v2, v4, [F

    fill-array-data v2, :array_f

    aput-object v2, v1, v8

    aput-object v1, v0, v8

    const/4 v1, 0x4

    const/4 v2, 0x4

    new-array v2, v2, [[F

    new-array v3, v4, [F

    fill-array-data v3, :array_10

    aput-object v3, v2, v6

    new-array v3, v4, [F

    fill-array-data v3, :array_11

    aput-object v3, v2, v7

    new-array v3, v4, [F

    fill-array-data v3, :array_12

    aput-object v3, v2, v5

    new-array v3, v4, [F

    fill-array-data v3, :array_13

    aput-object v3, v2, v8

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const/4 v2, 0x4

    new-array v2, v2, [[F

    new-array v3, v4, [F

    fill-array-data v3, :array_14

    aput-object v3, v2, v6

    new-array v3, v4, [F

    fill-array-data v3, :array_15

    aput-object v3, v2, v7

    new-array v3, v4, [F

    fill-array-data v3, :array_16

    aput-object v3, v2, v5

    new-array v3, v4, [F

    fill-array-data v3, :array_17

    aput-object v3, v2, v8

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mCircleCubicPos:[[[F

    .line 276
    new-array v0, v4, [[F

    new-array v1, v5, [F

    fill-array-data v1, :array_18

    aput-object v1, v0, v6

    new-array v1, v5, [F

    fill-array-data v1, :array_19

    aput-object v1, v0, v7

    new-array v1, v5, [F

    fill-array-data v1, :array_1a

    aput-object v1, v0, v5

    new-array v1, v5, [F

    fill-array-data v1, :array_1b

    aput-object v1, v0, v8

    const/4 v1, 0x4

    new-array v2, v5, [F

    fill-array-data v2, :array_1c

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v5, [F

    fill-array-data v2, :array_1d

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mCircleMovePos:[[F

    .line 570
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->rotateOffset:F

    .line 571
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->l:Landroid/animation/Animator$AnimatorListener;

    .line 728
    iput-boolean v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->isStop:Z

    .line 88
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->init()V

    .line 89
    return-void

    .line 211
    nop

    :array_0
    .array-data 4
        0x43149df4
        0x425e3646
        0x43184b44
        0x424f7efa    # 51.874f
        0x43184b44
        0x423d6979    # 47.353f
    .end array-data

    :array_1
    .array-data 4
        0x43184b44
        0x422b54fe    # 42.833f
        0x43149db2
        0x421c9fbe
        0x43101893
        0x421c9fbe
    .end array-data

    :array_2
    .array-data 4
        0x430b9375
        0x421c9fbe
        0x4307e6a8    # 135.901f
        0x422b5604    # 42.834f
        0x4307e6a8    # 135.901f
        0x423d6979    # 47.353f
    .end array-data

    :array_3
    .array-data 4
        0x4307e5e3
        0x424f7efa    # 51.874f
        0x430b9375
        0x425e3646
        0x43101893
        0x425e3646
    .end array-data

    :array_4
    .array-data 4
        0x4332c560
        0x42cbb53f
        0x432f1893
        0x42d31062
        0x432f1893
        0x42dc1a1d
    .end array-data

    :array_5
    .array-data 4
        0x432f1893
        0x42e524dd
        0x4332c625
        0x42ec8083    # 118.251f
        0x43374ac1
        0x42ec8083    # 118.251f
    .end array-data

    :array_6
    .array-data 4
        0x433bd021
        0x42ec8083    # 118.251f
        0x433f7db2
        0x42e524dd
        0x433f7db2
        0x42dc1a1d
    .end array-data

    :array_7
    .array-data 4
        0x433f7db2
        0x42d31062
        0x433bd021
        0x42cbb53f
        0x43374ac1
        0x42cbb53f
    .end array-data

    :array_8
    .array-data 4
        0x430b9375
        0x432472f2
        0x4307e6a8    # 135.901f
        0x43282083
        0x4307e6a8    # 135.901f
        0x432ca560
    .end array-data

    :array_9
    .array-data 4
        0x4307e6a8    # 135.901f
        0x43312ac1
        0x430b93f8
        0x4334d7cf
        0x43101893
        0x4334d7cf
    .end array-data

    :array_a
    .array-data 4
        0x43149df4
        0x4334d7cf
        0x43184b44
        0x43312a3d
        0x43184b44
        0x432ca560
    .end array-data

    :array_b
    .array-data 4
        0x43184b44
        0x43282083
        0x43149db2
        0x432472f2
        0x43101893
        0x432472f2
    .end array-data

    :array_c
    .array-data 4
        0x4287e666    # 67.95f
        0x43245aa0
        0x42808bc7
        0x43280831
        0x42808bc7
        0x432c8c8b
    .end array-data

    :array_d
    .array-data 4
        0x42808bc7
        0x433111ec    # 177.07f
        0x4287e6e9
        0x4334c000    # 180.75f
        0x4290f0a4    # 72.47f
        0x4334c000    # 180.75f
    .end array-data

    :array_e
    .array-data 4
        0x4299fb64
        0x4334c000    # 180.75f
        0x42a15687
        0x433111ec    # 177.07f
        0x42a15687
        0x432c8c8b
    .end array-data

    :array_f
    .array-data 4
        0x42a15687
        0x4328076d
        0x4299fb64
        0x43245aa0
        0x4290f0a4    # 72.47f
        0x43245aa0
    .end array-data

    :array_10
    .array-data 4
        0x4200be77    # 32.186f
        0x42cbe3d7    # 101.945f
        0x41e4126f    # 28.509f
        0x42d33e77
        0x41e4126f    # 28.509f
        0x42dc47ae    # 110.14f
    .end array-data

    :array_11
    .array-data 4
        0x41e4126f    # 28.509f
        0x42e5526f
        0x4200bf7d    # 32.187f
        0x42ecae14    # 118.34f
        0x4212d2f2
        0x42ecae14    # 118.34f
    .end array-data

    :array_12
    .array-data 4
        0x4224e873
        0x42ecae14    # 118.34f
        0x42339eb8
        0x42e5526f
        0x42339eb8
        0x42dc47ae    # 110.14f
    .end array-data

    :array_13
    .array-data 4
        0x42339eb8
        0x42d33e77
        0x4224e979    # 41.228f
        0x42cbe3d7    # 101.945f
        0x4212d2f2
        0x42cbe3d7    # 101.945f
    .end array-data

    :array_14
    .array-data 4
        0x42999893
        0x425ef9db    # 55.744f
        0x42a0f439
        0x4250428f
        0x42a0f439
        0x423e2c08    # 47.543f
    .end array-data

    :array_15
    .array-data 4
        0x42a0f439
        0x422c178d
        0x42999893
        0x421d624e    # 39.346f
        0x42908d50    # 72.276f
        0x421d624e    # 39.346f
    .end array-data

    :array_16
    .array-data 4
        0x42878396
        0x421d624e    # 39.346f
        0x42802979
        0x422c1893
        0x42802979
        0x423e2c08    # 47.543f
    .end array-data

    :array_17
    .array-data 4
        0x42802979
        0x4250428f
        0x42878419
        0x425ef9db    # 55.744f
        0x42908d50    # 72.276f
        0x425ef9db    # 55.744f
    .end array-data

    .line 276
    :array_18
    .array-data 4
        0x43101893
        0x425e3646
    .end array-data

    :array_19
    .array-data 4
        0x43374ac1
        0x42cbb53f
    .end array-data

    :array_1a
    .array-data 4
        0x43101893
        0x432472f2
    .end array-data

    :array_1b
    .array-data 4
        0x4290f0a4    # 72.47f
        0x43245aa0
    .end array-data

    :array_1c
    .array-data 4
        0x4212d2f2
        0x42cbe3d7    # 101.945f
    .end array-data

    :array_1d
    .array-data 4
        0x42908d50    # 72.276f
        0x425ef9db    # 55.744f
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mAnimators:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mIsActiveAnimationThread:Z

    return v0
.end method

.method private drawIce(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "path"    # Landroid/graphics/Path;
    .param p3, "paint"    # Landroid/graphics/Paint;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const v11, 0x430ad4fe    # 138.832f

    const/high16 v10, 0x42e40000    # 114.0f

    const v9, 0x42dc0083    # 110.001f

    const v8, 0x4271f6c9

    const/high16 v7, 0x42d60000    # 107.0f

    .line 115
    invoke-virtual {p2}, Landroid/graphics/Path;->reset()V

    .line 117
    const v0, 0x432b8f1b

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x4294e9fc    # 74.457f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 118
    const v0, 0x432ac49c

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x42922b85    # 73.085f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x43294000    # 169.25f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x42909fbe

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x4327a9ba

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x4290f5c3    # 72.48f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 120
    const v0, 0x43014000    # 129.25f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x42995168

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 121
    const v0, 0x42e352f2

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x4225374c    # 41.304f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 122
    const v0, 0x42e20bc7

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x421f6979    # 39.853f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42df2c08

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x421bab02    # 38.917f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x42dc0106    # 110.002f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x421bab02    # 38.917f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 124
    const v0, 0x42d8d581    # 108.417f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x421bab02    # 38.917f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42d5f646

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x421f6979    # 39.853f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x42d4af1b

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x42253646

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 126
    const v0, 0x42b58106

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x42995168

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 127
    const v0, 0x42515917

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x4290f5c3    # 72.48f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 128
    const v0, 0x424b0937

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x4290a0c5

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x4244ef9e

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x42922b02    # 73.084f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x4241c396

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x4294e9fc    # 74.457f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 130
    const v0, 0x423e978d

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x4297a8f6    # 75.83f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x423ef6c9

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x429b14fe    # 77.541f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x4242b53f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x429da354    # 78.819f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 132
    const v0, 0x428f0083    # 71.501f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x42dc0000    # 110.0f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 133
    const v0, 0x4242b646

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x430d2d91

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 134
    const v0, 0x423ef6c9

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x430e74bc

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x423e9893

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x43102ac1

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x4241c49c

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x43118a3d    # 145.54f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 136
    const v0, 0x4244f0a4    # 49.235f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x4312e9ba

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x424b051f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x4313b062

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x42515a1d

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x4313849c

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 138
    const v0, 0x42b58106

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x430f5687

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 139
    const v0, 0x42d4af1b

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x4332b1ec

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 140
    const v0, 0x42d5f6c9

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x43342560

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42d8d581    # 108.417f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x4335153f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x42dc0106    # 110.002f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x4335153f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 142
    const v0, 0x42df2c08

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x4335153f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42e20bc7

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x43342560

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x42e352f2

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x4332b1ec

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 144
    const v0, 0x43014000    # 129.25f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x430f5687

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 145
    const v0, 0x4327a9ba

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x4313849c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 146
    const v0, 0x43293eb8

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x4313b021

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x432ac45a

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x4312e9fc

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x432b8f1b

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x43118a3d    # 145.54f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 148
    const v0, 0x432c5a1d

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x43102ac1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x432c424e

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x430e74bc

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x432b52b0

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x430d2d91

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 150
    const v0, 0x43147fbe

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x42dc0000    # 110.0f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 151
    const v0, 0x432b51ec    # 171.32f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x429da354    # 78.819f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 152
    const v0, 0x432c424e

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x429b14fe    # 77.541f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x432c5a1d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x4297a873

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x432b8f1b

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x4294e9fc    # 74.457f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 154
    invoke-virtual {p2}, Landroid/graphics/Path;->close()V

    .line 155
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v9

    const v1, 0x425353f8    # 52.832f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 156
    const v0, 0x42f637cf

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x42a52148

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 157
    const v0, 0x42f687ae    # 123.265f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x42a5d604    # 82.918f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42f6f2b0    # 123.474f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x42a6774c

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x42f76d0e

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x42a708b4

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 159
    const v0, 0x42e1828f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x42cd0000    # 102.5f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 160
    const v0, 0x42c58ccd

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x429c93f8

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 161
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v9

    const v1, 0x425353f8    # 52.832f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 162
    invoke-virtual {p2}, Landroid/graphics/Path;->close()V

    .line 163
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v8

    const v1, 0x42a3547b    # 81.665f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 164
    const v0, 0x42b99810

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x42aa1ba6    # 85.054f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 165
    const v0, 0x42b9fbe7

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x42aa2666

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42ba5fbe

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x42aa87ae    # 85.265f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x42bac28f    # 93.38f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x42aa8396

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 167
    const v0, 0x42d3a666

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v7

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 168
    const v0, 0x429db4bc

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v7

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 169
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v8

    const v1, 0x42a3547b    # 81.665f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 170
    invoke-virtual {p2}, Landroid/graphics/Path;->close()V

    .line 171
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v8

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v11

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 172
    const v0, 0x429db4bc

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v10

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 173
    const v0, 0x42d4ced9    # 106.404f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v10

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 174
    const v0, 0x42bbd99a

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x43075c6a

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 175
    const v0, 0x42bb6560

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x4307526f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42baef1b

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x43072b85    # 135.17f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x42ba76c9

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x43072b85    # 135.17f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 177
    const v0, 0x42ba2d0e

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x43072b85    # 135.17f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42b9e2d1    # 92.943f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x43073d71    # 135.24f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x42b99810

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x43074189

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 179
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v8

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v11

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 180
    invoke-virtual {p2}, Landroid/graphics/Path;->close()V

    .line 181
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v9

    const v1, 0x43272b02    # 167.168f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 182
    const v0, 0x42c50d50    # 98.526f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x430d251f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 183
    const v0, 0x42e05a1d

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x42eb0000    # 117.5f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 184
    const v0, 0x42f6c625

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x4308eb02    # 136.918f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 185
    const v0, 0x42f69168

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x430914bc

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42f66148    # 123.19f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x43094042

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x42f63852    # 123.11f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x43096e98

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 187
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v9

    const v1, 0x43272b02    # 167.168f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 188
    invoke-virtual {p2}, Landroid/graphics/Path;->close()V

    .line 189
    const v0, 0x431f824e

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v11

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 190
    const v0, 0x4303d99a    # 131.85f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x4307f2f2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 191
    const v0, 0x42ee353f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v10

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 192
    const v0, 0x430d25a2

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v10

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 193
    const v0, 0x431f824e

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v11

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 194
    invoke-virtual {p2}, Landroid/graphics/Path;->close()V

    .line 195
    const v0, 0x430d2560

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v7

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 196
    const v0, 0x42ed0dd3    # 118.527f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v7

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 197
    const v0, 0x43033c6a

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x42a97ae1    # 84.74f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 198
    const v0, 0x431f81cb

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x42a3947b    # 81.79f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 199
    const v0, 0x430d2560

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v7

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 200
    invoke-virtual {p2}, Landroid/graphics/Path;->close()V

    .line 202
    sget-object v0, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {p2, v0}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 204
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mPaintColor:I

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 205
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 207
    invoke-virtual {p1, p2, p3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 209
    return-void
.end method

.method private drawIceCircle(ILandroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V
    .locals 9
    .param p1, "position"    # I
    .param p2, "canvas"    # Landroid/graphics/Canvas;
    .param p3, "path"    # Landroid/graphics/Path;
    .param p4, "paint"    # Landroid/graphics/Paint;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 293
    invoke-virtual {p3}, Landroid/graphics/Path;->reset()V

    .line 295
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mCircleMovePos:[[F

    aget-object v0, v0, p1

    const/4 v1, 0x0

    aget v0, v0, v1

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mCircleMovePos:[[F

    aget-object v1, v1, p1

    const/4 v2, 0x1

    aget v1, v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 296
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mCircleCubicPos:[[[F

    aget-object v7, v0, p1

    .line 297
    .local v7, "cubicPos":[[F
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    const/4 v0, 0x4

    if-ge v8, v0, :cond_0

    .line 298
    aget-object v0, v7, v8

    const/4 v1, 0x0

    aget v0, v0, v1

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v0

    aget-object v0, v7, v8

    const/4 v2, 0x1

    aget v0, v0, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v2, v0

    aget-object v0, v7, v8

    const/4 v3, 0x2

    aget v0, v0, v3

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v3, v0

    aget-object v0, v7, v8

    const/4 v4, 0x3

    aget v0, v0, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v0

    aget-object v0, v7, v8

    const/4 v5, 0x4

    aget v0, v0, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v5, v0

    aget-object v0, v7, v8

    const/4 v6, 0x5

    aget v0, v0, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p3

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 297
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 301
    :cond_0
    invoke-virtual {p3}, Landroid/graphics/Path;->close()V

    .line 303
    invoke-virtual {p2, p3, p4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 304
    return-void
.end method

.method private drawThermometer(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "path"    # Landroid/graphics/Path;
    .param p3, "paint"    # Landroid/graphics/Paint;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 356
    invoke-virtual {p2}, Landroid/graphics/Path;->reset()V

    .line 358
    invoke-direct {p0, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->getThermometerOutLinePath(Landroid/graphics/Path;)V

    .line 359
    invoke-direct {p0, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->getThermometerInnerLinePath(Landroid/graphics/Path;)V

    .line 361
    sget-object v0, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {p2, v0}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 363
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 364
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mPaintColor:I

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 365
    invoke-virtual {p1, p2, p3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 367
    return-void
.end method

.method private drawThermometerAnimationArea(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "path"    # Landroid/graphics/Path;
    .param p3, "paint"    # Landroid/graphics/Paint;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const v11, 0x4329ab02    # 169.668f

    const/high16 v10, 0x424c0000    # 51.0f

    const v9, 0x430eed50

    const/high16 v8, 0x42580000    # 54.0f

    const/high16 v7, 0x42400000    # 48.0f

    .line 389
    invoke-virtual {p2}, Landroid/graphics/Path;->reset()V

    .line 395
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v10

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v9

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 396
    const v0, 0x42455f3b

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v2, v9, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v3, v7, v0

    const v0, 0x4310451f    # 144.27f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v5, v7, v0

    const v0, 0x4311ed50

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 398
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v7

    const v1, 0x431d6d50

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 400
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v7

    const v1, 0x431b2b02    # 155.168f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 401
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v7

    const v1, 0x4326ab02    # 166.668f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 402
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v1, v7, v0

    const v0, 0x432852f2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42455f3b

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v4, v11, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v5, v10, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v6, v11, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 404
    const v0, 0x4252a0c5

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v2, v11, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v3, v8, v0

    const v0, 0x432852f2

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v5, v8, v0

    const v0, 0x4326ab02    # 166.668f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 406
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v8

    const v1, 0x431b2b02    # 155.168f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 413
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v8

    const v1, 0x431d6d50

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 414
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v8

    const v1, 0x4311ed50

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 415
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v1, v8, v0

    const v0, 0x4310451f    # 144.27f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x4252a1cb    # 52.658f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v4, v9, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v5, v10, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v6, v9, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 417
    invoke-virtual {p2}, Landroid/graphics/Path;->close()V

    .line 419
    const v0, -0xffa501

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 420
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 422
    invoke-virtual {p1, p2, p3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 423
    return-void
.end method

.method private drawThermometerMarking(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "path"    # Landroid/graphics/Path;
    .param p3, "paint"    # Landroid/graphics/Paint;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const v11, 0x4339d581    # 185.834f

    const v10, 0x432daac1

    const v9, 0x43218001    # 161.50002f

    const/high16 v8, 0x421c0000    # 39.0f

    const v7, 0x427d5604    # 63.334f

    .line 370
    invoke-virtual {p2}, Landroid/graphics/Path;->reset()V

    .line 371
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v7

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v10

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 372
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v1, v7, v0

    const v0, 0x433462fc

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42678bee

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v4, v11, v0

    const v0, 0x424cab02    # 51.167f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v5, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v6, v11, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 374
    const v0, 0x4231ca16

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v2, v11, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v3, v8, v0

    const v0, 0x433462fc

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v5, v8, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v6, v10, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 376
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v1, v8, v0

    const v0, 0x4326f286

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x4231ca16

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v4, v9, v0

    const v0, 0x424cab02    # 51.167f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v5, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v6, v9, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 378
    const v0, 0x42678bee

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v2, v9, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v3, v7, v0

    const v0, 0x4326f286

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v5, v7, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v6, v10, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 380
    invoke-virtual {p2}, Landroid/graphics/Path;->close()V

    .line 382
    const v0, -0xffa501

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 383
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 385
    invoke-virtual {p1, p2, p3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 386
    return-void
.end method

.method private drawTransparentArea(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Path;Landroid/graphics/Paint;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "path"    # Landroid/graphics/Path;
    .param p4, "paint"    # Landroid/graphics/Paint;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 436
    new-instance v0, Landroid/graphics/PorterDuffXfermode;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v1}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {p4, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 437
    invoke-virtual {p1, p2, v2, v2, p4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 438
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 439
    return-void
.end method

.method private getMasking(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "path"    # Landroid/graphics/Path;
    .param p3, "paint"    # Landroid/graphics/Paint;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 426
    invoke-virtual {p2}, Landroid/graphics/Path;->reset()V

    .line 428
    invoke-direct {p0, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->getThermometerInnerLinePath(Landroid/graphics/Path;)V

    .line 430
    const/high16 v0, -0x10000

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 431
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 432
    invoke-virtual {p1, p2, p3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 433
    return-void
.end method

.method private getThermometerInnerLinePath(Landroid/graphics/Path;)V
    .locals 12
    .param p1, "path"    # Landroid/graphics/Path;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const v11, 0x428d0fdf

    const v10, 0x426c0106    # 59.001f

    const v9, 0x422c0106    # 43.001f

    const v8, 0x4203a7f0

    const v7, 0x43407604

    .line 307
    const v0, 0x424f3127    # 51.798f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v7

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 308
    const v0, 0x42255f3b

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v2, v7, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v3, v8, v0

    const v0, 0x4337fbe7

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v5, v8, v0

    const v0, 0x432d876d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 310
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v1, v8, v0

    const v0, 0x43291f7d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42088f5c    # 34.14f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x432512b0

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x4212a8f6    # 36.665f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x4321dae1

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 312
    const v0, 0x4219b958    # 38.431f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x431f9ae1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42200106    # 40.001f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x431dc3d7    # 157.765f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v5, v9, v0

    const v0, 0x431c8042

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 314
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v9

    const v1, 0x42bc2979

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 315
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v1, v9, v0

    const v0, 0x42b305a2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x4239b958    # 46.431f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x42ab9c29    # 85.805f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x424c0106    # 51.001f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x42ab9c29    # 85.805f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 317
    const v0, 0x425e48b4    # 55.571f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x42ab9c29    # 85.805f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v2, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v3, v10, v0

    const v0, 0x42b305a2

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v5, v10, v0

    const v0, 0x42bc2979

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 319
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v10

    const v1, 0x431c8042

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 320
    const/high16 v0, 0x427c0000    # 63.0f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x431dc3d7    # 157.765f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x4280e0c5

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x431f9ae1

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x428468f6

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x4321dae1

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 322
    const v0, 0x4289753f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x432512b0

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v2, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v3, v11, v0

    const v0, 0x43291f7d

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v5, v11, v0

    const v0, 0x432d876d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 324
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v1, v11, v0

    const v0, 0x4337fbe7

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42790312

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v4, v7, v0

    const v0, 0x424f3127    # 51.798f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v5, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v6, v7, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 326
    invoke-virtual {p1}, Landroid/graphics/Path;->close()V

    .line 327
    return-void
.end method

.method private getThermometerOutLinePath(Landroid/graphics/Path;)V
    .locals 12
    .param p1, "path"    # Landroid/graphics/Path;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const v11, 0x4282df3b

    const v10, 0x4282c9ba

    const v9, 0x4212e979    # 36.728f

    const v8, 0x41cfd2f2    # 25.978f

    const v7, 0x42bd0083    # 94.501f

    .line 330
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v11

    const v1, 0x431908b4

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 331
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v11

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v7

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 332
    const v0, 0x4282c106

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v7

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 333
    const v0, 0x4282c28f    # 65.38f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x42bcc72b    # 94.389f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v2, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v3, v10, v0

    const v0, 0x42bc8f5c    # 94.28f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v5, v10, v0

    const v0, 0x42bc5581    # 94.167f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 335
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v1, v10, v0

    const v0, 0x42ac8083    # 86.251f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x426be873

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x429fab02    # 79.834f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x424c3e77    # 51.061f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x429fab02    # 79.834f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 337
    const v0, 0x422c947b    # 43.145f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x429fab02    # 79.834f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v2, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v3, v9, v0

    const v0, 0x42ac8083    # 86.251f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v5, v9, v0

    const v0, 0x42bc5581    # 94.167f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 339
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v1, v9, v0

    const v0, 0x42bc8f5c    # 94.28f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x4212f7cf    # 36.742f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x42bcc72b    # 94.389f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x4212fae1

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v5, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v6, v7, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 341
    const v0, 0x4212be77    # 36.686f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v7

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 342
    const v0, 0x4212be77    # 36.686f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x431908b4

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 343
    const v0, 0x41f1b646    # 30.214f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x431d91ec    # 157.57f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v2, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v3, v8, v0

    const v0, 0x4325147b    # 165.08f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v5, v8, v0

    const v0, 0x432d9581    # 173.584f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 345
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v1, v8, v0

    const v0, 0x433b6fdf

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x4214d4fe    # 37.208f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x4346aac1

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x424c3e77    # 51.061f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x4346aac1

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 347
    const v0, 0x4281d3f8

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x4346aac1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x429849ba

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x433b6fdf

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x429849ba

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x432d9581    # 173.584f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 349
    const v0, 0x429849ba

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x4325147b    # 165.08f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x428fd0e5

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x431d91ec    # 157.57f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v5, v11, v0

    const v0, 0x431908b4

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 351
    invoke-virtual {p1}, Landroid/graphics/Path;->close()V

    .line 352
    return-void
.end method

.method private init()V
    .locals 5

    .prologue
    const/high16 v4, 0x43960000    # 300.0f

    .line 93
    invoke-virtual {p0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 94
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mPaint:Landroid/graphics/Paint;

    .line 95
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mPath:Landroid/graphics/Path;

    .line 97
    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v2, v4

    float-to-int v2, v2

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mBitmap:Landroid/graphics/Bitmap;

    .line 99
    new-instance v1, Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v1, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 101
    .local v1, "mMaskingCanvas":Landroid/graphics/Canvas;
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mPath:Landroid/graphics/Path;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mPaint:Landroid/graphics/Paint;

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->getMasking(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 106
    :goto_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mPaint:Landroid/graphics/Paint;

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mPaintColor:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 107
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 108
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 109
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mPaint:Landroid/graphics/Paint;

    const/high16 v3, 0x40a00000    # 5.0f

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 110
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mPaint:Landroid/graphics/Paint;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 112
    return-void

    .line 102
    :catch_0
    move-exception v0

    .line 103
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public cancelAnimation()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 711
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mAnimators:Ljava/util/HashMap;

    if-eqz v3, :cond_1

    .line 712
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 713
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 714
    .local v2, "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 715
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 716
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    .line 717
    .local v0, "ani":Landroid/animation/ValueAnimator;
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 718
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    goto :goto_0

    .line 723
    .end local v0    # "ani":Landroid/animation/ValueAnimator;
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    .end local v2    # "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_1
    const/4 v3, 0x0

    iput v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->rotateOffset:F

    .line 724
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mIsActiveAnimationThread:Z

    .line 726
    return-void
.end method

.method public getScale()F
    .locals 1

    .prologue
    .line 770
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 568
    return-object p0
.end method

.method public isRunning()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 747
    const/4 v3, 0x0

    .line 748
    .local v3, "ret":Z
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mAnimators:Ljava/util/HashMap;

    if-eqz v4, :cond_1

    .line 749
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 750
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 751
    .local v2, "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 752
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 753
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    .line 754
    .local v0, "ani":Landroid/animation/ValueAnimator;
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 755
    const/4 v3, 0x1

    .line 761
    .end local v0    # "ani":Landroid/animation/ValueAnimator;
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    .end local v2    # "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_1
    iget-boolean v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mIsActiveAnimationThread:Z

    or-int/2addr v4, v3

    return v4
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 26
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 443
    :try_start_0
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    const/4 v4, 0x0

    cmpg-float v3, v3, v4

    if-gtz v3, :cond_1

    .line 444
    const-string v3, ""

    const-string v4, "scale is less then 0"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 565
    :cond_0
    :goto_0
    return-void

    .line 448
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mAnimators:Ljava/util/HashMap;

    const/16 v4, 0x11

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/animation/ValueAnimator;

    .line 449
    .local v13, "ani":Landroid/animation/ValueAnimator;
    const/high16 v24, 0x3f800000    # 1.0f

    .line 450
    .local v24, "thermometerScaleY":F
    const/4 v12, 0x0

    .line 451
    .local v12, "angle":F
    const/4 v14, 0x0

    .line 452
    .local v14, "circleTranslate":F
    const/16 v19, 0x0

    .line 453
    .local v19, "iceTransX":F
    const/16 v20, 0x0

    .line 455
    .local v20, "iceTransY":F
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mIsActiveAnimationThread:Z

    if-eqz v3, :cond_5

    .line 456
    if-eqz v13, :cond_2

    .line 457
    invoke-virtual {v13}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v24

    .line 460
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mAnimators:Ljava/util/HashMap;

    const/16 v4, 0x12

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    .end local v13    # "ani":Landroid/animation/ValueAnimator;
    check-cast v13, Landroid/animation/ValueAnimator;

    .line 461
    .restart local v13    # "ani":Landroid/animation/ValueAnimator;
    if-eqz v13, :cond_3

    .line 462
    invoke-virtual {v13}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->rotateOffset:F

    add-float v12, v3, v4

    .line 465
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mAnimators:Ljava/util/HashMap;

    const/16 v4, 0x13

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    .end local v13    # "ani":Landroid/animation/ValueAnimator;
    check-cast v13, Landroid/animation/ValueAnimator;

    .line 466
    .restart local v13    # "ani":Landroid/animation/ValueAnimator;
    if-eqz v13, :cond_4

    .line 467
    invoke-virtual {v13}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v14

    .line 470
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mAnimators:Ljava/util/HashMap;

    const/16 v4, 0x20

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    .end local v13    # "ani":Landroid/animation/ValueAnimator;
    check-cast v13, Landroid/animation/ValueAnimator;

    .line 471
    .restart local v13    # "ani":Landroid/animation/ValueAnimator;
    if-eqz v13, :cond_5

    .line 472
    const-string v3, "transX"

    invoke-virtual {v13, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v19

    .line 473
    const-string v3, "transY"

    invoke-virtual {v13, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v20

    .line 483
    :cond_5
    const/4 v3, 0x6

    new-array v15, v3, [[F

    const/4 v3, 0x0

    const/4 v4, 0x2

    new-array v4, v4, [F

    const/4 v5, 0x0

    neg-float v6, v14

    aput v6, v4, v5

    const/4 v5, 0x1

    aput v14, v4, v5

    aput-object v4, v15, v3

    const/4 v3, 0x1

    const/4 v4, 0x2

    new-array v4, v4, [F

    const/4 v5, 0x0

    neg-float v6, v14

    aput v6, v4, v5

    const/4 v5, 0x1

    const/4 v6, 0x0

    aput v6, v4, v5

    aput-object v4, v15, v3

    const/4 v3, 0x2

    const/4 v4, 0x2

    new-array v4, v4, [F

    const/4 v5, 0x0

    neg-float v6, v14

    aput v6, v4, v5

    const/4 v5, 0x1

    neg-float v6, v14

    aput v6, v4, v5

    aput-object v4, v15, v3

    const/4 v3, 0x3

    const/4 v4, 0x2

    new-array v4, v4, [F

    const/4 v5, 0x0

    aput v14, v4, v5

    const/4 v5, 0x1

    neg-float v6, v14

    aput v6, v4, v5

    aput-object v4, v15, v3

    const/4 v3, 0x4

    const/4 v4, 0x2

    new-array v4, v4, [F

    const/4 v5, 0x0

    aput v14, v4, v5

    const/4 v5, 0x1

    const/4 v6, 0x0

    aput v6, v4, v5

    aput-object v4, v15, v3

    const/4 v3, 0x5

    const/4 v4, 0x2

    new-array v4, v4, [F

    const/4 v5, 0x0

    aput v14, v4, v5

    const/4 v5, 0x1

    aput v14, v4, v5

    aput-object v4, v15, v3

    .line 499
    .local v15, "circleTranslatePos":[[F
    const/4 v3, 0x6

    new-array v11, v3, [I

    fill-array-data v11, :array_0

    .line 504
    .local v11, "alphaAniKeys":[I
    const/16 v9, 0x1f

    .line 506
    .local v9, "saveFlag":I
    const/16 v9, 0xf

    .line 509
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v3

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v5, v3

    const/high16 v3, 0x43960000    # 300.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v6, v3

    const/high16 v3, 0x43960000    # 300.0f

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v7, v3

    const/4 v8, 0x0

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v9}, Landroid/graphics/Canvas;->saveLayer(FFFFLandroid/graphics/Paint;I)I

    move-result v25

    .line 512
    .local v25, "transparentSaveLevel":I
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_1
    const/4 v3, 0x6

    move/from16 v0, v17

    if-ge v0, v3, :cond_7

    .line 513
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v21

    .line 514
    .local v21, "saveLavel":I
    const/high16 v3, 0x42dc0000    # 110.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v3, v4

    const/high16 v4, 0x42dc0000    # 110.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v3, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 515
    aget-object v3, v15, v17

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v3, v4

    aget-object v4, v15, v17

    const/4 v5, 0x1

    aget v4, v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 516
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v3, v3, v19

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v4, v4, v20

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 517
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mPaintColor:I

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 518
    const/16 v10, 0xff

    .line 519
    .local v10, "alpha":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mAnimators:Ljava/util/HashMap;

    aget v4, v11, v17

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    .end local v13    # "ani":Landroid/animation/ValueAnimator;
    check-cast v13, Landroid/animation/ValueAnimator;

    .line 520
    .restart local v13    # "ani":Landroid/animation/ValueAnimator;
    if-eqz v13, :cond_6

    .line 522
    invoke-virtual {v13}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v10

    .line 524
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v10}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 525
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 526
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    move/from16 v1, v17

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->drawIceCircle(ILandroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 527
    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 512
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_1

    .line 530
    .end local v10    # "alpha":I
    .end local v21    # "saveLavel":I
    :cond_7
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v18

    .line 532
    .local v18, "iceSaveLavel":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mPaint:Landroid/graphics/Paint;

    const/16 v4, 0xff

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 533
    const/high16 v3, 0x42dc0000    # 110.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v3, v4

    const/high16 v4, 0x42dc0000    # 110.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v3, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 534
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v3, v3, v19

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float v4, v4, v20

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 535
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->drawIce(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 537
    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 539
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->drawTransparentArea(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 540
    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 542
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v23

    .line 544
    .local v23, "thermometerSaveLavel":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->drawThermometer(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 545
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->drawThermometerMarking(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 546
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v22

    .line 548
    .local v22, "thermometerAniLavel":I
    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v4, 0x424c0000    # 51.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4329ab02    # 169.668f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    mul-float/2addr v5, v6

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v3, v1, v4, v5}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 549
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->drawThermometerAnimationArea(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 551
    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 553
    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 555
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->isRunning()Z

    move-result v3

    if-nez v3, :cond_0

    .line 556
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->cancelAnimation()V

    .line 557
    const-string v3, ""

    const-string v4, "cancelA;"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 560
    .end local v9    # "saveFlag":I
    .end local v11    # "alphaAniKeys":[I
    .end local v12    # "angle":F
    .end local v13    # "ani":Landroid/animation/ValueAnimator;
    .end local v14    # "circleTranslate":F
    .end local v15    # "circleTranslatePos":[[F
    .end local v17    # "i":I
    .end local v18    # "iceSaveLavel":I
    .end local v19    # "iceTransX":F
    .end local v20    # "iceTransY":F
    .end local v22    # "thermometerAniLavel":I
    .end local v23    # "thermometerSaveLavel":I
    .end local v24    # "thermometerScaleY":F
    .end local v25    # "transparentSaveLevel":I
    :catch_0
    move-exception v16

    .line 562
    .local v16, "e":Ljava/lang/Exception;
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 499
    nop

    :array_0
    .array-data 4
        0x14
        0x15
        0x16
        0x17
        0x18
        0x19
    .end array-data
.end method

.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 803
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x0

    .line 778
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onViewDetachedFromWindow : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 779
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mAnimators:Ljava/util/HashMap;

    if-eqz v3, :cond_2

    .line 780
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 781
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 782
    .local v2, "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 783
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 784
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    .line 785
    .local v0, "ani":Landroid/animation/ValueAnimator;
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 786
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 787
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    goto :goto_0

    .line 790
    .end local v0    # "ani":Landroid/animation/ValueAnimator;
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    :cond_1
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    .line 793
    .end local v2    # "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_2
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mAnimators:Ljava/util/HashMap;

    .line 794
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mPaint:Landroid/graphics/Paint;

    .line 795
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mPath:Landroid/graphics/Path;

    .line 796
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_3

    .line 797
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 799
    :cond_3
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mBitmap:Landroid/graphics/Bitmap;

    .line 800
    return-void
.end method

.method public setPaintColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 75
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mPaintColor:I

    .line 76
    return-void
.end method

.method public setScale(F)V
    .locals 0
    .param p1, "scale"    # F

    .prologue
    .line 765
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mScale:F

    .line 766
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->init()V

    .line 767
    return-void
.end method

.method public setWidthAndHeight(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 774
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, p1, p2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 775
    .local v0, "l":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 776
    return-void
.end method

.method public startAnimation()V
    .locals 15

    .prologue
    .line 630
    const/4 v11, 0x0

    iput-boolean v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->isStop:Z

    .line 631
    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v11}, Ljava/util/HashMap;->size()I

    move-result v11

    if-lez v11, :cond_2

    .line 632
    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v11}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 633
    .local v5, "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 634
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 635
    .local v4, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/animation/ValueAnimator;

    .line 636
    .local v2, "ani":Landroid/animation/ValueAnimator;
    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 637
    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->end()V

    goto :goto_0

    .line 640
    .end local v2    # "ani":Landroid/animation/ValueAnimator;
    .end local v4    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    :cond_1
    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v11}, Ljava/util/HashMap;->clear()V

    .line 642
    .end local v5    # "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_2
    const/4 v11, 0x0

    iput v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->rotateOffset:F

    .line 644
    const/4 v11, 0x3

    new-array v11, v11, [F

    fill-array-data v11, :array_0

    invoke-static {v11}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v10

    .line 645
    .local v10, "thermometerScaleYAni":Landroid/animation/ValueAnimator;
    const/4 v11, -0x1

    invoke-virtual {v10, v11}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 646
    const-wide/16 v11, 0xbb8

    invoke-virtual {v10, v11, v12}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 647
    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v10, v11}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 648
    invoke-virtual {v10}, Landroid/animation/ValueAnimator;->start()V

    .line 649
    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mAnimators:Ljava/util/HashMap;

    const/16 v12, 0x11

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v11, v12, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 651
    const/4 v11, 0x2

    new-array v11, v11, [F

    fill-array-data v11, :array_1

    invoke-static {v11}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v7

    .line 652
    .local v7, "iceRotateAni":Landroid/animation/ValueAnimator;
    const/4 v11, -0x1

    invoke-virtual {v7, v11}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 653
    new-instance v11, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v11}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v7, v11}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 654
    const-wide/16 v11, 0xbb8

    invoke-virtual {v7, v11, v12}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 655
    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v7, v11}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 656
    invoke-virtual {v7}, Landroid/animation/ValueAnimator;->start()V

    .line 657
    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mAnimators:Ljava/util/HashMap;

    const/16 v12, 0x12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v11, v12, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 659
    const/4 v11, 0x3

    new-array v11, v11, [F

    fill-array-data v11, :array_2

    invoke-static {v11}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v3

    .line 660
    .local v3, "circleTranslateAni":Landroid/animation/ValueAnimator;
    const/4 v11, -0x1

    invoke-virtual {v3, v11}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 661
    const-wide/16 v11, 0x7d0

    invoke-virtual {v3, v11, v12}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 662
    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v3, v11}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 663
    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->start()V

    .line 664
    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mAnimators:Ljava/util/HashMap;

    const/16 v12, 0x13

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v11, v12, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 666
    const/4 v0, 0x0

    .line 667
    .local v0, "alphaAni":Landroid/animation/ValueAnimator;
    const/4 v11, 0x6

    new-array v1, v11, [I

    fill-array-data v1, :array_3

    .line 671
    .local v1, "alphaAniKeys":[I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    const/4 v11, 0x6

    if-ge v6, v11, :cond_3

    .line 672
    const/4 v11, 0x2

    new-array v11, v11, [I

    fill-array-data v11, :array_4

    invoke-static {v11}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 673
    const/4 v11, -0x1

    invoke-virtual {v0, v11}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 674
    const/4 v11, 0x2

    invoke-virtual {v0, v11}, Landroid/animation/ValueAnimator;->setRepeatMode(I)V

    .line 675
    const-wide/16 v11, 0x5dc

    invoke-virtual {v0, v11, v12}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 676
    mul-int/lit16 v11, v6, 0x1f4

    int-to-long v11, v11

    invoke-virtual {v0, v11, v12}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 677
    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v0, v11}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 678
    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mAnimators:Ljava/util/HashMap;

    aget v12, v1, v6

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v11, v12, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 679
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 671
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 682
    :cond_3
    const/4 v11, 0x2

    new-array v11, v11, [Landroid/animation/PropertyValuesHolder;

    const/4 v12, 0x0

    const-string v13, "transX"

    const/4 v14, 0x3

    new-array v14, v14, [F

    fill-array-data v14, :array_5

    .line 684
    invoke-static {v13, v14}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    const-string v13, "transY"

    const/4 v14, 0x3

    new-array v14, v14, [F

    fill-array-data v14, :array_6

    .line 685
    invoke-static {v13, v14}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v13

    aput-object v13, v11, v12

    .line 683
    invoke-static {v11}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v8

    .line 686
    .local v8, "iceTranslateX":Landroid/animation/ValueAnimator;
    const/4 v11, -0x1

    invoke-virtual {v8, v11}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 687
    const-wide/16 v11, 0x7d0

    invoke-virtual {v8, v11, v12}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 688
    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v8, v11}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 689
    invoke-virtual {v8}, Landroid/animation/ValueAnimator;->start()V

    .line 690
    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mAnimators:Ljava/util/HashMap;

    const/16 v12, 0x20

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v11, v12, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 692
    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->mIsActiveAnimationThread:Z

    .line 693
    new-instance v9, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView$2;

    invoke-direct {v9, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;)V

    .line 706
    .local v9, "t":Ljava/lang/Thread;
    const/16 v11, 0xa

    invoke-virtual {v9, v11}, Ljava/lang/Thread;->setPriority(I)V

    .line 707
    invoke-virtual {v9}, Ljava/lang/Thread;->start()V

    .line 708
    return-void

    .line 644
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3fc00000    # 1.5f
        0x3f800000    # 1.0f
    .end array-data

    .line 651
    :array_1
    .array-data 4
        0x0
        0x42700000    # 60.0f
    .end array-data

    .line 659
    :array_2
    .array-data 4
        0x0
        -0x3ee00000    # -10.0f
        0x0
    .end array-data

    .line 667
    :array_3
    .array-data 4
        0x14
        0x19
        0x17
        0x18
        0x15
        0x16
    .end array-data

    .line 672
    :array_4
    .array-data 4
        0x0
        0xff
    .end array-data

    .line 682
    :array_5
    .array-data 4
        0x0
        -0x3f600000    # -5.0f
        0x0
    .end array-data

    .line 684
    :array_6
    .array-data 4
        0x0
        0x40a00000    # 5.0f
        0x0
    .end array-data
.end method

.method public stopAnimation()V
    .locals 1

    .prologue
    .line 730
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ColdIconView;->isStop:Z

    .line 744
    return-void
.end method
