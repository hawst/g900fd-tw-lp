.class Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;
.super Landroid/widget/ArrayAdapter;
.source "MenuSearch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MenuAddAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private items:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "textViewResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1746
    .local p3, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;>;"
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 1747
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;->context:Landroid/content/Context;

    .line 1748
    iput-object p3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;->items:Ljava/util/ArrayList;

    .line 1749
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 1752
    move-object v3, p2

    .line 1753
    .local v3, "v":Landroid/view/View;
    if-nez v3, :cond_0

    .line 1754
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;->context:Landroid/content/Context;

    const-string v6, "layout_inflater"

    .line 1755
    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    .line 1756
    .local v4, "vi":Landroid/view/LayoutInflater;
    const v5, 0x7f030033

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 1758
    .end local v4    # "vi":Landroid/view/LayoutInflater;
    :cond_0
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;->items:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    .line 1759
    .local v2, "p":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    if-eqz v2, :cond_2

    .line 1760
    const v5, 0x7f08011b

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1761
    .local v0, "addRowCity":Landroid/widget/TextView;
    const v5, 0x7f08011c

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1762
    .local v1, "addRowState":Landroid/widget/TextView;
    if-eqz v0, :cond_1

    .line 1763
    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getCity()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1765
    :cond_1
    if-eqz v1, :cond_2

    .line 1766
    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getProvider()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_3

    .line 1767
    const v5, 0x7f0d0033

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1773
    .end local v0    # "addRowCity":Landroid/widget/TextView;
    .end local v1    # "addRowState":Landroid/widget/TextView;
    :cond_2
    :goto_0
    return-object v3

    .line 1769
    .restart local v0    # "addRowCity":Landroid/widget/TextView;
    .restart local v1    # "addRowState":Landroid/widget/TextView;
    :cond_3
    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getState()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
