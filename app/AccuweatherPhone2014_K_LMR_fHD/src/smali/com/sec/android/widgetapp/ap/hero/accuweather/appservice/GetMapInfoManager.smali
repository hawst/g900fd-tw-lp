.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;
.super Ljava/lang/Object;
.source "GetMapInfoManager.java"


# static fields
.field private static mActivity:Landroid/app/Activity;

.field private static mContext:Landroid/content/Context;

.field private static mInstance:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

.field private static mPerformMapCancled:Z

.field private static mTempScale:I

.field private static mUrlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;


# instance fields
.field private mAddedMapCityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

.field private mAdvHttpClient:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

.field private mErrorHandler:Landroid/os/Handler;

.field private mGetMapInfoUrl5Result:I

.field private mGetMapInfoUrl6Result:I

.field private mGetMapInfoUrl7Result:I

.field private mGetMapInfoUrlAddedCityInfoResult:I

.field public mHttpThreads:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Thread;",
            ">;"
        }
    .end annotation
.end field

.field private mMapInfoZoomList5:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mMapInfoZoomList6:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mMapInfoZoomList7:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mPostProcHandler:Landroid/os/Handler;

.field private syncstamp:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 25
    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mInstance:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    .line 26
    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mContext:Landroid/content/Context;

    .line 27
    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mActivity:Landroid/app/Activity;

    .line 28
    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mUrlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    .line 29
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mTempScale:I

    .line 45
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mPerformMapCancled:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mAddedMapCityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    .line 32
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList5:Ljava/util/ArrayList;

    .line 33
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList6:Ljava/util/ArrayList;

    .line 34
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList7:Ljava/util/ArrayList;

    .line 36
    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mGetMapInfoUrlAddedCityInfoResult:I

    .line 37
    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mGetMapInfoUrl5Result:I

    .line 38
    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mGetMapInfoUrl6Result:I

    .line 39
    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mGetMapInfoUrl7Result:I

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mHttpThreads:Ljava/util/ArrayList;

    .line 42
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mAdvHttpClient:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    .line 43
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->syncstamp:J

    .line 46
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mPostProcHandler:Landroid/os/Handler;

    .line 47
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mErrorHandler:Landroid/os/Handler;

    .line 51
    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 24
    sget-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mPerformMapCancled:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    .prologue
    .line 24
    iget-wide v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->syncstamp:J

    return-wide v0
.end method

.method static synthetic access$1000(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mPostProcHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1100()Landroid/content/Context;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mAddedMapCityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    return-object v0
.end method

.method static synthetic access$200()I
    .locals 1

    .prologue
    .line 24
    sget v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mTempScale:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList5:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->setGetMapInfoResult(IZ)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList6:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList7:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->postProcess()V

    return-void
.end method

.method static synthetic access$800()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mErrorHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private static clearResourceStatic()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 417
    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mUrlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    .line 418
    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mInstance:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    .line 419
    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mContext:Landroid/content/Context;

    .line 420
    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mActivity:Landroid/app/Activity;

    .line 422
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mPerformMapCancled:Z

    .line 423
    return-void
.end method

.method private createHttpThread()V
    .locals 1

    .prologue
    .line 367
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->stopHttpThreadAll()V

    .line 368
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->deleteHttpThreadAll()V

    .line 370
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mHttpThreads:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 371
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mHttpThreads:Ljava/util/ArrayList;

    .line 373
    :cond_0
    return-void
.end method

.method public static getInstance(Landroid/content/Context;Landroid/app/Activity;I)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "tempScale"    # I

    .prologue
    .line 58
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mInstance:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    if-nez v0, :cond_0

    .line 59
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    invoke-direct {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;-><init>()V

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mInstance:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    .line 62
    :cond_0
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->setContext(Landroid/content/Context;)V

    .line 63
    sput-object p1, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mActivity:Landroid/app/Activity;

    .line 64
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mUrlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    .line 65
    sput p2, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mTempScale:I

    .line 66
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mPerformMapCancled:Z

    .line 68
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mInstance:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    return-object v0
.end method

.method private getMapCityInfo(I)V
    .locals 9
    .param p1, "urlType"    # I

    .prologue
    .line 149
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getURL(Landroid/content/Context;I)Ljava/net/URL;

    move-result-object v3

    .line 151
    .local v3, "url":Ljava/net/URL;
    if-nez v3, :cond_0

    .line 152
    const-string v0, ""

    const-string v1, "url n"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    :goto_0
    return-void

    .line 156
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 173
    const-string v0, ""

    const-string v1, "invalid urlType"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 158
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList5:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 159
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList5:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 177
    :cond_1
    :goto_1
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mUrlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeHeader()Lorg/apache/http/message/HeaderGroup;

    move-result-object v4

    .line 180
    .local v4, "headerGroup":Lorg/apache/http/message/HeaderGroup;
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mHttpThreads:Ljava/util/ArrayList;

    if-nez v0, :cond_2

    .line 181
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mHttpThreads:Ljava/util/ArrayList;

    .line 184
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mAdvHttpClient:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    if-nez v0, :cond_3

    .line 185
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->syncstamp:J

    .line 186
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mContext:Landroid/content/Context;

    const/4 v2, 0x1

    iget-wide v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->syncstamp:J

    invoke-direct {v0, v1, v2, v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;-><init>(Landroid/content/Context;ZJ)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mAdvHttpClient:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    .line 189
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mAdvHttpClient:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    sget-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mContext:Landroid/content/Context;

    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$1;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;)V

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->get(ILandroid/content/Context;Ljava/net/URL;Lorg/apache/http/message/HeaderGroup;Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;)Ljava/lang/Thread;

    move-result-object v6

    .line 319
    .local v6, "t":Ljava/lang/Thread;
    if-eqz v6, :cond_4

    .line 320
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 321
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "t s : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 163
    .end local v4    # "headerGroup":Lorg/apache/http/message/HeaderGroup;
    .end local v6    # "t":Ljava/lang/Thread;
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList6:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 164
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList6:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_1

    .line 168
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList7:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 169
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList7:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_1

    .line 324
    .restart local v4    # "headerGroup":Lorg/apache/http/message/HeaderGroup;
    .restart local v6    # "t":Ljava/lang/Thread;
    :cond_4
    const-string v0, ""

    const-string v1, "t is null"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 156
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static getPerformMapCancled()Z
    .locals 1

    .prologue
    .line 454
    sget-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mPerformMapCancled:Z

    return v0
.end method

.method private initGetMapInfoResult()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 101
    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mGetMapInfoUrlAddedCityInfoResult:I

    .line 102
    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mGetMapInfoUrl5Result:I

    .line 103
    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mGetMapInfoUrl6Result:I

    .line 104
    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mGetMapInfoUrl7Result:I

    .line 105
    return-void
.end method

.method private initMapInfoZoomList()V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList5:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList5:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 87
    :goto_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList6:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 88
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList6:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 93
    :goto_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList7:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 94
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList7:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 98
    :goto_2
    return-void

    .line 84
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList5:Ljava/util/ArrayList;

    goto :goto_0

    .line 90
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList6:Ljava/util/ArrayList;

    goto :goto_1

    .line 96
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList7:Ljava/util/ArrayList;

    goto :goto_2
.end method

.method private postProcess()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 330
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mGetMapInfoUrl5Result:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mGetMapInfoUrl6Result:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mGetMapInfoUrl7Result:I

    if-ne v0, v1, :cond_0

    .line 335
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList5:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList6:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->removeDuplicatedItem(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 336
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList5:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList7:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->removeDuplicatedItem(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 337
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList6:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList7:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->removeDuplicatedItem(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 339
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$2;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 347
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mAdvHttpClient:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    .line 348
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->deleteHttpThreadAll()V

    .line 353
    :goto_0
    return-void

    .line 350
    :cond_0
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pP not r c["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mGetMapInfoUrlAddedCityInfoResult:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mGetMapInfoUrl5Result:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mGetMapInfoUrl6Result:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mGetMapInfoUrl7Result:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static setContext(Landroid/content/Context;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 54
    sput-object p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mContext:Landroid/content/Context;

    .line 55
    return-void
.end method

.method private setGetMapInfoResult(IZ)V
    .locals 1
    .param p1, "zoomUrl"    # I
    .param p2, "isOk"    # Z

    .prologue
    .line 108
    const/4 v0, 0x0

    .line 110
    .local v0, "resultCode":I
    if-eqz p2, :cond_0

    .line 111
    const/4 v0, 0x1

    .line 116
    :goto_0
    packed-switch p1, :pswitch_data_0

    .line 130
    :goto_1
    return-void

    .line 113
    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    .line 118
    :pswitch_0
    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mGetMapInfoUrl5Result:I

    goto :goto_1

    .line 121
    :pswitch_1
    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mGetMapInfoUrl6Result:I

    goto :goto_1

    .line 124
    :pswitch_2
    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mGetMapInfoUrl7Result:I

    goto :goto_1

    .line 127
    :pswitch_3
    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mGetMapInfoUrlAddedCityInfoResult:I

    goto :goto_1

    .line 116
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static setPerformMapCancled(Z)V
    .locals 0
    .param p0, "isCancled"    # Z

    .prologue
    .line 450
    sput-boolean p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mPerformMapCancled:Z

    .line 451
    return-void
.end method

.method private stopHttpThreadAll()V
    .locals 3

    .prologue
    .line 376
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mHttpThreads:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 377
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 378
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Thread;

    .line 379
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 383
    .end local v0    # "t":Ljava/lang/Thread;
    :cond_0
    return-void
.end method


# virtual methods
.method public clearResource()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 386
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mPostProcHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 387
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mPostProcHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 388
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mPostProcHandler:Landroid/os/Handler;

    .line 391
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mErrorHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 392
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mErrorHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 393
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mErrorHandler:Landroid/os/Handler;

    .line 396
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList5:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 397
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList5:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 398
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList5:Ljava/util/ArrayList;

    .line 401
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList6:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 402
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList6:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 403
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList6:Ljava/util/ArrayList;

    .line 406
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList7:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    .line 407
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList7:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 408
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList7:Ljava/util/ArrayList;

    .line 411
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->deleteHttpThreadAll()V

    .line 413
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->clearResourceStatic()V

    .line 414
    return-void
.end method

.method public deleteHttpThreadAll()V
    .locals 2

    .prologue
    .line 356
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->stopHttpThreadAll()V

    .line 358
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mHttpThreads:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 359
    :goto_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 360
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mHttpThreads:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 362
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mHttpThreads:Ljava/util/ArrayList;

    .line 364
    :cond_1
    return-void
.end method

.method public getAddedMapCityInfoLocation()V
    .locals 10

    .prologue
    .line 462
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mAddedMapCityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    if-nez v0, :cond_0

    .line 463
    const-string v0, ""

    const-string v1, "gAMCIL mAMCI n"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 466
    :cond_0
    new-instance v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mContext:Landroid/content/Context;

    invoke-direct {v7, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;-><init>(Landroid/content/Context;)V

    .line 467
    .local v7, "urlManager":Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;
    const/4 v3, 0x0

    .line 469
    .local v3, "url":Ljava/net/URL;
    if-eqz v7, :cond_1

    .line 470
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mAddedMapCityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLocation()Ljava/lang/String;

    move-result-object v0

    sget v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mTempScale:I

    sget-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mContext:Landroid/content/Context;

    .line 471
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkLanguage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 470
    invoke-virtual {v7, v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeUrlForGetDetailData(Ljava/lang/String;ILjava/lang/String;)Ljava/net/URL;

    move-result-object v3

    .line 474
    :cond_1
    if-nez v3, :cond_2

    .line 475
    const-string v0, ""

    const-string v1, "url n"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 599
    :goto_0
    return-void

    .line 479
    :cond_2
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mUrlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeHeader()Lorg/apache/http/message/HeaderGroup;

    move-result-object v4

    .line 482
    .local v4, "headerGroup":Lorg/apache/http/message/HeaderGroup;
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mHttpThreads:Ljava/util/ArrayList;

    if-nez v0, :cond_3

    .line 483
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mHttpThreads:Ljava/util/ArrayList;

    .line 486
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mAdvHttpClient:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    if-nez v0, :cond_4

    .line 487
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->syncstamp:J

    .line 488
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mContext:Landroid/content/Context;

    const/4 v2, 0x1

    iget-wide v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->syncstamp:J

    invoke-direct {v0, v1, v2, v8, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;-><init>(Landroid/content/Context;ZJ)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mAdvHttpClient:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    .line 491
    :cond_4
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mAdvHttpClient:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    sget-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mContext:Landroid/content/Context;

    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$3;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$3;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;)V

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->get(ILandroid/content/Context;Ljava/net/URL;Lorg/apache/http/message/HeaderGroup;Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;)Ljava/lang/Thread;

    move-result-object v6

    .line 591
    .local v6, "t":Ljava/lang/Thread;
    if-eqz v6, :cond_5

    .line 592
    const/16 v0, 0x8

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 593
    const-string v0, ""

    const-string v1, "t s : 8"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 594
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 596
    :cond_5
    const-string v0, ""

    const-string v1, "t is null"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getMapInfoUrlAddedCityInfoResult()I
    .locals 1

    .prologue
    .line 602
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mGetMapInfoUrlAddedCityInfoResult:I

    return v0
.end method

.method public getMapZoomList(I)Ljava/util/ArrayList;
    .locals 3
    .param p1, "listType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 426
    const/4 v0, 0x0

    .line 428
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;>;"
    packed-switch p1, :pswitch_data_0

    .line 442
    :goto_0
    if-nez v0, :cond_0

    .line 443
    const-string v1, ""

    const-string v2, "gMZL l n"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 446
    :cond_0
    return-object v0

    .line 430
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList5:Ljava/util/ArrayList;

    .line 431
    goto :goto_0

    .line 434
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList6:Ljava/util/ArrayList;

    .line 435
    goto :goto_0

    .line 438
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mMapInfoZoomList7:Ljava/util/ArrayList;

    goto :goto_0

    .line 428
    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public performMapCities()V
    .locals 1

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->initGetMapInfoResult()V

    .line 134
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->initMapInfoZoomList()V

    .line 136
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->createHttpThread()V

    .line 138
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mAddedMapCityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    if-eqz v0, :cond_0

    .line 139
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->getAddedMapCityInfoLocation()V

    .line 142
    :cond_0
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->getMapCityInfo(I)V

    .line 143
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->getMapCityInfo(I)V

    .line 144
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->getMapCityInfo(I)V

    .line 145
    return-void
.end method

.method public setAddedMapCityInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;)V
    .locals 0
    .param p1, "mapCityInfo"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    .prologue
    .line 458
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mAddedMapCityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    .line 459
    return-void
.end method

.method public setErrorHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mErrorHandler:Landroid/os/Handler;

    .line 77
    return-void
.end method

.method public setPostProcHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mPostProcHandler:Landroid/os/Handler;

    .line 73
    return-void
.end method
