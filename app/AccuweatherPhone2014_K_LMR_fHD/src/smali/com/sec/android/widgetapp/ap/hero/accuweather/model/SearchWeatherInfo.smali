.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;
.super Ljava/lang/Object;
.source "SearchWeatherInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field mCurrentTemp:F

.field mDayHailProbability:I

.field mDayPrecipitationProbability:I

.field mDayRainProbability:I

.field mDaySnowProbability:I

.field mHighTemp:F

.field mIconNum:I

.field mLowTemp:F

.field mNightHailProbability:I

.field mNightPrecipitationProbability:I

.field mNightRainProbability:I

.field mNightSnowProbability:I

.field mRealFeel:F

.field mRelativeHumidity:Ljava/lang/String;

.field mSunRiseTime:Ljava/lang/String;

.field mSunSetTime:Ljava/lang/String;

.field mTempScale:I

.field mTimeZone:Ljava/lang/String;

.field mTimestamp:Ljava/lang/String;

.field mUVIndex:I

.field mUVIndexText:Ljava/lang/String;

.field mUpdateDate:Ljava/lang/String;

.field mUrl:Ljava/lang/String;

.field mWeatherText:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 168
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo$1;

    invoke-direct {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo$1;-><init>()V

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    return-void
.end method

.method public constructor <init>(FLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIFF)V
    .locals 0
    .param p1, "currentTemp"    # F
    .param p2, "timeZone"    # Ljava/lang/String;
    .param p3, "updateDate"    # Ljava/lang/String;
    .param p4, "weatherText"    # Ljava/lang/String;
    .param p5, "url"    # Ljava/lang/String;
    .param p6, "timestamp"    # Ljava/lang/String;
    .param p7, "tempScale"    # I
    .param p8, "iconNum"    # I
    .param p9, "highTemp"    # F
    .param p10, "lowTemp"    # F

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mCurrentTemp:F

    .line 64
    iput-object p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mTimeZone:Ljava/lang/String;

    .line 65
    iput-object p3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mUpdateDate:Ljava/lang/String;

    .line 66
    iput-object p4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mWeatherText:Ljava/lang/String;

    .line 67
    iput-object p5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mUrl:Ljava/lang/String;

    .line 68
    iput-object p6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mTimestamp:Ljava/lang/String;

    .line 69
    iput p7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mTempScale:I

    .line 70
    iput p8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mIconNum:I

    .line 71
    iput p9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mHighTemp:F

    .line 72
    iput p10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mLowTemp:F

    .line 73
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 138
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mCurrentTemp:F

    .line 139
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mTimeZone:Ljava/lang/String;

    .line 140
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mUpdateDate:Ljava/lang/String;

    .line 141
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mWeatherText:Ljava/lang/String;

    .line 142
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mUrl:Ljava/lang/String;

    .line 143
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mTimestamp:Ljava/lang/String;

    .line 144
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mTempScale:I

    .line 145
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mIconNum:I

    .line 146
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mHighTemp:F

    .line 147
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mLowTemp:F

    .line 148
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mRealFeel:F

    .line 149
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mSunRiseTime:Ljava/lang/String;

    .line 150
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mSunSetTime:Ljava/lang/String;

    .line 153
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mDayRainProbability:I

    .line 154
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mDaySnowProbability:I

    .line 155
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mDayHailProbability:I

    .line 156
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mDayPrecipitationProbability:I

    .line 158
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mNightRainProbability:I

    .line 159
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mNightSnowProbability:I

    .line 160
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mNightHailProbability:I

    .line 161
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mNightPrecipitationProbability:I

    .line 163
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mRelativeHumidity:Ljava/lang/String;

    .line 164
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mUVIndex:I

    .line 165
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mUVIndexText:Ljava/lang/String;

    .line 166
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;)V
    .locals 1
    .param p1, "todayInfo"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iget v0, p1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mCurrentTemp:F

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mCurrentTemp:F

    .line 77
    iget-object v0, p1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mTimeZone:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mTimeZone:Ljava/lang/String;

    .line 78
    iget-object v0, p1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mUpdateDate:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mUpdateDate:Ljava/lang/String;

    .line 79
    iget-object v0, p1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mWeatherText:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mWeatherText:Ljava/lang/String;

    .line 80
    iget-object v0, p1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mUrl:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mUrl:Ljava/lang/String;

    .line 81
    iget-object v0, p1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mTimestamp:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mTimestamp:Ljava/lang/String;

    .line 82
    iget v0, p1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mTempScale:I

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mTempScale:I

    .line 83
    iget v0, p1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mIconNum:I

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mIconNum:I

    .line 84
    iget v0, p1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mHighTemp:F

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mHighTemp:F

    .line 85
    iget v0, p1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mLowTemp:F

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mLowTemp:F

    .line 86
    iget-object v0, p1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mSunRiseTime:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mSunRiseTime:Ljava/lang/String;

    .line 87
    iget-object v0, p1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mSunSetTime:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mSunSetTime:Ljava/lang/String;

    .line 88
    iget v0, p1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mRealFeel:F

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mRealFeel:F

    .line 89
    iget v0, p1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDayRainProbability:I

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mDayRainProbability:I

    .line 90
    iget v0, p1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDaySnowProbability:I

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mDaySnowProbability:I

    .line 91
    iget v0, p1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDayHailProbability:I

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mDayHailProbability:I

    .line 92
    iget v0, p1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDayPrecipitationProbability:I

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mDayPrecipitationProbability:I

    .line 93
    iget v0, p1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightRainProbability:I

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mNightRainProbability:I

    .line 94
    iget v0, p1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightSnowProbability:I

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mNightSnowProbability:I

    .line 95
    iget v0, p1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightHailProbability:I

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mNightHailProbability:I

    .line 96
    iget v0, p1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightPrecipitationProbability:I

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mNightPrecipitationProbability:I

    .line 97
    iget-object v0, p1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mRelativeHumidity:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mRelativeHumidity:Ljava/lang/String;

    .line 98
    iget v0, p1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mUVIndex:I

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mUVIndex:I

    .line 99
    iget-object v0, p1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mUVIndexText:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mUVIndexText:Ljava/lang/String;

    .line 100
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 103
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public getCurrentTemp()F
    .locals 1

    .prologue
    .line 187
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mCurrentTemp:F

    return v0
.end method

.method public getDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mTimestamp:Ljava/lang/String;

    return-object v0
.end method

.method public getDayHailProbability()I
    .locals 1

    .prologue
    .line 304
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mDayHailProbability:I

    return v0
.end method

.method public getDayPrecipitationProbability()I
    .locals 1

    .prologue
    .line 312
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mDayPrecipitationProbability:I

    return v0
.end method

.method public getDayRainProbability()I
    .locals 1

    .prologue
    .line 288
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mDayRainProbability:I

    return v0
.end method

.method public getDaySnowProbability()I
    .locals 1

    .prologue
    .line 296
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mDaySnowProbability:I

    return v0
.end method

.method public getHighTemp()F
    .locals 1

    .prologue
    .line 219
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mHighTemp:F

    return v0
.end method

.method public getIconNum()I
    .locals 1

    .prologue
    .line 215
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mIconNum:I

    return v0
.end method

.method public getLowTemp()F
    .locals 1

    .prologue
    .line 223
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mLowTemp:F

    return v0
.end method

.method public getNightHailProbability()I
    .locals 1

    .prologue
    .line 336
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mNightHailProbability:I

    return v0
.end method

.method public getNightPrecipitationProbability()I
    .locals 1

    .prologue
    .line 344
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mNightPrecipitationProbability:I

    return v0
.end method

.method public getNightRainProbability()I
    .locals 1

    .prologue
    .line 320
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mNightRainProbability:I

    return v0
.end method

.method public getNightSnowProbability()I
    .locals 1

    .prologue
    .line 328
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mNightSnowProbability:I

    return v0
.end method

.method public getRealFeel()F
    .locals 1

    .prologue
    .line 227
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mRealFeel:F

    return v0
.end method

.method public getRelativeHumidity()Ljava/lang/String;
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mRelativeHumidity:Ljava/lang/String;

    return-object v0
.end method

.method public getSunRiseTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mSunRiseTime:Ljava/lang/String;

    return-object v0
.end method

.method public getSunSetTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mSunSetTime:Ljava/lang/String;

    return-object v0
.end method

.method public getTempScale()I
    .locals 1

    .prologue
    .line 211
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mTempScale:I

    return v0
.end method

.method public getTimeZone()Ljava/lang/String;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mTimeZone:Ljava/lang/String;

    return-object v0
.end method

.method public getUVIndex()I
    .locals 1

    .prologue
    .line 356
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mUVIndex:I

    return v0
.end method

.method public getUVIndexText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mUVIndexText:Ljava/lang/String;

    return-object v0
.end method

.method public getUpdateDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mUpdateDate:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getWeatherText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mWeatherText:Ljava/lang/String;

    return-object v0
.end method

.method public setCurrentTemp(F)V
    .locals 0
    .param p1, "mCurrentTemp"    # F

    .prologue
    .line 243
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mCurrentTemp:F

    .line 244
    return-void
.end method

.method public setDate(Ljava/lang/String;)V
    .locals 0
    .param p1, "timestamp"    # Ljava/lang/String;

    .prologue
    .line 263
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mTimestamp:Ljava/lang/String;

    .line 264
    return-void
.end method

.method public setDayHailProbability(I)V
    .locals 0
    .param p1, "hailprob"    # I

    .prologue
    .line 300
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mDayHailProbability:I

    .line 301
    return-void
.end method

.method public setDayPrecipitationProbability(I)V
    .locals 0
    .param p1, "precipitationprob"    # I

    .prologue
    .line 308
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mDayPrecipitationProbability:I

    .line 309
    return-void
.end method

.method public setDayRainProbability(I)V
    .locals 0
    .param p1, "rainprob"    # I

    .prologue
    .line 284
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mDayRainProbability:I

    .line 285
    return-void
.end method

.method public setDaySnowProbability(I)V
    .locals 0
    .param p1, "snowprob"    # I

    .prologue
    .line 292
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mDaySnowProbability:I

    .line 293
    return-void
.end method

.method public setHighTemp(F)V
    .locals 0
    .param p1, "mHighTemp"    # F

    .prologue
    .line 275
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mHighTemp:F

    .line 276
    return-void
.end method

.method public setIconNum(I)V
    .locals 0
    .param p1, "mIconNum"    # I

    .prologue
    .line 271
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mIconNum:I

    .line 272
    return-void
.end method

.method public setLowTemp(F)V
    .locals 0
    .param p1, "mLowTemp"    # F

    .prologue
    .line 279
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mLowTemp:F

    .line 280
    return-void
.end method

.method public setNightHailProbability(I)V
    .locals 0
    .param p1, "hailprob"    # I

    .prologue
    .line 332
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mNightHailProbability:I

    .line 333
    return-void
.end method

.method public setNightPrecipitationProbability(I)V
    .locals 0
    .param p1, "precipitationprob"    # I

    .prologue
    .line 340
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mNightPrecipitationProbability:I

    .line 341
    return-void
.end method

.method public setNightRainProbability(I)V
    .locals 0
    .param p1, "rainprob"    # I

    .prologue
    .line 316
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mNightRainProbability:I

    .line 317
    return-void
.end method

.method public setNightSnowProbability(I)V
    .locals 0
    .param p1, "snowprob"    # I

    .prologue
    .line 324
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mNightSnowProbability:I

    .line 325
    return-void
.end method

.method public setRealFeel(F)V
    .locals 0
    .param p1, "realfeel"    # F

    .prologue
    .line 231
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mRealFeel:F

    .line 232
    return-void
.end method

.method public setRelativeHumidity(Ljava/lang/String;)V
    .locals 0
    .param p1, "mRelativeHumidity"    # Ljava/lang/String;

    .prologue
    .line 352
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mRelativeHumidity:Ljava/lang/String;

    .line 353
    return-void
.end method

.method public setSunRiseTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "time"    # Ljava/lang/String;

    .prologue
    .line 235
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mSunRiseTime:Ljava/lang/String;

    .line 236
    return-void
.end method

.method public setSunSetTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "time"    # Ljava/lang/String;

    .prologue
    .line 239
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mSunSetTime:Ljava/lang/String;

    .line 240
    return-void
.end method

.method public setTempScale(I)V
    .locals 0
    .param p1, "mTempScale"    # I

    .prologue
    .line 267
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mTempScale:I

    .line 268
    return-void
.end method

.method public setTimeZone(Ljava/lang/String;)V
    .locals 0
    .param p1, "mTimeZone"    # Ljava/lang/String;

    .prologue
    .line 247
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mTimeZone:Ljava/lang/String;

    .line 248
    return-void
.end method

.method public setUVIndex(I)V
    .locals 0
    .param p1, "mUVIndex"    # I

    .prologue
    .line 360
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mUVIndex:I

    .line 361
    return-void
.end method

.method public setUVIndexText(Ljava/lang/String;)V
    .locals 0
    .param p1, "mUVIndexText"    # Ljava/lang/String;

    .prologue
    .line 368
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mUVIndexText:Ljava/lang/String;

    .line 369
    return-void
.end method

.method public setUpdateDate(Ljava/lang/String;)V
    .locals 0
    .param p1, "mUpdateDate"    # Ljava/lang/String;

    .prologue
    .line 251
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mUpdateDate:Ljava/lang/String;

    .line 252
    return-void
.end method

.method public setUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "mUrl"    # Ljava/lang/String;

    .prologue
    .line 259
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mUrl:Ljava/lang/String;

    .line 260
    return-void
.end method

.method public setWeatherText(Ljava/lang/String;)V
    .locals 0
    .param p1, "mWeatherText"    # Ljava/lang/String;

    .prologue
    .line 255
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mWeatherText:Ljava/lang/String;

    .line 256
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 107
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mCurrentTemp:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 108
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mTimeZone:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 109
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mUpdateDate:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 110
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mWeatherText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 111
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 112
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mTimestamp:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 113
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mTempScale:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 114
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mIconNum:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 115
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mHighTemp:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 116
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mLowTemp:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 117
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mRealFeel:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 118
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mSunRiseTime:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 119
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mSunSetTime:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 122
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mDayRainProbability:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 123
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mDaySnowProbability:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 124
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mDayHailProbability:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 125
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mDayPrecipitationProbability:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 127
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mNightRainProbability:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 128
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mNightSnowProbability:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 129
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mNightHailProbability:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 130
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mNightPrecipitationProbability:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 132
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mRelativeHumidity:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 133
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mUVIndex:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 134
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->mUVIndexText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 135
    return-void
.end method
