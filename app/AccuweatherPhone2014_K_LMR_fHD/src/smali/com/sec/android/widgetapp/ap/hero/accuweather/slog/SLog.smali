.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;
.super Ljava/lang/Object;
.source "SLog.java"


# static fields
.field static BUF_HEADER:Ljava/lang/String; = null

.field static final BUF_MAXLENGTH:I = 0x80

.field public static final LOG_LEVEL_HIGH:I = 0x2

.field public static final LOG_LEVEL_LOW:I = 0x0

.field public static final LOG_LEVEL_MID:I = 0x1

.field public static final PREFS_SLOG:Ljava/lang/String; = "prefs_slog"

.field public static final SLOG_ON:Ljava/lang/String; = "slog_on"

.field static final TAG:Ljava/lang/String; = "comsamsunglog"

.field static TAG_APP:Ljava/lang/String; = null

.field public static appContext:Landroid/content/Context; = null

.field static final bForceTag:Z = true

.field public static log_on:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->log_on:I

    .line 30
    const-string v0, "[kk Phone]>>> "

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->BUF_HEADER:Ljava/lang/String;

    .line 35
    const-string v0, "com.samsung.app"

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->TAG_APP:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;)I
    .locals 2
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 44
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-static {v0, p0, p1, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->println(ILjava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    return v0
.end method

.method public static d(ZLjava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p0, "withDot"    # Z
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 48
    const/4 v0, 0x3

    invoke-static {v0, p1, p2, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->println(ILjava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    return v0
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;)I
    .locals 2
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 52
    const/4 v0, 0x6

    const/4 v1, 0x0

    invoke-static {v0, p0, p1, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->println(ILjava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    return v0
.end method

.method public static i(Ljava/lang/String;Ljava/lang/String;)I
    .locals 2
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 56
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-static {v0, p0, p1, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->println(ILjava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    return v0
.end method

.method public static println(ILjava/lang/String;)I
    .locals 2
    .param p0, "priority"    # I
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 137
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {p0, v0, p1, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->println(ILjava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    return v0
.end method

.method public static println(ILjava/lang/String;Ljava/lang/String;Z)I
    .locals 15
    .param p0, "priority"    # I
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "withDot"    # Z

    .prologue
    .line 141
    sget v12, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->log_on:I

    const/4 v13, 0x2

    if-eq v12, v13, :cond_0

    sget v12, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->log_on:I

    const/4 v13, 0x1

    if-ne v12, v13, :cond_2

    const/4 v12, 0x6

    if-eq p0, v12, :cond_2

    const/4 v12, 0x3

    if-eq p0, v12, :cond_2

    .line 147
    :cond_0
    const/4 v6, 0x0

    .line 192
    :cond_1
    return v6

    .line 150
    :cond_2
    sget-object v12, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->appContext:Landroid/content/Context;

    if-eqz v12, :cond_3

    .line 151
    sget-object v12, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->appContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v12

    const-string v13, ""

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    .line 154
    :cond_3
    if-eqz p3, :cond_6

    .line 155
    const-string v12, "com.sec.android"

    const-string v13, ""

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    .line 160
    :goto_0
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v6

    .line 162
    .local v6, "length":I
    const/4 v7, 0x0

    .line 165
    .local v7, "linenum":I
    if-eqz p1, :cond_4

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v12

    if-lez v12, :cond_4

    .line 168
    :cond_4
    sget-object v11, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->TAG_APP:Ljava/lang/String;

    .line 172
    .local v11, "tag_app":Ljava/lang/String;
    new-instance v1, Ljava/lang/Exception;

    invoke-direct {v1}, Ljava/lang/Exception;-><init>()V

    .line 173
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v12

    const/4 v13, 0x2

    aget-object v2, v12, v13

    .line 175
    .local v2, "element":Ljava/lang/StackTraceElement;
    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getFileName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    .line 176
    .local v3, "filename":Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 178
    .local v10, "subFilename":Ljava/lang/StringBuilder;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v12

    if-ge v5, v12, :cond_7

    .line 179
    invoke-virtual {v3, v5}, Ljava/lang/String;->charAt(I)C

    move-result v12

    invoke-static {v12}, Ljava/lang/Character;->isUpperCase(C)Z

    move-result v12

    const/4 v13, 0x1

    if-ne v12, v13, :cond_5

    .line 180
    invoke-virtual {v3, v5}, Ljava/lang/String;->charAt(I)C

    move-result v12

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 178
    :cond_5
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 157
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "element":Ljava/lang/StackTraceElement;
    .end local v3    # "filename":Ljava/lang/String;
    .end local v5    # "i":I
    .end local v6    # "length":I
    .end local v7    # "linenum":I
    .end local v10    # "subFilename":Ljava/lang/StringBuilder;
    .end local v11    # "tag_app":Ljava/lang/String;
    :cond_6
    const-string v12, "com.sec.android"

    const-string v13, ""

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    const-string v13, "."

    const-string v14, ""

    invoke-virtual {v12, v13, v14}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 184
    .restart local v1    # "e":Ljava/lang/Exception;
    .restart local v2    # "element":Ljava/lang/StackTraceElement;
    .restart local v3    # "filename":Ljava/lang/String;
    .restart local v5    # "i":I
    .restart local v6    # "length":I
    .restart local v7    # "linenum":I
    .restart local v10    # "subFilename":Ljava/lang/StringBuilder;
    .restart local v11    # "tag_app":Ljava/lang/String;
    :cond_7
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->BUF_HEADER:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ":"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 186
    .local v4, "header_temp":Ljava/lang/String;
    const/4 v9, 0x0

    .local v9, "offset":I
    move v8, v7

    .end local v7    # "linenum":I
    .local v8, "linenum":I
    :goto_2
    if-le v6, v9, :cond_1

    .line 187
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " ["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    add-int/lit8 v7, v8, 0x1

    .end local v8    # "linenum":I
    .restart local v7    # "linenum":I
    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ":"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "] "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sub-int v13, v6, v9

    const/16 v14, 0x80

    .line 188
    invoke-static {v13, v14}, Ljava/lang/Math;->min(II)I

    move-result v13

    add-int/2addr v13, v9

    move-object/from16 v0, p2

    invoke-virtual {v0, v9, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 187
    invoke-static {p0, v11, v12}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I

    .line 186
    add-int/lit16 v9, v9, 0x80

    move v8, v7

    .end local v7    # "linenum":I
    .restart local v8    # "linenum":I
    goto :goto_2
.end method

.method public static setContext(Landroid/content/Context;)V
    .locals 13
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 71
    sput-object p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->appContext:Landroid/content/Context;

    .line 73
    sget-object v8, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->appContext:Landroid/content/Context;

    const-string v9, "prefs_slog"

    const/4 v10, 0x4

    invoke-virtual {v8, v9, v10}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    .line 74
    .local v6, "settings":Landroid/content/SharedPreferences;
    const-string v8, "slog_on"

    sget v9, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->log_on:I

    invoke-interface {v6, v8, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    sput v8, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->log_on:I

    .line 76
    sget-object v8, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->appContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 78
    .local v3, "manager":Landroid/content/pm/PackageManager;
    :try_start_0
    sget-object v8, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->appContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v3, v8, v9}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 79
    .local v2, "info":Landroid/content/pm/PackageInfo;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f070001

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v5

    .line 81
    .local v5, "resolutionInfo":[Ljava/lang/String;
    iget-object v8, v2, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    if-eqz v8, :cond_5

    .line 82
    iget-object v8, v2, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const-string v9, "\\."

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 83
    .local v4, "pn":[Ljava/lang/String;
    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    aget-object v0, v4, v8

    .line 84
    .local v0, "cpName":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 85
    const-string v8, "accuweather"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 86
    const-string v8, "[KK AccuPhone]>>> "

    sput-object v8, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->BUF_HEADER:Ljava/lang/String;

    .line 96
    :cond_0
    :goto_0
    const-string v8, "comsamsunglog"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->BUF_HEADER:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "=============================================================================================="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    const-string v8, "comsamsunglog"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->BUF_HEADER:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v2, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const-string v11, "com.sec.android"

    const-string v12, ""

    .line 101
    invoke-virtual {v10, v11, v12}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "."

    const-string v12, ""

    invoke-virtual {v10, v11, v12}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " [Version : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    const-string v11, "."

    const-string v12, ""

    .line 102
    invoke-virtual {v10, v11, v12}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " [ "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " ] "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 100
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    iget-object v8, v2, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const-string v9, "[.]"

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 112
    .local v7, "str":[Ljava/lang/String;
    array-length v8, v7

    add-int/lit8 v8, v8, -0x1

    aget-object v8, v7, v8

    sput-object v8, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->TAG_APP:Ljava/lang/String;

    .line 113
    if-eqz v5, :cond_4

    const/4 v8, 0x0

    aget-object v8, v5, v8

    if-eqz v8, :cond_4

    const/4 v8, 0x0

    aget-object v8, v5, v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_4

    .line 114
    const-string v8, "comsamsunglog"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->BUF_HEADER:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "Header set to : ===> \""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->TAG_APP:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\" <===  "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/4 v10, 0x0

    aget-object v10, v5, v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    .end local v0    # "cpName":Ljava/lang/String;
    .end local v4    # "pn":[Ljava/lang/String;
    .end local v7    # "str":[Ljava/lang/String;
    :goto_1
    const-string v8, "comsamsunglog"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->BUF_HEADER:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "=============================================================================================="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    .end local v2    # "info":Landroid/content/pm/PackageInfo;
    .end local v5    # "resolutionInfo":[Ljava/lang/String;
    :goto_2
    return-void

    .line 87
    .restart local v0    # "cpName":Ljava/lang/String;
    .restart local v2    # "info":Landroid/content/pm/PackageInfo;
    .restart local v4    # "pn":[Ljava/lang/String;
    .restart local v5    # "resolutionInfo":[Ljava/lang/String;
    :cond_1
    const-string v8, "cmaweather"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 88
    const-string v8, "[KK CmaPhone]>>> "

    sput-object v8, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->BUF_HEADER:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_0

    .line 129
    .end local v0    # "cpName":Ljava/lang/String;
    .end local v2    # "info":Landroid/content/pm/PackageInfo;
    .end local v4    # "pn":[Ljava/lang/String;
    .end local v5    # "resolutionInfo":[Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 130
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_2

    .line 89
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v0    # "cpName":Ljava/lang/String;
    .restart local v2    # "info":Landroid/content/pm/PackageInfo;
    .restart local v4    # "pn":[Ljava/lang/String;
    .restart local v5    # "resolutionInfo":[Ljava/lang/String;
    :cond_2
    :try_start_1
    const-string v8, "kweather"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 90
    const-string v8, "[KK KWeatherPhone]>>> "

    sput-object v8, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->BUF_HEADER:Ljava/lang/String;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 131
    .end local v0    # "cpName":Ljava/lang/String;
    .end local v2    # "info":Landroid/content/pm/PackageInfo;
    .end local v4    # "pn":[Ljava/lang/String;
    .end local v5    # "resolutionInfo":[Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 132
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 91
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "cpName":Ljava/lang/String;
    .restart local v2    # "info":Landroid/content/pm/PackageInfo;
    .restart local v4    # "pn":[Ljava/lang/String;
    .restart local v5    # "resolutionInfo":[Ljava/lang/String;
    :cond_3
    :try_start_2
    const-string v8, "weathernewsjp"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 92
    const-string v8, "[KK WeatherNewsPhone]>>> "

    sput-object v8, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->BUF_HEADER:Ljava/lang/String;

    goto/16 :goto_0

    .line 116
    .restart local v7    # "str":[Ljava/lang/String;
    :cond_4
    const-string v8, "comsamsunglog"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->BUF_HEADER:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "Header set to : ===> \""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->TAG_APP:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\" <==="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 119
    .end local v0    # "cpName":Ljava/lang/String;
    .end local v4    # "pn":[Ljava/lang/String;
    .end local v7    # "str":[Ljava/lang/String;
    :cond_5
    const-string v8, "comsamsunglog"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->BUF_HEADER:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "=============================================================================================="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    const-string v8, "comsamsunglog"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->BUF_HEADER:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "packageName missing !!"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_1
.end method

.method public static v(Ljava/lang/String;Ljava/lang/String;)I
    .locals 2
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 60
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-static {v0, p0, p1, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->println(ILjava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    return v0
.end method

.method public static w(Ljava/lang/String;Ljava/lang/String;)I
    .locals 2
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 64
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-static {v0, p0, p1, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->println(ILjava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    return v0
.end method
