.class Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$4;
.super Ljava/lang/Object;
.source "ScenManager.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setEventListener(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    .prologue
    .line 762
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 765
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 767
    .local v2, "y":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 768
    .local v0, "action":I
    if-ne v0, v7, :cond_4

    .line 774
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mTouchDownY:F
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->access$500()F

    move-result v3

    sub-float v1, v3, v2

    .line 776
    .local v1, "dy":F
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "touch : mIsStartScrollAnimation = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIsStartScrollAnimation:Z
    invoke-static {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;)Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 777
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIsStartScrollAnimation:Z
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 778
    const/high16 v3, 0x41f00000    # 30.0f

    cmpl-float v3, v1, v3

    if-lez v3, :cond_1

    .line 779
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->moveCity(I)V
    invoke-static {v3, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;I)V

    .line 804
    .end local v1    # "dy":F
    :cond_0
    :goto_0
    return v6

    .line 780
    .restart local v1    # "dy":F
    :cond_1
    const/high16 v3, -0x3e100000    # -30.0f

    cmpg-float v3, v1, v3

    if-gez v3, :cond_2

    .line 781
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    const/4 v4, 0x2

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->moveCity(I)V
    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;I)V

    goto :goto_0

    .line 783
    :cond_2
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->audio:Landroid/media/AudioManager;
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;)Landroid/media/AudioManager;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 784
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->audio:Landroid/media/AudioManager;
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;)Landroid/media/AudioManager;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 786
    :cond_3
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->getCityIndex()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->startActivity(I)V

    goto :goto_0

    .line 789
    .end local v1    # "dy":F
    :cond_4
    if-nez v0, :cond_0

    .line 794
    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mTouchDownY:F
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->access$502(F)F

    goto :goto_0
.end method
