.class Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;
.super Lcom/google/android/maps/ItemizedOverlay;
.source "MapsActivityNew.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MapItems"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/maps/ItemizedOverlay",
        "<",
        "Lcom/google/android/maps/OverlayItem;",
        ">;"
    }
.end annotation


# instance fields
.field private mBitMapList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mListType:I

.field private mMapCityInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mOverlayItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/maps/OverlayItem;",
            ">;"
        }
    .end annotation
.end field

.field private mResource:Landroid/content/res/Resources;

.field private mTempUnit:I

.field private mTopItemIndex:I

.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;


# direct methods
.method public constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Ljava/util/ArrayList;II)V
    .locals 4
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;
    .param p3, "tempUint"    # I
    .param p4, "listType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .local p2, "info":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;>;"
    const v3, 0x7f0d003b

    const/4 v0, 0x0

    .line 2234
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .line 2235
    invoke-direct {p0, v0}, Lcom/google/android/maps/ItemizedOverlay;-><init>(Landroid/graphics/drawable/Drawable;)V

    .line 2220
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mOverlayItemList:Ljava/util/ArrayList;

    .line 2222
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mMapCityInfoList:Ljava/util/ArrayList;

    .line 2224
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mBitMapList:Ljava/util/ArrayList;

    .line 2228
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mTopItemIndex:I

    .line 2232
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mListType:I

    .line 2236
    iput-object p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mMapCityInfoList:Ljava/util/ArrayList;

    .line 2238
    iput p3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mTempUnit:I

    .line 2240
    iput p4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mListType:I

    .line 2242
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mUpdateText:Landroid/widget/TextView;
    invoke-static {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$5100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 2243
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getTimestamp()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getDateTimeString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2242
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2244
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mUpdateText:Landroid/widget/TextView;
    invoke-static {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$5100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 2245
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getTimestamp()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getDateTimeString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2244
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2246
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mUpdateText:Landroid/widget/TextView;
    invoke-static {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$5100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->bringToFront()V

    .line 2248
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->init()V

    .line 2249
    return-void
.end method

.method private createOverlayedItem(Ljava/util/ArrayList;)V
    .locals 31
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2447
    .local p1, "mapCityInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;>;"
    const/4 v13, 0x0

    .line 2448
    .local v13, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    const/16 v23, 0x0

    .line 2450
    .local v23, "v":Landroid/view/View;
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_0
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v26

    move/from16 v0, v26

    if-ge v12, v0, :cond_d

    .line 2452
    :try_start_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    move-object/from16 v0, v26

    check-cast v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    move-object v13, v0

    .line 2455
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mIsWQHD:Z
    invoke-static/range {v26 .. v26}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$5500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Z

    move-result v26

    if-eqz v26, :cond_4

    .line 2457
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v26, v0

    const v27, 0x7f030019

    const/16 v28, 0x0

    invoke-static/range {v26 .. v28}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v23

    .line 2463
    :goto_1
    const v26, 0x7f080054

    move-object/from16 v0, v23

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v24

    check-cast v24, Landroid/widget/ImageView;

    .line 2464
    .local v24, "weather":Landroid/widget/ImageView;
    const v26, 0x7f080053

    move-object/from16 v0, v23

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 2465
    .local v6, "cityname":Landroid/widget/TextView;
    const v26, 0x7f080056

    move-object/from16 v0, v23

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/TextView;

    .line 2466
    .local v22, "tempview":Landroid/widget/TextView;
    const v26, 0x7f080055

    move-object/from16 v0, v23

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/ImageView;

    .line 2467
    .local v21, "tempscaleImage":Landroid/widget/ImageView;
    const v26, 0x7f080052

    move-object/from16 v0, v23

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/RelativeLayout;

    .line 2468
    .local v16, "mapTop":Landroid/widget/RelativeLayout;
    const v26, 0x7f080058

    move-object/from16 v0, v23

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 2470
    .local v7, "curLocImge":Landroid/view/View;
    const/16 v20, 0x0

    .line 2471
    .local v20, "strDayTime":Ljava/lang/String;
    const/4 v14, 0x0

    .line 2473
    .local v14, "isDayTime":Z
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mListType:I

    move/from16 v26, v0

    const/16 v27, 0x8

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_5

    .line 2474
    const v26, 0x7f02013b

    move-object/from16 v0, v16

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 2476
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getApplicationContext()Landroid/content/Context;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getLocation(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    .line 2478
    .local v8, "currentLocation":Ljava/lang/String;
    if-eqz v8, :cond_0

    .line 2479
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getApplicationContext()Landroid/content/Context;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getRealLocation(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v18

    .line 2481
    .local v18, "realLocation":Ljava/lang/String;
    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLocation()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_0

    .line 2482
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v27, 0x7f0a0067

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getColor(I)I

    move-result v26

    move/from16 v0, v26

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2483
    const/16 v26, 0x0

    move/from16 v0, v26

    invoke-virtual {v7, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2490
    .end local v8    # "currentLocation":Ljava/lang/String;
    .end local v18    # "realLocation":Ljava/lang/String;
    :cond_0
    :goto_2
    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getIcon()Ljava/lang/String;

    move-result-object v26

    if-eqz v26, :cond_8

    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getIcon()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->length()I

    move-result v26

    const/16 v27, 0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-lt v0, v1, :cond_8

    .line 2491
    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getIsDayTime()Ljava/lang/String;

    move-result-object v20

    .line 2492
    if-eqz v20, :cond_7

    const-string v26, ""

    move-object/from16 v0, v20

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-nez v26, :cond_7

    .line 2493
    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v26

    const-string v27, "TRUE"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_6

    const/4 v14, 0x1

    .line 2500
    :goto_3
    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getIcon()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v26

    const/16 v27, 0x8

    .line 2499
    move/from16 v0, v26

    move/from16 v1, v27

    invoke-static {v0, v1, v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setTodayWeatherIcon(IIZ)I

    move-result v26

    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2507
    :goto_4
    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getCity()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2512
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mIsLocaleRTL:Z
    invoke-static/range {v26 .. v26}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$5600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Z

    move-result v26

    if-eqz v26, :cond_9

    .line 2513
    const/16 v26, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v6, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->setSingleLine(Landroid/widget/TextView;Z)V

    .line 2518
    :goto_5
    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getTemp()Ljava/lang/String;

    move-result-object v9

    .line 2520
    .local v9, "currentTemp":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mTempScale:I
    invoke-static/range {v26 .. v26}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)I

    move-result v26

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mTempUnit:I

    move/from16 v27, v0

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_1

    .line 2521
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mTempUnit:I

    move/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v27, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mTempScale:I
    invoke-static/range {v27 .. v27}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)I

    move-result v27

    .line 2522
    invoke-static {v9}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Float;->floatValue()F

    move-result v28

    const/16 v29, 0x0

    .line 2521
    invoke-static/range {v26 .. v29}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIFZ)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    .line 2526
    :cond_1
    :try_start_1
    const-string v26, "%d"

    const/16 v27, 0x1

    move/from16 v0, v27

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v29

    aput-object v29, v27, v28

    invoke-static/range {v26 .. v27}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0

    .line 2531
    :goto_6
    :try_start_2
    sget-object v26, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mFontSamsungNumber3L:Landroid/graphics/Typeface;

    if-eqz v26, :cond_2

    .line 2532
    sget-object v26, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mFontSamsungNumber3L:Landroid/graphics/Typeface;

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2535
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mTempImage:I
    invoke-static/range {v26 .. v26}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$5200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)I

    move-result v26

    move-object/from16 v0, v21

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2537
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mIsWQHD:Z
    invoke-static/range {v26 .. v26}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$5500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Z

    move-result v26

    if-eqz v26, :cond_a

    .line 2539
    const/16 v26, 0x140

    const/high16 v27, 0x40000000    # 2.0f

    invoke-static/range {v26 .. v27}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v26

    const/16 v27, 0xdc

    const/high16 v28, 0x40000000    # 2.0f

    invoke-static/range {v27 .. v28}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v27

    move-object/from16 v0, v23

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    .line 2540
    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x140

    const/16 v29, 0xdc

    move-object/from16 v0, v23

    move/from16 v1, v26

    move/from16 v2, v27

    move/from16 v3, v28

    move/from16 v4, v29

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->layout(IIII)V

    .line 2550
    :goto_7
    const/16 v26, 0x1

    move-object/from16 v0, v23

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 2554
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mKoreaCityIds:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLocation()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v26

    const/16 v27, -0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;
    invoke-static/range {v26 .. v26}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getZoomLevel()I

    move-result v26

    const/16 v27, 0x9

    move/from16 v0, v26

    move/from16 v1, v27

    if-le v0, v1, :cond_b

    .line 2555
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v26, v0

    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLatitude()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v27

    .line 2556
    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLongitude()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v29 .. v29}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v29

    .line 2555
    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getPoint(DD)Lcom/google/android/maps/GeoPoint;
    invoke-static/range {v26 .. v30}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;DD)Lcom/google/android/maps/GeoPoint;

    move-result-object v17

    .line 2561
    .local v17, "point":Lcom/google/android/maps/GeoPoint;
    :goto_8
    new-instance v15, Lcom/google/android/maps/OverlayItem;

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v26

    const/16 v27, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-direct {v15, v0, v1, v2}, Lcom/google/android/maps/OverlayItem;-><init>(Lcom/google/android/maps/GeoPoint;Ljava/lang/String;Ljava/lang/String;)V

    .line 2562
    .local v15, "itm":Lcom/google/android/maps/OverlayItem;
    invoke-virtual/range {v23 .. v23}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v5

    .line 2564
    .local v5, "bb":Landroid/graphics/Bitmap;
    new-instance v26, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mResource:Landroid/content/res/Resources;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-direct {v0, v1, v5}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-static/range {v26 .. v26}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->boundCenterBottom(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    .line 2566
    .local v10, "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v19

    .line 2567
    .local v19, "rect":Landroid/graphics/Rect;
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v26, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v27, v0

    sub-int v25, v26, v27

    .line 2569
    .local v25, "width":I
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v26, v0

    div-int/lit8 v27, v25, 0x2

    add-int v26, v26, v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v27, v0

    .line 2570
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getApplicationContext()Landroid/content/Context;

    move-result-object v27

    const/16 v28, 0x15

    invoke-static/range {v27 .. v28}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getPixelFromDP(Landroid/content/Context;I)I

    move-result v27

    sub-int v26, v26, v27

    move/from16 v0, v26

    move-object/from16 v1, v19

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 2571
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v26, v0

    div-int/lit8 v27, v25, 0x2

    add-int v26, v26, v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v27, v0

    .line 2572
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getApplicationContext()Landroid/content/Context;

    move-result-object v27

    const/16 v28, 0x15

    invoke-static/range {v27 .. v28}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getPixelFromDP(Landroid/content/Context;I)I

    move-result v27

    sub-int v26, v26, v27

    move/from16 v0, v26

    move-object/from16 v1, v19

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 2573
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getApplicationContext()Landroid/content/Context;

    move-result-object v27

    const/16 v28, 0x1

    invoke-static/range {v27 .. v28}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getPixelFromDP(Landroid/content/Context;I)I

    move-result v27

    add-int v26, v26, v27

    move/from16 v0, v26

    move-object/from16 v1, v19

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 2574
    move-object/from16 v0, v19

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getApplicationContext()Landroid/content/Context;

    move-result-object v27

    const/16 v28, 0x1

    invoke-static/range {v27 .. v28}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getPixelFromDP(Landroid/content/Context;I)I

    move-result v27

    add-int v26, v26, v27

    move/from16 v0, v26

    move-object/from16 v1, v19

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 2575
    move-object/from16 v0, v19

    invoke-virtual {v10, v0}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 2577
    invoke-virtual {v15, v10}, Lcom/google/android/maps/OverlayItem;->setMarker(Landroid/graphics/drawable/Drawable;)V

    .line 2579
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mBitMapList:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2580
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mOverlayItemList:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2582
    const/16 v23, 0x0

    .line 2450
    .end local v5    # "bb":Landroid/graphics/Bitmap;
    .end local v6    # "cityname":Landroid/widget/TextView;
    .end local v7    # "curLocImge":Landroid/view/View;
    .end local v9    # "currentTemp":Ljava/lang/String;
    .end local v10    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v14    # "isDayTime":Z
    .end local v15    # "itm":Lcom/google/android/maps/OverlayItem;
    .end local v16    # "mapTop":Landroid/widget/RelativeLayout;
    .end local v17    # "point":Lcom/google/android/maps/GeoPoint;
    .end local v19    # "rect":Landroid/graphics/Rect;
    .end local v20    # "strDayTime":Ljava/lang/String;
    .end local v21    # "tempscaleImage":Landroid/widget/ImageView;
    .end local v22    # "tempview":Landroid/widget/TextView;
    .end local v24    # "weather":Landroid/widget/ImageView;
    .end local v25    # "width":I
    :goto_9
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_0

    .line 2460
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v26, v0

    const v27, 0x7f030018

    const/16 v28, 0x0

    invoke-static/range {v26 .. v28}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v23

    goto/16 :goto_1

    .line 2487
    .restart local v6    # "cityname":Landroid/widget/TextView;
    .restart local v7    # "curLocImge":Landroid/view/View;
    .restart local v14    # "isDayTime":Z
    .restart local v16    # "mapTop":Landroid/widget/RelativeLayout;
    .restart local v20    # "strDayTime":Ljava/lang/String;
    .restart local v21    # "tempscaleImage":Landroid/widget/ImageView;
    .restart local v22    # "tempview":Landroid/widget/TextView;
    .restart local v24    # "weather":Landroid/widget/ImageView;
    :cond_5
    const v26, 0x7f02013c

    move-object/from16 v0, v16

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_2

    .line 2583
    .end local v6    # "cityname":Landroid/widget/TextView;
    .end local v7    # "curLocImge":Landroid/view/View;
    .end local v14    # "isDayTime":Z
    .end local v16    # "mapTop":Landroid/widget/RelativeLayout;
    .end local v20    # "strDayTime":Ljava/lang/String;
    .end local v21    # "tempscaleImage":Landroid/widget/ImageView;
    .end local v22    # "tempview":Landroid/widget/TextView;
    .end local v24    # "weather":Landroid/widget/ImageView;
    :catch_0
    move-exception v11

    .line 2584
    .local v11, "e":Ljava/lang/OutOfMemoryError;
    if-eqz v13, :cond_c

    .line 2585
    const-string v26, ""

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "cOI OOME : "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLocation()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9

    .line 2493
    .end local v11    # "e":Ljava/lang/OutOfMemoryError;
    .restart local v6    # "cityname":Landroid/widget/TextView;
    .restart local v7    # "curLocImge":Landroid/view/View;
    .restart local v14    # "isDayTime":Z
    .restart local v16    # "mapTop":Landroid/widget/RelativeLayout;
    .restart local v20    # "strDayTime":Ljava/lang/String;
    .restart local v21    # "tempscaleImage":Landroid/widget/ImageView;
    .restart local v22    # "tempview":Landroid/widget/TextView;
    .restart local v24    # "weather":Landroid/widget/ImageView;
    :cond_6
    const/4 v14, 0x0

    goto/16 :goto_3

    .line 2495
    :cond_7
    const/4 v14, 0x1

    .line 2496
    :try_start_3
    const-string v26, ""

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "isDayTime n loc : "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLocation()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 2503
    :cond_8
    const/16 v26, 0x1

    const/16 v27, 0x8

    const/16 v28, 0x1

    invoke-static/range {v26 .. v28}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setTodayWeatherIcon(IIZ)I

    move-result v26

    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_4

    .line 2515
    :cond_9
    const/16 v26, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v6, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->setSingleLine(Landroid/widget/TextView;Z)V

    goto/16 :goto_5

    .line 2527
    .restart local v9    # "currentTemp":Ljava/lang/String;
    :catch_1
    move-exception v11

    .line 2528
    .local v11, "e":Ljava/lang/NumberFormatException;
    move-object/from16 v0, v22

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    .line 2543
    .end local v11    # "e":Ljava/lang/NumberFormatException;
    :cond_a
    new-instance v26, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v27, -0x2

    const/16 v28, -0x2

    invoke-direct/range {v26 .. v28}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2546
    const/16 v26, 0x0

    const/16 v27, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    .line 2547
    const/16 v26, 0x0

    const/16 v27, 0x0

    invoke-virtual/range {v23 .. v23}, Landroid/view/View;->getMeasuredWidth()I

    move-result v28

    invoke-virtual/range {v23 .. v23}, Landroid/view/View;->getMeasuredHeight()I

    move-result v29

    move-object/from16 v0, v23

    move/from16 v1, v26

    move/from16 v2, v27

    move/from16 v3, v28

    move/from16 v4, v29

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->layout(IIII)V

    goto/16 :goto_7

    .line 2558
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mKoreaLocation:[Lcom/google/android/maps/GeoPoint;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mKoreaCityIds:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLocation()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v27

    aget-object v17, v26, v27
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_0

    .restart local v17    # "point":Lcom/google/android/maps/GeoPoint;
    goto/16 :goto_8

    .line 2587
    .end local v6    # "cityname":Landroid/widget/TextView;
    .end local v7    # "curLocImge":Landroid/view/View;
    .end local v9    # "currentTemp":Ljava/lang/String;
    .end local v14    # "isDayTime":Z
    .end local v16    # "mapTop":Landroid/widget/RelativeLayout;
    .end local v17    # "point":Lcom/google/android/maps/GeoPoint;
    .end local v20    # "strDayTime":Ljava/lang/String;
    .end local v21    # "tempscaleImage":Landroid/widget/ImageView;
    .end local v22    # "tempview":Landroid/widget/TextView;
    .end local v24    # "weather":Landroid/widget/ImageView;
    .local v11, "e":Ljava/lang/OutOfMemoryError;
    :cond_c
    const-string v26, ""

    const-string v27, "cOI OOME."

    invoke-static/range {v26 .. v27}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_9

    .line 2591
    .end local v11    # "e":Ljava/lang/OutOfMemoryError;
    :cond_d
    return-void
.end method

.method private init()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2259
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->clearItems()V

    .line 2261
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mResource:Landroid/content/res/Resources;

    .line 2262
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mOverlayItemList:Ljava/util/ArrayList;

    .line 2263
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mBitMapList:Ljava/util/ArrayList;

    .line 2265
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mMapCityInfoList:Ljava/util/ArrayList;

    if-eqz v2, :cond_5

    .line 2266
    const/4 v0, 0x0

    .line 2268
    .local v0, "i":I
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mTempScale:I
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)I

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .line 2269
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mTempScale:I
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)I

    move-result v2

    if-eq v2, v4, :cond_0

    .line 2270
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getTempScaleSetting(Landroid/content/Context;)I

    move-result v3

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mTempScale:I
    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4102(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;I)I

    .line 2272
    :cond_0
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mTempScale:I
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)I

    move-result v2

    if-ne v2, v4, :cond_1

    const v2, 0x7f02013d

    :goto_0
    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mTempImage:I
    invoke-static {v3, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$5202(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;I)I

    .line 2275
    :goto_1
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mMapCityInfoList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-le v2, v0, :cond_4

    .line 2276
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mMapCityInfoList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    .line 2278
    .local v1, "item":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    invoke-direct {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->isValidMapItem(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2279
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mMapCityInfoList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_1

    .line 2272
    .end local v1    # "item":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    :cond_1
    const v2, 0x7f02013e

    goto :goto_0

    .line 2283
    .restart local v1    # "item":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    :cond_2
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLocation()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mPreLocation:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$5300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2284
    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mTopItemIndex:I

    .line 2286
    :cond_3
    add-int/lit8 v0, v0, 0x1

    .line 2287
    goto :goto_1

    .line 2289
    .end local v1    # "item":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    :cond_4
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mMapCityInfoList:Ljava/util/ArrayList;

    invoke-direct {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->createOverlayedItem(Ljava/util/ArrayList;)V

    .line 2292
    .end local v0    # "i":I
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->populateMapItem()V

    .line 2293
    return-void
.end method

.method private isValidMapItem(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;)Z
    .locals 4
    .param p1, "mapInfo"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    .prologue
    .line 2296
    if-nez p1, :cond_1

    .line 2297
    const-string v1, ""

    const-string v2, "iVMI mI n"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2298
    const/4 v0, 0x0

    .line 2325
    :cond_0
    :goto_0
    return v0

    .line 2300
    :cond_1
    const/4 v0, 0x1

    .line 2302
    .local v0, "isValid":Z
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getCity()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getCity()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2303
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLatitude()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLatitude()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2304
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLongitude()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLongitude()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2306
    :cond_2
    const/4 v0, 0x0

    .line 2313
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "error item "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getCity()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLocation()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2318
    :cond_3
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getIcon()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getTemp()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 2319
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getIcon()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getTemp()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2320
    :cond_4
    const/4 v0, 0x0

    .line 2322
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "error item :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getCity()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLocation()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private populateMapItem()V
    .locals 0

    .prologue
    .line 2594
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 2595
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->populate()V

    .line 2596
    return-void
.end method


# virtual methods
.method public clearItems()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2349
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mOverlayItemList:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    .line 2350
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mOverlayItemList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 2351
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mOverlayItemList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/maps/OverlayItem;

    invoke-virtual {v2, v4}, Lcom/google/android/maps/OverlayItem;->setMarker(Landroid/graphics/drawable/Drawable;)V

    .line 2350
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2353
    :cond_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mOverlayItemList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 2354
    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mOverlayItemList:Ljava/util/ArrayList;

    .line 2356
    .end local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mBitMapList:Ljava/util/ArrayList;

    if-eqz v2, :cond_4

    .line 2357
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mBitMapList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 2358
    .local v0, "bb":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_2

    .line 2359
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_1

    .line 2363
    .end local v0    # "bb":Landroid/graphics/Bitmap;
    :cond_3
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mBitMapList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 2364
    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mBitMapList:Ljava/util/ArrayList;

    .line 2366
    :cond_4
    return-void
.end method

.method protected createItem(I)Lcom/google/android/maps/OverlayItem;
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 2599
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mOverlayItemList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/OverlayItem;

    return-object v0
.end method

.method public draw(Landroid/graphics/Canvas;Lcom/google/android/maps/MapView;ZJ)Z
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "mapView"    # Lcom/google/android/maps/MapView;
    .param p3, "shadow"    # Z
    .param p4, "when"    # J

    .prologue
    const/4 v3, 0x0

    .line 2252
    if-eqz p3, :cond_0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v4, p4

    .line 2253
    invoke-super/range {v0 .. v5}, Lcom/google/android/maps/ItemizedOverlay;->draw(Landroid/graphics/Canvas;Lcom/google/android/maps/MapView;ZJ)Z

    move-result v3

    .line 2255
    :cond_0
    return v3
.end method

.method public getCurrentUint()I
    .locals 1

    .prologue
    .line 2378
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mTempUnit:I

    return v0
.end method

.method protected getIndexToDraw(I)I
    .locals 3
    .param p1, "pos"    # I

    .prologue
    .line 2330
    invoke-super {p0, p1}, Lcom/google/android/maps/ItemizedOverlay;->getIndexToDraw(I)I

    move-result v0

    .line 2332
    .local v0, "position":I
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mTopItemIndex:I

    const/4 v2, -0x1

    if-le v1, v2, :cond_0

    .line 2333
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mTopItemIndex:I

    if-ne v0, v1, :cond_1

    .line 2334
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mOverlayItemList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-super {p0, v1}, Lcom/google/android/maps/ItemizedOverlay;->getIndexToDraw(I)I

    move-result v0

    .line 2341
    .end local v0    # "position":I
    :cond_0
    :goto_0
    return v0

    .line 2335
    .restart local v0    # "position":I
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mOverlayItemList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne p1, v1, :cond_0

    .line 2336
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mTopItemIndex:I

    goto :goto_0
.end method

.method public getInfoList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2374
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mMapCityInfoList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public newItems()V
    .locals 1

    .prologue
    .line 2369
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mOverlayItemList:Ljava/util/ArrayList;

    .line 2370
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mBitMapList:Ljava/util/ArrayList;

    .line 2371
    return-void
.end method

.method protected onTap(I)Z
    .locals 12
    .param p1, "position"    # I

    .prologue
    const/high16 v11, 0x24000000

    const/4 v6, 0x1

    const/16 v10, -0x3e7

    const/4 v9, 0x0

    .line 2382
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getEnterOnTap()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2443
    :goto_0
    return v6

    .line 2385
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 2386
    .local v0, "curTime":J
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mPreTapTime:J
    invoke-static {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$5400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)J

    move-result-wide v7

    sub-long v2, v0, v7

    .line 2387
    .local v2, "diffTime":J
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mPreTapTime:J
    invoke-static {v7, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$5402(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;J)J

    .line 2388
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string v8, "flags"

    invoke-virtual {v7, v8, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    const/16 v8, 0x61a7

    if-eq v7, v8, :cond_1

    const-wide/16 v7, 0x3e8

    cmp-long v7, v2, v7

    if-gez v7, :cond_1

    .line 2390
    const-string v7, ""

    const-string v8, "return by double tap"

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2394
    :cond_1
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mMapCityInfoList:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    .line 2396
    .local v4, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    const-string v6, ""

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "TP:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", id = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLocation()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2398
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v7, "flags"

    invoke-virtual {v6, v7, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    const v7, 0xcb84

    if-ne v6, v7, :cond_3

    .line 2399
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLocation()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->isRegisteredToCityList(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2400
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;
    invoke-static {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    move-result-object v6

    invoke-virtual {v6, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->playSoundEffect(I)V

    .line 2401
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .line 2402
    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLocation()Ljava/lang/String;

    move-result-object v7

    .line 2401
    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateLastSelectedLocation(Landroid/content/Context;Ljava/lang/String;)I

    .line 2403
    new-instance v5, Landroid/content/Intent;

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    const-class v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-direct {v5, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2404
    .local v5, "intent":Landroid/content/Intent;
    invoke-virtual {v5, v11}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2406
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-virtual {v6, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->startActivity(Landroid/content/Intent;)V

    .line 2407
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->finish()V

    .line 2443
    .end local v5    # "intent":Landroid/content/Intent;
    :goto_1
    invoke-super {p0, p1}, Lcom/google/android/maps/ItemizedOverlay;->onTap(I)Z

    move-result v6

    goto/16 :goto_0

    .line 2409
    :cond_2
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;
    invoke-static {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    move-result-object v6

    invoke-virtual {v6, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->playSoundEffect(I)V

    .line 2410
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-virtual {v6, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->makeDetail(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;)V

    goto :goto_1

    .line 2412
    :cond_3
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v7, "flags"

    invoke-virtual {v6, v7, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    const/16 v7, 0x3070

    if-ne v6, v7, :cond_5

    .line 2413
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLocation()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->isRegisteredToCityList(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2414
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .line 2415
    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLocation()Ljava/lang/String;

    move-result-object v7

    .line 2414
    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateLastSelectedLocation(Landroid/content/Context;Ljava/lang/String;)I

    .line 2416
    new-instance v5, Landroid/content/Intent;

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    const-class v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-direct {v5, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2417
    .restart local v5    # "intent":Landroid/content/Intent;
    invoke-virtual {v5, v11}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2419
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-virtual {v6, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 2421
    .end local v5    # "intent":Landroid/content/Intent;
    :cond_4
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;
    invoke-static {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    move-result-object v6

    invoke-virtual {v6, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->playSoundEffect(I)V

    .line 2422
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-virtual {v6, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->makeDetail(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;)V

    goto :goto_1

    .line 2424
    :cond_5
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v7, "flags"

    invoke-virtual {v6, v7, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    const v7, -0xcf6c

    if-ne v6, v7, :cond_6

    .line 2425
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;
    invoke-static {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    move-result-object v6

    invoke-virtual {v6, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->playSoundEffect(I)V

    .line 2426
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-virtual {v6, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->makeDetail(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;)V

    goto :goto_1

    .line 2428
    :cond_6
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;
    invoke-static {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    move-result-object v6

    invoke-virtual {v6, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->playSoundEffect(I)V

    .line 2430
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLocation()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->isRegisteredToCityList(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 2433
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    const v7, 0x7f0d0022

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    goto/16 :goto_1

    .line 2435
    :cond_7
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v7, "cityinfo"

    invoke-virtual {v6, v7, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2436
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v7, "allcity"

    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListZoom5:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$1200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2437
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    const/16 v7, 0x12c

    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getIntent()Landroid/content/Intent;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->setResult(ILandroid/content/Intent;)V

    .line 2438
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->finish()V

    goto/16 :goto_1
.end method

.method public setSingleLine(Landroid/widget/TextView;Z)V
    .locals 1
    .param p1, "view"    # Landroid/widget/TextView;
    .param p2, "isSingleLine"    # Z

    .prologue
    .line 2613
    if-nez p1, :cond_0

    .line 2622
    :goto_0
    return-void

    .line 2617
    :cond_0
    if-eqz p2, :cond_1

    .line 2618
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setSingleLine(Z)V

    goto :goto_0

    .line 2620
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setSingleLine(Z)V

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 2603
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->mOverlayItemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
