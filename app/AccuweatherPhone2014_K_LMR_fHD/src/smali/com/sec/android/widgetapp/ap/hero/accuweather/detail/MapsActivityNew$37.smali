.class Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$37;
.super Landroid/os/Handler;
.source "MapsActivityNew.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 1358
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$37;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v9, 0x0

    .line 1360
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "RESPONSE_BODY"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1361
    .local v1, "body":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "RESPONSE_CODE"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 1363
    .local v3, "result":I
    const/16 v4, 0xc8

    if-ne v3, v4, :cond_2

    .line 1364
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$37;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mParser:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$3800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$37;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$37;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mTempScale:I
    invoke-static {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)I

    move-result v6

    .line 1365
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    .line 1364
    invoke-virtual {v4, v5, v6, v7, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->parseDetailWeather_SearchMapCity(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 1367
    .local v2, "info":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;>;"
    if-eqz v2, :cond_1

    .line 1368
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 1369
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$37;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->hideLoadingDialog()V
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$2300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    .line 1370
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$37;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->setKeyboardVisible(Z)V
    invoke-static {v4, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$1000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Z)V

    .line 1371
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$37;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->removeAllMapList()V
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    .line 1373
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$37;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$1500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1374
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$37;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$1500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->clearItems()V

    .line 1375
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$37;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    const/4 v5, 0x0

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;
    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$1502(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    .line 1378
    :cond_0
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$37;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$37;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$37;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mTempScale:I
    invoke-static {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)I

    move-result v7

    invoke-direct {v5, v6, v2, v7, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Ljava/util/ArrayList;II)V

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;
    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$1502(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    .line 1380
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$37;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getOverlays()Ljava/util/List;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$37;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;
    invoke-static {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$1500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1381
    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    .line 1382
    .local v0, "a":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$37;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getController()Lcom/google/android/maps/MapController;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$37;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .line 1383
    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLatitude()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    .line 1384
    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLongitude()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    .line 1383
    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getPoint(DD)Lcom/google/android/maps/GeoPoint;
    invoke-static {v5, v6, v7, v8, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;DD)Lcom/google/android/maps/GeoPoint;

    move-result-object v5

    .line 1382
    invoke-virtual {v4, v5}, Lcom/google/android/maps/MapController;->animateTo(Lcom/google/android/maps/GeoPoint;)V

    .line 1385
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$37;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getController()Lcom/google/android/maps/MapController;

    move-result-object v4

    const/4 v5, 0x5

    invoke-virtual {v4, v5}, Lcom/google/android/maps/MapController;->setZoom(I)I

    .line 1394
    .end local v0    # "a":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    .end local v2    # "info":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;>;"
    :cond_1
    :goto_0
    return-void

    .line 1392
    :cond_2
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$37;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->hideLoadingDialog()V
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$2300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    .line 1393
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$37;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    const v5, 0x7f0d001f

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    goto :goto_0
.end method
