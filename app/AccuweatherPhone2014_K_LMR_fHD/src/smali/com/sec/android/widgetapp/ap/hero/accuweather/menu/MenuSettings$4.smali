.class Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$4;
.super Landroid/os/Handler;
.source "MenuSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    .prologue
    .line 1148
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 16
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 1150
    move-object/from16 v0, p1

    iget v12, v0, Landroid/os/Message;->what:I

    const/16 v13, 0x3f3

    if-ne v12, v13, :cond_1

    .line 1151
    new-instance v6, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    const-class v13, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-direct {v6, v12, v13}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1153
    .local v6, "intent":Landroid/content/Intent;
    const-string v12, "flags"

    const/16 v13, -0x7530

    invoke-virtual {v6, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1154
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->hideLoadingDialog()V
    invoke-static {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V

    .line 1155
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    invoke-virtual {v12, v6}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 1156
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    invoke-virtual {v12}, Landroid/app/Activity;->finish()V

    .line 1248
    .end local v6    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->init()V
    invoke-static {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->access$800(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V

    .line 1249
    return-void

    .line 1157
    :cond_1
    move-object/from16 v0, p1

    iget v12, v0, Landroid/os/Message;->what:I

    const/16 v13, 0x3f2

    if-ne v12, v13, :cond_2

    .line 1158
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->isActivityVisible()Z

    move-result v12

    if-eqz v12, :cond_0

    .line 1159
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->hideAllDialog()V
    invoke-static {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V

    .line 1160
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    const v13, 0x7f0d001f

    invoke-static {v12, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    goto :goto_0

    .line 1162
    :cond_2
    move-object/from16 v0, p1

    iget v12, v0, Landroid/os/Message;->what:I

    const/16 v13, 0xca

    if-ne v12, v13, :cond_3

    .line 1163
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->hideLoadingDialog()V
    invoke-static {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V

    .line 1165
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->isActivityVisible()Z

    move-result v12

    if-eqz v12, :cond_0

    .line 1166
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    const/16 v14, 0x3f7

    new-instance v15, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$4$1;

    invoke-direct/range {v15 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$4$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$4;)V

    invoke-static {v13, v14, v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->showDialog(Landroid/content/Context;ILcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)Landroid/app/Dialog;

    move-result-object v13

    iput-object v13, v12, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mPopupDialog:Landroid/app/Dialog;

    goto :goto_0

    .line 1179
    :cond_3
    move-object/from16 v0, p1

    iget v12, v0, Landroid/os/Message;->what:I

    const/16 v13, 0xc8

    if-ne v12, v13, :cond_b

    .line 1180
    const-string v12, ""

    const-string v13, "GET_CURRENT_LOCATION_OK"

    invoke-static {v12, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1181
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    .line 1182
    .local v2, "data":Landroid/os/Bundle;
    const-string v12, "cityinfo"

    invoke-virtual {v2, v12}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    .line 1183
    .local v5, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    const-string v12, "cityxmlinfo"

    invoke-virtual {v2, v12}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1185
    .local v10, "xmlinfo":Ljava/lang/String;
    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    invoke-direct {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;-><init>()V

    .line 1186
    .local v3, "dinfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    const-string v12, "today"

    invoke-virtual {v2, v12}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v12

    check-cast v12, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    invoke-virtual {v3, v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->setTodayWeatherInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;)V

    .line 1189
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    const/4 v12, 0x7

    if-ge v4, v12, :cond_5

    .line 1190
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v2, v12}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v12

    if-eqz v12, :cond_4

    .line 1192
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    .line 1191
    invoke-virtual {v2, v12}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v12

    check-cast v12, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    invoke-virtual {v3, v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addForecastInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;)V

    .line 1189
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1196
    :cond_5
    const-string v12, "detailinfo"

    invoke-virtual {v2, v12}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v12

    check-cast v12, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    .line 1197
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfoSize()I

    move-result v9

    .line 1198
    .local v9, "size":I
    const/4 v7, 0x0

    .local v7, "j":I
    :goto_2
    if-ge v7, v9, :cond_7

    .line 1199
    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v2, v12}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v12

    if-eqz v12, :cond_6

    .line 1200
    const-string v12, "detailinfo"

    invoke-virtual {v2, v12}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v12

    check-cast v12, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    .line 1201
    invoke-virtual {v12, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;

    move-result-object v12

    .line 1200
    invoke-virtual {v3, v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addPhotosInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;)V

    .line 1198
    :cond_6
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 1205
    :cond_7
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "detailinfo"

    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v12

    check-cast v12, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    .line 1206
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getHourSize()I

    move-result v8

    .line 1207
    .local v8, "num":I
    if-eqz v8, :cond_8

    .line 1208
    const/4 v11, 0x0

    .local v11, "z":I
    :goto_3
    if-ge v11, v8, :cond_8

    .line 1209
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "detailinfo"

    .line 1210
    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v12

    check-cast v12, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    invoke-virtual {v12, v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getHourInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;

    move-result-object v12

    .line 1209
    invoke-virtual {v3, v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addHourInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;)V

    .line 1208
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 1215
    .end local v11    # "z":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    invoke-virtual {v12}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v12

    invoke-static {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getAllCityListCount(Landroid/content/Context;)I

    move-result v1

    .line 1216
    .local v1, "CityListCount":I
    const-string v12, ""

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "MS@CityListCount : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1217
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->saveData(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)V
    invoke-static {v12, v5, v3, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)V

    .line 1218
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->hideAllDialog()V
    invoke-static {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V

    .line 1219
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    const/4 v13, 0x1

    invoke-static {v12, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateCheckCurrentCityLocation(Landroid/content/Context;I)I

    .line 1220
    if-nez v1, :cond_9

    .line 1221
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;
    invoke-static {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)Landroid/content/Context;

    move-result-object v12

    const/4 v13, 0x1

    const/4 v14, 0x1

    invoke-static {v12, v13, v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;->setWidgetAtDaemon(Landroid/content/Context;ZZ)V

    .line 1222
    :cond_9
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    invoke-static {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;->checkSameDaemonCityIdAtDate(Landroid/content/Context;)V

    .line 1224
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    invoke-static {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getNotificationSettings(Landroid/content/Context;)I

    move-result v12

    const/4 v13, 0x1

    if-ne v12, v13, :cond_a

    .line 1225
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->set_notifications_preferences:Landroid/preference/CheckBoxPreference;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)Landroid/preference/CheckBoxPreference;

    move-result-object v13

    const/4 v14, 0x1

    invoke-static {v12, v13, v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->saveSwitchState(Landroid/content/Context;Landroid/preference/CheckBoxPreference;Z)V

    .line 1228
    :cond_a
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    new-instance v13, Landroid/content/Intent;

    const-string v14, "com.sec.android.widgetapp.ap.hero.accuweather.action.CHANGE_SETTING"

    invoke-direct {v13, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v13}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1231
    .end local v1    # "CityListCount":I
    .end local v2    # "data":Landroid/os/Bundle;
    .end local v3    # "dinfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .end local v4    # "i":I
    .end local v5    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .end local v7    # "j":I
    .end local v8    # "num":I
    .end local v9    # "size":I
    .end local v10    # "xmlinfo":Ljava/lang/String;
    :cond_b
    move-object/from16 v0, p1

    iget v12, v0, Landroid/os/Message;->what:I

    const/16 v13, 0xc9

    if-ne v12, v13, :cond_d

    .line 1232
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->hideAllDialog()V
    invoke-static {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V

    .line 1234
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->isActivityVisible()Z

    move-result v12

    if-eqz v12, :cond_0

    .line 1235
    sget-boolean v12, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isVerizon:Z

    if-eqz v12, :cond_c

    .line 1236
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    const v13, 0x7f0d002e

    invoke-static {v12, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    goto/16 :goto_0

    .line 1238
    :cond_c
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    const v13, 0x7f0d002d

    invoke-static {v12, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    goto/16 :goto_0

    .line 1242
    :cond_d
    move-object/from16 v0, p1

    iget v12, v0, Landroid/os/Message;->what:I

    const/16 v13, 0x3e7

    if-ne v12, v13, :cond_0

    .line 1243
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->hideAllDialog()V
    invoke-static {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V

    .line 1244
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    const/16 v13, 0x3e7

    const/4 v14, 0x0

    invoke-virtual {v12, v13, v14}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1245
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    invoke-virtual {v12}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0
.end method
