.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;
.super Landroid/view/View;
.source "PartlySunnyIconView.java"

# interfaces
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;


# instance fields
.field SunRotation:Landroid/animation/ValueAnimator;

.field isStop:Z

.field l:Landroid/animation/Animator$AnimatorListener;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mCloud:Landroid/graphics/Path;

.field private mIsActiveAnimationThread:Z

.field private mMasking:Landroid/graphics/Path;

.field private mPaint:Landroid/graphics/Paint;

.field private mPaintColor:I

.field private mScale:F

.field private mSun:Landroid/graphics/Path;

.field private mSunOuter:Landroid/graphics/Path;

.field rotateOffset:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 64
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 65
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 60
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 55
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->SunRotation:Landroid/animation/ValueAnimator;

    .line 29
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mIsActiveAnimationThread:Z

    .line 31
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mPaint:Landroid/graphics/Paint;

    .line 33
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mMasking:Landroid/graphics/Path;

    .line 35
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    .line 37
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mSun:Landroid/graphics/Path;

    .line 39
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mSunOuter:Landroid/graphics/Path;

    .line 41
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    .line 44
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mBitmap:Landroid/graphics/Bitmap;

    .line 48
    const v0, -0xa0a0b

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mPaintColor:I

    .line 325
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->rotateOffset:F

    .line 326
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->l:Landroid/animation/Animator$AnimatorListener;

    .line 392
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->isStop:Z

    .line 56
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->init()V

    .line 57
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mIsActiveAnimationThread:Z

    return v0
.end method

.method private drawCloud(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "paint"    # Landroid/graphics/Paint;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 227
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mPaintColor:I

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 228
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 229
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    invoke-virtual {p1, v0, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 230
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 231
    return-void
.end method

.method private drawSun(Landroid/graphics/Canvas;Landroid/graphics/Paint;F)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "paint"    # Landroid/graphics/Paint;
    .param p3, "rotation"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const v4, 0x42df75c3    # 111.73f

    const v3, 0x42a2d1ec    # 81.41f

    .line 234
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 235
    const/high16 v0, -0x3f600000    # -5.0f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v0, v1

    invoke-virtual {p1, v2, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 236
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mSun:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 238
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 239
    add-float v0, v2, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v4

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 240
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mSunOuter:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 241
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 243
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 244
    const/high16 v0, 0x42b40000    # 90.0f

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v4

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 245
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mSunOuter:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 246
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 248
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 249
    const/high16 v0, 0x43340000    # 180.0f

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v4

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 250
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mSunOuter:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 251
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 253
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 254
    const/high16 v0, 0x43870000    # 270.0f

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v4

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 255
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mSunOuter:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 256
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 258
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 259
    const/high16 v0, 0x42340000    # 45.0f

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v4

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 260
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mSunOuter:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 261
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 263
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 264
    const/high16 v0, 0x43070000    # 135.0f

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v4

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 265
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mSunOuter:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 266
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 268
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 269
    const/high16 v0, 0x43610000    # 225.0f

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v4

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 270
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mSunOuter:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 271
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 273
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 274
    const v0, 0x439d8000    # 315.0f

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v4

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 275
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mSunOuter:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 276
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 279
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 280
    return-void
.end method

.method private drawTransparentArea(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "paint"    # Landroid/graphics/Paint;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 221
    new-instance v0, Landroid/graphics/PorterDuffXfermode;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v1}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 222
    invoke-virtual {p1, p2, v2, v2, p3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 223
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 224
    return-void
.end method

.method private init()V
    .locals 4

    .prologue
    const/high16 v3, 0x43960000    # 300.0f

    .line 68
    invoke-virtual {p0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 69
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mPaint:Landroid/graphics/Paint;

    .line 70
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mPaintColor:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 71
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 72
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 73
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x40a00000    # 5.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 75
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->initPath()V

    .line 77
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mBitmap:Landroid/graphics/Bitmap;

    .line 79
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 80
    .local v0, "mMaskingCanvas":Landroid/graphics/Canvas;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mMasking:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 81
    return-void
.end method

.method private initPath()V
    .locals 13

    .prologue
    const v12, 0x42948c4a

    const v11, 0x4274b333    # 61.175f

    const v10, 0x43358fdf

    const v9, 0x432fcac1

    const v8, 0x42a3e560

    .line 84
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mMasking:Landroid/graphics/Path;

    .line 85
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x433613f8

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42fd1b23

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 86
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x43435917

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42fda7f0

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x434e045a

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4309d4fe    # 137.832f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x434e045a

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43172a3d

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 88
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x434e045a

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4324ab44

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x434311ec    # 195.07f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v9

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v10

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v9

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 90
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x42932f1b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v9

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 91
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x42857852    # 66.735f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v9

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v11

    const v4, 0x432a1021

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v11

    const v6, 0x432334bc

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 93
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mMasking:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v11

    const v2, 0x431c5958    # 156.349f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42857852    # 66.735f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4316b47b

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42932f1b

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4316b47b

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 95
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x4293ef1b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4316b47b

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4294ac08

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4316af1b

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x429567f0

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4316b78d

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 97
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x4294d99a

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43157f7d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v12

    const v4, 0x431435c3    # 148.21f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v12

    const v6, 0x4312e7ae

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 99
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mMasking:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v12

    const v2, 0x4309824e

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42a3c831

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4301e148    # 129.88f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42b693f8

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4301e148    # 129.88f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 101
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x42b6ab02    # 91.334f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4301e148    # 129.88f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42b6c083    # 91.376f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4301e189

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42b6d78d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4301e189

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 103
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x42ce8fdf

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42ecec08

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 104
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x42d8b0a4

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42dd0a3d    # 110.52f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42e8276d

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42d0e148    # 104.44f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42fa6e98

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42caf646

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 106
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x4300dc6a

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42c8ad0e

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43052ed9

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42c6fdf4

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4309d2f2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42c6fdf4

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 108
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x4318ad50

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42c6fdf4

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4326c625

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42d8322d    # 108.098f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x432dbae1    # 173.73f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42f18ac1

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 110
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mMasking:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 112
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    .line 113
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v10

    const v2, 0x43369c29    # 182.61f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 114
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x4346ce56    # 198.806f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43369c29    # 182.61f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4354d5c3

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x432868b4

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4354d5c3

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43172a3d

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x4354d5c3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4305ebc7

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4346ce56    # 198.806f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42efcb44

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v10

    const v6, 0x42efcb44

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 118
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x433554fe    # 181.332f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42efcb44

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43351a5e

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42efa0c5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4334df3b

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42efa2d1    # 119.818f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 120
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x432cfe77

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42cee148    # 103.44f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431c51ec    # 156.32f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42b971aa    # 92.722f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4309d2f2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42b971aa    # 92.722f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 122
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x430776c9

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42b971aa    # 92.722f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43051e35

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42b9c6a8    # 92.888f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4302cd0e

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42ba7852    # 93.235f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 124
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42f04106

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42bd5db2

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42dd072b    # 110.514f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42c668f6

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42ce61cb

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42d636c9

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42c77cee

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42dda6e9

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42c1e354    # 96.944f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42e62560

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42bda666

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42ef6979

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 128
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42bc8f5c    # 94.28f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42f1ab02    # 120.834f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42bb87ae    # 93.765f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42f3f1aa    # 121.972f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42ba9db2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42f647ae    # 123.14f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 130
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42b9472b    # 92.639f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42f62b85    # 123.085f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42b7ee98

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42f61ba6    # 123.054f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42b693f8

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42f61ba6    # 123.054f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 132
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x429db74c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42f61ba6    # 123.054f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42893d71    # 68.62f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43049e35

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42871893

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4310c6a8    # 144.776f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 134
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x426f9375

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43135062

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42596e98

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x431ab5c3    # 154.71f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42596e98

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x432334fe    # 163.207f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 136
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42596e98

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432dcfdf

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x427bf2b0    # 62.987f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43369cac    # 182.612f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42932f9e

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43369cac    # 182.612f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 138
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v10

    const v2, 0x43369c29    # 182.61f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 139
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v10

    const v2, 0x43369c29    # 182.61f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 140
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 141
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v10

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v9

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 142
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42932f1b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v9

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 143
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42857852    # 66.735f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v9

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v11

    const v4, 0x432a1021

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v11

    const v6, 0x432334bc

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 145
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v11

    const v2, 0x431c5958    # 156.349f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42857852    # 66.735f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4316b47b

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42932f1b

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4316b47b

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 147
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x4293ef1b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4316b47b

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4294ac08

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4316af1b

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x429567f0

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4316b78d

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 149
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x4294d99a

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43157f7d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v12

    const v4, 0x431435c3    # 148.21f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v12

    const v6, 0x4312e7ae

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 151
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v12

    const v2, 0x4309824e

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42a3c831

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4301e148    # 129.88f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42b693f8

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4301e148    # 129.88f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 153
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42b6ab02    # 91.334f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4301e148    # 129.88f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42b6c083    # 91.376f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4301e189

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42b6d78d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4301e189

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 155
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42b52b85    # 90.585f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4305a9fc

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42b44ccd    # 90.15f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x430997cf

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42b44ccd    # 90.15f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x430d9cac    # 141.612f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 157
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42b44ccd    # 90.15f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x430f7eb8

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42b75a1d

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x431104dd

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42bb1db2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x431104dd

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 159
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42bee1cb

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x431104dd

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42c1ef1b

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x430f7df4

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42c1ef1b

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x430d9be7

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 161
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42c1ef1b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4304d687

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42c688b4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42f9ee98

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42ce8fdf

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42ecec08

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 163
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42d8b0a4

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42dd0a3d    # 110.52f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42e8276d

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42d0e148    # 104.44f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42fa6e98

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42caf646

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 165
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x4300dc6a

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42c8ad0e

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43052ed9

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42c6fdf4

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4309d2f2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42c6fdf4

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 167
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x4318ad50

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42c6fdf4

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4326c625

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42d8322d    # 108.098f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x432dbae1    # 173.73f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42f18ac1

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 169
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x4325d021

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42f593f8

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431ee20c

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42ff7c6a

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431a5e35

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4306db64

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 171
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x43195ba6

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4308722d    # 136.446f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4319d3b6

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x430a8d50

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431b6ac1

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x430b8fdf

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 173
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x431bfc29    # 155.985f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x430bebc7

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431c9db2

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x430c17cf

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431d3df4

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x430c17cf

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 175
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x431e5eb8    # 158.37f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x430c17cf

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431f7917

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x430b8873

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43201f7d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x430a82d1

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 177
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x4324dd2f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43030a3d    # 131.04f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x432bf581    # 171.959f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42fd1b23

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x433613f8

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42fd1b23

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 179
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x43435917

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42fda7f0

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x434e045a

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4309d4fe    # 137.832f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x434e045a

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43172a3d

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 181
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x434e045a

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4324ab44

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x434311ec    # 195.07f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v9

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v10

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v9

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 183
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 185
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mSunOuter:Landroid/graphics/Path;

    .line 186
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mSunOuter:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v8

    const v2, 0x426ff9db    # 59.994f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 187
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mSunOuter:Landroid/graphics/Path;

    const v1, 0x42a01aa0

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x426ff9db    # 59.994f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x429d07ae    # 78.515f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4269d3f8    # 58.457f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x429d07ae    # 78.515f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42623f7d    # 56.562f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 189
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mSunOuter:Landroid/graphics/Path;

    const v1, 0x429d07ae    # 78.515f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42219cac    # 40.403f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 190
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mSunOuter:Landroid/graphics/Path;

    const v1, 0x429d07ae    # 78.515f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x421a072b    # 38.507f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42a01aa0

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4213e148    # 36.97f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v8

    const v6, 0x4213e148    # 36.97f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 192
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mSunOuter:Landroid/graphics/Path;

    const v1, 0x42a7b021

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4213e148    # 36.97f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42aac312

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x421a072b    # 38.507f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42aac312

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42219cac    # 40.403f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 194
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mSunOuter:Landroid/graphics/Path;

    const v1, 0x42aac312

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42623f7d    # 56.562f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 195
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mSunOuter:Landroid/graphics/Path;

    const v1, 0x42aac28f    # 85.38f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4269d3f8    # 58.457f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42a7af9e

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x426ff9db    # 59.994f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v8

    const v6, 0x426ff9db    # 59.994f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 198
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mSun:Landroid/graphics/Path;

    .line 199
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mSun:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v8

    const v2, 0x4317076d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 200
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mSun:Landroid/graphics/Path;

    const v1, 0x4271c8b4    # 60.446f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4317076d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x422bced9    # 42.952f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x430588f6

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x422bced9    # 42.952f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42e01810

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 202
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mSun:Landroid/graphics/Path;

    const v1, 0x422bced9    # 42.952f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42b51aa0

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4271c7ae    # 60.445f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42921cac    # 73.056f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v8

    const v6, 0x42921cac    # 73.056f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 204
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mSun:Landroid/graphics/Path;

    const v1, 0x42cee6e9

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42921cac    # 73.056f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42f1e979

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42b5199a    # 90.55f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42f1e979

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42e01810

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 206
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mSun:Landroid/graphics/Path;

    const v1, 0x42f1e979

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43058979

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42cee6e9

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4317076d

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v8

    const v6, 0x4317076d

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 208
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mSun:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 209
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mSun:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v8

    const v2, 0x429fd810

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 210
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mSun:Landroid/graphics/Path;

    const v1, 0x42807646

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x429fd810

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4247449c

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42bcac8b

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4247449c

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42e01810

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 212
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mSun:Landroid/graphics/Path;

    const v1, 0x4247449c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4301c000    # 129.75f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42807646

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43102a3d

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v8

    const v6, 0x43102a3d

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 214
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mSun:Landroid/graphics/Path;

    const v1, 0x42c753f8

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43102a3d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42e42e14    # 114.09f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4301c000    # 129.75f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42e42e14    # 114.09f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42e01810

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 216
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mSun:Landroid/graphics/Path;

    const v1, 0x42e42e14    # 114.09f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42bcac08

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42c753f8

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x429fd810

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v5, v8

    const v6, 0x429fd810

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 218
    return-void
.end method


# virtual methods
.method public cancelAnimation()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 385
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->SunRotation:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->SunRotation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 386
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->SunRotation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 389
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mIsActiveAnimationThread:Z

    .line 390
    return-void
.end method

.method public getScale()F
    .locals 1

    .prologue
    .line 411
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 323
    return-object p0
.end method

.method public isRunning()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 398
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->SunRotation:Landroid/animation/ValueAnimator;

    if-nez v0, :cond_0

    .line 399
    const/4 v0, 0x0

    .line 402
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->SunRotation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v4, 0x43960000    # 300.0f

    const/4 v2, 0x0

    .line 284
    :try_start_0
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 286
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    cmpg-float v0, v0, v2

    if-gtz v0, :cond_1

    .line 287
    const-string v0, ""

    const-string v1, "scale is less then 0"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    :cond_0
    :goto_0
    return-void

    .line 292
    :cond_1
    const/4 v8, 0x0

    .line 293
    .local v8, "rotation":F
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->SunRotation:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->SunRotation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 294
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->SunRotation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->rotateOffset:F

    add-float v8, v0, v1

    .line 297
    :cond_2
    const/16 v6, 0x1f

    .line 299
    .local v6, "saveFlag":I
    const/16 v6, 0xf

    .line 302
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float v1, v2, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v2, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float v3, v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    mul-float/2addr v4, v0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->saveLayer(FFFFLandroid/graphics/Paint;I)I

    move-result v9

    .line 306
    .local v9, "transparentSaveLevel":I
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mPaint:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->drawSun(Landroid/graphics/Canvas;Landroid/graphics/Paint;F)V

    .line 307
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mPaint:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->drawTransparentArea(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V

    .line 309
    invoke-virtual {p1, v9}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 311
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mPaint:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->drawCloud(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    .line 312
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 313
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->cancelAnimation()V

    .line 314
    const-string v0, ""

    const-string v1, "cancelA"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 317
    .end local v6    # "saveFlag":I
    .end local v8    # "rotation":F
    .end local v9    # "transparentSaveLevel":I
    :catch_0
    move-exception v7

    .line 318
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 440
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 419
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onViewDetachedFromWindow : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 421
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->SunRotation:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 422
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->SunRotation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 423
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->SunRotation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 426
    :cond_0
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->SunRotation:Landroid/animation/ValueAnimator;

    .line 427
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mPaint:Landroid/graphics/Paint;

    .line 428
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mMasking:Landroid/graphics/Path;

    .line 429
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mCloud:Landroid/graphics/Path;

    .line 430
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mSun:Landroid/graphics/Path;

    .line 431
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mSunOuter:Landroid/graphics/Path;

    .line 432
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 433
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 435
    :cond_1
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mBitmap:Landroid/graphics/Bitmap;

    .line 436
    return-void
.end method

.method public setPaintColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 51
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mPaintColor:I

    .line 52
    return-void
.end method

.method public setScale(F)V
    .locals 0
    .param p1, "scale"    # F

    .prologue
    .line 406
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mScale:F

    .line 407
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->init()V

    .line 408
    return-void
.end method

.method public setWidthAndHeight(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 415
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, p1, p2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 416
    .local v0, "l":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 417
    return-void
.end method

.method public startAnimation()V
    .locals 4

    .prologue
    .line 352
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->isStop:Z

    .line 353
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->rotateOffset:F

    .line 357
    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->SunRotation:Landroid/animation/ValueAnimator;

    .line 358
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->SunRotation:Landroid/animation/ValueAnimator;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 359
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->SunRotation:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x5dc

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 360
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->SunRotation:Landroid/animation/ValueAnimator;

    new-instance v2, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v2}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 361
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->SunRotation:Landroid/animation/ValueAnimator;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 362
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->SunRotation:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    .line 365
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->mIsActiveAnimationThread:Z

    .line 366
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;)V

    .line 379
    .local v0, "t":Ljava/lang/Thread;
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 380
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 381
    return-void

    .line 357
    :array_0
    .array-data 4
        0x0
        0x42340000    # 45.0f
    .end array-data
.end method

.method public stopAnimation()V
    .locals 1

    .prologue
    .line 394
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyIconView;->isStop:Z

    .line 395
    return-void
.end method
