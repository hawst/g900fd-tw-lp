.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;
.super Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/Animation;
.source "ScrollAnimation.java"


# static fields
.field public static final SCROLL_DOWN:I = 0x202

.field public static final SCROLL_UP:I = 0x201


# instance fields
.field private mCurCityBGView:Landroid/view/View;

.field private mCurCityInfoView:Landroid/view/View;

.field private mDirecton:I

.field private mNextCityBGView:Landroid/view/View;

.field private mNextCityInfoView:Landroid/view/View;

.field private moveCityBGAnimatorSet:Landroid/animation/AnimatorSet;

.field private moveCityInfoInAnimatorSet:Landroid/animation/AnimatorSet;

.field private moveCityInfoOutAnimatorSet:Landroid/animation/AnimatorSet;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 30
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/Animation;-><init>()V

    .line 16
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->mCurCityInfoView:Landroid/view/View;

    .line 18
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->mNextCityInfoView:Landroid/view/View;

    .line 20
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->mCurCityBGView:Landroid/view/View;

    .line 22
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->mNextCityBGView:Landroid/view/View;

    .line 24
    const/16 v0, 0x201

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->mDirecton:I

    .line 26
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->moveCityBGAnimatorSet:Landroid/animation/AnimatorSet;

    .line 27
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->moveCityInfoOutAnimatorSet:Landroid/animation/AnimatorSet;

    .line 28
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->moveCityInfoInAnimatorSet:Landroid/animation/AnimatorSet;

    .line 31
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->moveCityBGAnimatorSet:Landroid/animation/AnimatorSet;

    .line 32
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->moveCityInfoOutAnimatorSet:Landroid/animation/AnimatorSet;

    .line 33
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->moveCityInfoInAnimatorSet:Landroid/animation/AnimatorSet;

    .line 34
    return-void
.end method


# virtual methods
.method public animate()V
    .locals 2

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->mDirecton:I

    const/16 v1, 0x201

    if-ne v0, v1, :cond_0

    .line 38
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->playScrollUpAnim()V

    .line 42
    :goto_0
    return-void

    .line 40
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->playScrollDownAnim()V

    goto :goto_0
.end method

.method public cancelAnimation()V
    .locals 3

    .prologue
    .line 170
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->moveCityBGAnimatorSet:Landroid/animation/AnimatorSet;

    if-eqz v1, :cond_0

    .line 171
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->moveCityBGAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->removeAllListeners()V

    .line 172
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->moveCityBGAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->cancel()V

    .line 173
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->moveCityBGAnimatorSet:Landroid/animation/AnimatorSet;

    .line 175
    :cond_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->moveCityInfoOutAnimatorSet:Landroid/animation/AnimatorSet;

    if-eqz v1, :cond_1

    .line 176
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->moveCityInfoOutAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->removeAllListeners()V

    .line 177
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->moveCityInfoOutAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->cancel()V

    .line 178
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->moveCityInfoOutAnimatorSet:Landroid/animation/AnimatorSet;

    .line 180
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->moveCityInfoInAnimatorSet:Landroid/animation/AnimatorSet;

    if-eqz v1, :cond_2

    .line 181
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->moveCityInfoInAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->removeAllListeners()V

    .line 182
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->moveCityInfoInAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->cancel()V

    .line 183
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->moveCityInfoInAnimatorSet:Landroid/animation/AnimatorSet;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 189
    :cond_2
    :goto_0
    return-void

    .line 185
    :catch_0
    move-exception v0

    .line 186
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 187
    const-string v1, ""

    const-string v2, "cA NPE"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public playScrollDownAnim()V
    .locals 10

    .prologue
    .line 96
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->mCurCityInfoView:Landroid/view/View;

    const-string v6, "y"

    const/4 v7, 0x2

    new-array v7, v7, [F

    const/4 v8, 0x0

    const/4 v9, 0x0

    aput v9, v7, v8

    const/4 v8, 0x1

    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->mCurCityBGView:Landroid/view/View;

    .line 97
    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    move-result v9

    int-to-float v9, v9

    aput v9, v7, v8

    .line 96
    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 98
    .local v4, "outInfoAni":Landroid/animation/ObjectAnimator;
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->mNextCityInfoView:Landroid/view/View;

    const-string v6, "y"

    const/4 v7, 0x2

    new-array v7, v7, [F

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->mNextCityBGView:Landroid/view/View;

    .line 99
    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    move-result v9

    neg-int v9, v9

    int-to-float v9, v9

    aput v9, v7, v8

    const/4 v8, 0x1

    const/4 v9, 0x0

    aput v9, v7, v8

    .line 98
    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 100
    .local v2, "inInfoAni":Landroid/animation/ObjectAnimator;
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->mCurCityBGView:Landroid/view/View;

    const-string v6, "y"

    const/4 v7, 0x2

    new-array v7, v7, [F

    const/4 v8, 0x0

    const/4 v9, 0x0

    aput v9, v7, v8

    const/4 v8, 0x1

    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->mCurCityInfoView:Landroid/view/View;

    .line 101
    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    move-result v9

    int-to-float v9, v9

    aput v9, v7, v8

    .line 100
    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 102
    .local v3, "outBGAni":Landroid/animation/ObjectAnimator;
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->mNextCityBGView:Landroid/view/View;

    const-string v6, "y"

    const/4 v7, 0x2

    new-array v7, v7, [F

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->mNextCityInfoView:Landroid/view/View;

    .line 103
    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    move-result v9

    neg-int v9, v9

    int-to-float v9, v9

    aput v9, v7, v8

    const/4 v8, 0x1

    const/4 v9, 0x0

    aput v9, v7, v8

    .line 102
    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 106
    .local v1, "inBGAni":Landroid/animation/ObjectAnimator;
    const-wide/16 v5, 0x0

    invoke-static {v5, v6}, Landroid/animation/ObjectAnimator;->setFrameDelay(J)V

    .line 108
    invoke-virtual {p0, v4, v2, v3, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->startScrollAnimation(Landroid/animation/ObjectAnimator;Landroid/animation/ObjectAnimator;Landroid/animation/ObjectAnimator;Landroid/animation/ObjectAnimator;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 113
    .end local v1    # "inBGAni":Landroid/animation/ObjectAnimator;
    .end local v2    # "inInfoAni":Landroid/animation/ObjectAnimator;
    .end local v3    # "outBGAni":Landroid/animation/ObjectAnimator;
    .end local v4    # "outInfoAni":Landroid/animation/ObjectAnimator;
    :goto_0
    return-void

    .line 109
    :catch_0
    move-exception v0

    .line 110
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 111
    const-string v5, ""

    const-string v6, "sSA NPE"

    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public playScrollUpAnim()V
    .locals 10

    .prologue
    .line 70
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->mCurCityInfoView:Landroid/view/View;

    const-string v6, "y"

    const/4 v7, 0x2

    new-array v7, v7, [F

    const/4 v8, 0x0

    const/4 v9, 0x0

    aput v9, v7, v8

    const/4 v8, 0x1

    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->mCurCityBGView:Landroid/view/View;

    .line 71
    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    move-result v9

    neg-int v9, v9

    int-to-float v9, v9

    aput v9, v7, v8

    .line 70
    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 72
    .local v4, "outInfoAni":Landroid/animation/ObjectAnimator;
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->mNextCityInfoView:Landroid/view/View;

    const-string v6, "y"

    const/4 v7, 0x2

    new-array v7, v7, [F

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->mNextCityBGView:Landroid/view/View;

    .line 73
    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    move-result v9

    int-to-float v9, v9

    aput v9, v7, v8

    const/4 v8, 0x1

    const/4 v9, 0x0

    aput v9, v7, v8

    .line 72
    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 74
    .local v2, "inInfoAni":Landroid/animation/ObjectAnimator;
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->mCurCityBGView:Landroid/view/View;

    const-string v6, "y"

    const/4 v7, 0x2

    new-array v7, v7, [F

    const/4 v8, 0x0

    const/4 v9, 0x0

    aput v9, v7, v8

    const/4 v8, 0x1

    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->mCurCityInfoView:Landroid/view/View;

    .line 75
    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    move-result v9

    neg-int v9, v9

    int-to-float v9, v9

    aput v9, v7, v8

    .line 74
    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 76
    .local v3, "outBGAni":Landroid/animation/ObjectAnimator;
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->mNextCityBGView:Landroid/view/View;

    const-string v6, "y"

    const/4 v7, 0x2

    new-array v7, v7, [F

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->mNextCityInfoView:Landroid/view/View;

    .line 77
    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    move-result v9

    int-to-float v9, v9

    aput v9, v7, v8

    const/4 v8, 0x1

    const/4 v9, 0x0

    aput v9, v7, v8

    .line 76
    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 80
    .local v1, "inBGAni":Landroid/animation/ObjectAnimator;
    const-wide/16 v5, 0x0

    invoke-static {v5, v6}, Landroid/animation/ObjectAnimator;->setFrameDelay(J)V

    .line 82
    invoke-virtual {p0, v4, v2, v3, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->startScrollAnimation(Landroid/animation/ObjectAnimator;Landroid/animation/ObjectAnimator;Landroid/animation/ObjectAnimator;Landroid/animation/ObjectAnimator;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 87
    .end local v1    # "inBGAni":Landroid/animation/ObjectAnimator;
    .end local v2    # "inInfoAni":Landroid/animation/ObjectAnimator;
    .end local v3    # "outBGAni":Landroid/animation/ObjectAnimator;
    .end local v4    # "outInfoAni":Landroid/animation/ObjectAnimator;
    :goto_0
    return-void

    .line 83
    :catch_0
    move-exception v0

    .line 84
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 85
    const-string v5, ""

    const-string v6, "sSA NPE"

    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setDirection(I)V
    .locals 0
    .param p1, "direction"    # I

    .prologue
    .line 45
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->mDirecton:I

    .line 46
    return-void
.end method

.method public setViews(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 50
    .local p1, "views":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    :try_start_0
    invoke-super {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/Animation;->setViews(Ljava/util/ArrayList;)V

    .line 51
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->mCurCityBGView:Landroid/view/View;

    .line 52
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->mNextCityBGView:Landroid/view/View;

    .line 53
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->mCurCityInfoView:Landroid/view/View;

    .line 54
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->mNextCityInfoView:Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    .line 62
    :goto_0
    return-void

    .line 55
    :catch_0
    move-exception v0

    .line 56
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 57
    const-string v1, ""

    const-string v2, "sSA NPE"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 58
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v0

    .line 59
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    .line 60
    const-string v1, ""

    const-string v2, "sSA IOOBE"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected startScrollAnimation(Landroid/animation/ObjectAnimator;Landroid/animation/ObjectAnimator;Landroid/animation/ObjectAnimator;Landroid/animation/ObjectAnimator;)V
    .locals 4
    .param p1, "outAni"    # Landroid/animation/ObjectAnimator;
    .param p2, "inAni"    # Landroid/animation/ObjectAnimator;
    .param p3, "outBGAni"    # Landroid/animation/ObjectAnimator;
    .param p4, "inBGAni"    # Landroid/animation/ObjectAnimator;

    .prologue
    .line 131
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->moveCityBGAnimatorSet:Landroid/animation/AnimatorSet;

    if-eqz v1, :cond_0

    .line 132
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->moveCityBGAnimatorSet:Landroid/animation/AnimatorSet;

    .line 135
    :cond_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->moveCityInfoOutAnimatorSet:Landroid/animation/AnimatorSet;

    if-eqz v1, :cond_1

    .line 136
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->moveCityInfoOutAnimatorSet:Landroid/animation/AnimatorSet;

    .line 139
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->moveCityInfoInAnimatorSet:Landroid/animation/AnimatorSet;

    if-eqz v1, :cond_2

    .line 140
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->moveCityInfoInAnimatorSet:Landroid/animation/AnimatorSet;

    .line 143
    :cond_2
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->moveCityBGAnimatorSet:Landroid/animation/AnimatorSet;

    .line 144
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->moveCityInfoOutAnimatorSet:Landroid/animation/AnimatorSet;

    .line 145
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->moveCityInfoInAnimatorSet:Landroid/animation/AnimatorSet;

    .line 147
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->moveCityBGAnimatorSet:Landroid/animation/AnimatorSet;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 148
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->moveCityBGAnimatorSet:Landroid/animation/AnimatorSet;

    const/4 v2, 0x2

    new-array v2, v2, [Landroid/animation/Animator;

    const/4 v3, 0x0

    aput-object p3, v2, v3

    const/4 v3, 0x1

    aput-object p4, v2, v3

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 149
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->moveCityBGAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->start()V

    .line 151
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->moveCityInfoOutAnimatorSet:Landroid/animation/AnimatorSet;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 152
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->moveCityInfoOutAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v1, p1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 153
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->moveCityInfoOutAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->start()V

    .line 155
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->moveCityInfoInAnimatorSet:Landroid/animation/AnimatorSet;

    const-wide/16 v2, 0x4b0

    invoke-virtual {v1, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 156
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->moveCityInfoInAnimatorSet:Landroid/animation/AnimatorSet;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->mListener:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 157
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->moveCityInfoInAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v1, p2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 158
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->moveCityInfoInAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->start()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 163
    :goto_0
    return-void

    .line 159
    :catch_0
    move-exception v0

    .line 160
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 161
    const-string v1, ""

    const-string v2, "sSA NPE"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
