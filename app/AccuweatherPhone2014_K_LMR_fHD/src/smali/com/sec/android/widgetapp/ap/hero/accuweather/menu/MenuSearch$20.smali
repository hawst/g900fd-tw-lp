.class Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$20;
.super Landroid/os/Handler;
.source "MenuSearch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 664
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$20;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 666
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$20;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->performMapCancled:Z

    if-nez v0, :cond_1

    .line 667
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$20;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->performMapCancled:Z

    .line 668
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$20;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$2100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 669
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$20;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$2100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->clearResource()V

    .line 670
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$20;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$2102(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    .line 672
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$20;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->hideLoadingDialog()V
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$2000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    .line 673
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$20;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .line 674
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->ctx:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$2300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0d001f

    .line 673
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    .line 677
    :cond_1
    return-void
.end method
