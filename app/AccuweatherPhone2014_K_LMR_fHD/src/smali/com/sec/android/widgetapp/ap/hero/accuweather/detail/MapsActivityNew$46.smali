.class Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;
.super Landroid/os/Handler;
.source "MapsActivityNew.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field resultArrived:Z

.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V
    .locals 1
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 1765
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1766
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->resultArrived:Z

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 13
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1769
    iget v8, p1, Landroid/os/Message;->what:I

    const v9, -0x13d30

    if-ne v8, v9, :cond_1

    .line 1770
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    iget-object v8, v8, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->detailAddHandler:Landroid/os/Handler;

    if-eqz v8, :cond_0

    .line 1771
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    iget-object v8, v8, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->detailAddHandler:Landroid/os/Handler;

    const v9, -0x140b4

    const-wide/16 v10, 0x7530

    invoke-virtual {v8, v9, v10, v11}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1864
    :cond_0
    :goto_0
    return-void

    .line 1773
    :cond_1
    iget v8, p1, Landroid/os/Message;->what:I

    const v9, -0x140b4

    if-ne v8, v9, :cond_2

    .line 1774
    iget-boolean v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->resultArrived:Z

    if-nez v8, :cond_0

    .line 1775
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->hideLoadingDialog()V
    invoke-static {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$2300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    .line 1776
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    const v9, 0x7f0d001f

    invoke-static {v8, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    goto :goto_0

    .line 1779
    :cond_2
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->resultArrived:Z

    .line 1780
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "RESPONSE_BODY"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1781
    .local v0, "body":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "RESPONSE_CODE"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 1783
    .local v5, "result":I
    const/16 v8, 0xc8

    if-ne v5, v8, :cond_b

    .line 1784
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mParser:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;
    invoke-static {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$3800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mTempScale:I
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)I

    move-result v9

    .line 1785
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    .line 1784
    invoke-virtual {v8, v9, v0, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->parseDetailWeather(ILjava/lang/String;Ljava/lang/String;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    move-result-object v3

    .line 1786
    .local v3, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->detailitem:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    invoke-static {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLocation()Ljava/lang/String;

    move-result-object v4

    .line 1788
    .local v4, "location":Ljava/lang/String;
    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    invoke-direct {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;-><init>()V

    .line 1789
    .local v2, "imsi":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->detailitem:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    invoke-static {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getCity()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setCity(Ljava/lang/String;)V

    .line 1790
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->detailitem:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    invoke-static {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLocation()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setLocation(Ljava/lang/String;)V

    .line 1791
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->detailitem:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    invoke-static {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLatitude()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setLatitude(Ljava/lang/String;)V

    .line 1792
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->detailitem:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    invoke-static {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLongitude()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setLongitude(Ljava/lang/String;)V

    .line 1793
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->detailitem:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    invoke-static {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getProvider()I

    move-result v8

    invoke-virtual {v2, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setProvider(I)V

    .line 1794
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->detailitem:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    invoke-static {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getState()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setState(Ljava/lang/String;)V

    .line 1795
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->detailitem:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    invoke-static {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLocation()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setRealLocation(Ljava/lang/String;)V

    .line 1796
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mZoomlevel:I
    invoke-static {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$1600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)I

    move-result v8

    invoke-virtual {v2, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setZoomlevel(I)V

    .line 1797
    const/4 v7, 0x0

    .line 1799
    .local v7, "todayInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    if-eqz v3, :cond_3

    .line 1800
    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v7

    .line 1802
    :cond_3
    if-eqz v7, :cond_4

    .line 1803
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mTempScale:I
    invoke-static {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setTempScale(I)V

    .line 1806
    :cond_4
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-static {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->isMaxCityListAdded(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1807
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->finish()V

    goto/16 :goto_0

    .line 1811
    :cond_5
    if-eqz v3, :cond_9

    .line 1812
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-static {v8, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->isRegisteredToCityList(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_6

    .line 1813
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-static {v8, v2, v3, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->insertCity(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)I

    move-result v5

    .line 1814
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->checkResultCode(I)I
    invoke-static {v8, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$5000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;I)I

    move-result v6

    .line 1816
    .local v6, "resultCode":I
    const/16 v8, 0x3e7

    if-ne v8, v6, :cond_7

    .line 1817
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    const/16 v9, 0x3e7

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->setResult(ILandroid/content/Intent;)V

    .line 1818
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->finish()V

    goto/16 :goto_0

    .line 1822
    .end local v6    # "resultCode":I
    :cond_6
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-static {v8, v4, v2, v3, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateCity(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)I

    move-result v5

    .line 1824
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->checkResultCode(I)I
    invoke-static {v8, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$5000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;I)I

    move-result v6

    .line 1826
    .restart local v6    # "resultCode":I
    const/16 v8, 0x3e7

    if-ne v8, v6, :cond_7

    .line 1827
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    const/16 v9, 0x3e7

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->setResult(ILandroid/content/Intent;)V

    .line 1828
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->finish()V

    goto/16 :goto_0

    .line 1833
    :cond_7
    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v8

    if-eqz v8, :cond_9

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfoSize()I

    move-result v8

    if-lez v8, :cond_9

    .line 1834
    const/4 v8, 0x0

    invoke-virtual {v3, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;->getCityId()Ljava/lang/String;

    move-result-object v1

    .line 1835
    .local v1, "cityId":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-static {v8, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getExistPhotos(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 1836
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-static {v8, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->deletePhotosInfo(Landroid/content/Context;Ljava/lang/String;)I

    .line 1839
    :cond_8
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-static {v8, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->insertPhotosInfo(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)I

    .line 1843
    .end local v1    # "cityId":Ljava/lang/String;
    .end local v6    # "resultCode":I
    :cond_9
    const-string v8, ""

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "LC : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " -> "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1844
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateLastSelectedLocation(Landroid/content/Context;Ljava/lang/String;)I

    .line 1846
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-static {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->isMaxCityListAdded(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 1847
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .line 1848
    invoke-virtual {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0d0023

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const/16 v12, 0xa

    .line 1849
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    .line 1847
    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    new-instance v10, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46$1;

    invoke-direct {v10, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;)V

    invoke-static {v8, v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;Ljava/lang/String;Landroid/os/Handler;)V

    goto/16 :goto_0

    .line 1855
    :cond_a
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->finish()V

    goto/16 :goto_0

    .line 1860
    .end local v2    # "imsi":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .end local v3    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .end local v4    # "location":Ljava/lang/String;
    .end local v7    # "todayInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    :cond_b
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->hideLoadingDialog()V
    invoke-static {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$2300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    .line 1861
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    const v9, 0x7f0d001f

    invoke-static {v8, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    goto/16 :goto_0
.end method
