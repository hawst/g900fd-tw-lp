.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;
.super Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrderAdapter;
.source "MenuChangeOrdersList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "OrderItemAdapter"
.end annotation


# instance fields
.field arrWeatherInfo:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private mLayoutInflater:Landroid/view/LayoutInflater;

.field private mLayoutResId:I

.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;


# direct methods
.method public constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;Landroid/content/Context;ILjava/util/List;)V
    .locals 1
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "resourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1141
    .local p4, "objects":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;>;"
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    .line 1142
    invoke-direct {p0, p2, p3, p4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrderAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 1144
    iput p3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->mLayoutResId:I

    .line 1145
    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 1147
    return-void
.end method


# virtual methods
.method public allowDrag(I)Z
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 1201
    const/4 v0, 0x1

    return v0
.end method

.method public allowDrop(II)Z
    .locals 1
    .param p1, "arg0"    # I
    .param p2, "arg1"    # I

    .prologue
    .line 1210
    const/4 v0, 0x1

    return v0
.end method

.method public clearTextEffect()V
    .locals 2

    .prologue
    .line 1259
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->arrWeatherInfo:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 1260
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->arrWeatherInfo:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 1261
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->arrWeatherInfo:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1262
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->arrWeatherInfo:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->clearAllTextEffect()V

    .line 1260
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1266
    .end local v0    # "i":I
    :cond_1
    return-void
.end method

.method public dropDone(II)V
    .locals 6
    .param p1, "startPos"    # I
    .param p2, "destPos"    # I

    .prologue
    const/4 v1, 0x0

    .line 1292
    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;

    .line 1293
    .local v0, "draggedObj":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;
    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->setNotifyOnChange(Z)V

    .line 1294
    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->remove(Ljava/lang/Object;)V

    .line 1295
    invoke-virtual {p0, v0, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->insert(Ljava/lang/Object;I)V

    .line 1296
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->notifyDataSetChanged()V

    .line 1306
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "done ok?? "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mOrgItemList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)Ljava/util/ArrayList;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mWorkingArrayList:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v1, 0x1

    :cond_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1313
    return-void
.end method

.method public getArrayList(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;)V
    .locals 2
    .param p1, "vh"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;

    .prologue
    .line 1246
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->arrWeatherInfo:Ljava/util/ArrayList;

    .line 1248
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->arrWeatherInfo:Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;->tv_name:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1249
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->arrWeatherInfo:Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;->tv_currentTemp:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1250
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->arrWeatherInfo:Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;->tv_highTemp:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1251
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->arrWeatherInfo:Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;->tv_lowTemp:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1252
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->arrWeatherInfo:Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;->tv_state_name:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1253
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->arrWeatherInfo:Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;->tv_time:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1254
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->arrWeatherInfo:Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;->tv_time_ampm:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1255
    return-void
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x0

    .line 1154
    const/4 v2, 0x0

    .line 1155
    .local v2, "viewHolder":Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;
    move-object v1, p2

    .line 1157
    .local v1, "v":Landroid/view/View;
    if-nez v1, :cond_2

    .line 1158
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->mLayoutResId:I

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1160
    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;

    .end local v2    # "viewHolder":Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;
    invoke-direct {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;-><init>()V

    .line 1161
    .restart local v2    # "viewHolder":Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;
    const v3, 0x7f080072

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;->tv_contentsLayout:Landroid/widget/RelativeLayout;

    .line 1162
    const v3, 0x7f080070

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/FrameLayout;

    iput-object v3, v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;->tv_selector:Landroid/widget/FrameLayout;

    .line 1163
    const v3, 0x7f080071

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/FrameLayout;

    iput-object v3, v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;->tv_stroke:Landroid/widget/FrameLayout;

    .line 1164
    const v3, 0x7f080076

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;->tv_name:Landroid/widget/TextView;

    .line 1165
    const v3, 0x7f080077

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;->tv_state_name:Landroid/widget/TextView;

    .line 1166
    const v3, 0x7f080079

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;->tv_time:Landroid/widget/TextView;

    .line 1167
    const v3, 0x7f08007a

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;->tv_time_ampm:Landroid/widget/TextView;

    .line 1168
    const v3, 0x7f08007d

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;->tv_currentTemp:Landroid/widget/TextView;

    .line 1169
    const v3, 0x7f08002c

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;->tv_highTemp:Landroid/widget/TextView;

    .line 1170
    const v3, 0x7f08002d

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;->tv_lowTemp:Landroid/widget/TextView;

    .line 1171
    const v3, 0x7f08007c

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;->tv_weathericon:Landroid/widget/ImageView;

    .line 1172
    const v3, 0x7f08007e

    .line 1173
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;->tv_centorfahren:Landroid/widget/ImageView;

    .line 1174
    const v3, 0x7f080075

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;->tv_current:Landroid/widget/ImageView;

    .line 1175
    const v3, 0x7f080020

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;->tv_dst:Landroid/widget/ImageView;

    .line 1177
    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1182
    :goto_0
    if-eqz v1, :cond_0

    .line 1183
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->changeListViewRowLayoutParams(Landroid/view/View;)V
    invoke-static {v3, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->access$1100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;Landroid/view/View;)V

    .line 1186
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;

    .line 1188
    .local v0, "myCityData":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;
    if-eqz v0, :cond_1

    .line 1189
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->setMyCityData(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;Landroid/view/View;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;)V
    invoke-static {v3, v2, v5, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->access$1200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;Landroid/view/View;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;)V

    .line 1191
    :cond_1
    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->setStrokeAndShadow(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;)V

    .line 1192
    return-object v1

    .line 1179
    .end local v0    # "myCityData":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;
    :cond_2
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "viewHolder":Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;
    check-cast v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;

    .restart local v2    # "viewHolder":Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;
    goto :goto_0
.end method

.method public setStrokeAndShadow(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;)V
    .locals 10
    .param p1, "vh"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;

    .prologue
    .line 1215
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getSystemResolution(Landroid/content/Context;)I

    move-result v1

    .line 1217
    .local v1, "resolution":I
    const/high16 v9, 0x3f800000    # 1.0f

    .line 1218
    .local v9, "strokeSize":F
    const/high16 v7, -0x1000000

    .line 1219
    .local v7, "strokeColor":I
    const v8, 0x3ee66666    # 0.45f

    .line 1221
    .local v8, "strokeOffset":F
    const/high16 v2, 0x42b40000    # 90.0f

    .line 1222
    .local v2, "shadowAngle":F
    const/high16 v3, 0x3f800000    # 1.0f

    .line 1223
    .local v3, "shadowOffset":F
    const/high16 v4, 0x40800000    # 4.0f

    .line 1224
    .local v4, "shadowSoftness":F
    const/high16 v5, -0x1000000

    .line 1225
    .local v5, "shadowColor":I
    const v6, 0x3f4ccccd    # 0.8f

    .line 1227
    .local v6, "shadowOpacity":F
    const v0, 0x1fa400

    if-ne v1, v0, :cond_1

    .line 1228
    const/high16 v3, 0x40000000    # 2.0f

    .line 1229
    const/high16 v4, 0x40a00000    # 5.0f

    .line 1239
    :cond_0
    :goto_0
    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->getArrayList(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;)V

    .line 1240
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->clearTextEffect()V

    move-object v0, p0

    .line 1241
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->setTextShadow(IFFFIF)V

    .line 1242
    invoke-virtual {p0, v1, v9, v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->setTextStroke(IFIF)V

    .line 1243
    return-void

    .line 1230
    :cond_1
    const v0, 0xe1000

    if-ne v1, v0, :cond_2

    .line 1231
    const/high16 v3, 0x3f800000    # 1.0f

    goto :goto_0

    .line 1232
    :cond_2
    const v0, 0x7e900

    if-ne v1, v0, :cond_3

    .line 1233
    const/high16 v3, 0x3f800000    # 1.0f

    goto :goto_0

    .line 1234
    :cond_3
    const v0, 0x384000

    if-ne v1, v0, :cond_0

    .line 1235
    const/high16 v3, 0x40400000    # 3.0f

    .line 1236
    const/high16 v4, 0x40a00000    # 5.0f

    goto :goto_0
.end method

.method public setTextShadow(IFFFIF)V
    .locals 7
    .param p1, "resolution"    # I
    .param p2, "angle"    # F
    .param p3, "offset"    # F
    .param p4, "softness"    # F
    .param p5, "color"    # I
    .param p6, "blendingOpacity"    # F

    .prologue
    .line 1282
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->arrWeatherInfo:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 1283
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->arrWeatherInfo:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v6, v0, :cond_1

    .line 1284
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->arrWeatherInfo:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1285
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->arrWeatherInfo:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/widget/TextView;->addOuterShadowTextEffect(FFFIF)I

    .line 1283
    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 1289
    .end local v6    # "i":I
    :cond_1
    return-void
.end method

.method public setTextStroke(IFIF)V
    .locals 2
    .param p1, "resolution"    # I
    .param p2, "strokeSize"    # F
    .param p3, "strokeColor"    # I
    .param p4, "strokeOffset"    # F

    .prologue
    .line 1270
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->arrWeatherInfo:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 1271
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x4

    if-ge v0, v1, :cond_1

    .line 1272
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->arrWeatherInfo:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1273
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->arrWeatherInfo:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p2, p3, p4}, Landroid/widget/TextView;->addStrokeTextEffect(FIF)I

    .line 1271
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1277
    .end local v0    # "i":I
    :cond_1
    return-void
.end method

.method public setViewResourceId(I)V
    .locals 0
    .param p1, "resId"    # I

    .prologue
    .line 1150
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->mLayoutResId:I

    .line 1151
    return-void
.end method
