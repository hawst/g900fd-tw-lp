.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;
.super Landroid/view/View;
.source "IceIconView.java"

# interfaces
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;


# static fields
.field private static final ANI_TYPE_CIRCLE_ALPHA_0:I = 0x14

.field private static final ANI_TYPE_CIRCLE_ALPHA_1:I = 0x15

.field private static final ANI_TYPE_CIRCLE_ALPHA_2:I = 0x16

.field private static final ANI_TYPE_CIRCLE_ALPHA_3:I = 0x17

.field private static final ANI_TYPE_CIRCLE_ALPHA_4:I = 0x18

.field private static final ANI_TYPE_CIRCLE_ALPHA_5:I = 0x19

.field private static final ANI_TYPE_CIRCLE_TRANSLATE:I = 0x13

.field private static final ANI_TYPE_ICE_ROTATE:I = 0x12

.field private static final ANI_TYPE_THERMOMETER_SCALE_Y:I = 0x11


# instance fields
.field isStop:Z

.field l:Landroid/animation/Animator$AnimatorListener;

.field private mAnimators:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/animation/ValueAnimator;",
            ">;"
        }
    .end annotation
.end field

.field private mCenterIce:Landroid/graphics/Path;

.field private mIsActiveAnimationThread:Z

.field private mOutIce0:Landroid/graphics/Path;

.field private mOutIce1:Landroid/graphics/Path;

.field private mOutIce2:Landroid/graphics/Path;

.field private mOutIce3:Landroid/graphics/Path;

.field private mOutIce4:Landroid/graphics/Path;

.field private mOutIce5:Landroid/graphics/Path;

.field private mPaint:Landroid/graphics/Paint;

.field private mPaintColor:I

.field private mPath:Landroid/graphics/Path;

.field private mScale:F

.field rotateOffset:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 85
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 86
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 81
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 82
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 89
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mPaint:Landroid/graphics/Paint;

    .line 33
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mPath:Landroid/graphics/Path;

    .line 35
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    .line 37
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce0:Landroid/graphics/Path;

    .line 39
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce1:Landroid/graphics/Path;

    .line 41
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce2:Landroid/graphics/Path;

    .line 43
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce3:Landroid/graphics/Path;

    .line 45
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce4:Landroid/graphics/Path;

    .line 47
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce5:Landroid/graphics/Path;

    .line 49
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    .line 51
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mIsActiveAnimationThread:Z

    .line 72
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mAnimators:Ljava/util/HashMap;

    .line 74
    const v0, -0xa0a0b

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mPaintColor:I

    .line 388
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->rotateOffset:F

    .line 389
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->l:Landroid/animation/Animator$AnimatorListener;

    .line 534
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->isStop:Z

    .line 90
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->init()V

    .line 91
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mAnimators:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mIsActiveAnimationThread:Z

    return v0
.end method

.method private drawIce(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "path"    # Landroid/graphics/Path;
    .param p3, "paint"    # Landroid/graphics/Paint;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 260
    invoke-virtual {p2}, Landroid/graphics/Path;->reset()V

    .line 261
    sget-object v0, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {p2, v0}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 263
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mPaintColor:I

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 264
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 266
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    invoke-virtual {p1, v0, p3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 268
    return-void
.end method

.method private drawIceCircle(ILandroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V
    .locals 1
    .param p1, "position"    # I
    .param p2, "canvas"    # Landroid/graphics/Canvas;
    .param p3, "path"    # Landroid/graphics/Path;
    .param p4, "paint"    # Landroid/graphics/Paint;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 271
    packed-switch p1, :pswitch_data_0

    .line 292
    :goto_0
    return-void

    .line 273
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce0:Landroid/graphics/Path;

    invoke-virtual {p2, v0, p4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 276
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce1:Landroid/graphics/Path;

    invoke-virtual {p2, v0, p4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 279
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce2:Landroid/graphics/Path;

    invoke-virtual {p2, v0, p4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 282
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce3:Landroid/graphics/Path;

    invoke-virtual {p2, v0, p4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 285
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce4:Landroid/graphics/Path;

    invoke-virtual {p2, v0, p4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 288
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce5:Landroid/graphics/Path;

    invoke-virtual {p2, v0, p4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 271
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_5
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method private init()V
    .locals 2

    .prologue
    .line 94
    invoke-virtual {p0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 95
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mPaint:Landroid/graphics/Paint;

    .line 96
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mPath:Landroid/graphics/Path;

    .line 97
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->initPath()V

    .line 99
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mPaintColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 100
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 101
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 102
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40a00000    # 5.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 103
    return-void
.end method

.method private initPath()V
    .locals 13

    .prologue
    const v12, 0x425e3646

    const v11, 0x4307e6a8    # 135.901f

    const/high16 v10, 0x42d60000    # 107.0f

    const v9, 0x43184b44

    const v8, 0x43101893

    .line 106
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    .line 107
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x432b8f1b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4294e9fc    # 74.457f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 108
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x432ac49c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42922b85    # 73.085f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43294000    # 169.25f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42909fbe

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4327a9ba

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4290f5c3    # 72.48f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 110
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x43014000    # 129.25f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42995168

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 111
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x42e352f2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4225374c    # 41.304f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 112
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x42e20bc7

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x421f6979    # 39.853f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42df2c08

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x421bab02    # 38.917f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42dc0106    # 110.002f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x421bab02    # 38.917f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 114
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x42d8d581    # 108.417f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x421bab02    # 38.917f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42d5f646

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x421f6979    # 39.853f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42d4af1b

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42253646

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x42b58106

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42995168

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 117
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x42515917

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4290f5c3    # 72.48f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 118
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x424b0937

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4290a0c5

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4244ef9e

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42922b02    # 73.084f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4241c396

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4294e9fc    # 74.457f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 120
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x423e978d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4297a8f6    # 75.83f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x423ef6c9

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x429b14fe    # 77.541f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4242b53f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x429da354    # 78.819f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 122
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x428f0083    # 71.501f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x42dc0000    # 110.0f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 123
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x4242b646

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x430d2d91

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 124
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x423ef6c9

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x430e74bc

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x423e9893

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43102ac1

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4241c49c

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43118a3d    # 145.54f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x4244f0a4    # 49.235f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4312e9ba

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x424b051f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4313b062

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42515a1d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4313849c

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 128
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x42b58106

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x430f5687

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 129
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x42d4af1b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4332b1ec

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 130
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x42d5f6c9

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43342560

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42d8d581    # 108.417f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4335153f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42dc0106    # 110.002f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4335153f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 132
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x42df2c08

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4335153f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42e20bc7

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43342560

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42e352f2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4332b1ec

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 134
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x43014000    # 129.25f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x430f5687

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 135
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x4327a9ba

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4313849c

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 136
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x43293eb8

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4313b021

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x432ac45a

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4312e9fc

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x432b8f1b

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43118a3d    # 145.54f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 138
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x432c5a1d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43102ac1

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x432c424e

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x430e74bc

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x432b52b0

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x430d2d91

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 140
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x43147fbe

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x42dc0000    # 110.0f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 141
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x432b51ec    # 171.32f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x429da354    # 78.819f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 142
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x432c424e

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x429b14fe    # 77.541f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x432c5a1d

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4297a873

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x432b8f1b

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4294e9fc    # 74.457f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 144
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 145
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x42dc0083    # 110.001f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x425353f8    # 52.832f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 146
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x42f637cf

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a52148

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 147
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x42f687ae    # 123.265f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a5d604    # 82.918f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42f6f2b0    # 123.474f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42a6774c

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42f76d0e

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42a708b4

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 149
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x42e1828f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x42cd0000    # 102.5f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 150
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x42c58ccd

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x429c93f8

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 151
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x42dc0083    # 110.001f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x425353f8    # 52.832f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 152
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 153
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x4271f6c9

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a3547b    # 81.665f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 154
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x42b99810

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42aa1ba6    # 85.054f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 155
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x42b9fbe7

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42aa2666

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42ba5fbe

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42aa87ae    # 85.265f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42bac28f    # 93.38f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42aa8396

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 157
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x42d3a666

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v10

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 158
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x429db4bc

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v10

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 159
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x4271f6c9

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a3547b    # 81.665f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 160
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 161
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x4271f6c9

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x430ad4fe    # 138.832f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 162
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x429db4bc

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x42e40000    # 114.0f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 163
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x42d4ced9    # 106.404f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x42e40000    # 114.0f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 164
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x42bbd99a

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43075c6a

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 165
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x42bb6560

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4307526f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42baef1b

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43072b85    # 135.17f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42ba76c9

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43072b85    # 135.17f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 167
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x42ba2d0e

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43072b85    # 135.17f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42b9e2d1    # 92.943f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43073d71    # 135.24f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42b99810

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43074189

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 169
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x4271f6c9

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x430ad4fe    # 138.832f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 170
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 171
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x42dc0083    # 110.001f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43272b02    # 167.168f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 172
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x42c50d50    # 98.526f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x430d251f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 173
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x42e05a1d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x42eb0000    # 117.5f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 174
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x42f6c625

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4308eb02    # 136.918f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 175
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x42f69168

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x430914bc

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42f66148    # 123.19f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43094042

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42f63852    # 123.11f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43096e98

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 177
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x42dc0083    # 110.001f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43272b02    # 167.168f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 178
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 179
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x431f824e

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x430ad4fe    # 138.832f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 180
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x4303d99a    # 131.85f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4307f2f2

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 181
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x42ee353f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x42e40000    # 114.0f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 182
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x430d25a2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x42e40000    # 114.0f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 183
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x431f824e

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x430ad4fe    # 138.832f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 184
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 185
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x430d2560

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v10

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 186
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x42ed0dd3    # 118.527f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v10

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 187
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x43033c6a

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a97ae1    # 84.74f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 188
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x431f81cb

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a3947b    # 81.79f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 189
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    const v1, 0x430d2560

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v10

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 190
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 192
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce0:Landroid/graphics/Path;

    .line 193
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce0:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v8

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v12

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 194
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce0:Landroid/graphics/Path;

    const v1, 0x43149df4

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v12

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v9

    const v4, 0x424f7efa    # 51.874f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v9

    const v6, 0x423d6979    # 47.353f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 196
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce0:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v9

    const v2, 0x422b54fe    # 42.833f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43149db2

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x421c9fbe

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v8

    const v6, 0x421c9fbe

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 198
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce0:Landroid/graphics/Path;

    const v1, 0x430b9375

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x421c9fbe

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v11

    const v4, 0x422b5604    # 42.834f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v11

    const v6, 0x423d6979    # 47.353f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 200
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce0:Landroid/graphics/Path;

    const v1, 0x4307e5e3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x424f7efa    # 51.874f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x430b9375

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v12

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v8

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v12

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 203
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce1:Landroid/graphics/Path;

    .line 204
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce1:Landroid/graphics/Path;

    const v1, 0x43374ac1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42cbb53f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 205
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce1:Landroid/graphics/Path;

    const v1, 0x4332c560

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42cbb53f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x432f1893

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42d31062

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x432f1893

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42dc1a1d

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 207
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce1:Landroid/graphics/Path;

    const v1, 0x432f1893

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42e524dd

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4332c625

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42ec8083    # 118.251f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43374ac1

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42ec8083    # 118.251f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 209
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce1:Landroid/graphics/Path;

    const v1, 0x433bd021

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42ec8083    # 118.251f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x433f7db2

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42e524dd

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x433f7db2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42dc1a1d

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 211
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce1:Landroid/graphics/Path;

    const v1, 0x433f7db2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42d31062

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x433bd021

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42cbb53f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43374ac1

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42cbb53f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 214
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce2:Landroid/graphics/Path;

    .line 215
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce2:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v8

    const v2, 0x432472f2

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 216
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce2:Landroid/graphics/Path;

    const v1, 0x430b9375

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432472f2

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v11

    const v4, 0x43282083

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v11

    const v6, 0x432ca560

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 218
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce2:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v11

    const v2, 0x43312ac1

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x430b93f8

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4334d7cf

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v8

    const v6, 0x4334d7cf

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 220
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce2:Landroid/graphics/Path;

    const v1, 0x43149df4

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4334d7cf

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v9

    const v4, 0x43312a3d

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v9

    const v6, 0x432ca560

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 222
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce2:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v9

    const v2, 0x43282083

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43149db2

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x432472f2

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v8

    const v6, 0x432472f2

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 225
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce3:Landroid/graphics/Path;

    .line 226
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce3:Landroid/graphics/Path;

    const v1, 0x4290f0a4    # 72.47f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43245aa0

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 227
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce3:Landroid/graphics/Path;

    const v1, 0x4287e666    # 67.95f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43245aa0

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42808bc7

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43280831

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42808bc7

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x432c8c8b

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 229
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce3:Landroid/graphics/Path;

    const v1, 0x42808bc7

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x433111ec    # 177.07f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4287e6e9

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4334c000    # 180.75f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4290f0a4    # 72.47f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4334c000    # 180.75f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 231
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce3:Landroid/graphics/Path;

    const v1, 0x4299fb64

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4334c000    # 180.75f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42a15687

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x433111ec    # 177.07f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42a15687

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x432c8c8b

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 233
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce3:Landroid/graphics/Path;

    const v1, 0x42a15687

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4328076d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4299fb64

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43245aa0

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4290f0a4    # 72.47f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43245aa0

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 236
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce4:Landroid/graphics/Path;

    .line 237
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce4:Landroid/graphics/Path;

    const v1, 0x4212d2f2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42cbe3d7    # 101.945f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 238
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce4:Landroid/graphics/Path;

    const v1, 0x4200be77    # 32.186f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42cbe3d7    # 101.945f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x41e4126f    # 28.509f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42d33e77

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x41e4126f    # 28.509f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42dc47ae    # 110.14f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 240
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce4:Landroid/graphics/Path;

    const v1, 0x41e4126f    # 28.509f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42e5526f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4200bf7d    # 32.187f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42ecae14    # 118.34f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4212d2f2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42ecae14    # 118.34f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 242
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce4:Landroid/graphics/Path;

    const v1, 0x4224e873

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42ecae14    # 118.34f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42339eb8

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42e5526f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42339eb8

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42dc47ae    # 110.14f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 244
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce4:Landroid/graphics/Path;

    const v1, 0x42339eb8

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42d33e77

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4224e979    # 41.228f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42cbe3d7    # 101.945f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4212d2f2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42cbe3d7    # 101.945f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 247
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce5:Landroid/graphics/Path;

    .line 248
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce5:Landroid/graphics/Path;

    const v1, 0x42908d50    # 72.276f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x425ef9db    # 55.744f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 249
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce5:Landroid/graphics/Path;

    const v1, 0x42999893

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x425ef9db    # 55.744f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42a0f439

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4250428f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42a0f439

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x423e2c08    # 47.543f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 251
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce5:Landroid/graphics/Path;

    const v1, 0x42a0f439

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x422c178d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42999893

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x421d624e    # 39.346f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42908d50    # 72.276f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x421d624e    # 39.346f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 253
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce5:Landroid/graphics/Path;

    const v1, 0x42878396

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x421d624e    # 39.346f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42802979

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x422c1893

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42802979

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x423e2c08    # 47.543f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 255
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce5:Landroid/graphics/Path;

    const v1, 0x42802979

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4250428f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42878419

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x425ef9db    # 55.744f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42908d50    # 72.276f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x425ef9db    # 55.744f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 257
    return-void
.end method


# virtual methods
.method public cancelAnimation()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 519
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mIsActiveAnimationThread:Z

    .line 520
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mAnimators:Ljava/util/HashMap;

    if-eqz v3, :cond_1

    .line 521
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 522
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 523
    .local v2, "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 524
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 525
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    .line 526
    .local v0, "ani":Landroid/animation/ValueAnimator;
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 527
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    goto :goto_0

    .line 532
    .end local v0    # "ani":Landroid/animation/ValueAnimator;
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    .end local v2    # "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_1
    return-void
.end method

.method public getScale()F
    .locals 1

    .prologue
    .line 563
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 386
    return-object p0
.end method

.method public isRunning()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 540
    const/4 v3, 0x0

    .line 541
    .local v3, "ret":Z
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mAnimators:Ljava/util/HashMap;

    if-eqz v4, :cond_1

    .line 542
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 543
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 544
    .local v2, "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 545
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 546
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    .line 547
    .local v0, "ani":Landroid/animation/ValueAnimator;
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 548
    const/4 v3, 0x1

    .line 554
    .end local v0    # "ani":Landroid/animation/ValueAnimator;
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    .end local v2    # "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_1
    return v3
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 17
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 296
    :try_start_0
    invoke-super/range {p0 .. p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 298
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    const/4 v14, 0x0

    cmpg-float v13, v13, v14

    if-gtz v13, :cond_1

    .line 299
    const-string v13, ""

    const-string v14, "scale is less then 0"

    invoke-static {v13, v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    :cond_0
    :goto_0
    return-void

    .line 303
    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mAnimators:Ljava/util/HashMap;

    const/16 v14, 0x11

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/animation/ValueAnimator;

    .line 304
    .local v5, "ani":Landroid/animation/ValueAnimator;
    const/high16 v11, 0x3f800000    # 1.0f

    .line 305
    .local v11, "mThermometerScaleY":F
    if-eqz v5, :cond_5

    .line 306
    invoke-virtual {v5}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Float;

    invoke-virtual {v13}, Ljava/lang/Float;->floatValue()F

    move-result v11

    .line 311
    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mAnimators:Ljava/util/HashMap;

    const/16 v14, 0x12

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "ani":Landroid/animation/ValueAnimator;
    check-cast v5, Landroid/animation/ValueAnimator;

    .line 312
    .restart local v5    # "ani":Landroid/animation/ValueAnimator;
    const/4 v4, 0x0

    .line 313
    .local v4, "angle":F
    if-eqz v5, :cond_2

    .line 314
    invoke-virtual {v5}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Float;

    invoke-virtual {v13}, Ljava/lang/Float;->floatValue()F

    move-result v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->rotateOffset:F

    add-float v4, v13, v14

    .line 317
    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mAnimators:Ljava/util/HashMap;

    const/16 v14, 0x13

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "ani":Landroid/animation/ValueAnimator;
    check-cast v5, Landroid/animation/ValueAnimator;

    .line 318
    .restart local v5    # "ani":Landroid/animation/ValueAnimator;
    const/4 v6, 0x0

    .line 319
    .local v6, "circleTranslate":F
    if-eqz v5, :cond_3

    .line 320
    invoke-virtual {v5}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Float;

    invoke-virtual {v13}, Ljava/lang/Float;->floatValue()F

    move-result v6

    .line 328
    :cond_3
    const/4 v13, 0x6

    new-array v7, v13, [[F

    const/4 v13, 0x0

    const/4 v14, 0x2

    new-array v14, v14, [F

    const/4 v15, 0x0

    aput v6, v14, v15

    const/4 v15, 0x1

    aput v6, v14, v15

    aput-object v14, v7, v13

    const/4 v13, 0x1

    const/4 v14, 0x2

    new-array v14, v14, [F

    const/4 v15, 0x0

    aput v6, v14, v15

    const/4 v15, 0x1

    const/16 v16, 0x0

    aput v16, v14, v15

    aput-object v14, v7, v13

    const/4 v13, 0x2

    const/4 v14, 0x2

    new-array v14, v14, [F

    const/4 v15, 0x0

    aput v6, v14, v15

    const/4 v15, 0x1

    neg-float v0, v6

    move/from16 v16, v0

    aput v16, v14, v15

    aput-object v14, v7, v13

    const/4 v13, 0x3

    const/4 v14, 0x2

    new-array v14, v14, [F

    const/4 v15, 0x0

    neg-float v0, v6

    move/from16 v16, v0

    aput v16, v14, v15

    const/4 v15, 0x1

    neg-float v0, v6

    move/from16 v16, v0

    aput v16, v14, v15

    aput-object v14, v7, v13

    const/4 v13, 0x4

    const/4 v14, 0x2

    new-array v14, v14, [F

    const/4 v15, 0x0

    neg-float v0, v6

    move/from16 v16, v0

    aput v16, v14, v15

    const/4 v15, 0x1

    const/16 v16, 0x0

    aput v16, v14, v15

    aput-object v14, v7, v13

    const/4 v13, 0x5

    const/4 v14, 0x2

    new-array v14, v14, [F

    const/4 v15, 0x0

    neg-float v0, v6

    move/from16 v16, v0

    aput v16, v14, v15

    const/4 v15, 0x1

    aput v6, v14, v15

    aput-object v14, v7, v13

    .line 344
    .local v7, "circleTranslatePos":[[F
    const/4 v13, 0x6

    new-array v3, v13, [I

    fill-array-data v3, :array_0

    .line 349
    .local v3, "alphaAniKeys":[I
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_2
    const/4 v13, 0x6

    if-ge v9, v13, :cond_6

    .line 350
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v12

    .line 351
    .local v12, "saveLavel":I
    const/high16 v13, 0x42dc0000    # 110.0f

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v13, v14

    const/high16 v14, 0x42dc0000    # 110.0f

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v14, v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v13, v14}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 352
    aget-object v13, v7, v9

    const/4 v14, 0x0

    aget v13, v13, v14

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v13, v14

    aget-object v14, v7, v9

    const/4 v15, 0x1

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v14, v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Landroid/graphics/Canvas;->translate(FF)V

    .line 353
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mPaintColor:I

    invoke-virtual {v13, v14}, Landroid/graphics/Paint;->setColor(I)V

    .line 354
    const/16 v2, 0xff

    .line 355
    .local v2, "alpha":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mAnimators:Ljava/util/HashMap;

    aget v14, v3, v9

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "ani":Landroid/animation/ValueAnimator;
    check-cast v5, Landroid/animation/ValueAnimator;

    .line 356
    .restart local v5    # "ani":Landroid/animation/ValueAnimator;
    if-eqz v5, :cond_4

    .line 357
    invoke-virtual {v5}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 359
    :cond_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v13, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 360
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mPaint:Landroid/graphics/Paint;

    sget-object v14, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v13, v14}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 361
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v9, v1, v13, v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->drawIceCircle(ILandroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 362
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 349
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_2

    .line 308
    .end local v2    # "alpha":I
    .end local v3    # "alphaAniKeys":[I
    .end local v4    # "angle":F
    .end local v6    # "circleTranslate":F
    .end local v7    # "circleTranslatePos":[[F
    .end local v9    # "i":I
    .end local v12    # "saveLavel":I
    :cond_5
    const/high16 v11, 0x3f800000    # 1.0f

    goto/16 :goto_1

    .line 365
    .restart local v3    # "alphaAniKeys":[I
    .restart local v4    # "angle":F
    .restart local v6    # "circleTranslate":F
    .restart local v7    # "circleTranslatePos":[[F
    .restart local v9    # "i":I
    :cond_6
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v10

    .line 367
    .local v10, "iceSaveLavel":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mPaint:Landroid/graphics/Paint;

    const/16 v14, 0xff

    invoke-virtual {v13, v14}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 368
    const/high16 v13, 0x42dc0000    # 110.0f

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v13, v14

    const/high16 v14, 0x42dc0000    # 110.0f

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v14, v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v13, v14}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 369
    const/high16 v13, 0x42dc0000    # 110.0f

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v13, v14

    const/high16 v14, 0x42dc0000    # 110.0f

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v14, v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v11, v13, v14}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 370
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v13, v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->drawIce(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 373
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 375
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->isRunning()Z

    move-result v13

    if-nez v13, :cond_0

    .line 376
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->cancelAnimation()V

    .line 377
    const-string v13, ""

    const-string v14, "cancelA"

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 380
    .end local v3    # "alphaAniKeys":[I
    .end local v4    # "angle":F
    .end local v5    # "ani":Landroid/animation/ValueAnimator;
    .end local v6    # "circleTranslate":F
    .end local v7    # "circleTranslatePos":[[F
    .end local v9    # "i":I
    .end local v10    # "iceSaveLavel":I
    .end local v11    # "mThermometerScaleY":F
    :catch_0
    move-exception v8

    .line 381
    .local v8, "e":Ljava/lang/Exception;
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 344
    :array_0
    .array-data 4
        0x16
        0x15
        0x14
        0x19
        0x18
        0x17
    .end array-data
.end method

.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 602
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x0

    .line 571
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onViewDetachedFromWindow : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 572
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mAnimators:Ljava/util/HashMap;

    if-eqz v3, :cond_2

    .line 573
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 574
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 575
    .local v2, "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 576
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 577
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    .line 578
    .local v0, "ani":Landroid/animation/ValueAnimator;
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 579
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 580
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    goto :goto_0

    .line 583
    .end local v0    # "ani":Landroid/animation/ValueAnimator;
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    :cond_1
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    .line 587
    .end local v2    # "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_2
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mAnimators:Ljava/util/HashMap;

    .line 588
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mPaint:Landroid/graphics/Paint;

    .line 589
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mPath:Landroid/graphics/Path;

    .line 590
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mCenterIce:Landroid/graphics/Path;

    .line 591
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce0:Landroid/graphics/Path;

    .line 592
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce1:Landroid/graphics/Path;

    .line 593
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce2:Landroid/graphics/Path;

    .line 594
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce3:Landroid/graphics/Path;

    .line 595
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce4:Landroid/graphics/Path;

    .line 596
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mOutIce5:Landroid/graphics/Path;

    .line 598
    return-void
.end method

.method public setPaintColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 77
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mPaintColor:I

    .line 78
    return-void
.end method

.method public setScale(F)V
    .locals 0
    .param p1, "scale"    # F

    .prologue
    .line 558
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    .line 559
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->init()V

    .line 560
    return-void
.end method

.method public setWidthAndHeight(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 567
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, p1, p2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 568
    .local v0, "l":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 569
    return-void
.end method

.method public startAnimation()V
    .locals 14

    .prologue
    .line 445
    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->isStop:Z

    .line 446
    const/4 v10, 0x0

    iput v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->rotateOffset:F

    .line 447
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v10}, Ljava/util/HashMap;->size()I

    move-result v10

    if-lez v10, :cond_2

    .line 448
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v10}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 449
    .local v5, "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 450
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 451
    .local v4, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/animation/ValueAnimator;

    .line 452
    .local v2, "ani":Landroid/animation/ValueAnimator;
    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 453
    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->end()V

    goto :goto_0

    .line 457
    .end local v2    # "ani":Landroid/animation/ValueAnimator;
    .end local v4    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    :cond_1
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v10}, Ljava/util/HashMap;->clear()V

    .line 460
    .end local v5    # "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_2
    const/4 v10, 0x5

    new-array v10, v10, [F

    fill-array-data v10, :array_0

    invoke-static {v10}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v9

    .line 461
    .local v9, "thermometerScaleYAni":Landroid/animation/ValueAnimator;
    const/4 v10, -0x1

    invoke-virtual {v9, v10}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 462
    const-wide/16 v10, 0x9c4

    invoke-virtual {v9, v10, v11}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 463
    new-instance v10, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v10}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v9, v10}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 464
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v9, v10}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 465
    invoke-virtual {v9}, Landroid/animation/ValueAnimator;->start()V

    .line 466
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mAnimators:Ljava/util/HashMap;

    const/16 v11, 0x11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 468
    const/4 v10, 0x2

    new-array v10, v10, [F

    fill-array-data v10, :array_1

    invoke-static {v10}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v7

    .line 469
    .local v7, "iceRotateAni":Landroid/animation/ValueAnimator;
    const/4 v10, -0x1

    invoke-virtual {v7, v10}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 470
    const-wide/16 v10, 0x9c4

    invoke-virtual {v7, v10, v11}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 471
    new-instance v10, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v10}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v7, v10}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 472
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v7, v10}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 473
    invoke-virtual {v7}, Landroid/animation/ValueAnimator;->start()V

    .line 474
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mAnimators:Ljava/util/HashMap;

    const/16 v11, 0x12

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 476
    const/4 v10, 0x5

    new-array v10, v10, [F

    const/4 v11, 0x0

    const/4 v12, 0x0

    aput v12, v10, v11

    const/4 v11, 0x1

    const/high16 v12, -0x3fc00000    # -3.0f

    iget v13, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v12, v13

    aput v12, v10, v11

    const/4 v11, 0x2

    const/4 v12, 0x0

    aput v12, v10, v11

    const/4 v11, 0x3

    const/high16 v12, 0x40a00000    # 5.0f

    iget v13, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mScale:F

    mul-float/2addr v12, v13

    aput v12, v10, v11

    const/4 v11, 0x4

    const/4 v12, 0x0

    aput v12, v10, v11

    invoke-static {v10}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v3

    .line 477
    .local v3, "circleTranslateAni":Landroid/animation/ValueAnimator;
    const/4 v10, -0x1

    invoke-virtual {v3, v10}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 478
    new-instance v10, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v10}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v3, v10}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 479
    const-wide/16 v10, 0x7d0

    invoke-virtual {v3, v10, v11}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 480
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v3, v10}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 481
    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->start()V

    .line 482
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mAnimators:Ljava/util/HashMap;

    const/16 v11, 0x13

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 484
    const/4 v0, 0x0

    .line 485
    .local v0, "alphaAni":Landroid/animation/ValueAnimator;
    const/4 v10, 0x6

    new-array v1, v10, [I

    fill-array-data v1, :array_2

    .line 489
    .local v1, "alphaAniKeys":[I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    const/4 v10, 0x6

    if-ge v6, v10, :cond_3

    .line 490
    const/4 v10, 0x2

    new-array v10, v10, [I

    fill-array-data v10, :array_3

    invoke-static {v10}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 491
    const/4 v10, -0x1

    invoke-virtual {v0, v10}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 492
    const/4 v10, 0x2

    invoke-virtual {v0, v10}, Landroid/animation/ValueAnimator;->setRepeatMode(I)V

    .line 493
    mul-int/lit8 v10, v6, 0x64

    add-int/lit16 v10, v10, 0x3e8

    int-to-long v10, v10

    invoke-virtual {v0, v10, v11}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 494
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mAnimators:Ljava/util/HashMap;

    aget v11, v1, v6

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 495
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v0, v10}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 496
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 489
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 499
    :cond_3
    const/4 v10, 0x1

    iput-boolean v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->mIsActiveAnimationThread:Z

    .line 500
    new-instance v8, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView$2;

    invoke-direct {v8, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;)V

    .line 514
    .local v8, "t":Ljava/lang/Thread;
    const/16 v10, 0xa

    invoke-virtual {v8, v10}, Ljava/lang/Thread;->setPriority(I)V

    .line 515
    invoke-virtual {v8}, Ljava/lang/Thread;->start()V

    .line 516
    return-void

    .line 460
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3f59999a    # 0.85f
        0x3f800000    # 1.0f
        0x3f8ccccd    # 1.1f
        0x3f800000    # 1.0f
    .end array-data

    .line 468
    :array_1
    .array-data 4
        0x0
        0x42700000    # 60.0f
    .end array-data

    .line 485
    :array_2
    .array-data 4
        0x14
        0x19
        0x17
        0x18
        0x15
        0x16
    .end array-data

    .line 490
    :array_3
    .array-data 4
        0x0
        0xff
    .end array-data
.end method

.method public stopAnimation()V
    .locals 1

    .prologue
    .line 536
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/IceIconView;->isStop:Z

    .line 537
    return-void
.end method
