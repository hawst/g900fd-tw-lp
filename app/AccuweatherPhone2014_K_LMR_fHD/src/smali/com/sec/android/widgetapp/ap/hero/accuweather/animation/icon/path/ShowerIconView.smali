.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;
.super Landroid/view/View;
.source "ShowerIconView.java"

# interfaces
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;


# instance fields
.field private CloudTranslate:Landroid/animation/ValueAnimator;

.field isStop:Z

.field l:Landroid/animation/Animator$AnimatorListener;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mCloudLeft:Landroid/graphics/Path;

.field private mCloudRight:Landroid/graphics/Path;

.field private mCloudTop:Landroid/graphics/Path;

.field private mDrop1:Landroid/animation/ValueAnimator;

.field private mDrop2:Landroid/animation/ValueAnimator;

.field private mDrop3:Landroid/animation/ValueAnimator;

.field private mDrop4:Landroid/animation/ValueAnimator;

.field private mDrop5:Landroid/animation/ValueAnimator;

.field private mIconTranslate:Landroid/animation/ValueAnimator;

.field private mIsActiveAnimationThread:Z

.field private mMasking:Landroid/graphics/Path;

.field private mPaint:Landroid/graphics/Paint;

.field private mPaintColor:I

.field public mPreFixProperty1:Ljava/lang/String;

.field public mPreFixProperty2:Ljava/lang/String;

.field public mPreFixProperty3:Ljava/lang/String;

.field public mPreFixProperty4:Ljava/lang/String;

.field public mPreFixProperty5:Ljava/lang/String;

.field private mRainLine1:Landroid/graphics/Path;

.field private mRainLine2:Landroid/graphics/Path;

.field private mRainLine3:Landroid/graphics/Path;

.field private mRainLine4:Landroid/graphics/Path;

.field private mRainLine5:Landroid/graphics/Path;

.field private mScale:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 88
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 89
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 84
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 85
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 79
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    .line 29
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mIconTranslate:Landroid/animation/ValueAnimator;

    .line 32
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop1:Landroid/animation/ValueAnimator;

    .line 34
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop2:Landroid/animation/ValueAnimator;

    .line 36
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop3:Landroid/animation/ValueAnimator;

    .line 38
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop4:Landroid/animation/ValueAnimator;

    .line 40
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop5:Landroid/animation/ValueAnimator;

    .line 43
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mIsActiveAnimationThread:Z

    .line 45
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPaint:Landroid/graphics/Paint;

    .line 47
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mMasking:Landroid/graphics/Path;

    .line 49
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudLeft:Landroid/graphics/Path;

    .line 51
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudRight:Landroid/graphics/Path;

    .line 53
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudTop:Landroid/graphics/Path;

    .line 55
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine1:Landroid/graphics/Path;

    .line 57
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine2:Landroid/graphics/Path;

    .line 59
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine3:Landroid/graphics/Path;

    .line 61
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine4:Landroid/graphics/Path;

    .line 63
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine5:Landroid/graphics/Path;

    .line 65
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    .line 68
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mBitmap:Landroid/graphics/Bitmap;

    .line 72
    const v0, -0xa0a0b

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPaintColor:I

    .line 567
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPreFixProperty1:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPreFixProperty2:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPreFixProperty3:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPreFixProperty4:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPreFixProperty5:Ljava/lang/String;

    .line 573
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->l:Landroid/animation/Animator$AnimatorListener;

    .line 754
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->isStop:Z

    .line 80
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->init()V

    .line 81
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop1:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop2:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop3:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop4:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop5:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mIconTranslate:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mIsActiveAnimationThread:Z

    return v0
.end method

.method private drawCloud(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "paint"    # Landroid/graphics/Paint;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 379
    const/4 v0, 0x0

    .line 380
    .local v0, "dx":F
    iget-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mIsActiveAnimationThread:Z

    if-eqz v1, :cond_0

    .line 381
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    neg-float v0, v1

    .line 383
    :cond_0
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPaintColor:I

    invoke-virtual {p2, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 385
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 386
    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 387
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudLeft:Landroid/graphics/Path;

    invoke-virtual {p1, v1, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 388
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 390
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 391
    invoke-virtual {p1, v2, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 392
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudTop:Landroid/graphics/Path;

    invoke-virtual {p1, v1, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 393
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 395
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 396
    neg-float v1, v0

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 397
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudRight:Landroid/graphics/Path;

    invoke-virtual {p1, v1, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 398
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 399
    return-void
.end method

.method private drawDrop(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/high16 v13, -0x3de00000    # -40.0f

    .line 452
    const/4 v5, 0x0

    .line 453
    .local v5, "dy1":F
    const/4 v6, 0x0

    .line 454
    .local v6, "dy2":F
    const/4 v7, 0x0

    .line 455
    .local v7, "dy3":F
    const/4 v8, 0x0

    .line 456
    .local v8, "dy4":F
    const/4 v9, 0x0

    .line 457
    .local v9, "dy5":F
    const/4 v0, 0x0

    .line 458
    .local v0, "dx1":F
    const/4 v1, 0x0

    .line 459
    .local v1, "dx2":F
    const/4 v2, 0x0

    .line 460
    .local v2, "dx3":F
    const/4 v3, 0x0

    .line 461
    .local v3, "dx4":F
    const/4 v4, 0x0

    .line 463
    .local v4, "dx5":F
    iget-boolean v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mIsActiveAnimationThread:Z

    if-eqz v10, :cond_6

    .line 464
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop1:Landroid/animation/ValueAnimator;

    invoke-virtual {v10}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v10

    if-eqz v10, :cond_7

    .line 465
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop1:Landroid/animation/ValueAnimator;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPreFixProperty1:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "y"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Float;

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v5

    .line 466
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop1:Landroid/animation/ValueAnimator;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPreFixProperty1:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "x"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Float;

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 473
    :cond_0
    :goto_0
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop2:Landroid/animation/ValueAnimator;

    invoke-virtual {v10}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v10

    if-eqz v10, :cond_8

    .line 474
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop2:Landroid/animation/ValueAnimator;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPreFixProperty2:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "y"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Float;

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v6

    .line 475
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop2:Landroid/animation/ValueAnimator;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPreFixProperty2:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "x"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Float;

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 481
    :cond_1
    :goto_1
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop3:Landroid/animation/ValueAnimator;

    invoke-virtual {v10}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v10

    if-eqz v10, :cond_9

    .line 482
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop3:Landroid/animation/ValueAnimator;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPreFixProperty3:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "y"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Float;

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v7

    .line 483
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop3:Landroid/animation/ValueAnimator;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPreFixProperty3:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "x"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Float;

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v2

    .line 490
    :cond_2
    :goto_2
    iget-boolean v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->isStop:Z

    if-nez v10, :cond_3

    .line 491
    const/high16 v10, 0x42340000    # 45.0f

    iget v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float v3, v10, v11

    .line 493
    :cond_3
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop4:Landroid/animation/ValueAnimator;

    invoke-virtual {v10}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v10

    if-eqz v10, :cond_a

    .line 494
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop4:Landroid/animation/ValueAnimator;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPreFixProperty4:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "y"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Float;

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v8

    .line 495
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop4:Landroid/animation/ValueAnimator;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPreFixProperty4:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "x"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Float;

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v3

    .line 502
    :cond_4
    :goto_3
    iget-boolean v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->isStop:Z

    if-nez v10, :cond_5

    .line 503
    const/high16 v10, 0x41c80000    # 25.0f

    iget v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float v4, v10, v11

    .line 505
    :cond_5
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop5:Landroid/animation/ValueAnimator;

    invoke-virtual {v10}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v10

    if-eqz v10, :cond_b

    .line 506
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop5:Landroid/animation/ValueAnimator;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPreFixProperty5:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "y"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Float;

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v9

    .line 507
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop5:Landroid/animation/ValueAnimator;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPreFixProperty5:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "x"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Float;

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v4

    .line 516
    :cond_6
    :goto_4
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 517
    invoke-virtual {p1, v0, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 518
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine1:Landroid/graphics/Path;

    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v10, v11}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 519
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 521
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 522
    invoke-virtual {p1, v1, v6}, Landroid/graphics/Canvas;->translate(FF)V

    .line 523
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine2:Landroid/graphics/Path;

    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v10, v11}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 524
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 526
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 527
    invoke-virtual {p1, v2, v7}, Landroid/graphics/Canvas;->translate(FF)V

    .line 528
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine3:Landroid/graphics/Path;

    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v10, v11}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 529
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 531
    iget-boolean v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mIsActiveAnimationThread:Z

    if-eqz v10, :cond_d

    .line 532
    iget-boolean v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->isStop:Z

    if-eqz v10, :cond_c

    .line 533
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 534
    invoke-virtual {p1, v3, v8}, Landroid/graphics/Canvas;->translate(FF)V

    .line 535
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine4:Landroid/graphics/Path;

    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v10, v11}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 536
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 538
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 539
    invoke-virtual {p1, v4, v9}, Landroid/graphics/Canvas;->translate(FF)V

    .line 540
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine5:Landroid/graphics/Path;

    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v10, v11}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 541
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 566
    :goto_5
    return-void

    .line 468
    :cond_7
    iget-boolean v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->isStop:Z

    if-nez v10, :cond_0

    .line 469
    iget v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float v5, v13, v10

    goto/16 :goto_0

    .line 477
    :cond_8
    iget-boolean v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->isStop:Z

    if-nez v10, :cond_1

    .line 478
    iget v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float v6, v13, v10

    goto/16 :goto_1

    .line 485
    :cond_9
    iget-boolean v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->isStop:Z

    if-nez v10, :cond_2

    .line 486
    iget v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float v7, v13, v10

    goto/16 :goto_2

    .line 497
    :cond_a
    iget-boolean v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->isStop:Z

    if-nez v10, :cond_4

    .line 498
    iget v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float v8, v13, v10

    goto/16 :goto_3

    .line 509
    :cond_b
    iget-boolean v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->isStop:Z

    if-nez v10, :cond_6

    .line 510
    iget v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float v9, v13, v10

    goto/16 :goto_4

    .line 543
    :cond_c
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 544
    invoke-virtual {p1, v3, v8}, Landroid/graphics/Canvas;->translate(FF)V

    .line 545
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine2:Landroid/graphics/Path;

    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v10, v11}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 546
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 548
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 549
    invoke-virtual {p1, v4, v9}, Landroid/graphics/Canvas;->translate(FF)V

    .line 550
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine2:Landroid/graphics/Path;

    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v10, v11}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 551
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_5

    .line 555
    :cond_d
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 556
    invoke-virtual {p1, v3, v8}, Landroid/graphics/Canvas;->translate(FF)V

    .line 557
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine4:Landroid/graphics/Path;

    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v10, v11}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 558
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 560
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 561
    invoke-virtual {p1, v4, v9}, Landroid/graphics/Canvas;->translate(FF)V

    .line 562
    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine5:Landroid/graphics/Path;

    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v10, v11}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 563
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_5
.end method

.method private drawTransparentArea(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "paint"    # Landroid/graphics/Paint;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 373
    new-instance v0, Landroid/graphics/PorterDuffXfermode;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v1}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 374
    invoke-virtual {p1, p2, v2, v2, p3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 375
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 376
    return-void
.end method

.method private init()V
    .locals 4

    .prologue
    const/high16 v3, 0x43960000    # 300.0f

    .line 92
    invoke-virtual {p0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 93
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPaint:Landroid/graphics/Paint;

    .line 94
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->initPath()V

    .line 95
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPaintColor:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 96
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 97
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 98
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x40a00000    # 5.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 100
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mBitmap:Landroid/graphics/Bitmap;

    .line 102
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 103
    .local v0, "mMaskingCanvas":Landroid/graphics/Canvas;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mMasking:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 104
    return-void
.end method

.method private initPath()V
    .locals 13

    .prologue
    const v12, 0x418ab439    # 17.338f

    const v11, 0x424f20c5

    const v10, 0x42fa774c

    const/high16 v9, 0x42ec0000    # 118.0f

    const v8, 0x42ea8937

    .line 107
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mMasking:Landroid/graphics/Path;

    .line 108
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x432647f0

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v9

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 109
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x421e5f3b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v9

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 110
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x41fc6042    # 31.547f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v9

    const v3, 0x41c82f1b    # 25.023f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42ded0e5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x41c82f1b    # 25.023f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42ceb8d5    # 103.361f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 112
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x41c82f1b    # 25.023f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42bea1cb

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x41fc6042    # 31.547f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42b18419

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x421e5f3b

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42b18419

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 114
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x422020c5

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42b18419

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4221dd2f    # 40.466f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42b18937

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42239581    # 40.896f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42b19c29    # 88.805f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x422247ae    # 40.57f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42aec000    # 87.375f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42219168

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42abc28f    # 85.88f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42219168

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42a8b3b6

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 118
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x42219168

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4292a6e9

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x424551ec    # 49.33f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4280c28f    # 64.38f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42716e98

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4280c28f    # 64.38f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 120
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x4271a3d7    # 60.41f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4280c28f    # 64.38f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4271d70a    # 60.46f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4280c5a2

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42720c4a

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4280c5a2

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 122
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x426e1fbe

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4289a5e3

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x426c1375

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4292dfbe

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x426c1375

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x429c4e56    # 78.153f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 124
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x426c1375

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a0b958    # 80.362f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42733d71    # 60.81f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42a44e56    # 82.153f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x427c1375

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42a44e56    # 82.153f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x428274bc

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a44e56    # 82.153f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x428609ba

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42a0b8d5    # 80.361f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x428609ba

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x429c4dd3    # 78.152f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 128
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x428609ba

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4287b74c

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x428b70a4    # 69.72f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x426a7ae1    # 58.62f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4294dcac    # 74.431f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x424bf3b6

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 130
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x42a0bcee

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4226ac08    # 41.668f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42b2dc29    # 89.43f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x420a27f0

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42c848b4

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x41f88937    # 31.067f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 132
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x42d0d917

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41edc49c    # 29.721f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42d9fa5e

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x41e76a7f    # 28.927f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42e3872b    # 113.764f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x41e62f1b    # 28.773f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 134
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x42e3a560

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41e62b02    # 28.771f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42e3c000    # 113.875f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x41e60831    # 28.754f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42e3dd2f    # 113.932f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x41e6020c    # 28.751f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 136
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x42e48d50    # 114.276f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41e5f3b6    # 28.744f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42e53c6a

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x41e5d4fe    # 28.729f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42e5ec08

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x41e5d4fe    # 28.729f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 138
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x4305220c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41e5d4fe    # 28.729f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4314ee14    # 148.93f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x421b4bc7

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431d1810

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4256cfdf    # 53.703f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 140
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x4313cd91

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x426046a8    # 56.069f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x430bac08

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4277872b    # 61.882f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43065f7d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x428c753f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 142
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x43053062

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42903021

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4305bd71    # 133.74f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4295224e

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43079ae1

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42978000    # 75.75f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 144
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x43084560

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42985810

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x430902d1

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4298befa

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4309befa

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4298befa

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 146
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x430b11ec    # 139.07f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4298befa

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x430c5d71

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42976f1b

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x430d2083

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42950831

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 148
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x4312b0a4    # 146.69f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42838000    # 65.75f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431c3375

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4272126f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43269168

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4272126f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 150
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x4326accd

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4272126f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4326c6e9

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4271fae1

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4326e1cb

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4271f2b0    # 60.487f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 152
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x43367439

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42733a5e    # 60.807f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4342f9db

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42932d91

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4342f9db

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42b27852    # 89.235f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 154
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x4342fa1d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42d228f6    # 105.08f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4336220c

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v9

    const v5, 0x432647f0

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v9

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 156
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mMasking:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 157
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mMasking:Landroid/graphics/Path;

    const v1, -0x3ed17139

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x433b526b

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 158
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x437adfdd

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x433b526b

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 159
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x437b6160

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43941fc2

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 160
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mMasking:Landroid/graphics/Path;

    const v1, -0x3ed62f72

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43951fe0

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 161
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mMasking:Landroid/graphics/Path;

    const v1, -0x3ed17139

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x434f526b

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 163
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudTop:Landroid/graphics/Path;

    .line 164
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudTop:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 165
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x42733646

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x427eef9e

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 166
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x426f5a1d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42883efa

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x426d52f2

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42915b23

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x426d52f2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x429aac8b

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 168
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x426d52f2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x429f12f2

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42747df4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42a2a24e

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x427d52f2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42a2a24e

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 170
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x4283147b    # 65.54f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a2a24e

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4286a979

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x429f10e5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4286a979

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x429aaa7f    # 77.333f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 172
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x4286a979

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42862c8b

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x428c0f5c    # 70.03f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4267926f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42957a5e

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42492b02    # 50.292f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 174
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x42a15d2f    # 80.682f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42241581    # 41.021f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42b3820c

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4207ac08    # 33.918f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42c8f021

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x41f3b852    # 30.465f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 176
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x42d18831

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41e90419    # 29.127f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42dbb958    # 109.862f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x41e124dd    # 28.143f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42e6a2d1    # 115.318f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x41e124dd    # 28.143f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 178
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x4304bf3b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41e124dd    # 28.143f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431549ba

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4218c6a8    # 38.194f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431d72f2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4253f7cf    # 52.992f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 180
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x431d6000    # 157.375f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42540b44

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431efd2f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4262b74c    # 56.679f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431eea3d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4262cac1

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 182
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x43277e35

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x425e51ec    # 55.58f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 183
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x4325c042

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x424ee873

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 184
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x431c79db

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4202c083    # 32.688f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4308f687

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x41a1d70a    # 20.23f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42e6a2d1    # 115.318f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x41a1d70a    # 20.23f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 186
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x42e11810

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41a1d70a    # 20.23f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42db92f2

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x41a3645a    # 20.424f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42d61c29    # 107.055f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x41a6a5e3    # 20.831f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 188
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x42bcfbe7

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41b42d0e    # 22.522f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42a67333

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x41de70a4    # 27.805f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42954419

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x421425e3

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 190
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x428d2d0e

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4225820c

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42869b23

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x423954fe    # 46.333f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4281a24e

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x424efcee

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 192
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x42805b23

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42543f7d    # 53.062f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x427c0419

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x425ef8d5    # 55.743f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const/high16 v5, 0x427c0000    # 63.0f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x425f051f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 194
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x4277ee98

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x426b6560    # 58.849f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42733646

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x427eef9e

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42733646

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x427eef9e

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 196
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudTop:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 198
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudLeft:Landroid/graphics/Path;

    .line 199
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudLeft:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 200
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x421f9fbe

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 201
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x41fee148    # 31.86f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v8

    const v3, 0x41cab021    # 25.336f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42dd25e3

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x41cab021    # 25.336f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42cd2042

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 203
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x41cab021    # 25.336f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42bd1ba6    # 94.554f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x41fee148    # 31.86f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42afeb02    # 87.959f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x421f9fbe

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42afeb02    # 87.959f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 205
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x42216148

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42afeb02    # 87.959f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42231db2    # 40.779f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42afdf3b

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4224d604    # 41.209f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42aff333

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 207
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x4223872b    # 40.882f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42ad199a    # 86.55f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4222d1ec

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42aa178d

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4222d1ec

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42a70ac1

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 209
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x4222d1ec

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4291178d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42469168

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x427e872b    # 63.632f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4272ae14    # 60.67f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x427e872b    # 63.632f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 211
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x4272e354    # 60.722f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x427e872b    # 63.632f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4282e148    # 65.44f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42805f3b

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4282e148    # 65.44f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42805f3b

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 213
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x4286ad0e

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42606f9e

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 214
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x42880ccd

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42606042

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4275ced9    # 61.452f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x425ea1cb    # 55.658f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4272af1b

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x425ea1cb    # 55.658f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 216
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x4238570a    # 46.085f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x425ea1cb    # 55.658f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42084ac1

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4285a8f6    # 66.83f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42034396

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42a21062

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 218
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x41beae14    # 23.835f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a7fdf4

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v12

    const v4, 0x42b946a8    # 92.638f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v12

    const v6, 0x42cd1fbe

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 220
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudLeft:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v12

    const v2, 0x42e5e76d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x41dbb22d    # 27.462f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v10

    const v5, 0x421fa1cb    # 39.908f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v10

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 222
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x431c374c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v10

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 223
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x431c374c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42ea88b4

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 224
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x421f9fbe

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42ea88b4

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 225
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudLeft:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 227
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudRight:Landroid/graphics/Path;

    .line 228
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudRight:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 229
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x43267ba6

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v11

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 230
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x43263687

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v11

    const v3, 0x4321d5c3

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x424f51ec    # 51.83f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431d3aa0

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42543646

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 233
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x43140b02    # 148.043f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x425df6c9

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x430bfc6a

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4274cbc7

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4306bae1    # 134.73f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x428ae3d7    # 69.445f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 236
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x43058b44

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x428e9aa0

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43061852

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4293872b    # 73.764f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4307f5c3    # 135.96f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4295e24e

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 239
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x4308a083

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4296b9db

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43095df4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x429720c5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x430a19db

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x429720c5

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 242
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x430b6ccd

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x429720c5

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x430cb810

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4295d168

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x430d7be7

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42936d91

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 245
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x43130c08

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4281f852    # 64.985f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431b5efa

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x426efae1

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43273e35

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x426efae1

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 248
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x4336cfdf

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42704396

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4343553f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4291d687

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4343553f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42b0fe77

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 251
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x4343553f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42d08bc7

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43367cee

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v8

    const v5, 0x4326a354    # 166.638f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v8

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 254
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x42775a1d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 255
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x42775a1d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42fa77cf

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 256
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x4326a354    # 166.638f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42fa77cf

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 257
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x4326a354    # 166.638f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v10

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 258
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x433adf3b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v10

    const v3, 0x434b54bc

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42d949ba

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x434b54bc

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42b0fefa

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 261
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x434b54bc

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4288b439

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x433ab78d

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v11

    const v5, 0x43267ba6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v11

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 264
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudRight:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 266
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine1:Landroid/graphics/Path;

    .line 267
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine1:Landroid/graphics/Path;

    const v1, 0x429bfd71

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4301d3b6

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 268
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine1:Landroid/graphics/Path;

    const v1, 0x4298547b    # 76.165f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43009646

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42935b23

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43011127

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4290e0c5

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4302e4dd

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 271
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine1:Landroid/graphics/Path;

    const v1, 0x42590831    # 54.258f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x431be9ba

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 272
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine1:Landroid/graphics/Path;

    const v1, 0x4254051f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x431dbb23

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4255d917

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4320399a

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x425d1cac    # 55.278f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43217a5e

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 275
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine1:Landroid/graphics/Path;

    const v1, 0x42600b44

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4321fc6a

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42636a7f    # 56.854f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43223687

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4266b852    # 57.68f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43222f5c

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 278
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine1:Landroid/graphics/Path;

    const v1, 0x426b9eb8

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432224dd

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42706148

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43218b44

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42735c29    # 60.84f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4320753f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 281
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine1:Landroid/graphics/Path;

    const v1, 0x429e1f3b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43076148    # 135.38f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 282
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine1:Landroid/graphics/Path;

    const v1, 0x42a09aa0

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43058d50

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x429fa5e3

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x430310a4

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x429bfd71

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4301d3b6

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 286
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine2:Landroid/graphics/Path;

    .line 288
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine2:Landroid/graphics/Path;

    const v1, 0x42e16873

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4301d917

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 289
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine2:Landroid/graphics/Path;

    const v1, 0x42ddc5a2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4300978d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42d8c9ba

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43010c4a

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42d6472b    # 107.139f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4302dd71

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 292
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine2:Landroid/graphics/Path;

    const v1, 0x42b97127    # 92.721f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43169e77

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 293
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine2:Landroid/graphics/Path;

    const v1, 0x42b6f5c3    # 91.48f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x431871ec

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42b7e7f0

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x431aef9e

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42bb8e56    # 93.778f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x431c2d91

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 296
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine2:Landroid/graphics/Path;

    const v1, 0x42bd06a8    # 94.513f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x431cae14    # 156.68f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42beb7cf

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x431ce6a8    # 156.901f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42c05fbe

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x431cddf4

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 299
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine2:Landroid/graphics/Path;

    const v1, 0x42c2d26f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x431cd168

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42c531aa    # 98.597f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x431c35c3    # 156.21f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42c6aa7f    # 99.333f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x431b1e77

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 302
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine2:Landroid/graphics/Path;

    const v1, 0x42e36f9e

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x430769fc

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 303
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine2:Landroid/graphics/Path;

    const v1, 0x42e5f3b6

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43059893

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42e50ac1

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43031aa0

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42e16873

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4301d917

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 307
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine3:Landroid/graphics/Path;

    .line 309
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine3:Landroid/graphics/Path;

    const v1, 0x431c88f6

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4301e76d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 310
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine3:Landroid/graphics/Path;

    const v1, 0x431abf3b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43009a5e

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43183efa

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43010042

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4316f26f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4302c8f6

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 313
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine3:Landroid/graphics/Path;

    const v1, 0x42fa126f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432570a4    # 165.44f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 314
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine3:Landroid/graphics/Path;

    const v1, 0x42f788b4

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43273fbe

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42f86873

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4329be77

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42fc0625

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x432b0396

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 317
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine3:Landroid/graphics/Path;

    const v1, 0x42fd7a5e

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432b86e9

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42ff2873

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x432bc312

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43006873

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x432bbdf4

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 320
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine3:Landroid/graphics/Path;

    const v1, 0x4301a20c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432bb604

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4302d3b6

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x432b1e77

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x430394fe    # 131.582f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x432a0a3d    # 170.04f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 323
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine3:Landroid/graphics/Path;

    const v1, 0x431d6b02    # 157.418f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43077d71    # 135.49f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 324
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine3:Landroid/graphics/Path;

    const v1, 0x431eb78d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4305b3b6

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431e522d    # 158.321f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43033375

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431c88f6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4301e76d

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 328
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine4:Landroid/graphics/Path;

    .line 330
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine4:Landroid/graphics/Path;

    const v1, 0x42ab35c3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x431f5021

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 331
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine4:Landroid/graphics/Path;

    const v1, 0x42a7a1cb

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x431e051f    # 158.02f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42a2a148

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x431e6b85    # 158.42f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42a00a3d    # 80.02f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43203646

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 334
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine4:Landroid/graphics/Path;

    const v1, 0x427f8c4a

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4335a9ba

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 335
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine4:Landroid/graphics/Path;

    const v1, 0x427a6042

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43377439

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x427bfcee

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4339f47b

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x428192f2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x433b3fbe

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 338
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine4:Landroid/graphics/Path;

    const v1, 0x42830419

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x433bc560

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4284b0a4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x433c04dd

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42865893

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x433c024e

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 341
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine4:Landroid/graphics/Path;

    const v1, 0x4288cbc7

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x433bfe77

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x428b3333    # 69.6f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x433b6b02    # 187.418f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x428cbd71    # 70.37f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x433a599a    # 186.35f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 344
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine4:Landroid/graphics/Path;

    const v1, 0x42ad0189

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4324e5e3

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 345
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine4:Landroid/graphics/Path;

    const v1, 0x42af9917

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43231b64

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42aecac1

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43209b23

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42ab35c3

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x431f5021

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 349
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine5:Landroid/graphics/Path;

    .line 351
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine5:Landroid/graphics/Path;

    const/high16 v1, 0x42e30000    # 113.5f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43282ac1

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 352
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine5:Landroid/graphics/Path;

    const v1, 0x42df7333

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4326d9db

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42da7021

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x432739db

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42d7ced9    # 107.904f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4328ffbe

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 355
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine5:Landroid/graphics/Path;

    const v1, 0x42c56979

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4335876d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 356
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine5:Landroid/graphics/Path;

    const v1, 0x42c2c937

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43374dd3    # 183.304f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42c38831

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4339cf5c    # 185.81f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42c7147b    # 99.54f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x433b2000    # 187.125f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 359
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine5:Landroid/graphics/Path;

    const v1, 0x42c8828f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x433ba7f0

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42ca2e98

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x433be979

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42cbd581    # 101.917f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x433be979

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 362
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine5:Landroid/graphics/Path;

    const v1, 0x42ce4937

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x433be979

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42d0b439

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x433b59db

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42d2449c

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x433a4a7f    # 186.291f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 365
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine5:Landroid/graphics/Path;

    const v1, 0x42e4a9fc    # 114.332f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432dc2d1

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 366
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine5:Landroid/graphics/Path;

    const v1, 0x42e74bc7

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432bfc29    # 171.985f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42e68ccd

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43297b23

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v5

    const/high16 v5, 0x42e30000    # 113.5f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43282ac1

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 370
    return-void
.end method

.method private makeAnimation()V
    .locals 15

    .prologue
    const/high16 v14, -0x3de00000    # -40.0f

    const/4 v13, 0x0

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 609
    const-wide/16 v2, 0x320

    .line 610
    .local v2, "duration":J
    const-wide/16 v4, 0x4

    div-long v0, v2, v4

    .line 612
    .local v0, "delayGap":J
    const/4 v4, 0x4

    new-array v4, v4, [Landroid/animation/PropertyValuesHolder;

    const-string v5, "y"

    new-array v6, v12, [F

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v7, v14

    aput v7, v6, v10

    const/high16 v7, 0x42c80000    # 100.0f

    iget v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v7, v8

    aput v7, v6, v11

    .line 613
    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    aput-object v5, v4, v10

    const-string v5, "x"

    new-array v6, v12, [F

    const/high16 v7, 0x41f00000    # 30.0f

    iget v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v7, v8

    aput v7, v6, v10

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v7, v14

    aput v7, v6, v11

    .line 614
    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    aput-object v5, v4, v11

    const-string v5, "e_y"

    new-array v6, v12, [F

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v7, v14

    aput v7, v6, v10

    aput v13, v6, v11

    .line 615
    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    aput-object v5, v4, v12

    const/4 v5, 0x3

    const-string v6, "e_x"

    new-array v7, v12, [F

    const/high16 v8, 0x41f00000    # 30.0f

    iget v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v8, v9

    aput v8, v7, v10

    aput v13, v7, v11

    .line 616
    invoke-static {v6, v7}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v6

    aput-object v6, v4, v5

    .line 612
    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop1:Landroid/animation/ValueAnimator;

    .line 618
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop1:Landroid/animation/ValueAnimator;

    const/4 v5, -0x1

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 619
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop1:Landroid/animation/ValueAnimator;

    invoke-virtual {v4, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 620
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop1:Landroid/animation/ValueAnimator;

    new-instance v5, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 621
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop1:Landroid/animation/ValueAnimator;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v4, v5}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 622
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop1:Landroid/animation/ValueAnimator;

    invoke-virtual {v4}, Landroid/animation/ValueAnimator;->start()V

    .line 624
    const/4 v4, 0x4

    new-array v4, v4, [Landroid/animation/PropertyValuesHolder;

    const-string v5, "y"

    new-array v6, v12, [F

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v7, v14

    aput v7, v6, v10

    const/high16 v7, 0x42c80000    # 100.0f

    iget v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v7, v8

    aput v7, v6, v11

    .line 625
    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    aput-object v5, v4, v10

    const-string v5, "x"

    new-array v6, v12, [F

    const/high16 v7, 0x41f00000    # 30.0f

    iget v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v7, v8

    aput v7, v6, v10

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v7, v14

    aput v7, v6, v11

    .line 626
    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    aput-object v5, v4, v11

    const-string v5, "e_y"

    new-array v6, v12, [F

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v7, v14

    aput v7, v6, v10

    aput v13, v6, v11

    .line 627
    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    aput-object v5, v4, v12

    const/4 v5, 0x3

    const-string v6, "e_x"

    new-array v7, v12, [F

    const/high16 v8, 0x41f00000    # 30.0f

    iget v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v8, v9

    aput v8, v7, v10

    aput v13, v7, v11

    .line 628
    invoke-static {v6, v7}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v6

    aput-object v6, v4, v5

    .line 624
    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop2:Landroid/animation/ValueAnimator;

    .line 630
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop2:Landroid/animation/ValueAnimator;

    const/4 v5, -0x1

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 631
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop2:Landroid/animation/ValueAnimator;

    invoke-virtual {v4, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 632
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop2:Landroid/animation/ValueAnimator;

    invoke-virtual {v4, v0, v1}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 633
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop2:Landroid/animation/ValueAnimator;

    new-instance v5, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 634
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop2:Landroid/animation/ValueAnimator;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v4, v5}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 635
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop2:Landroid/animation/ValueAnimator;

    invoke-virtual {v4}, Landroid/animation/ValueAnimator;->start()V

    .line 637
    const/4 v4, 0x4

    new-array v4, v4, [Landroid/animation/PropertyValuesHolder;

    const-string v5, "y"

    new-array v6, v12, [F

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v7, v14

    aput v7, v6, v10

    const/high16 v7, 0x42c80000    # 100.0f

    iget v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v7, v8

    aput v7, v6, v11

    .line 638
    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    aput-object v5, v4, v10

    const-string v5, "x"

    new-array v6, v12, [F

    const/high16 v7, 0x41f00000    # 30.0f

    iget v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v7, v8

    aput v7, v6, v10

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v7, v14

    aput v7, v6, v11

    .line 639
    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    aput-object v5, v4, v11

    const-string v5, "e_y"

    new-array v6, v12, [F

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v7, v14

    aput v7, v6, v10

    aput v13, v6, v11

    .line 640
    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    aput-object v5, v4, v12

    const/4 v5, 0x3

    const-string v6, "e_x"

    new-array v7, v12, [F

    const/high16 v8, 0x41f00000    # 30.0f

    iget v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v8, v9

    aput v8, v7, v10

    aput v13, v7, v11

    .line 641
    invoke-static {v6, v7}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v6

    aput-object v6, v4, v5

    .line 637
    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop3:Landroid/animation/ValueAnimator;

    .line 643
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop3:Landroid/animation/ValueAnimator;

    const/4 v5, -0x1

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 644
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop3:Landroid/animation/ValueAnimator;

    invoke-virtual {v4, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 645
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop3:Landroid/animation/ValueAnimator;

    const-wide/16 v5, 0x2

    mul-long/2addr v5, v0

    invoke-virtual {v4, v5, v6}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 646
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop3:Landroid/animation/ValueAnimator;

    new-instance v5, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 647
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop3:Landroid/animation/ValueAnimator;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v4, v5}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 648
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop3:Landroid/animation/ValueAnimator;

    invoke-virtual {v4}, Landroid/animation/ValueAnimator;->start()V

    .line 651
    const/4 v4, 0x4

    new-array v4, v4, [Landroid/animation/PropertyValuesHolder;

    const-string v5, "y"

    new-array v6, v12, [F

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v7, v14

    aput v7, v6, v10

    const/high16 v7, 0x42c80000    # 100.0f

    iget v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v7, v8

    aput v7, v6, v11

    .line 652
    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    aput-object v5, v4, v10

    const-string v5, "x"

    new-array v6, v12, [F

    const/high16 v7, 0x41f00000    # 30.0f

    iget v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v7, v8

    aput v7, v6, v10

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v7, v14

    aput v7, v6, v11

    .line 653
    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    aput-object v5, v4, v11

    const-string v5, "e_y"

    new-array v6, v12, [F

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v7, v14

    aput v7, v6, v10

    aput v13, v6, v11

    .line 654
    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    aput-object v5, v4, v12

    const/4 v5, 0x3

    const-string v6, "e_x"

    new-array v7, v12, [F

    const/high16 v8, 0x41f00000    # 30.0f

    iget v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v8, v9

    aput v8, v7, v10

    aput v13, v7, v11

    .line 655
    invoke-static {v6, v7}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v6

    aput-object v6, v4, v5

    .line 651
    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop4:Landroid/animation/ValueAnimator;

    .line 657
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop4:Landroid/animation/ValueAnimator;

    const/4 v5, -0x1

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 658
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop4:Landroid/animation/ValueAnimator;

    invoke-virtual {v4, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 659
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop4:Landroid/animation/ValueAnimator;

    const-wide/16 v5, 0x3

    mul-long/2addr v5, v0

    invoke-virtual {v4, v5, v6}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 660
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop4:Landroid/animation/ValueAnimator;

    new-instance v5, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 661
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop4:Landroid/animation/ValueAnimator;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v4, v5}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 662
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop4:Landroid/animation/ValueAnimator;

    invoke-virtual {v4}, Landroid/animation/ValueAnimator;->start()V

    .line 664
    const/4 v4, 0x4

    new-array v4, v4, [Landroid/animation/PropertyValuesHolder;

    const-string v5, "y"

    new-array v6, v12, [F

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v7, v14

    aput v7, v6, v10

    const/high16 v7, 0x42c80000    # 100.0f

    iget v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v7, v8

    aput v7, v6, v11

    .line 665
    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    aput-object v5, v4, v10

    const-string v5, "x"

    new-array v6, v12, [F

    const/high16 v7, 0x41f00000    # 30.0f

    iget v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v7, v8

    aput v7, v6, v10

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v7, v14

    aput v7, v6, v11

    .line 666
    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    aput-object v5, v4, v11

    const-string v5, "e_y"

    new-array v6, v12, [F

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v7, v14

    aput v7, v6, v10

    aput v13, v6, v11

    .line 667
    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    aput-object v5, v4, v12

    const/4 v5, 0x3

    const-string v6, "e_x"

    new-array v7, v12, [F

    const/high16 v8, 0x41f00000    # 30.0f

    iget v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v8, v9

    aput v8, v7, v10

    aput v13, v7, v11

    .line 668
    invoke-static {v6, v7}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v6

    aput-object v6, v4, v5

    .line 664
    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop5:Landroid/animation/ValueAnimator;

    .line 670
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop5:Landroid/animation/ValueAnimator;

    const/4 v5, -0x1

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 671
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop5:Landroid/animation/ValueAnimator;

    invoke-virtual {v4, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 672
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop5:Landroid/animation/ValueAnimator;

    const-wide/16 v5, 0x4

    mul-long/2addr v5, v0

    invoke-virtual {v4, v5, v6}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 673
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop5:Landroid/animation/ValueAnimator;

    new-instance v5, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 674
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop5:Landroid/animation/ValueAnimator;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v4, v5}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 675
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop5:Landroid/animation/ValueAnimator;

    invoke-virtual {v4}, Landroid/animation/ValueAnimator;->start()V

    .line 677
    new-array v4, v12, [Landroid/animation/PropertyValuesHolder;

    const-string v5, "y"

    const/4 v6, 0x5

    new-array v6, v6, [F

    aput v13, v6, v10

    const/high16 v7, -0x3fc00000    # -3.0f

    iget v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v7, v8

    aput v7, v6, v11

    aput v13, v6, v12

    const/4 v7, 0x3

    const/high16 v8, -0x3fc00000    # -3.0f

    iget v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v8, v9

    aput v8, v6, v7

    const/4 v7, 0x4

    aput v13, v6, v7

    .line 678
    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    aput-object v5, v4, v10

    const-string v5, "x"

    const/4 v6, 0x5

    new-array v6, v6, [F

    aput v13, v6, v10

    const/high16 v7, -0x3ee00000    # -10.0f

    iget v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v7, v8

    aput v7, v6, v11

    aput v13, v6, v12

    const/4 v7, 0x3

    const/high16 v8, 0x41200000    # 10.0f

    iget v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v8, v9

    aput v8, v6, v7

    const/4 v7, 0x4

    aput v13, v6, v7

    .line 679
    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    aput-object v5, v4, v11

    .line 677
    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mIconTranslate:Landroid/animation/ValueAnimator;

    .line 681
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mIconTranslate:Landroid/animation/ValueAnimator;

    const-wide/16 v5, 0x1194

    invoke-virtual {v4, v5, v6}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 682
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mIconTranslate:Landroid/animation/ValueAnimator;

    const/4 v5, -0x1

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 683
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mIconTranslate:Landroid/animation/ValueAnimator;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v4, v5}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 684
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mIconTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v4}, Landroid/animation/ValueAnimator;->start()V

    .line 686
    const/4 v4, 0x3

    new-array v4, v4, [F

    aput v13, v4, v10

    const v5, -0x3fd33333    # -2.7f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v5, v6

    aput v5, v4, v11

    aput v13, v4, v12

    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    .line 687
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    const/4 v5, -0x1

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 688
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    const-wide/16 v5, 0x5dc

    invoke-virtual {v4, v5, v6}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 689
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v4, v5}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 690
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v4}, Landroid/animation/ValueAnimator;->start()V

    .line 691
    return-void
.end method


# virtual methods
.method public cancelAnimation()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 722
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 723
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 725
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mIconTranslate:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mIconTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 726
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mIconTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 729
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop1:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 730
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 733
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop2:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 734
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 737
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop3:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 738
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 741
    :cond_4
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop4:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop4:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 742
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop4:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 745
    :cond_5
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop5:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop5:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 746
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop5:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 751
    :cond_6
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mIsActiveAnimationThread:Z

    .line 752
    return-void
.end method

.method public getScale()F
    .locals 1

    .prologue
    .line 786
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 448
    return-object p0
.end method

.method public isRunning()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 760
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mIconTranslate:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop1:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop2:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop3:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop4:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop5:Landroid/animation/ValueAnimator;

    if-nez v1, :cond_1

    .line 777
    :cond_0
    :goto_0
    return v0

    .line 771
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mIconTranslate:Landroid/animation/ValueAnimator;

    .line 772
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop1:Landroid/animation/ValueAnimator;

    .line 773
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop2:Landroid/animation/ValueAnimator;

    .line 774
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop3:Landroid/animation/ValueAnimator;

    .line 775
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop4:Landroid/animation/ValueAnimator;

    .line 776
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop5:Landroid/animation/ValueAnimator;

    .line 777
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v4, 0x43960000    # 300.0f

    const/4 v2, 0x0

    .line 403
    :try_start_0
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 405
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    cmpg-float v0, v0, v2

    if-gtz v0, :cond_1

    .line 406
    const-string v0, ""

    const-string v1, "scale is less then 0"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 445
    :cond_0
    :goto_0
    return-void

    .line 410
    :cond_1
    const/4 v8, 0x0

    .line 411
    .local v8, "cloudTrX":F
    const/4 v9, 0x0

    .line 412
    .local v9, "cloudTrY":F
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mIsActiveAnimationThread:Z

    if-eqz v0, :cond_2

    .line 413
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mIconTranslate:Landroid/animation/ValueAnimator;

    const-string v1, "x"

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v8

    .line 414
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mIconTranslate:Landroid/animation/ValueAnimator;

    const-string v1, "y"

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v9

    .line 417
    :cond_2
    const/16 v6, 0x1f

    .line 419
    .local v6, "saveFlag":I
    const/16 v6, 0xf

    .line 422
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float v1, v2, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v2, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float v3, v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    mul-float/2addr v4, v0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->saveLayer(FFFFLandroid/graphics/Paint;I)I

    move-result v11

    .line 425
    .local v11, "transparentSaveLevel":I
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->drawDrop(Landroid/graphics/Canvas;)V

    .line 426
    invoke-virtual {p1, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    .line 427
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPaint:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->drawTransparentArea(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V

    .line 429
    invoke-virtual {p1, v11}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 431
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v7

    .line 432
    .local v7, "cloudSaveLevel":I
    invoke-virtual {p1, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    .line 433
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPaint:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->drawCloud(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    .line 435
    invoke-virtual {p1, v7}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 437
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 438
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->cancelAnimation()V

    .line 439
    const-string v0, ""

    const-string v1, "cancelA"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 442
    .end local v6    # "saveFlag":I
    .end local v7    # "cloudSaveLevel":I
    .end local v8    # "cloudTrX":F
    .end local v9    # "cloudTrY":F
    .end local v11    # "transparentSaveLevel":I
    :catch_0
    move-exception v10

    .line 443
    .local v10, "e":Ljava/lang/Exception;
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 856
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 795
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onViewDetachedFromWindow : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 796
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 797
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 798
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 800
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mIconTranslate:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_1

    .line 801
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mIconTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 802
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mIconTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 805
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop1:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_2

    .line 806
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 807
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 810
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop2:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_3

    .line 811
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 812
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 815
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop3:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_4

    .line 816
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 817
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 820
    :cond_4
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop4:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_5

    .line 821
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop4:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 822
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop4:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 825
    :cond_5
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop5:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_6

    .line 826
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop5:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 827
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop5:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 831
    :cond_6
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    .line 832
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mIconTranslate:Landroid/animation/ValueAnimator;

    .line 833
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop1:Landroid/animation/ValueAnimator;

    .line 834
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop2:Landroid/animation/ValueAnimator;

    .line 835
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop3:Landroid/animation/ValueAnimator;

    .line 836
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop4:Landroid/animation/ValueAnimator;

    .line 837
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mDrop5:Landroid/animation/ValueAnimator;

    .line 838
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPaint:Landroid/graphics/Paint;

    .line 839
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mMasking:Landroid/graphics/Path;

    .line 840
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudLeft:Landroid/graphics/Path;

    .line 841
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudRight:Landroid/graphics/Path;

    .line 842
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mCloudTop:Landroid/graphics/Path;

    .line 843
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine1:Landroid/graphics/Path;

    .line 844
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine2:Landroid/graphics/Path;

    .line 845
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine3:Landroid/graphics/Path;

    .line 846
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine4:Landroid/graphics/Path;

    .line 847
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mRainLine5:Landroid/graphics/Path;

    .line 848
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_7

    .line 849
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 851
    :cond_7
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mBitmap:Landroid/graphics/Bitmap;

    .line 852
    return-void
.end method

.method public setPaintColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 75
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPaintColor:I

    .line 76
    return-void
.end method

.method public setScale(F)V
    .locals 0
    .param p1, "scale"    # F

    .prologue
    .line 781
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mScale:F

    .line 782
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->init()V

    .line 783
    return-void
.end method

.method public setWidthAndHeight(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 790
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, p1, p2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 791
    .local v0, "l":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 793
    return-void
.end method

.method public startAnimation()V
    .locals 2

    .prologue
    .line 694
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->isStop:Z

    .line 695
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPreFixProperty1:Ljava/lang/String;

    .line 696
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPreFixProperty2:Ljava/lang/String;

    .line 697
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPreFixProperty3:Ljava/lang/String;

    .line 698
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPreFixProperty4:Ljava/lang/String;

    .line 699
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mPreFixProperty5:Ljava/lang/String;

    .line 701
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->makeAnimation()V

    .line 703
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->mIsActiveAnimationThread:Z

    .line 704
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;)V

    .line 717
    .local v0, "t":Ljava/lang/Thread;
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 718
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 719
    return-void
.end method

.method public stopAnimation()V
    .locals 1

    .prologue
    .line 756
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ShowerIconView;->isStop:Z

    .line 757
    return-void
.end method
