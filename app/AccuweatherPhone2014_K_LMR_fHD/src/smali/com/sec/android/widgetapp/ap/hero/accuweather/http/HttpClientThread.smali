.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpClientThread;
.super Ljava/lang/Object;
.source "HttpClientThread.java"


# static fields
.field public static final DEFAULT_HTTP_HEADER_ACCEPT:Ljava/lang/String; = "*,*/*"

.field public static final DEFAULT_HTTP_HEADER_CONTENTTYPE:Ljava/lang/String; = "text/xml"

.field public static final DEFAULT_HTTP_HEADER_GZIP:Ljava/lang/String; = "gzip"

.field public static final DEFAULT_HTTP_HEADER_USERAGENT:Ljava/lang/String; = "SAMSUNG-Android"

.field public static final DEFAULT_TIMEOUT_MILLI:I = 0x7530


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static get(Landroid/content/Context;Ljava/net/URL;Lorg/apache/http/message/HeaderGroup;)Lorg/apache/http/HttpResponse;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "url"    # Ljava/net/URL;
    .param p2, "headers"    # Lorg/apache/http/message/HeaderGroup;

    .prologue
    .line 51
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p0, v0, p1, v1, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpClientThread;->sendRequest(Landroid/content/Context;ILjava/net/URL;Ljava/lang/String;Lorg/apache/http/message/HeaderGroup;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method

.method public static post(Landroid/content/Context;Ljava/net/URL;Ljava/lang/String;Lorg/apache/http/message/HeaderGroup;)Lorg/apache/http/HttpResponse;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "url"    # Ljava/net/URL;
    .param p2, "body"    # Ljava/lang/String;
    .param p3, "headers"    # Lorg/apache/http/message/HeaderGroup;

    .prologue
    .line 59
    const/4 v0, 0x2

    invoke-static {p0, v0, p1, p2, p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpClientThread;->sendRequest(Landroid/content/Context;ILjava/net/URL;Ljava/lang/String;Lorg/apache/http/message/HeaderGroup;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method

.method public static post(Landroid/content/Context;Ljava/net/URL;Lorg/apache/http/message/HeaderGroup;)Lorg/apache/http/HttpResponse;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "url"    # Ljava/net/URL;
    .param p2, "headers"    # Lorg/apache/http/message/HeaderGroup;

    .prologue
    .line 55
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-static {p0, v0, p1, v1, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpClientThread;->sendRequest(Landroid/content/Context;ILjava/net/URL;Ljava/lang/String;Lorg/apache/http/message/HeaderGroup;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method

.method private static sendRequest(Landroid/content/Context;ILjava/net/URL;Ljava/lang/String;Lorg/apache/http/message/HeaderGroup;)Lorg/apache/http/HttpResponse;
    .locals 22
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "method"    # I
    .param p2, "url"    # Ljava/net/URL;
    .param p3, "body"    # Ljava/lang/String;
    .param p4, "headers"    # Lorg/apache/http/message/HeaderGroup;

    .prologue
    .line 69
    new-instance v6, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v6}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 71
    .local v6, "hParam":Lorg/apache/http/params/HttpParams;
    const/16 v18, 0x7530

    move/from16 v0, v18

    invoke-static {v6, v0}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 73
    const/16 v18, 0x7530

    move/from16 v0, v18

    invoke-static {v6, v0}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 75
    new-instance v14, Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct {v14}, Lorg/apache/http/conn/scheme/SchemeRegistry;-><init>()V

    .line 76
    .local v14, "schemeRegistry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    new-instance v18, Lorg/apache/http/conn/scheme/Scheme;

    const-string v19, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v20

    const/16 v21, 0x50

    invoke-direct/range {v18 .. v21}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 77
    new-instance v18, Lorg/apache/http/conn/scheme/Scheme;

    const-string v19, "https"

    invoke-static {}, Lorg/apache/http/conn/ssl/SSLSocketFactory;->getSocketFactory()Lorg/apache/http/conn/ssl/SSLSocketFactory;

    move-result-object v20

    const/16 v21, 0x1bb

    invoke-direct/range {v18 .. v21}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 80
    new-instance v12, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    invoke-direct {v12, v6, v14}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    .line 81
    .local v12, "manager":Lorg/apache/http/conn/ClientConnectionManager;
    new-instance v7, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v7, v12, v6}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    .line 83
    .local v7, "httpClient":Lorg/apache/http/impl/client/DefaultHttpClient;
    invoke-static/range {p0 .. p0}, Landroid/net/Proxy;->getHost(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v18

    if-eqz v18, :cond_0

    .line 84
    new-instance v9, Lorg/apache/http/HttpHost;

    invoke-static/range {p0 .. p0}, Landroid/net/Proxy;->getHost(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v18

    invoke-static/range {p0 .. p0}, Landroid/net/Proxy;->getPort(Landroid/content/Context;)I

    move-result v19

    const-string v20, "http"

    move-object/from16 v0, v18

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-direct {v9, v0, v1, v2}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 85
    .local v9, "httpHost":Lorg/apache/http/HttpHost;
    invoke-virtual {v7}, Lorg/apache/http/impl/client/DefaultHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v18

    const-string v19, "http.route.default-proxy"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-interface {v0, v1, v9}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 88
    .end local v9    # "httpHost":Lorg/apache/http/HttpHost;
    :cond_0
    invoke-virtual {v7}, Lorg/apache/http/impl/client/DefaultHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v18

    const-string v19, "http.protocol.cookie-policy"

    const-string v20, "rfc2109"

    invoke-interface/range {v18 .. v20}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 90
    const/4 v11, 0x0

    .line 91
    .local v11, "httpUriRequest":Lorg/apache/http/client/methods/HttpUriRequest;
    const/16 v17, 0x0

    .line 93
    .local v17, "uri":Ljava/net/URI;
    :try_start_0
    invoke-virtual/range {p2 .. p2}, Ljava/net/URL;->toURI()Ljava/net/URI;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v17

    .line 98
    :goto_0
    packed-switch p1, :pswitch_data_0

    .line 120
    const-string v18, ""

    const-string v19, "method Exception"

    invoke-static/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    :goto_1
    if-eqz v11, :cond_3

    .line 126
    const-string v18, "User-Agent"

    const-string v19, "SAMSUNG-Android"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-interface {v11, v0, v1}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    const-string v18, "Accept"

    const-string v19, "*,*/*"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-interface {v11, v0, v1}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    const-string v18, "Content-Type"

    const-string v19, "text/xml"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-interface {v11, v0, v1}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    if-eqz p4, :cond_1

    .line 132
    invoke-virtual/range {p4 .. p4}, Lorg/apache/http/message/HeaderGroup;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-interface {v11, v0}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeaders([Lorg/apache/http/Header;)V

    .line 134
    :cond_1
    const-string v18, "Accept-Encoding"

    const-string v19, "gzip"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-interface {v11, v0, v1}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    const/4 v13, 0x0

    .line 138
    .local v13, "response":Lorg/apache/http/HttpResponse;
    new-instance v3, Lorg/apache/http/protocol/BasicHttpContext;

    invoke-direct {v3}, Lorg/apache/http/protocol/BasicHttpContext;-><init>()V

    .line 140
    .local v3, "bContext":Lorg/apache/http/protocol/HttpContext;
    :try_start_1
    invoke-virtual {v7, v11, v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;
    :try_end_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5

    move-result-object v13

    .line 153
    :goto_2
    if-eqz v13, :cond_2

    .line 154
    const-string v18, "Cache-Control"

    const-string v19, "no-cache, no-store, must-revalidate"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-interface {v13, v0, v1}, Lorg/apache/http/HttpResponse;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    const-string v18, "Pragma"

    const-string v19, "no-cache"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-interface {v13, v0, v1}, Lorg/apache/http/HttpResponse;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    .end local v3    # "bContext":Lorg/apache/http/protocol/HttpContext;
    .end local v13    # "response":Lorg/apache/http/HttpResponse;
    :cond_2
    :goto_3
    return-object v13

    .line 94
    :catch_0
    move-exception v5

    .line 95
    .local v5, "e1":Ljava/net/URISyntaxException;
    invoke-virtual {v5}, Ljava/net/URISyntaxException;->printStackTrace()V

    goto :goto_0

    .line 100
    .end local v5    # "e1":Ljava/net/URISyntaxException;
    :pswitch_0
    const-string v18, ""

    const-string v19, "http request by @@ GET @@"

    invoke-static/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    new-instance v8, Lorg/apache/http/client/methods/HttpGet;

    move-object/from16 v0, v17

    invoke-direct {v8, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    .line 102
    .local v8, "httpGet":Lorg/apache/http/client/methods/HttpGet;
    move-object v11, v8

    .line 103
    goto :goto_1

    .line 105
    .end local v8    # "httpGet":Lorg/apache/http/client/methods/HttpGet;
    :pswitch_1
    const-string v18, ""

    const-string v19, "http request by @@ POST @@"

    invoke-static/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    new-instance v10, Lorg/apache/http/client/methods/HttpPost;

    move-object/from16 v0, v17

    invoke-direct {v10, v0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/net/URI;)V

    .line 107
    .local v10, "httpPost":Lorg/apache/http/client/methods/HttpPost;
    move-object v11, v10

    .line 109
    const/4 v15, 0x0

    .line 111
    .local v15, "strEntity":Lorg/apache/http/entity/StringEntity;
    :try_start_2
    new-instance v16, Lorg/apache/http/entity/StringEntity;

    const-string v18, "UTF-8"

    move-object/from16 v0, v16

    move-object/from16 v1, p3

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .end local v15    # "strEntity":Lorg/apache/http/entity/StringEntity;
    .local v16, "strEntity":Lorg/apache/http/entity/StringEntity;
    move-object/from16 v15, v16

    .line 117
    .end local v16    # "strEntity":Lorg/apache/http/entity/StringEntity;
    .restart local v15    # "strEntity":Lorg/apache/http/entity/StringEntity;
    :goto_4
    invoke-virtual {v10, v15}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    goto/16 :goto_1

    .line 112
    :catch_1
    move-exception v4

    .line 113
    .local v4, "e":Ljava/io/UnsupportedEncodingException;
    const-string v18, ""

    const-string v19, "ADVHTTP UnsupportedEncodingException"

    invoke-static/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 114
    .end local v4    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_2
    move-exception v4

    .line 115
    .local v4, "e":Ljava/lang/Exception;
    const-string v18, ""

    const-string v19, "ADVHTTP Exception"

    invoke-static/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 141
    .end local v4    # "e":Ljava/lang/Exception;
    .end local v10    # "httpPost":Lorg/apache/http/client/methods/HttpPost;
    .end local v15    # "strEntity":Lorg/apache/http/entity/StringEntity;
    .restart local v3    # "bContext":Lorg/apache/http/protocol/HttpContext;
    .restart local v13    # "response":Lorg/apache/http/HttpResponse;
    :catch_3
    move-exception v4

    .line 142
    .local v4, "e":Lorg/apache/http/client/ClientProtocolException;
    const-string v18, ""

    const-string v19, "ADVHTTP ClientProtocolException"

    invoke-static/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    invoke-virtual {v4}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    goto :goto_2

    .line 144
    .end local v4    # "e":Lorg/apache/http/client/ClientProtocolException;
    :catch_4
    move-exception v4

    .line 145
    .local v4, "e":Ljava/io/IOException;
    const-string v18, ""

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "ADVHTTP IOException "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    .line 147
    .end local v4    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v4

    .line 148
    .local v4, "e":Ljava/lang/Exception;
    const-string v18, ""

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "ADVHTTP Exception "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    .line 161
    .end local v3    # "bContext":Lorg/apache/http/protocol/HttpContext;
    .end local v4    # "e":Ljava/lang/Exception;
    .end local v13    # "response":Lorg/apache/http/HttpResponse;
    :cond_3
    const/4 v13, 0x0

    goto/16 :goto_3

    .line 98
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
