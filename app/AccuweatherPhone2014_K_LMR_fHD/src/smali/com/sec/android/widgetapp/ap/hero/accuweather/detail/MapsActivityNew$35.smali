.class Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$35;
.super Ljava/lang/Object;
.source "MapsActivityNew.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->showCities(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

.field final synthetic val$citylist:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Ljava/util/ArrayList;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 1311
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$35;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    iput-object p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$35;->val$citylist:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    const/4 v2, 0x0

    .line 1313
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$35;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    iget-boolean v1, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->positive:Z

    if-eqz v1, :cond_0

    .line 1314
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$35;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->setKeyboardVisible(Z)V
    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$1000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Z)V

    .line 1315
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$35;->val$citylist:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$35;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mDialogSelected:I
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    .line 1316
    .local v0, "i":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$35;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-virtual {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->perform_Acity(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;)V

    .line 1322
    .end local v0    # "i":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    :goto_0
    return-void

    .line 1318
    :cond_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$35;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mRetry:Z
    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$2402(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Z)Z

    .line 1319
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$35;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->setKeyboardVisible(Z)V
    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$1000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Z)V

    .line 1320
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$35;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->hideLoadingDialog()V
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$2300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    goto :goto_0
.end method
