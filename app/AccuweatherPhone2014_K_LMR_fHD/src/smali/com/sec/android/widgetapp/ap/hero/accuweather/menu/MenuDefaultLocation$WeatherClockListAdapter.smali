.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$WeatherClockListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "MenuDefaultLocation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "WeatherClockListAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;",
        ">;"
    }
.end annotation


# instance fields
.field private cityList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;",
            ">;"
        }
    .end annotation
.end field

.field private context:Landroid/content/Context;

.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;


# direct methods
.method public constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;
    .param p2, "ctx"    # Landroid/content/Context;
    .param p3, "resourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 285
    .local p4, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;>;"
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$WeatherClockListAdapter;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;

    .line 286
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 287
    iput-object p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$WeatherClockListAdapter;->context:Landroid/content/Context;

    .line 288
    iput-object p4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$WeatherClockListAdapter;->cityList:Ljava/util/ArrayList;

    .line 289
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v9, 0x0

    .line 299
    move-object v5, p2

    .line 300
    .local v5, "v":Landroid/view/View;
    if-nez v5, :cond_0

    .line 301
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$WeatherClockListAdapter;->context:Landroid/content/Context;

    const-string v8, "layout_inflater"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/LayoutInflater;

    .line 303
    .local v6, "vi":Landroid/view/LayoutInflater;
    const v7, 0x7f030031

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 305
    .end local v6    # "vi":Landroid/view/LayoutInflater;
    :cond_0
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$WeatherClockListAdapter;->cityList:Ljava/util/ArrayList;

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    .line 306
    .local v3, "item":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;
    if-eqz v3, :cond_1

    .line 307
    const v7, 0x7f080075

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 308
    .local v2, "currentLocation":Landroid/widget/ImageView;
    const v7, 0x7f080064

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 309
    .local v0, "cityName":Landroid/widget/TextView;
    const v7, 0x7f080065

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 310
    .local v1, "cityState":Landroid/widget/TextView;
    const v7, 0x7f080114

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RadioButton;

    .line 312
    .local v4, "radioButton":Landroid/widget/RadioButton;
    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v7

    const-string v8, "cityId:current"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 313
    invoke-virtual {v2, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 318
    :goto_0
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$WeatherClockListAdapter;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->defaultLocation:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 319
    const/4 v7, 0x1

    invoke-virtual {v4, v7}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 324
    :goto_1
    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getCityName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 325
    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getState()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 329
    .end local v0    # "cityName":Landroid/widget/TextView;
    .end local v1    # "cityState":Landroid/widget/TextView;
    .end local v2    # "currentLocation":Landroid/widget/ImageView;
    .end local v4    # "radioButton":Landroid/widget/RadioButton;
    :cond_1
    return-object v5

    .line 315
    .restart local v0    # "cityName":Landroid/widget/TextView;
    .restart local v1    # "cityState":Landroid/widget/TextView;
    .restart local v2    # "currentLocation":Landroid/widget/ImageView;
    .restart local v4    # "radioButton":Landroid/widget/RadioButton;
    :cond_2
    const/16 v7, 0x8

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 321
    :cond_3
    invoke-virtual {v4, v9}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_1
.end method
