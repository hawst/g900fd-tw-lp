.class Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$31;
.super Landroid/os/Handler;
.source "WeatherClockDetail.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 4008
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 4010
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->performMapCancled:Z

    if-nez v0, :cond_1

    .line 4011
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->performMapCancled:Z

    .line 4012
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$8100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 4013
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$8100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->clearResource()V

    .line 4014
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$8102(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    .line 4016
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->hideRefreshDialog()V
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$8400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    .line 4017
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->detailContext:Landroid/app/Activity;

    const v1, 0x7f0d001f

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    .line 4021
    :cond_1
    return-void
.end method
