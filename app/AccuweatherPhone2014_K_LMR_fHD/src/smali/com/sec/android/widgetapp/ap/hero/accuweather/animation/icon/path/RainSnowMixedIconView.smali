.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;
.super Landroid/view/View;
.source "RainSnowMixedIconView.java"

# interfaces
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;


# instance fields
.field private CloudTranslate:Landroid/animation/ValueAnimator;

.field private DropCenterT1:Landroid/animation/ValueAnimator;

.field private DropCenterT2:Landroid/animation/ValueAnimator;

.field private DropCenterT3:Landroid/animation/ValueAnimator;

.field private DropCenterT3_1:Landroid/animation/ValueAnimator;

.field private DropScale1:Landroid/animation/ValueAnimator;

.field private DropScale2:Landroid/animation/ValueAnimator;

.field private DropScale3:Landroid/animation/ValueAnimator;

.field private IconTranslate:Landroid/animation/ValueAnimator;

.field private RainDropT1:Landroid/animation/ValueAnimator;

.field private RainDropT1_1:Landroid/animation/ValueAnimator;

.field private RainDropT2:Landroid/animation/ValueAnimator;

.field isStop:Z

.field l:Landroid/animation/Animator$AnimatorListener;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mCloudLeft:Landroid/graphics/Path;

.field private mCloudRight:Landroid/graphics/Path;

.field private mCloudTop:Landroid/graphics/Path;

.field private mIsActiveAnimationThread:Z

.field private mMasking:Landroid/graphics/Path;

.field private mPaint:Landroid/graphics/Paint;

.field private mPaintColor:I

.field public mPreFixCT1:Ljava/lang/String;

.field public mPreFixCT2:Ljava/lang/String;

.field public mPreFixCT3:Ljava/lang/String;

.field public mPreFixCT3_1:Ljava/lang/String;

.field public mPreFixS1:Ljava/lang/String;

.field public mPreFixS2:Ljava/lang/String;

.field public mPreFixS3:Ljava/lang/String;

.field public mPreFixT1:Ljava/lang/String;

.field public mPreFixT1_1:Ljava/lang/String;

.field public mPreFixT2:Ljava/lang/String;

.field private mRainLine1:Landroid/graphics/Path;

.field private mRainLine2:Landroid/graphics/Path;

.field private mRainLine3:Landroid/graphics/Path;

.field private mScale:F

.field private mSnowCenter1:Landroid/graphics/Path;

.field private mSnowCenter2:Landroid/graphics/Path;

.field private mSnowCenter3:Landroid/graphics/Path;

.field private mSnowLeft1:Landroid/graphics/Path;

.field private mSnowLeft2:Landroid/graphics/Path;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 104
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 105
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 100
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 101
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 95
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    .line 31
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->IconTranslate:Landroid/animation/ValueAnimator;

    .line 33
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1:Landroid/animation/ValueAnimator;

    .line 35
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1_1:Landroid/animation/ValueAnimator;

    .line 37
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT2:Landroid/animation/ValueAnimator;

    .line 39
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale1:Landroid/animation/ValueAnimator;

    .line 41
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale2:Landroid/animation/ValueAnimator;

    .line 43
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale3:Landroid/animation/ValueAnimator;

    .line 45
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    .line 47
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    .line 49
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3:Landroid/animation/ValueAnimator;

    .line 51
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;

    .line 53
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mIsActiveAnimationThread:Z

    .line 55
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPaint:Landroid/graphics/Paint;

    .line 57
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mMasking:Landroid/graphics/Path;

    .line 59
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudLeft:Landroid/graphics/Path;

    .line 61
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudRight:Landroid/graphics/Path;

    .line 63
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudTop:Landroid/graphics/Path;

    .line 65
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine1:Landroid/graphics/Path;

    .line 67
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine2:Landroid/graphics/Path;

    .line 69
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine3:Landroid/graphics/Path;

    .line 71
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowCenter1:Landroid/graphics/Path;

    .line 73
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowCenter2:Landroid/graphics/Path;

    .line 75
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowCenter3:Landroid/graphics/Path;

    .line 77
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft1:Landroid/graphics/Path;

    .line 79
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft2:Landroid/graphics/Path;

    .line 81
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    .line 84
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mBitmap:Landroid/graphics/Bitmap;

    .line 88
    const v0, -0xa0a0b

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPaintColor:I

    .line 665
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixT1:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixT1_1:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixT2:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixS1:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixS2:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixS3:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixCT1:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixCT2:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixCT3:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixCT3_1:Ljava/lang/String;

    .line 675
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->l:Landroid/animation/Animator$AnimatorListener;

    .line 974
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->isStop:Z

    .line 96
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->init()V

    .line 97
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1_1:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->IconTranslate:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mIsActiveAnimationThread:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT2:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale1:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale2:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale3:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method private drawCloud(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "paint"    # Landroid/graphics/Paint;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 422
    const/4 v0, 0x0

    .line 423
    .local v0, "dx":F
    iget-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mIsActiveAnimationThread:Z

    if-eqz v1, :cond_0

    .line 424
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 426
    :cond_0
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPaintColor:I

    invoke-virtual {p2, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 428
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 429
    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 430
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudLeft:Landroid/graphics/Path;

    invoke-virtual {p1, v1, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 431
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 433
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 434
    invoke-virtual {p1, v2, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 435
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudTop:Landroid/graphics/Path;

    invoke-virtual {p1, v1, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 436
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 438
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 439
    neg-float v1, v0

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 440
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudRight:Landroid/graphics/Path;

    invoke-virtual {p1, v1, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 441
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 442
    return-void
.end method

.method private drawDrop(Landroid/graphics/Canvas;)V
    .locals 26
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 496
    const/4 v15, 0x0

    .line 497
    .local v15, "rainDy1":F
    const/4 v12, 0x0

    .line 498
    .local v12, "rainDx1":F
    const/16 v16, 0x0

    .line 499
    .local v16, "rainDy1_1":F
    const/4 v13, 0x0

    .line 500
    .local v13, "rainDx1_1":F
    const/16 v17, 0x0

    .line 501
    .local v17, "rainDy2":F
    const/4 v14, 0x0

    .line 503
    .local v14, "rainDx2":F
    const/4 v5, 0x0

    .line 504
    .local v5, "dcenterx1":F
    const/4 v8, 0x0

    .line 505
    .local v8, "dcentery1":F
    const/4 v7, 0x0

    .line 506
    .local v7, "dcenterx3":F
    const/4 v10, 0x0

    .line 507
    .local v10, "dcentery3":F
    const/4 v6, 0x0

    .line 508
    .local v6, "dcenterx2":F
    const/4 v9, 0x0

    .line 511
    .local v9, "dcentery2":F
    const/high16 v18, 0x3f800000    # 1.0f

    .line 513
    .local v18, "scale1_2":F
    const/high16 v19, 0x3f800000    # 1.0f

    .line 514
    .local v19, "scale2_1":F
    const/high16 v20, 0x3f800000    # 1.0f

    .line 516
    .local v20, "scale2_2":F
    const/high16 v21, 0x3f800000    # 1.0f

    .line 517
    .local v21, "scale3_1":F
    const/high16 v22, 0x3f800000    # 1.0f

    .line 518
    .local v22, "scale3_2":F
    const/high16 v23, -0x3d600000    # -80.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v24, v0

    mul-float v11, v23, v24

    .line 520
    .local v11, "initYvalue":F
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mIsActiveAnimationThread:Z

    move/from16 v23, v0

    if-eqz v23, :cond_8

    .line 522
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1:Landroid/animation/ValueAnimator;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v23

    if-eqz v23, :cond_9

    .line 523
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1:Landroid/animation/ValueAnimator;

    move-object/from16 v23, v0

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixT1:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "y"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Float;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Float;->floatValue()F

    move-result v15

    .line 524
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1:Landroid/animation/ValueAnimator;

    move-object/from16 v23, v0

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixT1:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "x"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Float;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Float;->floatValue()F

    move-result v12

    .line 531
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1_1:Landroid/animation/ValueAnimator;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v23

    if-eqz v23, :cond_a

    .line 532
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1_1:Landroid/animation/ValueAnimator;

    move-object/from16 v23, v0

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixT1_1:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "y"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Float;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Float;->floatValue()F

    move-result v16

    .line 533
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1_1:Landroid/animation/ValueAnimator;

    move-object/from16 v23, v0

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixT1_1:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "x"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Float;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Float;->floatValue()F

    move-result v13

    .line 541
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT2:Landroid/animation/ValueAnimator;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v23

    if-eqz v23, :cond_b

    .line 542
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT2:Landroid/animation/ValueAnimator;

    move-object/from16 v23, v0

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixT2:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "y"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Float;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Float;->floatValue()F

    move-result v17

    .line 543
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT2:Landroid/animation/ValueAnimator;

    move-object/from16 v23, v0

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixT2:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "x"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Float;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Float;->floatValue()F

    move-result v14

    .line 551
    :cond_2
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v23

    if-eqz v23, :cond_c

    .line 552
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    move-object/from16 v23, v0

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixCT1:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "x"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Float;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Float;->floatValue()F

    move-result v5

    .line 553
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    move-object/from16 v23, v0

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixCT1:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "y"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Float;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Float;->floatValue()F

    move-result v8

    .line 560
    :cond_3
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v23

    if-eqz v23, :cond_d

    .line 561
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    move-object/from16 v23, v0

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixCT2:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "x"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Float;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Float;->floatValue()F

    move-result v6

    .line 562
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    move-object/from16 v23, v0

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixCT2:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "y"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Float;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Float;->floatValue()F

    move-result v9

    .line 570
    :cond_4
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3:Landroid/animation/ValueAnimator;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v23

    if-eqz v23, :cond_5

    .line 571
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3:Landroid/animation/ValueAnimator;

    move-object/from16 v23, v0

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixCT3:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "x"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Float;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Float;->floatValue()F

    move-result v7

    .line 572
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3:Landroid/animation/ValueAnimator;

    move-object/from16 v23, v0

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixCT3:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "y"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Float;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Float;->floatValue()F

    move-result v10

    .line 575
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale1:Landroid/animation/ValueAnimator;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v23

    if-eqz v23, :cond_6

    .line 576
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale1:Landroid/animation/ValueAnimator;

    move-object/from16 v23, v0

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixS1:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "scale_2"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Float;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Float;->floatValue()F

    move-result v18

    .line 579
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale1:Landroid/animation/ValueAnimator;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v23

    if-eqz v23, :cond_7

    .line 580
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale2:Landroid/animation/ValueAnimator;

    move-object/from16 v23, v0

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixS2:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "scale"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Float;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Float;->floatValue()F

    move-result v19

    .line 581
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale2:Landroid/animation/ValueAnimator;

    move-object/from16 v23, v0

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixS2:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "scale_2"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Float;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Float;->floatValue()F

    move-result v20

    .line 584
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale1:Landroid/animation/ValueAnimator;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v23

    if-eqz v23, :cond_8

    .line 585
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale3:Landroid/animation/ValueAnimator;

    move-object/from16 v23, v0

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixS3:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "scale"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Float;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Float;->floatValue()F

    move-result v21

    .line 586
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale3:Landroid/animation/ValueAnimator;

    move-object/from16 v23, v0

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixS3:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "scale_2"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Float;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Float;->floatValue()F

    move-result v22

    .line 589
    :cond_8
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 590
    const/16 v23, 0x0

    const/high16 v24, -0x3ee00000    # -10.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v25, v0

    mul-float v24, v24, v25

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 592
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 593
    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v15}, Landroid/graphics/Canvas;->translate(FF)V

    .line 594
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine1:Landroid/graphics/Path;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v24, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 595
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 597
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mIsActiveAnimationThread:Z

    move/from16 v23, v0

    if-eqz v23, :cond_f

    .line 598
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->isStop:Z

    move/from16 v23, v0

    if-eqz v23, :cond_e

    .line 599
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 600
    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v13, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 601
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine3:Landroid/graphics/Path;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v24, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 602
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 604
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 605
    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v14, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 606
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine2:Landroid/graphics/Path;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v24, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 607
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 629
    :goto_5
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 631
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 632
    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v8}, Landroid/graphics/Canvas;->translate(FF)V

    .line 635
    const v23, 0x42d83333    # 108.1f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v24, v0

    mul-float v23, v23, v24

    const v24, 0x4314570a    # 148.34f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v25, v0

    mul-float v24, v24, v25

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v18

    move/from16 v3, v23

    move/from16 v4, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 636
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowCenter1:Landroid/graphics/Path;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v24, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 637
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 639
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 640
    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v9}, Landroid/graphics/Canvas;->translate(FF)V

    .line 641
    const v23, 0x42c0f5c3    # 96.48f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v24, v0

    mul-float v23, v23, v24

    const v24, 0x4325ee14    # 165.93f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v25, v0

    mul-float v24, v24, v25

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v19

    move/from16 v3, v23

    move/from16 v4, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 642
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowCenter2:Landroid/graphics/Path;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v24, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 643
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 645
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 646
    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v10}, Landroid/graphics/Canvas;->translate(FF)V

    .line 647
    const v23, 0x42ab0f5c    # 85.53f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v24, v0

    mul-float v23, v23, v24

    const v24, 0x4338cccd    # 184.8f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v25, v0

    mul-float v24, v24, v25

    move-object/from16 v0, p1

    move/from16 v1, v22

    move/from16 v2, v22

    move/from16 v3, v23

    move/from16 v4, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 648
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowCenter3:Landroid/graphics/Path;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v24, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 649
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 651
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 652
    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v8}, Landroid/graphics/Canvas;->translate(FF)V

    .line 653
    const v23, 0x429c0f5c    # 78.03f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v24, v0

    mul-float v23, v23, v24

    const v24, 0x4312d47b    # 146.83f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v25, v0

    mul-float v24, v24, v25

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v21

    move/from16 v3, v23

    move/from16 v4, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 654
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft2:Landroid/graphics/Path;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v24, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 655
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 657
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 658
    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v9}, Landroid/graphics/Canvas;->translate(FF)V

    .line 659
    const v23, 0x426aae14    # 58.67f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v24, v0

    mul-float v23, v23, v24

    const v24, 0x4329147b    # 169.08f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v25, v0

    mul-float v24, v24, v25

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v20

    move/from16 v3, v23

    move/from16 v4, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 661
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft1:Landroid/graphics/Path;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v24, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 662
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 664
    return-void

    .line 526
    :cond_9
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->isStop:Z

    move/from16 v23, v0

    if-nez v23, :cond_0

    .line 527
    move v15, v11

    goto/16 :goto_0

    .line 535
    :cond_a
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->isStop:Z

    move/from16 v23, v0

    if-nez v23, :cond_1

    .line 536
    const/high16 v23, -0x3de00000    # -40.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v24, v0

    mul-float v16, v23, v24

    .line 537
    const/high16 v23, 0x41a00000    # 20.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v24, v0

    mul-float v13, v23, v24

    goto/16 :goto_1

    .line 545
    :cond_b
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->isStop:Z

    move/from16 v23, v0

    if-nez v23, :cond_2

    .line 546
    move/from16 v17, v11

    .line 547
    const/high16 v23, 0x41c80000    # 25.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v24, v0

    mul-float v14, v23, v24

    goto/16 :goto_2

    .line 555
    :cond_c
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->isStop:Z

    move/from16 v23, v0

    if-nez v23, :cond_3

    .line 556
    move v8, v11

    goto/16 :goto_3

    .line 564
    :cond_d
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->isStop:Z

    move/from16 v23, v0

    if-nez v23, :cond_4

    .line 565
    const/high16 v23, 0x42f00000    # 120.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v24, v0

    mul-float v9, v23, v24

    goto/16 :goto_4

    .line 609
    :cond_e
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 610
    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v13, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 611
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine1:Landroid/graphics/Path;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v24, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 612
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 614
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 615
    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v14, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 616
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine2:Landroid/graphics/Path;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v24, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 617
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    goto/16 :goto_5

    .line 621
    :cond_f
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 622
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine3:Landroid/graphics/Path;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v24, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 623
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 625
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 626
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine2:Landroid/graphics/Path;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v24, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 627
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    goto/16 :goto_5
.end method

.method private drawTransparentArea(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "paint"    # Landroid/graphics/Paint;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 416
    new-instance v0, Landroid/graphics/PorterDuffXfermode;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v1}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 417
    invoke-virtual {p1, p2, v2, v2, p3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 418
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 419
    return-void
.end method

.method private init()V
    .locals 4

    .prologue
    const/high16 v3, 0x43960000    # 300.0f

    .line 108
    invoke-virtual {p0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 109
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPaint:Landroid/graphics/Paint;

    .line 110
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->initPath()V

    .line 111
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPaintColor:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 112
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 113
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 114
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x40a00000    # 5.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 116
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mBitmap:Landroid/graphics/Bitmap;

    .line 118
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 119
    .local v0, "mMaskingCanvas":Landroid/graphics/Canvas;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mMasking:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 120
    return-void
.end method

.method private initPath()V
    .locals 13

    .prologue
    const v12, 0x42fa774c

    const/high16 v11, 0x42ec0000    # 118.0f

    const v10, 0x42ea8937

    const/high16 v9, 0x432d0000    # 173.0f

    const/high16 v8, 0x43260000    # 166.0f

    .line 123
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mMasking:Landroid/graphics/Path;

    .line 124
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x432647f0

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v11

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 125
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x421e5f3b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v11

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x41fc6042    # 31.547f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v11

    const v3, 0x41c82f1b    # 25.023f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42ded0e5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x41c82f1b    # 25.023f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42ceb8d5    # 103.361f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 128
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x41c82f1b    # 25.023f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42bea1cb

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x41fc6042    # 31.547f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42b18419

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x421e5f3b

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42b18419

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 130
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x422020c5

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42b18419

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4221dd2f    # 40.466f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42b18937

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42239581    # 40.896f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42b19c29    # 88.805f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 132
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x422247ae    # 40.57f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42aec000    # 87.375f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42219168

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42abc28f    # 85.88f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42219168

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42a8b3b6

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 134
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x42219168

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4292a6e9

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x424551ec    # 49.33f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4280c28f    # 64.38f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42716e98

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4280c28f    # 64.38f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 136
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x4271a3d7    # 60.41f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4280c28f    # 64.38f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4271d70a    # 60.46f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4280c5a2

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42720c4a

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4280c5a2

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 138
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x426e1fbe

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4289a5e3

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x426c1375

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4292dfbe

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x426c1375

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x429c4e56    # 78.153f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 140
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x426c1375

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a0b958    # 80.362f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42733d71    # 60.81f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42a44e56    # 82.153f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x427c1375

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42a44e56    # 82.153f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 142
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x428274bc

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a44e56    # 82.153f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x428609ba

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42a0b8d5    # 80.361f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x428609ba

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x429c4dd3    # 78.152f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 144
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x428609ba

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4287b74c

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x428b70a4    # 69.72f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x426a7ae1    # 58.62f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4294dcac    # 74.431f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x424bf3b6

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 146
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x42a0bcee

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4226ac08    # 41.668f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42b2dc29    # 89.43f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x420a27f0

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42c848b4

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x41f88937    # 31.067f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 148
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x42d0d917

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41edc49c    # 29.721f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42d9fa5e

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x41e76a7f    # 28.927f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42e3872b    # 113.764f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x41e62f1b    # 28.773f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 150
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x42e3a560

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41e62b02    # 28.771f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42e3c000    # 113.875f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x41e60831    # 28.754f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42e3dd2f    # 113.932f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x41e6020c    # 28.751f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 152
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x42e48d50    # 114.276f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41e5f3b6    # 28.744f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42e53c6a

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x41e5d4fe    # 28.729f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42e5ec08

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x41e5d4fe    # 28.729f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 154
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x4305220c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41e5d4fe    # 28.729f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4314ee14    # 148.93f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x421b4bc7

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431d1810

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4256cfdf    # 53.703f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 156
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x4313cd91

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x426046a8    # 56.069f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x430bac08

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4277872b    # 61.882f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43065f7d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x428c753f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 158
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x43053062

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42903021

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4305bd71    # 133.74f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4295224e

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43079ae1

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42978000    # 75.75f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 160
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x43084560

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42985810

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x430902d1

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4298befa

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4309befa

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4298befa

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 162
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x430b11ec    # 139.07f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4298befa

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x430c5d71

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42976f1b

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x430d2083

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42950831

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 164
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x4312b0a4    # 146.69f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42838000    # 65.75f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431c3375

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4272126f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43269168

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4272126f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 166
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x4326accd

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4272126f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4326c6e9

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4271fae1

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4326e1cb

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4271f2b0    # 60.487f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 168
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x43367439

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42733a5e    # 60.807f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4342f9db

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42932d91

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4342f9db

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42b27852    # 89.235f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 170
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x4342fa1d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42d228f6    # 105.08f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4336220c

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v11

    const v5, 0x432647f0

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v11

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 172
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mMasking:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 173
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mMasking:Landroid/graphics/Path;

    const v1, -0x3ed17139

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x434f526b

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 174
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x437adfdd

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x434f526b

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 175
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x437b6160

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43941fc2

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 176
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mMasking:Landroid/graphics/Path;

    const v1, -0x3ed62f72

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43951fe0

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 177
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mMasking:Landroid/graphics/Path;

    const v1, -0x3ed17139

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x434f526b

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 179
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudTop:Landroid/graphics/Path;

    .line 180
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudTop:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 181
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x42733646

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x427eef9e

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 182
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x426f5a1d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42883efa

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x426d52f2

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42915b23

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x426d52f2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x429aac8b

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 184
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x426d52f2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x429f12f2

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42747df4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42a2a24e

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x427d52f2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42a2a24e

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 186
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x4283147b    # 65.54f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a2a24e

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4286a979

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x429f10e5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4286a979

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x429aaa7f    # 77.333f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 188
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x4286a979

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42862c8b

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x428c0f5c    # 70.03f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4267926f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42957a5e

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42492b02    # 50.292f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 190
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x42a15d2f    # 80.682f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42241581    # 41.021f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42b3820c

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4207ac08    # 33.918f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42c8f021

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x41f3b852    # 30.465f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 192
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x42d18831

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41e90419    # 29.127f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42dbb958    # 109.862f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x41e124dd    # 28.143f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42e6a2d1    # 115.318f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x41e124dd    # 28.143f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 194
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x4304bf3b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41e124dd    # 28.143f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431549ba

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4218c6a8    # 38.194f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431d72f2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4253f7cf    # 52.992f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 196
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x431d6000    # 157.375f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42540b44

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431efd2f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4262b74c    # 56.679f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431eea3d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4262cac1

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 198
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x43277e35

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x425e51ec    # 55.58f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 199
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x4325c042

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x424ee873

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 200
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x431c79db

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4202c083    # 32.688f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4308f687

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x41a1d70a    # 20.23f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42e6a2d1    # 115.318f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x41a1d70a    # 20.23f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 202
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x42e11810

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41a1d70a    # 20.23f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42db92f2

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x41a3645a    # 20.424f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42d61c29    # 107.055f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x41a6a5e3    # 20.831f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 204
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x42bcfbe7

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41b42d0e    # 22.522f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42a67333

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x41de70a4    # 27.805f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42954419

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x421425e3

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 206
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x428d2d0e

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4225820c

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42869b23

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x423954fe    # 46.333f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4281a24e

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x424efcee

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 208
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x42805b23

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42543f7d    # 53.062f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x427c0419

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x425ef8d5    # 55.743f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const/high16 v5, 0x427c0000    # 63.0f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x425f051f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 210
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x4277ee98

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x426b6560    # 58.849f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42733646

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x427eef9e

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42733646

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x427eef9e

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 212
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudTop:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 214
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudLeft:Landroid/graphics/Path;

    .line 215
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudLeft:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 216
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x421f9fbe

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v10

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 217
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x41fee148    # 31.86f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v10

    const v3, 0x41cab021    # 25.336f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42dd25e3

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x41cab021    # 25.336f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42cd2042

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 219
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x41cab021    # 25.336f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42bd1ba6    # 94.554f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x41fee148    # 31.86f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42afeb02    # 87.959f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x421f9fbe

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42afeb02    # 87.959f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 221
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x42216148

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42afeb02    # 87.959f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42231db2    # 40.779f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42afdf3b

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4224d604    # 41.209f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42aff333

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 223
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x4223872b    # 40.882f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42ad199a    # 86.55f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4222d1ec

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42aa178d

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4222d1ec

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42a70ac1

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 225
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x4222d1ec

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4291178d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42469168

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x427e872b    # 63.632f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4272ae14    # 60.67f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x427e872b    # 63.632f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 227
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x4272e354    # 60.722f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x427e872b    # 63.632f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4282e148    # 65.44f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42805f3b

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4282e148    # 65.44f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42805f3b

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 229
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x4286ad0e

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42606f9e

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 230
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x42880ccd

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42606042

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4275ced9    # 61.452f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x425ea1cb    # 55.658f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4272af1b

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x425ea1cb    # 55.658f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 232
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x4238570a    # 46.085f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x425ea1cb    # 55.658f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42084ac1

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4285a8f6    # 66.83f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42034396

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42a21062

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 234
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x41beae14    # 23.835f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a7fdf4

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x418ab439    # 17.338f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42b946a8    # 92.638f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x418ab439    # 17.338f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42cd1fbe

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 236
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x418ab439    # 17.338f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42e5e76d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x41dbb22d    # 27.462f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v12

    const v5, 0x421fa1cb    # 39.908f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v12

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 238
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x431c374c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v12

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 239
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x431c374c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42ea88b4

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 240
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x421f9fbe

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42ea88b4

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 241
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudLeft:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 243
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudRight:Landroid/graphics/Path;

    .line 244
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudRight:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 245
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x43267ba6

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x424f20c5

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 246
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x43263687

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x424f20c5

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4321d5c3

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x424f51ec    # 51.83f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431d3aa0

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42543646

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 248
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x43140b02    # 148.043f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x425df6c9

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x430bfc6a

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4274cbc7

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4306bae1    # 134.73f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x428ae3d7    # 69.445f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 250
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x43058b44

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x428e9aa0

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43061852

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4293872b    # 73.764f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4307f5c3    # 135.96f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4295e24e

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 252
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x4308a083

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4296b9db

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43095df4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x429720c5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x430a19db

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x429720c5

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 254
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x430b6ccd

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x429720c5

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x430cb810

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4295d168

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x430d7be7

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42936d91

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 256
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x43130c08

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4281f852    # 64.985f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431b5efa

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x426efae1

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43273e35

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x426efae1

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 258
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x4336cfdf

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42704396

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4343553f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4291d687

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4343553f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42b0fe77

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 260
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x4343553f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42d08bc7

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43367cee

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v10

    const v5, 0x4326a354    # 166.638f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v10

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 262
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x42775a1d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v10

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 263
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x42775a1d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42fa77cf

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 264
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x4326a354    # 166.638f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42fa77cf

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 265
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x4326a354    # 166.638f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v12

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 266
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x433adf3b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v12

    const v3, 0x434b54bc

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42d949ba

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x434b54bc

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42b0fefa

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 268
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x434b54bc

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4288b439

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x433ab78d

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x424f20c5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43267ba6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x424f20c5

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 270
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudRight:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 272
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine1:Landroid/graphics/Path;

    .line 274
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine1:Landroid/graphics/Path;

    const v1, 0x42fefdf4

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432d4000    # 173.25f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 275
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine1:Landroid/graphics/Path;

    const v1, 0x4310c560

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x431545e3

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 276
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine1:Landroid/graphics/Path;

    const v1, 0x43120fdf

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43137ae1    # 147.48f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4311a7f0

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4310fae1    # 144.98f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x430fdcee

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x430fb062

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 278
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine1:Landroid/graphics/Path;

    const v1, 0x430e12f2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x430e6666    # 142.4f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x430b926f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x430ecdd3    # 142.804f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x430a47f0

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x431098d5    # 144.597f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 280
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine1:Landroid/graphics/Path;

    const v1, 0x42f20312

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432892f2

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 281
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine1:Landroid/graphics/Path;

    const v1, 0x42ef6e14

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432a5db2

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42f03df4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x432cddb2

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42f3d3f8

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x432e2831

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 283
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine1:Landroid/graphics/Path;

    const v1, 0x42f53df4

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432eaac1

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42f6dfbe

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x432ee979

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42f87efa

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x432ee979

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 285
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine1:Landroid/graphics/Path;

    const v1, 0x42fafae1    # 125.49f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432ee979

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42fd6e14

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x432e55c3

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42fefdf4

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x432d4000    # 173.25f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 288
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine3:Landroid/graphics/Path;

    .line 290
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine3:Landroid/graphics/Path;

    const v1, 0x432f5604

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x430d36c9

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 291
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine3:Landroid/graphics/Path;

    const v1, 0x432d8c08

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x430bec8b

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x432b0b85

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x430c53f8

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4329c106

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x430e1efa

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 293
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine3:Landroid/graphics/Path;

    const v1, 0x431c5df4

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4320b3f8

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 294
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine3:Landroid/graphics/Path;

    const v1, 0x431b12f2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43227eb8

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431b7ae1    # 155.48f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4324feb8

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431d45e3

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43264937

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 296
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine3:Landroid/graphics/Path;

    const v1, 0x431dfae1    # 157.98f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4326cbc7

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431ecc08

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43270a7f    # 167.041f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431f9be7

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43270a7f    # 167.041f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 298
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine3:Landroid/graphics/Path;

    const v1, 0x4320d9db

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43270a7f    # 167.041f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43221375

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x432676c9

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4322db64

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43256106

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 300
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine3:Landroid/graphics/Path;

    const v1, 0x43303e77

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4312cc08

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 301
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine3:Landroid/graphics/Path;

    const v1, 0x433188f6

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43110148

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43312106

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x430e8148

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x432f5604

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x430d36c9

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 304
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine2:Landroid/graphics/Path;

    .line 306
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine2:Landroid/graphics/Path;

    const v1, 0x43142312

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4327c9ba

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 307
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine2:Landroid/graphics/Path;

    const v1, 0x43125917

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43267f7d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x430fd893

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4326e6e9

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x430e8d91

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4328b1ec

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 309
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine2:Landroid/graphics/Path;

    const v1, 0x42fe1604    # 127.043f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x433e3810

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 310
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine2:Landroid/graphics/Path;

    const v1, 0x42fb8106

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x434002d1

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42fc50e5

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x434282d1

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42ffe6e9

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4343cd50

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 312
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine2:Landroid/graphics/Path;

    const v1, 0x4300a873

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43444fdf

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43017958    # 129.474f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43448ed9

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x430248f6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43448ed9

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 314
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine2:Landroid/graphics/Path;

    const v1, 0x430386e9

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43448ed9

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4304c083

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4343fae1    # 195.98f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43058873

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4342e560

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 316
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine2:Landroid/graphics/Path;

    const v1, 0x43150b02    # 149.043f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432d5efa

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 317
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine2:Landroid/graphics/Path;

    const v1, 0x43165604

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432b9439

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4315ee14    # 149.93f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43291439

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43142312

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4327c9ba

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 321
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowCenter1:Landroid/graphics/Path;

    .line 322
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowCenter1:Landroid/graphics/Path;

    const v1, 0x42d826e9

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x430bf2b0

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 323
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowCenter1:Landroid/graphics/Path;

    const v1, 0x42d09687

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x430bf2b0

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42ca753f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x430f045a

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42ca753f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4312cc8b

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 325
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowCenter1:Landroid/graphics/Path;

    const v1, 0x42ca753f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43169375

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42d09604    # 104.293f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4319a5a2

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42d826e9

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4319a5a2

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 327
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowCenter1:Landroid/graphics/Path;

    const v1, 0x42dfb6c9

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4319a5a2

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42e5d70a    # 114.92f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x431693f8

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42e5d70a    # 114.92f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4312cc8b

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 329
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowCenter1:Landroid/graphics/Path;

    const v1, 0x42e5d70a    # 114.92f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x430f051f    # 143.02f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42dfb6c9

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x430bf2b0

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42d826e9

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x430bf2b0

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 332
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowCenter2:Landroid/graphics/Path;

    .line 334
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowCenter2:Landroid/graphics/Path;

    const v1, 0x42c0dcac    # 96.431f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43207062

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 335
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowCenter2:Landroid/graphics/Path;

    const v1, 0x42bad1ec    # 93.41f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43207062

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42b5e76d

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4322e560

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42b5e76d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4325eb02    # 165.918f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 337
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowCenter2:Landroid/graphics/Path;

    const v1, 0x42b5e76d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4328f0a4    # 168.94f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42bad26f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x432b65a2

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42c0dcac    # 96.431f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x432b65a2

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 339
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowCenter2:Landroid/graphics/Path;

    const v1, 0x42c6e6e9

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432b65a2

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42cbd1ec    # 101.91f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4328f0a4    # 168.94f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42cbd1ec    # 101.91f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4325eb02    # 165.918f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 341
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowCenter2:Landroid/graphics/Path;

    const v1, 0x42cbd26f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4322e560

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42c6e76d

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43207062

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42c0dcac    # 96.431f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43207062

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 344
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowCenter3:Landroid/graphics/Path;

    .line 346
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowCenter3:Landroid/graphics/Path;

    const v1, 0x42aabb64

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4331db64

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 347
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowCenter3:Landroid/graphics/Path;

    const v1, 0x42a32c8b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4331db64

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x429d072b    # 78.514f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4334edd3    # 180.929f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x429d072b    # 78.514f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4338b4fe    # 184.707f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 349
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowCenter3:Landroid/graphics/Path;

    const v1, 0x429d072b    # 78.514f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x433c7c29    # 188.485f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42a32c08

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x433f8e14

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42aabb64

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x433f8e14

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 351
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowCenter3:Landroid/graphics/Path;

    const v1, 0x42b24831

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x433f8e14

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42b86e98

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x433c7c29    # 188.485f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42b86e98

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4338b4fe    # 184.707f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 353
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowCenter3:Landroid/graphics/Path;

    const v1, 0x42b86e98

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4334edd3    # 180.929f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42b24831

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4331db64

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42aabb64

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4331db64

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 357
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft1:Landroid/graphics/Path;

    .line 359
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft1:Landroid/graphics/Path;

    const v1, 0x428e5f3b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 360
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft1:Landroid/graphics/Path;

    const v1, 0x42813a5e

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 361
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft1:Landroid/graphics/Path;

    const v1, 0x4288276d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x431fddf4

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 362
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft1:Landroid/graphics/Path;

    const v1, 0x428a1687

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x431e3168

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4288f0a4    # 68.47f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x431bfbe7

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4285978d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x431b045a

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 364
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft1:Landroid/graphics/Path;

    const v1, 0x42823f7d    # 65.124f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x431a0d0e

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x427bed91

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x431a970a    # 154.59f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42780f5c    # 62.015f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x431c43d7    # 156.265f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 366
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft1:Landroid/graphics/Path;

    const v1, 0x426972b0    # 58.362f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432293b6

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 367
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft1:Landroid/graphics/Path;

    const v1, 0x425ae148    # 54.72f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x431c4312

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 368
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft1:Landroid/graphics/Path;

    const v1, 0x42570312

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x431a9646

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x424e72b0    # 51.612f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x431a028f    # 154.01f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4247c189    # 49.939f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x431afa1d

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 370
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft1:Landroid/graphics/Path;

    const v1, 0x42410f5c    # 48.265f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x431bf1aa    # 155.944f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x423ec396

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x431e374c

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4242a1cb    # 48.658f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x431fe419

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 372
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft1:Landroid/graphics/Path;

    const v1, 0x42506f9e

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 373
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft1:Landroid/graphics/Path;

    const v1, 0x4234bf7d    # 45.187f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 374
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft1:Landroid/graphics/Path;

    const v1, 0x422d0419

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v8

    const v3, 0x4226bf7d    # 41.687f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x432790e5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4226bf7d    # 41.687f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43298000    # 169.5f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 376
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft1:Landroid/graphics/Path;

    const v1, 0x4226bf7d    # 41.687f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432b6f1b

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x422d0419

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v9

    const v5, 0x4234bf7d    # 45.187f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v9

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 378
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft1:Landroid/graphics/Path;

    const v1, 0x4251f5c3    # 52.49f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v9

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 379
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft1:Landroid/graphics/Path;

    const v1, 0x4242b74c    # 48.679f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4333774c

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 380
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft1:Landroid/graphics/Path;

    const v1, 0x423ed917

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43352419

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x424124dd    # 48.286f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43373687

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4247d70a    # 49.96f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43382e14    # 184.18f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 382
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft1:Landroid/graphics/Path;

    const v1, 0x424a0b44

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43387f7d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x424c73b6

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43389df4

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x424ed3f8    # 51.707f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43389df4

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 384
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft1:Landroid/graphics/Path;

    const v1, 0x4253a9fc    # 52.916f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43389df4

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42585f3b

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4337f893

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x425af6c9

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4336d917

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 386
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft1:Landroid/graphics/Path;

    const v1, 0x426970a4    # 58.36f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4330922d    # 176.571f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 387
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft1:Landroid/graphics/Path;

    const v1, 0x4277f7cf    # 61.992f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4336db23

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 388
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft1:Landroid/graphics/Path;

    const v1, 0x427a8f5c    # 62.64f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4337faa0

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x427f449c

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43389b23

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42820d50    # 65.026f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43389b23

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 390
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft1:Landroid/graphics/Path;

    const v1, 0x42833d71    # 65.62f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43389b23

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x428471aa    # 66.222f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43387439

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42858bc7

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4338224e

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 392
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft1:Landroid/graphics/Path;

    const v1, 0x4288e4dd

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43372ac1

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x428a0ac1

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43352937

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42881ba6    # 68.054f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43337cee

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 394
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft1:Landroid/graphics/Path;

    const v1, 0x428076c9

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v9

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 395
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft1:Landroid/graphics/Path;

    const v1, 0x428e5f3b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v9

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 396
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft1:Landroid/graphics/Path;

    const v1, 0x42923cee

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v9

    const v3, 0x42955f3b

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x432b6f1b

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42955f3b

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43298000    # 169.5f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 398
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft1:Landroid/graphics/Path;

    const v1, 0x42955f3b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432790e5

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42923cee

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v8

    const v5, 0x428e5f3b

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v8

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 401
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft2:Landroid/graphics/Path;

    .line 403
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft2:Landroid/graphics/Path;

    const v1, 0x42a6276d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4312cc8b

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 404
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft2:Landroid/graphics/Path;

    const v1, 0x42a6276d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4315a2fb

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42a18da4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4317efdf

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x429be0c5

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4317efdf

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 406
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft2:Landroid/graphics/Path;

    const v1, 0x429633e6

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4317efdf

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42919a1d

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4315a2fb

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42919a1d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4312cc8b

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 408
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft2:Landroid/graphics/Path;

    const v1, 0x42919a1d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x430ff61b

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x429633e6

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x430da937

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x429be0c5

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x430da937

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 410
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft2:Landroid/graphics/Path;

    const v1, 0x42a18da4

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x430da937

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42a6276d

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x430ff61b

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42a6276d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4312cc8b

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 413
    return-void
.end method

.method private makeAnimation()V
    .locals 31

    .prologue
    .line 725
    const-wide/16 v5, 0x320

    .line 726
    .local v5, "duration":J
    const-wide/16 v24, 0x4

    div-long v3, v5, v24

    .line 728
    .local v3, "delayGap":J
    const/16 v24, 0x4

    move/from16 v0, v24

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const-string v26, "y"

    const/16 v27, 0x2

    move/from16 v0, v27

    new-array v0, v0, [F

    move-object/from16 v27, v0

    const/16 v28, 0x0

    const/high16 v29, -0x3de00000    # -40.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    const/16 v28, 0x1

    const/high16 v29, 0x42c80000    # 100.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    .line 729
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x1

    const-string v26, "x"

    const/16 v27, 0x2

    move/from16 v0, v27

    new-array v0, v0, [F

    move-object/from16 v27, v0

    const/16 v28, 0x0

    const/high16 v29, 0x41f00000    # 30.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    const/16 v28, 0x1

    const/high16 v29, -0x3de00000    # -40.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    .line 730
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x2

    const-string v26, "e_y"

    const/16 v27, 0x2

    move/from16 v0, v27

    new-array v0, v0, [F

    move-object/from16 v27, v0

    const/16 v28, 0x0

    const/high16 v29, -0x3de00000    # -40.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    const/16 v28, 0x1

    const/16 v29, 0x0

    aput v29, v27, v28

    .line 731
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x3

    const-string v26, "e_x"

    const/16 v27, 0x2

    move/from16 v0, v27

    new-array v0, v0, [F

    move-object/from16 v27, v0

    const/16 v28, 0x0

    const/high16 v29, 0x41f00000    # 30.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    const/16 v28, 0x1

    const/16 v29, 0x0

    aput v29, v27, v28

    .line 732
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    .line 728
    invoke-static/range {v24 .. v24}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1:Landroid/animation/ValueAnimator;

    .line 734
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    const/16 v25, -0x1

    invoke-virtual/range {v24 .. v25}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 735
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v5, v6}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 736
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v4}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 737
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    new-instance v25, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct/range {v25 .. v25}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual/range {v24 .. v25}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 738
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 739
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/animation/ValueAnimator;->start()V

    .line 741
    const/16 v24, 0x4

    move/from16 v0, v24

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const-string v26, "y"

    const/16 v27, 0x2

    move/from16 v0, v27

    new-array v0, v0, [F

    move-object/from16 v27, v0

    const/16 v28, 0x0

    const/high16 v29, -0x3de00000    # -40.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    const/16 v28, 0x1

    const/high16 v29, 0x42c80000    # 100.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    .line 742
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x1

    const-string v26, "x"

    const/16 v27, 0x2

    move/from16 v0, v27

    new-array v0, v0, [F

    move-object/from16 v27, v0

    const/16 v28, 0x0

    const/high16 v29, 0x41f00000    # 30.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    const/16 v28, 0x1

    const/high16 v29, -0x3de00000    # -40.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    .line 743
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x2

    const-string v26, "e_y"

    const/16 v27, 0x2

    move/from16 v0, v27

    new-array v0, v0, [F

    move-object/from16 v27, v0

    const/16 v28, 0x0

    const/high16 v29, -0x3de00000    # -40.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    const/16 v28, 0x1

    const/16 v29, 0x0

    aput v29, v27, v28

    .line 744
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x3

    const-string v26, "e_x"

    const/16 v27, 0x2

    move/from16 v0, v27

    new-array v0, v0, [F

    move-object/from16 v27, v0

    const/16 v28, 0x0

    const/high16 v29, 0x41f00000    # 30.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    const/16 v28, 0x1

    const/16 v29, 0x0

    aput v29, v27, v28

    .line 745
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    .line 741
    invoke-static/range {v24 .. v24}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1_1:Landroid/animation/ValueAnimator;

    .line 747
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1_1:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    const/16 v25, -0x1

    invoke-virtual/range {v24 .. v25}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 748
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1_1:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v5, v6}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 749
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1_1:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    const-wide/16 v25, 0x3

    mul-long v25, v25, v3

    invoke-virtual/range {v24 .. v26}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 750
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1_1:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    new-instance v25, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct/range {v25 .. v25}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual/range {v24 .. v25}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 751
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1_1:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 752
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1_1:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/animation/ValueAnimator;->start()V

    .line 755
    const/16 v24, 0x4

    move/from16 v0, v24

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const-string v26, "y"

    const/16 v27, 0x2

    move/from16 v0, v27

    new-array v0, v0, [F

    move-object/from16 v27, v0

    const/16 v28, 0x0

    const/high16 v29, -0x3de00000    # -40.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    const/16 v28, 0x1

    const/high16 v29, 0x42c80000    # 100.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    .line 756
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x1

    const-string v26, "x"

    const/16 v27, 0x2

    move/from16 v0, v27

    new-array v0, v0, [F

    move-object/from16 v27, v0

    const/16 v28, 0x0

    const/high16 v29, 0x41f00000    # 30.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    const/16 v28, 0x1

    const/high16 v29, -0x3de00000    # -40.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    .line 757
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x2

    const-string v26, "e_y"

    const/16 v27, 0x2

    move/from16 v0, v27

    new-array v0, v0, [F

    move-object/from16 v27, v0

    const/16 v28, 0x0

    const/high16 v29, -0x3de00000    # -40.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    const/16 v28, 0x1

    const/16 v29, 0x0

    aput v29, v27, v28

    .line 758
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x3

    const-string v26, "e_x"

    const/16 v27, 0x2

    move/from16 v0, v27

    new-array v0, v0, [F

    move-object/from16 v27, v0

    const/16 v28, 0x0

    const/high16 v29, 0x41f00000    # 30.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    const/16 v28, 0x1

    const/16 v29, 0x0

    aput v29, v27, v28

    .line 759
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    .line 755
    invoke-static/range {v24 .. v24}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT2:Landroid/animation/ValueAnimator;

    .line 761
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT2:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    const/16 v25, -0x1

    invoke-virtual/range {v24 .. v25}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 762
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT2:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v5, v6}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 763
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT2:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    const-wide/16 v25, 0x4

    mul-long v25, v25, v3

    invoke-virtual/range {v24 .. v26}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 764
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT2:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    new-instance v25, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct/range {v25 .. v25}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual/range {v24 .. v25}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 765
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT2:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 766
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT2:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/animation/ValueAnimator;->start()V

    .line 768
    const-wide/16 v22, 0x3e8

    .line 769
    .local v22, "snowduration":J
    const-wide/16 v24, 0x3

    div-long v20, v5, v24

    .line 771
    .local v20, "snowdelayGap":J
    const/16 v24, 0x0

    const/high16 v25, 0x3f800000    # 1.0f

    invoke-static/range {v24 .. v25}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v8

    .line 772
    .local v8, "key1":Landroid/animation/Keyframe;
    const v24, 0x3e99999a    # 0.3f

    const/high16 v25, 0x3fc00000    # 1.5f

    invoke-static/range {v24 .. v25}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v10

    .line 773
    .local v10, "key2":Landroid/animation/Keyframe;
    const v24, 0x3f19999a    # 0.6f

    const v25, 0x3f4ccccd    # 0.8f

    invoke-static/range {v24 .. v25}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v12

    .line 774
    .local v12, "key3":Landroid/animation/Keyframe;
    const v24, 0x3f666666    # 0.9f

    const/high16 v25, 0x3fc00000    # 1.5f

    invoke-static/range {v24 .. v25}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v14

    .line 775
    .local v14, "key4":Landroid/animation/Keyframe;
    const v24, 0x3f733333    # 0.95f

    const/16 v25, 0x0

    invoke-static/range {v24 .. v25}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v16

    .line 776
    .local v16, "key5":Landroid/animation/Keyframe;
    const/high16 v24, 0x3f800000    # 1.0f

    const/16 v25, 0x0

    invoke-static/range {v24 .. v25}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v18

    .line 777
    .local v18, "key6":Landroid/animation/Keyframe;
    const/16 v24, 0x0

    const/high16 v25, 0x3f800000    # 1.0f

    invoke-static/range {v24 .. v25}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v9

    .line 778
    .local v9, "key1_":Landroid/animation/Keyframe;
    const v24, 0x3e99999a    # 0.3f

    const/high16 v25, 0x3f000000    # 0.5f

    invoke-static/range {v24 .. v25}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v11

    .line 779
    .local v11, "key2_":Landroid/animation/Keyframe;
    const v24, 0x3f19999a    # 0.6f

    const v25, 0x3f99999a    # 1.2f

    invoke-static/range {v24 .. v25}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v13

    .line 780
    .local v13, "key3_":Landroid/animation/Keyframe;
    const v24, 0x3f666666    # 0.9f

    const/high16 v25, 0x3f000000    # 0.5f

    invoke-static/range {v24 .. v25}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v15

    .line 781
    .local v15, "key4_":Landroid/animation/Keyframe;
    const v24, 0x3f733333    # 0.95f

    const/16 v25, 0x0

    invoke-static/range {v24 .. v25}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v17

    .line 782
    .local v17, "key5_":Landroid/animation/Keyframe;
    const/high16 v24, 0x3f800000    # 1.0f

    const/16 v25, 0x0

    invoke-static/range {v24 .. v25}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v19

    .line 783
    .local v19, "key6_":Landroid/animation/Keyframe;
    const v24, 0x3e99999a    # 0.3f

    const/high16 v25, 0x3f800000    # 1.0f

    invoke-static/range {v24 .. v25}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v7

    .line 784
    .local v7, "endKey":Landroid/animation/Keyframe;
    const/16 v24, 0x4

    move/from16 v0, v24

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const-string v26, "scale"

    const/16 v27, 0x6

    move/from16 v0, v27

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    aput-object v8, v27, v28

    const/16 v28, 0x1

    aput-object v10, v27, v28

    const/16 v28, 0x2

    aput-object v12, v27, v28

    const/16 v28, 0x3

    aput-object v14, v27, v28

    const/16 v28, 0x4

    aput-object v16, v27, v28

    const/16 v28, 0x5

    aput-object v18, v27, v28

    .line 785
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x1

    const-string v26, "e_scale"

    const/16 v27, 0x2

    move/from16 v0, v27

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    aput-object v8, v27, v28

    const/16 v28, 0x1

    aput-object v7, v27, v28

    .line 786
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x2

    const-string v26, "scale_2"

    const/16 v27, 0x6

    move/from16 v0, v27

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    aput-object v9, v27, v28

    const/16 v28, 0x1

    aput-object v11, v27, v28

    const/16 v28, 0x2

    aput-object v13, v27, v28

    const/16 v28, 0x3

    aput-object v15, v27, v28

    const/16 v28, 0x4

    aput-object v17, v27, v28

    const/16 v28, 0x5

    aput-object v19, v27, v28

    .line 787
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x3

    const-string v26, "e_scale_2"

    const/16 v27, 0x2

    move/from16 v0, v27

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    aput-object v9, v27, v28

    const/16 v28, 0x1

    aput-object v7, v27, v28

    .line 788
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    .line 784
    invoke-static/range {v24 .. v24}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale1:Landroid/animation/ValueAnimator;

    .line 790
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale1:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    const/16 v25, -0x1

    invoke-virtual/range {v24 .. v25}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 791
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale1:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 792
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale1:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    const-wide/16 v25, 0x2

    mul-long v25, v25, v20

    invoke-virtual/range {v24 .. v26}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 793
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale1:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    new-instance v25, Landroid/view/animation/LinearInterpolator;

    invoke-direct/range {v25 .. v25}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual/range {v24 .. v25}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 794
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale1:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 795
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale1:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/animation/ValueAnimator;->start()V

    .line 797
    const/16 v24, 0x4

    move/from16 v0, v24

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const-string v26, "scale"

    const/16 v27, 0x6

    move/from16 v0, v27

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    aput-object v8, v27, v28

    const/16 v28, 0x1

    aput-object v10, v27, v28

    const/16 v28, 0x2

    aput-object v12, v27, v28

    const/16 v28, 0x3

    aput-object v14, v27, v28

    const/16 v28, 0x4

    aput-object v16, v27, v28

    const/16 v28, 0x5

    aput-object v18, v27, v28

    .line 798
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x1

    const-string v26, "e_scale"

    const/16 v27, 0x2

    move/from16 v0, v27

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    aput-object v8, v27, v28

    const/16 v28, 0x1

    aput-object v7, v27, v28

    .line 799
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x2

    const-string v26, "scale_2"

    const/16 v27, 0x6

    move/from16 v0, v27

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    aput-object v9, v27, v28

    const/16 v28, 0x1

    aput-object v11, v27, v28

    const/16 v28, 0x2

    aput-object v13, v27, v28

    const/16 v28, 0x3

    aput-object v15, v27, v28

    const/16 v28, 0x4

    aput-object v17, v27, v28

    const/16 v28, 0x5

    aput-object v19, v27, v28

    .line 800
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x3

    const-string v26, "e_scale_2"

    const/16 v27, 0x2

    move/from16 v0, v27

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    aput-object v9, v27, v28

    const/16 v28, 0x1

    aput-object v7, v27, v28

    .line 801
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    .line 797
    invoke-static/range {v24 .. v24}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale2:Landroid/animation/ValueAnimator;

    .line 802
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale2:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    const/16 v25, -0x1

    invoke-virtual/range {v24 .. v25}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 803
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale2:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 804
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale2:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 805
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale2:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    new-instance v25, Landroid/view/animation/LinearInterpolator;

    invoke-direct/range {v25 .. v25}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual/range {v24 .. v25}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 806
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale2:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 807
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale2:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/animation/ValueAnimator;->start()V

    .line 809
    const/16 v24, 0x4

    move/from16 v0, v24

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const-string v26, "scale"

    const/16 v27, 0x6

    move/from16 v0, v27

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    aput-object v8, v27, v28

    const/16 v28, 0x1

    aput-object v10, v27, v28

    const/16 v28, 0x2

    aput-object v12, v27, v28

    const/16 v28, 0x3

    aput-object v14, v27, v28

    const/16 v28, 0x4

    aput-object v16, v27, v28

    const/16 v28, 0x5

    aput-object v18, v27, v28

    .line 810
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x1

    const-string v26, "e_scale"

    const/16 v27, 0x2

    move/from16 v0, v27

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    aput-object v8, v27, v28

    const/16 v28, 0x1

    aput-object v7, v27, v28

    .line 811
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x2

    const-string v26, "scale_2"

    const/16 v27, 0x6

    move/from16 v0, v27

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    aput-object v9, v27, v28

    const/16 v28, 0x1

    aput-object v11, v27, v28

    const/16 v28, 0x2

    aput-object v13, v27, v28

    const/16 v28, 0x3

    aput-object v15, v27, v28

    const/16 v28, 0x4

    aput-object v17, v27, v28

    const/16 v28, 0x5

    aput-object v19, v27, v28

    .line 812
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x3

    const-string v26, "e_scale_2"

    const/16 v27, 0x2

    move/from16 v0, v27

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    aput-object v9, v27, v28

    const/16 v28, 0x1

    aput-object v7, v27, v28

    .line 813
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    .line 809
    invoke-static/range {v24 .. v24}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale3:Landroid/animation/ValueAnimator;

    .line 814
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale3:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    const/16 v25, -0x1

    invoke-virtual/range {v24 .. v25}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 815
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale3:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 816
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale3:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    new-instance v25, Landroid/view/animation/LinearInterpolator;

    invoke-direct/range {v25 .. v25}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual/range {v24 .. v25}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 817
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale3:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 818
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale3:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/animation/ValueAnimator;->start()V

    .line 821
    const/16 v24, 0x4

    move/from16 v0, v24

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const-string v26, "y"

    const/16 v27, 0x2

    move/from16 v0, v27

    new-array v0, v0, [F

    move-object/from16 v27, v0

    const/16 v28, 0x0

    const/high16 v29, -0x3e380000    # -25.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    const/16 v28, 0x1

    const/high16 v29, 0x42700000    # 60.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    .line 822
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x1

    const-string v26, "x"

    const/16 v27, 0x2

    move/from16 v0, v27

    new-array v0, v0, [F

    move-object/from16 v27, v0

    const/16 v28, 0x0

    const/high16 v29, 0x41200000    # 10.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    const/16 v28, 0x1

    const/high16 v29, -0x3de00000    # -40.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    .line 823
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x2

    const-string v26, "e_y"

    const/16 v27, 0x2

    move/from16 v0, v27

    new-array v0, v0, [F

    move-object/from16 v27, v0

    const/16 v28, 0x0

    const/high16 v29, -0x3e380000    # -25.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    const/16 v28, 0x1

    const/16 v29, 0x0

    aput v29, v27, v28

    .line 824
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x3

    const-string v26, "e_x"

    const/16 v27, 0x2

    move/from16 v0, v27

    new-array v0, v0, [F

    move-object/from16 v27, v0

    const/16 v28, 0x0

    const/high16 v29, 0x41200000    # 10.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    const/16 v28, 0x1

    const/16 v29, 0x0

    aput v29, v27, v28

    .line 825
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    .line 821
    invoke-static/range {v24 .. v24}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    .line 827
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    const/16 v25, -0x1

    invoke-virtual/range {v24 .. v25}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 828
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 829
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    const-wide/16 v25, 0x2

    mul-long v25, v25, v20

    invoke-virtual/range {v24 .. v26}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 830
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    new-instance v25, Landroid/view/animation/LinearInterpolator;

    invoke-direct/range {v25 .. v25}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual/range {v24 .. v25}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 831
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 832
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/animation/ValueAnimator;->start()V

    .line 834
    const/16 v24, 0x4

    move/from16 v0, v24

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const-string v26, "y"

    const/16 v27, 0x2

    move/from16 v0, v27

    new-array v0, v0, [F

    move-object/from16 v27, v0

    const/16 v28, 0x0

    const/high16 v29, -0x3d900000    # -60.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    const/16 v28, 0x1

    const/high16 v29, 0x42200000    # 40.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    .line 835
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x1

    const-string v26, "x"

    const/16 v27, 0x2

    move/from16 v0, v27

    new-array v0, v0, [F

    move-object/from16 v27, v0

    const/16 v28, 0x0

    const/high16 v29, 0x41a00000    # 20.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    const/16 v28, 0x1

    const/high16 v29, -0x3e100000    # -30.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    .line 836
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x2

    const-string v26, "e_y"

    const/16 v27, 0x2

    move/from16 v0, v27

    new-array v0, v0, [F

    move-object/from16 v27, v0

    const/16 v28, 0x0

    const/high16 v29, -0x3d900000    # -60.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    const/16 v28, 0x1

    const/16 v29, 0x0

    aput v29, v27, v28

    .line 837
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x3

    const-string v26, "e_x"

    const/16 v27, 0x2

    move/from16 v0, v27

    new-array v0, v0, [F

    move-object/from16 v27, v0

    const/16 v28, 0x0

    const/high16 v29, 0x41a00000    # 20.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    const/16 v28, 0x1

    const/16 v29, 0x0

    aput v29, v27, v28

    .line 838
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    .line 834
    invoke-static/range {v24 .. v24}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    .line 840
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    const/16 v25, -0x1

    invoke-virtual/range {v24 .. v25}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 841
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 842
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 843
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    new-instance v25, Landroid/view/animation/LinearInterpolator;

    invoke-direct/range {v25 .. v25}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual/range {v24 .. v25}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 844
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 845
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/animation/ValueAnimator;->start()V

    .line 847
    const/16 v24, 0x4

    move/from16 v0, v24

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const-string v26, "y"

    const/16 v27, 0x2

    move/from16 v0, v27

    new-array v0, v0, [F

    move-object/from16 v27, v0

    const/16 v28, 0x0

    const/high16 v29, -0x3d900000    # -60.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    const/16 v28, 0x1

    const/high16 v29, 0x41c80000    # 25.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    .line 848
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x1

    const-string v26, "x"

    const/16 v27, 0x2

    move/from16 v0, v27

    new-array v0, v0, [F

    move-object/from16 v27, v0

    const/16 v28, 0x0

    const/high16 v29, 0x41a00000    # 20.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    const/16 v28, 0x1

    const/high16 v29, -0x3e600000    # -20.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    .line 849
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x2

    const-string v26, "e_y"

    const/16 v27, 0x2

    move/from16 v0, v27

    new-array v0, v0, [F

    move-object/from16 v27, v0

    const/16 v28, 0x0

    const/high16 v29, -0x3d900000    # -60.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    const/16 v28, 0x1

    const/16 v29, 0x0

    aput v29, v27, v28

    .line 850
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x3

    const-string v26, "e_x"

    const/16 v27, 0x2

    move/from16 v0, v27

    new-array v0, v0, [F

    move-object/from16 v27, v0

    const/16 v28, 0x0

    const/high16 v29, 0x41a00000    # 20.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    const/16 v28, 0x1

    const/16 v29, 0x0

    aput v29, v27, v28

    .line 851
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    .line 847
    invoke-static/range {v24 .. v24}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3:Landroid/animation/ValueAnimator;

    .line 853
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    const/16 v25, -0x1

    invoke-virtual/range {v24 .. v25}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 854
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 855
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    new-instance v25, Landroid/view/animation/LinearInterpolator;

    invoke-direct/range {v25 .. v25}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual/range {v24 .. v25}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 856
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 857
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/animation/ValueAnimator;->start()V

    .line 859
    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [F

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const/high16 v26, 0x41f00000    # 30.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v27, v0

    mul-float v26, v26, v27

    aput v26, v24, v25

    const/16 v25, 0x1

    const/high16 v26, -0x3e600000    # -20.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v27, v0

    mul-float v26, v26, v27

    aput v26, v24, v25

    invoke-static/range {v24 .. v24}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;

    .line 860
    const/16 v24, 0x4

    move/from16 v0, v24

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const-string v26, "y"

    const/16 v27, 0x2

    move/from16 v0, v27

    new-array v0, v0, [F

    move-object/from16 v27, v0

    const/16 v28, 0x0

    const/high16 v29, -0x3db80000    # -50.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    const/16 v28, 0x1

    const/high16 v29, 0x42340000    # 45.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    .line 861
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x1

    const-string v26, "x"

    const/16 v27, 0x2

    move/from16 v0, v27

    new-array v0, v0, [F

    move-object/from16 v27, v0

    const/16 v28, 0x0

    const/high16 v29, 0x41f00000    # 30.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    const/16 v28, 0x1

    const/high16 v29, -0x3e600000    # -20.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    .line 862
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x2

    const-string v26, "e_y"

    const/16 v27, 0x2

    move/from16 v0, v27

    new-array v0, v0, [F

    move-object/from16 v27, v0

    const/16 v28, 0x0

    const/high16 v29, -0x3db80000    # -50.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    const/16 v28, 0x1

    const/16 v29, 0x0

    aput v29, v27, v28

    .line 863
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x3

    const-string v26, "e_x"

    const/16 v27, 0x2

    move/from16 v0, v27

    new-array v0, v0, [F

    move-object/from16 v27, v0

    const/16 v28, 0x0

    const/high16 v29, 0x41f00000    # 30.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v30, v0

    mul-float v29, v29, v30

    aput v29, v27, v28

    const/16 v28, 0x1

    const/16 v29, 0x0

    aput v29, v27, v28

    .line 864
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    .line 860
    invoke-static/range {v24 .. v24}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    .line 866
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    const/16 v25, -0x1

    invoke-virtual/range {v24 .. v25}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 867
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 868
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    const-wide/16 v25, 0x3

    mul-long v25, v25, v20

    invoke-virtual/range {v24 .. v26}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 869
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    new-instance v25, Landroid/view/animation/LinearInterpolator;

    invoke-direct/range {v25 .. v25}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual/range {v24 .. v25}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 870
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 871
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/animation/ValueAnimator;->start()V

    .line 873
    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const-string v26, "y"

    const/16 v27, 0x5

    move/from16 v0, v27

    new-array v0, v0, [F

    move-object/from16 v27, v0

    fill-array-data v27, :array_0

    .line 874
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x1

    const-string v26, "x"

    const/16 v27, 0x5

    move/from16 v0, v27

    new-array v0, v0, [F

    move-object/from16 v27, v0

    fill-array-data v27, :array_1

    .line 875
    invoke-static/range {v26 .. v27}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v26

    aput-object v26, v24, v25

    .line 873
    invoke-static/range {v24 .. v24}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->IconTranslate:Landroid/animation/ValueAnimator;

    .line 877
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->IconTranslate:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    const-wide/16 v25, 0xbb8

    invoke-virtual/range {v24 .. v26}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 878
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->IconTranslate:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    const/16 v25, -0x1

    invoke-virtual/range {v24 .. v25}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 879
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->IconTranslate:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 880
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->IconTranslate:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/animation/ValueAnimator;->start()V

    .line 883
    const/16 v24, 0x3

    move/from16 v0, v24

    new-array v0, v0, [F

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const/16 v26, 0x0

    aput v26, v24, v25

    const/16 v25, 0x1

    const v26, -0x3fd33333    # -2.7f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    move/from16 v27, v0

    mul-float v26, v26, v27

    aput v26, v24, v25

    const/16 v25, 0x2

    const/16 v26, 0x0

    aput v26, v24, v25

    invoke-static/range {v24 .. v24}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    .line 884
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    const/16 v25, -0x1

    invoke-virtual/range {v24 .. v25}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 885
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    const-wide/16 v25, 0x5dc

    invoke-virtual/range {v24 .. v26}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 886
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 887
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/animation/ValueAnimator;->start()V

    .line 888
    return-void

    .line 873
    :array_0
    .array-data 4
        0x0
        -0x3fc00000    # -3.0f
        0x0
        0x40400000    # 3.0f
        0x0
    .end array-data

    .line 874
    :array_1
    .array-data 4
        0x0
        -0x3ee00000    # -10.0f
        0x0
        0x41200000    # 10.0f
        0x0
    .end array-data
.end method


# virtual methods
.method public cancelAnimation()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 923
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 924
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 926
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->IconTranslate:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->IconTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 927
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->IconTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 930
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 931
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 934
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1_1:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1_1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 935
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1_1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 938
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT2:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 939
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 942
    :cond_4
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale1:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 943
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 946
    :cond_5
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale2:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 947
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 951
    :cond_6
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale3:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 952
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 955
    :cond_7
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 956
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 959
    :cond_8
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 960
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 963
    :cond_9
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 964
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 967
    :cond_a
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 968
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 971
    :cond_b
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mIsActiveAnimationThread:Z

    .line 972
    return-void
.end method

.method public getScale()F
    .locals 1

    .prologue
    .line 1017
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 491
    return-object p0
.end method

.method public isRunning()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 981
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->IconTranslate:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1_1:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT2:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale1:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale2:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale3:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;

    if-nez v1, :cond_1

    .line 1008
    :cond_0
    :goto_0
    return v0

    .line 997
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->IconTranslate:Landroid/animation/ValueAnimator;

    .line 998
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1:Landroid/animation/ValueAnimator;

    .line 999
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1_1:Landroid/animation/ValueAnimator;

    .line 1000
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT2:Landroid/animation/ValueAnimator;

    .line 1001
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale1:Landroid/animation/ValueAnimator;

    .line 1002
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale2:Landroid/animation/ValueAnimator;

    .line 1003
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale3:Landroid/animation/ValueAnimator;

    .line 1004
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    .line 1005
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    .line 1006
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3:Landroid/animation/ValueAnimator;

    .line 1007
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;

    .line 1008
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v4, 0x43960000    # 300.0f

    const/4 v2, 0x0

    .line 446
    :try_start_0
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 448
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    cmpg-float v0, v0, v2

    if-gtz v0, :cond_1

    .line 449
    const-string v0, ""

    const-string v1, "scale is less then 0"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 488
    :cond_0
    :goto_0
    return-void

    .line 453
    :cond_1
    const/4 v8, 0x0

    .line 454
    .local v8, "cloudTrX":F
    const/4 v9, 0x0

    .line 455
    .local v9, "cloudTrY":F
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mIsActiveAnimationThread:Z

    if-eqz v0, :cond_2

    .line 456
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->IconTranslate:Landroid/animation/ValueAnimator;

    const-string v1, "x"

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v8

    .line 457
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->IconTranslate:Landroid/animation/ValueAnimator;

    const-string v1, "y"

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v9

    .line 460
    :cond_2
    const/16 v6, 0x1f

    .line 462
    .local v6, "saveFlag":I
    const/16 v6, 0xf

    .line 465
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float v1, v2, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v2, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float v3, v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v4, v0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->saveLayer(FFFFLandroid/graphics/Paint;I)I

    move-result v11

    .line 468
    .local v11, "transparentSaveLevel":I
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->drawDrop(Landroid/graphics/Canvas;)V

    .line 469
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v0, v8

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v9

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 470
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPaint:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->drawTransparentArea(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V

    .line 472
    invoke-virtual {p1, v11}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 474
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v7

    .line 475
    .local v7, "cloudSaveLevel":I
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v0, v8

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    mul-float/2addr v1, v9

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 476
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPaint:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->drawCloud(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    .line 478
    invoke-virtual {p1, v7}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 480
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 481
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->cancelAnimation()V

    .line 482
    const-string v0, ""

    const-string v1, "cancelA"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 485
    .end local v6    # "saveFlag":I
    .end local v7    # "cloudSaveLevel":I
    .end local v8    # "cloudTrX":F
    .end local v9    # "cloudTrY":F
    .end local v11    # "transparentSaveLevel":I
    :catch_0
    move-exception v10

    .line 486
    .local v10, "e":Ljava/lang/Exception;
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1120
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 1026
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onViewDetachedFromWindow : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1027
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 1028
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 1029
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 1031
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->IconTranslate:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_1

    .line 1032
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->IconTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 1033
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->IconTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 1036
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_2

    .line 1037
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 1038
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 1041
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1_1:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_3

    .line 1042
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1_1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 1043
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1_1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 1046
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT2:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_4

    .line 1047
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 1048
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 1051
    :cond_4
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale1:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_5

    .line 1052
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 1053
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 1056
    :cond_5
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale2:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_6

    .line 1057
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 1058
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 1062
    :cond_6
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale3:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_7

    .line 1063
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 1064
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 1067
    :cond_7
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_8

    .line 1068
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 1069
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 1072
    :cond_8
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_9

    .line 1073
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 1074
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 1077
    :cond_9
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_a

    .line 1078
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 1079
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 1082
    :cond_a
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_b

    .line 1083
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 1084
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 1087
    :cond_b
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    .line 1088
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->IconTranslate:Landroid/animation/ValueAnimator;

    .line 1089
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1:Landroid/animation/ValueAnimator;

    .line 1090
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1_1:Landroid/animation/ValueAnimator;

    .line 1091
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT2:Landroid/animation/ValueAnimator;

    .line 1092
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale1:Landroid/animation/ValueAnimator;

    .line 1093
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale2:Landroid/animation/ValueAnimator;

    .line 1094
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale3:Landroid/animation/ValueAnimator;

    .line 1095
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    .line 1096
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    .line 1097
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3:Landroid/animation/ValueAnimator;

    .line 1098
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;

    .line 1099
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPaint:Landroid/graphics/Paint;

    .line 1100
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mMasking:Landroid/graphics/Path;

    .line 1101
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudLeft:Landroid/graphics/Path;

    .line 1102
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudRight:Landroid/graphics/Path;

    .line 1103
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mCloudTop:Landroid/graphics/Path;

    .line 1104
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine1:Landroid/graphics/Path;

    .line 1105
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine2:Landroid/graphics/Path;

    .line 1106
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mRainLine3:Landroid/graphics/Path;

    .line 1107
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowCenter1:Landroid/graphics/Path;

    .line 1108
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowCenter2:Landroid/graphics/Path;

    .line 1109
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowCenter3:Landroid/graphics/Path;

    .line 1110
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft1:Landroid/graphics/Path;

    .line 1111
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mSnowLeft2:Landroid/graphics/Path;

    .line 1112
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_c

    .line 1113
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1115
    :cond_c
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mBitmap:Landroid/graphics/Bitmap;

    .line 1116
    return-void
.end method

.method public setPaintColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 91
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPaintColor:I

    .line 92
    return-void
.end method

.method public setScale(F)V
    .locals 0
    .param p1, "scale"    # F

    .prologue
    .line 1012
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mScale:F

    .line 1013
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->init()V

    .line 1014
    return-void
.end method

.method public setWidthAndHeight(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 1021
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, p1, p2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1022
    .local v0, "l":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1024
    return-void
.end method

.method public startAnimation()V
    .locals 2

    .prologue
    .line 891
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->isStop:Z

    .line 892
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixT1:Ljava/lang/String;

    .line 893
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixT1_1:Ljava/lang/String;

    .line 894
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixT2:Ljava/lang/String;

    .line 895
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixS1:Ljava/lang/String;

    .line 896
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixS2:Ljava/lang/String;

    .line 897
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixS3:Ljava/lang/String;

    .line 898
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixCT1:Ljava/lang/String;

    .line 899
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixCT2:Ljava/lang/String;

    .line 900
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixCT3:Ljava/lang/String;

    .line 901
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixCT3_1:Ljava/lang/String;

    .line 902
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->makeAnimation()V

    .line 904
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mIsActiveAnimationThread:Z

    .line 905
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)V

    .line 918
    .local v0, "t":Ljava/lang/Thread;
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 919
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 920
    return-void
.end method

.method public stopAnimation()V
    .locals 1

    .prologue
    .line 976
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->isStop:Z

    .line 977
    return-void
.end method
