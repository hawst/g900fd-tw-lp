.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/http/NetworkChangeReceiver;
.super Landroid/content/BroadcastReceiver;
.source "NetworkChangeReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widgetapp/ap/hero/accuweather/http/NetworkChangeReceiver$INetworkChangeLinster;
    }
.end annotation


# instance fields
.field private nwChangeListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/NetworkChangeReceiver$INetworkChangeLinster;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 31
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 37
    :try_start_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 38
    .local v0, "action":Ljava/lang/String;
    const-string v4, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 39
    const-string v4, "networkInfo"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/net/NetworkInfo;

    .line 40
    .local v2, "net":Landroid/net/NetworkInfo;
    const/4 v3, 0x0

    .line 41
    .local v3, "st":Landroid/net/NetworkInfo$State;
    if-eqz v2, :cond_0

    .line 42
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v3

    .line 44
    :cond_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    if-eqz v3, :cond_1

    sget-object v4, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    .line 45
    invoke-virtual {v3, v4}, Landroid/net/NetworkInfo$State;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 46
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/NetworkChangeReceiver;->nwChangeListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/NetworkChangeReceiver$INetworkChangeLinster;

    if-eqz v4, :cond_1

    .line 47
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/NetworkChangeReceiver;->nwChangeListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/NetworkChangeReceiver$INetworkChangeLinster;

    invoke-interface {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/NetworkChangeReceiver$INetworkChangeLinster;->onWifiConnected()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    .end local v0    # "action":Ljava/lang/String;
    .end local v2    # "net":Landroid/net/NetworkInfo;
    .end local v3    # "st":Landroid/net/NetworkInfo$State;
    :cond_1
    :goto_0
    return-void

    .line 50
    :catch_0
    move-exception v1

    .line 51
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public setNwChangeListener(Lcom/sec/android/widgetapp/ap/hero/accuweather/http/NetworkChangeReceiver$INetworkChangeLinster;)V
    .locals 0
    .param p1, "nwChangeListener"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/http/NetworkChangeReceiver$INetworkChangeLinster;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/NetworkChangeReceiver;->nwChangeListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/NetworkChangeReceiver$INetworkChangeLinster;

    .line 57
    return-void
.end method
