.class Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$19;
.super Ljava/lang/Object;
.source "WeatherClockDetail.java"

# interfaces
.implements Landroid/widget/PopupMenu$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->displayOptionMenu(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 2847
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$19;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 9
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 2849
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    :goto_0
    :pswitch_0
    move v3, v4

    .line 2890
    :goto_1
    return v3

    .line 2851
    :pswitch_1
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$19;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->runAddtoList()V
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$2100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    goto :goto_1

    .line 2855
    :pswitch_2
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$19;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->runMap()V
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$3200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    goto :goto_1

    .line 2859
    :pswitch_3
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$19;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->runDeleteCity()V
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$3300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    goto :goto_1

    .line 2863
    :pswitch_4
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$19;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->runMore()V
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$3400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    goto :goto_1

    .line 2867
    :pswitch_5
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$19;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->runSetDefaultCity()V
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$3500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    goto :goto_1

    .line 2871
    :pswitch_6
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$19;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->runSettings()V
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$3600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    goto :goto_1

    .line 2880
    :pswitch_7
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$19;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$19;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "REPORT_WRONG_CITY"

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setViewUri(Landroid/content/Context;Ljava/lang/String;I)Landroid/net/Uri;

    move-result-object v2

    .line 2881
    .local v2, "uri":Landroid/net/Uri;
    new-instance v1, Landroid/content/Intent;

    const-string v5, "android.intent.action.VIEW"

    invoke-direct {v1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2882
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2883
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$19;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-virtual {v5, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 2885
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v0

    .line 2886
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v3, ""

    const-string v5, "ANFE wrongCity"

    invoke-static {v3, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2887
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 2849
    :pswitch_data_0
    .packed-switch 0x7f080130
        :pswitch_5
        :pswitch_6
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_7
    .end packed-switch
.end method
