.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;
.super Landroid/view/View;
.source "SnowIconView.java"

# interfaces
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;


# static fields
.field private static final ANI_TYPE_CLOUD_TRANSLATE_X:I = 0x11

.field private static final ANI_TYPE_FIRST:I = 0x22

.field private static final ANI_TYPE_LEFT_FlOWER_ROTATE:I = 0x21

.field private static final ANI_TYPE_SECOND:I = 0x25


# instance fields
.field private TranslateAni:Landroid/animation/ValueAnimator;

.field private isStart2ndAni:Z

.field isStop:Z

.field private mAnimators:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/animation/ValueAnimator;",
            ">;"
        }
    .end annotation
.end field

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mIsActiveAnimationThread:Z

.field private mListener:Landroid/animation/Animator$AnimatorListener;

.field private mListener2:Landroid/animation/Animator$AnimatorListener;

.field private mPaint:Landroid/graphics/Paint;

.field private mPaintColor:I

.field private mPath:Landroid/graphics/Path;

.field public mPreFixProperty1:Ljava/lang/String;

.field private mScale:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 79
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 75
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 76
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 70
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mAnimators:Ljava/util/HashMap;

    .line 44
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    .line 46
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mIsActiveAnimationThread:Z

    .line 48
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->isStart2ndAni:Z

    .line 52
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPaint:Landroid/graphics/Paint;

    .line 54
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPath:Landroid/graphics/Path;

    .line 56
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    .line 59
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mBitmap:Landroid/graphics/Bitmap;

    .line 63
    const v0, -0xa0a0b

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPaintColor:I

    .line 655
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mListener:Landroid/animation/Animator$AnimatorListener;

    .line 687
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mListener2:Landroid/animation/Animator$AnimatorListener;

    .line 714
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPreFixProperty1:Ljava/lang/String;

    .line 914
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->isStop:Z

    .line 71
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->init()V

    .line 72
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mAnimators:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;
    .param p1, "x1"    # Z

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->isStart2ndAni:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mIsActiveAnimationThread:Z

    return v0
.end method

.method private drawCenterDrop1(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "path"    # Landroid/graphics/Path;
    .param p3, "paint"    # Landroid/graphics/Paint;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const v11, 0x43176560

    const v10, 0x42e9c106

    const v9, 0x42d7353f

    const v8, 0x42c4a979

    const v7, 0x4304d893

    .line 418
    invoke-virtual {p2}, Landroid/graphics/Path;->reset()V

    .line 419
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v9

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v7

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 420
    const v0, 0x42ccfb64

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v2, v7, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v3, v8, v0

    const v0, 0x4309020c

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v5, v8, v0

    const v0, 0x430e1f7d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 422
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v1, v8, v0

    const v0, 0x43133c6a

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42ccfb64

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v4, v11, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v5, v9, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v6, v11, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 424
    const v0, 0x42e16f1b

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v2, v11, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v3, v10, v0

    const v0, 0x43133c6a

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v5, v10, v0

    const v0, 0x430e1f7d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 426
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v1, v10, v0

    const v0, 0x4309020c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42e16f1b

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v4, v7, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v5, v9, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v6, v7, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 428
    invoke-virtual {p2}, Landroid/graphics/Path;->close()V

    .line 430
    invoke-virtual {p1, p2, p3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 431
    return-void
.end method

.method private drawCenterDrop2(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "path"    # Landroid/graphics/Path;
    .param p3, "paint"    # Landroid/graphics/Paint;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const v11, 0x432b30e5

    const v10, 0x42d0ee14

    const v9, 0x42c35810

    const v8, 0x42b5c20c

    const v7, 0x431d9ae1

    .line 434
    invoke-virtual {p2}, Landroid/graphics/Path;->reset()V

    .line 435
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v9

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v7

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 436
    const v0, 0x42bbda1d

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v2, v7, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v3, v8, v0

    const v0, 0x4320a6e9

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v5, v8, v0

    const v0, 0x432465e3

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 438
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v1, v8, v0

    const v0, 0x432824dd

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42bbda1d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v4, v11, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v5, v9, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v6, v11, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 440
    const v0, 0x42cad604    # 101.418f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v2, v11, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v3, v10, v0

    const v0, 0x432824dd

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v5, v10, v0

    const v0, 0x432465e3

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 442
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v1, v10, v0

    const v0, 0x4320a6e9

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42cad604    # 101.418f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v4, v7, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v5, v9, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v6, v7, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 444
    invoke-virtual {p2}, Landroid/graphics/Path;->close()V

    .line 446
    invoke-virtual {p1, p2, p3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 447
    return-void
.end method

.method private drawCenterDrop3(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "path"    # Landroid/graphics/Path;
    .param p3, "paint"    # Landroid/graphics/Paint;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const v11, 0x43438c4a

    const v10, 0x42c01f3b

    const v9, 0x42af38d5    # 87.611f

    const v8, 0x429e5168

    const v7, 0x4332a6e9

    .line 450
    invoke-virtual {p2}, Landroid/graphics/Path;->reset()V

    .line 451
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v9

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v7

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 452
    const v0, 0x42a5e6e9

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v2, v7, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v3, v8, v0

    const v0, 0x433670e5

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v5, v8, v0

    const v0, 0x433b19db

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 454
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v1, v8, v0

    const v0, 0x433fc24e

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42a5e6e9

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v4, v11, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v5, v9, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v6, v11, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 456
    const v0, 0x42b88a3d    # 92.27f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v2, v11, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v3, v10, v0

    const v0, 0x433fc24e

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v5, v10, v0

    const v0, 0x433b19db

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 458
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v1, v10, v0

    const v0, 0x433670e5

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42b88a3d    # 92.27f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v4, v7, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v5, v9, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v6, v7, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 460
    invoke-virtual {p2}, Landroid/graphics/Path;->close()V

    .line 462
    invoke-virtual {p1, p2, p3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 463
    return-void
.end method

.method private drawCloud(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "path"    # Landroid/graphics/Path;
    .param p3, "paint"    # Landroid/graphics/Paint;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 262
    const/4 v0, 0x0

    .line 263
    .local v0, "dx":F
    iget-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->isStart2ndAni:Z

    if-eqz v1, :cond_0

    .line 264
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    neg-float v0, v1

    .line 266
    :cond_0
    invoke-virtual {p2}, Landroid/graphics/Path;->reset()V

    .line 267
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPaintColor:I

    invoke-virtual {p3, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 269
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 270
    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 271
    invoke-direct {p0, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->getCloudLeftPath(Landroid/graphics/Path;)V

    .line 272
    invoke-virtual {p1, p2, p3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 273
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 275
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 276
    invoke-virtual {p1, v2, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 277
    invoke-direct {p0, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->getCloudTopPath(Landroid/graphics/Path;)V

    .line 278
    invoke-virtual {p1, p2, p3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 279
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 281
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 282
    neg-float v1, v0

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 283
    invoke-direct {p0, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->getCloudRightPath(Landroid/graphics/Path;)V

    .line 284
    invoke-virtual {p1, p2, p3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 285
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 286
    return-void
.end method

.method private drawLeftDrop(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "path"    # Landroid/graphics/Path;
    .param p3, "paint"    # Landroid/graphics/Paint;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const v11, 0x43148000    # 148.5f

    const v10, 0x429ccb44

    const v9, 0x428e6042

    const v8, 0x427fec8b

    const v7, 0x43061604

    .line 338
    invoke-virtual {p2}, Landroid/graphics/Path;->reset()V

    .line 339
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v9

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v7

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 340
    const v0, 0x42866d91

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v2, v7, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v3, v8, v0

    const v0, 0x430951ec    # 137.32f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v5, v8, v0

    const v0, 0x430d4b85

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 342
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v1, v8, v0

    const v0, 0x4311449c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42866d91

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v4, v11, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v5, v9, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v6, v11, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 344
    const v0, 0x42965375

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v2, v11, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v3, v10, v0

    const v0, 0x4311449c

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v5, v10, v0

    const v0, 0x430d4b85

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 346
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v1, v10, v0

    const v0, 0x430951ec    # 137.32f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42965375

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v4, v7, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v5, v9, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v6, v7, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 348
    invoke-virtual {p2}, Landroid/graphics/Path;->close()V

    .line 350
    invoke-virtual {p1, p2, p3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 351
    return-void
.end method

.method private drawLeftFlower(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "path"    # Landroid/graphics/Path;
    .param p3, "paint"    # Landroid/graphics/Paint;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const v11, 0x42895f3b

    const v10, 0x42815f3b

    const v9, 0x420ebe77    # 35.686f

    const/high16 v8, 0x432d0000    # 173.0f

    const/high16 v7, 0x43250000    # 165.0f

    .line 290
    invoke-virtual {p2}, Landroid/graphics/Path;->reset()V

    .line 291
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v10

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v7

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 292
    const v0, 0x426e0d50    # 59.513f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v7

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 293
    const v0, 0x427b24dd    # 62.786f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x431f5be7

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 294
    const v0, 0x427f8f5c    # 63.89f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x431d71ec

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x427cf0a4    # 63.235f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x431b0396

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x427549ba

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x4319e8b4

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 296
    const v0, 0x426da2d1    # 59.409f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x4318cdd3    # 152.804f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x4263d917

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x4319778d

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x425f6e98

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x431b6189

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 298
    const v0, 0x4252cdd3    # 52.701f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x4320da1d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 299
    const v0, 0x42462d0e

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x431b6312

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 300
    const v0, 0x4241c28f    # 48.44f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x43197917

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x4237f8d5    # 45.993f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x4318d168

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x423051ec    # 44.08f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x4319ec4a

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 302
    const v0, 0x4228ab02    # 42.167f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x431b072b    # 155.028f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42260c4a

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x431d71ec

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x422a76c9

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x431f5c29    # 159.36f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 304
    const v0, 0x42379062

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v7

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 305
    const v0, 0x421ebe77    # 39.686f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v7

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 306
    const v0, 0x4215e873

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v2, v7, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v3, v9, v0

    const v0, 0x4326ca7f    # 166.791f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v5, v9, v0

    const/high16 v0, 0x43290000    # 169.0f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 308
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v1, v9, v0

    const v0, 0x432b3581    # 171.209f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x4215e873

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v4, v8, v0

    const v0, 0x421ebe77    # 39.686f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v6, v8, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 310
    const v0, 0x4236a0c5

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v8

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 311
    const v0, 0x422a77cf    # 42.617f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x43324b85

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 312
    const v0, 0x42260d50    # 41.513f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x433434fe    # 180.207f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x4228ac08    # 42.168f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x4336ab44

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x423052f2

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x4337c5e3

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 314
    const v0, 0x4232d810

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x43382354    # 184.138f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x4235978d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x43385127

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x42384ed9    # 46.077f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x43385127

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 316
    const v0, 0x423dd70a    # 47.46f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x43385127

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42433852    # 48.805f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x43379aa0

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x42462e14

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x4336522d    # 182.321f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 318
    const v0, 0x4252ced9    # 52.702f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x4330daa0

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 319
    const v0, 0x425f6f9e

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x4336526f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 320
    const v0, 0x42626666    # 56.6f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x43379ae1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x4267c6a8    # 57.944f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x433852b0

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x426d4ed9    # 59.327f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x433852b0

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 322
    const v0, 0x42700625    # 60.006f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x433852b0

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x4272c5a2    # 60.693f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x433826a8    # 184.151f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x42754ac1

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x4337c937

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 324
    const v0, 0x427cf1aa    # 63.236f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x4336ae98

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x427f9062

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x433434bc

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x427b25e3

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x43324b44

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 326
    const v0, 0x426efdf4

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v8

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 327
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v10

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v8

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 328
    const v0, 0x4285ca3d

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v2, v8, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v3, v11, v0

    const v0, 0x432b3581    # 171.209f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v5, v11, v0

    const/high16 v0, 0x43290000    # 169.0f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 330
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v1, v11, v0

    const v0, 0x4326ca7f    # 166.791f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x4285ca3d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v4, v7, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v5, v10, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v6, v7, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 332
    invoke-virtual {p2}, Landroid/graphics/Path;->close()V

    .line 334
    invoke-virtual {p1, p2, p3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 335
    return-void
.end method

.method private drawRightDrop(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "path"    # Landroid/graphics/Path;
    .param p3, "paint"    # Landroid/graphics/Paint;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const v11, 0x4313e8f6    # 147.91f

    const v10, 0x430d88f6

    const v9, 0x430727f0

    const v8, 0x43061604

    const v7, 0x4312d893

    .line 402
    invoke-virtual {p2}, Landroid/graphics/Path;->reset()V

    .line 403
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v10

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v7

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 404
    const v0, 0x43110d0e

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v2, v7, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v3, v11, v0

    const v0, 0x430ffc29    # 143.985f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v5, v11, v0

    const v0, 0x430c778d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 406
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v1, v11, v0

    const v0, 0x4308f2f2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x43110d0e

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v4, v8, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v5, v10, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v6, v8, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 408
    const v0, 0x430a045a

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v2, v8, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v3, v9, v0

    const v0, 0x4308f2f2

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v5, v9, v0

    const v0, 0x430c778d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 410
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v1, v9, v0

    const v0, 0x430ffc29    # 143.985f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x430a049c

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v4, v7, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v5, v10, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v6, v7, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 412
    invoke-virtual {p2}, Landroid/graphics/Path;->close()V

    .line 414
    invoke-virtual {p1, p2, p3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 415
    return-void
.end method

.method private drawRightFlower(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "path"    # Landroid/graphics/Path;
    .param p3, "paint"    # Landroid/graphics/Paint;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const v11, 0x4324af9e

    const v10, 0x4320af9e

    const v9, 0x42ed5f3b

    const/high16 v8, 0x43370000    # 183.0f

    const/high16 v7, 0x432f0000    # 175.0f

    .line 354
    invoke-virtual {p2}, Landroid/graphics/Path;->reset()V

    .line 355
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v10

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v7

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 356
    const v0, 0x4314da1d

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v7

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 357
    const v0, 0x431b1021

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x432445e3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 358
    const v0, 0x431c2ac1

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x43225be7

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x431b8312

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x431fed91

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x43199917

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x431ed2b0

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 360
    const v0, 0x4317af9e

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x431db7cf

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x43153d2f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x431e6189

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x4314228f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x43204b85

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 362
    const v0, 0x430e147b    # 142.08f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x432ac873

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 363
    const v0, 0x430806e9

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x43204d0e

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 364
    const v0, 0x4306ec4a

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x431e6312

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x43047a5e

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x431dbb64

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x43029062

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x431ed646

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 366
    const v0, 0x4300a666    # 128.65f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x431ff127

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42fffdf4

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x43225be7

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x43011958    # 129.099f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x43244625

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 368
    const v0, 0x43074f9e

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v7

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 369
    const v0, 0x42f55f3b

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v7

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 370
    const v0, 0x42f0f439

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v2, v7, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v3, v9, v0

    const v0, 0x4330ca7f    # 176.791f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v5, v9, v0

    const/high16 v0, 0x43330000    # 179.0f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 372
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v1, v9, v0

    const v0, 0x43353581    # 181.209f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42f0f439

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v4, v8, v0

    const v0, 0x42f55f3b

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v6, v8, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 374
    const v0, 0x4306ff3b

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v8

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 375
    const v0, 0x430119db

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x43413df4

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 376
    const v0, 0x42fffe77

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x4343276d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x4300a6e9

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x43459db2

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x430290e5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x4346b852    # 198.72f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 378
    const v0, 0x430331ec

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x434715c3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x4303e1cb

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x43474396

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x43048fdf

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x43474396

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 380
    const v0, 0x4305f1ec

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x43474396

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x430749fc

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x43468d0e

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x4308076d

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x4345449c

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 382
    const v0, 0x430e14fe    # 142.082f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x433ac937

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 383
    const v0, 0x43142312

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x43454560

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 384
    const v0, 0x4314e083

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x43468dd3    # 198.554f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x43163893

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x434745a2

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x43179aa0

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x434745a2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 386
    const v0, 0x431848b4

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x434745a2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x4318f893

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x4347199a    # 199.1f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x4319999a    # 153.6f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x4346bc29    # 198.735f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 388
    const v0, 0x431b8396

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x4345a189

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x431c2b02    # 156.168f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x434327ae

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x431b10a4

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x43413e35

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 390
    const v0, 0x43152a7f    # 149.166f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v8

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 391
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v10

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v8

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 392
    const v0, 0x4322e51f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v2, v8, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v3, v11, v0

    const v0, 0x43353581    # 181.209f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v5, v11, v0

    const/high16 v0, 0x43330000    # 179.0f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 394
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v1, v11, v0

    const v0, 0x4330ca7f    # 176.791f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x4322e51f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v4, v7, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v5, v10, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v6, v7, v0

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 396
    invoke-virtual {p2}, Landroid/graphics/Path;->close()V

    .line 398
    invoke-virtual {p1, p2, p3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 399
    return-void
.end method

.method private drawTransparentArea(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Path;Landroid/graphics/Paint;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "path"    # Landroid/graphics/Path;
    .param p4, "paint"    # Landroid/graphics/Paint;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 256
    new-instance v0, Landroid/graphics/PorterDuffXfermode;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v1}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {p4, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 257
    invoke-virtual {p1, p2, v2, v2, p4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 258
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 259
    return-void
.end method

.method private getCloudInnerPath(Landroid/graphics/Path;)V
    .locals 12
    .param p1, "path"    # Landroid/graphics/Path;

    .prologue
    const v11, 0x426c1375

    const v10, 0x42219168

    const v9, 0x41e5d4fe    # 28.729f

    const v8, 0x41c82f1b    # 25.023f

    const/high16 v7, 0x42ec0000    # 118.0f

    .line 99
    const v0, 0x432647f0

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v7

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 100
    const v0, 0x421e5f3b

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v7

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 101
    const v0, 0x41fc6042    # 31.547f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v2, v7, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v3, v8, v0

    const v0, 0x42ded0e5

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v5, v8, v0

    const v0, 0x42ceb8d5    # 103.361f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 103
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v1, v8, v0

    const v0, 0x42bea1cb

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x41fc6042    # 31.547f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x42b18419

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x421e5f3b

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x42b18419

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 105
    const v0, 0x422020c5

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x42b18419

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x4221dd2f    # 40.466f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x42b18937

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x42239581    # 40.896f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x42b19c29    # 88.805f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 107
    const v0, 0x422247ae    # 40.57f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x42aec000    # 87.375f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v3, v10, v0

    const v0, 0x42abc28f    # 85.88f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v5, v10, v0

    const v0, 0x42a8b3b6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 109
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v1, v10, v0

    const v0, 0x4292a6e9

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x424551ec    # 49.33f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x4280c28f    # 64.38f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x42716e98

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x4280c28f    # 64.38f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 111
    const v0, 0x4271a3d7    # 60.41f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x4280c28f    # 64.38f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x4271d70a    # 60.46f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x4280c5a2

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x42720c4a

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x4280c5a2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 113
    const v0, 0x426e1fbe

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x4289a5e3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v3, v11, v0

    const v0, 0x4292dfbe

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v5, v11, v0

    const v0, 0x429c4e56    # 78.153f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 115
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v1, v11, v0

    const v0, 0x42a0b958    # 80.362f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42733d71    # 60.81f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x42a44e56    # 82.153f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x427c1375

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x42a44e56    # 82.153f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 117
    const v0, 0x428274bc

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x42a44e56    # 82.153f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x428609ba

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x42a0b8d5    # 80.361f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x428609ba

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x429c4dd3    # 78.152f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 119
    const v0, 0x428609ba

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x4287b74c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x428b70a4    # 69.72f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x426a7ae1    # 58.62f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x4294dcac    # 74.431f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x424bf3b6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 121
    const v0, 0x42a0bcee

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x4226ac08    # 41.668f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42b2dc29    # 89.43f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x420a27f0

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x42c848b4

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x41f88937    # 31.067f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 123
    const v0, 0x42d0d917

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x41edc49c    # 29.721f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42d9fa5e

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x41e76a7f    # 28.927f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x42e3872b    # 113.764f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x41e62f1b    # 28.773f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 125
    const v0, 0x42e3a560

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x41e62b02    # 28.771f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42e3c000    # 113.875f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x41e60831    # 28.754f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x42e3dd2f    # 113.932f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x41e6020c    # 28.751f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 127
    const v0, 0x42e48d50    # 114.276f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x41e5f3b6    # 28.744f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42e53c6a

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v4, v9, v0

    const v0, 0x42e5ec08

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v6, v9, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 129
    const v0, 0x4305220c

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v2, v9, v0

    const v0, 0x4314ee14    # 148.93f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x421b4bc7

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x431d1810

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x4256cfdf    # 53.703f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 131
    const v0, 0x4313cd91

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x426046a8    # 56.069f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x430bac08

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x4277872b    # 61.882f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x43065f7d

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x428c753f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 133
    const v0, 0x43053062

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x42903021

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x4305bd71    # 133.74f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x4295224e

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x43079ae1

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x42978000    # 75.75f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 135
    const v0, 0x43084560

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x42985810

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x430902d1

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x4298befa

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x4309befa

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x4298befa

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 137
    const v0, 0x430b11ec    # 139.07f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x4298befa

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x430c5d71

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x42976f1b

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x430d2083

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x42950831

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 139
    const v0, 0x4312b0a4    # 146.69f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x42838000    # 65.75f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x431c3375

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x4272126f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x43269168

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x4272126f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 141
    const v0, 0x4326accd

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x4272126f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x4326c6e9

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x4271fae1

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x4326e1cb

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x4271f2b0    # 60.487f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 143
    const v0, 0x43367439

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x42733a5e    # 60.807f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x4342f9db

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x42932d91

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x4342f9db

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x42b27852    # 89.235f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 145
    const v0, 0x4342fa1d

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x42d228f6    # 105.08f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x4336220c

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v4, v7, v0

    const v0, 0x432647f0

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v6, v7, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 147
    invoke-virtual {p1}, Landroid/graphics/Path;->close()V

    .line 148
    return-void
.end method

.method private getCloudLeftPath(Landroid/graphics/Path;)V
    .locals 12
    .param p1, "path"    # Landroid/graphics/Path;

    .prologue
    const v11, 0x425ea1cb    # 55.658f

    const v10, 0x4222d1ec

    const v9, 0x421f9fbe

    const v8, 0x41cab021    # 25.336f

    const v7, 0x418ab439    # 17.338f

    .line 217
    invoke-virtual {p1}, Landroid/graphics/Path;->reset()V

    .line 218
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v9

    const v1, 0x42ea8937

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 219
    const v0, 0x41fee148    # 31.86f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x42ea8937

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v3, v8, v0

    const v0, 0x42dd25e3

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v5, v8, v0

    const v0, 0x42cd2042

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 221
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v1, v8, v0

    const v0, 0x42bd1ba6    # 94.554f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x41fee148    # 31.86f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x42afeb02    # 87.959f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v5, v9, v0

    const v0, 0x42afeb02    # 87.959f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 223
    const v0, 0x42216148

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x42afeb02    # 87.959f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42231db2    # 40.779f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x42afdf3b

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x4224d604    # 41.209f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x42aff333

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 225
    const v0, 0x4223872b    # 40.882f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x42ad199a    # 86.55f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v3, v10, v0

    const v0, 0x42aa178d

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v5, v10, v0

    const v0, 0x42a70ac1

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 227
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v1, v10, v0

    const v0, 0x4291178d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42469168

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x427e872b    # 63.632f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x4272ae14    # 60.67f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x427e872b    # 63.632f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 229
    const v0, 0x4272e354    # 60.722f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x427e872b    # 63.632f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x4282e148    # 65.44f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x42805f3b

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x4282e148    # 65.44f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x42805f3b

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 231
    const v0, 0x4286ad0e

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x42606f9e

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 232
    const v0, 0x42880ccd

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x42606042

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x4275ced9    # 61.452f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v4, v11, v0

    const v0, 0x4272af1b

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v6, v11, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 234
    const v0, 0x4238570a    # 46.085f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v2, v11, v0

    const v0, 0x42084ac1

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x4285a8f6    # 66.83f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x42034396

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x42a21062

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 236
    const v0, 0x41beae14    # 23.835f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x42a7fdf4

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v3, v7, v0

    const v0, 0x42b946a8    # 92.638f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v5, v7, v0

    const v0, 0x42cd1fbe

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 238
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v1, v7, v0

    const v0, 0x42e5e76d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x41dbb22d    # 27.462f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x42fa774c

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x421fa1cb    # 39.908f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x42fa774c

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 240
    const v0, 0x431c374c

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x42fa774c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 241
    const v0, 0x431c374c

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x42ea88b4

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 242
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v9

    const v1, 0x42ea88b4

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 243
    invoke-virtual {p1}, Landroid/graphics/Path;->close()V

    .line 244
    return-void
.end method

.method private getCloudRightPath(Landroid/graphics/Path;)V
    .locals 12
    .param p1, "path"    # Landroid/graphics/Path;

    .prologue
    const v11, 0x4343553f

    const v10, 0x4326a354    # 166.638f

    const v9, 0x42ea8937

    const v8, 0x429720c5

    const v7, 0x424f20c5

    .line 187
    invoke-virtual {p1}, Landroid/graphics/Path;->reset()V

    .line 188
    const v0, 0x43267ba6

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v7

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 189
    const v0, 0x43263687

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v2, v7, v0

    const v0, 0x4321d5c3

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x424f51ec    # 51.83f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x431d3aa0

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x42543646

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 191
    const v0, 0x43140b02    # 148.043f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x425df6c9

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x430bfc6a

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x4274cbc7

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x4306bae1    # 134.73f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x428ae3d7    # 69.445f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 193
    const v0, 0x43058b44

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x428e9aa0

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x43061852

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x4293872b    # 73.764f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x4307f5c3    # 135.96f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x4295e24e

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 195
    const v0, 0x4308a083

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x4296b9db

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x43095df4

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v4, v8, v0

    const v0, 0x430a19db

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v6, v8, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 197
    const v0, 0x430b6ccd

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v2, v8, v0

    const v0, 0x430cb810

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x4295d168

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x430d7be7

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x42936d91

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 199
    const v0, 0x43130c08

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x4281f852    # 64.985f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x431b5efa

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x426efae1

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x43273e35

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x426efae1

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 201
    const v0, 0x4336cfdf

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x42704396

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v3, v11, v0

    const v0, 0x4291d687

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v5, v11, v0

    const v0, 0x42b0fe77

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 203
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v1, v11, v0

    const v0, 0x42d08bc7

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x43367cee

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v4, v9, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v5, v10, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v6, v9, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 205
    const v0, 0x42775a1d

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v9

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 206
    const v0, 0x42775a1d

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x42fa77cf

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 207
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v10

    const v1, 0x42fa77cf

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 208
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v10

    const v1, 0x42fa774c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 209
    const v0, 0x433adf3b

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x42fa774c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x434b54bc

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x42d949ba

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x434b54bc

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x42b0fefa

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 211
    const v0, 0x434b54bc

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x4288b439

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x433ab78d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v4, v7, v0

    const v0, 0x43267ba6

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v6, v7, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 213
    invoke-virtual {p1}, Landroid/graphics/Path;->close()V

    .line 214
    return-void
.end method

.method private getCloudTopPath(Landroid/graphics/Path;)V
    .locals 12
    .param p1, "path"    # Landroid/graphics/Path;

    .prologue
    const v11, 0x427eef9e

    const v10, 0x42733646

    const v9, 0x426d52f2

    const v8, 0x41e124dd    # 28.143f

    const v7, 0x41a1d70a    # 20.23f

    .line 151
    invoke-virtual {p1}, Landroid/graphics/Path;->reset()V

    .line 152
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v10

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v11

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 153
    const v0, 0x426f5a1d

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x42883efa

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v3, v9, v0

    const v0, 0x42915b23

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v5, v9, v0

    const v0, 0x429aac8b

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 155
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v1, v9, v0

    const v0, 0x429f12f2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42747df4

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x42a2a24e

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x427d52f2

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x42a2a24e

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 157
    const v0, 0x4283147b    # 65.54f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x42a2a24e

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x4286a979

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x429f10e5

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x4286a979

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x429aaa7f    # 77.333f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 159
    const v0, 0x4286a979

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x42862c8b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x428c0f5c    # 70.03f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x4267926f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x42957a5e

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x42492b02    # 50.292f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 161
    const v0, 0x42a15d2f    # 80.682f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x42241581    # 41.021f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42b3820c

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x4207ac08    # 33.918f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x42c8f021

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x41f3b852    # 30.465f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 163
    const v0, 0x42d18831

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x41e90419    # 29.127f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42dbb958    # 109.862f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v4, v8, v0

    const v0, 0x42e6a2d1    # 115.318f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v6, v8, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 165
    const v0, 0x4304bf3b

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v2, v8, v0

    const v0, 0x431549ba

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x4218c6a8    # 38.194f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x431d72f2

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x4253f7cf    # 52.992f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 167
    const v0, 0x431d6000    # 157.375f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x42540b44

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x431efd2f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x4262b74c    # 56.679f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x431eea3d

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x4262cac1

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 169
    const v0, 0x43277e35

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x425e51ec    # 55.58f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 170
    const v0, 0x4325c042

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v0, v1

    const v1, 0x424ee873

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 171
    const v0, 0x431c79db

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x4202c083    # 32.688f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x4308f687

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v4, v7, v0

    const v0, 0x42e6a2d1    # 115.318f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v6, v7, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 173
    const v0, 0x42e11810

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v2, v7, v0

    const v0, 0x42db92f2

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x41a3645a    # 20.424f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x42d61c29    # 107.055f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x41a6a5e3    # 20.831f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 175
    const v0, 0x42bcfbe7

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x41b42d0e    # 22.522f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42a67333

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x41de70a4    # 27.805f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x42954419

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x421425e3

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 177
    const v0, 0x428d2d0e

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x4225820c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x42869b23

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x423954fe    # 46.333f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const v0, 0x4281a24e

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x424efcee

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 179
    const v0, 0x42805b23

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x42543f7d    # 53.062f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    const v0, 0x427c0419

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v0

    const v0, 0x425ef8d5    # 55.743f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v0

    const/high16 v0, 0x427c0000    # 63.0f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v0

    const v0, 0x425f051f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 181
    const v0, 0x4277ee98

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v0

    const v0, 0x426b6560    # 58.849f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v3, v10, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v4, v11, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v5, v10, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v6, v11, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 183
    invoke-virtual {p1}, Landroid/graphics/Path;->close()V

    .line 184
    return-void
.end method

.method private getMasking(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "path"    # Landroid/graphics/Path;
    .param p3, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 247
    invoke-virtual {p2}, Landroid/graphics/Path;->reset()V

    .line 248
    invoke-direct {p0, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->getCloudInnerPath(Landroid/graphics/Path;)V

    .line 250
    const/high16 v0, -0x10000

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 252
    invoke-virtual {p1, p2, p3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 253
    return-void
.end method

.method private init()V
    .locals 4

    .prologue
    const/high16 v3, 0x43960000    # 300.0f

    .line 83
    invoke-virtual {p0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 84
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPaint:Landroid/graphics/Paint;

    .line 85
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPath:Landroid/graphics/Path;

    .line 87
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mBitmap:Landroid/graphics/Bitmap;

    .line 89
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 90
    .local v0, "mMaskingCanvas":Landroid/graphics/Canvas;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPath:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPaint:Landroid/graphics/Paint;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->getMasking(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 92
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPaintColor:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 93
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 94
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 95
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x40a00000    # 5.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 96
    return-void
.end method


# virtual methods
.method public cancelAnimation()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 896
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 897
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 898
    .local v2, "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 899
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 900
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    .line 901
    .local v0, "ani":Landroid/animation/ValueAnimator;
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 902
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    goto :goto_0

    .line 907
    .end local v0    # "ani":Landroid/animation/ValueAnimator;
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    .end local v2    # "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_1
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 908
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->cancel()V

    .line 911
    :cond_2
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mIsActiveAnimationThread:Z

    .line 912
    return-void
.end method

.method public getScale()F
    .locals 1

    .prologue
    .line 946
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 652
    return-object p0
.end method

.method public isRunning()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 920
    const/4 v3, 0x0

    .line 921
    .local v3, "ret":Z
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v4}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 922
    const/4 v3, 0x1

    .line 924
    :cond_0
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mAnimators:Ljava/util/HashMap;

    if-eqz v4, :cond_2

    .line 925
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 926
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 927
    .local v2, "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 928
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 929
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    .line 930
    .local v0, "ani":Landroid/animation/ValueAnimator;
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 931
    const/4 v3, 0x1

    .line 937
    .end local v0    # "ani":Landroid/animation/ValueAnimator;
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    .end local v2    # "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_2
    return v3
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 49
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 467
    :try_start_0
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    const/4 v4, 0x0

    cmpg-float v3, v3, v4

    if-gtz v3, :cond_1

    .line 468
    const-string v3, ""

    const-string v4, "scale is less then 0"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 649
    :cond_0
    :goto_0
    return-void

    .line 472
    :cond_1
    const/16 v25, 0x0

    .line 473
    .local v25, "leftAngle":F
    const/16 v22, 0x0

    .line 474
    .local v22, "cloudTrX":F
    const/16 v23, 0x0

    .line 475
    .local v23, "cloudTrY":F
    const/16 v38, 0x0

    .line 476
    .local v38, "leftFlowserTrX":F
    const/16 v40, 0x0

    .line 477
    .local v40, "leftFlowserTrY":F
    const/high16 v36, 0x3f800000    # 1.0f

    .line 478
    .local v36, "leftFlowserSc":F
    const/16 v30, 0x0

    .line 479
    .local v30, "leftDropTrX":F
    const/16 v19, 0x0

    .line 480
    .local v19, "centerDropTrX":F
    const/16 v32, 0x0

    .line 481
    .local v32, "leftDropTrY":F
    const/high16 v28, 0x3f800000    # 1.0f

    .line 482
    .local v28, "leftDropSc":F
    const/high16 v44, 0x3f800000    # 1.0f

    .line 483
    .local v44, "rightDropSc":F
    const/16 v20, 0x0

    .line 484
    .local v20, "centerDropTrY":F
    const/high16 v12, 0x3f800000    # 1.0f

    .line 485
    .local v12, "centerDrop1Sc":F
    const/high16 v14, 0x3f800000    # 1.0f

    .line 486
    .local v14, "centerDrop2Sc":F
    const/high16 v17, 0x3f800000    # 1.0f

    .line 489
    .local v17, "centerDrop3Sc":F
    const/16 v39, 0x0

    .line 490
    .local v39, "leftFlowserTrX1":F
    const/16 v41, 0x0

    .line 491
    .local v41, "leftFlowserTrY1":F
    const/high16 v37, 0x3f800000    # 1.0f

    .line 492
    .local v37, "leftFlowserSc1":F
    const/16 v31, 0x0

    .line 493
    .local v31, "leftDropTrX1":F
    const/16 v33, 0x0

    .line 494
    .local v33, "leftDropTrY1":F
    const/high16 v29, 0x3f800000    # 1.0f

    .line 495
    .local v29, "leftDropSc1":F
    const/high16 v45, 0x3f800000    # 1.0f

    .line 496
    .local v45, "rightDropSc1":F
    const/high16 v18, 0x3f800000    # 1.0f

    .line 498
    .local v18, "centerDrop3Sc1":F
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mIsActiveAnimationThread:Z

    if-eqz v3, :cond_5

    .line 499
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mAnimators:Ljava/util/HashMap;

    const/16 v4, 0x21

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/animation/ValueAnimator;

    .line 500
    .local v10, "ani":Landroid/animation/ValueAnimator;
    if-eqz v10, :cond_2

    .line 501
    invoke-virtual {v10}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v25

    .line 504
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mAnimators:Ljava/util/HashMap;

    const/16 v4, 0x11

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "ani":Landroid/animation/ValueAnimator;
    check-cast v10, Landroid/animation/ValueAnimator;

    .line 505
    .restart local v10    # "ani":Landroid/animation/ValueAnimator;
    if-eqz v10, :cond_3

    .line 506
    const-string v3, "transX"

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v22

    .line 507
    const-string v3, "transY"

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v23

    .line 510
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mAnimators:Ljava/util/HashMap;

    const/16 v4, 0x22

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "ani":Landroid/animation/ValueAnimator;
    check-cast v10, Landroid/animation/ValueAnimator;

    .line 511
    .restart local v10    # "ani":Landroid/animation/ValueAnimator;
    if-eqz v10, :cond_4

    .line 512
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPreFixProperty1:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "leftFlowerTrX"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v38

    .line 513
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPreFixProperty1:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "leftFlowerTrY"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v40

    .line 514
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPreFixProperty1:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "leftFlowerSc"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v36

    .line 515
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPreFixProperty1:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "leftDropTrX"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v30

    .line 516
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPreFixProperty1:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "centerDropTrX"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v19

    .line 517
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPreFixProperty1:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "leftDropTrY"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v32

    .line 518
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPreFixProperty1:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "leftDropSc"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v28

    .line 519
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPreFixProperty1:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "rightDropSc"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v44

    .line 520
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPreFixProperty1:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "centerDropTrY2"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v20

    .line 521
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPreFixProperty1:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "centerDrop1Sc"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v12

    .line 522
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPreFixProperty1:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "centerDrop2Sc"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v14

    .line 523
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPreFixProperty1:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "centerDrop3Sc"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v17

    .line 527
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mAnimators:Ljava/util/HashMap;

    const/16 v4, 0x25

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "ani":Landroid/animation/ValueAnimator;
    check-cast v10, Landroid/animation/ValueAnimator;

    .line 528
    .restart local v10    # "ani":Landroid/animation/ValueAnimator;
    if-eqz v10, :cond_5

    .line 529
    const-string v3, "leftFlowerTrX1"

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v39

    .line 530
    const-string v3, "leftFlowerTrY1"

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v41

    .line 531
    const-string v3, "leftFlowerSc1"

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v37

    .line 532
    const-string v3, "leftDropTrX"

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v31

    .line 533
    const-string v3, "leftDropTrY1"

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v33

    .line 534
    const-string v3, "leftDropSc1"

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v29

    .line 535
    const-string v3, "rightDropSc1"

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v45

    .line 536
    const-string v3, "centerDrop1Sc1"

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v18

    .line 542
    .end local v10    # "ani":Landroid/animation/ValueAnimator;
    :cond_5
    const/16 v9, 0x1f

    .line 544
    .local v9, "saveFlag":I
    const/16 v9, 0xf

    .line 547
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v3

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v3

    const/high16 v3, 0x43960000    # 300.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v6, v3

    const/high16 v3, 0x43960000    # 300.0f

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v7, v3

    const/4 v8, 0x0

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v9}, Landroid/graphics/Canvas;->saveLayer(FFFFLandroid/graphics/Paint;I)I

    move-result v48

    .line 550
    .local v48, "transparentSaveLevel":I
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v34

    .line 551
    .local v34, "leftFlowerLevel":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v3, v3, v38

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v4, v4, v40

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 552
    const/high16 v3, 0x422c0000    # 43.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v4

    const/high16 v4, 0x43290000    # 169.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v5

    move-object/from16 v0, p1

    move/from16 v1, v36

    move/from16 v2, v36

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 553
    const/high16 v3, 0x42540000    # 53.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v4

    const/high16 v4, 0x43290000    # 169.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v5

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1, v3, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 555
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->drawLeftFlower(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 556
    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 558
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v46

    .line 559
    .local v46, "rightFlowerLevel":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v3, v3, v38

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v4, v4, v40

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 560
    const/high16 v3, 0x43180000    # 152.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v4

    const/high16 v4, 0x43320000    # 178.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v5

    move-object/from16 v0, p1

    move/from16 v1, v36

    move/from16 v2, v36

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 561
    move/from16 v0, v25

    neg-float v3, v0

    const/high16 v4, 0x430e0000    # 142.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v5

    const/high16 v5, 0x43320000    # 178.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 562
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->drawRightFlower(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 563
    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 565
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v26

    .line 566
    .local v26, "leftDropLevel":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v3, v3, v30

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v4, v4, v32

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 567
    const v3, 0x4276ae14    # 61.67f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x430d1eb8    # 141.12f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v5

    move-object/from16 v0, p1

    move/from16 v1, v28

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 568
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->drawLeftDrop(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 569
    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 571
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v42

    .line 572
    .local v42, "rightDropLevel":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v3, v3, v30

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v4, v4, v32

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 573
    const v3, 0x43177d71    # 151.49f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x430c6148    # 140.38f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v5

    move-object/from16 v0, p1

    move/from16 v1, v44

    move/from16 v2, v44

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 574
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->drawRightDrop(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 575
    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 577
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v11

    .line 578
    .local v11, "centerDrop1Level":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v3, v3, v30

    const/high16 v4, 0x40400000    # 3.0f

    sub-float v4, v32, v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 579
    const v3, 0x42d7bd71    # 107.87f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x430ddc29    # 141.86f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v12, v3, v4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 580
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->drawCenterDrop1(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 581
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 583
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v13

    .line 584
    .local v13, "centerDrop2Level":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v3, v3, v19

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v4, v4, v20

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 585
    const v3, 0x42c3cccd    # 97.9f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4324051f    # 164.02f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v14, v3, v4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 586
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->drawCenterDrop2(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 587
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 589
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v15

    .line 590
    .local v15, "centerDrop3Level":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v3, v3, v38

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v4, v4, v40

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 591
    const v3, 0x42af199a    # 87.55f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x433aee14    # 186.93f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v5

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 592
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->drawCenterDrop3(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 593
    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 596
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->isStart2ndAni:Z

    if-eqz v3, :cond_6

    .line 597
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v35

    .line 598
    .local v35, "leftFlowerLevel1":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v3, v3, v39

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v4, v4, v41

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 599
    const/high16 v3, 0x422c0000    # 43.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v4

    const/high16 v4, 0x43290000    # 169.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v5

    move-object/from16 v0, p1

    move/from16 v1, v37

    move/from16 v2, v37

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 600
    const/high16 v3, 0x42540000    # 53.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v4

    const/high16 v4, 0x43290000    # 169.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v5

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1, v3, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 601
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->drawLeftFlower(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 602
    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 604
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v47

    .line 605
    .local v47, "rightFlowerLevel1":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v3, v3, v39

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v4, v4, v41

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 606
    const/high16 v3, 0x43180000    # 152.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v4

    const/high16 v4, 0x43320000    # 178.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v5

    move-object/from16 v0, p1

    move/from16 v1, v37

    move/from16 v2, v37

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 607
    move/from16 v0, v25

    neg-float v3, v0

    const/high16 v4, 0x430e0000    # 142.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v5

    const/high16 v5, 0x43320000    # 178.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v5, v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 608
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->drawRightFlower(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 609
    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 611
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v27

    .line 612
    .local v27, "leftDropLevel1":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v3, v3, v31

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v4, v4, v33

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 613
    const v3, 0x4276ae14    # 61.67f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x430d1eb8    # 141.12f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v5

    move-object/from16 v0, p1

    move/from16 v1, v29

    move/from16 v2, v29

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 614
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->drawLeftDrop(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 615
    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 617
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v43

    .line 618
    .local v43, "rightDropLevel1":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v3, v3, v31

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v4, v4, v33

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 619
    const v3, 0x43177d71    # 151.49f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x430c6148    # 140.38f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v5

    move-object/from16 v0, p1

    move/from16 v1, v45

    move/from16 v2, v45

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 620
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->drawRightDrop(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 621
    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 623
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v16

    .line 624
    .local v16, "centerDrop3Level1":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v3, v3, v39

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v4, v4, v41

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 625
    const v3, 0x42af199a    # 87.55f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x433aee14    # 186.93f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float/2addr v4, v5

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 626
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->drawCenterDrop3(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 627
    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 631
    .end local v16    # "centerDrop3Level1":I
    .end local v27    # "leftDropLevel1":I
    .end local v35    # "leftFlowerLevel1":I
    .end local v43    # "rightDropLevel1":I
    .end local v47    # "rightFlowerLevel1":I
    :cond_6
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v3, v3, v22

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v4, v4, v23

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 632
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->drawTransparentArea(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 634
    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 636
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v21

    .line 637
    .local v21, "cloudSaveLevel":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v3, v3, v22

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    mul-float v4, v4, v23

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 638
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->drawCloud(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 640
    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 641
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->isRunning()Z

    move-result v3

    if-nez v3, :cond_0

    .line 642
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->cancelAnimation()V

    .line 643
    const-string v3, ""

    const-string v4, "cancel"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 646
    .end local v9    # "saveFlag":I
    .end local v11    # "centerDrop1Level":I
    .end local v12    # "centerDrop1Sc":F
    .end local v13    # "centerDrop2Level":I
    .end local v14    # "centerDrop2Sc":F
    .end local v15    # "centerDrop3Level":I
    .end local v17    # "centerDrop3Sc":F
    .end local v18    # "centerDrop3Sc1":F
    .end local v19    # "centerDropTrX":F
    .end local v20    # "centerDropTrY":F
    .end local v21    # "cloudSaveLevel":I
    .end local v22    # "cloudTrX":F
    .end local v23    # "cloudTrY":F
    .end local v25    # "leftAngle":F
    .end local v26    # "leftDropLevel":I
    .end local v28    # "leftDropSc":F
    .end local v29    # "leftDropSc1":F
    .end local v30    # "leftDropTrX":F
    .end local v31    # "leftDropTrX1":F
    .end local v32    # "leftDropTrY":F
    .end local v33    # "leftDropTrY1":F
    .end local v34    # "leftFlowerLevel":I
    .end local v36    # "leftFlowserSc":F
    .end local v37    # "leftFlowserSc1":F
    .end local v38    # "leftFlowserTrX":F
    .end local v39    # "leftFlowserTrX1":F
    .end local v40    # "leftFlowserTrY":F
    .end local v41    # "leftFlowserTrY1":F
    .end local v42    # "rightDropLevel":I
    .end local v44    # "rightDropSc":F
    .end local v45    # "rightDropSc1":F
    .end local v46    # "rightFlowerLevel":I
    .end local v48    # "transparentSaveLevel":I
    :catch_0
    move-exception v24

    .line 647
    .local v24, "e":Ljava/lang/Exception;
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0
.end method

.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 985
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x0

    .line 954
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onViewDetachedFromWindow : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 955
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mAnimators:Ljava/util/HashMap;

    if-eqz v3, :cond_2

    .line 956
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 957
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 958
    .local v2, "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 959
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 960
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    .line 961
    .local v0, "ani":Landroid/animation/ValueAnimator;
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 962
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 963
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    goto :goto_0

    .line 966
    .end local v0    # "ani":Landroid/animation/ValueAnimator;
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    :cond_1
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    .line 969
    .end local v2    # "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_2
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mAnimators:Ljava/util/HashMap;

    .line 970
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    if-eqz v3, :cond_3

    .line 971
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->cancel()V

    .line 972
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 974
    :cond_3
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    .line 975
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPaint:Landroid/graphics/Paint;

    .line 976
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPath:Landroid/graphics/Path;

    .line 977
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_4

    .line 978
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 980
    :cond_4
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mBitmap:Landroid/graphics/Bitmap;

    .line 981
    return-void
.end method

.method public setPaintColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 66
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPaintColor:I

    .line 67
    return-void
.end method

.method public setScale(F)V
    .locals 0
    .param p1, "scale"    # F

    .prologue
    .line 941
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    .line 942
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->init()V

    .line 943
    return-void
.end method

.method public setWidthAndHeight(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 950
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, p1, p2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 951
    .local v0, "l":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 952
    return-void
.end method

.method public startAnimation()V
    .locals 62

    .prologue
    .line 717
    const/16 v55, 0x0

    move/from16 v0, v55

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->isStop:Z

    .line 718
    const-string v55, ""

    move-object/from16 v0, v55

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mPreFixProperty1:Ljava/lang/String;

    .line 720
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mAnimators:Ljava/util/HashMap;

    move-object/from16 v55, v0

    invoke-virtual/range {v55 .. v55}, Ljava/util/HashMap;->size()I

    move-result v55

    if-lez v55, :cond_2

    .line 721
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mAnimators:Ljava/util/HashMap;

    move-object/from16 v55, v0

    invoke-virtual/range {v55 .. v55}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v55

    invoke-interface/range {v55 .. v55}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 722
    .local v6, "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v55

    if-eqz v55, :cond_1

    .line 723
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 724
    .local v5, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/animation/ValueAnimator;

    .line 725
    .local v2, "ani":Landroid/animation/ValueAnimator;
    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v55

    if-eqz v55, :cond_0

    .line 726
    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->end()V

    goto :goto_0

    .line 730
    .end local v2    # "ani":Landroid/animation/ValueAnimator;
    .end local v5    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mAnimators:Ljava/util/HashMap;

    move-object/from16 v55, v0

    invoke-virtual/range {v55 .. v55}, Ljava/util/HashMap;->clear()V

    .line 733
    .end local v6    # "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_2
    const/4 v2, 0x0

    .line 735
    .restart local v2    # "ani":Landroid/animation/ValueAnimator;
    const/16 v55, 0x2

    move/from16 v0, v55

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v55, v0

    const/16 v56, 0x0

    const-string v57, "transX"

    const/16 v58, 0x5

    move/from16 v0, v58

    new-array v0, v0, [F

    move-object/from16 v58, v0

    const/16 v59, 0x0

    const/16 v60, 0x0

    aput v60, v58, v59

    const/16 v59, 0x1

    const/high16 v60, -0x3ef80000    # -8.5f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    move/from16 v61, v0

    mul-float v60, v60, v61

    aput v60, v58, v59

    const/16 v59, 0x2

    const/16 v60, 0x0

    aput v60, v58, v59

    const/16 v59, 0x3

    const/high16 v60, 0x41080000    # 8.5f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    move/from16 v61, v0

    mul-float v60, v60, v61

    aput v60, v58, v59

    const/16 v59, 0x4

    const/16 v60, 0x0

    aput v60, v58, v59

    .line 736
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x1

    const-string v57, "transY"

    const/16 v58, 0x3

    move/from16 v0, v58

    new-array v0, v0, [F

    move-object/from16 v58, v0

    const/16 v59, 0x0

    const/16 v60, 0x0

    aput v60, v58, v59

    const/16 v59, 0x1

    const/high16 v60, -0x3fc00000    # -3.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    move/from16 v61, v0

    mul-float v60, v60, v61

    aput v60, v58, v59

    const/16 v59, 0x2

    const/16 v60, 0x0

    aput v60, v58, v59

    .line 737
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    .line 735
    invoke-static/range {v55 .. v55}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v2

    .line 740
    const-wide/16 v55, 0x1194

    move-wide/from16 v0, v55

    invoke-virtual {v2, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 741
    const/16 v55, -0x1

    move/from16 v0, v55

    invoke-virtual {v2, v0}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 742
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mListener:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v55, v0

    move-object/from16 v0, v55

    invoke-virtual {v2, v0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 743
    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->start()V

    .line 744
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mAnimators:Ljava/util/HashMap;

    move-object/from16 v55, v0

    const/16 v56, 0x11

    invoke-static/range {v56 .. v56}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v56

    move-object/from16 v0, v55

    move-object/from16 v1, v56

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 746
    const/16 v55, 0x3

    move/from16 v0, v55

    new-array v0, v0, [F

    move-object/from16 v55, v0

    const/16 v56, 0x0

    const/16 v57, 0x0

    aput v57, v55, v56

    const/16 v56, 0x1

    const v57, -0x3fd33333    # -2.7f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mScale:F

    move/from16 v58, v0

    mul-float v57, v57, v58

    aput v57, v55, v56

    const/16 v56, 0x2

    const/16 v57, 0x0

    aput v57, v55, v56

    invoke-static/range {v55 .. v55}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v55

    move-object/from16 v0, v55

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    .line 747
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    move-object/from16 v55, v0

    const/16 v56, -0x1

    invoke-virtual/range {v55 .. v56}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 748
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    move-object/from16 v55, v0

    const-wide/16 v56, 0x3e8

    invoke-virtual/range {v55 .. v57}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 749
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    move-object/from16 v55, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mListener:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v56, v0

    invoke-virtual/range {v55 .. v56}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 750
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    move-object/from16 v55, v0

    invoke-virtual/range {v55 .. v55}, Landroid/animation/ValueAnimator;->start()V

    .line 752
    const/16 v55, 0x2

    move/from16 v0, v55

    new-array v0, v0, [F

    move-object/from16 v55, v0

    fill-array-data v55, :array_0

    invoke-static/range {v55 .. v55}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v2

    .line 753
    const-wide/16 v55, 0xbb8

    move-wide/from16 v0, v55

    invoke-virtual {v2, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 754
    const/16 v55, -0x1

    move/from16 v0, v55

    invoke-virtual {v2, v0}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 755
    new-instance v55, Landroid/view/animation/LinearInterpolator;

    invoke-direct/range {v55 .. v55}, Landroid/view/animation/LinearInterpolator;-><init>()V

    move-object/from16 v0, v55

    invoke-virtual {v2, v0}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 756
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mListener:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v55, v0

    move-object/from16 v0, v55

    invoke-virtual {v2, v0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 757
    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->start()V

    .line 758
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mAnimators:Ljava/util/HashMap;

    move-object/from16 v55, v0

    const/16 v56, 0x21

    invoke-static/range {v56 .. v56}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v56

    move-object/from16 v0, v55

    move-object/from16 v1, v56

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 760
    const/16 v55, 0x0

    const/high16 v56, 0x41700000    # 15.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v40

    .line 761
    .local v40, "kFlowerTrX1":Landroid/animation/Keyframe;
    const v55, 0x3f4ccccd    # 0.8f

    const/high16 v56, -0x3e600000    # -20.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v41

    .line 762
    .local v41, "kFlowerTrX2":Landroid/animation/Keyframe;
    const/high16 v55, 0x3f800000    # 1.0f

    const/high16 v56, -0x3e600000    # -20.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v42

    .line 764
    .local v42, "kFlowerTrX3":Landroid/animation/Keyframe;
    const/16 v55, 0x0

    const/high16 v56, -0x3d7e0000    # -65.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v43

    .line 765
    .local v43, "kFlowerTrY1":Landroid/animation/Keyframe;
    const v55, 0x3f4ccccd    # 0.8f

    const/high16 v56, 0x41f00000    # 30.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v44

    .line 766
    .local v44, "kFlowerTrY2":Landroid/animation/Keyframe;
    const/high16 v55, 0x3f800000    # 1.0f

    const/high16 v56, 0x41f00000    # 30.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v45

    .line 768
    .local v45, "kFlowerTrY3":Landroid/animation/Keyframe;
    const/16 v55, 0x0

    const/high16 v56, 0x3f800000    # 1.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v36

    .line 769
    .local v36, "kFlowerSc1":Landroid/animation/Keyframe;
    const v55, 0x3f19999a    # 0.6f

    const/high16 v56, 0x3f800000    # 1.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v37

    .line 770
    .local v37, "kFlowerSc2":Landroid/animation/Keyframe;
    const v55, 0x3f4ccccd    # 0.8f

    const/16 v56, 0x0

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v38

    .line 771
    .local v38, "kFlowerSc3":Landroid/animation/Keyframe;
    const/high16 v55, 0x3f800000    # 1.0f

    const/16 v56, 0x0

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v39

    .line 773
    .local v39, "kFlowerSc4":Landroid/animation/Keyframe;
    const/16 v55, 0x0

    const/high16 v56, 0x41700000    # 15.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v30

    .line 774
    .local v30, "kDropTrX1":Landroid/animation/Keyframe;
    const v55, 0x3e4ccccd    # 0.2f

    const/high16 v56, 0x41700000    # 15.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v31

    .line 775
    .local v31, "kDropTrX2":Landroid/animation/Keyframe;
    const/high16 v55, 0x3f800000    # 1.0f

    const/high16 v56, -0x3e600000    # -20.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v32

    .line 777
    .local v32, "kDropTrX3":Landroid/animation/Keyframe;
    const/16 v55, 0x0

    const/high16 v56, 0x41700000    # 15.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v22

    .line 778
    .local v22, "kCenterTrX1":Landroid/animation/Keyframe;
    const v55, 0x3dcccccd    # 0.1f

    const/high16 v56, 0x41700000    # 15.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v23

    .line 779
    .local v23, "kCenterTrX2":Landroid/animation/Keyframe;
    const v55, 0x3f666666    # 0.9f

    const/high16 v56, -0x3e600000    # -20.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v24

    .line 780
    .local v24, "kCenterTrX3":Landroid/animation/Keyframe;
    const/high16 v55, 0x3f800000    # 1.0f

    const/high16 v56, -0x3e600000    # -20.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v25

    .line 782
    .local v25, "kCenterTrX4":Landroid/animation/Keyframe;
    const/16 v55, 0x0

    const/high16 v56, -0x3df40000    # -35.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v33

    .line 783
    .local v33, "kDropTrY1":Landroid/animation/Keyframe;
    const v55, 0x3ecccccd    # 0.4f

    const/high16 v56, -0x3df40000    # -35.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v34

    .line 784
    .local v34, "kDropTrY2":Landroid/animation/Keyframe;
    const/high16 v55, 0x3f800000    # 1.0f

    const/high16 v56, 0x42700000    # 60.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v35

    .line 786
    .local v35, "kDropTrY3":Landroid/animation/Keyframe;
    const/16 v55, 0x0

    const/high16 v56, 0x3f800000    # 1.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v46

    .line 787
    .local v46, "kLeftDropSc1":Landroid/animation/Keyframe;
    const v55, 0x3e4ccccd    # 0.2f

    const/high16 v56, 0x3f800000    # 1.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v47

    .line 788
    .local v47, "kLeftDropSc2":Landroid/animation/Keyframe;
    const v55, 0x3f333333    # 0.7f

    const/high16 v56, 0x3fc00000    # 1.5f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v48

    .line 789
    .local v48, "kLeftDropSc3":Landroid/animation/Keyframe;
    const v55, 0x3f4ccccd    # 0.8f

    const/high16 v56, 0x3fc00000    # 1.5f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v49

    .line 790
    .local v49, "kLeftDropSc4":Landroid/animation/Keyframe;
    const/high16 v55, 0x3f800000    # 1.0f

    const/16 v56, 0x0

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v50

    .line 792
    .local v50, "kLeftDropSc5":Landroid/animation/Keyframe;
    const/16 v55, 0x0

    const/high16 v56, 0x3f800000    # 1.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v51

    .line 793
    .local v51, "kRightDropSc1":Landroid/animation/Keyframe;
    const v55, 0x3f4ccccd    # 0.8f

    const/high16 v56, 0x3f800000    # 1.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v52

    .line 794
    .local v52, "kRightDropSc2":Landroid/animation/Keyframe;
    const/high16 v55, 0x3f800000    # 1.0f

    const/16 v56, 0x0

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v53

    .line 796
    .local v53, "kRightDropSc3":Landroid/animation/Keyframe;
    const/16 v55, 0x0

    const/high16 v56, -0x3da40000    # -55.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v26

    .line 797
    .local v26, "kCenterTrY1":Landroid/animation/Keyframe;
    const v55, 0x3e4ccccd    # 0.2f

    const/high16 v56, -0x3da40000    # -55.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v27

    .line 798
    .local v27, "kCenterTrY2":Landroid/animation/Keyframe;
    const v55, 0x3f4ccccd    # 0.8f

    const/high16 v56, 0x42200000    # 40.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v28

    .line 799
    .local v28, "kCenterTrY3":Landroid/animation/Keyframe;
    const/high16 v55, 0x3f800000    # 1.0f

    const/high16 v56, 0x42200000    # 40.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v29

    .line 801
    .local v29, "kCenterTrY4":Landroid/animation/Keyframe;
    const/16 v55, 0x0

    const v56, 0x3f99999a    # 1.2f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v7

    .line 802
    .local v7, "kCenterSc11":Landroid/animation/Keyframe;
    const v55, 0x3e99999a    # 0.3f

    const/high16 v56, 0x3f000000    # 0.5f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v8

    .line 803
    .local v8, "kCenterSc12":Landroid/animation/Keyframe;
    const v55, 0x3f19999a    # 0.6f

    const v56, 0x3f99999a    # 1.2f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v9

    .line 804
    .local v9, "kCenterSc13":Landroid/animation/Keyframe;
    const v55, 0x3f666666    # 0.9f

    const/high16 v56, 0x3f000000    # 0.5f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v10

    .line 805
    .local v10, "kCenterSc14":Landroid/animation/Keyframe;
    const/high16 v55, 0x3f800000    # 1.0f

    const/16 v56, 0x0

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v11

    .line 807
    .local v11, "kCenterSc15":Landroid/animation/Keyframe;
    const/16 v55, 0x0

    const/high16 v56, 0x3f000000    # 0.5f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v12

    .line 808
    .local v12, "kCenterSc21":Landroid/animation/Keyframe;
    const v55, 0x3e99999a    # 0.3f

    const v56, 0x3f99999a    # 1.2f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v13

    .line 809
    .local v13, "kCenterSc22":Landroid/animation/Keyframe;
    const v55, 0x3f19999a    # 0.6f

    const/high16 v56, 0x3f000000    # 0.5f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v14

    .line 810
    .local v14, "kCenterSc23":Landroid/animation/Keyframe;
    const v55, 0x3f666666    # 0.9f

    const/16 v56, 0x0

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v15

    .line 811
    .local v15, "kCenterSc24":Landroid/animation/Keyframe;
    const/high16 v55, 0x3f800000    # 1.0f

    const/16 v56, 0x0

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v16

    .line 813
    .local v16, "kCenterSc25":Landroid/animation/Keyframe;
    const/16 v55, 0x0

    const v56, 0x3f99999a    # 1.2f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v17

    .line 814
    .local v17, "kCenterSc31":Landroid/animation/Keyframe;
    const v55, 0x3e99999a    # 0.3f

    const/high16 v56, 0x3f000000    # 0.5f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v18

    .line 815
    .local v18, "kCenterSc32":Landroid/animation/Keyframe;
    const v55, 0x3f19999a    # 0.6f

    const v56, 0x3f99999a    # 1.2f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v19

    .line 816
    .local v19, "kCenterSc33":Landroid/animation/Keyframe;
    const v55, 0x3f666666    # 0.9f

    const/16 v56, 0x0

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v20

    .line 817
    .local v20, "kCenterSc34":Landroid/animation/Keyframe;
    const/high16 v55, 0x3f800000    # 1.0f

    const/16 v56, 0x0

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v21

    .line 819
    .local v21, "kCenterSc35":Landroid/animation/Keyframe;
    const v55, 0x3dcccccd    # 0.1f

    const/16 v56, 0x0

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v4

    .line 820
    .local v4, "ekTransValue1":Landroid/animation/Keyframe;
    const v55, 0x3d4ccccd    # 0.05f

    const/high16 v56, 0x3f800000    # 1.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v3

    .line 822
    .local v3, "ekScaleValue1":Landroid/animation/Keyframe;
    const/16 v55, 0x18

    move/from16 v0, v55

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v55, v0

    const/16 v56, 0x0

    const-string v57, "leftFlowerTrX"

    const/16 v58, 0x3

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v40, v58, v59

    const/16 v59, 0x1

    aput-object v41, v58, v59

    const/16 v59, 0x2

    aput-object v42, v58, v59

    .line 823
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x1

    const-string v57, "leftFlowerTrY"

    const/16 v58, 0x3

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v43, v58, v59

    const/16 v59, 0x1

    aput-object v44, v58, v59

    const/16 v59, 0x2

    aput-object v45, v58, v59

    .line 824
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x2

    const-string v57, "leftFlowerSc"

    const/16 v58, 0x4

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v36, v58, v59

    const/16 v59, 0x1

    aput-object v37, v58, v59

    const/16 v59, 0x2

    aput-object v38, v58, v59

    const/16 v59, 0x3

    aput-object v39, v58, v59

    .line 825
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x3

    const-string v57, "leftDropTrX"

    const/16 v58, 0x3

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v30, v58, v59

    const/16 v59, 0x1

    aput-object v31, v58, v59

    const/16 v59, 0x2

    aput-object v32, v58, v59

    .line 826
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x4

    const-string v57, "centerDropTrX"

    const/16 v58, 0x4

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v22, v58, v59

    const/16 v59, 0x1

    aput-object v23, v58, v59

    const/16 v59, 0x2

    aput-object v24, v58, v59

    const/16 v59, 0x3

    aput-object v25, v58, v59

    .line 827
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x5

    const-string v57, "leftDropTrY"

    const/16 v58, 0x3

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v33, v58, v59

    const/16 v59, 0x1

    aput-object v34, v58, v59

    const/16 v59, 0x2

    aput-object v35, v58, v59

    .line 828
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x6

    const-string v57, "leftDropSc"

    const/16 v58, 0x5

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v46, v58, v59

    const/16 v59, 0x1

    aput-object v47, v58, v59

    const/16 v59, 0x2

    aput-object v48, v58, v59

    const/16 v59, 0x3

    aput-object v49, v58, v59

    const/16 v59, 0x4

    aput-object v50, v58, v59

    .line 829
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x7

    const-string v57, "rightDropSc"

    const/16 v58, 0x3

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v51, v58, v59

    const/16 v59, 0x1

    aput-object v52, v58, v59

    const/16 v59, 0x2

    aput-object v53, v58, v59

    .line 830
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x8

    const-string v57, "centerDropTrY2"

    const/16 v58, 0x4

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v26, v58, v59

    const/16 v59, 0x1

    aput-object v27, v58, v59

    const/16 v59, 0x2

    aput-object v28, v58, v59

    const/16 v59, 0x3

    aput-object v29, v58, v59

    .line 831
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x9

    const-string v57, "centerDrop1Sc"

    const/16 v58, 0x5

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v7, v58, v59

    const/16 v59, 0x1

    aput-object v8, v58, v59

    const/16 v59, 0x2

    aput-object v9, v58, v59

    const/16 v59, 0x3

    aput-object v10, v58, v59

    const/16 v59, 0x4

    aput-object v11, v58, v59

    .line 832
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0xa

    const-string v57, "centerDrop2Sc"

    const/16 v58, 0x5

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v12, v58, v59

    const/16 v59, 0x1

    aput-object v13, v58, v59

    const/16 v59, 0x2

    aput-object v14, v58, v59

    const/16 v59, 0x3

    aput-object v15, v58, v59

    const/16 v59, 0x4

    aput-object v16, v58, v59

    .line 833
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0xb

    const-string v57, "centerDrop3Sc"

    const/16 v58, 0x5

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v17, v58, v59

    const/16 v59, 0x1

    aput-object v18, v58, v59

    const/16 v59, 0x2

    aput-object v19, v58, v59

    const/16 v59, 0x3

    aput-object v20, v58, v59

    const/16 v59, 0x4

    aput-object v21, v58, v59

    .line 834
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0xc

    const-string v57, "e_leftFlowerTrX"

    const/16 v58, 0x2

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v40, v58, v59

    const/16 v59, 0x1

    aput-object v4, v58, v59

    .line 836
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0xd

    const-string v57, "e_leftFlowerTrY"

    const/16 v58, 0x2

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v43, v58, v59

    const/16 v59, 0x1

    aput-object v4, v58, v59

    .line 837
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0xe

    const-string v57, "e_leftFlowerSc"

    const/16 v58, 0x2

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v36, v58, v59

    const/16 v59, 0x1

    aput-object v3, v58, v59

    .line 838
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0xf

    const-string v57, "e_leftDropTrX"

    const/16 v58, 0x2

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v30, v58, v59

    const/16 v59, 0x1

    aput-object v4, v58, v59

    .line 839
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x10

    const-string v57, "e_centerDropTrX"

    const/16 v58, 0x2

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v22, v58, v59

    const/16 v59, 0x1

    aput-object v4, v58, v59

    .line 840
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x11

    const-string v57, "e_leftDropTrY"

    const/16 v58, 0x2

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v33, v58, v59

    const/16 v59, 0x1

    aput-object v4, v58, v59

    .line 841
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x12

    const-string v57, "e_leftDropSc"

    const/16 v58, 0x2

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v46, v58, v59

    const/16 v59, 0x1

    aput-object v3, v58, v59

    .line 842
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x13

    const-string v57, "e_rightDropSc"

    const/16 v58, 0x2

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v51, v58, v59

    const/16 v59, 0x1

    aput-object v3, v58, v59

    .line 843
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x14

    const-string v57, "e_centerDropTrY2"

    const/16 v58, 0x2

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v26, v58, v59

    const/16 v59, 0x1

    aput-object v4, v58, v59

    .line 844
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x15

    const-string v57, "e_centerDrop1Sc"

    const/16 v58, 0x2

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v7, v58, v59

    const/16 v59, 0x1

    aput-object v3, v58, v59

    .line 845
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x16

    const-string v57, "e_centerDrop2Sc"

    const/16 v58, 0x2

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v12, v58, v59

    const/16 v59, 0x1

    aput-object v3, v58, v59

    .line 846
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x17

    const-string v57, "e_centerDrop3Sc"

    const/16 v58, 0x2

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v17, v58, v59

    const/16 v59, 0x1

    aput-object v3, v58, v59

    .line 847
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    .line 822
    invoke-static/range {v55 .. v55}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v2

    .line 849
    const-wide/16 v55, 0xbb8

    move-wide/from16 v0, v55

    invoke-virtual {v2, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 850
    const/16 v55, -0x1

    move/from16 v0, v55

    invoke-virtual {v2, v0}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 851
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mListener:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v55, v0

    move-object/from16 v0, v55

    invoke-virtual {v2, v0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 852
    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->start()V

    .line 853
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mAnimators:Ljava/util/HashMap;

    move-object/from16 v55, v0

    const/16 v56, 0x22

    invoke-static/range {v56 .. v56}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v56

    move-object/from16 v0, v55

    move-object/from16 v1, v56

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 858
    const/16 v55, 0x8

    move/from16 v0, v55

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v55, v0

    const/16 v56, 0x0

    const-string v57, "leftFlowerTrX1"

    const/16 v58, 0x3

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v40, v58, v59

    const/16 v59, 0x1

    aput-object v41, v58, v59

    const/16 v59, 0x2

    aput-object v42, v58, v59

    .line 859
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x1

    const-string v57, "leftFlowerTrY1"

    const/16 v58, 0x3

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v43, v58, v59

    const/16 v59, 0x1

    aput-object v44, v58, v59

    const/16 v59, 0x2

    aput-object v45, v58, v59

    .line 860
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x2

    const-string v57, "leftFlowerSc1"

    const/16 v58, 0x3

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v36, v58, v59

    const/16 v59, 0x1

    aput-object v37, v58, v59

    const/16 v59, 0x2

    aput-object v38, v58, v59

    .line 861
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x3

    const-string v57, "leftDropTrX"

    const/16 v58, 0x3

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v30, v58, v59

    const/16 v59, 0x1

    aput-object v31, v58, v59

    const/16 v59, 0x2

    aput-object v32, v58, v59

    .line 862
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x4

    const-string v57, "leftDropTrY1"

    const/16 v58, 0x3

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v33, v58, v59

    const/16 v59, 0x1

    aput-object v34, v58, v59

    const/16 v59, 0x2

    aput-object v35, v58, v59

    .line 863
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x5

    const-string v57, "leftDropSc1"

    const/16 v58, 0x5

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v46, v58, v59

    const/16 v59, 0x1

    aput-object v47, v58, v59

    const/16 v59, 0x2

    aput-object v48, v58, v59

    const/16 v59, 0x3

    aput-object v49, v58, v59

    const/16 v59, 0x4

    aput-object v50, v58, v59

    .line 864
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x6

    const-string v57, "rightDropSc1"

    const/16 v58, 0x3

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v51, v58, v59

    const/16 v59, 0x1

    aput-object v52, v58, v59

    const/16 v59, 0x2

    aput-object v53, v58, v59

    .line 865
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x7

    const-string v57, "centerDrop1Sc1"

    const/16 v58, 0x5

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v12, v58, v59

    const/16 v59, 0x1

    aput-object v13, v58, v59

    const/16 v59, 0x2

    aput-object v14, v58, v59

    const/16 v59, 0x3

    aput-object v15, v58, v59

    const/16 v59, 0x4

    aput-object v16, v58, v59

    .line 866
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    .line 858
    invoke-static/range {v55 .. v55}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v2

    .line 868
    const-wide/16 v55, 0xbb8

    move-wide/from16 v0, v55

    invoke-virtual {v2, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 869
    const-wide/16 v55, 0x5dc

    move-wide/from16 v0, v55

    invoke-virtual {v2, v0, v1}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 870
    const/16 v55, -0x1

    move/from16 v0, v55

    invoke-virtual {v2, v0}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 871
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mListener2:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v55, v0

    move-object/from16 v0, v55

    invoke-virtual {v2, v0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 872
    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->start()V

    .line 873
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mAnimators:Ljava/util/HashMap;

    move-object/from16 v55, v0

    const/16 v56, 0x25

    invoke-static/range {v56 .. v56}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v56

    move-object/from16 v0, v55

    move-object/from16 v1, v56

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 877
    const/16 v55, 0x1

    move/from16 v0, v55

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->mIsActiveAnimationThread:Z

    .line 878
    new-instance v54, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView$3;

    move-object/from16 v0, v54

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView$3;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;)V

    .line 891
    .local v54, "t":Ljava/lang/Thread;
    const/16 v55, 0xa

    invoke-virtual/range {v54 .. v55}, Ljava/lang/Thread;->setPriority(I)V

    .line 892
    invoke-virtual/range {v54 .. v54}, Ljava/lang/Thread;->start()V

    .line 893
    return-void

    .line 752
    nop

    :array_0
    .array-data 4
        0x0
        0x43b40000    # 360.0f
    .end array-data
.end method

.method public stopAnimation()V
    .locals 1

    .prologue
    .line 916
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SnowIconView;->isStop:Z

    .line 917
    return-void
.end method
