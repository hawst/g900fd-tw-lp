.class Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$21;
.super Landroid/os/Handler;
.source "MenuAdd.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 789
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v4, 0x61a7

    .line 791
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->isActivityVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 792
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->hideLoadingDialog()V
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    .line 794
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    const-class v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 796
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "mapweatherzoom5"

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .line 797
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    move-result-object v2

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->getMapZoomList(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 796
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 798
    const-string v1, "mapweatherzoom6"

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .line 799
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    move-result-object v2

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->getMapZoomList(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 798
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 800
    const-string v1, "mapweatherzoom7"

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .line 801
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    move-result-object v2

    const/4 v3, 0x7

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->getMapZoomList(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 800
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 803
    const-string v1, "latitude"

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    iget-object v2, v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->point:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 804
    const-string v1, "longitude"

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    iget-object v2, v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->point:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 805
    const-string v1, "tempscale"

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->tempscale:I
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 806
    const-string v1, "launcher"

    const/16 v2, -0x5208

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 808
    const-string v1, "flags"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 810
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 812
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    invoke-virtual {v1, v0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->startActivityForResult(Landroid/content/Intent;I)V

    .line 815
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->clearResource()V

    .line 816
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$21;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;
    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2202(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    .line 822
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 820
    :cond_0
    const-string v1, ""

    const-string v2, "rOUT iAV f"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
