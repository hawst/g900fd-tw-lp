.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;
.super Landroid/view/View;
.source "ClearIconView.java"

# interfaces
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;


# instance fields
.field final STOP_REPEAT_COUNT:I

.field private TranslateAni:Landroid/animation/ValueAnimator;

.field private UnderLineAlphaAni1:Landroid/animation/ValueAnimator;

.field private UnderLineAlphaAni2:Landroid/animation/ValueAnimator;

.field private UnderLineAlphaAni3:Landroid/animation/ValueAnimator;

.field a1:I

.field a2:I

.field a3:I

.field cloudyX:F

.field isStop:Z

.field l:Landroid/animation/Animator$AnimatorListener;

.field private mIsActiveAnimationThread:Z

.field private mMoonPath:Landroid/graphics/Path;

.field private mPaint:Landroid/graphics/Paint;

.field private mPaintColor:I

.field public mPreFixProperty1:Ljava/lang/String;

.field public mPreFixProperty2:Ljava/lang/String;

.field public mPreFixProperty3:Ljava/lang/String;

.field private mScale:F

.field private mUnderLine1:Landroid/graphics/Path;

.field private mUnderLine2:Landroid/graphics/Path;

.field private mUnderLine3:Landroid/graphics/Path;

.field t1:F

.field t2:F

.field t3:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attributeset"    # Landroid/util/AttributeSet;

    .prologue
    .line 29
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attributeset"    # Landroid/util/AttributeSet;
    .param p3, "i"    # I

    .prologue
    const/16 v4, 0xff

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 33
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    .line 49
    iput-boolean v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mIsActiveAnimationThread:Z

    .line 51
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    .line 53
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni1:Landroid/animation/ValueAnimator;

    .line 55
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni2:Landroid/animation/ValueAnimator;

    .line 57
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni3:Landroid/animation/ValueAnimator;

    .line 59
    const v0, -0xa0a0b

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mPaintColor:I

    .line 164
    iput-boolean v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->isStop:Z

    .line 165
    iput v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->STOP_REPEAT_COUNT:I

    .line 170
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->l:Landroid/animation/Animator$AnimatorListener;

    .line 200
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mPreFixProperty1:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mPreFixProperty2:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mPreFixProperty3:Ljava/lang/String;

    .line 291
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->cloudyX:F

    .line 293
    iput v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->a1:I

    .line 294
    iput v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->a2:I

    .line 295
    iput v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->a3:I

    .line 297
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->t1:F

    .line 298
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->t2:F

    .line 299
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->t3:F

    .line 34
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->init()V

    .line 35
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni1:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni2:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni3:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;

    .prologue
    .line 23
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mIsActiveAnimationThread:Z

    return v0
.end method

.method private init()V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->initPaint()V

    .line 67
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->initPath()V

    .line 68
    invoke-virtual {p0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 69
    return-void
.end method

.method private initPaint()V
    .locals 3

    .prologue
    .line 157
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mPaint:Landroid/graphics/Paint;

    .line 158
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 159
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 160
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41400000    # 12.0f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 161
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mPaintColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 162
    return-void
.end method

.method private initPath()V
    .locals 13

    .prologue
    const v12, 0x43189efa

    const v11, 0x43485893

    const/high16 v10, 0x43320000    # 178.0f

    const v9, 0x43209efa

    const v8, 0x419d3b64    # 19.654f

    .line 73
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mMoonPath:Landroid/graphics/Path;

    .line 74
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mMoonPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 75
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mMoonPath:Landroid/graphics/Path;

    const v1, 0x430841cb

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41fc9581    # 31.573f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 76
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mMoonPath:Landroid/graphics/Path;

    const v1, 0x431729ba

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42248c4a

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4320970a    # 160.59f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42674ed9    # 57.827f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4320970a    # 160.59f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42981604    # 76.043f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 78
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mMoonPath:Landroid/graphics/Path;

    const v1, 0x4320970a    # 160.59f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42d24d50    # 105.151f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4308e873

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4300d4fe    # 128.832f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42d7999a    # 107.8f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4300d4fe    # 128.832f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 80
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mMoonPath:Landroid/graphics/Path;

    const v1, 0x42b32b85    # 89.585f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4300d4fe    # 128.832f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4291cac1

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42eecfdf

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x427d52f2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42d10083    # 104.501f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 82
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mMoonPath:Landroid/graphics/Path;

    const v1, 0x4284849c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42d1ced9    # 104.904f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x428a71aa    # 69.222f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42d23646

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42906b02    # 72.209f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42d23646

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 84
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mMoonPath:Landroid/graphics/Path;

    const v1, 0x42d7b74c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42d23646

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4308dc6a

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x429834bc

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4308dc6a

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4221cfdf    # 40.453f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 86
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mMoonPath:Landroid/graphics/Path;

    const v1, 0x4308dc6a

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4215dd2f    # 37.466f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4308a8b4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x420a020c

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x430841cb

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x41fc9581    # 31.573f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 88
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mMoonPath:Landroid/graphics/Path;

    const v1, 0x43008831

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 89
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mMoonPath:Landroid/graphics/Path;

    const v1, 0x42fffc6a

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v8

    const v3, 0x42feee14

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x419eeb85    # 19.865f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42fe22d1    # 127.068f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x41a2126f    # 20.259f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 91
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mMoonPath:Landroid/graphics/Path;

    const v1, 0x42fd0083    # 126.501f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41a6978d    # 20.824f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42fca0c5

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x41ad4fdf    # 21.664f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42fd2a7f    # 126.583f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x41b351ec    # 22.415f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 93
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mMoonPath:Landroid/graphics/Path;

    const v1, 0x4300c312

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41e426e9    # 28.519f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4300dc6a

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4207b22d    # 33.924f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4300dc6a

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4221ced9    # 40.452f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 95
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mMoonPath:Landroid/graphics/Path;

    const v1, 0x4300dc6a

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x428f624e

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42cee666    # 103.45f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42c235c3

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42906b02    # 72.209f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42c235c3

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 97
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mMoonPath:Landroid/graphics/Path;

    const v1, 0x42835e35

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42c235c3

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42711ba6    # 60.277f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42c20189

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4258b22d    # 54.174f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42bda6e9

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 99
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mMoonPath:Landroid/graphics/Path;

    const v1, 0x4257c9ba

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42bd7d71

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4256da1d

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42bd6979

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4255eb85    # 53.48f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42bd6979

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 101
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mMoonPath:Landroid/graphics/Path;

    const v1, 0x4253c396

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42bd6979

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4251a5e3

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42bdd581    # 94.917f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42501168

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42be9fbe

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 103
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mMoonPath:Landroid/graphics/Path;

    const v1, 0x424dcccd    # 51.45f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42bfc083    # 95.876f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x424d0d50    # 51.263f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42c16e14

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x424e20c5

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42c2ef9e

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 105
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mMoonPath:Landroid/graphics/Path;

    const v1, 0x42709375

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42f33852    # 121.61f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42a450e5

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4308d47b    # 136.83f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42d79a1d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4308d47b    # 136.83f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 107
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mMoonPath:Landroid/graphics/Path;

    const v1, 0x430d522d    # 141.321f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4308d47b    # 136.83f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4328974c

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42db1eb8    # 109.56f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4328974c

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x429814fe    # 76.041f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 109
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mMoonPath:Landroid/graphics/Path;

    const v1, 0x4328974c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4249978d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43195e77

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x41e3126f    # 28.384f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43013a1d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x419e2d0e    # 19.772f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 111
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mMoonPath:Landroid/graphics/Path;

    const v1, 0x4300ffbe

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x419d8b44    # 19.693f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4300c419

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v4, v8

    const v5, 0x43008831

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v6, v8

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 113
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mMoonPath:Landroid/graphics/Path;

    const v1, 0x43008831

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 115
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mUnderLine1:Landroid/graphics/Path;

    .line 116
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mUnderLine2:Landroid/graphics/Path;

    .line 117
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mUnderLine3:Landroid/graphics/Path;

    .line 119
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mUnderLine1:Landroid/graphics/Path;

    const v1, 0x42dcaa7f    # 110.333f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v9

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 120
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mUnderLine1:Landroid/graphics/Path;

    const v1, 0x42a1147b    # 80.54f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v9

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 121
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mUnderLine1:Landroid/graphics/Path;

    const v1, 0x429ca979

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v9

    const v3, 0x4299147b    # 76.54f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x431ed47b    # 158.83f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4299147b    # 76.54f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x431c9efa

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 123
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mUnderLine1:Landroid/graphics/Path;

    const v1, 0x4299147b    # 76.54f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x431a6979

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x429ca979

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v4, v12

    const v5, 0x42a1147b    # 80.54f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v6, v12

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 125
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mUnderLine1:Landroid/graphics/Path;

    const v1, 0x42dcaa7f    # 110.333f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v12

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mUnderLine1:Landroid/graphics/Path;

    const v1, 0x42e11581    # 112.542f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v12

    const v3, 0x42e4aa7f    # 114.333f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x431a6979

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42e4aa7f    # 114.333f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x431c9efa

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 128
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mUnderLine1:Landroid/graphics/Path;

    const v1, 0x42e4aa7f    # 114.333f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x431ed47b    # 158.83f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42e11581    # 112.542f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v4, v9

    const v5, 0x42dcaa7f    # 110.333f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v6, v9

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 131
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mUnderLine2:Landroid/graphics/Path;

    const v1, 0x431d8148

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v10

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 132
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mUnderLine2:Landroid/graphics/Path;

    const v1, 0x42e69893

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v10

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 133
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mUnderLine2:Landroid/graphics/Path;

    const v1, 0x42e22d91

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v10

    const v3, 0x42de9893

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43303581    # 176.209f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42de9893

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v5, v6

    const/high16 v6, 0x432e0000    # 174.0f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 135
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mUnderLine2:Landroid/graphics/Path;

    const v1, 0x42de9893

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432bca7f    # 171.791f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42e22d91

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v3, v4

    const/high16 v4, 0x432a0000    # 170.0f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42e69893

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v5, v6

    const/high16 v6, 0x432a0000    # 170.0f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 137
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mUnderLine2:Landroid/graphics/Path;

    const v1, 0x431d8148

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x432a0000    # 170.0f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 138
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mUnderLine2:Landroid/graphics/Path;

    const v1, 0x431fb6c9

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x432a0000    # 170.0f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43218148

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x432bca7f    # 171.791f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43218148

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v5, v6

    const/high16 v6, 0x432e0000    # 174.0f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 140
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mUnderLine2:Landroid/graphics/Path;

    const v1, 0x43218148

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43303581    # 176.209f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431fb6c9

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v4, v10

    const v5, 0x431d8148

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v6, v10

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 143
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mUnderLine3:Landroid/graphics/Path;

    const v1, 0x430dc76d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v11

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 144
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mUnderLine3:Landroid/graphics/Path;

    const v1, 0x42ab028f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v11

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 145
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mUnderLine3:Landroid/graphics/Path;

    const v1, 0x42a6978d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v11

    const v3, 0x42a3028f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43468e14

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42a3028f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43445893

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 147
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mUnderLine3:Landroid/graphics/Path;

    const v1, 0x42a3028f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43422312

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42a6978d

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43405893

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42ab028f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43405893

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 149
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mUnderLine3:Landroid/graphics/Path;

    const v1, 0x430dc76d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43405893

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 150
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mUnderLine3:Landroid/graphics/Path;

    const v1, 0x430ffcee

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43405893

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4311c76d

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43422312

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4311c76d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43445893

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 152
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mUnderLine3:Landroid/graphics/Path;

    const v1, 0x4311c76d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43468e14

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x430ffcee

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v4, v11

    const v5, 0x430dc76d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v6, v11

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 154
    return-void
.end method


# virtual methods
.method public cancelAnimation()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/16 v2, 0xff

    const/4 v1, 0x0

    .line 357
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 358
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 361
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni1:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 362
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 364
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni2:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 365
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 367
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni3:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 368
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 372
    :cond_3
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->cloudyX:F

    .line 374
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->a1:I

    .line 375
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->a2:I

    .line 376
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->a3:I

    .line 378
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->t1:F

    .line 379
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->t2:F

    .line 380
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->t3:F

    .line 381
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mIsActiveAnimationThread:Z

    .line 382
    return-void
.end method

.method public drewUnderLine(Landroid/graphics/Canvas;IFLandroid/graphics/Path;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "alpah"    # I
    .param p3, "transX"    # F
    .param p4, "p"    # Landroid/graphics/Path;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 344
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 345
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 346
    const/4 v0, 0x0

    invoke-virtual {p1, p3, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 347
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p4, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 348
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 349
    return-void
.end method

.method public getIsPalyAnimation()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 277
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni1:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni2:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni3:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    if-nez v1, :cond_1

    .line 286
    :cond_0
    :goto_0
    return v0

    .line 283
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni1:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni2:Landroid/animation/ValueAnimator;

    .line 284
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni3:Landroid/animation/ValueAnimator;

    .line 285
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    .line 286
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getScale()F
    .locals 1

    .prologue
    .line 394
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 352
    return-object p0
.end method

.method public isRunning()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 385
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->getIsPalyAnimation()Z

    move-result v0

    return v0
.end method

.method public makeAnimation()V
    .locals 15

    .prologue
    .line 202
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->isStop:Z

    .line 203
    const-string v8, ""

    iput-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mPreFixProperty1:Ljava/lang/String;

    .line 204
    const-string v8, ""

    iput-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mPreFixProperty2:Ljava/lang/String;

    .line 205
    const-string v8, ""

    iput-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mPreFixProperty3:Ljava/lang/String;

    .line 207
    const/4 v8, 0x1

    new-array v8, v8, [Landroid/animation/PropertyValuesHolder;

    const/4 v9, 0x0

    const-string v10, "cloudX"

    const/4 v11, 0x5

    new-array v11, v11, [F

    const/4 v12, 0x0

    const/4 v13, 0x0

    aput v13, v11, v12

    const/4 v12, 0x1

    const/high16 v13, -0x3ee00000    # -10.0f

    iget v14, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v13, v14

    aput v13, v11, v12

    const/4 v12, 0x2

    const/4 v13, 0x0

    aput v13, v11, v12

    const/4 v12, 0x3

    const/high16 v13, 0x40e00000    # 7.0f

    iget v14, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v13, v14

    aput v13, v11, v12

    const/4 v12, 0x4

    const/4 v13, 0x0

    aput v13, v11, v12

    .line 208
    invoke-static {v10, v11}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v10

    aput-object v10, v8, v9

    .line 207
    invoke-static {v8}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    .line 209
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    const/4 v9, -0x1

    invoke-virtual {v8, v9}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 210
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    const-wide/16 v9, 0xbb8

    invoke-virtual {v8, v9, v10}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 211
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v8, v9}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 213
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v8}, Landroid/animation/ValueAnimator;->start()V

    .line 215
    const-wide/16 v2, 0xfa0

    .line 216
    .local v2, "duration":J
    const-wide/16 v8, 0x4

    div-long v0, v2, v8

    .line 218
    .local v0, "delayGap":J
    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static {v8, v9}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v4

    .line 219
    .local v4, "key1":Landroid/animation/Keyframe;
    const v8, 0x3f19999a    # 0.6f

    const/high16 v9, 0x437f0000    # 255.0f

    invoke-static {v8, v9}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v5

    .line 220
    .local v5, "key4":Landroid/animation/Keyframe;
    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v9, 0x0

    invoke-static {v8, v9}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v6

    .line 221
    .local v6, "key5":Landroid/animation/Keyframe;
    const/high16 v8, 0x3f800000    # 1.0f

    const/high16 v9, 0x437f0000    # 255.0f

    invoke-static {v8, v9}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v7

    .line 222
    .local v7, "key6":Landroid/animation/Keyframe;
    const/4 v8, 0x4

    new-array v8, v8, [Landroid/animation/PropertyValuesHolder;

    const/4 v9, 0x0

    const-string v10, "value"

    const/4 v11, 0x3

    new-array v11, v11, [Landroid/animation/Keyframe;

    const/4 v12, 0x0

    aput-object v4, v11, v12

    const/4 v12, 0x1

    aput-object v5, v11, v12

    const/4 v12, 0x2

    aput-object v6, v11, v12

    .line 223
    invoke-static {v10, v11}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const-string v10, "e_value"

    const/4 v11, 0x3

    new-array v11, v11, [Landroid/animation/Keyframe;

    const/4 v12, 0x0

    aput-object v4, v11, v12

    const/4 v12, 0x1

    aput-object v5, v11, v12

    const/4 v12, 0x2

    aput-object v7, v11, v12

    .line 224
    invoke-static {v10, v11}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    const-string v10, "trans"

    const/4 v11, 0x2

    new-array v11, v11, [F

    const/4 v12, 0x0

    const/high16 v13, -0x3e600000    # -20.0f

    iget v14, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v13, v14

    aput v13, v11, v12

    const/4 v12, 0x1

    const/high16 v13, 0x41a00000    # 20.0f

    iget v14, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v13, v14

    aput v13, v11, v12

    .line 225
    invoke-static {v10, v11}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x3

    const-string v10, "e_trans"

    const/4 v11, 0x2

    new-array v11, v11, [F

    const/4 v12, 0x0

    const/high16 v13, -0x3e600000    # -20.0f

    iget v14, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v13, v14

    aput v13, v11, v12

    const/4 v12, 0x1

    const/4 v13, 0x0

    iget v14, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v13, v14

    aput v13, v11, v12

    .line 226
    invoke-static {v10, v11}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v10

    aput-object v10, v8, v9

    .line 222
    invoke-static {v8}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni1:Landroid/animation/ValueAnimator;

    .line 227
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni1:Landroid/animation/ValueAnimator;

    const/4 v9, -0x1

    invoke-virtual {v8, v9}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 228
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni1:Landroid/animation/ValueAnimator;

    invoke-virtual {v8, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 229
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni1:Landroid/animation/ValueAnimator;

    invoke-virtual {v8}, Landroid/animation/ValueAnimator;->start()V

    .line 230
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni1:Landroid/animation/ValueAnimator;

    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v8, v9}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 232
    const/4 v8, 0x4

    new-array v8, v8, [Landroid/animation/PropertyValuesHolder;

    const/4 v9, 0x0

    const-string v10, "value"

    const/4 v11, 0x3

    new-array v11, v11, [Landroid/animation/Keyframe;

    const/4 v12, 0x0

    aput-object v4, v11, v12

    const/4 v12, 0x1

    aput-object v5, v11, v12

    const/4 v12, 0x2

    aput-object v6, v11, v12

    .line 233
    invoke-static {v10, v11}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const-string v10, "e_value"

    const/4 v11, 0x3

    new-array v11, v11, [Landroid/animation/Keyframe;

    const/4 v12, 0x0

    aput-object v4, v11, v12

    const/4 v12, 0x1

    aput-object v5, v11, v12

    const/4 v12, 0x2

    aput-object v7, v11, v12

    .line 234
    invoke-static {v10, v11}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    const-string v10, "trans"

    const/4 v11, 0x2

    new-array v11, v11, [F

    const/4 v12, 0x0

    const/high16 v13, 0x41a00000    # 20.0f

    iget v14, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v13, v14

    aput v13, v11, v12

    const/4 v12, 0x1

    const/high16 v13, -0x3ee00000    # -10.0f

    iget v14, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v13, v14

    aput v13, v11, v12

    .line 235
    invoke-static {v10, v11}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x3

    const-string v10, "e_trans"

    const/4 v11, 0x2

    new-array v11, v11, [F

    const/4 v12, 0x0

    const/high16 v13, 0x41a00000    # 20.0f

    iget v14, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v13, v14

    aput v13, v11, v12

    const/4 v12, 0x1

    const/4 v13, 0x0

    iget v14, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v13, v14

    aput v13, v11, v12

    .line 236
    invoke-static {v10, v11}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v10

    aput-object v10, v8, v9

    .line 232
    invoke-static {v8}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni2:Landroid/animation/ValueAnimator;

    .line 237
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni2:Landroid/animation/ValueAnimator;

    const/4 v9, -0x1

    invoke-virtual {v8, v9}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 238
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni2:Landroid/animation/ValueAnimator;

    invoke-virtual {v8, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 239
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni2:Landroid/animation/ValueAnimator;

    invoke-virtual {v8, v0, v1}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 240
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni2:Landroid/animation/ValueAnimator;

    invoke-virtual {v8}, Landroid/animation/ValueAnimator;->start()V

    .line 241
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni2:Landroid/animation/ValueAnimator;

    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v8, v9}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 243
    const/4 v8, 0x4

    new-array v8, v8, [Landroid/animation/PropertyValuesHolder;

    const/4 v9, 0x0

    const-string v10, "value"

    const/4 v11, 0x3

    new-array v11, v11, [Landroid/animation/Keyframe;

    const/4 v12, 0x0

    aput-object v4, v11, v12

    const/4 v12, 0x1

    aput-object v5, v11, v12

    const/4 v12, 0x2

    aput-object v6, v11, v12

    .line 244
    invoke-static {v10, v11}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const-string v10, "e_value"

    const/4 v11, 0x3

    new-array v11, v11, [Landroid/animation/Keyframe;

    const/4 v12, 0x0

    aput-object v4, v11, v12

    const/4 v12, 0x1

    aput-object v5, v11, v12

    const/4 v12, 0x2

    aput-object v7, v11, v12

    .line 245
    invoke-static {v10, v11}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    const-string v10, "trans"

    const/4 v11, 0x2

    new-array v11, v11, [F

    const/4 v12, 0x0

    const/high16 v13, 0x41a00000    # 20.0f

    iget v14, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v13, v14

    aput v13, v11, v12

    const/4 v12, 0x1

    const/high16 v13, -0x3ee00000    # -10.0f

    iget v14, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v13, v14

    aput v13, v11, v12

    .line 246
    invoke-static {v10, v11}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x3

    const-string v10, "e_trans"

    const/4 v11, 0x2

    new-array v11, v11, [F

    const/4 v12, 0x0

    const/high16 v13, 0x41a00000    # 20.0f

    iget v14, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v13, v14

    aput v13, v11, v12

    const/4 v12, 0x1

    const/4 v13, 0x0

    iget v14, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    mul-float/2addr v13, v14

    aput v13, v11, v12

    .line 247
    invoke-static {v10, v11}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v10

    aput-object v10, v8, v9

    .line 243
    invoke-static {v8}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni3:Landroid/animation/ValueAnimator;

    .line 248
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni3:Landroid/animation/ValueAnimator;

    const/4 v9, -0x1

    invoke-virtual {v8, v9}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 249
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni3:Landroid/animation/ValueAnimator;

    invoke-virtual {v8, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 250
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni3:Landroid/animation/ValueAnimator;

    const-wide/16 v9, 0x2

    mul-long/2addr v9, v0

    invoke-virtual {v8, v9, v10}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 251
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni3:Landroid/animation/ValueAnimator;

    invoke-virtual {v8}, Landroid/animation/ValueAnimator;->start()V

    .line 252
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni3:Landroid/animation/ValueAnimator;

    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v8, v9}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 253
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v2, 0x0

    .line 303
    :try_start_0
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_1

    .line 304
    const-string v1, ""

    const-string v2, "scale is less then 0"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    :cond_0
    :goto_0
    return-void

    .line 308
    :cond_1
    iget-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mIsActiveAnimationThread:Z

    if-eqz v1, :cond_2

    .line 309
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    const-string v2, "cloudX"

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->cloudyX:F

    .line 311
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni1:Landroid/animation/ValueAnimator;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mPreFixProperty1:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "value"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->intValue()I

    move-result v1

    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->a1:I

    .line 312
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni2:Landroid/animation/ValueAnimator;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mPreFixProperty2:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "value"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->intValue()I

    move-result v1

    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->a2:I

    .line 313
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni3:Landroid/animation/ValueAnimator;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mPreFixProperty3:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "value"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->intValue()I

    move-result v1

    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->a3:I

    .line 315
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni1:Landroid/animation/ValueAnimator;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mPreFixProperty1:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "trans"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->t1:F

    .line 316
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni2:Landroid/animation/ValueAnimator;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mPreFixProperty2:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "trans"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->t2:F

    .line 317
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni3:Landroid/animation/ValueAnimator;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mPreFixProperty3:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "trans"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->t3:F

    .line 321
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 322
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mMoonPath:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 323
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 324
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 325
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->cloudyX:F

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 327
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->a1:I

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->t1:F

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mUnderLine1:Landroid/graphics/Path;

    invoke-virtual {p0, p1, v1, v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->drewUnderLine(Landroid/graphics/Canvas;IFLandroid/graphics/Path;)V

    .line 328
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->a2:I

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->t2:F

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mUnderLine2:Landroid/graphics/Path;

    invoke-virtual {p0, p1, v1, v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->drewUnderLine(Landroid/graphics/Canvas;IFLandroid/graphics/Path;)V

    .line 329
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->a3:I

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->t3:F

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mUnderLine3:Landroid/graphics/Path;

    invoke-virtual {p0, p1, v1, v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->drewUnderLine(Landroid/graphics/Canvas;IFLandroid/graphics/Path;)V

    .line 331
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mPaint:Landroid/graphics/Paint;

    const/16 v2, 0xff

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 332
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 334
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->getIsPalyAnimation()Z

    move-result v1

    if-nez v1, :cond_0

    .line 335
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->cancelAnimation()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 338
    :catch_0
    move-exception v0

    .line 339
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0
.end method

.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 437
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 404
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onViewDetachedFromWindow : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mPaint:Landroid/graphics/Paint;

    .line 407
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mMoonPath:Landroid/graphics/Path;

    .line 408
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mUnderLine1:Landroid/graphics/Path;

    .line 409
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mUnderLine2:Landroid/graphics/Path;

    .line 410
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mUnderLine3:Landroid/graphics/Path;

    .line 412
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 413
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 414
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 417
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni1:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_1

    .line 418
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 419
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 421
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni2:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_2

    .line 422
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 423
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 425
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni3:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_3

    .line 426
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 427
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 430
    :cond_3
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    .line 431
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni1:Landroid/animation/ValueAnimator;

    .line 432
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni2:Landroid/animation/ValueAnimator;

    .line 433
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni3:Landroid/animation/ValueAnimator;

    .line 434
    return-void
.end method

.method public setPaintColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 62
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mPaintColor:I

    .line 63
    return-void
.end method

.method public setScale(F)V
    .locals 0
    .param p1, "scale"    # F

    .prologue
    .line 389
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mScale:F

    .line 390
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->init()V

    .line 391
    return-void
.end method

.method public setWidthAndHeight(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 398
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, p1, p2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 399
    .local v0, "l":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 400
    return-void
.end method

.method public startAnimation()V
    .locals 2

    .prologue
    .line 256
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->makeAnimation()V

    .line 257
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mIsActiveAnimationThread:Z

    .line 258
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;)V

    .line 272
    .local v0, "t":Ljava/lang/Thread;
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 273
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 274
    return-void
.end method

.method public stopAnimation()V
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->isStop:Z

    .line 168
    return-void
.end method
