.class Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$28;
.super Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultTwMultiSelectListener;
.source "WeatherClockCityListAnimationAndDelete.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    .prologue
    .line 2457
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultTwMultiSelectListener;-><init>()V

    return-void
.end method


# virtual methods
.method public OnTwMultiSelectStart(II)V
    .locals 2
    .param p1, "startX"    # I
    .param p2, "startY"    # I

    .prologue
    .line 2459
    const-string v0, ""

    const-string v1, "OnTwMultiSelectStart"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2461
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->mTwDragSelectedItemArray:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->access$5600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;)Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2462
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->mTwDragSelectedItemArray:Ljava/util/ArrayList;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->access$5602(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 2464
    :cond_0
    return-void
.end method

.method public OnTwMultiSelectStop(II)V
    .locals 7
    .param p1, "endX"    # I
    .param p2, "endY"    # I

    .prologue
    const/4 v3, 0x1

    .line 2467
    const-string v2, ""

    const-string v4, "OnTwMultiSelectStop"

    invoke-static {v2, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2469
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->mIsDeleteMode:Z
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->access$900(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2470
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->mActionModeCallback:Landroid/view/ActionMode$Callback;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->access$1000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;)Landroid/view/ActionMode$Callback;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    .line 2473
    :cond_0
    const/4 v1, 0x0

    .line 2475
    .local v1, "myCityData":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->mTwDragSelectedItemArray:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->access$5600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2476
    .local v0, "mTwDragSelectedViewPosition":Ljava/lang/Integer;
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->mListAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$MyListAdapter;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$MyListAdapter;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v2, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$MyListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "myCityData":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;
    check-cast v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;

    .line 2478
    .restart local v1    # "myCityData":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;
    if-eqz v1, :cond_2

    .line 2479
    const-string v2, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mCD : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->isChecked()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2480
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->isChecked()Z

    move-result v2

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    invoke-virtual {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->setChecked(Z)V

    .line 2485
    :goto_2
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->mListAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$MyListAdapter;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$MyListAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$MyListAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 2480
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 2482
    :cond_2
    const-string v2, ""

    const-string v5, "mCD n"

    invoke-static {v2, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 2489
    .end local v0    # "mTwDragSelectedViewPosition":Ljava/lang/Integer;
    :cond_3
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->updateSelect()V

    .line 2490
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->setDeleteMenuVisible(Z)V
    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->access$1100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;Z)V

    .line 2492
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->mTwDragSelectedItemArray:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->access$5600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 2493
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->mTwDragSelectedItemArray:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->access$5600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 2495
    :cond_4
    return-void
.end method

.method public onTwMultiSelected(Lcom/sec/android/touchwiz/widget/TwAdapterView;Landroid/view/View;IJZZZ)V
    .locals 3
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .param p6, "isShiftpress"    # Z
    .param p7, "isCtrlpress"    # Z
    .param p8, "isPenpress"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/touchwiz/widget/TwAdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJZZZ)V"
        }
    .end annotation

    .prologue
    .line 2499
    .local p1, "parent":Lcom/sec/android/touchwiz/widget/TwAdapterView;, "Lcom/sec/android/touchwiz/widget/TwAdapterView<*>;"
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onTwMultiSelected call position = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  PenPress is = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2501
    if-eqz p8, :cond_0

    .line 2502
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    invoke-virtual {v0, p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->addDragItemToListArray(I)V

    .line 2504
    :cond_0
    return-void
.end method
