.class Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$30;
.super Landroid/os/Handler;
.source "WeatherClockDetail.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 3916
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 3918
    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-virtual {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->isActivityVisible()Z

    move-result v9

    if-eqz v9, :cond_6

    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$8100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    move-result-object v9

    if-eqz v9, :cond_6

    .line 3919
    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->hideRefreshDialog()V
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$8400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    .line 3920
    const-string v9, ""

    const-string v10, "all map info is received"

    invoke-static {v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3921
    new-instance v1, Landroid/content/Intent;

    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    iget-object v9, v9, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->detailContext:Landroid/app/Activity;

    const-class v10, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-direct {v1, v9, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3923
    .local v1, "intent":Landroid/content/Intent;
    const/4 v2, 0x0

    .line 3925
    .local v2, "isErrorPreLatLon":Z
    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mapitem:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$8700(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    move-result-object v9

    if-eqz v9, :cond_2

    .line 3926
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 3927
    .local v3, "item":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;>;"
    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mapitem:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$8700(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3929
    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$8100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    move-result-object v9

    const/4 v10, 0x5

    invoke-virtual {v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->getMapZoomList(I)Ljava/util/ArrayList;

    move-result-object v9

    invoke-static {v3, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->removeDuplicatedItem(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 3930
    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$8100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    move-result-object v9

    const/4 v10, 0x6

    invoke-virtual {v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->getMapZoomList(I)Ljava/util/ArrayList;

    move-result-object v9

    invoke-static {v3, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->removeDuplicatedItem(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 3931
    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$8100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    move-result-object v9

    const/4 v10, 0x7

    invoke-virtual {v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->getMapZoomList(I)Ljava/util/ArrayList;

    move-result-object v9

    invoke-static {v3, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->removeDuplicatedItem(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 3933
    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$8100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->getMapInfoUrlAddedCityInfoResult()I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_4

    .line 3934
    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mapitem:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$8700(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLatitude()Ljava/lang/String;

    move-result-object v4

    .line 3935
    .local v4, "lat":Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mapitem:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$8700(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLongitude()Ljava/lang/String;

    move-result-object v5

    .line 3937
    .local v5, "lon":Ljava/lang/String;
    if-eqz v4, :cond_3

    const-string v9, ""

    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    if-eqz v5, :cond_3

    const-string v9, ""

    .line 3938
    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 3939
    const-string v9, "pre_lat"

    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .line 3940
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mapitem:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    invoke-static {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$8700(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLatitude()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v10

    .line 3939
    invoke-virtual {v1, v9, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 3941
    const-string v9, "pre_lon"

    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .line 3942
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mapitem:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    invoke-static {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$8700(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLongitude()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v10

    .line 3941
    invoke-virtual {v1, v9, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 3952
    .end local v4    # "lat":Ljava/lang/String;
    .end local v5    # "lon":Ljava/lang/String;
    :goto_0
    if-eqz v2, :cond_1

    .line 3955
    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getMccCode()Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$8800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Ljava/lang/String;

    move-result-object v6

    .line 3956
    .local v6, "mcc":Ljava/lang/String;
    const/4 v8, 0x7

    .line 3958
    .local v8, "region":I
    if-eqz v6, :cond_0

    const-string v9, ""

    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    const-string v9, "000"

    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 3959
    :cond_0
    const-string v9, ""

    const-string v10, "using eur"

    invoke-static {v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3960
    sget-object v9, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Constants;->REGION_POINTS:[[I

    const/4 v10, 0x7

    aget-object v7, v9, v10

    .line 3967
    .local v7, "point":[I
    :goto_1
    const-string v9, "latitude"

    const/4 v10, 0x0

    aget v10, v7, v10

    invoke-virtual {v1, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3968
    const-string v9, "longitude"

    const/4 v10, 0x1

    aget v10, v7, v10

    invoke-virtual {v1, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3973
    .end local v6    # "mcc":Ljava/lang/String;
    .end local v7    # "point":[I
    .end local v8    # "region":I
    :cond_1
    const-string v9, "pre_loc"

    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .line 3974
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mapitem:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    invoke-static {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$8700(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLocation()Ljava/lang/String;

    move-result-object v10

    .line 3973
    invoke-virtual {v1, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3976
    const-string v9, "mapitem"

    invoke-virtual {v1, v9, v3}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 3979
    .end local v3    # "item":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;>;"
    :cond_2
    const-string v9, "mapweatherzoom5"

    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .line 3980
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;
    invoke-static {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$8100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    move-result-object v10

    const/4 v11, 0x5

    invoke-virtual {v10, v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->getMapZoomList(I)Ljava/util/ArrayList;

    move-result-object v10

    .line 3979
    invoke-virtual {v1, v9, v10}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 3981
    const-string v9, "mapweatherzoom6"

    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .line 3982
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;
    invoke-static {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$8100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    move-result-object v10

    const/4 v11, 0x6

    invoke-virtual {v10, v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->getMapZoomList(I)Ljava/util/ArrayList;

    move-result-object v10

    .line 3981
    invoke-virtual {v1, v9, v10}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 3983
    const-string v9, "mapweatherzoom7"

    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .line 3984
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;
    invoke-static {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$8100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    move-result-object v10

    const/4 v11, 0x7

    invoke-virtual {v10, v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->getMapZoomList(I)Ljava/util/ArrayList;

    move-result-object v10

    .line 3983
    invoke-virtual {v1, v9, v10}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 3986
    const-string v9, "tempscale"

    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I
    invoke-static {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$4300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)I

    move-result v10

    invoke-virtual {v1, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3987
    const-string v9, "launcher"

    const v10, -0x14564

    invoke-virtual {v1, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3989
    const-string v9, "flags"

    const v10, 0xcb84

    invoke-virtual {v1, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3991
    const/high16 v9, 0x24000000

    invoke-virtual {v1, v9}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 3994
    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const v10, 0xcb84

    invoke-virtual {v9, v1, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->startActivityForResult(Landroid/content/Intent;I)V

    .line 3998
    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$8100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->clearResource()V

    .line 3999
    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const/4 v10, 0x0

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;
    invoke-static {v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$8102(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    .line 4005
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "isErrorPreLatLon":Z
    :goto_2
    return-void

    .line 3944
    .restart local v1    # "intent":Landroid/content/Intent;
    .restart local v2    # "isErrorPreLatLon":Z
    .restart local v3    # "item":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;>;"
    .restart local v4    # "lat":Ljava/lang/String;
    .restart local v5    # "lon":Ljava/lang/String;
    :cond_3
    const-string v9, ""

    const-string v10, "mMIPPH l o c N"

    invoke-static {v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3945
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 3948
    .end local v4    # "lat":Ljava/lang/String;
    .end local v5    # "lon":Ljava/lang/String;
    :cond_4
    const-string v9, ""

    const-string v10, "mMIPPH Ret N O"

    invoke-static {v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3949
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 3962
    .restart local v6    # "mcc":Ljava/lang/String;
    .restart local v8    # "region":I
    :cond_5
    const-string v9, ""

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "using "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3963
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;

    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    iget-object v9, v9, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->detailContext:Landroid/app/Activity;

    invoke-direct {v0, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;-><init>(Landroid/content/Context;)V

    .line 3964
    .local v0, "citydb":Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;
    invoke-virtual {v0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->getRegionByMCC(Ljava/lang/String;)I

    move-result v8

    .line 3965
    sget-object v9, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Constants;->REGION_POINTS:[[I

    aget-object v7, v9, v8

    .restart local v7    # "point":[I
    goto/16 :goto_1

    .line 4003
    .end local v0    # "citydb":Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "isErrorPreLatLon":Z
    .end local v3    # "item":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;>;"
    .end local v6    # "mcc":Ljava/lang/String;
    .end local v7    # "point":[I
    .end local v8    # "region":I
    :cond_6
    const-string v9, ""

    const-string v10, "rOUT iAV f"

    invoke-static {v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
