.class Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService$3;
.super Landroid/content/BroadcastReceiver;
.source "WeatherClockService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;

    .prologue
    .line 135
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 139
    if-nez p2, :cond_0

    .line 140
    :try_start_0
    const-string v2, "WeatherClockService"

    const-string v3, "action : intent = null"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    :goto_0
    return-void

    .line 143
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 144
    .local v0, "action":Ljava/lang/String;
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "action : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    const-string v2, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 155
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.android.widgetapp.ap.hero.accuweather.action.CONFIGURATION_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 165
    .end local v0    # "action":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 166
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 157
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "action":Ljava/lang/String;
    :cond_1
    :try_start_1
    const-string v2, "android.intent.action.DATE_CHANGED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "android.intent.action.TIMEZONE_CHANGED"

    .line 158
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "android.intent.action.NETWORK_SET_TIMEZONE"

    .line 159
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 160
    :cond_2
    invoke-static {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;

    move-result-object v2

    const-string v3, "com.sec.android.widgetapp.ap.hero.accuweather.widget.action.APPWIDGET_UPDATE"

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;->onDirectUpdateClock(Ljava/lang/String;)V

    goto :goto_0

    .line 162
    :cond_3
    invoke-static {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;->onDirectUpdateClock(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
