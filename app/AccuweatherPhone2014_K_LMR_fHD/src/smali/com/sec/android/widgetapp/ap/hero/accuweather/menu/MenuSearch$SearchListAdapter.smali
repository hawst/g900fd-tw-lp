.class Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$SearchListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "MenuSearch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SearchListAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private searchWord:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "textViewResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1702
    .local p3, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 1703
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "textViewResourceId"    # I
    .param p4, "searchWord"    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/CharSequence;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1697
    .local p3, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$SearchListAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    .line 1698
    iput-object p4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$SearchListAdapter;->searchWord:Ljava/lang/CharSequence;

    .line 1699
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 1706
    move-object v3, p2

    .line 1708
    .local v3, "v":Landroid/view/View;
    if-nez v3, :cond_0

    .line 1709
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$SearchListAdapter;->getContext()Landroid/content/Context;

    move-result-object v5

    const-string v6, "layout_inflater"

    .line 1710
    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    .line 1711
    .local v4, "vi":Landroid/view/LayoutInflater;
    const v5, 0x7f030024

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 1714
    .end local v4    # "vi":Landroid/view/LayoutInflater;
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$SearchListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1715
    .local v0, "cityName":Ljava/lang/String;
    const/4 v1, 0x0

    .line 1717
    .local v1, "cityNameHighlighted":Ljava/lang/CharSequence;
    if-eqz v0, :cond_3

    .line 1718
    const v5, 0x1020014

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1720
    .local v2, "tv":Landroid/widget/TextView;
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$SearchListAdapter;->searchWord:Ljava/lang/CharSequence;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$SearchListAdapter;->searchWord:Ljava/lang/CharSequence;

    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v5

    if-lez v5, :cond_1

    .line 1721
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$SearchListAdapter;->searchWord:Ljava/lang/CharSequence;

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->insertHighlight(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 1724
    :cond_1
    if-eqz v1, :cond_2

    .line 1725
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1735
    .end local v2    # "tv":Landroid/widget/TextView;
    :goto_0
    return-object v3

    .line 1727
    .restart local v2    # "tv":Landroid/widget/TextView;
    :cond_2
    const-string v5, ""

    const-string v6, "SLA cNH n"

    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1728
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1732
    .end local v2    # "tv":Landroid/widget/TextView;
    :cond_3
    const-string v5, ""

    const-string v6, "SLA cN n"

    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
