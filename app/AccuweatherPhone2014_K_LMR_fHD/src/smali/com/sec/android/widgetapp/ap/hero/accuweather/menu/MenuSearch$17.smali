.class Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$17;
.super Ljava/lang/Object;
.source "MenuSearch.java"

# interfaces
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->performClickGpsBtn(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

.field final synthetic val$v:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;Landroid/view/View;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 560
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$17;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    iput-object p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$17;->val$v:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public click(I)V
    .locals 5
    .param p1, "buttonType"    # I

    .prologue
    const/4 v4, 0x1

    .line 562
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$17;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->isActivityVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 563
    const/16 v1, 0xa

    if-ne p1, v1, :cond_1

    .line 564
    const-string v1, ""

    const-string v2, "set err!"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 565
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$17;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    invoke-virtual {v1, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->setResult(I)V

    .line 566
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$17;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->finish()V

    .line 567
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$17;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$17;->val$v:Landroid/view/View;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->performClickGpsBtn(Landroid/view/View;)V
    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$1900(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;Landroid/view/View;)V

    .line 582
    :cond_0
    :goto_0
    return-void

    .line 576
    :cond_1
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 577
    .local v0, "msg":Landroid/os/Message;
    iput v4, v0, Landroid/os/Message;->arg1:I

    .line 578
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$17;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    iget-object v1, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mKeyboardHanlder:Landroid/os/Handler;

    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 579
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$17;->val$v:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0
.end method
