.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherLauncher;
.super Landroid/app/Activity;
.source "WeatherLauncher.java"


# instance fields
.field private cityList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 22
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 23
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherLauncher;->requestWindowFeature(I)Z

    .line 24
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getAllCityList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherLauncher;->cityList:Ljava/util/ArrayList;

    .line 25
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 28
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 29
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 32
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 33
    return-void
.end method

.method protected onResume()V
    .locals 6

    .prologue
    const/high16 v5, 0x24000000

    .line 37
    const/4 v1, 0x0

    .line 38
    .local v1, "intent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherLauncher;->cityList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    .line 39
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherLauncher;->cityList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 43
    .local v0, "cityCount":I
    :goto_0
    if-nez v0, :cond_1

    .line 44
    new-instance v1, Landroid/content/Intent;

    .end local v1    # "intent":Landroid/content/Intent;
    const-class v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 45
    .restart local v1    # "intent":Landroid/content/Intent;
    invoke-virtual {v1, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 46
    const-string v3, "flags"

    const/16 v4, 0x6977

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 47
    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherLauncher;->startActivity(Landroid/content/Intent;)V

    .line 56
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherLauncher;->finish()V

    .line 57
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 58
    return-void

    .line 41
    .end local v0    # "cityCount":I
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "cityCount":I
    goto :goto_0

    .line 49
    :cond_1
    const/4 v2, 0x0

    .line 50
    .local v2, "location":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherLauncher;->cityList:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v2

    .line 51
    new-instance v1, Landroid/content/Intent;

    .end local v1    # "intent":Landroid/content/Intent;
    const-class v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 52
    .restart local v1    # "intent":Landroid/content/Intent;
    invoke-virtual {v1, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 53
    const-string v3, "searchlocation"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 54
    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherLauncher;->startActivity(Landroid/content/Intent;)V

    goto :goto_1
.end method
