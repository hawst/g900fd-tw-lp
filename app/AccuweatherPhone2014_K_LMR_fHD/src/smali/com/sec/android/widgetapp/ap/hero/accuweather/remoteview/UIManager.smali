.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;
.super Ljava/lang/Object;
.source "UIManager.java"


# static fields
.field static final DATE_FORMAT_CHANGED:Ljava/lang/String; = "clock.date_format_changed"

.field private static mUIManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;


# instance fields
.field private mCtx:Landroid/content/Context;

.field private mViews:Landroid/widget/RemoteViews;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;->mUIManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;->mCtx:Landroid/content/Context;

    .line 20
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;->mViews:Landroid/widget/RemoteViews;

    .line 29
    const-string v0, ""

    const-string v1, "UIManager : create ui manager"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 30
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;->mCtx:Landroid/content/Context;

    .line 31
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    const-string v0, ""

    const-string v1, "getInstance"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;->mUIManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;

    if-nez v0, :cond_0

    .line 42
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;->mUIManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;

    .line 45
    :cond_0
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;->mUIManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;

    return-object v0
.end method

.method private makeViews(Z)V
    .locals 3
    .param p1, "bForceMake"    # Z

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;->mViews:Landroid/widget/RemoteViews;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    .line 75
    :cond_0
    const-string v0, ""

    const-string v1, "make widget view!!! "

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;->mCtx:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f030035

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;->mViews:Landroid/widget/RemoteViews;

    .line 78
    :cond_1
    return-void
.end method


# virtual methods
.method public isExistWidget([I)Z
    .locals 2
    .param p1, "ids"    # [I

    .prologue
    .line 55
    array-length v0, p1

    if-gtz v0, :cond_0

    .line 56
    const-string v0, ""

    const-string v1, "The widget does not exist in idle!!"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    const/4 v0, 0x0

    .line 59
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onDirectUpdateClock(Ljava/lang/String;)V
    .locals 3
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 85
    const-string v0, ""

    const-string v1, "direct update clock"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    if-eqz p1, :cond_0

    const-string v0, "com.sec.android.widgetapp.ap.hero.accuweather.widget.action.WEATHER_SCREEN_ON"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "action -> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;->mCtx:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 92
    :cond_0
    return-void
.end method

.method public onDisableWidget()V
    .locals 2

    .prologue
    .line 125
    const-string v0, ""

    const-string v1, "onDisableWidget"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    return-void
.end method

.method public onEnableWidget()V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;->mCtx:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->setContext(Landroid/content/Context;)V

    .line 67
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;->makeViews(Z)V

    .line 68
    return-void
.end method

.method public onRecieveWidget(Landroid/content/Intent;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "awm"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "ids"    # [I

    .prologue
    .line 102
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 103
    .local v0, "action":Ljava/lang/String;
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " action:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;->makeViews(Z)V

    .line 112
    return-void
.end method

.method public onUpdateWidget(Landroid/appwidget/AppWidgetManager;[IZ)V
    .locals 0
    .param p1, "awm"    # Landroid/appwidget/AppWidgetManager;
    .param p2, "ids"    # [I
    .param p3, "isRotation"    # Z

    .prologue
    .line 122
    return-void
.end method

.method public setScreenOnFlag(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 81
    return-void
.end method
