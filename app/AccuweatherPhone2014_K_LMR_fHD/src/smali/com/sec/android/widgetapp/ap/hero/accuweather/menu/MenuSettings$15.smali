.class Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$15;
.super Ljava/lang/Object;
.source "MenuSettings.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->onPreferenceClick(Landroid/preference/Preference;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

.field final synthetic val$input:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;Landroid/widget/EditText;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    .prologue
    .line 1830
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$15;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    iput-object p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$15;->val$input:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    .line 1832
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$15;->val$input:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1834
    .local v0, "m_Text":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x5

    if-gt v2, v3, :cond_0

    .line 1835
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$15;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->durationDefalut:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->access$1100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)Ljava/lang/String;

    move-result-object v0

    .line 1838
    :cond_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$15;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mCtx:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)Landroid/content/Context;

    move-result-object v2

    const-string v3, "EFFECT_15"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateViEffectSettings(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v1

    .line 1839
    .local v1, "result":I
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "result = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1841
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$15;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->setActivityVisibleState(Z)V

    .line 1842
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 1843
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$15;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->init()V
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->access$800(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V

    .line 1844
    return-void
.end method
