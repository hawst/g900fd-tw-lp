.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;
.super Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
.source "WeatherRenderer.java"


# instance fields
.field private mOrientation:I

.field private mResolutionInfo:[Ljava/lang/String;

.field private mScenManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;ILandroid/os/Handler;Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$DrawListener;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resources"    # Landroid/content/res/Resources;
    .param p3, "instance"    # I
    .param p4, "handler"    # Landroid/os/Handler;
    .param p5, "listener"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$DrawListener;

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;-><init>(Landroid/content/Context;Landroid/content/res/Resources;ILandroid/os/Handler;)V

    .line 20
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mScenManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    .line 21
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mResolutionInfo:[Ljava/lang/String;

    .line 22
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mOrientation:I

    .line 37
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    invoke-direct {v0, p1, p3, p5, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;-><init>(Landroid/content/Context;ILcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$DrawListener;Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mScenManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    .line 38
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mResolutionInfo:[Ljava/lang/String;

    .line 39
    return-void
.end method


# virtual methods
.method public doChangeDayOrNight()Z
    .locals 2

    .prologue
    .line 98
    const/4 v0, 0x1

    .line 100
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mScenManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mScenManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->doChangeDayOrNight()Z

    move-result v0

    .line 102
    :cond_0
    return v0
.end method

.method public getSceneManager()Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mScenManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    return-object v0
.end method

.method public isPaused()Z
    .locals 1

    .prologue
    .line 116
    iget-boolean v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbPaused:Z

    return v0
.end method

.method protected onContentDescriptionUpdate()Ljava/lang/String;
    .locals 3

    .prologue
    .line 186
    const-string v1, ""

    const-string v2, "onContentDescriptionUpdate"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mScenManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->getDescription()Ljava/lang/String;

    move-result-object v0

    .line 188
    .local v0, "description":Ljava/lang/String;
    return-object v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;Landroid/view/View;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "mainView"    # Landroid/view/View;

    .prologue
    .line 120
    invoke-super {p0, p1, p2}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->onDraw(Landroid/graphics/Canvas;Landroid/view/View;)V

    .line 121
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 132
    const-string v0, ""

    const-string v1, "onPause"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mScenManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mScenManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->onPause()V

    .line 134
    :cond_0
    invoke-super {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->onPause()V

    .line 135
    return-void
.end method

.method protected onPostdraw()V
    .locals 0

    .prologue
    .line 124
    invoke-super {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->onPostdraw()V

    .line 125
    return-void
.end method

.method protected onPredraw()V
    .locals 0

    .prologue
    .line 128
    invoke-super {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->onPredraw()V

    .line 129
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 138
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mResolutionInfo:[Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mResolutionInfo:[Ljava/lang/String;

    aget-object v0, v0, v3

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mResolutionInfo:[Ljava/lang/String;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 139
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onResume orientation = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mOrientation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", from res = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mResolutionInfo:[Ljava/lang/String;

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    :goto_0
    invoke-super {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->onResume()V

    .line 144
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mScenManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mScenManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->onResume()V

    .line 147
    :cond_0
    return-void

    .line 141
    :cond_1
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onResume orientation = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mOrientation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 4
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "spanX"    # I
    .param p4, "spanY"    # I

    .prologue
    const/4 v3, 0x0

    .line 156
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mResolutionInfo:[Ljava/lang/String;

    .line 157
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mOrientation:I

    .line 158
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mResolutionInfo:[Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mResolutionInfo:[Ljava/lang/String;

    aget-object v0, v0, v3

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mResolutionInfo:[Ljava/lang/String;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 159
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSizeChanged : ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "], ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "orientation = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mOrientation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", from res = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mResolutionInfo:[Ljava/lang/String;

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    :goto_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mScenManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mScenManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->getResizeStep()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 167
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mScenManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->drawLayoutAfterResize()V

    .line 170
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->onSizeChanged(IIII)V

    .line 171
    return-void

    .line 162
    :cond_1
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSizeChanged : ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "], ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "orientation = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mOrientation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 174
    const-string v0, ""

    const-string v1, "onStart"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->setContext(Landroid/content/Context;)V

    .line 176
    invoke-super {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->onStart()V

    .line 177
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 180
    const-string v0, ""

    const-string v1, "onStop"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mScenManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mScenManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->onDestroy()V

    .line 182
    :cond_0
    invoke-super {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->onStop()V

    .line 183
    return-void
.end method

.method protected setSurface(Landroid/view/Surface;IIII)V
    .locals 2
    .param p1, "aSurface"    # Landroid/view/Surface;
    .param p2, "aWidth"    # I
    .param p3, "aHeight"    # I
    .param p4, "aSpanX"    # I
    .param p5, "aSpanY"    # I

    .prologue
    .line 151
    const-string v0, ""

    const-string v1, "set surface!!"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    invoke-super/range {p0 .. p5}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->setSurface(Landroid/view/Surface;IIII)V

    .line 153
    return-void
.end method

.method public showMessage(I)V
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mbPaused:Z

    if-nez v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mScenManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mScenManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->showMessage(I)V

    .line 64
    :cond_0
    return-void
.end method

.method public startRefresh()V
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mbPaused:Z

    if-nez v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mScenManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mScenManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->startRefreshIcon()V

    .line 82
    :cond_0
    return-void
.end method

.method public stopRefresh()V
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mbPaused:Z

    if-nez v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mScenManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mScenManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->stopRefreshIcon()V

    .line 91
    :cond_0
    return-void
.end method

.method public updateClock(Z)V
    .locals 1
    .param p1, "isChangeTimeZone"    # Z

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mScenManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mScenManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->updateClock(Z)V

    .line 113
    :cond_0
    return-void
.end method

.method public updateView(Z)V
    .locals 1
    .param p1, "isStartAnim"    # Z

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mbPaused:Z

    if-nez v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mScenManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mScenManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->updateView(Z)V

    .line 73
    :cond_0
    return-void
.end method

.method public weatherRequestLayout(I)V
    .locals 2
    .param p1, "instance"    # I

    .prologue
    .line 42
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 43
    .local v0, "layoutMsg":Landroid/os/Message;
    const/4 v1, 0x2

    iput v1, v0, Landroid/os/Message;->what:I

    .line 44
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 45
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 46
    return-void
.end method
