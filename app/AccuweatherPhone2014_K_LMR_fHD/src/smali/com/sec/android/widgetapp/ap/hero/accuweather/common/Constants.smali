.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# static fields
.field public static final ACCOUNT_CHECK_NOTIFICATION_ITEM:Ljava/lang/String; = "com.samsung.accessory.intent.action.CHECK_NOTIFICATION_ITEM"

.field public static final ACCOUNT_DIALOG_AUTOREFRESH_SETTING:Ljava/lang/String; = "AUTOREFRESH_SETTING"

.field public static final ACCOUNT_DIALOG_FROM:Ljava/lang/String; = "ACCOUNTING_DIALOG_FROM"

.field public static final ACCOUNT_DIALOG_FROM_NO:Ljava/lang/String; = "FROM_NO"

.field public static final ACCOUNT_DIALOG_FROM_REFRESH:Ljava/lang/String; = "FROM_REFRESH"

.field public static final ACCOUNT_DIALOG_FROM_WIDGET:Ljava/lang/String; = "FROM_TOUCH_WIDGET"

.field public static final ACCOUNT_DIALOG_FROM_WIDGET_EASY:Ljava/lang/String; = "FROM_TOUCH_WIDGET_EASY"

.field public static final ACCOUNT_DIALOG_SELECTED_INDEX:Ljava/lang/String; = "SELECTED_INDEX"

.field public static final ACCOUNT_DIALOG_SELECTED_INDEX_EASY:Ljava/lang/String; = "SELECTED_INDEX_EASY"

.field public static final ACCOUNT_UPDATE_NOTIFICATION_ITEM:Ljava/lang/String; = "com.samsung.accessory.intent.action.UPDATE_NOTIFICATION_ITEM"

.field public static final ACTIONBAR_SIZE_HEIGHT_DP:I = 0x2f

.field public static final ACTION_CURRENT_LOCATION_WEATHER_DATA:Ljava/lang/String; = "com.sec.android.widgetapp.ap.accuweatherdaemon.action.CURRENT_LOCATION_WEATHER_DATA"

.field public static final ACTION_DAEMON_AUTOREFRESH_START:Ljava/lang/String; = "com.sec.android.daemonapp.action.DAEMON_AUTOREFRESH_START"

.field public static final ACTION_DISABLE_LOCATION_SERVICES:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.action.DISABLE_LOCATION_SERVICES"

.field public static final ACTION_DRAW_WIDGET_BY_EASY:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.action.DRAW_WIDGET_BY_EASY"

.field public static final ACTION_EASY_MODE_CHANGE:Ljava/lang/String; = "com.android.launcher.action.EASY_MODE_CHANGE"

.field public static final ACTION_EASY_WIDGET_CITY_CNT_ZERO:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.action.CITY_CNT_ZERO_FOR_EASY"

.field public static final ACTION_EASY_WIDGET_START_ACTIVITY:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.easy.widget.action.ACTIVITY"

.field public static final ACTION_GET_CURRENT_LOCATION_DELETE_FROM_DAEMON:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweatherdaemon.action.SYNC_CURRENT_LOCATION_WEATHER_DATA_DELETE"

.field public static final ACTION_GET_CURRENT_LOCATION_ERROR:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.action.GET_CURRENT_LOCATION_ERROR"

.field public static final ACTION_GET_CURRENT_LOCATION_EXCEPTION:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.action.GET_CURRENT_LOCATION_EXCEPTION"

.field public static final ACTION_GET_CURRENT_LOCATION_FROM_DAEMON:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweatherdaemon.action.SYNC_CURRENT_LOCATION_WEATHER_DATA"

.field public static final ACTION_GET_CURRENT_LOCATION_NETWORK_FAILED:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.action.GET_CURRENT_LOCATION_NETWORK_FAILED"

.field public static final ACTION_GET_CURRENT_LOCATION_OK:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.action.GET_CURRENT_LOCATION_OK"

.field public static final ACTION_GET_CURRENT_LOCATION_PROVIDER_DISABLE:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.action.GET_CURRENT_LOCATION_PROVIDER_DISABLE"

.field public static final ACTION_GET_CURRENT_LOCATION_PROVIDER_ERR:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.action.GET_CURRENT_LOCATION_PROVIDER_ERR"

.field public static final ACTION_GET_CURRENT_LOCATION_START:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.action.GET_CURRENT_LOCATION_START"

.field public static final ACTION_GET_DATA_NETWORK_ERROR:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.action.GET_DATA_NETWORK_ERROR"

.field public static final ACTION_GET_LOCATION_INFO:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.action.GET_LOCATION_INFO"

.field public static final ACTION_LOCATION_SETTING_FAIL:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.action.GET_LOCATION_SETTING_FAIL"

.field public static final ACTION_LOCATION_SETTING_OK:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.action.GET_LOCATION_SETTING_OK"

.field public static final ACTION_MANUALREFRESH_DB_FULL:Ljava/lang/String; = "HYBRID_WEATHER_MANUALREFRESH_DB_FULL"

.field public static final ACTION_MANUALREFRESH_NETWORK_ERROR:Ljava/lang/String; = "HYBRID_WEATHER_MANUALREFRESH_NETWORK_ERROR"

.field public static final ACTION_MANUALREFRESH_SERVICE_NOT_AVAILABLE:Ljava/lang/String; = "HYBRID_WEATHER_MANUALREFRESH_SERVICE_NOT_AVAILABLE"

.field public static final ACTION_MANUALREFRESH_SETUI:Ljava/lang/String; = "HYBRID_WEATHER_MANUALREFRESH_SETUI"

.field public static final ACTION_MANUALREFRESH_START:Ljava/lang/String; = "HYBRID_WEATHER_MANUALREFRESH_START"

.field public static final ACTION_MANUALREFRESH_STOP:Ljava/lang/String; = "HYBRID_WEATHER_MANUALREFRESH_STOP"

.field public static final ACTION_MYCURRENTLOCATION:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.action.MYCURRENTLOCATION"

.field public static final ACTION_NOT_ENOUGHT_SPACE_ERR:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.action.NOT_ENOUGHT_SPACE_ERR"

.field public static final ACTION_PERFORM_NETWORK_ACCESS:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.action.PERFORM_NETWORK_ACCESS"

.field public static final ACTION_PERFORM_NETWORK_ACCESS_RESPONCE:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.action.ACTION_PERFORM_NETWORK_ACCESS_RESPONCE"

.field public static final ACTION_PHONE_STATE:Ljava/lang/String; = "android.intent.action.PHONE_STATE"

.field public static final ACTION_REFRESH_FROM_ACCOUNTING_DIALOG:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.action.REFRESH_FROM_ACCOUNTING_DIALOG"

.field public static final ACTION_RESTART_SERVICE:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.action.RESTART_SERVICE"

.field public static final ACTION_SEC_ALERT_NOTIFICATION:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.action.ALERT_NOTIFICATION"

.field public static final ACTION_SEC_CHANGE_SETTING:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.action.CHANGE_SETTING"

.field public static final ACTION_SEC_DAEMON_AUTO_REFRESH_END:Ljava/lang/String; = "com.sec.android.daemonapp.action.DAEMON_AUTOREFRESH_END"

.field public static final ACTION_SEC_DAEMON_VERSION_CHECK:Ljava/lang/String; = "com.sec.android.widgetapp.at.hero.accuweather.action.DAEMON_VERSION_CHECK"

.field public static final ACTION_SEC_DATE_CHANGED:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.widget.action.DATE_CHANGED"

.field public static final ACTION_SEC_MANUAL_REFRESH:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.widget.action.MANUAL_REFRESH"

.field public static final ACTION_SEC_MANUAL_REFRESH_FOR_EASY:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.widget.action.MANUAL_REFRESH_FOR_EASY"

.field public static final ACTION_SEC_REMOTE_WIDGET_MANUAL_REFRESH_END:Ljava/lang/String; = "com.sec.android.daemonapp.action.REMOTE_WIDGET_MANUAL_REFRESH_END"

.field public static final ACTION_SEC_SERVICE_SCREEN_ON:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.action.SERVICE_SCREEN_ON"

.field public static final ACTION_SEC_SYNC_DATA_WITH_DAEMON:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweatherdaemon.action.SYNC_DATA_WITH_DAEMON"

.field public static final ACTION_SEC_TIMEZONE_CHANGED:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.widget.action.TIMEZONE_CHANGED"

.field public static final ACTION_SEC_TIME_CHANGED:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.widget.action.TIME_CHANGED"

.field public static final ACTION_SEC_TIME_TICK:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.action.TIME_TICK"

.field public static final ACTION_SEC_WEATHER_UPDATE:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.widget.action.APPWIDGET_UPDATE"

.field public static final ACTION_SEC_WIDGET_RESIZE:Ljava/lang/String; = "com.sec.android.widgetapp.APPWIDGET_RESIZE"

.field public static final ACTION_SEND_INFO_TO_CLOCK:Ljava/lang/String; = "com.sec.android.widgetapp.ap.accuweatherdaemon.action.SEND_INFO_TO_CLOCK"

.field public static final ACTION_START_ACTIVITY_FROM_ACCOUNTING_DIALOG:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.action.START_ACTIVITY_FROM_ACCOUNTING_DIALOG"

.field public static final ACTION_START_DETAILS:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.action.START_DETAILS"

.field public static final ACTION_STOP_ERROR_MSG:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.action.ACTION_STOP_ERROR_MSG"

.field public static final ACTION_STOP_THREAD:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.action.ACTION_STOP_THREAD"

.field public static final ACTION_SYNC_DATA_WITH_WIDGET:Ljava/lang/String; = "com.sec.android.widgetapp.ap.accuweatherdaemon.action.SYNC_DATA_WITH_WIDGET"

.field public static final ACTION_SYNC_SETTING_WITH_WIDGET:Ljava/lang/String; = "com.sec.android.widgetapp.ap.accuweatherdaemon.action.SYNC_SETTING_WITH_WIDGET"

.field public static final ACTION_WEATHER_NOTIFICATION_SETTING_SYNC:Ljava/lang/String; = "com.sec.android.widgetapp.ap.accuweatherdaemon.action.WEATHER_NOTIFICATION_SETTING_SYNC"

.field public static final ACTION_WEATHER_SCREEN_OFF:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.widget.action.WEATHER_SCREEN_OFF"

.field public static final ACTION_WEATHER_SCREEN_ON:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.widget.action.WEATHER_SCREEN_ON"

.field public static final ACTION_WEATHER_SHOW_USER_CURRENTLOCATION_POPUP:Ljava/lang/String; = "com.sec.android.widgetapp.ap.accuweatherdaemon.action.WEATHER_SHOW_USER_CURRENTLOCATION_POPUP"

.field public static final ACTION_WIDGET_CITY_CNT_ZERO:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.action.CITY_CNT_ZERO"

.field public static final ACTION_WIDGET_CONFIGURATION_CHANGED:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.action.CONFIGURATION_CHANGED"

.field public static final ACTION_WIDGET_GOOGLE_BAR:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.action.SEARCH_BAR"

.field public static final ACTION_WIDGET_GOOGLE_VOICE_SEARCH:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.action.VOICE_SEARCH_BAR"

.field public static final ACTION_WIDGET_NOTIFICATION:Ljava/lang/String; = "com.system.action.NOTIFICATION"

.field public static final ACTION_WIDGET_START_ACTIVITY:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather.action.ACTIVITY"

.field public static final ACTIVITY_FLAG_ADD_LIST:I = 0x2edf

.field public static final ACTIVITY_FLAG_ADD_TO_CTYLIST:I = 0x6977

.field public static final ACTIVITY_FLAG_ADD_TO_LIST:I = 0x658f

.field public static final ACTIVITY_FLAG_ADD_TO_MAP:I = 0x61a7

.field public static final ACTIVITY_FLAG_CHANGE_ORDERS:I = 0x36af

.field public static final ACTIVITY_FLAG_DEFAULT_LOCATION:I = 0x3a97

.field public static final ACTIVITY_FLAG_DELETE_LIST:I = 0x32c7

.field public static final ACTIVITY_FLAG_DETAIL_DIRECT_DELETE:I = 0x5dbf

.field public static final ACTIVITY_FLAG_DETAIL_TO_CTYLIST:I = 0x691d

.field public static final ACTIVITY_FLAG_DETAIL_TO_LIST:I = 0x5207

.field public static final ACTIVITY_FLAG_DETAIL_TO_MAP:I = 0xcb84

.field public static final ACTIVITY_FLAG_DETAIL_TO_SEARCH:I = 0x80e7

.field public static final ACTIVITY_FLAG_DETAIL_TO_SETTINGS:I = 0x55ef

.field public static final ACTIVITY_FLAG_LIST_TO_SEARCH:I = 0x7917

.field public static final ACTIVITY_FLAG_MAIN_DIRECT_ADD:I = 0x7cf

.field public static final ACTIVITY_FLAG_MAIN_TO_DETAIL:I = -0x7530

.field public static final ACTIVITY_FLAG_MAIN_TO_LIST:I = 0xbb7

.field public static final ACTIVITY_FLAG_PREDETAIL_ADD_DETAIL:I = -0x10428

.field public static final ACTIVITY_FLAG_PREDETAIL_TO_MAP:I = -0xcf6c

.field public static final ACTIVITY_FLAG_SEARCH_TO_DETAIL:I = 0xf22f

.field public static final ACTIVITY_FLAG_SEARCH_TO_LIST:I = 0x7cff

.field public static final ACTIVITY_FLAG_SET_DST:I = 0x3e7f

.field public static final ACTIVITY_RESULT_FAIL:I = 0x12d

.field public static final ACTIVITY_RESULT_OK:I = 0x12c

.field public static final ANNIVERSARY:I = 0x1

.field public static final AUTO_REFRESH_FROM_DAEMON:I = 0x1

.field public static final AUTO_REFRESH_FROM_NOTIFICATION:I = 0x2

.field public static final AUTO_REFRESH_FROM_WIDGET:I = 0x0

.field public static final AUTO_REFRESH_WHERE_FROM:Ljava/lang/String; = "AUTO_REFRESH_WHERE_FROM"

.field public static final BG_TYPE_DETAILS_BG:I = 0x101

.field public static final BG_TYPE_DETAILS_FIRST_FRAME_BG:I = 0x103

.field public static final BG_TYPE_EASY_BG:I = 0x104

.field public static final BG_TYPE_LIST_BG:I = 0x102

.field public static final BG_TYPE_SUB_WIDGET_BG:I = 0x106

.field public static final BG_TYPE_WIDGET_BG:I = 0x105

.field public static final BIRTHDAY:I = 0x3

.field public static final CITYID_CURRENT_LOCATION:Ljava/lang/String; = "cityId:current"

.field public static final CITY_ID:Ljava/lang/String; = "CITY_ID"

.field public static final CITY_NAME:Ljava/lang/String; = "CITY_NAME"

.field public static final CITY_PROVIDER_DEFAULT:I = 0x0

.field public static final CITY_PROVIDER_GPS:I = 0x1

.field public static final CITY_STATE:Ljava/lang/String; = "CITY_STATE"

.field public static final CLOUDY_IDX:I = 0x3

.field public static final CLOUDY_NIGHT_IDX:I = 0x9

.field public static final CLOUD_ICON:I = 0x1

.field public static final COLD_IDX:I = 0xf

.field public static final COLLAB_MODE:Ljava/lang/String; = "COLLAB_MODE"

.field public static final CONNECTION_CHANGED:Ljava/lang/Object;

.field public static final CP_TYPE_CHN:Ljava/lang/String; = "cp_chn"

.field public static final CP_TYPE_ENG:Ljava/lang/String; = "cp_eng"

.field public static final CP_TYPE_JPN:Ljava/lang/String; = "cp_jpn"

.field public static final CP_TYPE_KOR:Ljava/lang/String; = "cp_kor"

.field public static final CURRENT_LOCATION_SETTING_CHANGED:I = 0x4b00

.field public static final DAEMON_EXTRA_PACKAGE:Ljava/lang/String; = "PACKAGE"

.field public static final DAEMON_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.daemonapp"

.field public static final DATA_FORMAT_CHANG:Ljava/lang/String; = "DATA_FORMAT_CHANG"

.field public static final DAY_ICON:I = 0x0

.field public static final DB_ERROR_MEMORY_FULL:I = -0x5a

.field public static final DB_ERROR_NONE:I = 0x0

.field public static final DB_FULL_EXCEPTION_ERR:I = -0x1

.field public static final DB_PROCESS_NONE:I = 0x0

.field public static final DB_PROCESS_SUCCESS:I = 0x1

.field public static final DEB_PRINT:Z = true

.field public static final DEB_TAG:Ljava/lang/String; = "WeatherClock"

.field public static final DEFAULT_IDX:I = 0x6

.field public static final DETAILS_FROM_NOTIFICATION:I = 0x102

.field public static final DETAILS_FROM_WIDGET:I = 0x101

.field public static final DETAILS_WHERE_FROM:Ljava/lang/String; = "where_from_to_details"

.field public static final DETAIL_DAY:Ljava/lang/String; = "DETAIL_DAY"

.field public static final DETAIL_HOME:Ljava/lang/String; = "DETAIL_HOME"

.field public static final DETAIL_MORE:Ljava/lang/String; = "DETAIL_MORE"

.field public static final DISABLE_CURRENT_LOCATION:I = 0xbba

.field public static final DISABLE_LOCATION_SERVICES:I = 0xbbb

.field public static final ENABLE_CURRENT_LOCATION:I = 0xbbd

.field public static final ENABLE_FACTORY_MODE:I = 0xbc1

.field public static final ENABLE_JAPAN_PROTOCOL:Z = false

.field public static final ENABLE_LOCATION_SERVICES:I = 0xbbe

.field public static final ENABLE_PERIODIC_AUTO_REFRESH:Z = true

.field public static final ENG_PRINT:Z = false

.field public static final EXTRA_GET_NOTI_ON_OFF:Ljava/lang/String; = "NOTIONOFF"

.field public static final FOG_IDX:I = 0xc

.field public static final FOG_NIGHT_IDX:I = 0xd

.field public static final FRIEND:I = 0x2

.field public static final FULL_HD:I = 0x1fa400

.field public static final GET_CURRENT_LOCATION_ERROR:I = 0xca

.field public static final GET_CURRENT_LOCATION_FROM_DAEMON:I = 0x401

.field public static final GET_CURRENT_LOCATION_OK:I = 0xc8

.field public static final GET_CURRENT_LOCATION_PROVIDER_ERR:I = 0xc9

.field public static final GET_DATA_NETWORK_ERROR:I = 0x3f2

.field public static final GET_DATA_OK:I = 0x3f3

.field public static final GET_MAPINFO_RESULT_ERROR:I = 0x2

.field public static final GET_MAPINFO_RESULT_INIT:I = 0x0

.field public static final GET_MAPINFO_RESULT_OK:I = 0x1

.field public static final GET_TIME_AMPM:I = 0x2

.field public static final GET_TIME_STR:I = 0x1

.field public static final GOOGLE_TYPE:Ljava/lang/String; = "com.google"

.field public static final GOT_ADD_CONDITION:I = 0x2332

.field public static final HD:I = 0xe1000

.field public static final HD_CAMERA_HOR:I = 0xd23c0

.field public static final HD_CAMERA_VAR:I = 0xd0200

.field public static final HOT_IDX:I = 0xe

.field public static final HVGA:I = 0x25800

.field public static final ICON_TYPE_FOR_DETAILS:I = 0x1

.field public static final ICON_TYPE_FOR_EASY_MODE_WIDGET:I = 0x6

.field public static final ICON_TYPE_FOR_LIST:I = 0x3

.field public static final ICON_TYPE_FOR_MORE:I = 0x7

.field public static final ICON_TYPE_FOR_SUB_WIDGET_4x1:I = 0x14

.field public static final ICON_TYPE_FOR_SUB_WIDGET_4x2:I = 0xa

.field public static final ICON_TYPE_FOR_WATHERMAP:I = 0x5

.field public static final ICON_TYPE_FOR_WATHERMAP_NEW:I = 0x8

.field public static final ICON_TYPE_FOR_WIDGET:I = 0x0

.field public static final ICON_TYPE_SMALL:I = 0x2

.field public static final IDLE_TYPE_A:I = 0x1

.field public static final IDLE_TYPE_B:I = 0x2

.field public static final IDLE_TYPE_C:I = 0x3

.field public static final IDLE_TYPE_D:I = 0x4

.field public static final IDLE_TYPE_E:I = 0x5

.field public static final INSERT_CITY_RESULT:I = 0x2333

.field public static final ISDAYTYPE_SUNRISE:I = 0x0

.field public static final ISDAYTYPE_SUNSET:I = 0x1

.field public static final JB_OS_VER:Ljava/lang/String; = "4.1"

.field public static final KEY_AUTOREFRESH_INTERVAL:Ljava/lang/String; = "aw_daemon_service_key_autorefresh_interval"

.field public static final KEY_AUTOREFRESH_NEXT_TIME:Ljava/lang/String; = "aw_daemon_service_key_autorefresh_next_time"

.field public static final KEY_INVALID_LOC:Ljava/lang/String; = "aw_daemon_service_key_invalid_loc"

.field public static final KEY_LOC_CODE:Ljava/lang/String; = "aw_daemon_service_key_loc_code"

.field public static final KEY_SERVICE_STATUS:Ljava/lang/String; = "aw_daemon_service_key_service_status"

.field public static final KEY_TEMP_SCALE:Ljava/lang/String; = "aw_daemon_service_key_temp_scale"

.field public static final KEY_USE_AGREE_POPUP_CHECK:Ljava/lang/String; = "aw_daemon_service_key_agree_popup_check"

.field public static final LAUNCHER_MAP:I = -0x2710

.field public static final LAUNCH_MODE:Ljava/lang/String; = "LAUNCH_MODE"

.field public static final LOCATION_READY:I = 0xbb8

.field public static final LOCATION_SERVICE_GPS_ONLY:I = 0xbc0

.field public static final MAIN_LAYOUT_2X1:I = 0x1

.field public static final MAIN_LAYOUT_2X2:I = 0x2

.field public static final MAIN_LAYOUT_4X1:I = 0x3

.field public static final MAIN_LAYOUT_4X2:I = 0x4

.field public static final MAIN_LAYOUT_5X1:I = 0x5

.field public static final MAIN_LAYOUT_5X2:I = 0x6

.field public static final MAIN_LAYOUT_NONE:I = 0x0

.field public static final MAIN_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather"

.field public static final MAPITEM_INVISIBLE:I = 0x1

.field public static final MAPVIEW_SETUP:I = 0x0

.field public static final MAP_LAUNCHER_ADD:I = -0x5208

.field public static final MAP_LAUNCHER_DETAIL:I = -0x14564

.field public static final MAP_LAUNCHER_SEARCH:I = -0x526c

.field public static final MAP_TO_PREDETAIL:I = -0x9bdc

.field public static final MAX_CITIES:I = 0xa

.field public static final MCL_CITYNAME_NULL:I = 0x1b58

.field public static final MSG_DETAIL_FORCAST_ANI_START_CUBE_ICON:I = 0xdc

.field public static final MSG_SHOWING_TIME:J = 0xbb8L

.field public static final MYPROFILE:I = 0x1

.field public static final MY_PROFILE_TYPE:Ljava/lang/String; = "vnd.sec.contact.my_profile"

.field public static final NEED_LOCATE_ONBOOT:Ljava/lang/String; = "NEED_LOCATE_ONBOOT"

.field public static final NETWORK_AVAILABLE:I = 0xbbc

.field public static final NETWORK_UNAVAILABLE:I = 0xbb9

.field public static final NEWYEAR:I = 0x0

.field public static final NEW_LOCATION:Ljava/lang/String; = "WEATHER_CLOCK_NEW_LOCATION"

.field public static final NEXT_DAY_SUNRISE:I = 0x3

.field public static final NIGHT_ICON:I = 0x2

.field public static final NOTI_ID:I = 0x1

.field public static final NOT_ENOUGHT_SPACE_ERR:I = 0x3e7

.field public static final PACKAGE_DATA_CLEARED:Ljava/lang/String; = "android.intent.action.PACKAGE_DATA_CLEARED"

.field public static final PHONE_TYPE:Ljava/lang/String; = "vnd.sec.contact.phone"

.field public static final PREFERENCE_WIDGET_COUNT:Ljava/lang/String; = "pref_widget_count"

.field public static final PREFERENCE_WIDGET_COUNT_REMOTE_KEY:Ljava/lang/String; = "widget_count_remote"

.field public static final PREFERENCE_WIDGET_COUNT_SURFACE_KEY:Ljava/lang/String; = "widget_count_surface"

.field public static final QHD:I = 0x7e900

.field public static final QHD2:I = 0x7eb1c

.field public static final QVGA:I = 0x12c00

.field public static final RAIN_IDX:I = 0x0

.field public static final RAIN_NIGHT_IDX:I = 0x6

.field public static final REFRESH_CNT_DECREASE:I = 0x300

.field public static final REFRESH_CNT_INCREASE:I = 0x200

.field public static final REFRESH_CNT_INIT:I = 0x100

.field public static final REFRESH_END:I = 0x20

.field public static final REFRESH_FROM_CITY_LIST:I = 0x1

.field public static final REFRESH_FROM_DETAIL:I = 0x2

.field public static final REFRESH_FROM_DETAIL_WITH_CITYLIST:I = 0x3

.field public static final REFRESH_FROM_EASY_WIDGET:I = 0x5

.field public static final REFRESH_FROM_SEARCH_DETAIL:I = 0x4

.field public static final REFRESH_FROM_WIDGET:I = 0x0

.field public static final REFRESH_START:I = 0x10

.field public static REGION_NAMES:[Ljava/lang/String; = null

.field public static REGION_POINTS:[[I = null

.field public static final REPORT_WRONG_CITY:Ljava/lang/String; = "REPORT_WRONG_CITY"

.field public static final SEARCH_TO_MAP:I = 0x3070

.field public static final SEND_DAEMON_SETTING_UPDATE_WIDGET:Ljava/lang/String; = "SEND_DAEMON_SETTING_UPDATE_WIDGET"

.field public static final SERVICE_OFF:I = 0x0

.field public static final SERVICE_ON:I = 0x1

.field public static final SET_DISABLE:I = 0x0

.field public static final SET_ENABLE:I = 0x1

.field public static final SET_TIMEOUT:I = -0x13d30

.field public static final SHOW_DIALOG_DST:Z

.field public static final SNOW_IDX:I = 0x1

.field public static final SNOW_NIGHT_IDX:I = 0x7

.field public static final SUNNY_CLOUDY_IDX:I = 0x5

.field public static final SUNNY_CLOUDY_NIGHT_IDX:I = 0xb

.field public static final SUNNY_IDX:I = 0x2

.field public static final SUNNY_NIGHT_IDX:I = 0x8

.field public static final TEMP_SCALE_CENTIGRADE:I = 0x1

.field public static final TEMP_SCALE_FAHRENHEIT:I = 0x0

.field public static final THUNDER_IDX:I = 0x4

.field public static final THUNDER_NIGHT_IDX:I = 0xa

.field public static final TIME_EXCEEDED:I = -0x140b4

.field public static final TODAY_SUNRISE:I = 0x1

.field public static final TODAY_SUNSET:I = 0x2

.field public static final USE_THREE_HOURS_AUTO_REFRESH:Z

.field public static final WEATHER_IDX:[I

.field public static final WEATHER_PROVIDER_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.weatherprovider"

.field public static final WIDGET_IDS_EXTRA:Ljava/lang/String; = "WIDGET_IDS_EXTRA"

.field public static final WIDGET_MODE_EASY_REMOTE:I = 0xfa1

.field public static final WIDGET_MODE_REMOTE:I = 0xfa2

.field public static final WIDGET_MODE_SURFACE:I = 0xfa0

.field public static final WQHD:I = 0x384000

.field public static final WSVGA_HARDKEY:I = 0x96000

.field public static final WSVGA_HOR:I = 0x8a000

.field public static final WSVGA_VAR:I = 0x8ef80

.field public static final WVGA:I = 0x5dc00

.field public static final WXGA:I = 0xfa000

.field public static final ZOOM_ADDED_ITEM:I = 0x8

.field public static final ZOOM_INIT:I = 0x0

.field public static final ZOOM_URL5:I = 0x5

.field public static final ZOOM_URL6:I = 0x6

.field public static final ZOOM_URL7:I = 0x7


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v5, 0x2

    .line 15
    const-string v0, "sprint"

    sget-object v3, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "verizon"

    sget-object v3, Landroid/os/Build;->BRAND:Ljava/lang/String;

    .line 16
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "uscc"

    sget-object v3, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "cs"

    sget-object v3, Landroid/os/Build;->BRAND:Ljava/lang/String;

    .line 17
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ATT"

    sget-object v3, Landroid/os/Build;->BRAND:Ljava/lang/String;

    .line 18
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "MTR"

    sget-object v3, Landroid/os/Build;->BRAND:Ljava/lang/String;

    .line 19
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    sput-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Constants;->SHOW_DIALOG_DST:Z

    .line 23
    const-string v0, "cs"

    sget-object v3, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Constants;->USE_THREE_HOURS_AUTO_REFRESH:Z

    .line 223
    const-string v0, "android.net.conn.CONNECTIVITY_CHANGE"

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Constants;->CONNECTION_CHANGED:Ljava/lang/Object;

    .line 618
    const/16 v0, 0x10

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Constants;->WEATHER_IDX:[I

    .line 642
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, ""

    aput-object v3, v0, v1

    const-string v3, "Asia"

    aput-object v3, v0, v2

    const-string v3, "Middle East"

    aput-object v3, v0, v5

    const-string v3, "Oceania"

    aput-object v3, v0, v6

    const-string v3, "North America"

    aput-object v3, v0, v7

    const/4 v3, 0x5

    const-string v4, "Central America"

    aput-object v4, v0, v3

    const/4 v3, 0x6

    const-string v4, "South America"

    aput-object v4, v0, v3

    const/4 v3, 0x7

    const-string v4, "Europe"

    aput-object v4, v0, v3

    const/16 v3, 0x8

    const-string v4, "Africa"

    aput-object v4, v0, v3

    const/16 v3, 0x9

    const-string v4, "Europe"

    aput-object v4, v0, v3

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Constants;->REGION_NAMES:[Ljava/lang/String;

    .line 655
    const/16 v0, 0xa

    new-array v0, v0, [[I

    new-array v3, v1, [I

    aput-object v3, v0, v1

    new-array v1, v5, [I

    fill-array-data v1, :array_1

    aput-object v1, v0, v2

    new-array v1, v5, [I

    fill-array-data v1, :array_2

    aput-object v1, v0, v5

    new-array v1, v5, [I

    fill-array-data v1, :array_3

    aput-object v1, v0, v6

    new-array v1, v5, [I

    fill-array-data v1, :array_4

    aput-object v1, v0, v7

    const/4 v1, 0x5

    new-array v2, v5, [I

    fill-array-data v2, :array_5

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v5, [I

    fill-array-data v2, :array_6

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v5, [I

    fill-array-data v2, :array_7

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v5, [I

    fill-array-data v2, :array_8

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-array v2, v5, [I

    fill-array-data v2, :array_9

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Constants;->REGION_POINTS:[[I

    return-void

    :cond_1
    move v0, v1

    .line 19
    goto/16 :goto_0

    .line 618
    :array_0
    .array-data 4
        0x0
        0x1
        0x2
        0x3
        0x4
        0x5
        0x6
        0x7
        0x8
        0x9
        0xa
        0xb
        0xc
        0xd
        0xe
        0xf
    .end array-data

    .line 655
    :array_1
    .array-data 4
        0x1452a3f
        0x71d31ac
    .end array-data

    :array_2
    .array-data 4
        0x1ac3b26
        0x306abf1
    .end array-data

    :array_3
    .array-data 4
        -0x1604974
        0x9844d24
    .end array-data

    :array_4
    .array-data 4
        0x2b4a299
        -0x674ba1e
    .end array-data

    :array_5
    .array-data 4
        0x583bfc
        -0x50fa891
    .end array-data

    :array_6
    .array-data 4
        -0x14ca4c9
        -0x38696d4
    .end array-data

    :array_7
    .array-data 4
        0x32c25d9
        0xa58c71
    .end array-data

    :array_8
    .array-data 4
        -0xd761c4
        0x1d21f7f
    .end array-data

    :array_9
    .array-data 4
        0x32c25d9
        0xa58c71
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
