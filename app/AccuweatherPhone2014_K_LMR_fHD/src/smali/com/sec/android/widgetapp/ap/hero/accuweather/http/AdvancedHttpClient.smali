.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;
.super Ljava/lang/Object;
.source "AdvancedHttpClient.java"


# instance fields
.field private context:Landroid/content/Context;

.field private responseOnThread:Z

.field private stamp:J

.field statusCode:I

.field strResult:Ljava/lang/String;

.field strStatus:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->responseOnThread:Z

    .line 37
    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->statusCode:I

    .line 39
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->strStatus:Ljava/lang/String;

    .line 41
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->strResult:Ljava/lang/String;

    .line 44
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->context:Landroid/content/Context;

    .line 45
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->stamp:J

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZJ)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "responseOnThread"    # Z
    .param p3, "stamp"    # J

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->responseOnThread:Z

    .line 37
    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->statusCode:I

    .line 39
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->strStatus:Ljava/lang/String;

    .line 41
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->strResult:Ljava/lang/String;

    .line 49
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->context:Landroid/content/Context;

    .line 50
    iput-boolean p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->responseOnThread:Z

    .line 51
    iput-wide p3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->stamp:J

    .line 52
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    .prologue
    .line 30
    iget-wide v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->stamp:J

    return-wide v0
.end method

.method static synthetic access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;Landroid/os/Message;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;
    .param p1, "x1"    # Landroid/os/Message;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->processResult(Landroid/os/Message;)V

    return-void
.end method

.method private callReceive(Landroid/content/Context;Landroid/os/Message;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "msg"    # Landroid/os/Message;

    .prologue
    .line 218
    iget-object v1, p2, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;

    .line 219
    .local v1, "httpResponseHandler":Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;
    if-eqz p1, :cond_0

    .line 221
    :try_start_0
    check-cast p1, Landroid/app/Activity;

    .end local p1    # "context":Landroid/content/Context;
    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$4;

    invoke-direct {v2, p0, v1, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$4;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;Landroid/os/Message;)V

    invoke-virtual {p1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 236
    :cond_0
    :goto_0
    return-void

    .line 231
    :catch_0
    move-exception v0

    .line 232
    .local v0, "ce":Ljava/lang/ClassCastException;
    iget v2, p2, Landroid/os/Message;->arg1:I

    const/4 v3, -0x1

    const-string v4, "HTTP parse error"

    const-string v5, ""

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;->onReceive(IILjava/lang/String;Ljava/lang/String;)V

    .line 233
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ADVHTTP : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static isNetWorkConnected(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 333
    const-string v2, "connectivity"

    .line 334
    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 336
    .local v0, "connManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 337
    .local v1, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isNetWorkConnectedStatus(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 315
    const-string v6, "connectivity"

    .line 316
    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 318
    .local v0, "connManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v0, v5}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 319
    .local v2, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v2, :cond_0

    .line 320
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v1

    .line 321
    .local v1, "ds":Landroid/net/NetworkInfo$DetailedState;
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v3

    .line 323
    .local v3, "st":Landroid/net/NetworkInfo$State;
    sget-object v6, Landroid/net/NetworkInfo$State;->UNKNOWN:Landroid/net/NetworkInfo$State;

    if-ne v3, v6, :cond_1

    .line 329
    .end local v1    # "ds":Landroid/net/NetworkInfo$DetailedState;
    .end local v3    # "st":Landroid/net/NetworkInfo$State;
    :cond_0
    :goto_0
    return v4

    .line 326
    .restart local v1    # "ds":Landroid/net/NetworkInfo$DetailedState;
    .restart local v3    # "st":Landroid/net/NetworkInfo$State;
    :cond_1
    sget-object v6, Landroid/net/NetworkInfo$DetailedState;->IDLE:Landroid/net/NetworkInfo$DetailedState;

    if-eq v1, v6, :cond_2

    sget-object v6, Landroid/net/NetworkInfo$DetailedState;->SCANNING:Landroid/net/NetworkInfo$DetailedState;

    if-eq v1, v6, :cond_2

    sget-object v6, Landroid/net/NetworkInfo$State;->CONNECTING:Landroid/net/NetworkInfo$State;

    if-ne v3, v6, :cond_0

    :cond_2
    move v4, v5

    goto :goto_0
.end method

.method private processResult(Landroid/os/Message;)V
    .locals 17
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 55
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;

    .line 56
    .local v2, "httpResponseHandler":Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;
    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;->getResponse()Lorg/apache/http/HttpResponse;

    move-result-object v13

    .line 57
    .local v13, "httpResponse":Lorg/apache/http/HttpResponse;
    if-eqz v13, :cond_b

    .line 58
    invoke-interface {v13}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v16

    .line 60
    .local v16, "responseResultEntity":Lorg/apache/http/HttpEntity;
    if-eqz v16, :cond_a

    .line 62
    :try_start_0
    invoke-interface {v13}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->statusCode:I

    .line 63
    invoke-interface {v13}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->strStatus:Ljava/lang/String;

    .line 67
    invoke-interface {v13}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v3

    const/16 v4, 0xc8

    if-ne v3, v4, :cond_6

    .line 68
    const-string v3, "Content-Encoding"

    invoke-interface {v13, v3}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v11

    .line 70
    .local v11, "contentEncoding":Lorg/apache/http/Header;
    if-eqz v11, :cond_4

    .line 71
    invoke-interface {v11}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v3

    const-string v4, "gzip"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 72
    const-string v3, ""

    const-string v4, "using gzip"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    new-instance v14, Ljava/util/zip/GZIPInputStream;

    .line 74
    invoke-interface/range {v16 .. v16}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v14, v3}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    .line 76
    .local v14, "inStream":Ljava/io/InputStream;
    new-instance v15, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v15}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 78
    .local v15, "outStream":Ljava/io/ByteArrayOutputStream;
    const/4 v9, 0x0

    .line 79
    .local v9, "a":I
    :goto_0
    invoke-virtual {v14}, Ljava/io/InputStream;->read()I

    move-result v9

    const/4 v3, -0x1

    if-eq v9, v3, :cond_2

    .line 80
    invoke-virtual {v15, v9}, Ljava/io/ByteArrayOutputStream;->write(I)V
    :try_end_0
    .catch Landroid/net/ParseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 117
    .end local v9    # "a":I
    .end local v11    # "contentEncoding":Lorg/apache/http/Header;
    .end local v14    # "inStream":Ljava/io/InputStream;
    .end local v15    # "outStream":Ljava/io/ByteArrayOutputStream;
    :catch_0
    move-exception v12

    .line 119
    .local v12, "e":Landroid/net/ParseException;
    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ADVHTTP ParseException : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V

    .line 120
    invoke-virtual {v12}, Landroid/net/ParseException;->printStackTrace()V

    .line 123
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->context:Landroid/content/Context;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v3, :cond_0

    .line 125
    :try_start_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->context:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    new-instance v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$2;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v4, v0, v2, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;Landroid/os/Message;)V

    invoke-virtual {v3, v4}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_2
    .catch Ljava/lang/ClassCastException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 167
    :cond_0
    :goto_1
    if-eqz v16, :cond_1

    .line 169
    :try_start_3
    invoke-interface/range {v16 .. v16}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_6

    .line 215
    .end local v12    # "e":Landroid/net/ParseException;
    .end local v16    # "responseResultEntity":Lorg/apache/http/HttpEntity;
    :cond_1
    :goto_2
    return-void

    .line 82
    .restart local v9    # "a":I
    .restart local v11    # "contentEncoding":Lorg/apache/http/Header;
    .restart local v14    # "inStream":Ljava/io/InputStream;
    .restart local v15    # "outStream":Ljava/io/ByteArrayOutputStream;
    .restart local v16    # "responseResultEntity":Lorg/apache/http/HttpEntity;
    :cond_2
    :try_start_4
    invoke-virtual {v14}, Ljava/io/InputStream;->close()V

    .line 84
    invoke-virtual {v15}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->strResult:Ljava/lang/String;

    .line 85
    invoke-virtual {v15}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 95
    .end local v9    # "a":I
    .end local v11    # "contentEncoding":Lorg/apache/http/Header;
    .end local v14    # "inStream":Ljava/io/InputStream;
    .end local v15    # "outStream":Ljava/io/ByteArrayOutputStream;
    :goto_3
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->responseOnThread:Z

    const/4 v4, 0x1

    if-ne v3, v4, :cond_9

    .line 96
    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->stamp:J

    const-wide/16 v5, -0x1

    cmp-long v3, v3, v5

    if-nez v3, :cond_8

    .line 97
    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->statusCode:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->strStatus:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->strResult:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;->onReceive(IILjava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Landroid/net/ParseException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 167
    :cond_3
    :goto_4
    if-eqz v16, :cond_1

    .line 169
    :try_start_5
    invoke-interface/range {v16 .. v16}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_2

    .line 170
    :catch_1
    move-exception v12

    .line 171
    .local v12, "e":Ljava/lang/Exception;
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 87
    .end local v12    # "e":Ljava/lang/Exception;
    .restart local v11    # "contentEncoding":Lorg/apache/http/Header;
    :cond_4
    :try_start_6
    const-string v3, ""

    const-string v4, "don\'t using gzip"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    const-string v3, "UTF-8"

    move-object/from16 v0, v16

    invoke-static {v0, v3}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->strResult:Ljava/lang/String;
    :try_end_6
    .catch Landroid/net/ParseException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_3

    .line 141
    .end local v11    # "contentEncoding":Lorg/apache/http/Header;
    :catch_2
    move-exception v12

    .line 143
    .restart local v12    # "e":Ljava/lang/Exception;
    :try_start_7
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ADVHTTP Exception : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V

    .line 144
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V

    .line 147
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->context:Landroid/content/Context;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    if-eqz v3, :cond_5

    .line 149
    :try_start_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->context:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    new-instance v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$3;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v4, v0, v2, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$3;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;Landroid/os/Message;)V

    invoke-virtual {v3, v4}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_8
    .catch Ljava/lang/ClassCastException; {:try_start_8 .. :try_end_8} :catch_7
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 167
    :cond_5
    :goto_5
    if-eqz v16, :cond_1

    .line 169
    :try_start_9
    invoke-interface/range {v16 .. v16}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3

    goto/16 :goto_2

    .line 170
    :catch_3
    move-exception v12

    .line 171
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    .line 91
    .end local v12    # "e":Ljava/lang/Exception;
    :cond_6
    :try_start_a
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Rsp Cd is not 200, is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v13}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v5

    invoke-interface {v5}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    const-string v3, "UTF-8"

    move-object/from16 v0, v16

    invoke-static {v0, v3}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->strResult:Ljava/lang/String;
    :try_end_a
    .catch Landroid/net/ParseException; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_3

    .line 167
    :catchall_0
    move-exception v3

    if-eqz v16, :cond_7

    .line 169
    :try_start_b
    invoke-interface/range {v16 .. v16}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_8

    .line 172
    :cond_7
    :goto_6
    throw v3

    .line 100
    :cond_8
    :try_start_c
    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->statusCode:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->strStatus:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->strResult:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->stamp:J

    invoke-virtual/range {v2 .. v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;->onReceive(IILjava/lang/String;Ljava/lang/String;J)V

    goto/16 :goto_4

    .line 103
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->context:Landroid/content/Context;
    :try_end_c
    .catch Landroid/net/ParseException; {:try_start_c .. :try_end_c} :catch_0
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_2
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    if-eqz v3, :cond_3

    .line 105
    :try_start_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->context:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    new-instance v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$1;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v4, v0, v2, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;Landroid/os/Message;)V

    invoke-virtual {v3, v4}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_d
    .catch Ljava/lang/ClassCastException; {:try_start_d .. :try_end_d} :catch_4
    .catch Landroid/net/ParseException; {:try_start_d .. :try_end_d} :catch_0
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_2
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto/16 :goto_4

    .line 111
    :catch_4
    move-exception v10

    .line 112
    .local v10, "ce":Ljava/lang/ClassCastException;
    :try_start_e
    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->statusCode:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->strStatus:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->strResult:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;->onReceive(IILjava/lang/String;Ljava/lang/String;)V

    .line 114
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ADVHTTP : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_e
    .catch Landroid/net/ParseException; {:try_start_e .. :try_end_e} :catch_0
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_2
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto/16 :goto_4

    .line 136
    .end local v10    # "ce":Ljava/lang/ClassCastException;
    .local v12, "e":Landroid/net/ParseException;
    :catch_5
    move-exception v10

    .line 137
    .restart local v10    # "ce":Ljava/lang/ClassCastException;
    :try_start_f
    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    const/4 v4, -0x1

    const-string v5, "HTTP parse error"

    const-string v6, ""

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;->onReceive(IILjava/lang/String;Ljava/lang/String;)V

    .line 138
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ADVHTTP : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    goto/16 :goto_1

    .line 170
    .end local v10    # "ce":Ljava/lang/ClassCastException;
    :catch_6
    move-exception v12

    .line 171
    .local v12, "e":Ljava/lang/Exception;
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    .line 160
    :catch_7
    move-exception v10

    .line 161
    .restart local v10    # "ce":Ljava/lang/ClassCastException;
    :try_start_10
    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    const/4 v4, -0x1

    const-string v5, "HTTP undefined error"

    const-string v6, ""

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;->onReceive(IILjava/lang/String;Ljava/lang/String;)V

    .line 162
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ADVHTTP : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    goto/16 :goto_5

    .line 170
    .end local v10    # "ce":Ljava/lang/ClassCastException;
    .end local v12    # "e":Ljava/lang/Exception;
    :catch_8
    move-exception v12

    .line 171
    .restart local v12    # "e":Ljava/lang/Exception;
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_6

    .line 193
    .end local v12    # "e":Ljava/lang/Exception;
    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->context:Landroid/content/Context;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v3, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->callReceive(Landroid/content/Context;Landroid/os/Message;)V

    goto/16 :goto_2

    .line 213
    .end local v16    # "responseResultEntity":Lorg/apache/http/HttpEntity;
    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->context:Landroid/content/Context;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v3, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->callReceive(Landroid/content/Context;Landroid/os/Message;)V

    goto/16 :goto_2
.end method


# virtual methods
.method public get(ILandroid/content/Context;Ljava/net/URL;Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;)Ljava/lang/Thread;
    .locals 6
    .param p1, "position"    # I
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "url"    # Ljava/net/URL;
    .param p4, "handler"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;

    .prologue
    .line 240
    const/4 v4, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->get(ILandroid/content/Context;Ljava/net/URL;Lorg/apache/http/message/HeaderGroup;Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;)Ljava/lang/Thread;

    move-result-object v0

    return-object v0
.end method

.method public get(ILandroid/content/Context;Ljava/net/URL;Lorg/apache/http/message/HeaderGroup;Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;)Ljava/lang/Thread;
    .locals 7
    .param p1, "position"    # I
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "url"    # Ljava/net/URL;
    .param p4, "headers"    # Lorg/apache/http/message/HeaderGroup;
    .param p5, "handler"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;

    .prologue
    .line 265
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$6;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$6;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;Landroid/content/Context;Ljava/net/URL;Lorg/apache/http/message/HeaderGroup;Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;I)V

    .line 278
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 280
    return-object v0
.end method

.method public getSync(Landroid/content/Context;Ljava/net/URL;Lorg/apache/http/message/HeaderGroup;Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;)Lorg/apache/http/HttpResponse;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "url"    # Ljava/net/URL;
    .param p3, "headers"    # Lorg/apache/http/message/HeaderGroup;
    .param p4, "handler"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;

    .prologue
    .line 285
    invoke-static {p1, p2, p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpClientThread;->get(Landroid/content/Context;Ljava/net/URL;Lorg/apache/http/message/HeaderGroup;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 286
    .local v0, "response":Lorg/apache/http/HttpResponse;
    return-object v0
.end method

.method public post(ILandroid/content/Context;Ljava/net/URL;Ljava/lang/String;Lorg/apache/http/message/HeaderGroup;Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;)Ljava/lang/Thread;
    .locals 8
    .param p1, "position"    # I
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "url"    # Ljava/net/URL;
    .param p4, "body"    # Ljava/lang/String;
    .param p5, "headers"    # Lorg/apache/http/message/HeaderGroup;
    .param p6, "handler"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;

    .prologue
    .line 296
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$7;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move v7, p1

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$7;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;Landroid/content/Context;Ljava/net/URL;Ljava/lang/String;Lorg/apache/http/message/HeaderGroup;Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;I)V

    .line 309
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 311
    return-object v0
.end method

.method public post(ILandroid/content/Context;Ljava/net/URL;Lorg/apache/http/message/HeaderGroup;Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;)Ljava/lang/Thread;
    .locals 7
    .param p1, "position"    # I
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "url"    # Ljava/net/URL;
    .param p4, "headers"    # Lorg/apache/http/message/HeaderGroup;
    .param p5, "handler"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;

    .prologue
    .line 245
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$5;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$5;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;Landroid/content/Context;Ljava/net/URL;Lorg/apache/http/message/HeaderGroup;Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;I)V

    .line 258
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 260
    return-object v0
.end method

.method public post(ILandroid/content/Context;Ljava/net/URL;Ljava/lang/String;Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;)V
    .locals 7
    .param p1, "position"    # I
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "url"    # Ljava/net/URL;
    .param p4, "body"    # Ljava/lang/String;
    .param p5, "handler"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;

    .prologue
    .line 291
    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->post(ILandroid/content/Context;Ljava/net/URL;Ljava/lang/String;Lorg/apache/http/message/HeaderGroup;Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;)Ljava/lang/Thread;

    .line 292
    return-void
.end method
