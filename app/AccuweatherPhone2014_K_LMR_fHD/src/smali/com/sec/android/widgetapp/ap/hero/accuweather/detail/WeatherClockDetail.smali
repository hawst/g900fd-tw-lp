.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
.super Landroid/app/Activity;
.source "WeatherClockDetail.java"

# interfaces
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/model/IActivityVisibleState;


# static fields
.field public static final TOUCH_GAP:F = 50.0f

.field private static mTouchDownX:F

.field private static mTouchDownY:F

.field private static mTz:Ljava/util/TimeZone;


# instance fields
.field NETWORK_TRY:Z

.field arrForcastInfo:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field arrMoreInfo:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field arrWeatherInfo:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private cal_b:Ljava/util/Calendar;

.field public checkAddCity:Z

.field citynames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public detailContext:Landroid/app/Activity;

.field private hourList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;",
            ">;"
        }
    .end annotation
.end field

.field private isIncludeWeeklyInfoInViewPager:Z

.field private isResultByGPS:Z

.field private listBtnText:Landroid/widget/TextView;

.field private listBtnView:Landroid/view/View;

.field mAccessibilityDelegateCompat:Landroid/support/v4/view/AccessibilityDelegateCompat;

.field private mAccessibilityDelegateNoButtonText:Landroid/view/View$AccessibilityDelegate;

.field private mAddToListTopButton:Landroid/widget/ImageView;

.field private mAddTopButton:Landroid/widget/ImageView;

.field private mBottomCityInfo:Landroid/widget/LinearLayout;

.field private mBottomForcastLayout:Landroid/widget/LinearLayout;

.field private mCenterCityInfo:Landroid/widget/LinearLayout;

.field private mCityList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;",
            ">;"
        }
    .end annotation
.end field

.field private mCityNameAndStateLayuout:Landroid/widget/LinearLayout;

.field private mCityNameLayuout:Landroid/widget/LinearLayout;

.field private mCityOtherInfoLayout:Landroid/widget/LinearLayout;

.field private mCityOtherValueLayout:Landroid/widget/LinearLayout;

.field private mCityTempLayout:Landroid/widget/LinearLayout;

.field private mCityTimeAndDateLayout:Landroid/widget/LinearLayout;

.field private mCityWeatherTextLayout:Landroid/widget/LinearLayout;

.field mCubeViewControlListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;

.field private mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

.field private mCubeViewTop:Landroid/widget/RelativeLayout;

.field private mCurBGView:Landroid/widget/ImageView;

.field private mCurrentLocationIcon:Landroid/widget/ImageView;

.field private mDefaultTouchDuration:I

.field private mDeletedialog:Landroid/app/Dialog;

.field private mEnteringRefreshAndCheckVersionHandler:Landroid/os/Handler;

.field private mFiveDayDateText:Landroid/widget/TextView;

.field private mFiveDayFocastInfoLayout:Landroid/widget/LinearLayout;

.field private mFiveDayHiTemp:Landroid/widget/TextView;

.field private mFiveDayImg:Landroid/widget/ImageView;

.field private mFiveDayLowTemp:Landroid/widget/TextView;

.field private mFiveDayProbability:Landroid/widget/TextView;

.field private mFiveDayText:Landroid/widget/TextView;

.field private mForcastLayout:Landroid/widget/LinearLayout;

.field private mFourDayDateText:Landroid/widget/TextView;

.field private mFourDayFocastInfoLayout:Landroid/widget/LinearLayout;

.field private mFourDayHiTemp:Landroid/widget/TextView;

.field private mFourDayImg:Landroid/widget/ImageView;

.field private mFourDayLowTemp:Landroid/widget/TextView;

.field private mFourDayProbability:Landroid/widget/TextView;

.field private mFourDayText:Landroid/widget/TextView;

.field private mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

.field private mGoToListTopButton:Landroid/widget/ImageView;

.field private mIconAnimationHandler:Landroid/os/Handler;

.field private mIconManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;

.field private mIconStopHanler:Landroid/os/Handler;

.field private mImagesFirstframe:Landroid/content/res/TypedArray;

.field private mImagesNormalBG:Landroid/content/res/TypedArray;

.field private mInvisiblePopupMenuHolder:Landroid/view/View;

.field private mIsAcceptTouch:Z

.field public mIsChangeOrientation:Z

.field private mIsCreateActivity:Z

.field public mIsDay:I

.field private mIsShownDst:Z

.field private mIsUpdateBGView:Z

.field mIsVisible:Z

.field private mLogo:Landroid/widget/RelativeLayout;

.field private mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

.field private mMPDelayHandler:Landroid/os/Handler;

.field private mMPView:Landroid/widget/VideoView;

.field private mMainLayout:Landroid/widget/LinearLayout;

.field private mMapInfoPostErrorHandler:Landroid/os/Handler;

.field private mMapInfoPostProcHandler:Landroid/os/Handler;

.field private mMoreTopButton:Landroid/widget/ImageView;

.field private mOneDayDateText:Landroid/widget/TextView;

.field private mOneDayFocastInfoLayout:Landroid/widget/LinearLayout;

.field private mOneDayHiTemp:Landroid/widget/TextView;

.field private mOneDayImg:Landroid/widget/ImageView;

.field private mOneDayLowTemp:Landroid/widget/TextView;

.field private mOneDayProbability:Landroid/widget/TextView;

.field private mOneDayText:Landroid/widget/TextView;

.field private mPageIndicatorLayout:Landroid/widget/LinearLayout;

.field public mPageindIcator:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field private mPopupDialog:Landroid/app/Dialog;

.field private mPrecipitaionOneLine:Landroid/widget/TextView;

.field private mPrecipitaionOneLineLayout:Landroid/widget/LinearLayout;

.field private mPrecipitaionOneLineValue:Landroid/widget/TextView;

.field private mPrecipitaionTwoLine:Landroid/widget/TextView;

.field private mPrecipitaionTwoLineLayout:Landroid/widget/LinearLayout;

.field private mPrecipitaionTwoLineValue:Landroid/widget/TextView;

.field private mRealfeel:Landroid/widget/TextView;

.field private mRealfeelValue:Landroid/widget/TextView;

.field private mReceive:Landroid/content/BroadcastReceiver;

.field private mRefreshDialog:Landroid/app/Dialog;

.field private mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

.field private mSearchXmlInfo:Ljava/lang/String;

.field private mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

.field private mSixDayDateText:Landroid/widget/TextView;

.field private mSixDayFocastInfoLayout:Landroid/widget/LinearLayout;

.field private mSixDayHiTemp:Landroid/widget/TextView;

.field private mSixDayImg:Landroid/widget/ImageView;

.field private mSixDayLowTemp:Landroid/widget/TextView;

.field private mSixDayProbability:Landroid/widget/TextView;

.field private mSixDayText:Landroid/widget/TextView;

.field private mStartAniHandler:Landroid/os/Handler;

.field private mSunState:Landroid/widget/TextView;

.field private mThreeDayDateText:Landroid/widget/TextView;

.field private mThreeDayFocastInfoLayout:Landroid/widget/LinearLayout;

.field private mThreeDayHiTemp:Landroid/widget/TextView;

.field private mThreeDayImg:Landroid/widget/ImageView;

.field private mThreeDayLowTemp:Landroid/widget/TextView;

.field private mThreeDayProbability:Landroid/widget/TextView;

.field private mThreeDayText:Landroid/widget/TextView;

.field private mTimeValue:Landroid/widget/TextView;

.field private mTodayImgLayout:Landroid/widget/RelativeLayout;

.field private mTopCityInfo:Landroid/widget/LinearLayout;

.field private mTopLayout:Landroid/widget/RelativeLayout;

.field private mTouchTimer:Ljava/util/Timer;

.field private mTwoDayDateText:Landroid/widget/TextView;

.field private mTwoDayFocastInfoLayout:Landroid/widget/LinearLayout;

.field private mTwoDayHiTemp:Landroid/widget/TextView;

.field private mTwoDayImg:Landroid/widget/ImageView;

.field private mTwoDayLowTemp:Landroid/widget/TextView;

.field private mTwoDayProbability:Landroid/widget/TextView;

.field private mTwoDayText:Landroid/widget/TextView;

.field private mUVIndex:Landroid/widget/TextView;

.field private mUVIndexValue:Landroid/widget/TextView;

.field private mUiHandler:Landroid/os/Handler;

.field private mUpdateBtn:Landroid/widget/ImageView;

.field private mUpdateFlipper:Landroid/widget/ProgressBar;

.field private mUpdateText:Landroid/widget/TextView;

.field private mUseCurrentLocation:Z

.field private mVideoListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

.field private mWeatherCity:Landroid/widget/TextView;

.field private mWeatherDate:Landroid/widget/TextView;

.field private mWeatherEffectLayout:Landroid/widget/FrameLayout;

.field private mWeatherEffectView:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;

.field private mWeatherHighTemp:Landroid/widget/TextView;

.field private mWeatherIcon:Landroid/widget/ImageView;

.field private mWeatherIconLayout:Landroid/widget/RelativeLayout;

.field private mWeatherLowTemp:Landroid/widget/TextView;

.field private mWeatherTemp:Landroid/widget/TextView;

.field private mWeatherTemper:Landroid/widget/ImageView;

.field private mWeatherText:Landroid/widget/TextView;

.field private mWeelkyDetailViewLayout:Landroid/view/View;

.field mapHandler:Landroid/os/Handler;

.field private mapLocation:Ljava/lang/String;

.field private mapLocationName:Ljava/lang/String;

.field private mapitem:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

.field private menu:Landroid/view/Menu;

.field private optionMenuPopup:Landroid/widget/PopupMenu;

.field public performMapCancled:Z

.field private photosInfo:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;",
            ">;"
        }
    .end annotation
.end field

.field private searchCityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

.field private searchDetailMode:Ljava/lang/Boolean;

.field private searchForcastList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;",
            ">;"
        }
    .end annotation
.end field

.field private searchInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

.field private searchMode:Z

.field private searchTempToday:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

.field private searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

.field private selLocation:Ljava/lang/String;

.field private tempscale:I

.field private toastHint:Landroid/widget/Toast;

.field private todayImgId:I

.field private urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 286
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTz:Ljava/util/TimeZone;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 116
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 128
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTodayImgLayout:Landroid/widget/RelativeLayout;

    .line 131
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMainLayout:Landroid/widget/LinearLayout;

    .line 134
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTopCityInfo:Landroid/widget/LinearLayout;

    .line 136
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityNameAndStateLayuout:Landroid/widget/LinearLayout;

    .line 138
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityNameLayuout:Landroid/widget/LinearLayout;

    .line 140
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherCity:Landroid/widget/TextView;

    .line 142
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCurrentLocationIcon:Landroid/widget/ImageView;

    .line 144
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityTimeAndDateLayout:Landroid/widget/LinearLayout;

    .line 146
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherDate:Landroid/widget/TextView;

    .line 149
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCenterCityInfo:Landroid/widget/LinearLayout;

    .line 152
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityTempLayout:Landroid/widget/LinearLayout;

    .line 154
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherTemp:Landroid/widget/TextView;

    .line 156
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherTemper:Landroid/widget/ImageView;

    .line 158
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityWeatherTextLayout:Landroid/widget/LinearLayout;

    .line 160
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherText:Landroid/widget/TextView;

    .line 163
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mBottomCityInfo:Landroid/widget/LinearLayout;

    .line 165
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityOtherInfoLayout:Landroid/widget/LinearLayout;

    .line 167
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityOtherValueLayout:Landroid/widget/LinearLayout;

    .line 169
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRealfeelValue:Landroid/widget/TextView;

    .line 171
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRealfeel:Landroid/widget/TextView;

    .line 173
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionOneLineLayout:Landroid/widget/LinearLayout;

    .line 174
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionTwoLineLayout:Landroid/widget/LinearLayout;

    .line 176
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionOneLine:Landroid/widget/TextView;

    .line 177
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionOneLineValue:Landroid/widget/TextView;

    .line 179
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionTwoLine:Landroid/widget/TextView;

    .line 180
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionTwoLineValue:Landroid/widget/TextView;

    .line 183
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSunState:Landroid/widget/TextView;

    .line 185
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTimeValue:Landroid/widget/TextView;

    .line 187
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUVIndexValue:Landroid/widget/TextView;

    .line 189
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUVIndex:Landroid/widget/TextView;

    .line 191
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherEffectLayout:Landroid/widget/FrameLayout;

    .line 193
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherEffectView:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;

    .line 195
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPageIndicatorLayout:Landroid/widget/LinearLayout;

    .line 199
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPView:Landroid/widget/VideoView;

    .line 201
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mBottomForcastLayout:Landroid/widget/LinearLayout;

    .line 203
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mForcastLayout:Landroid/widget/LinearLayout;

    .line 205
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCurBGView:Landroid/widget/ImageView;

    .line 207
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherHighTemp:Landroid/widget/TextView;

    .line 209
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherLowTemp:Landroid/widget/TextView;

    .line 211
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherIconLayout:Landroid/widget/RelativeLayout;

    .line 213
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherIcon:Landroid/widget/ImageView;

    .line 236
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->todayImgId:I

    .line 244
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->listBtnView:Landroid/view/View;

    .line 246
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->listBtnText:Landroid/widget/TextView;

    .line 248
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->optionMenuPopup:Landroid/widget/PopupMenu;

    .line 254
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityList:Ljava/util/ArrayList;

    .line 256
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    .line 260
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->detailContext:Landroid/app/Activity;

    .line 264
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    .line 268
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mImagesNormalBG:Landroid/content/res/TypedArray;

    .line 270
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mImagesFirstframe:Landroid/content/res/TypedArray;

    .line 272
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUseCurrentLocation:Z

    .line 274
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIsVisible:Z

    .line 276
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    .line 278
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    .line 280
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRefreshDialog:Landroid/app/Dialog;

    .line 282
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPopupDialog:Landroid/app/Dialog;

    .line 284
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->NETWORK_TRY:Z

    .line 288
    iput-boolean v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIsShownDst:Z

    .line 291
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchMode:Z

    .line 295
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchCityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    .line 297
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    .line 299
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchForcastList:Ljava/util/ArrayList;

    .line 301
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->hourList:Ljava/util/ArrayList;

    .line 303
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTempToday:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    .line 305
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    .line 307
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchDetailMode:Ljava/lang/Boolean;

    .line 309
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSearchXmlInfo:Ljava/lang/String;

    .line 313
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->isResultByGPS:Z

    .line 315
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->isIncludeWeeklyInfoInViewPager:Z

    .line 316
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTopLayout:Landroid/widget/RelativeLayout;

    .line 317
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeelkyDetailViewLayout:Landroid/view/View;

    .line 321
    iput-boolean v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIsUpdateBGView:Z

    .line 323
    const/16 v0, 0x44c

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mDefaultTouchDuration:I

    .line 325
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    .line 329
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPDelayHandler:Landroid/os/Handler;

    .line 344
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$1;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mVideoListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    .line 412
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    .line 414
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewTop:Landroid/widget/RelativeLayout;

    .line 416
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIconManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;

    .line 418
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$2;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIconAnimationHandler:Landroid/os/Handler;

    .line 435
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIconStopHanler:Landroid/os/Handler;

    .line 442
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mStartAniHandler:Landroid/os/Handler;

    .line 444
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIsDay:I

    .line 446
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$3;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$3;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mReceive:Landroid/content/BroadcastReceiver;

    .line 543
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIsCreateActivity:Z

    .line 708
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIsChangeOrientation:Z

    .line 798
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIsAcceptTouch:Z

    .line 1057
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$15;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$15;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mAccessibilityDelegateCompat:Landroid/support/v4/view/AccessibilityDelegateCompat;

    .line 1449
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$16;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$16;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mEnteringRefreshAndCheckVersionHandler:Landroid/os/Handler;

    .line 2454
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->checkAddCity:Z

    .line 2980
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$21;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewControlListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;

    .line 3533
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mDeletedialog:Landroid/app/Dialog;

    .line 3783
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$28;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$28;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUiHandler:Landroid/os/Handler;

    .line 3877
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->performMapCancled:Z

    .line 3889
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$29;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$29;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mapHandler:Landroid/os/Handler;

    .line 3916
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$30;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$30;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMapInfoPostProcHandler:Landroid/os/Handler;

    .line 4008
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$31;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$31;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMapInfoPostErrorHandler:Landroid/os/Handler;

    .line 4331
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$32;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$32;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mAccessibilityDelegateNoButtonText:Landroid/view/View$AccessibilityDelegate;

    return-void
.end method

.method private acceptFlickAnimation()Z
    .locals 4

    .prologue
    .line 801
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIsAcceptTouch:Z

    .line 802
    .local v0, "isAcceptTouch":Z
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IAT = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIsAcceptTouch:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 803
    iget-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIsAcceptTouch:Z

    if-eqz v1, :cond_0

    .line 804
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->resetTouchTimer()V

    .line 805
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setTouchTimer(I)V

    .line 807
    :cond_0
    return v0
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherEffectView:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Ljava/util/Calendar;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Ljava/util/Calendar;
    .param p2, "x2"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .param p3, "x3"    # Z

    .prologue
    .line 116
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setIsDay(Ljava/util/Calendar;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Z)V

    return-void
.end method

.method static synthetic access$1302(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # I

    .prologue
    .line 116
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->todayImgId:I

    return p1
.end method

.method static synthetic access$1400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Z

    .prologue
    .line 116
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->dataUpdate(Z)V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getSearchMode()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->runList()V

    return-void
.end method

.method static synthetic access$1700(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/view/View;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # I

    .prologue
    .line 116
    invoke-direct {p0, p1, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->showToastHint(Landroid/view/View;I)V

    return-void
.end method

.method static synthetic access$1800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGoToListTopButton:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->runAdd()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mStartAniHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mAddTopButton:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->runAddtoList()V

    return-void
.end method

.method static synthetic access$2200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mAddToListTopButton:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mInvisiblePopupMenuHolder:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 116
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->displayOptionMenu(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$2500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMoreTopButton:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityNameAndStateLayuout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$2602(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Landroid/widget/LinearLayout;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityNameAndStateLayuout:Landroid/widget/LinearLayout;

    return-object p1
.end method

.method static synthetic access$2700(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mEnteringRefreshAndCheckVersionHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->refreshWhenEntering()V

    return-void
.end method

.method static synthetic access$2900(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionOneLine:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2902(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionOneLine:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionOneLineLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$3002(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Landroid/widget/LinearLayout;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionOneLineLayout:Landroid/widget/LinearLayout;

    return-object p1
.end method

.method static synthetic access$3100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionTwoLineLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$3102(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Landroid/widget/LinearLayout;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionTwoLineLayout:Landroid/widget/LinearLayout;

    return-object p1
.end method

.method static synthetic access$3200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->runMap()V

    return-void
.end method

.method static synthetic access$3300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->runDeleteCity()V

    return-void
.end method

.method static synthetic access$3400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->runMore()V

    return-void
.end method

.method static synthetic access$3500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->runSetDefaultCity()V

    return-void
.end method

.method static synthetic access$3600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->runSettings()V

    return-void
.end method

.method static synthetic access$3702(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/PopupMenu;)Landroid/widget/PopupMenu;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Landroid/widget/PopupMenu;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->optionMenuPopup:Landroid/widget/PopupMenu;

    return-object p1
.end method

.method static synthetic access$3800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherCity:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3802(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherCity:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$3900()Ljava/util/TimeZone;
    .locals 1

    .prologue
    .line 116
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTz:Ljava/util/TimeZone;

    return-object v0
.end method

.method static synthetic access$3902(Ljava/util/TimeZone;)Ljava/util/TimeZone;
    .locals 0
    .param p0, "x0"    # Ljava/util/TimeZone;

    .prologue
    .line 116
    sput-object p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTz:Ljava/util/TimeZone;

    return-object p0
.end method

.method static synthetic access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # I

    .prologue
    .line 116
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->startCubeIconAnimation(I)V

    return-void
.end method

.method static synthetic access$4000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Ljava/util/Calendar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->cal_b:Ljava/util/Calendar;

    return-object v0
.end method

.method static synthetic access$4002(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Ljava/util/Calendar;)Ljava/util/Calendar;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Ljava/util/Calendar;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->cal_b:Ljava/util/Calendar;

    return-object p1
.end method

.method static synthetic access$4100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherDate:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$4102(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherDate:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$4200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCurrentLocationIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$4202(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCurrentLocationIcon:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$4300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    return v0
.end method

.method static synthetic access$4302(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # I

    .prologue
    .line 116
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    return p1
.end method

.method static synthetic access$4400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherTemper:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$4402(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherTemper:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$4500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherTemp:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$4502(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherTemp:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$4600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherHighTemp:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$4602(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherHighTemp:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$4700(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherLowTemp:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$4702(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherLowTemp:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$4800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherIconLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$4802(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/RelativeLayout;)Landroid/widget/RelativeLayout;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Landroid/widget/RelativeLayout;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherIconLayout:Landroid/widget/RelativeLayout;

    return-object p1
.end method

.method static synthetic access$4900(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$4902(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherIcon:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIsAcceptTouch:Z

    return v0
.end method

.method static synthetic access$5000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIconManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Z

    .prologue
    .line 116
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIsAcceptTouch:Z

    return p1
.end method

.method static synthetic access$5100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIconStopHanler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$5200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$5202(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherText:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$5300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRealfeelValue:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$5302(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRealfeelValue:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$5400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionOneLineValue:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$5402(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionOneLineValue:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$5500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionTwoLine:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$5502(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionTwoLine:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$5600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionTwoLineValue:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$5602(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionTwoLineValue:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$5700(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setVisibilePrecipitationLayout()V

    return-void
.end method

.method static synthetic access$5800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUVIndex:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$5802(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUVIndex:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$5900(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUVIndexValue:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$5902(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUVIndexValue:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->resetTouchTimer()V

    return-void
.end method

.method static synthetic access$6000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSunState:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$6002(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSunState:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$6100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTimeValue:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$6102(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTimeValue:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$6200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->isIncludeWeeklyInfoInViewPager:Z

    return v0
.end method

.method static synthetic access$6300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchCityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    return-object v0
.end method

.method static synthetic access$6400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    return-object v0
.end method

.method static synthetic access$6502(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Landroid/widget/LinearLayout;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTopCityInfo:Landroid/widget/LinearLayout;

    return-object p1
.end method

.method static synthetic access$6602(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Landroid/widget/LinearLayout;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityNameLayuout:Landroid/widget/LinearLayout;

    return-object p1
.end method

.method static synthetic access$6702(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Landroid/widget/LinearLayout;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityTimeAndDateLayout:Landroid/widget/LinearLayout;

    return-object p1
.end method

.method static synthetic access$6802(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Landroid/widget/LinearLayout;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCenterCityInfo:Landroid/widget/LinearLayout;

    return-object p1
.end method

.method static synthetic access$6902(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Landroid/widget/LinearLayout;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityTempLayout:Landroid/widget/LinearLayout;

    return-object p1
.end method

.method static synthetic access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # I

    .prologue
    .line 116
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setTouchTimer(I)V

    return-void
.end method

.method static synthetic access$7002(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Landroid/widget/LinearLayout;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityWeatherTextLayout:Landroid/widget/LinearLayout;

    return-object p1
.end method

.method static synthetic access$7102(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Landroid/widget/LinearLayout;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mBottomCityInfo:Landroid/widget/LinearLayout;

    return-object p1
.end method

.method static synthetic access$7202(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Landroid/widget/LinearLayout;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityOtherInfoLayout:Landroid/widget/LinearLayout;

    return-object p1
.end method

.method static synthetic access$7302(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Landroid/widget/LinearLayout;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityOtherValueLayout:Landroid/widget/LinearLayout;

    return-object p1
.end method

.method static synthetic access$7402(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRealfeel:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$7500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 116
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->initWeatherClockDetailView(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$7600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->deleteCity()V

    return-void
.end method

.method static synthetic access$7700(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/app/Dialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mDeletedialog:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$7702(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/app/Dialog;)Landroid/app/Dialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Landroid/app/Dialog;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mDeletedialog:Landroid/app/Dialog;

    return-object p1
.end method

.method static synthetic access$7800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getLocationInfo()V

    return-void
.end method

.method static synthetic access$7900(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCurBGView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$8002(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/app/Dialog;)Landroid/app/Dialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Landroid/app/Dialog;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRefreshDialog:Landroid/app/Dialog;

    return-object p1
.end method

.method static synthetic access$8100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    return-object v0
.end method

.method static synthetic access$8102(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    return-object p1
.end method

.method static synthetic access$8200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->stopRefreshAnimation()V

    return-void
.end method

.method static synthetic access$8300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    return-object v0
.end method

.method static synthetic access$8400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->hideRefreshDialog()V

    return-void
.end method

.method static synthetic access$8500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->updateCityList()V

    return-void
.end method

.method static synthetic access$8600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
    .param p1, "x1"    # Z

    .prologue
    .line 116
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->updateUI(Z)V

    return-void
.end method

.method static synthetic access$8700(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mapitem:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    return-object v0
.end method

.method static synthetic access$8800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getMccCode()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 116
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIsCreateActivity:Z

    return v0
.end method

.method private cancelCubeIconAnimation(I)V
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 3626
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getIconAnimation()I

    move-result v1

    if-nez v1, :cond_1

    .line 3640
    :cond_0
    :goto_0
    return-void

    .line 3630
    :cond_1
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stopCIconA : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3631
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    const v2, 0x7f0800ad

    invoke-virtual {v1, p1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->getLayoutInController(II)Landroid/view/ViewGroup;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 3633
    .local v0, "layout":Landroid/widget/RelativeLayout;
    if-eqz v0, :cond_2

    .line 3634
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIconManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;

    invoke-virtual {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->cancelIconAnimation(Landroid/view/ViewGroup;)V

    .line 3637
    :cond_2
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIconStopHanler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 3638
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIconStopHanler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private changeToDetailView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2109
    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->updateSearchMode(I)V

    .line 2111
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "flags"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 2113
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->allStopEffect()V

    .line 2114
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->clearHandlers()V

    .line 2116
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->todayImgId:I

    .line 2118
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->updateCityList()V

    .line 2120
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setupCubeViewPager()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2121
    const-string v0, ""

    const-string v1, "Set cube view"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2125
    :goto_0
    return-void

    .line 2123
    :cond_0
    invoke-direct {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->updateUI(Z)V

    goto :goto_0
.end method

.method private checkPrecipitaionLayout()V
    .locals 2

    .prologue
    .line 1543
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionOneLine:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionOneLineLayout:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_0

    .line 1544
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionOneLine:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 1545
    .local v0, "vto":Landroid/view/ViewTreeObserver;
    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$17;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$17;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1558
    .end local v0    # "vto":Landroid/view/ViewTreeObserver;
    :cond_0
    return-void
.end method

.method private checkResultCode(I)I
    .locals 2
    .param p1, "resultCode"    # I

    .prologue
    .line 2716
    const/4 v0, 0x0

    .line 2717
    .local v0, "result":I
    const/4 v1, -0x1

    if-ne v1, p1, :cond_1

    .line 2718
    const/16 v0, 0x3e7

    .line 2722
    :cond_0
    :goto_0
    return v0

    .line 2719
    :cond_1
    if-nez p1, :cond_0

    .line 2720
    const/16 v0, 0x3e7

    goto :goto_0
.end method

.method private cleanResource()V
    .locals 0

    .prologue
    .line 4141
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->clearHandlers()V

    .line 4142
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->clearView()V

    .line 4143
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->clearListener()V

    .line 4150
    return-void
.end method

.method private clearHandlers()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4153
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mapHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 4154
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mStartAniHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 4155
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIconAnimationHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 4156
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIconStopHanler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 4157
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 4158
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mEnteringRefreshAndCheckVersionHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 4159
    return-void
.end method

.method private dataUpdate(Z)V
    .locals 5
    .param p1, "isEnteringRefresh"    # Z

    .prologue
    .line 3660
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "dataUpdate : eR = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3661
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->isNetWorkConnected(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 3663
    const v2, 0x7f0d0020

    invoke-static {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    .line 3700
    :goto_0
    return-void

    .line 3667
    :cond_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    const-string v3, "cityId:current"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 3668
    iget-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUseCurrentLocation:Z

    if-eqz v2, :cond_5

    .line 3669
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->detailContext:Landroid/app/Activity;

    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getLocationCheck(Landroid/content/Context;)I

    move-result v0

    .line 3670
    .local v0, "locationSetting":I
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "dataUpdate : lS = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", isV = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-boolean v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isVerizon:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3671
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->detailContext:Landroid/app/Activity;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->isLocationAvailable(Landroid/content/Context;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    if-ne p1, v2, :cond_1

    sget-boolean v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isVerizon:Z

    if-nez v2, :cond_1

    .line 3673
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getLocationInfo()V

    goto :goto_0

    .line 3674
    :cond_1
    const/16 v2, 0xbc0

    if-ne v0, v2, :cond_2

    sget-boolean v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isVerizon:Z

    if-nez v2, :cond_2

    .line 3676
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getPreferenceGPSOnlyDoNotShow(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_2

    if-nez p1, :cond_2

    .line 3678
    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$25;

    invoke-direct {v2, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$25;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    invoke-static {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->showGPSonlyDialog(Landroid/app/Activity;Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/LocationGetter;)Landroid/app/Dialog;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPopupDialog:Landroid/app/Dialog;

    goto :goto_0

    .line 3684
    :cond_2
    const/16 v2, 0xbbb

    if-ne v0, v2, :cond_3

    .line 3685
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->detailContext:Landroid/app/Activity;

    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->showGPSoffDialog(Landroid/app/Activity;)Landroid/app/Dialog;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPopupDialog:Landroid/app/Dialog;

    goto :goto_0

    .line 3686
    :cond_3
    const/16 v2, 0xbb8

    if-ne v0, v2, :cond_4

    .line 3687
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getLocationInfo()V

    goto :goto_0

    .line 3689
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getLocationInfo()V

    goto/16 :goto_0

    .line 3692
    .end local v0    # "locationSetting":I
    :cond_5
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getRealLocation(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 3693
    .local v1, "realLocation":Ljava/lang/String;
    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_6

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getDataRefresh(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3695
    :cond_6
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getDataRefresh(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3698
    .end local v1    # "realLocation":Ljava/lang/String;
    :cond_7
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getDataRefresh(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private deleteCity()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 3554
    const-string v5, ""

    const-string v6, "Delete city"

    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3555
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->allStopEffect()V

    .line 3556
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 3557
    .local v0, "citySize":I
    const/4 v3, 0x0

    .line 3558
    .local v3, "i":I
    :goto_0
    if-ge v3, v0, :cond_0

    .line 3559
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 3563
    :cond_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    .line 3564
    .local v1, "delLocation":Ljava/lang/String;
    add-int/lit8 v3, v3, 0x1

    .line 3565
    if-lt v3, v0, :cond_1

    .line 3566
    const/4 v3, 0x0

    .line 3568
    :cond_1
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    .line 3569
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->detailContext:Landroid/app/Activity;

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateLastSelectedLocation(Landroid/content/Context;Ljava/lang/String;)I

    .line 3571
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 3572
    .local v2, "deleteItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3573
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->detailContext:Landroid/app/Activity;

    invoke-static {v5, v2, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->deleteCitys(Landroid/content/Context;Ljava/util/ArrayList;Z)I

    .line 3575
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSettingsForMain(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-result-object v4

    .line 3576
    .local v4, "settings":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->updateCityList()V

    .line 3577
    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getLastSelectLocation()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    .line 3579
    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getTempScaleSetting()I

    move-result v5

    iput v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    .line 3581
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityList:Ljava/util/ArrayList;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v5, v7, :cond_4

    .line 3582
    :cond_2
    const-string v5, ""

    const-string v6, "City size is null"

    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3583
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->finish()V

    .line 3602
    :goto_1
    return-void

    .line 3558
    .end local v1    # "delLocation":Ljava/lang/String;
    .end local v2    # "deleteItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v4    # "settings":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 3586
    .restart local v1    # "delLocation":Ljava/lang/String;
    .restart local v2    # "deleteItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v4    # "settings":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    :cond_4
    iput v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->todayImgId:I

    .line 3587
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getBackgroundVideo()I

    move-result v5

    if-ne v5, v7, :cond_5

    .line 3588
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCurBGView:Landroid/widget/ImageView;

    if-eqz v5, :cond_5

    .line 3589
    invoke-virtual {p0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setBGAlpha(Z)V

    .line 3592
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setupCubeViewPager()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 3593
    const-string v5, ""

    const-string v6, "setup Cube View"

    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3597
    :goto_2
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->resetTouchTimer()V

    .line 3598
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getWIdgetMode(Landroid/content/Context;)I

    move-result v5

    const/16 v6, 0xfa1

    if-eq v5, v6, :cond_6

    .line 3599
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setAddViewControl()V

    .line 3601
    :cond_6
    const/16 v5, 0x3e8

    invoke-direct {p0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setTouchTimer(I)V

    goto :goto_1

    .line 3595
    :cond_7
    invoke-direct {p0, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->updateUI(Z)V

    goto :goto_2
.end method

.method private displayOptionMenu(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v5, 0x7f0d00d1

    const/4 v4, 0x0

    .line 2822
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->optionMenuPopup:Landroid/widget/PopupMenu;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRefreshDialog:Landroid/app/Dialog;

    if-eqz v1, :cond_1

    .line 2904
    :cond_0
    :goto_0
    return-void

    .line 2825
    :cond_1
    new-instance v0, Landroid/widget/PopupMenu;

    const v1, 0x800005

    invoke-direct {v0, p0, p1, v1}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;I)V

    .line 2832
    .local v0, "menuPopup":Landroid/widget/PopupMenu;
    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const v2, 0x7f0f000c

    .line 2833
    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v3

    .line 2832
    invoke-virtual {v1, v2, v3}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 2835
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getWIdgetMode(Landroid/content/Context;)I

    move-result v1

    const/16 v2, 0xfa1

    if-ne v1, v2, :cond_2

    .line 2836
    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    const v2, 0x7f0d003d

    invoke-static {p0, v1, v2, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->menuItemGrayByTitle(Landroid/content/Context;Landroid/view/Menu;IZ)V

    .line 2838
    :cond_2
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getWIdgetMode(Landroid/content/Context;)I

    move-result v1

    const/16 v2, 0xfa2

    if-ne v1, v2, :cond_3

    .line 2839
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090013

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2840
    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {p0, v1, v5, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->menuItemGrayByTitle(Landroid/content/Context;Landroid/view/Menu;IZ)V

    .line 2847
    :goto_1
    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$19;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$19;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    invoke-virtual {v0, v1}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 2895
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->optionMenuPopup:Landroid/widget/PopupMenu;

    .line 2897
    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$20;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$20;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    invoke-virtual {v0, v1}, Landroid/widget/PopupMenu;->setOnDismissListener(Landroid/widget/PopupMenu$OnDismissListener;)V

    .line 2903
    invoke-virtual {v0}, Landroid/widget/PopupMenu;->show()V

    goto :goto_0

    .line 2842
    :cond_3
    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    invoke-static {p0, v1, v5, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->menuItemGrayByTitle(Landroid/content/Context;Landroid/view/Menu;IZ)V

    goto :goto_1
.end method

.method private getDayOfWeek(Landroid/widget/TextView;II)Ljava/lang/String;
    .locals 4
    .param p1, "daytext"    # Landroid/widget/TextView;
    .param p2, "iDayOfWeek"    # I
    .param p3, "iLength"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    .line 2143
    invoke-static {p2, p3}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v0

    .line 2144
    .local v0, "sDayOfWeek":Ljava/lang/String;
    const/16 v1, 0x58

    const/16 v2, 0x5f

    const/16 v3, 0x64

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2146
    packed-switch p2, :pswitch_data_0

    .line 2154
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 2148
    :pswitch_0
    const/16 v1, 0xea

    const/16 v2, 0x74

    const/16 v3, 0x45

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 2146
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private getLocationInfo()V
    .locals 5

    .prologue
    .line 3704
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    if-nez v1, :cond_0

    .line 3705
    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    .line 3707
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->startUpdateAnimation()V

    .line 3708
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->showRefreshDialog()V

    .line 3709
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "gLI : start :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3710
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUiHandler:Landroid/os/Handler;

    const-string v4, "cityId:current"

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->doManualRefresh(ILandroid/os/Handler;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3715
    :goto_0
    return-void

    .line 3712
    :catch_0
    move-exception v0

    .line 3713
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method private getMccCode()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x3

    .line 4320
    const-string v2, "mcc"

    const-string v3, "@@@"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4321
    const-string v2, "phone"

    .line 4322
    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 4323
    .local v0, "manager":Landroid/telephony/TelephonyManager;
    const-string v2, "mcc"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "code = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4324
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v1

    .line 4325
    .local v1, "mcc":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v5, :cond_0

    .line 4326
    const/4 v2, 0x0

    invoke-virtual {v1, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 4327
    :cond_0
    return-object v1
.end method

.method private getSearchMode()Z
    .locals 1

    .prologue
    .line 341
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchMode:Z

    return v0
.end method

.method private hideRefreshDialog()V
    .locals 1

    .prologue
    .line 3773
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRefreshDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 3775
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRefreshDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3779
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRefreshDialog:Landroid/app/Dialog;

    .line 3781
    :cond_0
    return-void

    .line 3776
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private initWeatherClockDetailView(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1397
    const v0, 0x7f0800c7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mBottomForcastLayout:Landroid/widget/LinearLayout;

    .line 1398
    const v0, 0x7f0800c8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mForcastLayout:Landroid/widget/LinearLayout;

    .line 1400
    const v0, 0x7f0800cb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mOneDayFocastInfoLayout:Landroid/widget/LinearLayout;

    .line 1401
    const v0, 0x7f0800cc

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mOneDayText:Landroid/widget/TextView;

    .line 1402
    const v0, 0x7f0800cd

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mOneDayDateText:Landroid/widget/TextView;

    .line 1403
    const v0, 0x7f0800ce

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mOneDayImg:Landroid/widget/ImageView;

    .line 1404
    const v0, 0x7f0800d1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mOneDayHiTemp:Landroid/widget/TextView;

    .line 1405
    const v0, 0x7f0800d2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mOneDayLowTemp:Landroid/widget/TextView;

    .line 1406
    const v0, 0x7f0800d4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mOneDayProbability:Landroid/widget/TextView;

    .line 1408
    const v0, 0x7f0800d5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTwoDayFocastInfoLayout:Landroid/widget/LinearLayout;

    .line 1409
    const v0, 0x7f0800d6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTwoDayText:Landroid/widget/TextView;

    .line 1410
    const v0, 0x7f0800d7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTwoDayDateText:Landroid/widget/TextView;

    .line 1411
    const v0, 0x7f0800d8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTwoDayImg:Landroid/widget/ImageView;

    .line 1412
    const v0, 0x7f0800db

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTwoDayHiTemp:Landroid/widget/TextView;

    .line 1413
    const v0, 0x7f0800dc

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTwoDayLowTemp:Landroid/widget/TextView;

    .line 1414
    const v0, 0x7f0800de

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTwoDayProbability:Landroid/widget/TextView;

    .line 1416
    const v0, 0x7f0800df

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mThreeDayFocastInfoLayout:Landroid/widget/LinearLayout;

    .line 1417
    const v0, 0x7f0800e0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mThreeDayText:Landroid/widget/TextView;

    .line 1418
    const v0, 0x7f0800e1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mThreeDayDateText:Landroid/widget/TextView;

    .line 1419
    const v0, 0x7f0800e2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mThreeDayImg:Landroid/widget/ImageView;

    .line 1420
    const v0, 0x7f0800e5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mThreeDayHiTemp:Landroid/widget/TextView;

    .line 1421
    const v0, 0x7f0800e6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mThreeDayLowTemp:Landroid/widget/TextView;

    .line 1422
    const v0, 0x7f0800e8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mThreeDayProbability:Landroid/widget/TextView;

    .line 1424
    const v0, 0x7f0800e9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFourDayFocastInfoLayout:Landroid/widget/LinearLayout;

    .line 1425
    const v0, 0x7f0800ea

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFourDayText:Landroid/widget/TextView;

    .line 1426
    const v0, 0x7f0800eb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFourDayDateText:Landroid/widget/TextView;

    .line 1427
    const v0, 0x7f0800ec

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFourDayImg:Landroid/widget/ImageView;

    .line 1428
    const v0, 0x7f0800ef

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFourDayHiTemp:Landroid/widget/TextView;

    .line 1429
    const v0, 0x7f0800f0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFourDayLowTemp:Landroid/widget/TextView;

    .line 1430
    const v0, 0x7f0800f2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFourDayProbability:Landroid/widget/TextView;

    .line 1432
    const v0, 0x7f0800f3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFiveDayFocastInfoLayout:Landroid/widget/LinearLayout;

    .line 1433
    const v0, 0x7f0800f4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFiveDayText:Landroid/widget/TextView;

    .line 1434
    const v0, 0x7f0800f5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFiveDayDateText:Landroid/widget/TextView;

    .line 1435
    const v0, 0x7f0800f6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFiveDayImg:Landroid/widget/ImageView;

    .line 1436
    const v0, 0x7f0800f9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFiveDayHiTemp:Landroid/widget/TextView;

    .line 1437
    const v0, 0x7f0800fa

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFiveDayLowTemp:Landroid/widget/TextView;

    .line 1438
    const v0, 0x7f0800fc

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFiveDayProbability:Landroid/widget/TextView;

    .line 1440
    const v0, 0x7f0800fd

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSixDayFocastInfoLayout:Landroid/widget/LinearLayout;

    .line 1441
    const v0, 0x7f0800fe

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSixDayText:Landroid/widget/TextView;

    .line 1442
    const v0, 0x7f0800ff

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSixDayDateText:Landroid/widget/TextView;

    .line 1443
    const v0, 0x7f080100

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSixDayImg:Landroid/widget/ImageView;

    .line 1444
    const v0, 0x7f080103

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSixDayHiTemp:Landroid/widget/TextView;

    .line 1445
    const v0, 0x7f080104

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSixDayLowTemp:Landroid/widget/TextView;

    .line 1446
    const v0, 0x7f080106

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSixDayProbability:Landroid/widget/TextView;

    .line 1447
    return-void
.end method

.method private insertSearchCityToDB()I
    .locals 12

    .prologue
    .line 2581
    const/4 v5, 0x1

    .line 2584
    .local v5, "result":I
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->isMaxCityListAdded(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-boolean v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->isResultByGPS:Z

    if-eqz v7, :cond_10

    .line 2585
    :cond_0
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchCityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v3

    .line 2586
    .local v3, "location":Ljava/lang/String;
    iget-boolean v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->isResultByGPS:Z

    if-eqz v7, :cond_7

    .line 2587
    const-string v3, "cityId:current"

    .line 2588
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchCityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchCityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setRealLocation(Ljava/lang/String;)V

    .line 2589
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchCityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    invoke-virtual {v7, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setLocation(Ljava/lang/String;)V

    .line 2595
    :goto_0
    const/4 v4, 0x0

    .line 2597
    .local v4, "message":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchCityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    .line 2598
    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v8

    .line 2597
    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->isRegisteredToCityList(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_9

    .line 2600
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchCityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSearchXmlInfo:Ljava/lang/String;

    invoke-static {v7, v8, v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->insertCity(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)I

    move-result v5

    .line 2603
    iget-boolean v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->isResultByGPS:Z

    if-eqz v7, :cond_1

    const/4 v7, 0x1

    if-ne v5, v7, :cond_1

    .line 2604
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getCheckCurrentCityLocation(Landroid/content/Context;)I

    move-result v7

    if-nez v7, :cond_1

    .line 2605
    const/4 v7, 0x1

    invoke-static {p0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateCheckCurrentCityLocation(Landroid/content/Context;I)I

    .line 2609
    :cond_1
    invoke-direct {p0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->checkResultCode(I)I

    move-result v6

    .line 2610
    .local v6, "resultCode":I
    const/16 v7, 0x3e7

    if-ne v7, v6, :cond_2

    .line 2611
    const/16 v7, 0x3e7

    const/4 v8, 0x0

    invoke-virtual {p0, v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setResult(ILandroid/content/Intent;)V

    .line 2612
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->finish()V

    .line 2615
    :cond_2
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->isMaxCityListAdded(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 2617
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0d0023

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    const/16 v10, 0xa

    .line 2618
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    .line 2616
    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2669
    .end local v6    # "resultCode":I
    :cond_3
    :goto_1
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->photosInfo:Ljava/util/ArrayList;

    if-eqz v7, :cond_5

    .line 2670
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->photosInfo:Ljava/util/ArrayList;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;->getCityId()Ljava/lang/String;

    move-result-object v0

    .line 2671
    .local v0, "cityId":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->detailContext:Landroid/app/Activity;

    invoke-static {v7, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getExistPhotos(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 2672
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->detailContext:Landroid/app/Activity;

    invoke-static {v7, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->deletePhotosInfo(Landroid/content/Context;Ljava/lang/String;)I

    .line 2675
    :cond_4
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->detailContext:Landroid/app/Activity;

    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->photosInfo:Ljava/util/ArrayList;

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->insertPhotosInfo(Landroid/content/Context;Ljava/util/ArrayList;)I

    .line 2677
    .end local v0    # "cityId":Ljava/lang/String;
    :cond_5
    invoke-static {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;Ljava/lang/String;)V

    .line 2679
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchCityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    .line 2680
    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v7

    .line 2679
    invoke-static {p0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->isRegisteredToDetailHourInfo(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_f

    .line 2681
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchCityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    .line 2682
    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    .line 2681
    invoke-static {p0, v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateDetailHourInfo(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)I

    move-result v5

    .line 2683
    invoke-direct {p0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->checkResultCode(I)I

    move-result v6

    .line 2685
    .restart local v6    # "resultCode":I
    const/16 v7, 0x3e7

    if-ne v7, v6, :cond_6

    .line 2686
    const/16 v7, 0x3e7

    const/4 v8, 0x0

    invoke-virtual {p0, v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setResult(ILandroid/content/Intent;)V

    .line 2687
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->finish()V

    .line 2703
    :cond_6
    :goto_2
    invoke-static {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateLastSelectedLocation(Landroid/content/Context;Ljava/lang/String;)I

    .line 2705
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    .line 2707
    const/4 v7, 0x1

    .line 2712
    .end local v3    # "location":Ljava/lang/String;
    .end local v4    # "message":Ljava/lang/String;
    .end local v6    # "resultCode":I
    :goto_3
    return v7

    .line 2592
    .restart local v3    # "location":Ljava/lang/String;
    :cond_7
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchCityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    .line 2620
    .restart local v4    # "message":Ljava/lang/String;
    .restart local v6    # "resultCode":I
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0d0024

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 2623
    .end local v6    # "resultCode":I
    :cond_9
    iget-boolean v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->isResultByGPS:Z

    if-eqz v7, :cond_3

    .line 2624
    sget-boolean v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isCurCityAtTopInCityList:Z

    if-nez v7, :cond_a

    sget-boolean v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isCurCityAtTopInCityList:Z

    if-nez v7, :cond_e

    .line 2626
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getCheckCurrentCityLocation(Landroid/content/Context;)I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_e

    .line 2628
    :cond_a
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchCityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchCityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSearchXmlInfo:Ljava/lang/String;

    invoke-static {v7, v8, v9, v10, v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateCity(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)I

    move-result v5

    .line 2631
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getCheckCurrentCityLocation(Landroid/content/Context;)I

    move-result v7

    if-nez v7, :cond_b

    .line 2632
    const/4 v7, 0x1

    invoke-static {p0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateCheckCurrentCityLocation(Landroid/content/Context;I)I

    .line 2651
    :cond_b
    :goto_4
    invoke-direct {p0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->checkResultCode(I)I

    move-result v6

    .line 2652
    .restart local v6    # "resultCode":I
    if-nez v6, :cond_c

    .line 2654
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->detailContext:Landroid/app/Activity;

    invoke-static {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/BManager;->isCheckSupportClock(Landroid/content/Context;)Z

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_c

    .line 2655
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->detailContext:Landroid/app/Activity;

    const/4 v8, 0x1

    const-string v9, "ISCD"

    invoke-static {v7, v8, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/BManager;->sendBroadCastToProvider(Landroid/content/Context;ZLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2661
    :cond_c
    :goto_5
    const/16 v7, 0x3e7

    if-ne v7, v6, :cond_d

    .line 2662
    const/16 v7, 0x3e7

    const/4 v8, 0x0

    invoke-virtual {p0, v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setResult(ILandroid/content/Intent;)V

    .line 2663
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->finish()V

    .line 2666
    :cond_d
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0d0024

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 2635
    .end local v6    # "resultCode":I
    :cond_e
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2636
    .local v1, "deleteList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2637
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const/4 v8, 0x1

    invoke-static {v7, v1, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->deleteCitys(Landroid/content/Context;Ljava/util/ArrayList;Z)I

    move-result v5

    .line 2639
    const/4 v7, 0x1

    if-ne v5, v7, :cond_b

    .line 2640
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->deletePhotosInfo(Landroid/content/Context;Ljava/util/ArrayList;)I

    .line 2642
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchCityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    iget-object v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSearchXmlInfo:Ljava/lang/String;

    invoke-static {v7, v8, v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->insertCity(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)I

    move-result v5

    .line 2645
    const/4 v7, 0x1

    if-ne v5, v7, :cond_b

    .line 2646
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const/4 v8, 0x1

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateCheckCurrentCityLocation(Landroid/content/Context;I)I

    goto :goto_4

    .line 2657
    .end local v1    # "deleteList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v6    # "resultCode":I
    :catch_0
    move-exception v2

    .line 2658
    .local v2, "e":Ljava/lang/Exception;
    const-string v7, ""

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "db : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 2690
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v6    # "resultCode":I
    :cond_f
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchCityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    .line 2691
    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    .line 2690
    invoke-static {p0, v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->insertDetailHourInfo(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)I

    move-result v5

    .line 2692
    invoke-direct {p0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->checkResultCode(I)I

    move-result v6

    .line 2694
    .restart local v6    # "resultCode":I
    const/16 v7, 0x3e7

    if-ne v7, v6, :cond_6

    .line 2695
    const/16 v7, 0x3e7

    const/4 v8, 0x0

    invoke-virtual {p0, v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setResult(ILandroid/content/Intent;)V

    .line 2697
    const/16 v7, 0x3e7

    if-ne v7, v6, :cond_6

    .line 2698
    const/16 v7, 0x3e7

    const/4 v8, 0x0

    invoke-virtual {p0, v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setResult(ILandroid/content/Intent;)V

    .line 2699
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->finish()V

    goto/16 :goto_2

    .line 2709
    .end local v3    # "location":Ljava/lang/String;
    .end local v4    # "message":Ljava/lang/String;
    .end local v6    # "resultCode":I
    :cond_10
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0d0023

    .line 2710
    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    const/16 v10, 0xa

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    .line 2709
    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;Ljava/lang/String;)V

    .line 2712
    const/4 v7, 0x0

    goto/16 :goto_3
.end method

.method private makeForcastDateString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "datestring"    # Ljava/lang/String;

    .prologue
    .line 1579
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 1580
    .local v0, "date":Ljava/lang/StringBuffer;
    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1581
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private performMapCities()V
    .locals 2

    .prologue
    .line 3905
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mapHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 3906
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mapHandler:Landroid/os/Handler;

    const v1, -0x13d30

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 3909
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setupGetMapInfoManager()V

    .line 3911
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mapitem:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->setAddedMapCityInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;)V

    .line 3913
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->performMapCities()V

    .line 3914
    return-void
.end method

.method private refreshWhenEntering()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 3643
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getRefreshEnteringSettings(Landroid/content/Context;)I

    move-result v1

    if-ne v1, v3, :cond_0

    .line 3644
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getWIdgetMode(Landroid/content/Context;)I

    move-result v1

    const/16 v2, 0xfa2

    if-ne v1, v2, :cond_1

    .line 3645
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090013

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3646
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSelectedDefaultLocation(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    .line 3650
    :goto_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    invoke-static {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getDetailWeatherInfo(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    move-result-object v0

    .line 3652
    .local v0, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3653
    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getUpdateDate()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkRefreshCondition(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3654
    invoke-direct {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->dataUpdate(Z)V

    .line 3657
    .end local v0    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    :cond_0
    return-void

    .line 3648
    :cond_1
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getLastSelectedLocation(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    goto :goto_0
.end method

.method private registerTimeReceiver()V
    .locals 2

    .prologue
    .line 528
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 529
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.TIME_TICK"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 530
    const-string v1, "android.intent.action.TIME_SET"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 532
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mReceive:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 533
    return-void
.end method

.method private resetTouchTimer()V
    .locals 1

    .prologue
    .line 831
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTouchTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 832
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTouchTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 834
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIsAcceptTouch:Z

    .line 835
    return-void
.end method

.method private runAdd()V
    .locals 3

    .prologue
    const/16 v2, 0x2edf

    .line 2471
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2472
    .local v0, "intentAdd":Landroid/content/Intent;
    const-string v1, "flags"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2473
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2474
    invoke-virtual {p0, v0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2475
    return-void
.end method

.method private runAddtoList()V
    .locals 3

    .prologue
    .line 2457
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->insertSearchCityToDB()I

    move-result v0

    .line 2459
    .local v0, "rtn":I
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2462
    const/4 v1, -0x1

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setResult(ILandroid/content/Intent;)V

    .line 2464
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->finish()V

    .line 2468
    :cond_0
    return-void
.end method

.method private runDeleteCity()V
    .locals 4

    .prologue
    .line 3535
    const-string v0, ""

    const-string v1, "display Delete dialLog"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3536
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->detailContext:Landroid/app/Activity;

    const/16 v1, 0x3fa

    const/4 v2, 0x1

    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$22;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$22;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->showDialog(Landroid/content/Context;IILcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mDeletedialog:Landroid/app/Dialog;

    .line 3544
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mDeletedialog:Landroid/app/Dialog;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$23;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$23;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 3551
    return-void
.end method

.method private runList()V
    .locals 23

    .prologue
    .line 2747
    const/4 v12, 0x0

    .line 2749
    .local v12, "intentCityList":Landroid/content/Intent;
    new-instance v12, Landroid/content/Intent;

    .end local v12    # "intentCityList":Landroid/content/Intent;
    const-class v20, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v12, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2752
    .restart local v12    # "intentCityList":Landroid/content/Intent;
    const-string v20, "flags"

    const/16 v21, -0x7530

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2753
    const-string v20, "searchlocation"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2754
    const/high16 v20, 0x24000000

    move/from16 v0, v20

    invoke-virtual {v12, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2757
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getCitySelectAnimation()I

    move-result v20

    if-nez v20, :cond_0

    .line 2758
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->startActivity(Landroid/content/Intent;)V

    .line 2818
    :goto_0
    return-void

    .line 2760
    :cond_0
    const/4 v6, 0x0

    .line 2761
    .local v6, "cityIndex":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityList:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v18

    .line 2762
    .local v18, "totalCitySize":I
    :goto_1
    move/from16 v0, v18

    if-ge v6, v0, :cond_1

    .line 2763
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityList:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    .line 2764
    .local v14, "item":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;
    invoke-virtual {v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_3

    .line 2768
    .end local v14    # "item":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f0b0010

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v20

    move/from16 v0, v20

    float-to-int v3, v0

    .line 2769
    .local v3, "ah":I
    const/16 v16, 0x0

    .line 2770
    .local v16, "row":I
    const/4 v7, 0x0

    .line 2771
    .local v7, "col":I
    const/4 v10, 0x0

    .line 2773
    .local v10, "hoffset":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->detailContext:Landroid/app/Activity;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getOrientation(Landroid/content/Context;)I

    move-result v20

    if-nez v20, :cond_4

    .line 2774
    const/16 v16, 0x0

    .line 2775
    rem-int/lit8 v7, v6, 0x3

    .line 2776
    const-string v20, "itemIndex"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2796
    :cond_2
    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f0b000e

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v20

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    mul-int v9, v20, v7

    .line 2797
    .local v9, "gridw":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f0b000f

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v20

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    mul-int v8, v20, v16

    .line 2799
    .local v8, "gridh":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    move-object/from16 v20, v0

    .line 2800
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getDetailWeatherInfo(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    move-result-object v11

    .line 2802
    .local v11, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v17

    .line 2803
    .local v17, "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v21

    .line 2804
    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v22

    .line 2803
    invoke-static/range {v20 .. v22}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkDayOrNight(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v13

    .line 2805
    .local v13, "isDay":Z
    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getIconNum()I

    move-result v20

    const/16 v21, 0x102

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-static {v0, v13, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getTodayBG(Landroid/content/Context;ZII)I

    move-result v15

    .line 2808
    .local v15, "resID":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-static {v0, v15}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 2809
    .local v5, "bm2":Landroid/graphics/Bitmap;
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2811
    .local v4, "b":Landroid/os/Bundle;
    const-string v20, "android:animType"

    const/16 v21, 0x4

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2812
    const-string v20, "android:animThumbnail"

    move-object/from16 v0, v20

    invoke-virtual {v4, v0, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2813
    const-string v20, "android:animStartX"

    move-object/from16 v0, v20

    invoke-virtual {v4, v0, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2814
    const-string v20, "android:animStartY"

    add-int v21, v8, v3

    add-int v21, v21, v10

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2816
    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 2762
    .end local v3    # "ah":I
    .end local v4    # "b":Landroid/os/Bundle;
    .end local v5    # "bm2":Landroid/graphics/Bitmap;
    .end local v7    # "col":I
    .end local v8    # "gridh":I
    .end local v9    # "gridw":I
    .end local v10    # "hoffset":I
    .end local v11    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .end local v13    # "isDay":Z
    .end local v15    # "resID":I
    .end local v16    # "row":I
    .end local v17    # "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    .restart local v14    # "item":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;
    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_1

    .line 2778
    .end local v14    # "item":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;
    .restart local v3    # "ah":I
    .restart local v7    # "col":I
    .restart local v10    # "hoffset":I
    .restart local v16    # "row":I
    :cond_4
    div-int/lit8 v16, v6, 0x2

    .line 2779
    rem-int/lit8 v7, v6, 0x2

    .line 2780
    const/16 v20, 0x3

    move/from16 v0, v18

    move/from16 v1, v20

    if-le v0, v1, :cond_2

    const/16 v20, 0x3

    move/from16 v0, v20

    if-le v6, v0, :cond_2

    .line 2781
    rem-int/lit8 v16, v16, 0x2

    .line 2782
    const/16 v19, 0x0

    .line 2783
    .local v19, "value":I
    rem-int/lit8 v20, v18, 0x2

    if-nez v20, :cond_6

    .line 2784
    const/16 v19, 0x2

    .line 2788
    :goto_3
    sub-int v20, v18, v19

    move/from16 v0, v20

    if-lt v6, v0, :cond_5

    .line 2789
    const/16 v16, 0x1

    .line 2790
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f0b000f

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v20

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    div-int/lit8 v10, v20, 0x3

    .line 2791
    mul-int/lit8 v10, v10, 0x2

    .line 2793
    :cond_5
    const-string v20, "itemIndex"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/16 :goto_2

    .line 2786
    :cond_6
    const/16 v19, 0x1

    goto :goto_3
.end method

.method private runMap()V
    .locals 12

    .prologue
    const/4 v9, 0x0

    .line 2491
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getWIdgetMode(Landroid/content/Context;)I

    move-result v7

    const/16 v8, 0xfa1

    if-ne v7, v8, :cond_0

    .line 2572
    :goto_0
    return-void

    .line 2495
    :cond_0
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->isNetWorkConnected(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 2496
    const v7, 0x7f0d0020

    invoke-static {p0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    goto :goto_0

    .line 2500
    :cond_1
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 2501
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    iput-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mapLocation:Ljava/lang/String;

    .line 2506
    :goto_1
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mapLocation:Ljava/lang/String;

    const-string v8, "cityId:current"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 2507
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getRealLocation(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 2508
    .local v5, "realLocation":Ljava/lang/String;
    if-eqz v5, :cond_2

    const-string v7, ""

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 2509
    iput-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mapLocation:Ljava/lang/String;

    .line 2512
    .end local v5    # "realLocation":Ljava/lang/String;
    :cond_2
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherCity:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    if-eqz v7, :cond_3

    .line 2513
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherCity:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setMapLocationName(Ljava/lang/String;)V

    .line 2516
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->showRefreshDialog()V

    .line 2518
    iput-boolean v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->performMapCancled:Z

    .line 2519
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    if-eqz v7, :cond_6

    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    const-string v8, "cityId:current"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 2520
    const-string v7, "cityId:current"

    invoke-static {p0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getMapLocation(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mapitem:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    .line 2527
    :goto_2
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mapitem:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mapLocation:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->setLocation(Ljava/lang/String;)V

    .line 2537
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mapitem:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mapitem:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getTemp()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->setTemp(Ljava/lang/String;)V

    .line 2538
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mapitem:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIsDay:I

    if-nez v7, :cond_7

    const-string v7, "True"

    :goto_3
    invoke-virtual {v8, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->setIsDayTime(Ljava/lang/String;)V

    .line 2540
    const/4 v2, 0x0

    .line 2541
    .local v2, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    const/4 v1, 0x0

    .line 2542
    .local v1, "hinfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    if-eqz v7, :cond_8

    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    const-string v8, "cityId:current"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 2543
    const-string v7, "cityId:current"

    invoke-static {p0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getDetailWeatherInfo(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    move-result-object v2

    .line 2553
    :goto_4
    if-eqz v2, :cond_4

    .line 2554
    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v6

    .line 2555
    .local v6, "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v7

    .line 2556
    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v9

    .line 2555
    invoke-static {v7, v8, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkDayOrNight(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    .line 2557
    .local v3, "isDay":Z
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mapitem:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getIcon()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    invoke-static {v7, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertIcon(IZ)I

    move-result v4

    .line 2558
    .local v4, "mIcon":I
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mapitem:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->setIcon(Ljava/lang/String;)V

    .line 2559
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mapitem:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v8

    iget v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    .line 2560
    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getCurrentTemp()F

    move-result v10

    const/4 v11, 0x0

    .line 2559
    invoke-static {v8, v9, v10, v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIFZ)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->setTemp(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2571
    .end local v1    # "hinfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .end local v2    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .end local v3    # "isDay":Z
    .end local v4    # "mIcon":I
    .end local v6    # "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    :cond_4
    :goto_5
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->performMapCities()V

    goto/16 :goto_0

    .line 2503
    :cond_5
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mapLocation:Ljava/lang/String;

    goto/16 :goto_1

    .line 2523
    :cond_6
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mapLocation:Ljava/lang/String;

    invoke-static {p0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getMapLocation(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mapitem:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    goto/16 :goto_2

    .line 2538
    :cond_7
    :try_start_1
    const-string v7, "False"

    goto :goto_3

    .line 2550
    .restart local v1    # "hinfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .restart local v2    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    :cond_8
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    invoke-static {p0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getDetailWeatherInfo(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v2

    goto :goto_4

    .line 2567
    .end local v1    # "hinfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .end local v2    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    :catch_0
    move-exception v0

    .line 2568
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_5
.end method

.method private runMore()V
    .locals 5

    .prologue
    .line 2480
    :try_start_0
    const-string v3, "DETAIL_MORE"

    const/4 v4, 0x0

    invoke-virtual {p0, p0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setViewUri(Landroid/content/Context;Ljava/lang/String;I)Landroid/net/Uri;

    move-result-object v2

    .line 2481
    .local v2, "uri":Landroid/net/Uri;
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2482
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2483
    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2488
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "uri":Landroid/net/Uri;
    :goto_0
    return-void

    .line 2484
    :catch_0
    move-exception v0

    .line 2485
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v3, ""

    const-string v4, "ANFE more"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2486
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private runSetDefaultCity()V
    .locals 3

    .prologue
    .line 2726
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2727
    .local v0, "intentDelete":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateLastSelectedLocation(Landroid/content/Context;Ljava/lang/String;)I

    .line 2728
    const/16 v1, 0x55ef

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2729
    return-void
.end method

.method private runSettings()V
    .locals 3

    .prologue
    .line 2732
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2733
    .local v0, "settingsIntent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateLastSelectedLocation(Landroid/content/Context;Ljava/lang/String;)I

    .line 2734
    const/16 v1, 0x55ef

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2735
    return-void
.end method

.method private setAddViewControl()V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 1371
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getSearchMode()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1394
    :cond_0
    :goto_0
    return-void

    .line 1374
    :cond_1
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->detailContext:Landroid/app/Activity;

    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->isMaxCityListAdded(Landroid/content/Context;)Z

    move-result v0

    .line 1376
    .local v0, "isMaxCityAdded":Z
    const/4 v2, 0x0

    .local v2, "menudividerid":I
    const/4 v1, 0x0

    .line 1378
    .local v1, "menudivider1id":I
    sget-boolean v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isLollipopOS:Z

    if-nez v3, :cond_2

    .line 1379
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const-string v4, "menudivider"

    const-string v5, "id"

    const-string v6, "com.sec.android.widgetapp.ap.hero.accuweather"

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 1380
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const-string v4, "menudivider1"

    const-string v5, "id"

    const-string v6, "com.sec.android.widgetapp.ap.hero.accuweather"

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 1383
    :cond_2
    if-eqz v0, :cond_3

    .line 1384
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mAddTopButton:Landroid/widget/ImageView;

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1385
    sget-boolean v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isLollipopOS:Z

    if-nez v3, :cond_0

    .line 1386
    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1389
    :cond_3
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mAddTopButton:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1390
    sget-boolean v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isLollipopOS:Z

    if-nez v3, :cond_0

    .line 1391
    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private setIsDay(Ljava/util/Calendar;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Z)V
    .locals 2
    .param p1, "cal"    # Ljava/util/Calendar;
    .param p2, "info"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .param p3, "isDay"    # Z

    .prologue
    const/4 v0, 0x1

    .line 2129
    if-nez p2, :cond_0

    .line 2130
    const-string v0, ""

    const-string v1, " not info !"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2134
    :goto_0
    return-void

    .line 2133
    :cond_0
    if-ne p3, v0, :cond_1

    const/4 v0, 0x0

    :cond_1
    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIsDay:I

    goto :goto_0
.end method

.method private setTTSForSPenModel()V
    .locals 2

    .prologue
    .line 4341
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mAccessibilityDelegateNoButtonText:Landroid/view/View$AccessibilityDelegate;

    if-nez v0, :cond_0

    .line 4342
    const-string v0, ""

    const-string v1, "sTTSFSPM mADNBT n"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4364
    :goto_0
    return-void

    .line 4346
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGoToListTopButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 4347
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGoToListTopButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mAccessibilityDelegateNoButtonText:Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 4352
    :goto_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mAddTopButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 4353
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mAddTopButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mAccessibilityDelegateNoButtonText:Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 4358
    :goto_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mAddToListTopButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 4359
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mAddToListTopButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mAccessibilityDelegateNoButtonText:Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    goto :goto_0

    .line 4349
    :cond_1
    const-string v0, ""

    const-string v1, "sTTSFSPM mGLTB n"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 4355
    :cond_2
    const-string v0, ""

    const-string v1, "sTTSFSPM mATB n"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 4361
    :cond_3
    const-string v0, ""

    const-string v1, "sTTSFSPM mATLTB n"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setTouchTimer(I)V
    .locals 6
    .param p1, "manualTime"    # I

    .prologue
    .line 811
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mDefaultTouchDuration:I

    .line 813
    .local v1, "time":I
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getBackgroundVideo()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 814
    if-lez p1, :cond_0

    .line 815
    move v1, p1

    .line 818
    :cond_0
    new-instance v2, Ljava/util/Timer;

    invoke-direct {v2}, Ljava/util/Timer;-><init>()V

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTouchTimer:Ljava/util/Timer;

    .line 819
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTouchTimer:Ljava/util/Timer;

    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$4;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$4;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    int-to-long v4, v1

    invoke-virtual {v2, v3, v4, v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 828
    :goto_0
    return-void

    .line 824
    :catch_0
    move-exception v0

    .line 825
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 826
    const-string v2, ""

    const-string v3, "stt nul"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setVisibilePrecipitationLayout()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1532
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionTwoLineLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 1533
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionOneLineLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 1534
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionOneLineLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1535
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionTwoLineLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1541
    :cond_0
    :goto_0
    return-void

    .line 1537
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionOneLineLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1538
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionTwoLineLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private setupCubeViewPager()Z
    .locals 7

    .prologue
    .line 3479
    const/4 v2, 0x0

    .line 3480
    .local v2, "i":I
    const/4 v0, 0x0

    .line 3481
    .local v0, "citySize":I
    const/4 v3, 0x0

    .line 3483
    .local v3, "isSetView":Z
    const-string v4, ""

    const-string v5, "setupCubeViewPager"

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3486
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 3487
    :goto_0
    if-ge v2, v0, :cond_0

    .line 3488
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 3493
    :cond_0
    if-lt v2, v0, :cond_1

    .line 3494
    const/4 v2, 0x0

    .line 3495
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    .line 3498
    :cond_1
    iget-boolean v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIsChangeOrientation:Z

    const/4 v5, 0x1

    if-eq v4, v5, :cond_2

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    if-nez v4, :cond_5

    .line 3500
    :cond_2
    new-instance v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-direct {v4, p0, v5, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;-><init>(Landroid/content/Context;II)V

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    .line 3503
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    if-nez v4, :cond_3

    .line 3504
    new-instance v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    invoke-direct {v4, p0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;-><init>(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;)V

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    .line 3505
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mVideoListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    invoke-virtual {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->setOnMediaPlayerListener(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;)V

    .line 3506
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPView:Landroid/widget/VideoView;

    invoke-virtual {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->setMPVideoView(Landroid/widget/VideoView;)V

    .line 3510
    :cond_3
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewControlListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;

    invoke-virtual {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->setListener(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;)V

    .line 3511
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    const v4, 0x7f08008e

    invoke-virtual {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    const v5, 0x7f08008f

    .line 3512
    invoke-virtual {p0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    .line 3511
    invoke-virtual {v6, v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->setViewArray(Landroid/view/ViewGroup;Landroid/view/ViewGroup;)V

    .line 3513
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->initRotationViewController()Z

    move-result v3

    .line 3518
    :goto_1
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    invoke-virtual {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->setViEffectSetting(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;)V

    .line 3519
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    invoke-virtual {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->setMPController(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)V

    .line 3530
    :goto_2
    return v3

    .line 3487
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 3515
    :cond_5
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->resetData(II)Z
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v3

    goto :goto_1

    .line 3520
    :catch_0
    move-exception v1

    .line 3521
    .local v1, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v1}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    .line 3522
    const-string v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "sCVP ioob ct size = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3523
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->finish()V

    goto :goto_2

    .line 3524
    .end local v1    # "e":Ljava/lang/IndexOutOfBoundsException;
    :catch_1
    move-exception v1

    .line 3525
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 3526
    const-string v4, ""

    const-string v5, "sCVP NPE "

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3527
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->finish()V

    goto :goto_2
.end method

.method private setupGetMapInfoManager()V
    .locals 2

    .prologue
    .line 2575
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->detailContext:Landroid/app/Activity;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    invoke-static {v0, p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->getInstance(Landroid/content/Context;Landroid/app/Activity;I)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    .line 2576
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMapInfoPostProcHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->setPostProcHandler(Landroid/os/Handler;)V

    .line 2577
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMapInfoPostErrorHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->setErrorHandler(Landroid/os/Handler;)V

    .line 2578
    return-void
.end method

.method private showRefreshDialog()V
    .locals 2

    .prologue
    .line 3718
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 3720
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->isActivityVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3721
    const-string v0, ""

    const-string v1, " RefreshDialog Show! "

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3723
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->allStopEffect()V

    .line 3724
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRefreshDialog:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 3725
    const/16 v0, 0x3ef

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$26;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$26;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    invoke-static {p0, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->showDialog(Landroid/content/Context;ILcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRefreshDialog:Landroid/app/Dialog;

    .line 3732
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRefreshDialog:Landroid/app/Dialog;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$27;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$27;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 3764
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRefreshDialog:Landroid/app/Dialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 3765
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRefreshDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 3767
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->resetTouchTimer()V

    .line 3770
    :cond_0
    return-void
.end method

.method private showToastHint(Landroid/view/View;I)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "stringId"    # I

    .prologue
    .line 1025
    .line 1028
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1025
    invoke-static {p0, p1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toastHint(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->toastHint:Landroid/widget/Toast;

    .line 1030
    return-void
.end method

.method private startCubeIconAnimation(I)V
    .locals 5
    .param p1, "position"    # I

    .prologue
    .line 3604
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getIconAnimation()I

    move-result v1

    if-nez v1, :cond_1

    .line 3623
    :cond_0
    :goto_0
    return-void

    .line 3608
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    const v2, 0x7f0800ad

    invoke-virtual {v1, p1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->getLayoutInController(II)Landroid/view/ViewGroup;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 3610
    .local v0, "layout":Landroid/widget/RelativeLayout;
    if-eqz v0, :cond_2

    .line 3611
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIconManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;

    invoke-virtual {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->startIconAnimation(Landroid/view/ViewGroup;)V

    .line 3614
    :cond_2
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getIconAnimationRepeat()I

    move-result v1

    if-nez v1, :cond_0

    .line 3615
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIconStopHanler:Landroid/os/Handler;

    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$24;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$24;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/widget/RelativeLayout;)V

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIconManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    .line 3621
    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getIconDurationVal()I

    move-result v4

    invoke-virtual {v3, v0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;->calcAnimationDuration(Landroid/view/ViewGroup;I)J

    move-result-wide v3

    .line 3615
    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private startUpdateAnimation()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3854
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUpdateFlipper:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUpdateBtn:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    .line 3866
    :cond_0
    const v0, 0x7f08006a

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUpdateFlipper:Landroid/widget/ProgressBar;

    .line 3867
    const v0, 0x7f080069

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUpdateBtn:Landroid/widget/ImageView;

    .line 3870
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUpdateBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 3871
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUpdateBtn:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3872
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUpdateFlipper:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 3873
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUpdateFlipper:Landroid/widget/ProgressBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setFocusable(Z)V

    .line 3874
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUpdateFlipper:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->requestFocus()Z

    .line 3875
    return-void
.end method

.method private stopAnimationByFlick()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1034
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->getViewIndex()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->cancelCubeIconAnimation(I)V

    .line 1035
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->getPreViewIndex()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->cancelCubeIconAnimation(I)V

    .line 1036
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->getNextViewIndex()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->cancelCubeIconAnimation(I)V

    .line 1037
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mStartAniHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1038
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->getMPDelayHandler()Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1039
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherEffectView:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getWeatherEffect()I

    move-result v1

    if-ne v1, v3, :cond_0

    .line 1040
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherEffectView:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->stopAnimation()V

    .line 1042
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIsAcceptTouch:Z

    .line 1043
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    if-eqz v1, :cond_1

    .line 1044
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->stopByFlick()V

    .line 1045
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->resetFirstFrameAnim()V

    .line 1047
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getBackgroundVideo()I

    move-result v1

    if-ne v1, v3, :cond_2

    .line 1048
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setBGAlpha(Z)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1055
    :cond_2
    :goto_0
    return-void

    .line 1051
    :catch_0
    move-exception v0

    .line 1052
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 1053
    const-string v1, ""

    const-string v2, "sABF NPE"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private stopRefreshAnimation()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 3828
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUpdateFlipper:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUpdateBtn:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    .line 3840
    :cond_0
    const v0, 0x7f08006a

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUpdateFlipper:Landroid/widget/ProgressBar;

    .line 3841
    const v0, 0x7f080069

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUpdateBtn:Landroid/widget/ImageView;

    .line 3844
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUpdateFlipper:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUpdateBtn:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 3845
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUpdateBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 3846
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUpdateBtn:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3847
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUpdateFlipper:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 3848
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUpdateBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 3849
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUpdateBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->requestFocus()Z

    .line 3851
    :cond_2
    return-void
.end method

.method private unregisterTimeReceiver()V
    .locals 2

    .prologue
    .line 537
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mReceive:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 541
    :goto_0
    return-void

    .line 538
    :catch_0
    move-exception v0

    .line 539
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method private updateCityList()V
    .locals 2

    .prologue
    .line 1560
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getWIdgetMode(Landroid/content/Context;)I

    move-result v0

    const/16 v1, 0xfa1

    if-ne v0, v1, :cond_0

    .line 1561
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getCurrentCity(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityList:Ljava/util/ArrayList;

    .line 1565
    :goto_0
    return-void

    .line 1563
    :cond_0
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getAllCityList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityList:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method private updateUI(Z)V
    .locals 3
    .param p1, "bUpdateCube"    # Z

    .prologue
    .line 1568
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1576
    :cond_0
    :goto_0
    return-void

    .line 1571
    :cond_1
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " ud UI = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1573
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1574
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewControlListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;

    invoke-interface {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;->setCurrentView()V

    goto :goto_0
.end method


# virtual methods
.method public allStopEffect()V
    .locals 1

    .prologue
    .line 2202
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->allStopEffect(Z)V

    .line 2203
    return-void
.end method

.method public allStopEffect(Z)V
    .locals 4
    .param p1, "isEnd"    # Z

    .prologue
    const/4 v2, 0x1

    .line 2206
    if-nez p1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getBackgroundVideo()I

    move-result v1

    if-ne v1, v2, :cond_0

    .line 2207
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setBGAlpha(Z)V

    .line 2210
    :cond_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    if-nez v1, :cond_1

    .line 2211
    const-string v1, ""

    const-string v2, "mpCon is null!!!!!"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2218
    :goto_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->getMPDelayHandler()Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2224
    :goto_1
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->getViewIndex()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->cancelCubeIconAnimation(I)V

    .line 2225
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->getPreViewIndex()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->cancelCubeIconAnimation(I)V

    .line 2226
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->getNextViewIndex()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->cancelCubeIconAnimation(I)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2232
    :goto_2
    :try_start_2
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->setIsRunningAni(Z)V
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_2

    .line 2238
    :goto_3
    :try_start_3
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherEffectView:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->stopAnimation()V
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_3

    .line 2243
    :goto_4
    return-void

    .line 2213
    :cond_1
    :try_start_4
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "aSE Video Playing = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->isPlaying()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", isEnd = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2214
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->stop()V

    .line 2215
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->setMPState(I)V

    .line 2216
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->resetFirstFrameAnim()V
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0

    .line 2219
    :catch_0
    move-exception v0

    .line 2220
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "aSE : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 2227
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v0

    .line 2228
    .restart local v0    # "e":Ljava/lang/NullPointerException;
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "aSE : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 2233
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_2
    move-exception v0

    .line 2234
    .restart local v0    # "e":Ljava/lang/NullPointerException;
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "aSE : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 2239
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_3
    move-exception v0

    .line 2240
    .restart local v0    # "e":Ljava/lang/NullPointerException;
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "aSE : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4
.end method

.method public clearListener()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4162
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUpdateBtn:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 4163
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUpdateBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 4164
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUpdateBtn:Landroid/widget/ImageView;

    .line 4167
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewControlListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;

    if-eqz v0, :cond_1

    .line 4168
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewControlListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;

    invoke-interface {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;->clearView()V

    .line 4169
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewControlListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;

    .line 4172
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mOneDayFocastInfoLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    .line 4173
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mOneDayFocastInfoLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 4175
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTwoDayFocastInfoLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_3

    .line 4176
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTwoDayFocastInfoLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 4178
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mThreeDayFocastInfoLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_4

    .line 4179
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mThreeDayFocastInfoLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 4181
    :cond_4
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFourDayFocastInfoLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_5

    .line 4182
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFourDayFocastInfoLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 4184
    :cond_5
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFiveDayFocastInfoLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_6

    .line 4185
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFiveDayFocastInfoLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 4187
    :cond_6
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSixDayFocastInfoLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_7

    .line 4188
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSixDayFocastInfoLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 4190
    :cond_7
    return-void
.end method

.method public clearTextEffect()V
    .locals 2

    .prologue
    .line 1286
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrWeatherInfo:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 1287
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrWeatherInfo:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 1288
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrWeatherInfo:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1289
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrWeatherInfo:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->clearAllTextEffect()V

    .line 1287
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1294
    .end local v0    # "i":I
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrMoreInfo:Ljava/util/ArrayList;

    if-eqz v1, :cond_3

    .line 1295
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrMoreInfo:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 1296
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrMoreInfo:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1297
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrMoreInfo:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->clearAllTextEffect()V

    .line 1295
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1301
    .end local v0    # "i":I
    :cond_3
    sget-boolean v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isLollipopOS:Z

    if-nez v1, :cond_5

    .line 1302
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    if-eqz v1, :cond_5

    .line 1303
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 1304
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 1305
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->clearAllTextEffect()V

    .line 1303
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1310
    .end local v0    # "i":I
    :cond_5
    return-void
.end method

.method public clearView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4193
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMainLayout:Landroid/widget/LinearLayout;

    .line 4194
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCurBGView:Landroid/widget/ImageView;

    .line 4196
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    if-eqz v0, :cond_0

    .line 4197
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->onDestroy()V

    .line 4198
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    .line 4201
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPView:Landroid/widget/VideoView;

    if-eqz v0, :cond_1

    .line 4202
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->destroyDrawingCache()V

    .line 4203
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPView:Landroid/widget/VideoView;

    .line 4206
    :cond_1
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherEffectLayout:Landroid/widget/FrameLayout;

    .line 4207
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherEffectView:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;

    .line 4209
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPageIndicatorLayout:Landroid/widget/LinearLayout;

    .line 4210
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityTempLayout:Landroid/widget/LinearLayout;

    .line 4212
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPageindIcator:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 4213
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPageindIcator:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 4214
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPageindIcator:Ljava/util/ArrayList;

    .line 4217
    :cond_2
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mBottomForcastLayout:Landroid/widget/LinearLayout;

    .line 4218
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mForcastLayout:Landroid/widget/LinearLayout;

    .line 4220
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mOneDayFocastInfoLayout:Landroid/widget/LinearLayout;

    .line 4221
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mOneDayText:Landroid/widget/TextView;

    .line 4222
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mOneDayDateText:Landroid/widget/TextView;

    .line 4223
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mOneDayImg:Landroid/widget/ImageView;

    .line 4224
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mOneDayHiTemp:Landroid/widget/TextView;

    .line 4225
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mOneDayLowTemp:Landroid/widget/TextView;

    .line 4226
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mOneDayProbability:Landroid/widget/TextView;

    .line 4228
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTwoDayFocastInfoLayout:Landroid/widget/LinearLayout;

    .line 4229
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTwoDayText:Landroid/widget/TextView;

    .line 4230
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTwoDayDateText:Landroid/widget/TextView;

    .line 4231
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTwoDayImg:Landroid/widget/ImageView;

    .line 4232
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTwoDayHiTemp:Landroid/widget/TextView;

    .line 4233
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTwoDayLowTemp:Landroid/widget/TextView;

    .line 4234
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTwoDayProbability:Landroid/widget/TextView;

    .line 4236
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mThreeDayFocastInfoLayout:Landroid/widget/LinearLayout;

    .line 4237
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mThreeDayText:Landroid/widget/TextView;

    .line 4238
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mThreeDayDateText:Landroid/widget/TextView;

    .line 4239
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mThreeDayImg:Landroid/widget/ImageView;

    .line 4240
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mThreeDayHiTemp:Landroid/widget/TextView;

    .line 4241
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mThreeDayLowTemp:Landroid/widget/TextView;

    .line 4242
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mThreeDayProbability:Landroid/widget/TextView;

    .line 4244
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFourDayFocastInfoLayout:Landroid/widget/LinearLayout;

    .line 4245
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFourDayText:Landroid/widget/TextView;

    .line 4246
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFourDayDateText:Landroid/widget/TextView;

    .line 4247
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFourDayImg:Landroid/widget/ImageView;

    .line 4248
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFourDayHiTemp:Landroid/widget/TextView;

    .line 4249
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFourDayLowTemp:Landroid/widget/TextView;

    .line 4250
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFourDayProbability:Landroid/widget/TextView;

    .line 4252
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFiveDayFocastInfoLayout:Landroid/widget/LinearLayout;

    .line 4253
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFiveDayText:Landroid/widget/TextView;

    .line 4254
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFiveDayDateText:Landroid/widget/TextView;

    .line 4255
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFiveDayImg:Landroid/widget/ImageView;

    .line 4256
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFiveDayHiTemp:Landroid/widget/TextView;

    .line 4257
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFiveDayLowTemp:Landroid/widget/TextView;

    .line 4258
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFiveDayProbability:Landroid/widget/TextView;

    .line 4260
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSixDayFocastInfoLayout:Landroid/widget/LinearLayout;

    .line 4261
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSixDayText:Landroid/widget/TextView;

    .line 4262
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSixDayDateText:Landroid/widget/TextView;

    .line 4263
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSixDayImg:Landroid/widget/ImageView;

    .line 4264
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSixDayHiTemp:Landroid/widget/TextView;

    .line 4265
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSixDayLowTemp:Landroid/widget/TextView;

    .line 4266
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSixDayProbability:Landroid/widget/TextView;

    .line 4268
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUpdateText:Landroid/widget/TextView;

    .line 4269
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUpdateBtn:Landroid/widget/ImageView;

    .line 4270
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUpdateFlipper:Landroid/widget/ProgressBar;

    .line 4272
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewTop:Landroid/widget/RelativeLayout;

    .line 4274
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mLogo:Landroid/widget/RelativeLayout;

    .line 4275
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMoreTopButton:Landroid/widget/ImageView;

    .line 4276
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGoToListTopButton:Landroid/widget/ImageView;

    .line 4277
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mAddToListTopButton:Landroid/widget/ImageView;

    .line 4278
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mAddTopButton:Landroid/widget/ImageView;

    .line 4279
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mInvisiblePopupMenuHolder:Landroid/view/View;

    .line 4280
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v8, 0x2

    const/4 v5, 0x1

    .line 838
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getWIdgetMode(Landroid/content/Context;)I

    move-result v6

    const/16 v7, 0xfa1

    if-ne v6, v7, :cond_0

    .line 839
    const-string v5, ""

    const-string v6, "skip flick : easy"

    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 840
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v5

    .line 905
    :goto_0
    return v5

    .line 843
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getSearchMode()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 844
    const-string v5, ""

    const-string v6, "skip flick : search mode"

    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 845
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v5

    goto :goto_0

    .line 848
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    .line 849
    .local v3, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    .line 851
    .local v4, "y":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 852
    .local v0, "action":I
    if-ne v0, v5, :cond_8

    .line 853
    sget v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTouchDownX:F

    sub-float v1, v6, v3

    .line 854
    .local v1, "dx":F
    sget v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTouchDownY:F

    sub-float v2, v6, v4

    .line 864
    .local v2, "dy":F
    sget v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTouchDownY:F

    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewTop:Landroid/widget/RelativeLayout;

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getTop()I

    move-result v7

    int-to-float v7, v7

    cmpl-float v6, v6, v7

    if-ltz v6, :cond_2

    sget v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTouchDownY:F

    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewTop:Landroid/widget/RelativeLayout;

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getBottom()I

    move-result v7

    int-to-float v7, v7

    cmpg-float v6, v6, v7

    if-lez v6, :cond_3

    :cond_2
    sget v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTouchDownY:F

    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewTop:Landroid/widget/RelativeLayout;

    .line 865
    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getTop()I

    move-result v7

    int-to-float v7, v7

    cmpg-float v6, v6, v7

    if-gez v6, :cond_7

    sget v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTouchDownX:F

    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGoToListTopButton:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLeft()I

    move-result v7

    int-to-float v7, v7

    cmpg-float v6, v6, v7

    if-gez v6, :cond_7

    .line 866
    :cond_3
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v6

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v7

    cmpl-float v6, v6, v7

    if-ltz v6, :cond_5

    .line 867
    const/high16 v6, 0x42480000    # 50.0f

    cmpl-float v6, v1, v6

    if-lez v6, :cond_4

    .line 868
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->isRunningAni()Z

    move-result v6

    if-nez v6, :cond_6

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    .line 869
    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->getDataSize()I

    move-result v6

    if-lt v6, v8, :cond_6

    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->acceptFlickAnimation()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 870
    const-string v6, ""

    const-string v7, "dte flick start L"

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 871
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->stopAnimationByFlick()V

    .line 874
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->turnLeft()V

    goto/16 :goto_0

    .line 878
    :cond_4
    const/high16 v6, -0x3db80000    # -50.0f

    cmpg-float v6, v1, v6

    if-gez v6, :cond_6

    .line 879
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->isRunningAni()Z

    move-result v6

    if-nez v6, :cond_6

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    .line 880
    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->getDataSize()I

    move-result v6

    if-lt v6, v8, :cond_6

    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->acceptFlickAnimation()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 881
    const-string v6, ""

    const-string v7, "dte flick start R"

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 882
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->stopAnimationByFlick()V

    .line 885
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->turnRight()V

    goto/16 :goto_0

    .line 891
    :cond_5
    const-string v5, ""

    const-string v6, "flick vertical"

    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 905
    .end local v1    # "dx":F
    .end local v2    # "dy":F
    :cond_6
    :goto_1
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v5

    goto/16 :goto_0

    .line 894
    .restart local v1    # "dx":F
    .restart local v2    # "dy":F
    :cond_7
    const-string v5, ""

    const-string v6, "isn\'t flick area"

    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 896
    .end local v1    # "dx":F
    .end local v2    # "dy":F
    :cond_8
    if-nez v0, :cond_6

    .line 901
    sput v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTouchDownX:F

    .line 902
    sput v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTouchDownY:F

    goto :goto_1
.end method

.method public getArrayList()V
    .locals 2

    .prologue
    .line 1218
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrWeatherInfo:Ljava/util/ArrayList;

    .line 1219
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrMoreInfo:Ljava/util/ArrayList;

    .line 1220
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    .line 1222
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrWeatherInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherCity:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1223
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrWeatherInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherDate:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1224
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrWeatherInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherTemp:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1225
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrWeatherInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherHighTemp:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1226
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrWeatherInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherLowTemp:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1227
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrWeatherInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1229
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrMoreInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRealfeelValue:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1230
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrMoreInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRealfeel:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1231
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionOneLine:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 1232
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrMoreInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionOneLine:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1233
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrMoreInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionOneLineValue:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1235
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionTwoLine:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 1236
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrMoreInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionTwoLine:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1237
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrMoreInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPrecipitaionTwoLineValue:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1240
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrMoreInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUVIndexValue:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1241
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrMoreInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUVIndex:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1242
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrMoreInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSunState:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1243
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrMoreInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTimeValue:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1246
    sget-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isLollipopOS:Z

    if-nez v0, :cond_2

    .line 1247
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mOneDayText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1248
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mOneDayDateText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1249
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mOneDayHiTemp:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1250
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mOneDayLowTemp:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1251
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mOneDayProbability:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1253
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTwoDayText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1254
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTwoDayDateText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1255
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTwoDayHiTemp:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1256
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTwoDayLowTemp:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1257
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTwoDayProbability:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1259
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mThreeDayText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1260
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mThreeDayDateText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1261
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mThreeDayHiTemp:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1262
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mThreeDayLowTemp:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1263
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mThreeDayProbability:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1265
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFourDayText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1266
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFourDayDateText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1267
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFourDayHiTemp:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1268
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFourDayLowTemp:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1269
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFourDayProbability:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1271
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFiveDayText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1272
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFiveDayDateText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1273
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFiveDayHiTemp:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1274
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFiveDayLowTemp:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1275
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFiveDayProbability:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1277
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSixDayText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1278
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSixDayDateText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1279
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSixDayHiTemp:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1280
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSixDayLowTemp:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1281
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSixDayProbability:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1283
    :cond_2
    return-void
.end method

.method public getDataRefresh(Ljava/lang/String;)V
    .locals 3
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    .line 3816
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getDataRefresh : l = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3817
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    if-nez v0, :cond_0

    .line 3818
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    .line 3820
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->startUpdateAnimation()V

    .line 3821
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->showRefreshDialog()V

    .line 3822
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->setRefreshCanceled(Z)V

    .line 3823
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1, v2, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->doManualRefresh(ILandroid/os/Handler;Ljava/lang/String;)V

    .line 3824
    return-void
.end method

.method public getMapLocationName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4127
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mapLocationName:Ljava/lang/String;

    return-object v0
.end method

.method public initWeatherClockView()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/16 v7, 0x8

    .line 1078
    const v3, 0x7f08000c

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTopLayout:Landroid/widget/RelativeLayout;

    .line 1080
    const v3, 0x7f0800a4

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMainLayout:Landroid/widget/LinearLayout;

    .line 1081
    const v3, 0x7f080081

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/VideoView;

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPView:Landroid/widget/VideoView;

    .line 1082
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPView:Landroid/widget/VideoView;

    invoke-virtual {v3, v8}, Landroid/widget/VideoView;->setStopMusic(Z)V

    .line 1083
    const v3, 0x7f080083

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCurBGView:Landroid/widget/ImageView;

    .line 1085
    const v3, 0x7f080085

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/FrameLayout;

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherEffectLayout:Landroid/widget/FrameLayout;

    .line 1086
    const v3, 0x7f080086

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherEffectView:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;

    .line 1088
    const v3, 0x7f080091

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPageIndicatorLayout:Landroid/widget/LinearLayout;

    .line 1089
    const v3, 0x7f0800af

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityTempLayout:Landroid/widget/LinearLayout;

    .line 1091
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPageindIcator:Ljava/util/ArrayList;

    .line 1092
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPageindIcator:Ljava/util/ArrayList;

    const v3, 0x7f080092

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1093
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPageindIcator:Ljava/util/ArrayList;

    const v3, 0x7f080093

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1094
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPageindIcator:Ljava/util/ArrayList;

    const v3, 0x7f080094

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1095
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPageindIcator:Ljava/util/ArrayList;

    const v3, 0x7f080095

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1096
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPageindIcator:Ljava/util/ArrayList;

    const v3, 0x7f080096

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1097
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPageindIcator:Ljava/util/ArrayList;

    const v3, 0x7f080097

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1098
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPageindIcator:Ljava/util/ArrayList;

    const v3, 0x7f080098

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1099
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPageindIcator:Ljava/util/ArrayList;

    const v3, 0x7f080099

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1100
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPageindIcator:Ljava/util/ArrayList;

    const v3, 0x7f08009a

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1101
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPageindIcator:Ljava/util/ArrayList;

    const v3, 0x7f08009b

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1103
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPageindIcator:Ljava/util/ArrayList;

    const v3, 0x7f08009c

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1104
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPageindIcator:Ljava/util/ArrayList;

    const v3, 0x7f08009d

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1105
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPageindIcator:Ljava/util/ArrayList;

    const v3, 0x7f08009e

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1106
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPageindIcator:Ljava/util/ArrayList;

    const v3, 0x7f08009f

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1107
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPageindIcator:Ljava/util/ArrayList;

    const v3, 0x7f0800a0

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1108
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPageindIcator:Ljava/util/ArrayList;

    const v3, 0x7f0800a1

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1109
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPageindIcator:Ljava/util/ArrayList;

    const v3, 0x7f0800a2

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1112
    const v3, 0x7f08008d

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewTop:Landroid/widget/RelativeLayout;

    .line 1113
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewTop:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mAccessibilityDelegateCompat:Landroid/support/v4/view/AccessibilityDelegateCompat;

    invoke-static {v3, v4}, Landroid/support/v4/view/ViewCompat;->setAccessibilityDelegate(Landroid/view/View;Landroid/support/v4/view/AccessibilityDelegateCompat;)V

    .line 1114
    const v3, 0x7f080090

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeelkyDetailViewLayout:Landroid/view/View;

    .line 1116
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPView:Landroid/widget/VideoView;

    invoke-virtual {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->setMPVideoView(Landroid/widget/VideoView;)V

    .line 1118
    iget-boolean v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->isIncludeWeeklyInfoInViewPager:Z

    if-eqz v3, :cond_2

    .line 1119
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeelkyDetailViewLayout:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1125
    :goto_0
    const v3, 0x7f08006b

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUpdateText:Landroid/widget/TextView;

    .line 1126
    const v3, 0x7f080069

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUpdateBtn:Landroid/widget/ImageView;

    .line 1127
    const v3, 0x7f08006a

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ProgressBar;

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUpdateFlipper:Landroid/widget/ProgressBar;

    .line 1129
    const v3, 0x7f0800a3

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mLogo:Landroid/widget/RelativeLayout;

    .line 1130
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1131
    .local v0, "logoDesc":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d010b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132
    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1133
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d00e3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mLogo:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1136
    const v3, 0x7f08008a

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMoreTopButton:Landroid/widget/ImageView;

    .line 1137
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMoreTopButton:Landroid/widget/ImageView;

    const v4, 0x7f0800a6

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setNextFocusUpId(I)V

    .line 1138
    const v3, 0x7f080082

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGoToListTopButton:Landroid/widget/ImageView;

    .line 1139
    const v3, 0x7f08008c

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mAddToListTopButton:Landroid/widget/ImageView;

    .line 1140
    const v3, 0x7f08008b

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mAddTopButton:Landroid/widget/ImageView;

    .line 1142
    const v3, 0x7f080089

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mInvisiblePopupMenuHolder:Landroid/view/View;

    .line 1144
    const/4 v2, 0x0

    .local v2, "menudividerid":I
    const/4 v1, 0x0

    .line 1146
    .local v1, "menudivider1id":I
    sget-boolean v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isLollipopOS:Z

    if-nez v3, :cond_0

    .line 1147
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const-string v4, "menudivider"

    const-string v5, "id"

    const-string v6, "com.sec.android.widgetapp.ap.hero.accuweather"

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 1148
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const-string v4, "menudivider1"

    const-string v5, "id"

    const-string v6, "com.sec.android.widgetapp.ap.hero.accuweather"

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 1151
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getSearchMode()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1152
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGoToListTopButton:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1153
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mAddToListTopButton:Landroid/widget/ImageView;

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1154
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMoreTopButton:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1155
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mAddTopButton:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1156
    sget-boolean v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isLollipopOS:Z

    if-nez v3, :cond_1

    .line 1157
    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1158
    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1181
    :cond_1
    :goto_1
    return-void

    .line 1121
    .end local v0    # "logoDesc":Ljava/lang/StringBuilder;
    .end local v1    # "menudivider1id":I
    .end local v2    # "menudividerid":I
    :cond_2
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeelkyDetailViewLayout:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1122
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTopLayout:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->initWeatherClockDetailView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 1161
    .restart local v0    # "logoDesc":Ljava/lang/StringBuilder;
    .restart local v1    # "menudivider1id":I
    .restart local v2    # "menudividerid":I
    :cond_3
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getWIdgetMode(Landroid/content/Context;)I

    move-result v3

    const/16 v4, 0xfa1

    if-ne v3, v4, :cond_5

    .line 1162
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGoToListTopButton:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1163
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mAddTopButton:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1164
    sget-boolean v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isLollipopOS:Z

    if-nez v3, :cond_4

    .line 1165
    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1166
    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1177
    :cond_4
    :goto_2
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mAddToListTopButton:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1178
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMoreTopButton:Landroid/widget/ImageView;

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 1169
    :cond_5
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGoToListTopButton:Landroid/widget/ImageView;

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1170
    sget-boolean v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isLollipopOS:Z

    if-nez v3, :cond_6

    .line 1171
    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1174
    :cond_6
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setAddViewControl()V

    goto :goto_2
.end method

.method public isActivityVisible()Z
    .locals 1

    .prologue
    .line 2911
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIsVisible:Z

    return v0
.end method

.method makeFormatDate()Ljava/text/SimpleDateFormat;
    .locals 2

    .prologue
    .line 2137
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getMonthAndDayFormat(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 2138
    .local v0, "dateFormatText":Ljava/lang/String;
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-direct {v1, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    return-object v1
.end method

.method makeTodayDate(Ljava/util/Calendar;)Ljava/lang/String;
    .locals 11
    .param p1, "cal"    # Ljava/util/Calendar;

    .prologue
    .line 2915
    .line 2916
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "date_format"

    invoke-static {v7, v8}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2918
    .local v1, "format":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    if-nez v7, :cond_1

    .line 2919
    :cond_0
    const-string v1, "mm-dd-yyyy"

    .line 2922
    :cond_1
    const/4 v7, 0x2

    invoke-virtual {p1, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    const/16 v8, 0x14

    invoke-static {v7, v8}, Landroid/text/format/DateUtils;->getMonthString(II)Ljava/lang/String;

    move-result-object v3

    .line 2924
    .local v3, "monthName":Ljava/lang/String;
    const/4 v7, 0x7

    invoke-virtual {p1, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    const/16 v8, 0x14

    invoke-static {v7, v8}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v5

    .line 2926
    .local v5, "weekName":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0d00da

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2927
    .local v0, "dayText":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0d00db

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 2928
    .local v6, "yearText":Ljava/lang/String;
    const/4 v4, 0x0

    .line 2929
    .local v4, "todayDate":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getBaseContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v7

    iget-object v7, v7, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v7}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    .line 2931
    .local v2, "locale":Ljava/lang/String;
    if-eqz v2, :cond_3

    .line 2932
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    .line 2933
    const-string v7, "ja"

    invoke-virtual {v2, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string v7, "ko"

    invoke-virtual {v2, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 2934
    :cond_2
    const-string v7, "dd-MM-yyyy"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 2935
    const-string v7, "%te%s %s %s%s%s"

    const/4 v8, 0x6

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object p1, v8, v9

    const/4 v9, 0x1

    aput-object v0, v8, v9

    const/4 v9, 0x2

    aput-object v3, v8, v9

    const/4 v9, 0x3

    const-string v10, "("

    aput-object v10, v8, v9

    const/4 v9, 0x4

    .line 2936
    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x5

    const-string v10, ")"

    aput-object v10, v8, v9

    .line 2935
    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2978
    :cond_3
    :goto_0
    return-object v4

    .line 2937
    :cond_4
    const-string v7, "MM-dd-yyyy"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 2938
    const-string v7, "%s %te%s %s%s%s"

    const/4 v8, 0x6

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v3, v8, v9

    const/4 v9, 0x1

    aput-object p1, v8, v9

    const/4 v9, 0x2

    aput-object v0, v8, v9

    const/4 v9, 0x3

    const-string v10, "("

    aput-object v10, v8, v9

    const/4 v9, 0x4

    .line 2939
    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x5

    const-string v10, ")"

    aput-object v10, v8, v9

    .line 2938
    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 2942
    :cond_5
    const-string v7, "%s %te%s %s%s%s"

    const/4 v8, 0x6

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v3, v8, v9

    const/4 v9, 0x1

    aput-object p1, v8, v9

    const/4 v9, 0x2

    aput-object v0, v8, v9

    const/4 v9, 0x3

    const-string v10, "("

    aput-object v10, v8, v9

    const/4 v9, 0x4

    .line 2943
    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x5

    const-string v10, ")"

    aput-object v10, v8, v9

    .line 2942
    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 2945
    :cond_6
    const-string v7, "zh"

    invoke-virtual {v2, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 2946
    const-string v7, "dd-MM-yyyy"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 2947
    const-string v7, "%s %te%s %s"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    aput-object p1, v8, v9

    const/4 v9, 0x2

    aput-object v0, v8, v9

    const/4 v9, 0x3

    aput-object v3, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 2949
    :cond_7
    const-string v7, "MM-dd-yyyy"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 2950
    const-string v7, "%s %s %te%s"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    aput-object v3, v8, v9

    const/4 v9, 0x2

    aput-object p1, v8, v9

    const/4 v9, 0x3

    aput-object v0, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 2952
    :cond_8
    const-string v7, "yyyy-MM-dd"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 2954
    const-string v7, "%s %te%s %s"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v3, v8, v9

    const/4 v9, 0x1

    aput-object p1, v8, v9

    const/4 v9, 0x2

    aput-object v0, v8, v9

    const/4 v9, 0x3

    .line 2955
    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    .line 2954
    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 2957
    :cond_9
    const-string v7, "%s %tY%s %s %te%s"

    const/4 v8, 0x6

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    aput-object p1, v8, v9

    const/4 v9, 0x2

    aput-object v6, v8, v9

    const/4 v9, 0x3

    aput-object v3, v8, v9

    const/4 v9, 0x4

    aput-object p1, v8, v9

    const/4 v9, 0x5

    aput-object v0, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 2961
    :cond_a
    const-string v7, "dd-MM-yyyy"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 2962
    const-string v7, "%s %te%s %s"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    aput-object p1, v8, v9

    const/4 v9, 0x2

    aput-object v0, v8, v9

    const/4 v9, 0x3

    aput-object v3, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 2964
    :cond_b
    const-string v7, "MM-dd-yyyy"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 2965
    const-string v7, "%s %s %te%s"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    aput-object v3, v8, v9

    const/4 v9, 0x2

    aput-object p1, v8, v9

    const/4 v9, 0x3

    aput-object v0, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 2967
    :cond_c
    const-string v7, "yyyy-MM-dd"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 2969
    const-string v7, "%s %te%s %s"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    aput-object p1, v8, v9

    const/4 v9, 0x2

    aput-object v0, v8, v9

    const/4 v9, 0x3

    aput-object v3, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 2972
    :cond_d
    const-string v7, "%s %tY%s %s %te%s"

    const/4 v8, 0x6

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    aput-object p1, v8, v9

    const/4 v9, 0x2

    aput-object v6, v8, v9

    const/4 v9, 0x3

    aput-object v3, v8, v9

    const/4 v9, 0x4

    aput-object p1, v8, v9

    const/4 v9, 0x5

    aput-object v0, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 5
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x0

    .line 4025
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 4026
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "reqcod "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4028
    const v1, 0xcb84

    if-ne p1, v1, :cond_2

    const/16 v1, 0x12c

    if-ne p2, v1, :cond_2

    .line 4029
    const-string v1, ""

    const-string v2, "ACTIVITY_FLAG_DETAIL_TO_MAP"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4031
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "flags"

    const/16 v3, -0x3e7

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->updateSearchMode(I)V

    .line 4032
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getSearchMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4033
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setSearchDBDate(Landroid/os/Bundle;)V

    .line 4036
    :cond_0
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSettingsForMain(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-result-object v0

    .line 4037
    .local v0, "settings":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getLastSelectLocation()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    .line 4038
    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getTempScaleSetting()I

    move-result v1

    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    .line 4039
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->updateCityList()V

    .line 4042
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->registerTimeReceiver()V

    .line 4044
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityList:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ge v1, v2, :cond_3

    .line 4045
    :cond_1
    const-string v1, ""

    const-string v2, "City size is null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4046
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->finish()V

    .line 4058
    .end local v0    # "settings":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    :cond_2
    :goto_0
    return-void

    .line 4049
    .restart local v0    # "settings":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    :cond_3
    iput v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->todayImgId:I

    .line 4051
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setupCubeViewPager()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 4052
    const-string v1, ""

    const-string v2, "setup Cube View"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 4054
    :cond_4
    invoke-direct {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->updateUI(Z)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 4137
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 4138
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v2, 0x1

    .line 711
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 713
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIsChangeOrientation:Z

    .line 714
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIsUpdateBGView:Z

    .line 715
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->resetTouchTimer()V

    .line 716
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->allStopEffect()V

    .line 717
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->clearHandlers()V

    .line 719
    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setActivityVisibleState(Z)V

    .line 720
    const v0, 0x7f03002a

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setContentView(I)V

    .line 722
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->initWeatherClockView()V

    .line 723
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setEventListener()V

    .line 726
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->optionMenuPopup:Landroid/widget/PopupMenu;

    if-eqz v0, :cond_0

    .line 727
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->optionMenuPopup:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->dismiss()V

    .line 728
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMoreTopButton:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->callOnClick()Z

    .line 731
    :cond_0
    const-string v0, ""

    const-string v1, "onConfigurationChanged"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 732
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getViEffectSettings(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    .line 733
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getTouchDuration()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mDefaultTouchDuration:I

    .line 735
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->updateCityList()V

    .line 736
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v0, v2, :cond_2

    .line 737
    :cond_1
    const-string v0, ""

    const-string v1, "City size is null"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 738
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->finish()V

    .line 754
    :goto_0
    return-void

    .line 746
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setupCubeViewPager()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 747
    const-string v0, ""

    const-string v1, "setup Cube View"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 751
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->resetTouchTimer()V

    .line 752
    const/16 v0, 0x3e8

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setTouchTimer(I)V

    goto :goto_0

    .line 749
    :cond_3
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->updateUI(Z)V

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x1

    .line 546
    const-string v7, ""

    const-string v8, "weather 2nd create start -->"

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 547
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 548
    iput-boolean v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIsCreateActivity:Z

    .line 550
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 551
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 553
    .local v1, "extras":Landroid/os/Bundle;
    const-string v7, "where_from_to_details"

    const/4 v8, 0x0

    invoke-virtual {v2, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    const/16 v8, 0x102

    if-ne v7, v8, :cond_0

    .line 554
    const-string v7, ""

    const-string v8, "detail Noti cancel"

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 555
    const-string v7, "notification"

    invoke-virtual {p0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/NotificationManager;

    .line 556
    .local v6, "notiManager":Landroid/app/NotificationManager;
    invoke-virtual {v6, v9}, Landroid/app/NotificationManager;->cancel(I)V

    .line 560
    .end local v6    # "notiManager":Landroid/app/NotificationManager;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090006

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v7

    iput-boolean v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUseCurrentLocation:Z

    .line 565
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getAllCityList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    .line 567
    .local v0, "cityList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-gtz v7, :cond_1

    .line 568
    const-string v7, ""

    const-string v8, "onCreate : City size is null"

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 569
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->finish()V

    .line 638
    :goto_0
    return-void

    .line 573
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 574
    const/4 v0, 0x0

    .line 576
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getWindow()Landroid/view/Window;

    move-result-object v7

    const/16 v8, 0x9

    invoke-virtual {v7, v8}, Landroid/view/Window;->requestFeature(I)Z

    .line 578
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f09000e

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v7

    iput-boolean v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->isIncludeWeeklyInfoInViewPager:Z

    .line 581
    const v7, 0x7f03002a

    invoke-virtual {p0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setContentView(I)V

    .line 583
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090009

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v7

    iput-boolean v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIsShownDst:Z

    .line 585
    iput-object p0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->detailContext:Landroid/app/Activity;

    .line 586
    invoke-virtual {p0, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setActivityVisibleState(Z)V

    .line 587
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getViEffectSettings(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    .line 589
    new-instance v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    invoke-direct {v7, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    .line 590
    new-instance v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    invoke-direct {v7, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    .line 591
    new-instance v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    invoke-direct {v7, p0, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;-><init>(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;)V

    iput-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    .line 592
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mVideoListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    invoke-virtual {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->setOnMediaPlayerListener(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;)V

    .line 594
    if-eqz v1, :cond_2

    .line 596
    const-string v7, "flags"

    const/16 v8, -0x3e7

    invoke-virtual {v1, v7, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v7

    invoke-virtual {p0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->updateSearchMode(I)V

    .line 597
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getSearchMode()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 598
    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setSearchDBDate(Landroid/os/Bundle;)V

    .line 631
    :cond_2
    :goto_1
    new-instance v7, Landroid/content/Intent;

    const-string v8, "com.sec.android.widgetapp.ap.hero.accuweather.action.START_DETAILS"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->sendBroadcast(Landroid/content/Intent;)V

    .line 633
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->initWeatherClockView()V

    .line 634
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setEventListener()V

    .line 637
    new-instance v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIconManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimationManager;

    goto/16 :goto_0

    .line 600
    :cond_3
    const-string v7, "searchlocation"

    invoke-virtual {v1, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 603
    .local v4, "location":Ljava/lang/String;
    const-string v7, "widget_mode"

    invoke-virtual {v1, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    if-eqz v7, :cond_6

    .line 604
    const-string v7, "widget_mode"

    invoke-virtual {v1, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 605
    .local v5, "mode":I
    invoke-static {p0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setWidgetMode(Landroid/content/Context;I)Z

    .line 614
    .end local v5    # "mode":I
    :cond_4
    :goto_2
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getWIdgetMode(Landroid/content/Context;)I

    move-result v7

    const/16 v8, 0xfa2

    if-ne v7, v8, :cond_7

    .line 615
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090013

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 616
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSelectedDefaultLocation(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    .line 618
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    .line 617
    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateLastSelectedLocation(Landroid/content/Context;Ljava/lang/String;)I

    .line 619
    const-string v7, ""

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onCreate : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 626
    :cond_5
    :goto_3
    const-string v7, ""

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "WMD:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getWIdgetMode(Landroid/content/Context;)I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 606
    :cond_6
    const-string v7, "NOTIFICATION_CHECK"

    invoke-virtual {v1, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 607
    new-instance v3, Landroid/content/Intent;

    const-string v7, "com.samsung.accessory.intent.action.CHECK_NOTIFICATION_ITEM"

    invoke-direct {v3, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 608
    .local v3, "intentnotiB":Landroid/content/Intent;
    const-string v7, "NOTIFICATION_PACKAGE_NAME"

    const-string v8, "com.sec.android.widgetapp.ap.hero.accuweather"

    invoke-virtual {v3, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 609
    const-string v7, "NOTIFICATION_ID"

    invoke-virtual {v3, v7, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 610
    const-string v7, ""

    const-string v8, "Noti check S"

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 611
    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_2

    .line 620
    .end local v3    # "intentnotiB":Landroid/content/Intent;
    :cond_7
    if-eqz v4, :cond_5

    .line 623
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    .line 622
    invoke-static {v7, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateLastSelectedLocation(Landroid/content/Context;Ljava/lang/String;)I

    .line 624
    const-string v7, ""

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onCreate : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2405
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 2407
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setActivityVisibleState(Z)V

    .line 2409
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->allStopEffect(Z)V

    .line 2411
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    if-eqz v0, :cond_0

    .line 2412
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->finish()V

    .line 2413
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    .line 2416
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    if-eqz v0, :cond_1

    .line 2417
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->onDestroy()V

    .line 2418
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    .line 2421
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRefreshDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_2

    .line 2422
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRefreshDialog:Landroid/app/Dialog;

    .line 2425
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPopupDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_3

    .line 2426
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPopupDialog:Landroid/app/Dialog;

    .line 2429
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mImagesNormalBG:Landroid/content/res/TypedArray;

    if-eqz v0, :cond_4

    .line 2430
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mImagesNormalBG:Landroid/content/res/TypedArray;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2431
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mImagesNormalBG:Landroid/content/res/TypedArray;

    .line 2434
    :cond_4
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mImagesFirstframe:Landroid/content/res/TypedArray;

    if-eqz v0, :cond_5

    .line 2435
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mImagesFirstframe:Landroid/content/res/TypedArray;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2436
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mImagesFirstframe:Landroid/content/res/TypedArray;

    .line 2439
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->cleanResource()V

    .line 2441
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTouchTimer:Ljava/util/Timer;

    if-eqz v0, :cond_6

    .line 2442
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTouchTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 2443
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTouchTimer:Ljava/util/Timer;

    .line 2447
    :cond_6
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    if-eqz v0, :cond_7

    .line 2448
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->clearResource()V

    .line 2449
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    .line 2452
    :cond_7
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 4283
    const/16 v0, 0x52

    if-ne p1, v0, :cond_0

    .line 4284
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getSearchMode()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mInvisiblePopupMenuHolder:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 4285
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mInvisiblePopupMenuHolder:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->displayOptionMenu(Landroid/view/View;)V

    .line 4288
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onPause()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2352
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 2353
    const-string v0, ""

    const-string v1, "onPause"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2355
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->toastHint:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    .line 2356
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->toastHint:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 2358
    :cond_0
    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setActivityVisibleState(Z)V

    .line 2359
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    if-eqz v0, :cond_1

    .line 2360
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    invoke-virtual {v0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->setIsVisibleActivity(Z)V

    .line 2362
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->unregisterTimeReceiver()V

    .line 2365
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateLastSelectedLocation(Landroid/content/Context;Ljava/lang/String;)I

    .line 2367
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2368
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.widgetapp.ap.hero.accuweather.widget.action.APPWIDGET_UPDATE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->sendBroadcast(Landroid/content/Intent;)V

    .line 2370
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIsUpdateBGView:Z

    .line 2376
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPDelayHandler:Landroid/os/Handler;

    if-nez v0, :cond_3

    .line 2377
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPDelayHandler:Landroid/os/Handler;

    .line 2380
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPDelayHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$18;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$18;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    const-wide/16 v2, 0x258

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2395
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->resetTouchTimer()V

    .line 2396
    return-void
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1468
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 1470
    const-string v1, ""

    const-string v2, "onResume"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1472
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->detailContext:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090016

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1473
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setTTSForSPenModel()V

    .line 1476
    :cond_0
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getViEffectSettings(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    .line 1477
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getTouchDuration()I

    move-result v1

    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mDefaultTouchDuration:I

    .line 1479
    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setActivityVisibleState(Z)V

    .line 1480
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    if-eqz v1, :cond_1

    .line 1481
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    invoke-virtual {v1, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->setIsVisibleActivity(Z)V

    .line 1483
    :cond_1
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getWIdgetMode(Landroid/content/Context;)I

    move-result v1

    const/16 v2, 0xfa1

    if-eq v1, v2, :cond_2

    .line 1484
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setAddViewControl()V

    .line 1486
    :cond_2
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->setContext(Landroid/content/Context;)V

    .line 1488
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSettingsForMain(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-result-object v0

    .line 1489
    .local v0, "settings":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->updateCityList()V

    .line 1490
    if-eqz v0, :cond_3

    .line 1491
    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getLastSelectLocation()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    .line 1493
    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getTempScaleSetting()I

    move-result v1

    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    .line 1495
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->registerTimeReceiver()V

    .line 1497
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityList:Ljava/util/ArrayList;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v1, v3, :cond_5

    .line 1498
    :cond_4
    const-string v1, ""

    const-string v2, "City size is null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1499
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->finish()V

    .line 1529
    :goto_0
    return-void

    .line 1502
    :cond_5
    iput v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->todayImgId:I

    .line 1503
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getBackgroundVideo()I

    move-result v1

    if-ne v1, v3, :cond_6

    .line 1504
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCurBGView:Landroid/widget/ImageView;

    if-eqz v1, :cond_6

    .line 1505
    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setBGAlpha(Z)V

    .line 1509
    :cond_6
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setupCubeViewPager()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1510
    const-string v1, ""

    const-string v2, "setup Cube View"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1514
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->checkPrecipitaionLayout()V

    .line 1516
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->resetTouchTimer()V

    .line 1517
    const/16 v1, 0x3e8

    invoke-direct {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setTouchTimer(I)V

    .line 1519
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    if-eqz v1, :cond_7

    .line 1520
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->clearResource()V

    .line 1521
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    .line 1524
    :cond_7
    iget-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIsCreateActivity:Z

    if-eqz v1, :cond_8

    .line 1525
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mEnteringRefreshAndCheckVersionHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1526
    iput-boolean v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIsCreateActivity:Z

    .line 1528
    :cond_8
    const-string v1, ""

    const-string v2, "weather 2nd resume end -->"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1512
    :cond_9
    invoke-direct {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->updateUI(Z)V

    goto :goto_1
.end method

.method public onTouchForcastInfo(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 758
    const/4 v2, 0x0

    .line 759
    .local v2, "uri":Landroid/net/Uri;
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 761
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 782
    :goto_0
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 783
    const/high16 v3, 0x24000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 784
    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->startActivity(Landroid/content/Intent;)V

    .line 791
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_1
    return-void

    .line 763
    .restart local v1    # "intent":Landroid/content/Intent;
    :sswitch_0
    const-string v3, "DETAIL_DAY"

    const/4 v4, 0x2

    invoke-virtual {p0, p0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setViewUri(Landroid/content/Context;Ljava/lang/String;I)Landroid/net/Uri;

    move-result-object v2

    .line 764
    goto :goto_0

    .line 766
    :sswitch_1
    const-string v3, "DETAIL_DAY"

    const/4 v4, 0x3

    invoke-virtual {p0, p0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setViewUri(Landroid/content/Context;Ljava/lang/String;I)Landroid/net/Uri;

    move-result-object v2

    .line 767
    goto :goto_0

    .line 769
    :sswitch_2
    const-string v3, "DETAIL_DAY"

    const/4 v4, 0x4

    invoke-virtual {p0, p0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setViewUri(Landroid/content/Context;Ljava/lang/String;I)Landroid/net/Uri;

    move-result-object v2

    .line 770
    goto :goto_0

    .line 772
    :sswitch_3
    const-string v3, "DETAIL_DAY"

    const/4 v4, 0x5

    invoke-virtual {p0, p0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setViewUri(Landroid/content/Context;Ljava/lang/String;I)Landroid/net/Uri;

    move-result-object v2

    .line 773
    goto :goto_0

    .line 775
    :sswitch_4
    const-string v3, "DETAIL_DAY"

    const/4 v4, 0x6

    invoke-virtual {p0, p0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setViewUri(Landroid/content/Context;Ljava/lang/String;I)Landroid/net/Uri;

    move-result-object v2

    .line 776
    goto :goto_0

    .line 778
    :sswitch_5
    const-string v3, "DETAIL_DAY"

    const/4 v4, 0x7

    invoke-virtual {p0, p0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setViewUri(Landroid/content/Context;Ljava/lang/String;I)Landroid/net/Uri;
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    goto :goto_0

    .line 785
    .end local v1    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 786
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v3, ""

    const-string v4, "ANFE focast"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 787
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 788
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :catch_1
    move-exception v0

    .line 789
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 761
    :sswitch_data_0
    .sparse-switch
        0x7f0800cb -> :sswitch_0
        0x7f0800d5 -> :sswitch_1
        0x7f0800df -> :sswitch_2
        0x7f0800e9 -> :sswitch_3
        0x7f0800f3 -> :sswitch_4
        0x7f0800fd -> :sswitch_5
    .end sparse-switch
.end method

.method protected onUserLeaveHint()V
    .locals 2

    .prologue
    .line 2400
    invoke-super {p0}, Landroid/app/Activity;->onUserLeaveHint()V

    .line 2401
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.widgetapp.ap.hero.accuweather.widget.action.APPWIDGET_UPDATE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->sendBroadcast(Landroid/content/Intent;)V

    .line 2402
    return-void
.end method

.method public reStartEffect()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2321
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->isActivityVisible()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2349
    :cond_0
    :goto_0
    return-void

    .line 2325
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getBackgroundVideo()I

    move-result v1

    if-ne v1, v2, :cond_2

    .line 2326
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->isPlaying()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    .line 2327
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->getMPState()I

    move-result v1

    const/4 v2, 0x5

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    .line 2328
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->getMPState()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    .line 2329
    const-string v1, ""

    const-string v2, "restart effect!!"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2330
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->start()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2345
    :catch_0
    move-exception v0

    .line 2346
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 2347
    const-string v1, ""

    const-string v2, "can\'t restart effect!!"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2334
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getWeatherEffect()I

    move-result v1

    if-ne v1, v2, :cond_3

    .line 2335
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherEffectView:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->startAnimation()V

    .line 2338
    :cond_3
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getIconAnimation()I

    move-result v1

    if-ne v1, v2, :cond_0

    .line 2339
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIconAnimationHandler:Landroid/os/Handler;

    const/16 v2, 0xdc

    .line 2340
    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 2341
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIconAnimationHandler:Landroid/os/Handler;

    const/16 v2, 0xdc

    .line 2342
    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public setActivityVisibleState(Z)V
    .locals 0
    .param p1, "onoff"    # Z

    .prologue
    .line 2907
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIsVisible:Z

    .line 2908
    return-void
.end method

.method public setBGAlpha(Z)V
    .locals 4
    .param p1, "isOn"    # Z

    .prologue
    .line 4293
    :try_start_0
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sBGA on? --> "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4294
    if-eqz p1, :cond_0

    .line 4295
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCurBGView:Landroid/widget/ImageView;

    const/16 v2, 0xff

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageAlpha(I)V

    .line 4303
    :goto_0
    return-void

    .line 4297
    :cond_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCurBGView:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageAlpha(I)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 4299
    :catch_0
    move-exception v0

    .line 4300
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 4301
    const-string v1, ""

    const-string v2, "sBA NPE"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setDateString(Ljava/util/Calendar;)Ljava/lang/String;
    .locals 5
    .param p1, "cal_b"    # Ljava/util/Calendar;

    .prologue
    const/4 v4, 0x0

    .line 2185
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 2186
    .local v1, "locale":Ljava/lang/String;
    const/4 v0, 0x0

    .line 2188
    .local v0, "dateString":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 2189
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "zh"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2190
    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->makeTodayDate(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v0

    .line 2198
    :goto_0
    return-object v0

    .line 2192
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, p1, v4, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->dateFormat(Landroid/content/Context;Ljava/util/Calendar;ZZ)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2195
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, p1, v4, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->dateFormat(Landroid/content/Context;Ljava/util/Calendar;ZZ)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public setEventListener()V
    .locals 2

    .prologue
    .line 909
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUpdateBtn:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$5;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$5;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 922
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mLogo:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$6;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$6;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 936
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGoToListTopButton:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$7;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$7;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 947
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGoToListTopButton:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$8;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$8;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 957
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mAddTopButton:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$9;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$9;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 968
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mAddTopButton:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$10;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$10;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 979
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mAddToListTopButton:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$11;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$11;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 991
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mAddToListTopButton:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$12;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$12;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1001
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMoreTopButton:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$13;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$13;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1013
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMoreTopButton:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$14;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$14;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1022
    return-void
.end method

.method public setMapLocationName(Ljava/lang/String;)V
    .locals 0
    .param p1, "mapLocationName"    # Ljava/lang/String;

    .prologue
    .line 4131
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mapLocationName:Ljava/lang/String;

    .line 4132
    return-void
.end method

.method public setPageIndicator()V
    .locals 6

    .prologue
    const v5, 0x7f0200f6

    const/4 v4, 0x0

    .line 2158
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPageindIcator:Ljava/util/ArrayList;

    if-nez v2, :cond_1

    .line 2182
    :cond_0
    :goto_0
    return-void

    .line 2162
    :cond_1
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPageindIcator:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 2163
    .local v1, "view":Landroid/view/View;
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 2167
    .end local v1    # "view":Landroid/view/View;
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getSearchMode()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2168
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPageindIcator:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2169
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPageindIcator:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 2172
    :cond_3
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 2173
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPageindIcator:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2175
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->getCurrentDataIndex()I

    move-result v2

    if-ne v0, v2, :cond_4

    .line 2176
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPageindIcator:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2172
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2178
    :cond_4
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mPageindIcator:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    const v3, 0x7f0200f5

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_3
.end method

.method public setSearchDBDate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 641
    const-string v2, "resultbygps"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->isResultByGPS:Z

    .line 642
    const-string v2, "cityInfo"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchCityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    .line 643
    const-string v2, "todayInfo"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    .line 644
    const-string v2, "forcastList"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchForcastList:Ljava/util/ArrayList;

    .line 645
    const-string v2, "hourlyweather"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->hourList:Ljava/util/ArrayList;

    .line 646
    const-string v2, "photosInfo"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->photosInfo:Ljava/util/ArrayList;

    .line 648
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchCityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    if-eqz v2, :cond_1

    .line 649
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchCityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    .line 658
    :goto_0
    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    invoke-direct {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;-><init>()V

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    .line 659
    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    invoke-direct {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;-><init>()V

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTempToday:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    .line 660
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    if-eqz v2, :cond_0

    .line 661
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTempToday:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getCurrentTemp()F

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setCurrentTemp(F)V

    .line 662
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTempToday:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getDate()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDate(Ljava/lang/String;)V

    .line 663
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTempToday:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getHighTemp()F

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setHighTemp(F)V

    .line 664
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTempToday:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getIconNum()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setIconNum(I)V

    .line 665
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTempToday:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getLowTemp()F

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setLowTemp(F)V

    .line 666
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTempToday:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTempScale()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setTempScale(I)V

    .line 667
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTempToday:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setTimeZone(Ljava/lang/String;)V

    .line 668
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTempToday:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getUpdateDate()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setUpdateDate(Ljava/lang/String;)V

    .line 669
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTempToday:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setUrl(Ljava/lang/String;)V

    .line 670
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTempToday:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getWeatherText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setWeatherText(Ljava/lang/String;)V

    .line 671
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTempToday:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setSunRiseTime(Ljava/lang/String;)V

    .line 672
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTempToday:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setSunSetTime(Ljava/lang/String;)V

    .line 673
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTempToday:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getRealFeel()F

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setRealFeel(F)V

    .line 674
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTempToday:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchCityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getSummerTime()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setSummerTime(Ljava/lang/String;)V

    .line 677
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTempToday:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getDayRainProbability()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDayRainProbability(I)V

    .line 678
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTempToday:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getDaySnowProbability()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDaySnowProbability(I)V

    .line 679
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTempToday:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getDayHailProbability()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDayHailProbability(I)V

    .line 680
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTempToday:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    .line 681
    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getDayPrecipitationProbability()I

    move-result v3

    .line 680
    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDayPrecipitationProbability(I)V

    .line 682
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTempToday:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getNightRainProbability()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightRainProbability(I)V

    .line 683
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTempToday:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getNightSnowProbability()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightSnowProbability(I)V

    .line 684
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTempToday:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getNightHailProbability()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightHailProbability(I)V

    .line 685
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTempToday:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    .line 686
    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getNightPrecipitationProbability()I

    move-result v3

    .line 685
    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightPrecipitationProbability(I)V

    .line 688
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTempToday:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getRelativeHumidity()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setRelativeHumidity(Ljava/lang/String;)V

    .line 689
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTempToday:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getUVIndex()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setUVIndex(I)V

    .line 690
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTempToday:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getUVIndexText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setUVIndexText(Ljava/lang/String;)V

    .line 693
    :cond_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTempToday:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->setTodayWeatherInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;)V

    .line 694
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchForcastList:Ljava/util/ArrayList;

    if-eqz v2, :cond_2

    .line 695
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchForcastList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 696
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchForcastList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    invoke-virtual {v3, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addForecastInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;)V

    .line 695
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 654
    .end local v1    # "i":I
    :cond_1
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getLastSelectedLocation(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    goto/16 :goto_0

    .line 700
    :cond_2
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->hourList:Ljava/util/ArrayList;

    if-eqz v2, :cond_3

    .line 701
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->hourList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;

    .line 702
    .local v0, "g":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    invoke-virtual {v3, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addHourInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;)V

    goto :goto_2

    .line 706
    .end local v0    # "g":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;
    :cond_3
    return-void
.end method

.method public setStrokeAndShadow()V
    .locals 10

    .prologue
    .line 1188
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getSystemResolution(Landroid/content/Context;)I

    move-result v1

    .line 1190
    .local v1, "resolution":I
    const/high16 v9, 0x3f800000    # 1.0f

    .line 1191
    .local v9, "strokeSize":F
    const/high16 v7, -0x1000000

    .line 1192
    .local v7, "strokeColor":I
    const/high16 v8, 0x3f000000    # 0.5f

    .line 1194
    .local v8, "strokeOffset":F
    const/high16 v2, 0x42b40000    # 90.0f

    .line 1195
    .local v2, "shadowAngle":F
    const/high16 v3, 0x3f800000    # 1.0f

    .line 1196
    .local v3, "shadowOffset":F
    const/high16 v4, 0x40800000    # 4.0f

    .line 1197
    .local v4, "shadowSoftness":F
    const/high16 v5, -0x1000000

    .line 1198
    .local v5, "shadowColor":I
    const v6, 0x3f333333    # 0.7f

    .line 1200
    .local v6, "shadowOpacity":F
    const v0, 0x1fa400

    if-ne v1, v0, :cond_1

    .line 1201
    const/high16 v3, 0x40000000    # 2.0f

    .line 1202
    const/high16 v4, 0x40a00000    # 5.0f

    .line 1211
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getArrayList()V

    .line 1212
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->clearTextEffect()V

    move-object v0, p0

    .line 1213
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setTextShadow(IFFFIF)V

    .line 1214
    invoke-virtual {p0, v1, v9, v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setTextStroke(IFIF)V

    .line 1215
    return-void

    .line 1203
    :cond_1
    const v0, 0xe1000

    if-ne v1, v0, :cond_2

    .line 1204
    const/high16 v3, 0x3f800000    # 1.0f

    goto :goto_0

    .line 1205
    :cond_2
    const v0, 0x7e900

    if-ne v1, v0, :cond_3

    .line 1206
    const/high16 v3, 0x3f800000    # 1.0f

    goto :goto_0

    .line 1207
    :cond_3
    const v0, 0x384000

    if-ne v1, v0, :cond_0

    .line 1208
    const/high16 v3, 0x40400000    # 3.0f

    .line 1209
    const/high16 v4, 0x40a00000    # 5.0f

    goto :goto_0
.end method

.method public setTextShadow(IFFFIF)V
    .locals 7
    .param p1, "resolution"    # I
    .param p2, "angle"    # F
    .param p3, "offset"    # F
    .param p4, "softness"    # F
    .param p5, "color"    # I
    .param p6, "blendingOpacity"    # F

    .prologue
    .line 1315
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrWeatherInfo:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 1316
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrWeatherInfo:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v6, v0, :cond_1

    .line 1317
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrWeatherInfo:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1318
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrWeatherInfo:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/widget/TextView;->addOuterShadowTextEffect(FFFIF)I

    .line 1316
    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 1323
    .end local v6    # "i":I
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrMoreInfo:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 1324
    const/4 v6, 0x0

    .restart local v6    # "i":I
    :goto_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrMoreInfo:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v6, v0, :cond_3

    .line 1325
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrMoreInfo:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1326
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrMoreInfo:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/widget/TextView;->addOuterShadowTextEffect(FFFIF)I

    .line 1324
    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 1330
    .end local v6    # "i":I
    :cond_3
    sget-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isLollipopOS:Z

    if-nez v0, :cond_5

    .line 1331
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    if-eqz v0, :cond_5

    .line 1332
    const/4 v6, 0x0

    .restart local v6    # "i":I
    :goto_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v6, v0, :cond_5

    .line 1333
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1334
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    invoke-virtual/range {v0 .. v5}, Landroid/widget/TextView;->addOuterShadowTextEffect(FFFIF)I

    .line 1332
    :cond_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 1339
    .end local v6    # "i":I
    :cond_5
    return-void
.end method

.method public setTextStroke(IFIF)V
    .locals 2
    .param p1, "resolution"    # I
    .param p2, "strokeSize"    # F
    .param p3, "strokeColor"    # I
    .param p4, "strokeOffset"    # F

    .prologue
    .line 1343
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrWeatherInfo:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 1344
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrWeatherInfo:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 1345
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrWeatherInfo:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1346
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrWeatherInfo:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p2, p3, p4}, Landroid/widget/TextView;->addStrokeTextEffect(FIF)I

    .line 1344
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1351
    .end local v0    # "i":I
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrMoreInfo:Ljava/util/ArrayList;

    if-eqz v1, :cond_3

    .line 1352
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrMoreInfo:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 1353
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrMoreInfo:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1354
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrMoreInfo:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p2, p3, p4}, Landroid/widget/TextView;->addStrokeTextEffect(FIF)I

    .line 1352
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1358
    .end local v0    # "i":I
    :cond_3
    sget-boolean v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isLollipopOS:Z

    if-nez v1, :cond_5

    .line 1359
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    if-eqz v1, :cond_5

    .line 1360
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 1361
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 1362
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->arrForcastInfo:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p2, p3, p4}, Landroid/widget/TextView;->addStrokeTextEffect(FIF)I

    .line 1360
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1367
    .end local v0    # "i":I
    :cond_5
    return-void
.end method

.method public setUiDeatilView(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)V
    .locals 33
    .param p1, "info"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    .prologue
    .line 1585
    const-string v3, ""

    const-string v5, "setUiDeatilView "

    invoke-static {v3, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1586
    const-string v3, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mSelLoc = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1588
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v23

    .line 1589
    .local v23, "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v5

    .line 1590
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v6

    .line 1589
    invoke-static {v3, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkDayOrNight(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v15

    .line 1594
    .local v15, "isDay":Z
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v4

    .line 1596
    .local v4, "cal":Ljava/util/Calendar;
    if-nez v4, :cond_0

    .line 1597
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    .line 1598
    const-string v3, ""

    const-string v5, " cal nul set default"

    invoke-static {v3, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1601
    :cond_0
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDate()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1603
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->makeFormatDate()Ljava/text/SimpleDateFormat;

    move-result-object v20

    .line 1605
    .local v20, "sdf":Ljava/text/SimpleDateFormat;
    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/text/SimpleDateFormat;->setCalendar(Ljava/util/Calendar;)V

    .line 1608
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setPageIndicator()V

    .line 1610
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getIconNum()I

    move-result v3

    const/16 v5, 0x103

    move-object/from16 v0, p0

    invoke-static {v0, v15, v3, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getTodayBG(Landroid/content/Context;ZII)I

    move-result v14

    .line 1612
    .local v14, "imageID":I
    const/4 v3, -0x1

    if-le v14, v3, :cond_2

    .line 1613
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCurBGView:Landroid/widget/ImageView;

    invoke-virtual {v3, v14}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1614
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getBackgroundVideo()I

    move-result v3

    const/4 v5, 0x1

    if-ne v3, v5, :cond_2

    .line 1615
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIsUpdateBGView:Z

    if-nez v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->getMPState()I

    move-result v3

    const/4 v5, 0x1

    if-eq v3, v5, :cond_7

    .line 1616
    :cond_1
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setBGAlpha(Z)V

    .line 1618
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIsUpdateBGView:Z

    .line 1627
    :cond_2
    :goto_0
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getLocation()Ljava/lang/String;

    move-result-object v3

    const-string v5, "cityId:current"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1628
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->detailContext:Landroid/app/Activity;

    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getDaemonWeatherIconNum(Landroid/content/Context;)I

    move-result v29

    .line 1629
    .local v29, "weatherIcon":I
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getIconNum()I

    move-result v3

    invoke-static {v3, v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getWeatherInfoIcon(IZ)I

    move-result v3

    move/from16 v0, v29

    if-eq v0, v3, :cond_3

    .line 1630
    const-string v3, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "weatherIcon : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v29

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1631
    const-string v3, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "convertIcon : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getIconNum()I

    move-result v6

    invoke-static {v6, v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getWeatherInfoIcon(IZ)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1632
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->detailContext:Landroid/app/Activity;

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getIconNum()I

    move-result v5

    invoke-static {v5, v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getWeatherInfoIcon(IZ)I

    move-result v5

    invoke-static {v3, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateDaemonWeatherIconNum(Landroid/content/Context;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1640
    .end local v29    # "weatherIcon":I
    :cond_3
    :goto_1
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1, v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setIsDay(Ljava/util/Calendar;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Z)V

    .line 1643
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getIconNum()I

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->startEffect(IZ)V

    .line 1646
    new-instance v19, Ljava/lang/StringBuffer;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuffer;-><init>()V

    .line 1647
    .local v19, "sb":Ljava/lang/StringBuffer;
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getLocation()Ljava/lang/String;

    move-result-object v3

    const-string v5, "cityId:current"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1648
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0d0033

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1650
    :cond_4
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getCityName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1652
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCityNameAndStateLayuout:Landroid/widget/LinearLayout;

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1654
    new-instance v19, Ljava/lang/StringBuffer;

    .end local v19    # "sb":Ljava/lang/StringBuffer;
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuffer;-><init>()V

    .line 1655
    .restart local v19    # "sb":Ljava/lang/StringBuffer;
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_8

    .line 1656
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    sput-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTz:Ljava/util/TimeZone;

    .line 1661
    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->cal_b:Ljava/util/Calendar;

    if-nez v3, :cond_5

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->cal_b:Ljava/util/Calendar;

    .line 1662
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->cal_b:Ljava/util/Calendar;

    sget-object v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTz:Ljava/util/TimeZone;

    invoke-virtual {v3, v5}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1663
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->cal_b:Ljava/util/Calendar;

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getUpdateDate()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-virtual {v3, v5, v6}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1665
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->cal_b:Ljava/util/Calendar;

    const/16 v6, 0xa

    move-object/from16 v0, p0

    invoke-static {v0, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->makeTodayDate(Landroid/content/Context;Ljava/util/Calendar;I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1667
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    .line 1668
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getCurrentTemp()F

    move-result v6

    const/16 v30, 0x0

    .line 1667
    move/from16 v0, v30

    invoke-static {v3, v5, v6, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIFZ)Ljava/lang/String;

    move-result-object v24

    .line 1669
    .local v24, "todayCurrentTemp":Ljava/lang/String;
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getHighTemp()F

    move-result v6

    const/16 v30, 0x0

    move/from16 v0, v30

    invoke-static {v3, v5, v6, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIFZ)Ljava/lang/String;

    move-result-object v25

    .line 1671
    .local v25, "todayHiTemp":Ljava/lang/String;
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getLowTemp()F

    move-result v6

    const/16 v30, 0x0

    move/from16 v0, v30

    invoke-static {v3, v5, v6, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIFZ)Ljava/lang/String;

    move-result-object v26

    .line 1673
    .local v26, "todayLowTemp":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v5

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-static {v0, v5, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getCurrentTempTTS(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1675
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v5

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    invoke-static {v0, v5, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getHighLowTempTTS(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1677
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getWeatherText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1678
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCenterCityInfo:Landroid/widget/LinearLayout;

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1680
    new-instance v19, Ljava/lang/StringBuffer;

    .end local v19    # "sb":Ljava/lang/StringBuffer;
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuffer;-><init>()V

    .line 1681
    .restart local v19    # "sb":Ljava/lang/StringBuffer;
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getRealFeel()F

    move-result v6

    const/16 v30, 0x0

    move/from16 v0, v30

    invoke-static {v3, v5, v6, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIFZ)Ljava/lang/String;

    move-result-object v27

    .line 1682
    .local v27, "todayRealFeel":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v5

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-static {v0, v5, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getRealfeelTempTTS(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1683
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-static {v0, v1, v5, v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setPrecipitaionString(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v5, 0x0

    .line 1684
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-static {v0, v1, v5, v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setPrecipitaionValue(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;Z)I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "%"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1683
    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1685
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0d00b5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getUVIndexText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1687
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v3, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v17

    .line 1688
    .local v17, "locale":Ljava/lang/String;
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkSunriseOrSunset(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v10

    .line 1690
    .local v10, "checkSun":I
    const/4 v3, 0x2

    if-ne v10, v3, :cond_9

    .line 1691
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0d00b7

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1692
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-static {v0, v5, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getSunRiseAndSunSetByFormat(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1691
    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1700
    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mBottomCityInfo:Landroid/widget/LinearLayout;

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1706
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDate()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1708
    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/text/SimpleDateFormat;->setCalendar(Ljava/util/Calendar;)V

    .line 1709
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v18

    .line 1712
    .local v18, "oneday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    const/4 v3, 0x5

    const/4 v5, 0x1

    invoke-virtual {v4, v3, v5}, Ljava/util/Calendar;->add(II)V

    .line 1714
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mOneDayText:Landroid/widget/TextView;

    const/4 v5, 0x7

    invoke-virtual {v4, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    const/16 v6, 0x14

    invoke-static {v5, v6}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1716
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mOneDayDateText:Landroid/widget/TextView;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->makeForcastDateString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1717
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mOneDayImg:Landroid/widget/ImageView;

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v5

    const/4 v6, 0x2

    const/16 v30, 0x1

    move/from16 v0, v30

    invoke-static {v5, v6, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setTodayWeatherIcon(IIZ)I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1719
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v6

    invoke-static {v3, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v7

    .line 1720
    .local v7, "hiTemp":I
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v6

    invoke-static {v3, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v8

    .line 1721
    .local v8, "lowTemp":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mOneDayHiTemp:Landroid/widget/TextView;

    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/16 v30, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    aput-object v31, v6, v30

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1722
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mOneDayLowTemp:Landroid/widget/TextView;

    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/16 v30, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    aput-object v31, v6, v30

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1724
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationProbability()I

    move-result v9

    .line 1728
    .local v9, "rainProbability":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mOneDayProbability:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "%d"

    const/16 v30, 0x1

    move/from16 v0, v30

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v30, v0

    const/16 v31, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    aput-object v32, v30, v31

    move-object/from16 v0, v30

    invoke-static {v6, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1729
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mOneDayFocastInfoLayout:Landroid/widget/LinearLayout;

    move-object/from16 v30, v0

    .line 1730
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v5

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v6

    move-object/from16 v3, p0

    .line 1729
    invoke-static/range {v3 .. v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getForcastListTTS(Landroid/content/Context;Ljava/util/Calendar;IIIII)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v30

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1733
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v28

    .line 1734
    .local v28, "twoday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    const/4 v3, 0x5

    const/4 v5, 0x1

    invoke-virtual {v4, v3, v5}, Ljava/util/Calendar;->add(II)V

    .line 1735
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTwoDayText:Landroid/widget/TextView;

    const/4 v5, 0x7

    invoke-virtual {v4, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    const/16 v6, 0x14

    invoke-static {v5, v6}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1737
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTwoDayDateText:Landroid/widget/TextView;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->makeForcastDateString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1738
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTwoDayImg:Landroid/widget/ImageView;

    invoke-virtual/range {v28 .. v28}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v5

    const/4 v6, 0x2

    const/16 v30, 0x1

    move/from16 v0, v30

    invoke-static {v5, v6, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setTodayWeatherIcon(IIZ)I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1740
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    invoke-virtual/range {v28 .. v28}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v6

    invoke-static {v3, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v7

    .line 1741
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    invoke-virtual/range {v28 .. v28}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v6

    invoke-static {v3, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v8

    .line 1742
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTwoDayHiTemp:Landroid/widget/TextView;

    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/16 v30, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    aput-object v31, v6, v30

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1743
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTwoDayLowTemp:Landroid/widget/TextView;

    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/16 v30, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    aput-object v31, v6, v30

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1745
    invoke-virtual/range {v28 .. v28}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationProbability()I

    move-result v9

    .line 1749
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTwoDayProbability:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "%d"

    const/16 v30, 0x1

    move/from16 v0, v30

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v30, v0

    const/16 v31, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    aput-object v32, v30, v31

    move-object/from16 v0, v30

    invoke-static {v6, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1750
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTwoDayFocastInfoLayout:Landroid/widget/LinearLayout;

    move-object/from16 v30, v0

    .line 1751
    invoke-virtual/range {v28 .. v28}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v5

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v6

    move-object/from16 v3, p0

    .line 1750
    invoke-static/range {v3 .. v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getForcastListTTS(Landroid/content/Context;Ljava/util/Calendar;IIIII)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v30

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1754
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v22

    .line 1755
    .local v22, "threeday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    const/4 v3, 0x5

    const/4 v5, 0x1

    invoke-virtual {v4, v3, v5}, Ljava/util/Calendar;->add(II)V

    .line 1756
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mThreeDayText:Landroid/widget/TextView;

    const/4 v5, 0x7

    invoke-virtual {v4, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    const/16 v6, 0x14

    invoke-static {v5, v6}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1758
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mThreeDayDateText:Landroid/widget/TextView;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->makeForcastDateString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1759
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mThreeDayImg:Landroid/widget/ImageView;

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v5

    const/4 v6, 0x2

    const/16 v30, 0x1

    move/from16 v0, v30

    invoke-static {v5, v6, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setTodayWeatherIcon(IIZ)I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1761
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v6

    invoke-static {v3, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v7

    .line 1762
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v6

    invoke-static {v3, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v8

    .line 1763
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mThreeDayHiTemp:Landroid/widget/TextView;

    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/16 v30, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    aput-object v31, v6, v30

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1764
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mThreeDayLowTemp:Landroid/widget/TextView;

    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/16 v30, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    aput-object v31, v6, v30

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1766
    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationProbability()I

    move-result v9

    .line 1770
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mThreeDayProbability:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "%d"

    const/16 v30, 0x1

    move/from16 v0, v30

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v30, v0

    const/16 v31, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    aput-object v32, v30, v31

    move-object/from16 v0, v30

    invoke-static {v6, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1771
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mThreeDayFocastInfoLayout:Landroid/widget/LinearLayout;

    move-object/from16 v30, v0

    .line 1772
    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v5

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v6

    move-object/from16 v3, p0

    .line 1771
    invoke-static/range {v3 .. v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getForcastListTTS(Landroid/content/Context;Ljava/util/Calendar;IIIII)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v30

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1775
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v13

    .line 1776
    .local v13, "fourday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    const/4 v3, 0x5

    const/4 v5, 0x1

    invoke-virtual {v4, v3, v5}, Ljava/util/Calendar;->add(II)V

    .line 1777
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFourDayText:Landroid/widget/TextView;

    const/4 v5, 0x7

    invoke-virtual {v4, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    const/16 v6, 0x14

    invoke-static {v5, v6}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1779
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFourDayDateText:Landroid/widget/TextView;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->makeForcastDateString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1780
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFourDayImg:Landroid/widget/ImageView;

    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v5

    const/4 v6, 0x2

    const/16 v30, 0x1

    move/from16 v0, v30

    invoke-static {v5, v6, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setTodayWeatherIcon(IIZ)I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1782
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v6

    invoke-static {v3, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v7

    .line 1783
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v6

    invoke-static {v3, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v8

    .line 1784
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFourDayHiTemp:Landroid/widget/TextView;

    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/16 v30, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    aput-object v31, v6, v30

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1785
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFourDayLowTemp:Landroid/widget/TextView;

    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/16 v30, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    aput-object v31, v6, v30

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1787
    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationProbability()I

    move-result v9

    .line 1791
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFourDayProbability:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "%d"

    const/16 v30, 0x1

    move/from16 v0, v30

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v30, v0

    const/16 v31, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    aput-object v32, v30, v31

    move-object/from16 v0, v30

    invoke-static {v6, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1792
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFourDayFocastInfoLayout:Landroid/widget/LinearLayout;

    move-object/from16 v30, v0

    .line 1793
    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v5

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v6

    move-object/from16 v3, p0

    .line 1792
    invoke-static/range {v3 .. v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getForcastListTTS(Landroid/content/Context;Ljava/util/Calendar;IIIII)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v30

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1796
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v12

    .line 1797
    .local v12, "fiveday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    const/4 v3, 0x5

    const/4 v5, 0x1

    invoke-virtual {v4, v3, v5}, Ljava/util/Calendar;->add(II)V

    .line 1798
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFiveDayText:Landroid/widget/TextView;

    const/4 v5, 0x7

    invoke-virtual {v4, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    const/16 v6, 0x14

    invoke-static {v5, v6}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1800
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFiveDayDateText:Landroid/widget/TextView;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->makeForcastDateString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1801
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFiveDayImg:Landroid/widget/ImageView;

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v5

    const/4 v6, 0x2

    const/16 v30, 0x1

    move/from16 v0, v30

    invoke-static {v5, v6, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setTodayWeatherIcon(IIZ)I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1803
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v6

    invoke-static {v3, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v7

    .line 1804
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v6

    invoke-static {v3, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v8

    .line 1805
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFiveDayHiTemp:Landroid/widget/TextView;

    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/16 v30, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    aput-object v31, v6, v30

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1806
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFiveDayLowTemp:Landroid/widget/TextView;

    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/16 v30, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    aput-object v31, v6, v30

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1808
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationProbability()I

    move-result v9

    .line 1812
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFiveDayProbability:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "%d"

    const/16 v30, 0x1

    move/from16 v0, v30

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v30, v0

    const/16 v31, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    aput-object v32, v30, v31

    move-object/from16 v0, v30

    invoke-static {v6, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1813
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFiveDayFocastInfoLayout:Landroid/widget/LinearLayout;

    move-object/from16 v30, v0

    .line 1814
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v5

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v6

    move-object/from16 v3, p0

    .line 1813
    invoke-static/range {v3 .. v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getForcastListTTS(Landroid/content/Context;Ljava/util/Calendar;IIIII)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v30

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1817
    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v21

    .line 1818
    .local v21, "sixday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    const/4 v3, 0x5

    const/4 v5, 0x1

    invoke-virtual {v4, v3, v5}, Ljava/util/Calendar;->add(II)V

    .line 1819
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSixDayText:Landroid/widget/TextView;

    const/4 v5, 0x7

    invoke-virtual {v4, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    const/16 v6, 0x14

    invoke-static {v5, v6}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1821
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSixDayDateText:Landroid/widget/TextView;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->makeForcastDateString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1822
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSixDayImg:Landroid/widget/ImageView;

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v5

    const/4 v6, 0x2

    const/16 v30, 0x1

    move/from16 v0, v30

    invoke-static {v5, v6, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setTodayWeatherIcon(IIZ)I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1824
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v6

    invoke-static {v3, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v7

    .line 1825
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v6

    invoke-static {v3, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v8

    .line 1826
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSixDayHiTemp:Landroid/widget/TextView;

    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/16 v30, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    aput-object v31, v6, v30

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1827
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSixDayLowTemp:Landroid/widget/TextView;

    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/16 v30, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    aput-object v31, v6, v30

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1829
    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationProbability()I

    move-result v9

    .line 1833
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSixDayProbability:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "%d"

    const/16 v30, 0x1

    move/from16 v0, v30

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v30, v0

    const/16 v31, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    aput-object v32, v30, v31

    move-object/from16 v0, v30

    invoke-static {v6, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1834
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSixDayFocastInfoLayout:Landroid/widget/LinearLayout;

    move-object/from16 v30, v0

    .line 1835
    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v5

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v6

    move-object/from16 v3, p0

    .line 1834
    invoke-static/range {v3 .. v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getForcastListTTS(Landroid/content/Context;Ljava/util/Calendar;IIIII)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v30

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1841
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getUpdateDate()Ljava/lang/String;

    move-result-object v3

    .line 1840
    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getDateTimeString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 1844
    .local v16, "lastUpdateString":Ljava/lang/String;
    invoke-static/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getSystemResolution(Landroid/content/Context;)I

    move-result v3

    const v5, 0x12c00

    if-eq v3, v5, :cond_6

    .line 1845
    invoke-static/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getSystemResolution(Landroid/content/Context;)I

    move-result v3

    const v5, 0x25800

    if-ne v3, v5, :cond_b

    .line 1846
    :cond_6
    invoke-static/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getOrientation(Landroid/content/Context;)I

    move-result v3

    const/4 v5, 0x1

    if-ne v3, v5, :cond_b

    .line 1847
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUpdateText:Landroid/widget/TextView;

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1855
    :goto_4
    return-void

    .line 1620
    .end local v7    # "hiTemp":I
    .end local v8    # "lowTemp":I
    .end local v9    # "rainProbability":I
    .end local v10    # "checkSun":I
    .end local v12    # "fiveday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .end local v13    # "fourday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .end local v16    # "lastUpdateString":Ljava/lang/String;
    .end local v17    # "locale":Ljava/lang/String;
    .end local v18    # "oneday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .end local v19    # "sb":Ljava/lang/StringBuffer;
    .end local v21    # "sixday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .end local v22    # "threeday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .end local v24    # "todayCurrentTemp":Ljava/lang/String;
    .end local v25    # "todayHiTemp":Ljava/lang/String;
    .end local v26    # "todayLowTemp":Ljava/lang/String;
    .end local v27    # "todayRealFeel":Ljava/lang/String;
    .end local v28    # "twoday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->getMPState()I

    move-result v3

    const/4 v5, 0x1

    if-ne v3, v5, :cond_2

    .line 1621
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->playBGFadeIn()V

    goto/16 :goto_0

    .line 1635
    :catch_0
    move-exception v11

    .line 1636
    .local v11, "e":Ljava/lang/Exception;
    const-string v3, ""

    const-string v5, "CI exception"

    invoke-static {v3, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 1658
    .end local v11    # "e":Ljava/lang/Exception;
    .restart local v19    # "sb":Ljava/lang/StringBuffer;
    :cond_8
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v3

    sput-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTz:Ljava/util/TimeZone;

    goto/16 :goto_2

    .line 1693
    .restart local v10    # "checkSun":I
    .restart local v17    # "locale":Ljava/lang/String;
    .restart local v24    # "todayCurrentTemp":Ljava/lang/String;
    .restart local v25    # "todayHiTemp":Ljava/lang/String;
    .restart local v26    # "todayLowTemp":Ljava/lang/String;
    .restart local v27    # "todayRealFeel":Ljava/lang/String;
    :cond_9
    const/4 v3, 0x3

    if-ne v10, v3, :cond_a

    .line 1694
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0d00b6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1695
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-static {v0, v5, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getSunRiseAndSunSetByFormat(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1694
    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_3

    .line 1697
    :cond_a
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0d00b6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1698
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-static {v0, v5, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getSunRiseAndSunSetByFormat(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1697
    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_3

    .line 1850
    .restart local v7    # "hiTemp":I
    .restart local v8    # "lowTemp":I
    .restart local v9    # "rainProbability":I
    .restart local v12    # "fiveday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .restart local v13    # "fourday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .restart local v16    # "lastUpdateString":Ljava/lang/String;
    .restart local v18    # "oneday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .restart local v21    # "sixday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .restart local v22    # "threeday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .restart local v28    # "twoday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUpdateText:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f0d003b

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1852
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUpdateText:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f0d003b

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_4
.end method

.method public setUiSearchDeatilView()V
    .locals 31

    .prologue
    .line 1858
    const-string v3, ""

    const-string v5, "setUiWithDatabaseData "

    invoke-static {v3, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1859
    const-string v3, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mSelLoc = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1861
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    .line 1862
    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v6

    .line 1861
    invoke-static {v3, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkDayOrNight(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v15

    .line 1864
    .local v15, "isDay":Z
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v3

    .line 1865
    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1864
    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v4

    .line 1867
    .local v4, "cal":Ljava/util/Calendar;
    if-nez v4, :cond_0

    .line 1868
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    .line 1869
    const-string v3, ""

    const-string v5, " cal nul set default"

    invoke-static {v3, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1871
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getDate()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1873
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->makeFormatDate()Ljava/text/SimpleDateFormat;

    move-result-object v20

    .line 1875
    .local v20, "sdf":Ljava/text/SimpleDateFormat;
    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/text/SimpleDateFormat;->setCalendar(Ljava/util/Calendar;)V

    .line 1878
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setPageIndicator()V

    .line 1880
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getIconNum()I

    move-result v3

    const/16 v5, 0x103

    move-object/from16 v0, p0

    invoke-static {v0, v15, v3, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getTodayBG(Landroid/content/Context;ZII)I

    move-result v14

    .line 1882
    .local v14, "imageID":I
    const/4 v3, -0x1

    if-le v14, v3, :cond_1

    .line 1888
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCurBGView:Landroid/widget/ImageView;

    invoke-virtual {v3, v14}, Landroid/widget/ImageView;->setImageResource(I)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1895
    :cond_1
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v3, v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setIsDay(Ljava/util/Calendar;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Z)V

    .line 1898
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getIconNum()I

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->startEffect(IZ)V

    .line 1901
    new-instance v19, Ljava/lang/StringBuffer;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuffer;-><init>()V

    .line 1902
    .local v19, "sb":Ljava/lang/StringBuffer;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchCityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v3

    const-string v5, "cityId:current"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1903
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0d0033

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1905
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchCityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getCity()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1908
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 1909
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    sput-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTz:Ljava/util/TimeZone;

    .line 1914
    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->cal_b:Ljava/util/Calendar;

    if-nez v3, :cond_3

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->cal_b:Ljava/util/Calendar;

    .line 1915
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->cal_b:Ljava/util/Calendar;

    sget-object v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTz:Ljava/util/TimeZone;

    invoke-virtual {v3, v5}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1916
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->cal_b:Ljava/util/Calendar;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getUpdateDate()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-virtual {v3, v5, v6}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1918
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->cal_b:Ljava/util/Calendar;

    const/16 v6, 0xa

    move-object/from16 v0, p0

    invoke-static {v0, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->makeTodayDate(Landroid/content/Context;Ljava/util/Calendar;I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1920
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTempScale()I

    move-result v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    .line 1921
    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getCurrentTemp()F

    move-result v6

    const/16 v28, 0x0

    .line 1920
    move/from16 v0, v28

    invoke-static {v3, v5, v6, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIFZ)Ljava/lang/String;

    move-result-object v23

    .line 1922
    .local v23, "todayCurrentTemp":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTempScale()I

    move-result v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    .line 1923
    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getHighTemp()F

    move-result v6

    const/16 v28, 0x0

    .line 1922
    move/from16 v0, v28

    invoke-static {v3, v5, v6, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIFZ)Ljava/lang/String;

    move-result-object v24

    .line 1924
    .local v24, "todayHiTemp":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTempScale()I

    move-result v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    .line 1925
    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getLowTemp()F

    move-result v6

    const/16 v28, 0x0

    .line 1924
    move/from16 v0, v28

    invoke-static {v3, v5, v6, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIFZ)Ljava/lang/String;

    move-result-object v25

    .line 1926
    .local v25, "todayLowTemp":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTempScale()I

    move-result v5

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-static {v0, v5, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getCurrentTempTTS(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1928
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTempScale()I

    move-result v5

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    invoke-static {v0, v5, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getHighLowTempTTS(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1930
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getWeatherText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1932
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTempScale()I

    move-result v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getRealFeel()F

    move-result v6

    const/16 v28, 0x0

    move/from16 v0, v28

    invoke-static {v3, v5, v6, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIFZ)Ljava/lang/String;

    move-result-object v26

    .line 1933
    .local v26, "todayRealFeel":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTempScale()I

    move-result v5

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-static {v0, v5, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getRealfeelTempTTS(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1934
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    move-object/from16 v0, p0

    invoke-static {v0, v5, v6, v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setPrecipitaionString(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    .line 1935
    move-object/from16 v0, p0

    invoke-static {v0, v5, v6, v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setPrecipitaionValue(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;Z)I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "%"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1934
    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1936
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0d00b5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getUVIndexText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1938
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v3, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v17

    .line 1939
    .local v17, "locale":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkSunriseOrSunset(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v10

    .line 1941
    .local v10, "checkSun":I
    const/4 v3, 0x2

    if-ne v10, v3, :cond_6

    .line 1942
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0d00b7

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    .line 1943
    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-static {v0, v5, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getSunRiseAndSunSetByFormat(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1942
    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1951
    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewTop:Landroid/widget/RelativeLayout;

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1956
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getDate()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1958
    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/text/SimpleDateFormat;->setCalendar(Ljava/util/Calendar;)V

    .line 1959
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v18

    .line 1962
    .local v18, "oneday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    const/4 v3, 0x5

    const/4 v5, 0x1

    invoke-virtual {v4, v3, v5}, Ljava/util/Calendar;->add(II)V

    .line 1964
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mOneDayText:Landroid/widget/TextView;

    const/4 v5, 0x7

    invoke-virtual {v4, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    const/16 v6, 0x14

    invoke-static {v5, v6}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1966
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mOneDayDateText:Landroid/widget/TextView;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->makeForcastDateString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1967
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mOneDayImg:Landroid/widget/ImageView;

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v5

    const/4 v6, 0x2

    const/16 v28, 0x1

    move/from16 v0, v28

    invoke-static {v5, v6, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setTodayWeatherIcon(IIZ)I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1969
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTempScale()I

    move-result v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v6

    invoke-static {v3, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v7

    .line 1970
    .local v7, "hiTemp":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTempScale()I

    move-result v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v6

    invoke-static {v3, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v8

    .line 1971
    .local v8, "lowTemp":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mOneDayHiTemp:Landroid/widget/TextView;

    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/16 v28, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v29

    aput-object v29, v6, v28

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1972
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mOneDayLowTemp:Landroid/widget/TextView;

    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/16 v28, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v29

    aput-object v29, v6, v28

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1974
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationProbability()I

    move-result v9

    .line 1978
    .local v9, "rainProbability":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mOneDayProbability:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "%d"

    const/16 v28, 0x1

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v30

    aput-object v30, v28, v29

    move-object/from16 v0, v28

    invoke-static {v6, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1979
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mOneDayFocastInfoLayout:Landroid/widget/LinearLayout;

    move-object/from16 v28, v0

    .line 1980
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    .line 1981
    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTempScale()I

    move-result v6

    move-object/from16 v3, p0

    .line 1979
    invoke-static/range {v3 .. v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getForcastListTTS(Landroid/content/Context;Ljava/util/Calendar;IIIII)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1983
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v27

    .line 1984
    .local v27, "twoday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    const/4 v3, 0x5

    const/4 v5, 0x1

    invoke-virtual {v4, v3, v5}, Ljava/util/Calendar;->add(II)V

    .line 1985
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTwoDayText:Landroid/widget/TextView;

    const/4 v5, 0x7

    invoke-virtual {v4, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    const/16 v6, 0x14

    invoke-static {v5, v6}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1987
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTwoDayDateText:Landroid/widget/TextView;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->makeForcastDateString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1988
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTwoDayImg:Landroid/widget/ImageView;

    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v5

    const/4 v6, 0x2

    const/16 v28, 0x1

    move/from16 v0, v28

    invoke-static {v5, v6, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setTodayWeatherIcon(IIZ)I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1990
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTempScale()I

    move-result v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v6

    invoke-static {v3, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v7

    .line 1991
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTempScale()I

    move-result v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v6

    invoke-static {v3, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v8

    .line 1992
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTwoDayHiTemp:Landroid/widget/TextView;

    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/16 v28, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v29

    aput-object v29, v6, v28

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1993
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTwoDayLowTemp:Landroid/widget/TextView;

    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/16 v28, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v29

    aput-object v29, v6, v28

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1995
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationProbability()I

    move-result v9

    .line 1999
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTwoDayProbability:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "%d"

    const/16 v28, 0x1

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v30

    aput-object v30, v28, v29

    move-object/from16 v0, v28

    invoke-static {v6, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2000
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTwoDayFocastInfoLayout:Landroid/widget/LinearLayout;

    move-object/from16 v28, v0

    .line 2001
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    .line 2002
    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTempScale()I

    move-result v6

    move-object/from16 v3, p0

    .line 2000
    invoke-static/range {v3 .. v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getForcastListTTS(Landroid/content/Context;Ljava/util/Calendar;IIIII)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2004
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    const/4 v5, 0x2

    invoke-virtual {v3, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v22

    .line 2005
    .local v22, "threeday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    const/4 v3, 0x5

    const/4 v5, 0x1

    invoke-virtual {v4, v3, v5}, Ljava/util/Calendar;->add(II)V

    .line 2006
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mThreeDayText:Landroid/widget/TextView;

    const/4 v5, 0x7

    invoke-virtual {v4, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    const/16 v6, 0x14

    invoke-static {v5, v6}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2008
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mThreeDayDateText:Landroid/widget/TextView;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->makeForcastDateString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2009
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mThreeDayImg:Landroid/widget/ImageView;

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v5

    const/4 v6, 0x2

    const/16 v28, 0x1

    move/from16 v0, v28

    invoke-static {v5, v6, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setTodayWeatherIcon(IIZ)I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2011
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTempScale()I

    move-result v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v6

    invoke-static {v3, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v7

    .line 2012
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTempScale()I

    move-result v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v6

    invoke-static {v3, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v8

    .line 2013
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mThreeDayHiTemp:Landroid/widget/TextView;

    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/16 v28, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v29

    aput-object v29, v6, v28

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2014
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mThreeDayLowTemp:Landroid/widget/TextView;

    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/16 v28, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v29

    aput-object v29, v6, v28

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2016
    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationProbability()I

    move-result v9

    .line 2020
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mThreeDayProbability:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "%d"

    const/16 v28, 0x1

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v30

    aput-object v30, v28, v29

    move-object/from16 v0, v28

    invoke-static {v6, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2021
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mThreeDayFocastInfoLayout:Landroid/widget/LinearLayout;

    move-object/from16 v28, v0

    .line 2022
    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    .line 2023
    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTempScale()I

    move-result v6

    move-object/from16 v3, p0

    .line 2021
    invoke-static/range {v3 .. v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getForcastListTTS(Landroid/content/Context;Ljava/util/Calendar;IIIII)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2025
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    const/4 v5, 0x3

    invoke-virtual {v3, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v13

    .line 2026
    .local v13, "fourday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    const/4 v3, 0x5

    const/4 v5, 0x1

    invoke-virtual {v4, v3, v5}, Ljava/util/Calendar;->add(II)V

    .line 2027
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFourDayText:Landroid/widget/TextView;

    const/4 v5, 0x7

    invoke-virtual {v4, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    const/16 v6, 0x14

    invoke-static {v5, v6}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2029
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFourDayDateText:Landroid/widget/TextView;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->makeForcastDateString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2030
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFourDayImg:Landroid/widget/ImageView;

    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v5

    const/4 v6, 0x2

    const/16 v28, 0x1

    move/from16 v0, v28

    invoke-static {v5, v6, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setTodayWeatherIcon(IIZ)I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2032
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTempScale()I

    move-result v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v6

    invoke-static {v3, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v7

    .line 2033
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTempScale()I

    move-result v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v6

    invoke-static {v3, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v8

    .line 2034
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFourDayHiTemp:Landroid/widget/TextView;

    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/16 v28, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v29

    aput-object v29, v6, v28

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2035
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFourDayLowTemp:Landroid/widget/TextView;

    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/16 v28, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v29

    aput-object v29, v6, v28

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2037
    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationProbability()I

    move-result v9

    .line 2041
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFourDayProbability:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "%d"

    const/16 v28, 0x1

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v30

    aput-object v30, v28, v29

    move-object/from16 v0, v28

    invoke-static {v6, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2042
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFourDayFocastInfoLayout:Landroid/widget/LinearLayout;

    move-object/from16 v28, v0

    .line 2043
    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    .line 2044
    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTempScale()I

    move-result v6

    move-object/from16 v3, p0

    .line 2042
    invoke-static/range {v3 .. v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getForcastListTTS(Landroid/content/Context;Ljava/util/Calendar;IIIII)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2046
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    const/4 v5, 0x4

    invoke-virtual {v3, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v12

    .line 2047
    .local v12, "fiveday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    const/4 v3, 0x5

    const/4 v5, 0x1

    invoke-virtual {v4, v3, v5}, Ljava/util/Calendar;->add(II)V

    .line 2048
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFiveDayText:Landroid/widget/TextView;

    const/4 v5, 0x7

    invoke-virtual {v4, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    const/16 v6, 0x14

    invoke-static {v5, v6}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2050
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFiveDayDateText:Landroid/widget/TextView;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->makeForcastDateString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2051
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFiveDayImg:Landroid/widget/ImageView;

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v5

    const/4 v6, 0x2

    const/16 v28, 0x1

    move/from16 v0, v28

    invoke-static {v5, v6, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setTodayWeatherIcon(IIZ)I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2053
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTempScale()I

    move-result v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v6

    invoke-static {v3, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v7

    .line 2054
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTempScale()I

    move-result v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v6

    invoke-static {v3, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v8

    .line 2055
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFiveDayHiTemp:Landroid/widget/TextView;

    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/16 v28, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v29

    aput-object v29, v6, v28

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2056
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFiveDayLowTemp:Landroid/widget/TextView;

    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/16 v28, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v29

    aput-object v29, v6, v28

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2058
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationProbability()I

    move-result v9

    .line 2062
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFiveDayProbability:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "%d"

    const/16 v28, 0x1

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v30

    aput-object v30, v28, v29

    move-object/from16 v0, v28

    invoke-static {v6, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2063
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mFiveDayFocastInfoLayout:Landroid/widget/LinearLayout;

    move-object/from16 v28, v0

    .line 2064
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    .line 2065
    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTempScale()I

    move-result v6

    move-object/from16 v3, p0

    .line 2063
    invoke-static/range {v3 .. v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getForcastListTTS(Landroid/content/Context;Ljava/util/Calendar;IIIII)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2067
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    const/4 v5, 0x5

    invoke-virtual {v3, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v21

    .line 2068
    .local v21, "sixday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    const/4 v3, 0x5

    const/4 v5, 0x1

    invoke-virtual {v4, v3, v5}, Ljava/util/Calendar;->add(II)V

    .line 2069
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSixDayText:Landroid/widget/TextView;

    const/4 v5, 0x7

    invoke-virtual {v4, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    const/16 v6, 0x14

    invoke-static {v5, v6}, Landroid/text/format/DateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2071
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSixDayDateText:Landroid/widget/TextView;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->makeForcastDateString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2072
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSixDayImg:Landroid/widget/ImageView;

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v5

    const/4 v6, 0x2

    const/16 v28, 0x1

    move/from16 v0, v28

    invoke-static {v5, v6, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setTodayWeatherIcon(IIZ)I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2074
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTempScale()I

    move-result v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v6

    invoke-static {v3, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v7

    .line 2075
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTempScale()I

    move-result v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v6

    invoke-static {v3, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v8

    .line 2076
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSixDayHiTemp:Landroid/widget/TextView;

    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/16 v28, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v29

    aput-object v29, v6, v28

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2077
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSixDayLowTemp:Landroid/widget/TextView;

    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/16 v28, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v29

    aput-object v29, v6, v28

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2079
    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationProbability()I

    move-result v9

    .line 2083
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSixDayProbability:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "%d"

    const/16 v28, 0x1

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v30

    aput-object v30, v28, v29

    move-object/from16 v0, v28

    invoke-static {v6, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2084
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSixDayFocastInfoLayout:Landroid/widget/LinearLayout;

    move-object/from16 v28, v0

    .line 2085
    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    .line 2086
    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getTempScale()I

    move-result v6

    move-object/from16 v3, p0

    .line 2084
    invoke-static/range {v3 .. v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getForcastListTTS(Landroid/content/Context;Ljava/util/Calendar;IIIII)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2090
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    .line 2091
    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getUpdateDate()Ljava/lang/String;

    move-result-object v3

    .line 2090
    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getDateTimeString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 2094
    .local v16, "lastUpdateString":Ljava/lang/String;
    invoke-static/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getSystemResolution(Landroid/content/Context;)I

    move-result v3

    const v5, 0x12c00

    if-eq v3, v5, :cond_4

    .line 2095
    invoke-static/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getSystemResolution(Landroid/content/Context;)I

    move-result v3

    const v5, 0x25800

    if-ne v3, v5, :cond_8

    .line 2096
    :cond_4
    invoke-static/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getOrientation(Landroid/content/Context;)I

    move-result v3

    const/4 v5, 0x1

    if-ne v3, v5, :cond_8

    .line 2097
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUpdateText:Landroid/widget/TextView;

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2105
    :goto_3
    return-void

    .line 1889
    .end local v7    # "hiTemp":I
    .end local v8    # "lowTemp":I
    .end local v9    # "rainProbability":I
    .end local v10    # "checkSun":I
    .end local v12    # "fiveday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .end local v13    # "fourday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .end local v16    # "lastUpdateString":Ljava/lang/String;
    .end local v17    # "locale":Ljava/lang/String;
    .end local v18    # "oneday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .end local v19    # "sb":Ljava/lang/StringBuffer;
    .end local v21    # "sixday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .end local v22    # "threeday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .end local v23    # "todayCurrentTemp":Ljava/lang/String;
    .end local v24    # "todayHiTemp":Ljava/lang/String;
    .end local v25    # "todayLowTemp":Ljava/lang/String;
    .end local v26    # "todayRealFeel":Ljava/lang/String;
    .end local v27    # "twoday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    :catch_0
    move-exception v11

    .line 1890
    .local v11, "e":Ljava/lang/OutOfMemoryError;
    const-string v3, ""

    const-string v5, "sUSDV OOME"

    invoke-static {v3, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1911
    .end local v11    # "e":Ljava/lang/OutOfMemoryError;
    .restart local v19    # "sb":Ljava/lang/StringBuffer;
    :cond_5
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v3

    sput-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mTz:Ljava/util/TimeZone;

    goto/16 :goto_1

    .line 1944
    .restart local v10    # "checkSun":I
    .restart local v17    # "locale":Ljava/lang/String;
    .restart local v23    # "todayCurrentTemp":Ljava/lang/String;
    .restart local v24    # "todayHiTemp":Ljava/lang/String;
    .restart local v25    # "todayLowTemp":Ljava/lang/String;
    .restart local v26    # "todayRealFeel":Ljava/lang/String;
    :cond_6
    const/4 v3, 0x3

    if-ne v10, v3, :cond_7

    .line 1945
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0d00b6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    .line 1946
    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-static {v0, v5, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getSunRiseAndSunSetByFormat(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1945
    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_2

    .line 1948
    :cond_7
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0d00b6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    .line 1949
    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-static {v0, v5, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getSunRiseAndSunSetByFormat(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1948
    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_2

    .line 2100
    .restart local v7    # "hiTemp":I
    .restart local v8    # "lowTemp":I
    .restart local v9    # "rainProbability":I
    .restart local v12    # "fiveday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .restart local v13    # "fourday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .restart local v16    # "lastUpdateString":Ljava/lang/String;
    .restart local v18    # "oneday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .restart local v21    # "sixday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .restart local v22    # "threeday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .restart local v27    # "twoday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUpdateText:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f0d003b

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2102
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mUpdateText:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v6, 0x7f0d003b

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_3
.end method

.method setViewUri(Landroid/content/Context;Ljava/lang/String;I)Landroid/net/Uri;
    .locals 14
    .param p1, "Ctx"    # Landroid/content/Context;
    .param p2, "postion"    # Ljava/lang/String;
    .param p3, "index"    # I

    .prologue
    .line 4061
    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuweatherUiUtil;

    invoke-direct {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuweatherUiUtil;-><init>()V

    .line 4062
    .local v5, "mUiUtil":Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuweatherUiUtil;
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    .line 4063
    .local v8, "selLocationTemp":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->selLocation:Ljava/lang/String;

    .line 4065
    .local v6, "realLocaionTemp":Ljava/lang/String;
    const/4 v9, 0x0

    .line 4067
    .local v9, "uri":Landroid/net/Uri;
    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    if-nez v11, :cond_0

    .line 4068
    new-instance v11, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    invoke-direct {v11, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;-><init>(Landroid/content/Context;)V

    iput-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    .line 4071
    :cond_0
    const-string v11, "cityId:current"

    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 4072
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getRealLocation(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    .line 4073
    .local v7, "realLocation":Ljava/lang/String;
    if-eqz v7, :cond_1

    const-string v11, ""

    invoke-virtual {v7, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_1

    .line 4074
    move-object v6, v7

    .line 4078
    .end local v7    # "realLocation":Ljava/lang/String;
    :cond_1
    const-string v11, "DETAIL_HOME"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    if-nez p3, :cond_9

    .line 4079
    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    invoke-virtual {v5, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuweatherUiUtil;->getLanguageString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v12

    iget v13, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    invoke-virtual {v11, v12, v6, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeQuickViewUri(Ljava/lang/String;Ljava/lang/String;I)Landroid/net/Uri;

    move-result-object v9

    .line 4115
    :cond_2
    :goto_0
    const-string v11, "DETAIL_HOME"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    if-eqz p3, :cond_5

    :cond_3
    const-string v11, "DETAIL_MORE"

    .line 4116
    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    if-eqz p3, :cond_5

    :cond_4
    const-string v11, "DETAIL_DAY"

    .line 4117
    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 4118
    :cond_5
    const-string v11, "DETAIL_MORE"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_6

    const-string v11, "DETAIL_DAY"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 4119
    :cond_6
    iget v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->tempscale:I

    invoke-static {v9, v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->appendUnitCodeAt(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v9

    .line 4121
    :cond_7
    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->appendPartnerCodeAt(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v9

    .line 4123
    :cond_8
    return-object v9

    .line 4080
    :cond_9
    const-string v11, "DETAIL_MORE"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_b

    if-nez p3, :cond_b

    .line 4083
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getSearchMode()Z

    move-result v11

    if-eqz v11, :cond_a

    .line 4084
    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchTodayInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    goto :goto_0

    .line 4086
    :cond_a
    move/from16 v0, p3

    invoke-static {p1, v0, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getUri(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    goto :goto_0

    .line 4088
    :cond_b
    const-string v11, "DETAIL_DAY"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_d

    .line 4092
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getSearchMode()Z

    move-result v11

    if-eqz v11, :cond_c

    .line 4093
    iget-object v11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchForcastList:Ljava/util/ArrayList;

    add-int/lit8 v12, p3, -0x2

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    goto/16 :goto_0

    .line 4095
    :cond_c
    move/from16 v0, p3

    invoke-static {p1, v0, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getUri(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    goto/16 :goto_0

    .line 4097
    :cond_d
    const-string v11, "REPORT_WRONG_CITY"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 4098
    const-string v1, "https://feedback.accuweather.com/?language=%s&device=samsung_%s&version=widget_%s"

    .line 4100
    .local v1, "FEEDBACK_HOME_URL":Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkLanguage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 4101
    .local v4, "language":Ljava/lang/String;
    sget-object v11, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->replaceModelName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4102
    .local v2, "deviceId":Ljava/lang/String;
    const/4 v10, 0x0

    .line 4104
    .local v10, "versionName":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v11

    const-string v12, "com.sec.android.widgetapp.ap.hero.accuweather"

    const/16 v13, 0x80

    invoke-virtual {v11, v12, v13}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v11

    iget-object v10, v11, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4111
    :goto_1
    const-string v11, "https://feedback.accuweather.com/?language=%s&device=samsung_%s&version=widget_%s"

    const/4 v12, 0x3

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v4, v12, v13

    const/4 v13, 0x1

    aput-object v2, v12, v13

    const/4 v13, 0x2

    aput-object v10, v12, v13

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    .line 4112
    const-string v11, ""

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "R W C uri = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 4106
    :catch_0
    move-exception v3

    .line 4108
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v3}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1
.end method

.method public startEffect(IZ)V
    .locals 6
    .param p1, "iconNum"    # I
    .param p2, "isDay"    # Z

    .prologue
    const/4 v5, 0x1

    .line 2247
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->isActivityVisible()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2318
    :cond_0
    :goto_0
    return-void

    .line 2255
    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->setSetting(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;)V

    .line 2256
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getBackgroundVideo()I

    move-result v2

    if-nez v2, :cond_6

    .line 2257
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPView:Landroid/widget/VideoView;

    invoke-virtual {v2}, Landroid/widget/VideoView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_2

    .line 2258
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPView:Landroid/widget/VideoView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/VideoView;->setVisibility(I)V

    .line 2267
    :cond_2
    :goto_1
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "prepare Effect!! mp Playing = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->isPlaying()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mp state = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    .line 2268
    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->getMPState()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2267
    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2269
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRefreshDialog:Landroid/app/Dialog;

    if-eqz v2, :cond_3

    .line 2270
    const-string v2, ""

    const-string v3, "dsp ing~ rfdl dnt mp"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2272
    :cond_3
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRefreshDialog:Landroid/app/Dialog;

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->isPlaying()Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    .line 2273
    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->getMPState()I

    move-result v2

    const/4 v3, 0x5

    if-eq v2, v3, :cond_4

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    .line 2274
    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->getMPState()I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_4

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    .line 2275
    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->getMPState()I

    move-result v2

    if-eq v2, v5, :cond_4

    .line 2276
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->start()V

    .line 2281
    :cond_4
    const-string v0, "none"

    .line 2282
    .local v0, "WeatherEffectType":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getWeatherEffect()I

    move-result v2

    if-ne v2, v5, :cond_9

    .line 2283
    invoke-static {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/AnimUtils;->getWeatherEffectType(I)Ljava/lang/String;

    move-result-object v0

    .line 2285
    const-string v2, "none"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2286
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherEffectLayout:Landroid/widget/FrameLayout;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2287
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherEffectView:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;

    const-string v3, "none"

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->setWeatherType(Ljava/lang/String;)V

    .line 2304
    :cond_5
    :goto_2
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getIconAnimation()I

    move-result v2

    if-ne v2, v5, :cond_0

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    .line 2305
    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getBackgroundVideo()I

    move-result v2

    if-nez v2, :cond_0

    .line 2306
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRefreshDialog:Landroid/app/Dialog;

    if-nez v2, :cond_0

    .line 2307
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIconAnimationHandler:Landroid/os/Handler;

    const/16 v3, 0xdc

    .line 2308
    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 2309
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIconAnimationHandler:Landroid/os/Handler;

    const/16 v3, 0xdc

    .line 2310
    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 2314
    .end local v0    # "WeatherEffectType":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 2315
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 2316
    const-string v2, ""

    const-string v3, "can\'t start effect!!"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2261
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :cond_6
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPView:Landroid/widget/VideoView;

    invoke-virtual {v2}, Landroid/widget/VideoView;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_7

    .line 2262
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPView:Landroid/widget/VideoView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/VideoView;->setVisibility(I)V

    .line 2264
    :cond_7
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    invoke-virtual {v2, p1, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->setUri(IZ)V

    goto/16 :goto_1

    .line 2289
    .restart local v0    # "WeatherEffectType":Ljava/lang/String;
    :cond_8
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherEffectLayout:Landroid/widget/FrameLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2290
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherEffectView:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;

    invoke-virtual {v2, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->setWeatherType(Ljava/lang/String;)V

    .line 2291
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherEffectView:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;

    invoke-virtual {v2, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->setObject(Ljava/lang/String;)V

    .line 2292
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getBackgroundVideo()I

    move-result v2

    if-nez v2, :cond_5

    .line 2293
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRefreshDialog:Landroid/app/Dialog;

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherEffectView:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->isRunningAni()Z

    move-result v2

    if-nez v2, :cond_5

    .line 2294
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherEffectView:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->startAnimation()V

    goto :goto_2

    .line 2299
    :cond_9
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mWeatherEffectLayout:Landroid/widget/FrameLayout;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public updateSearchMode(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 332
    const v0, 0xf22f

    if-eq p1, v0, :cond_0

    const v0, -0x9bdc

    if-ne p1, v0, :cond_1

    .line 334
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchMode:Z

    .line 338
    :goto_0
    return-void

    .line 336
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->searchMode:Z

    goto :goto_0
.end method
