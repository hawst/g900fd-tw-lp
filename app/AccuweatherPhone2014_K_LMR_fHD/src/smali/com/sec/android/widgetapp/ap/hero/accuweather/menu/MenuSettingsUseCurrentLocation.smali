.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;
.super Landroid/app/Activity;
.source "MenuSettingsUseCurrentLocation.java"

# interfaces
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/model/IActivityVisibleState;


# instance fields
.field private actionbar:Landroid/app/ActionBar;

.field private actionbarBack:Landroid/widget/LinearLayout;

.field private actionbarLayout:Landroid/widget/RelativeLayout;

.field private actionbarTitle:Landroid/widget/TextView;

.field getLocDataHandler:Landroid/os/Handler;

.field private mContext:Landroid/content/Context;

.field public mHttpThreads:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Thread;",
            ">;"
        }
    .end annotation
.end field

.field public mLoadingDialog:Landroid/app/Dialog;

.field public mPopupDialog:Landroid/app/Dialog;

.field private mcl:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

.field switchView:Landroid/widget/Switch;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 51
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 52
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;

    .line 59
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->actionbarTitle:Landroid/widget/TextView;

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mHttpThreads:Ljava/util/ArrayList;

    .line 197
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->switchView:Landroid/widget/Switch;

    .line 346
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mLoadingDialog:Landroid/app/Dialog;

    .line 348
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mPopupDialog:Landroid/app/Dialog;

    .line 351
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$3;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$3;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->getLocDataHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->hideAllDialog()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->getCurrentLocation()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->getLocationInfo()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->hideLoadingDialog()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;
    .param p1, "x1"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .param p2, "x2"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->saveData(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mcl:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->stopHttpThread()V

    return-void
.end method

.method private checkResultCode(I)I
    .locals 2
    .param p1, "resultCode"    # I

    .prologue
    .line 568
    const/4 v0, 0x0

    .line 570
    .local v0, "result":I
    const/4 v1, -0x1

    if-ne v1, p1, :cond_1

    .line 571
    const/16 v0, 0x3e7

    .line 576
    :cond_0
    :goto_0
    return v0

    .line 572
    :cond_1
    if-nez p1, :cond_0

    .line 573
    const/16 v0, 0x3e7

    goto :goto_0
.end method

.method private getCurrentLocation()V
    .locals 1

    .prologue
    .line 479
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkNetworkConnected(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 480
    const v0, 0x7f0d0020

    invoke-static {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    .line 484
    :goto_0
    return-void

    .line 483
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->getLocationInfo()V

    goto :goto_0
.end method

.method private getLocationInfo()V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 581
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->hideAllDialog()V

    .line 583
    const-string v0, "location"

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/location/LocationManager;

    .line 585
    .local v7, "locationManager":Landroid/location/LocationManager;
    const-string v0, "gps"

    invoke-virtual {v7, v0}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "network"

    .line 586
    invoke-virtual {v7, v0}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 587
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->showLocatingDialog()V

    .line 590
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mcl:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->getLocDataHandler:Landroid/os/Handler;

    move v4, v2

    move v5, v3

    move v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->performGetCurrentLocation(Landroid/os/Handler;IZZZZ)V

    .line 591
    return-void
.end method

.method private hideAllDialog()V
    .locals 1

    .prologue
    .line 445
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->hideLoadingDialog()V

    .line 447
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mPopupDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 449
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mPopupDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 455
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mLoadingDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    .line 457
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 462
    :cond_1
    :goto_1
    return-void

    .line 458
    :catch_0
    move-exception v0

    goto :goto_1

    .line 450
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method private hideLoadingDialog()V
    .locals 3

    .prologue
    .line 465
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mLoadingDialog:Landroid/app/Dialog;

    if-eqz v1, :cond_0

    .line 467
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 473
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mLoadingDialog:Landroid/app/Dialog;

    .line 475
    :cond_0
    return-void

    .line 468
    :catch_0
    move-exception v0

    .line 470
    .local v0, "ex":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MenuAdd.hideLoadingDialog() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private saveData(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)V
    .locals 9
    .param p1, "cityInfo"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .param p2, "detailInfo"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .param p3, "xmlinfo"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 488
    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    .line 489
    const/4 v4, 0x1

    .line 490
    .local v4, "result":I
    const/4 v5, 0x0

    .line 492
    .local v5, "resultCode":I
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getRealLocation(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 496
    .local v2, "oldRealLocation":Ljava/lang/String;
    const-string v0, "cityId:current"

    .line 497
    .local v0, "curLocation":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v3

    .line 498
    .local v3, "realLocation":Ljava/lang/String;
    invoke-virtual {p1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setLocation(Ljava/lang/String;)V

    .line 499
    invoke-virtual {p1, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setRealLocation(Ljava/lang/String;)V

    .line 501
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;

    invoke-static {v6, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->isRegisteredToCityList(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 502
    sget-boolean v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isCurCityAtTopInCityList:Z

    if-nez v6, :cond_0

    sget-boolean v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isCurCityAtTopInCityList:Z

    if-nez v6, :cond_4

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;

    .line 504
    invoke-static {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getCheckCurrentCityLocation(Landroid/content/Context;)I

    move-result v6

    if-ne v6, v7, :cond_4

    .line 505
    :cond_0
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;

    invoke-static {v6, v0, p1, p2, p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateCity(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)I

    move-result v4

    .line 529
    :cond_1
    :goto_0
    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_8

    .line 530
    invoke-virtual {p2, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;->getCityId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 531
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;

    invoke-static {v6, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getExistPhotos(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 532
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;

    invoke-static {v6, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->deletePhotosInfo(Landroid/content/Context;Ljava/lang/String;)I

    .line 550
    :cond_2
    :goto_1
    invoke-direct {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->checkResultCode(I)I

    move-result v5

    .line 552
    const/16 v6, 0x3e7

    if-ne v6, v5, :cond_9

    .line 553
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->getLocDataHandler:Landroid/os/Handler;

    new-instance v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$4;

    invoke-direct {v7, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$4;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;)V

    invoke-virtual {v6, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 565
    .end local v0    # "curLocation":Ljava/lang/String;
    .end local v2    # "oldRealLocation":Ljava/lang/String;
    .end local v3    # "realLocation":Ljava/lang/String;
    .end local v4    # "result":I
    .end local v5    # "resultCode":I
    :cond_3
    :goto_2
    return-void

    .line 508
    .restart local v0    # "curLocation":Ljava/lang/String;
    .restart local v2    # "oldRealLocation":Ljava/lang/String;
    .restart local v3    # "realLocation":Ljava/lang/String;
    .restart local v4    # "result":I
    .restart local v5    # "resultCode":I
    :cond_4
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 509
    .local v1, "deleteList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 510
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;

    invoke-static {v6, v1, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->deleteCitys(Landroid/content/Context;Ljava/util/ArrayList;Z)I

    move-result v4

    .line 512
    if-ne v4, v7, :cond_1

    .line 513
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;

    invoke-static {v6, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->deletePhotosInfo(Landroid/content/Context;Ljava/util/ArrayList;)I

    .line 515
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;

    invoke-static {v6, p1, p2, p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->insertCityCurrentLocation(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)I

    move-result v4

    .line 518
    if-ne v4, v7, :cond_1

    .line 519
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateCheckCurrentCityLocation(Landroid/content/Context;I)I

    goto :goto_0

    .line 525
    .end local v1    # "deleteList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_5
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;

    invoke-static {v6, p1, p2, p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->insertCityCurrentLocation(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)I

    move-result v4

    goto :goto_0

    .line 535
    :cond_6
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;

    invoke-static {v6, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->isRegisteredToCityList(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_7

    .line 536
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;

    invoke-static {v6, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getExistPhotos(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 537
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;

    invoke-static {v6, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->deletePhotosInfo(Landroid/content/Context;Ljava/lang/String;)I

    .line 540
    :cond_7
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;

    invoke-virtual {p2, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;->getCityId()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getExistPhotos(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 541
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;

    invoke-virtual {p2, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;->getCityId()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->deletePhotosInfo(Landroid/content/Context;Ljava/lang/String;)I

    goto :goto_1

    .line 545
    :cond_8
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;

    invoke-virtual {p2, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;->getCityId()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getExistPhotos(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 546
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;

    invoke-virtual {p2, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;->getCityId()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->deletePhotosInfo(Landroid/content/Context;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 562
    :cond_9
    const-string v6, ""

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "LC : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " -> "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 563
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateLastSelectedLocation(Landroid/content/Context;Ljava/lang/String;)I

    goto/16 :goto_2
.end method

.method private setupActionbar()V
    .locals 8

    .prologue
    const v7, 0x7f0d0004

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 84
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->actionbar:Landroid/app/ActionBar;

    if-nez v2, :cond_0

    .line 85
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->actionbar:Landroid/app/ActionBar;

    .line 87
    :cond_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->actionbar:Landroid/app/ActionBar;

    if-eqz v2, :cond_1

    .line 88
    sget-boolean v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isLollipopOS:Z

    if-nez v2, :cond_2

    .line 90
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02008f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 92
    const v2, 0x7f030005

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->actionbarLayout:Landroid/widget/RelativeLayout;

    .line 94
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->actionbar:Landroid/app/ActionBar;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->actionbarLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 95
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->actionbarLayout:Landroid/widget/RelativeLayout;

    const v3, 0x7f080008

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->actionbarTitle:Landroid/widget/TextView;

    .line 96
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->actionbarLayout:Landroid/widget/RelativeLayout;

    const v3, 0x7f080006

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->actionbarBack:Landroid/widget/LinearLayout;

    .line 98
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->actionbarBack:Landroid/widget/LinearLayout;

    const v3, 0x7f080009

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 99
    .local v1, "v":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200aa

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 100
    .local v0, "backIcon":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0, v6}, Landroid/graphics/drawable/Drawable;->setAutoMirrored(Z)V

    .line 101
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 103
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v5}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 104
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v5}, Landroid/app/ActionBar;->setDisplayUseLogoEnabled(Z)V

    .line 105
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v5}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 106
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v6}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 108
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->actionbarTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setText(I)V

    .line 110
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->actionbarBack:Landroid/widget/LinearLayout;

    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$1;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;)V

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->actionbarBack:Landroid/widget/LinearLayout;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;

    .line 118
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d0097

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 116
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 128
    .end local v0    # "backIcon":Landroid/graphics/drawable/Drawable;
    .end local v1    # "v":Landroid/widget/ImageView;
    :cond_1
    :goto_0
    return-void

    .line 120
    :cond_2
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v5}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 121
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v5}, Landroid/app/ActionBar;->setDisplayUseLogoEnabled(Z)V

    .line 122
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v6}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 123
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v5}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 124
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v6}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 125
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v7}, Landroid/app/ActionBar;->setTitle(I)V

    goto :goto_0
.end method

.method private stopHttpThread()V
    .locals 3

    .prologue
    .line 621
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mHttpThreads:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 622
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 623
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Thread;

    .line 624
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 627
    .end local v0    # "t":Ljava/lang/Thread;
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mHttpThreads:Ljava/util/ArrayList;

    .line 629
    :cond_1
    return-void
.end method


# virtual methods
.method public isActivityVisible()Z
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 177
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 178
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 179
    invoke-virtual {p0, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->setResult(I)V

    .line 180
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->finish()V

    .line 183
    :cond_0
    if-nez p1, :cond_1

    .line 184
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->setSwitchStatus()V

    .line 188
    :cond_1
    const/16 v0, 0x4b3

    if-ne v0, p1, :cond_2

    .line 189
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getLocationCheck(Landroid/content/Context;)I

    move-result v0

    const/16 v1, 0xbbb

    if-ne v0, v1, :cond_3

    .line 196
    :cond_2
    :goto_0
    return-void

    .line 191
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getCheckCurrentCityLocation(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_2

    .line 192
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->getCurrentLocation()V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 168
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 169
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 159
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 160
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->setupActionbar()V

    .line 161
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 74
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 75
    const v0, 0x7f030023

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->setContentView(I)V

    .line 76
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutDirection(I)V

    .line 77
    iput-object p0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;

    .line 78
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mcl:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    .line 80
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->setupActionbar()V

    .line 81
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menus"    # Landroid/view/Menu;

    .prologue
    .line 199
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 201
    if-eqz p1, :cond_1

    .line 202
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v1

    if-eqz v1, :cond_0

    .line 203
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 206
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 208
    .local v0, "inflater":Landroid/view/MenuInflater;
    const v1, 0x7f0f0001

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 210
    const v1, 0x7f08012b

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f080015

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Switch;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->switchView:Landroid/widget/Switch;

    .line 212
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->switchView:Landroid/widget/Switch;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->isAgreeUseLocation(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setChecked(Z)V

    .line 213
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->switchView:Landroid/widget/Switch;

    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;

    invoke-direct {v2, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;)V

    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 341
    const/4 v1, 0x1

    .line 343
    .end local v0    # "inflater":Landroid/view/MenuInflater;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 146
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 147
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 131
    sget-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isLollipopOS:Z

    if-eqz v0, :cond_0

    .line 132
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 140
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 135
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->finish()V

    .line 136
    const/4 v0, 0x1

    goto :goto_0

    .line 132
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 150
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 152
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 155
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 156
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 0

    .prologue
    .line 164
    invoke-super {p0}, Landroid/app/Activity;->onUserLeaveHint()V

    .line 165
    return-void
.end method

.method public setActivityVisibleState(Z)V
    .locals 0
    .param p1, "onoff"    # Z

    .prologue
    .line 67
    return-void
.end method

.method public setSwitchStatus()V
    .locals 2

    .prologue
    .line 172
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->switchView:Landroid/widget/Switch;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->switchView:Landroid/widget/Switch;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->isAgreeUseLocation(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 175
    :cond_0
    return-void
.end method

.method public showLocatingDialog()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 597
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 599
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mLoadingDialog:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 600
    const/4 v0, 0x0

    const v1, 0x7f0d0038

    .line 601
    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 600
    invoke-static {p0, v0, v1, v2, v2}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mLoadingDialog:Landroid/app/Dialog;

    .line 603
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mLoadingDialog:Landroid/app/Dialog;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$5;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$5;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 616
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mLoadingDialog:Landroid/app/Dialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 617
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 618
    return-void
.end method
