.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/db/OpenHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "OpenHelper.java"


# static fields
.field public static final COL_AUTO_REFRESH_TIME:Ljava/lang/String; = "AUTO_REFRESH_TIME"

.field public static final COL_AUTO_REF_NEXT_TIME:Ljava/lang/String; = "AUTO_REF_NEXT_TIME"

.field public static final COL_AUTO_SCROLL:Ljava/lang/String; = "AUTO_SCROLL"

.field public static final COL_CHECK_CURRENT_CITY_LOCATION:Ljava/lang/String; = "CHECK_CURRENT_CITY_LOCATION"

.field public static final COL_DATE_1:Ljava/lang/String; = "DATE_1"

.field public static final COL_DATE_10:Ljava/lang/String; = "DATE_10"

.field public static final COL_DATE_11:Ljava/lang/String; = "DATE_11"

.field public static final COL_DATE_12:Ljava/lang/String; = "DATE_12"

.field public static final COL_DATE_13:Ljava/lang/String; = "DATE_13"

.field public static final COL_DATE_14:Ljava/lang/String; = "DATE_14"

.field public static final COL_DATE_15:Ljava/lang/String; = "DATE_15"

.field public static final COL_DATE_16:Ljava/lang/String; = "DATE_16"

.field public static final COL_DATE_17:Ljava/lang/String; = "DATE_17"

.field public static final COL_DATE_18:Ljava/lang/String; = "DATE_18"

.field public static final COL_DATE_19:Ljava/lang/String; = "DATE_19"

.field public static final COL_DATE_2:Ljava/lang/String; = "DATE_2"

.field public static final COL_DATE_20:Ljava/lang/String; = "DATE_20"

.field public static final COL_DATE_21:Ljava/lang/String; = "DATE_21"

.field public static final COL_DATE_22:Ljava/lang/String; = "DATE_22"

.field public static final COL_DATE_23:Ljava/lang/String; = "DATE_23"

.field public static final COL_DATE_24:Ljava/lang/String; = "DATE_24"

.field public static final COL_DATE_25:Ljava/lang/String; = "DATE_25"

.field public static final COL_DATE_26:Ljava/lang/String; = "DATE_26"

.field public static final COL_DATE_27:Ljava/lang/String; = "DATE_27"

.field public static final COL_DATE_28:Ljava/lang/String; = "DATE_28"

.field public static final COL_DATE_29:Ljava/lang/String; = "DATE_29"

.field public static final COL_DATE_3:Ljava/lang/String; = "DATE_3"

.field public static final COL_DATE_30:Ljava/lang/String; = "DATE_30"

.field public static final COL_DATE_31:Ljava/lang/String; = "DATE_31"

.field public static final COL_DATE_32:Ljava/lang/String; = "DATE_32"

.field public static final COL_DATE_33:Ljava/lang/String; = "DATE_33"

.field public static final COL_DATE_34:Ljava/lang/String; = "DATE_34"

.field public static final COL_DATE_35:Ljava/lang/String; = "DATE_35"

.field public static final COL_DATE_36:Ljava/lang/String; = "DATE_36"

.field public static final COL_DATE_37:Ljava/lang/String; = "DATE_37"

.field public static final COL_DATE_38:Ljava/lang/String; = "DATE_38"

.field public static final COL_DATE_39:Ljava/lang/String; = "DATE_39"

.field public static final COL_DATE_4:Ljava/lang/String; = "DATE_4"

.field public static final COL_DATE_40:Ljava/lang/String; = "DATE_40"

.field public static final COL_DATE_41:Ljava/lang/String; = "DATE_41"

.field public static final COL_DATE_42:Ljava/lang/String; = "DATE_42"

.field public static final COL_DATE_43:Ljava/lang/String; = "DATE_43"

.field public static final COL_DATE_44:Ljava/lang/String; = "DATE_44"

.field public static final COL_DATE_45:Ljava/lang/String; = "DATE_45"

.field public static final COL_DATE_46:Ljava/lang/String; = "DATE_46"

.field public static final COL_DATE_47:Ljava/lang/String; = "DATE_47"

.field public static final COL_DATE_48:Ljava/lang/String; = "DATE_48"

.field public static final COL_DATE_5:Ljava/lang/String; = "DATE_5"

.field public static final COL_DATE_6:Ljava/lang/String; = "DATE_6"

.field public static final COL_DATE_7:Ljava/lang/String; = "DATE_7"

.field public static final COL_DATE_8:Ljava/lang/String; = "DATE_8"

.field public static final COL_DATE_9:Ljava/lang/String; = "DATE_9"

.field public static final COL_DEFAULT_LOCATION:Ljava/lang/String; = "DEFAULT_LOCATION"

.field public static final COL_EDGE_SCREEN:Ljava/lang/String; = "EDGE_SCREEN"

.field public static final COL_EFFECT_1:Ljava/lang/String; = "EFFECT_1"

.field public static final COL_EFFECT_10:Ljava/lang/String; = "EFFECT_10"

.field public static final COL_EFFECT_11:Ljava/lang/String; = "EFFECT_11"

.field public static final COL_EFFECT_12:Ljava/lang/String; = "EFFECT_12"

.field public static final COL_EFFECT_13:Ljava/lang/String; = "EFFECT_13"

.field public static final COL_EFFECT_14:Ljava/lang/String; = "EFFECT_14"

.field public static final COL_EFFECT_15:Ljava/lang/String; = "EFFECT_15"

.field public static final COL_EFFECT_16:Ljava/lang/String; = "EFFECT_16"

.field public static final COL_EFFECT_17:Ljava/lang/String; = "EFFECT_17"

.field public static final COL_EFFECT_18:Ljava/lang/String; = "EFFECT_18"

.field public static final COL_EFFECT_19:Ljava/lang/String; = "EFFECT_19"

.field public static final COL_EFFECT_2:Ljava/lang/String; = "EFFECT_2"

.field public static final COL_EFFECT_20:Ljava/lang/String; = "EFFECT_20"

.field public static final COL_EFFECT_21:Ljava/lang/String; = "EFFECT_21"

.field public static final COL_EFFECT_22:Ljava/lang/String; = "EFFECT_22"

.field public static final COL_EFFECT_23:Ljava/lang/String; = "EFFECT_23"

.field public static final COL_EFFECT_24:Ljava/lang/String; = "EFFECT_24"

.field public static final COL_EFFECT_25:Ljava/lang/String; = "EFFECT_25"

.field public static final COL_EFFECT_26:Ljava/lang/String; = "EFFECT_26"

.field public static final COL_EFFECT_27:Ljava/lang/String; = "EFFECT_27"

.field public static final COL_EFFECT_28:Ljava/lang/String; = "EFFECT_28"

.field public static final COL_EFFECT_29:Ljava/lang/String; = "EFFECT_29"

.field public static final COL_EFFECT_3:Ljava/lang/String; = "EFFECT_3"

.field public static final COL_EFFECT_30:Ljava/lang/String; = "EFFECT_30"

.field public static final COL_EFFECT_31:Ljava/lang/String; = "EFFECT_31"

.field public static final COL_EFFECT_32:Ljava/lang/String; = "EFFECT_32"

.field public static final COL_EFFECT_33:Ljava/lang/String; = "EFFECT_33"

.field public static final COL_EFFECT_34:Ljava/lang/String; = "EFFECT_34"

.field public static final COL_EFFECT_35:Ljava/lang/String; = "EFFECT_35"

.field public static final COL_EFFECT_36:Ljava/lang/String; = "EFFECT_36"

.field public static final COL_EFFECT_37:Ljava/lang/String; = "EFFECT_37"

.field public static final COL_EFFECT_38:Ljava/lang/String; = "EFFECT_38"

.field public static final COL_EFFECT_39:Ljava/lang/String; = "EFFECT_39"

.field public static final COL_EFFECT_4:Ljava/lang/String; = "EFFECT_4"

.field public static final COL_EFFECT_40:Ljava/lang/String; = "EFFECT_40"

.field public static final COL_EFFECT_5:Ljava/lang/String; = "EFFECT_5"

.field public static final COL_EFFECT_6:Ljava/lang/String; = "EFFECT_6"

.field public static final COL_EFFECT_7:Ljava/lang/String; = "EFFECT_7"

.field public static final COL_EFFECT_8:Ljava/lang/String; = "EFFECT_8"

.field public static final COL_EFFECT_9:Ljava/lang/String; = "EFFECT_9"

.field public static final COL_EFFECT_ANIM_CITY_CHANGE:Ljava/lang/String; = "EFFECT_ANIM_CITY_CHANGE"

.field public static final COL_EFFECT_BLUR:Ljava/lang/String; = "EFFECT_BLUR"

.field public static final COL_EFFECT_CLOUDY:Ljava/lang/String; = "EFFECT_CLOUDY"

.field public static final COL_EFFECT_RAIN:Ljava/lang/String; = "EFFECT_RAIN"

.field public static final COL_EFFECT_SCALE_DOWN:Ljava/lang/String; = "EFFECT_SCALE_DOWN"

.field public static final COL_EFFECT_SENSOR:Ljava/lang/String; = "EFFECT_SEMSOR"

.field public static final COL_EFFECT_SNOW:Ljava/lang/String; = "EFFECT_SNOW"

.field public static final COL_EFFECT_SUNNY:Ljava/lang/String; = "EFFECT_SUNNY"

.field public static final COL_EFFECT_THUNDER:Ljava/lang/String; = "EFFECT_THUNDER"

.field public static final COL_FAIL_LOG:Ljava/lang/String; = "FAIL_LOG"

.field public static final COL_FIVEDAY_DAY_HAIL_PROBABILITY:Ljava/lang/String; = "FIVEDAY_DAY_HAIL_PROBABILITY"

.field public static final COL_FIVEDAY_DAY_PRECIPITATION_PROBABILITY:Ljava/lang/String; = "FIVEDAY_DAY_PRECIPITATION_PROBABILITY"

.field public static final COL_FIVEDAY_DAY_RAIN_PROBABILITY:Ljava/lang/String; = "FIVEDAY_DAY_RAIN_PROBABILITY"

.field public static final COL_FIVEDAY_DAY_SNOW_PROBABILITY:Ljava/lang/String; = "FIVEDAY_DAY_SNOW_PROBABILITY"

.field public static final COL_FIVEDAY_HIGH_TEMP:Ljava/lang/String; = "FIVEDAY_HIGH_TEMP"

.field public static final COL_FIVEDAY_ICON_NUM:Ljava/lang/String; = "FIVEDAY_ICON_NUM"

.field public static final COL_FIVEDAY_LOW_TEMP:Ljava/lang/String; = "FIVEDAY_LOW_TEMP"

.field public static final COL_FIVEDAY_SUNRISE_TIME:Ljava/lang/String; = "FIVEDAY_SUNRISE_TIME"

.field public static final COL_FIVEDAY_SUNSET_TIME:Ljava/lang/String; = "FIVEDAY_SUNSET_TIME"

.field public static final COL_FIVEDAY_URL:Ljava/lang/String; = "FIVEDAY_URL"

.field public static final COL_FOURDAY_DAY_HAIL_PROBABILITY:Ljava/lang/String; = "FOURDAY_DAY_HAIL_PROBABILITY"

.field public static final COL_FOURDAY_DAY_PRECIPITATION_PROBABILITY:Ljava/lang/String; = "FOURDAY_DAY_PRECIPITATION_PROBABILITY"

.field public static final COL_FOURDAY_DAY_RAIN_PROBABILITY:Ljava/lang/String; = "FOURDAY_DAY_RAIN_PROBABILITY"

.field public static final COL_FOURDAY_DAY_SNOW_PROBABILITY:Ljava/lang/String; = "FOURDAY_DAY_SNOW_PROBABILITY"

.field public static final COL_FOURDAY_HIGH_TEMP:Ljava/lang/String; = "FOURDAY_HIGH_TEMP"

.field public static final COL_FOURDAY_ICON_NUM:Ljava/lang/String; = "FOURDAY_ICON_NUM"

.field public static final COL_FOURDAY_LOW_TEMP:Ljava/lang/String; = "FOURDAY_LOW_TEMP"

.field public static final COL_FOURDAY_SUNRISE_TIME:Ljava/lang/String; = "FOURDAY_SUNRISE_TIME"

.field public static final COL_FOURDAY_SUNSET_TIME:Ljava/lang/String; = "FOURDAY_SUNSET_TIME"

.field public static final COL_FOURDAY_URL:Ljava/lang/String; = "FOURDAY_URL"

.field public static final COL_HOUR_1:Ljava/lang/String; = "HOUR_1"

.field public static final COL_HOUR_10:Ljava/lang/String; = "HOUR_10"

.field public static final COL_HOUR_11:Ljava/lang/String; = "HOUR_11"

.field public static final COL_HOUR_12:Ljava/lang/String; = "HOUR_12"

.field public static final COL_HOUR_13:Ljava/lang/String; = "HOUR_13"

.field public static final COL_HOUR_14:Ljava/lang/String; = "HOUR_14"

.field public static final COL_HOUR_15:Ljava/lang/String; = "HOUR_15"

.field public static final COL_HOUR_16:Ljava/lang/String; = "HOUR_16"

.field public static final COL_HOUR_17:Ljava/lang/String; = "HOUR_17"

.field public static final COL_HOUR_18:Ljava/lang/String; = "HOUR_18"

.field public static final COL_HOUR_19:Ljava/lang/String; = "HOUR_19"

.field public static final COL_HOUR_2:Ljava/lang/String; = "HOUR_2"

.field public static final COL_HOUR_20:Ljava/lang/String; = "HOUR_20"

.field public static final COL_HOUR_21:Ljava/lang/String; = "HOUR_21"

.field public static final COL_HOUR_22:Ljava/lang/String; = "HOUR_22"

.field public static final COL_HOUR_23:Ljava/lang/String; = "HOUR_23"

.field public static final COL_HOUR_24:Ljava/lang/String; = "HOUR_24"

.field public static final COL_HOUR_25:Ljava/lang/String; = "HOUR_25"

.field public static final COL_HOUR_26:Ljava/lang/String; = "HOUR_26"

.field public static final COL_HOUR_27:Ljava/lang/String; = "HOUR_27"

.field public static final COL_HOUR_28:Ljava/lang/String; = "HOUR_28"

.field public static final COL_HOUR_29:Ljava/lang/String; = "HOUR_29"

.field public static final COL_HOUR_3:Ljava/lang/String; = "HOUR_3"

.field public static final COL_HOUR_30:Ljava/lang/String; = "HOUR_30"

.field public static final COL_HOUR_31:Ljava/lang/String; = "HOUR_31"

.field public static final COL_HOUR_32:Ljava/lang/String; = "HOUR_32"

.field public static final COL_HOUR_33:Ljava/lang/String; = "HOUR_33"

.field public static final COL_HOUR_34:Ljava/lang/String; = "HOUR_34"

.field public static final COL_HOUR_35:Ljava/lang/String; = "HOUR_35"

.field public static final COL_HOUR_36:Ljava/lang/String; = "HOUR_36"

.field public static final COL_HOUR_37:Ljava/lang/String; = "HOUR_37"

.field public static final COL_HOUR_38:Ljava/lang/String; = "HOUR_38"

.field public static final COL_HOUR_39:Ljava/lang/String; = "HOUR_39"

.field public static final COL_HOUR_4:Ljava/lang/String; = "HOUR_4"

.field public static final COL_HOUR_40:Ljava/lang/String; = "HOUR_40"

.field public static final COL_HOUR_41:Ljava/lang/String; = "HOUR_41"

.field public static final COL_HOUR_42:Ljava/lang/String; = "HOUR_42"

.field public static final COL_HOUR_43:Ljava/lang/String; = "HOUR_43"

.field public static final COL_HOUR_44:Ljava/lang/String; = "HOUR_44"

.field public static final COL_HOUR_45:Ljava/lang/String; = "HOUR_45"

.field public static final COL_HOUR_46:Ljava/lang/String; = "HOUR_46"

.field public static final COL_HOUR_47:Ljava/lang/String; = "HOUR_47"

.field public static final COL_HOUR_48:Ljava/lang/String; = "HOUR_48"

.field public static final COL_HOUR_5:Ljava/lang/String; = "HOUR_5"

.field public static final COL_HOUR_6:Ljava/lang/String; = "HOUR_6"

.field public static final COL_HOUR_7:Ljava/lang/String; = "HOUR_7"

.field public static final COL_HOUR_8:Ljava/lang/String; = "HOUR_8"

.field public static final COL_HOUR_9:Ljava/lang/String; = "HOUR_9"

.field public static final COL_ICON_NUM_1:Ljava/lang/String; = "ICON_NUM_1"

.field public static final COL_ICON_NUM_10:Ljava/lang/String; = "ICON_NUM_10"

.field public static final COL_ICON_NUM_11:Ljava/lang/String; = "ICON_NUM_11"

.field public static final COL_ICON_NUM_12:Ljava/lang/String; = "ICON_NUM_12"

.field public static final COL_ICON_NUM_13:Ljava/lang/String; = "ICON_NUM_13"

.field public static final COL_ICON_NUM_14:Ljava/lang/String; = "ICON_NUM_14"

.field public static final COL_ICON_NUM_15:Ljava/lang/String; = "ICON_NUM_15"

.field public static final COL_ICON_NUM_16:Ljava/lang/String; = "ICON_NUM_16"

.field public static final COL_ICON_NUM_17:Ljava/lang/String; = "ICON_NUM_17"

.field public static final COL_ICON_NUM_18:Ljava/lang/String; = "ICON_NUM_18"

.field public static final COL_ICON_NUM_19:Ljava/lang/String; = "ICON_NUM_19"

.field public static final COL_ICON_NUM_2:Ljava/lang/String; = "ICON_NUM_2"

.field public static final COL_ICON_NUM_20:Ljava/lang/String; = "ICON_NUM_20"

.field public static final COL_ICON_NUM_21:Ljava/lang/String; = "ICON_NUM_21"

.field public static final COL_ICON_NUM_22:Ljava/lang/String; = "ICON_NUM_22"

.field public static final COL_ICON_NUM_23:Ljava/lang/String; = "ICON_NUM_23"

.field public static final COL_ICON_NUM_24:Ljava/lang/String; = "ICON_NUM_24"

.field public static final COL_ICON_NUM_25:Ljava/lang/String; = "ICON_NUM_25"

.field public static final COL_ICON_NUM_26:Ljava/lang/String; = "ICON_NUM_26"

.field public static final COL_ICON_NUM_27:Ljava/lang/String; = "ICON_NUM_27"

.field public static final COL_ICON_NUM_28:Ljava/lang/String; = "ICON_NUM_28"

.field public static final COL_ICON_NUM_29:Ljava/lang/String; = "ICON_NUM_29"

.field public static final COL_ICON_NUM_3:Ljava/lang/String; = "ICON_NUM_3"

.field public static final COL_ICON_NUM_30:Ljava/lang/String; = "ICON_NUM_30"

.field public static final COL_ICON_NUM_31:Ljava/lang/String; = "ICON_NUM_31"

.field public static final COL_ICON_NUM_32:Ljava/lang/String; = "ICON_NUM_32"

.field public static final COL_ICON_NUM_33:Ljava/lang/String; = "ICON_NUM_33"

.field public static final COL_ICON_NUM_34:Ljava/lang/String; = "ICON_NUM_34"

.field public static final COL_ICON_NUM_35:Ljava/lang/String; = "ICON_NUM_35"

.field public static final COL_ICON_NUM_36:Ljava/lang/String; = "ICON_NUM_36"

.field public static final COL_ICON_NUM_37:Ljava/lang/String; = "ICON_NUM_37"

.field public static final COL_ICON_NUM_38:Ljava/lang/String; = "ICON_NUM_38"

.field public static final COL_ICON_NUM_39:Ljava/lang/String; = "ICON_NUM_39"

.field public static final COL_ICON_NUM_4:Ljava/lang/String; = "ICON_NUM_4"

.field public static final COL_ICON_NUM_40:Ljava/lang/String; = "ICON_NUM_40"

.field public static final COL_ICON_NUM_41:Ljava/lang/String; = "ICON_NUM_41"

.field public static final COL_ICON_NUM_42:Ljava/lang/String; = "ICON_NUM_42"

.field public static final COL_ICON_NUM_43:Ljava/lang/String; = "ICON_NUM_43"

.field public static final COL_ICON_NUM_44:Ljava/lang/String; = "ICON_NUM_44"

.field public static final COL_ICON_NUM_45:Ljava/lang/String; = "ICON_NUM_45"

.field public static final COL_ICON_NUM_46:Ljava/lang/String; = "ICON_NUM_46"

.field public static final COL_ICON_NUM_47:Ljava/lang/String; = "ICON_NUM_47"

.field public static final COL_ICON_NUM_48:Ljava/lang/String; = "ICON_NUM_48"

.field public static final COL_ICON_NUM_5:Ljava/lang/String; = "ICON_NUM_5"

.field public static final COL_ICON_NUM_6:Ljava/lang/String; = "ICON_NUM_6"

.field public static final COL_ICON_NUM_7:Ljava/lang/String; = "ICON_NUM_7"

.field public static final COL_ICON_NUM_8:Ljava/lang/String; = "ICON_NUM_8"

.field public static final COL_ICON_NUM_9:Ljava/lang/String; = "ICON_NUM_9"

.field public static final COL_IS_FORECAST_CITY:Ljava/lang/String; = "IS_FORECAST_CITY"

.field public static final COL_LAST_SEL_LOCATION:Ljava/lang/String; = "LAST_SEL_LOCATION"

.field public static final COL_LATITUDE:Ljava/lang/String; = "LATITUDE"

.field public static final COL_LOCATION:Ljava/lang/String; = "LOCATION"

.field public static final COL_LOCATION_SERVICES:Ljava/lang/String; = "LOCATION_SERVICES"

.field public static final COL_LOCKSCREEN_AND_S_VIEW_COVER:Ljava/lang/String; = "LOCKSCREEN_AND_S_VIEW_COVER"

.field public static final COL_LONGITUDE:Ljava/lang/String; = "LONGITUDE"

.field public static final COL_NAME:Ljava/lang/String; = "NAME"

.field public static final COL_NOTIFICATION:Ljava/lang/String; = "NOTIFICATION"

.field public static final COL_NOTIFICATION_SET_TIME:Ljava/lang/String; = "NOTIFICATION_SET_TIME"

.field public static final COL_NOW_TIME:Ljava/lang/String; = "NOW_TIME"

.field public static final COL_ONEDAY_DAY_HAIL_AMOUNT:Ljava/lang/String; = "ONEDAY_DAY_HAIL_AMOUNT"

.field public static final COL_ONEDAY_DAY_HAIL_PROBABILITY:Ljava/lang/String; = "ONEDAY_DAY_HAIL_PROBABILITY"

.field public static final COL_ONEDAY_DAY_ICON:Ljava/lang/String; = "ONEDAY_DAY_ICON"

.field public static final COL_ONEDAY_DAY_PRECIPITATION_AMOUNT:Ljava/lang/String; = "ONEDAY_DAY_PRECIPITATION_AMOUNT"

.field public static final COL_ONEDAY_DAY_PRECIPITATION_PROBABILITY:Ljava/lang/String; = "ONEDAY_DAY_PRECIPITATION_PROBABILITY"

.field public static final COL_ONEDAY_DAY_RAIN_AMOUNT:Ljava/lang/String; = "ONEDAY_DAY_RAIN_AMOUNT"

.field public static final COL_ONEDAY_DAY_RAIN_PROBABILITY:Ljava/lang/String; = "ONEDAY_DAY_RAIN_PROBABILITY"

.field public static final COL_ONEDAY_DAY_SNOW_AMOUNT:Ljava/lang/String; = "ONEDAY_DAY_SNOW_AMOUNT"

.field public static final COL_ONEDAY_DAY_SNOW_PROBABILITY:Ljava/lang/String; = "ONEDAY_DAY_SNOW_PROBABILITY"

.field public static final COL_ONEDAY_HIGH_TEMP:Ljava/lang/String; = "ONEDAY_HIGH_TEMP"

.field public static final COL_ONEDAY_ICON_NUM:Ljava/lang/String; = "ONEDAY_ICON_NUM"

.field public static final COL_ONEDAY_LOW_TEMP:Ljava/lang/String; = "ONEDAY_LOW_TEMP"

.field public static final COL_ONEDAY_NIGHT_HAIL_AMOUNT:Ljava/lang/String; = "ONEDAY_NIGHT_HAIL_AMOUNT"

.field public static final COL_ONEDAY_NIGHT_HAIL_PROBABILITY:Ljava/lang/String; = "ONEDAY_NIGHT_HAIL_PROBABILITY"

.field public static final COL_ONEDAY_NIGHT_ICON:Ljava/lang/String; = "ONEDAY_NIGHT_ICON"

.field public static final COL_ONEDAY_NIGHT_PRECIPITATION_AMOUNT:Ljava/lang/String; = "ONEDAY_NIGHT_PRECIPITATION_AMOUNT"

.field public static final COL_ONEDAY_NIGHT_PRECIPITATION_PROBABILITY:Ljava/lang/String; = "ONEDAY_NIGHT_PRECIPITATION_PROBABILITY"

.field public static final COL_ONEDAY_NIGHT_RAIN_AMOUNT:Ljava/lang/String; = "ONEDAY_NIGHT_RAIN_AMOUNT"

.field public static final COL_ONEDAY_NIGHT_RAIN_PROBABILITY:Ljava/lang/String; = "ONEDAY_NIGHT_RAIN_PROBABILITY"

.field public static final COL_ONEDAY_NIGHT_SNOW_AMOUNT:Ljava/lang/String; = "ONEDAY_NIGHT_SNOW_AMOUNT"

.field public static final COL_ONEDAY_NIGHT_SNOW_PROBABILITY:Ljava/lang/String; = "ONEDAY_NIGHT_SNOW_PROBABILITY"

.field public static final COL_ONEDAY_SUNRISE_TIME:Ljava/lang/String; = "ONEDAY_SUNRISE_TIME"

.field public static final COL_ONEDAY_SUNSET_TIME:Ljava/lang/String; = "ONEDAY_SUNSET_TIME"

.field public static final COL_ONEDAY_URL:Ljava/lang/String; = "ONEDAY_URL"

.field public static final COL_PACKAGENAME:Ljava/lang/String; = "PACKAGENAME"

.field public static final COL_PHOTO_CITY_ID:Ljava/lang/String; = "LOCATION"

.field public static final COL_PHOTO_DESCRIPTION:Ljava/lang/String; = "PHOTO_DESCRIPTION"

.field public static final COL_PHOTO_LANDSCAPE_LINK:Ljava/lang/String; = "PHOTO_LANDSCAPE_LINK"

.field public static final COL_PHOTO_PORTRAIT_LINK:Ljava/lang/String; = "PHOTO_PORTRAIT_LINK"

.field public static final COL_PHOTO_SOURCE:Ljava/lang/String; = "PHOTO_SOURCE"

.field public static final COL_RAIN_FORECAST_1:Ljava/lang/String; = "RAIN_FORECAST_1"

.field public static final COL_RAIN_FORECAST_10:Ljava/lang/String; = "RAIN_FORECAST_10"

.field public static final COL_RAIN_FORECAST_11:Ljava/lang/String; = "RAIN_FORECAST_11"

.field public static final COL_RAIN_FORECAST_12:Ljava/lang/String; = "RAIN_FORECAST_12"

.field public static final COL_RAIN_FORECAST_13:Ljava/lang/String; = "RAIN_FORECAST_13"

.field public static final COL_RAIN_FORECAST_14:Ljava/lang/String; = "RAIN_FORECAST_14"

.field public static final COL_RAIN_FORECAST_15:Ljava/lang/String; = "RAIN_FORECAST_15"

.field public static final COL_RAIN_FORECAST_16:Ljava/lang/String; = "RAIN_FORECAST_16"

.field public static final COL_RAIN_FORECAST_17:Ljava/lang/String; = "RAIN_FORECAST_17"

.field public static final COL_RAIN_FORECAST_18:Ljava/lang/String; = "RAIN_FORECAST_18"

.field public static final COL_RAIN_FORECAST_19:Ljava/lang/String; = "RAIN_FORECAST_19"

.field public static final COL_RAIN_FORECAST_2:Ljava/lang/String; = "RAIN_FORECAST_2"

.field public static final COL_RAIN_FORECAST_20:Ljava/lang/String; = "RAIN_FORECAST_20"

.field public static final COL_RAIN_FORECAST_21:Ljava/lang/String; = "RAIN_FORECAST_21"

.field public static final COL_RAIN_FORECAST_22:Ljava/lang/String; = "RAIN_FORECAST_22"

.field public static final COL_RAIN_FORECAST_23:Ljava/lang/String; = "RAIN_FORECAST_23"

.field public static final COL_RAIN_FORECAST_24:Ljava/lang/String; = "RAIN_FORECAST_24"

.field public static final COL_RAIN_FORECAST_25:Ljava/lang/String; = "RAIN_FORECAST_25"

.field public static final COL_RAIN_FORECAST_26:Ljava/lang/String; = "RAIN_FORECAST_26"

.field public static final COL_RAIN_FORECAST_27:Ljava/lang/String; = "RAIN_FORECAST_27"

.field public static final COL_RAIN_FORECAST_28:Ljava/lang/String; = "RAIN_FORECAST_28"

.field public static final COL_RAIN_FORECAST_29:Ljava/lang/String; = "RAIN_FORECAST_29"

.field public static final COL_RAIN_FORECAST_3:Ljava/lang/String; = "RAIN_FORECAST_3"

.field public static final COL_RAIN_FORECAST_30:Ljava/lang/String; = "RAIN_FORECAST_30"

.field public static final COL_RAIN_FORECAST_31:Ljava/lang/String; = "RAIN_FORECAST_31"

.field public static final COL_RAIN_FORECAST_32:Ljava/lang/String; = "RAIN_FORECAST_32"

.field public static final COL_RAIN_FORECAST_33:Ljava/lang/String; = "RAIN_FORECAST_33"

.field public static final COL_RAIN_FORECAST_34:Ljava/lang/String; = "RAIN_FORECAST_34"

.field public static final COL_RAIN_FORECAST_35:Ljava/lang/String; = "RAIN_FORECAST_35"

.field public static final COL_RAIN_FORECAST_36:Ljava/lang/String; = "RAIN_FORECAST_36"

.field public static final COL_RAIN_FORECAST_37:Ljava/lang/String; = "RAIN_FORECAST_37"

.field public static final COL_RAIN_FORECAST_38:Ljava/lang/String; = "RAIN_FORECAST_38"

.field public static final COL_RAIN_FORECAST_39:Ljava/lang/String; = "RAIN_FORECAST_39"

.field public static final COL_RAIN_FORECAST_4:Ljava/lang/String; = "RAIN_FORECAST_4"

.field public static final COL_RAIN_FORECAST_40:Ljava/lang/String; = "RAIN_FORECAST_40"

.field public static final COL_RAIN_FORECAST_41:Ljava/lang/String; = "RAIN_FORECAST_41"

.field public static final COL_RAIN_FORECAST_42:Ljava/lang/String; = "RAIN_FORECAST_42"

.field public static final COL_RAIN_FORECAST_43:Ljava/lang/String; = "RAIN_FORECAST_43"

.field public static final COL_RAIN_FORECAST_44:Ljava/lang/String; = "RAIN_FORECAST_44"

.field public static final COL_RAIN_FORECAST_45:Ljava/lang/String; = "RAIN_FORECAST_45"

.field public static final COL_RAIN_FORECAST_46:Ljava/lang/String; = "RAIN_FORECAST_46"

.field public static final COL_RAIN_FORECAST_47:Ljava/lang/String; = "RAIN_FORECAST_47"

.field public static final COL_RAIN_FORECAST_48:Ljava/lang/String; = "RAIN_FORECAST_48"

.field public static final COL_RAIN_FORECAST_5:Ljava/lang/String; = "RAIN_FORECAST_5"

.field public static final COL_RAIN_FORECAST_6:Ljava/lang/String; = "RAIN_FORECAST_6"

.field public static final COL_RAIN_FORECAST_7:Ljava/lang/String; = "RAIN_FORECAST_7"

.field public static final COL_RAIN_FORECAST_8:Ljava/lang/String; = "RAIN_FORECAST_8"

.field public static final COL_RAIN_FORECAST_9:Ljava/lang/String; = "RAIN_FORECAST_9"

.field public static final COL_REAL_LOCATION:Ljava/lang/String; = "REAL_LOCATION"

.field public static final COL_REFRESH_ENTERING:Ljava/lang/String; = "REFRESH_ENTERING"

.field public static final COL_REFRESH_ROAMING:Ljava/lang/String; = "REFRESH_ROAMING"

.field public static final COL_SETTINGS_WIDGET_COUNT:Ljava/lang/String; = "WIDGET_COUNT"

.field public static final COL_SHOW_USE_LOCATION_POPUP:Ljava/lang/String; = "SHOW_USE_LOCATION_POPUP"

.field public static final COL_SIXDAY_DAY_HAIL_PROBABILITY:Ljava/lang/String; = "SIXDAY_DAY_HAIL_PROBABILITY"

.field public static final COL_SIXDAY_DAY_PRECIPITATION_PROBABILITY:Ljava/lang/String; = "SIXDAY_DAY_PRECIPITATION_PROBABILITY"

.field public static final COL_SIXDAY_DAY_RAIN_PROBABILITY:Ljava/lang/String; = "SIXDAY_DAY_RAIN_PROBABILITY"

.field public static final COL_SIXDAY_DAY_SNOW_PROBABILITY:Ljava/lang/String; = "SIXDAY_DAY_SNOW_PROBABILITY"

.field public static final COL_SIXDAY_HIGH_TEMP:Ljava/lang/String; = "SIXDAY_HIGH_TEMP"

.field public static final COL_SIXDAY_ICON_NUM:Ljava/lang/String; = "SIXDAY_ICON_NUM"

.field public static final COL_SIXDAY_LOW_TEMP:Ljava/lang/String; = "SIXDAY_LOW_TEMP"

.field public static final COL_SIXDAY_SUNRISE_TIME:Ljava/lang/String; = "SIXDAY_SUNRISE_TIME"

.field public static final COL_SIXDAY_SUNSET_TIME:Ljava/lang/String; = "SIXDAY_SUNSET_TIME"

.field public static final COL_SIXDAY_URL:Ljava/lang/String; = "SIXDAY_URL"

.field public static final COL_STATE:Ljava/lang/String; = "STATE"

.field public static final COL_SUMMER_TIME:Ljava/lang/String; = "SUMMER_TIME"

.field public static final COL_S_PLANNER:Ljava/lang/String; = "S_PLANNER"

.field public static final COL_TEMP_1:Ljava/lang/String; = "TEMP_1"

.field public static final COL_TEMP_10:Ljava/lang/String; = "TEMP_10"

.field public static final COL_TEMP_11:Ljava/lang/String; = "TEMP_11"

.field public static final COL_TEMP_12:Ljava/lang/String; = "TEMP_12"

.field public static final COL_TEMP_13:Ljava/lang/String; = "TEMP_13"

.field public static final COL_TEMP_14:Ljava/lang/String; = "TEMP_14"

.field public static final COL_TEMP_15:Ljava/lang/String; = "TEMP_15"

.field public static final COL_TEMP_16:Ljava/lang/String; = "TEMP_16"

.field public static final COL_TEMP_17:Ljava/lang/String; = "TEMP_17"

.field public static final COL_TEMP_18:Ljava/lang/String; = "TEMP_18"

.field public static final COL_TEMP_19:Ljava/lang/String; = "TEMP_19"

.field public static final COL_TEMP_2:Ljava/lang/String; = "TEMP_2"

.field public static final COL_TEMP_20:Ljava/lang/String; = "TEMP_20"

.field public static final COL_TEMP_21:Ljava/lang/String; = "TEMP_21"

.field public static final COL_TEMP_22:Ljava/lang/String; = "TEMP_22"

.field public static final COL_TEMP_23:Ljava/lang/String; = "TEMP_23"

.field public static final COL_TEMP_24:Ljava/lang/String; = "TEMP_24"

.field public static final COL_TEMP_25:Ljava/lang/String; = "TEMP_25"

.field public static final COL_TEMP_26:Ljava/lang/String; = "TEMP_26"

.field public static final COL_TEMP_27:Ljava/lang/String; = "TEMP_27"

.field public static final COL_TEMP_28:Ljava/lang/String; = "TEMP_28"

.field public static final COL_TEMP_29:Ljava/lang/String; = "TEMP_29"

.field public static final COL_TEMP_3:Ljava/lang/String; = "TEMP_3"

.field public static final COL_TEMP_30:Ljava/lang/String; = "TEMP_30"

.field public static final COL_TEMP_31:Ljava/lang/String; = "TEMP_31"

.field public static final COL_TEMP_32:Ljava/lang/String; = "TEMP_32"

.field public static final COL_TEMP_33:Ljava/lang/String; = "TEMP_33"

.field public static final COL_TEMP_34:Ljava/lang/String; = "TEMP_34"

.field public static final COL_TEMP_35:Ljava/lang/String; = "TEMP_35"

.field public static final COL_TEMP_36:Ljava/lang/String; = "TEMP_36"

.field public static final COL_TEMP_37:Ljava/lang/String; = "TEMP_37"

.field public static final COL_TEMP_38:Ljava/lang/String; = "TEMP_38"

.field public static final COL_TEMP_39:Ljava/lang/String; = "TEMP_39"

.field public static final COL_TEMP_4:Ljava/lang/String; = "TEMP_4"

.field public static final COL_TEMP_40:Ljava/lang/String; = "TEMP_40"

.field public static final COL_TEMP_41:Ljava/lang/String; = "TEMP_41"

.field public static final COL_TEMP_42:Ljava/lang/String; = "TEMP_42"

.field public static final COL_TEMP_43:Ljava/lang/String; = "TEMP_43"

.field public static final COL_TEMP_44:Ljava/lang/String; = "TEMP_44"

.field public static final COL_TEMP_45:Ljava/lang/String; = "TEMP_45"

.field public static final COL_TEMP_46:Ljava/lang/String; = "TEMP_46"

.field public static final COL_TEMP_47:Ljava/lang/String; = "TEMP_47"

.field public static final COL_TEMP_48:Ljava/lang/String; = "TEMP_48"

.field public static final COL_TEMP_5:Ljava/lang/String; = "TEMP_5"

.field public static final COL_TEMP_6:Ljava/lang/String; = "TEMP_6"

.field public static final COL_TEMP_7:Ljava/lang/String; = "TEMP_7"

.field public static final COL_TEMP_8:Ljava/lang/String; = "TEMP_8"

.field public static final COL_TEMP_9:Ljava/lang/String; = "TEMP_9"

.field public static final COL_TEMP_SCALE:Ljava/lang/String; = "TEMP_SCALE"

.field public static final COL_THREEDAY_DAY_HAIL_PROBABILITY:Ljava/lang/String; = "THREEDAY_DAY_HAIL_PROBABILITY"

.field public static final COL_THREEDAY_DAY_PRECIPITATION_PROBABILITY:Ljava/lang/String; = "THREEDAY_DAY_PRECIPITATION_PROBABILITY"

.field public static final COL_THREEDAY_DAY_RAIN_PROBABILITY:Ljava/lang/String; = "THREEDAY_DAY_RAIN_PROBABILITY"

.field public static final COL_THREEDAY_DAY_SNOW_PROBABILITY:Ljava/lang/String; = "THREEDAY_DAY_SNOW_PROBABILITY"

.field public static final COL_THREEDAY_HIGH_TEMP:Ljava/lang/String; = "THREEDAY_HIGH_TEMP"

.field public static final COL_THREEDAY_ICON_NUM:Ljava/lang/String; = "THREEDAY_ICON_NUM"

.field public static final COL_THREEDAY_LOW_TEMP:Ljava/lang/String; = "THREEDAY_LOW_TEMP"

.field public static final COL_THREEDAY_SUNRISE_TIME:Ljava/lang/String; = "THREEDAY_SUNRISE_TIME"

.field public static final COL_THREEDAY_SUNSET_TIME:Ljava/lang/String; = "THREEDAY_SUNSET_TIME"

.field public static final COL_THREEDAY_URL:Ljava/lang/String; = "THREEDAY_URL"

.field public static final COL_TIMEZONE:Ljava/lang/String; = "TIMEZONE"

.field public static final COL_TODAY_DATE:Ljava/lang/String; = "TODAY_DATE"

.field public static final COL_TODAY_DAY_HAIL_AMOUNT:Ljava/lang/String; = "TODAY_DAY_HAIL_AMOUNT"

.field public static final COL_TODAY_DAY_HAIL_PROBABILITY:Ljava/lang/String; = "TODAY_DAY_HAIL_PROBABILITY"

.field public static final COL_TODAY_DAY_ICON:Ljava/lang/String; = "TODAY_DAY_ICON"

.field public static final COL_TODAY_DAY_PRECIPITATION_AMOUNT:Ljava/lang/String; = "TODAY_DAY_PRECIPITATION_AMOUNT"

.field public static final COL_TODAY_DAY_PRECIPITATION_PROBABILITY:Ljava/lang/String; = "TODAY_DAY_PRECIPITATION_PROBABILITY"

.field public static final COL_TODAY_DAY_RAIN_AMOUNT:Ljava/lang/String; = "TODAY_DAY_RAIN_AMOUNT"

.field public static final COL_TODAY_DAY_RAIN_PROBABILITY:Ljava/lang/String; = "TODAY_DAY_RAIN_PROBABILITY"

.field public static final COL_TODAY_DAY_SNOW_AMOUNT:Ljava/lang/String; = "TODAY_DAY_SNOW_AMOUNT"

.field public static final COL_TODAY_DAY_SNOW_PROBABILITY:Ljava/lang/String; = "TODAY_DAY_SNOW_PROBABILITY"

.field public static final COL_TODAY_HIGH_TEMP:Ljava/lang/String; = "TODAY_HIGH_TEMP"

.field public static final COL_TODAY_HUM:Ljava/lang/String; = "TODAY_HUM"

.field public static final COL_TODAY_ICON_NUM:Ljava/lang/String; = "TODAY_ICON_NUM"

.field public static final COL_TODAY_LOW_TEMP:Ljava/lang/String; = "TODAY_LOW_TEMP"

.field public static final COL_TODAY_NIGHT_HAIL_AMOUNT:Ljava/lang/String; = "TODAY_NIGHT_HAIL_AMOUNT"

.field public static final COL_TODAY_NIGHT_HAIL_PROBABILITY:Ljava/lang/String; = "TODAY_NIGHT_HAIL_PROBABILITY"

.field public static final COL_TODAY_NIGHT_ICON:Ljava/lang/String; = "TODAY_NIGHT_ICON"

.field public static final COL_TODAY_NIGHT_PRECIPITATION_AMOUNT:Ljava/lang/String; = "TODAY_NIGHT_PRECIPITATION_AMOUNT"

.field public static final COL_TODAY_NIGHT_PRECIPITATION_PROBABILITY:Ljava/lang/String; = "TODAY_NIGHT_PRECIPITATION_PROBABILITY"

.field public static final COL_TODAY_NIGHT_RAIN_AMOUNT:Ljava/lang/String; = "TODAY_NIGHT_RAIN_AMOUNT"

.field public static final COL_TODAY_NIGHT_RAIN_PROBABILITY:Ljava/lang/String; = "TODAY_NIGHT_RAIN_PROBABILITY"

.field public static final COL_TODAY_NIGHT_SNOW_AMOUNT:Ljava/lang/String; = "TODAY_NIGHT_SNOW_AMOUNT"

.field public static final COL_TODAY_NIGHT_SNOW_PROBABILITY:Ljava/lang/String; = "TODAY_NIGHT_SNOW_PROBABILITY"

.field public static final COL_TODAY_REALFEEL:Ljava/lang/String; = "TODAY_REALFELL"

.field public static final COL_TODAY_SUNRISE_TIME:Ljava/lang/String; = "TODAY_SUNRISE_TIME"

.field public static final COL_TODAY_SUNSET_TIME:Ljava/lang/String; = "TODAY_SUNSET_TIME"

.field public static final COL_TODAY_TEMP:Ljava/lang/String; = "TODAY_TEMP"

.field public static final COL_TODAY_UV_INDEX:Ljava/lang/String; = "TODAY_UV_INDEX"

.field public static final COL_TODAY_UV_INDEX_TEXT:Ljava/lang/String; = "TODAY_UV_INDEX_TEXT"

.field public static final COL_TODAY_WEATHER_TEXT:Ljava/lang/String; = "TODAY_WEATHER_TEXT"

.field public static final COL_TODAY_WEATHER_URL:Ljava/lang/String; = "TODAY_WEATHER_URL"

.field public static final COL_TODAY_WIND_DIRECTION:Ljava/lang/String; = "TODAY_WIND_DIRECTION"

.field public static final COL_TODAY_WIND_SPEED:Ljava/lang/String; = "TODAY_WIND_SPEED"

.field public static final COL_TWODAY_DAY_HAIL_PROBABILITY:Ljava/lang/String; = "TWODAY_DAY_HAIL_PROBABILITY"

.field public static final COL_TWODAY_DAY_PRECIPITATION_PROBABILITY:Ljava/lang/String; = "TWODAY_DAY_PRECIPITATION_PROBABILITY"

.field public static final COL_TWODAY_DAY_RAIN_PROBABILITY:Ljava/lang/String; = "TWODAY_DAY_RAIN_PROBABILITY"

.field public static final COL_TWODAY_DAY_SNOW_PROBABILITY:Ljava/lang/String; = "TWODAY_DAY_SNOW_PROBABILITY"

.field public static final COL_TWODAY_HIGH_TEMP:Ljava/lang/String; = "TWODAY_HIGH_TEMP"

.field public static final COL_TWODAY_ICON_NUM:Ljava/lang/String; = "TWODAY_ICON_NUM"

.field public static final COL_TWODAY_LOW_TEMP:Ljava/lang/String; = "TWODAY_LOW_TEMP"

.field public static final COL_TWODAY_SUNRISE_TIME:Ljava/lang/String; = "TWODAY_SUNRISE_TIME"

.field public static final COL_TWODAY_SUNSET_TIME:Ljava/lang/String; = "TWODAY_SUNSET_TIME"

.field public static final COL_TWODAY_URL:Ljava/lang/String; = "TWODAY_URL"

.field public static final COL_UPDATE_DATE:Ljava/lang/String; = "UPDATE_DATE"

.field public static final COL_WEATHER_ICON_NUM:Ljava/lang/String; = "WEATHER_ICON_NUM"

.field public static final COL_WIND_SPEED_1:Ljava/lang/String; = "WIND_SPEED_1"

.field public static final COL_WIND_SPEED_10:Ljava/lang/String; = "WIND_SPEED_10"

.field public static final COL_WIND_SPEED_11:Ljava/lang/String; = "WIND_SPEED_11"

.field public static final COL_WIND_SPEED_12:Ljava/lang/String; = "WIND_SPEED_12"

.field public static final COL_WIND_SPEED_13:Ljava/lang/String; = "WIND_SPEED_13"

.field public static final COL_WIND_SPEED_14:Ljava/lang/String; = "WIND_SPEED_14"

.field public static final COL_WIND_SPEED_15:Ljava/lang/String; = "WIND_SPEED_15"

.field public static final COL_WIND_SPEED_16:Ljava/lang/String; = "WIND_SPEED_16"

.field public static final COL_WIND_SPEED_17:Ljava/lang/String; = "WIND_SPEED_17"

.field public static final COL_WIND_SPEED_18:Ljava/lang/String; = "WIND_SPEED_18"

.field public static final COL_WIND_SPEED_19:Ljava/lang/String; = "WIND_SPEED_19"

.field public static final COL_WIND_SPEED_2:Ljava/lang/String; = "WIND_SPEED_2"

.field public static final COL_WIND_SPEED_20:Ljava/lang/String; = "WIND_SPEED_20"

.field public static final COL_WIND_SPEED_21:Ljava/lang/String; = "WIND_SPEED_21"

.field public static final COL_WIND_SPEED_22:Ljava/lang/String; = "WIND_SPEED_22"

.field public static final COL_WIND_SPEED_23:Ljava/lang/String; = "WIND_SPEED_23"

.field public static final COL_WIND_SPEED_24:Ljava/lang/String; = "WIND_SPEED_24"

.field public static final COL_WIND_SPEED_25:Ljava/lang/String; = "WIND_SPEED_25"

.field public static final COL_WIND_SPEED_26:Ljava/lang/String; = "WIND_SPEED_26"

.field public static final COL_WIND_SPEED_27:Ljava/lang/String; = "WIND_SPEED_27"

.field public static final COL_WIND_SPEED_28:Ljava/lang/String; = "WIND_SPEED_28"

.field public static final COL_WIND_SPEED_29:Ljava/lang/String; = "WIND_SPEED_29"

.field public static final COL_WIND_SPEED_3:Ljava/lang/String; = "WIND_SPEED_3"

.field public static final COL_WIND_SPEED_30:Ljava/lang/String; = "WIND_SPEED_30"

.field public static final COL_WIND_SPEED_31:Ljava/lang/String; = "WIND_SPEED_31"

.field public static final COL_WIND_SPEED_32:Ljava/lang/String; = "WIND_SPEED_32"

.field public static final COL_WIND_SPEED_33:Ljava/lang/String; = "WIND_SPEED_33"

.field public static final COL_WIND_SPEED_34:Ljava/lang/String; = "WIND_SPEED_34"

.field public static final COL_WIND_SPEED_35:Ljava/lang/String; = "WIND_SPEED_35"

.field public static final COL_WIND_SPEED_36:Ljava/lang/String; = "WIND_SPEED_36"

.field public static final COL_WIND_SPEED_37:Ljava/lang/String; = "WIND_SPEED_37"

.field public static final COL_WIND_SPEED_38:Ljava/lang/String; = "WIND_SPEED_38"

.field public static final COL_WIND_SPEED_39:Ljava/lang/String; = "WIND_SPEED_39"

.field public static final COL_WIND_SPEED_4:Ljava/lang/String; = "WIND_SPEED_4"

.field public static final COL_WIND_SPEED_40:Ljava/lang/String; = "WIND_SPEED_40"

.field public static final COL_WIND_SPEED_41:Ljava/lang/String; = "WIND_SPEED_41"

.field public static final COL_WIND_SPEED_42:Ljava/lang/String; = "WIND_SPEED_42"

.field public static final COL_WIND_SPEED_43:Ljava/lang/String; = "WIND_SPEED_43"

.field public static final COL_WIND_SPEED_44:Ljava/lang/String; = "WIND_SPEED_44"

.field public static final COL_WIND_SPEED_45:Ljava/lang/String; = "WIND_SPEED_45"

.field public static final COL_WIND_SPEED_46:Ljava/lang/String; = "WIND_SPEED_46"

.field public static final COL_WIND_SPEED_47:Ljava/lang/String; = "WIND_SPEED_47"

.field public static final COL_WIND_SPEED_48:Ljava/lang/String; = "WIND_SPEED_48"

.field public static final COL_WIND_SPEED_5:Ljava/lang/String; = "WIND_SPEED_5"

.field public static final COL_WIND_SPEED_6:Ljava/lang/String; = "WIND_SPEED_6"

.field public static final COL_WIND_SPEED_7:Ljava/lang/String; = "WIND_SPEED_7"

.field public static final COL_WIND_SPEED_8:Ljava/lang/String; = "WIND_SPEED_8"

.field public static final COL_WIND_SPEED_9:Ljava/lang/String; = "WIND_SPEED_9"

.field public static final COL_XML_DETAIL_INFO:Ljava/lang/String; = "XML_DETAIL_INFO"

.field private static final DB_NAME:Ljava/lang/String; = "WeatherClock"

.field public static final WEATHER_INFO_TABLE:Ljava/lang/String; = "MY_WEATHER_INFO"

.field public static final WEATHER_LOG_TABLE:Ljava/lang/String; = "MY_WEATHER_LOG"

.field public static final WEATHER_SETTING_INFO_TABLE:Ljava/lang/String; = "MY_WEATHER_SETTING_INFO"

.field public static final WEATHER_VIEFFECT_TABLE:Ljava/lang/String; = "MY_WEATHER_VIEFFECT"

.field public static sWeatherInfoProjection:[Ljava/lang/String;


# instance fields
.field public final CREATE_WEATHER_VIEFFECT_TABLE:Ljava/lang/String;

.field private context:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 463
    const/16 v0, 0x76

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "NAME"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "STATE"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "LOCATION"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "TIMEZONE"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "SUMMER_TIME"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "LATITUDE"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "LONGITUDE"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "REAL_LOCATION"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "TEMP_SCALE"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "TODAY_DATE"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "TODAY_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "TODAY_HIGH_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "TODAY_LOW_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "TODAY_ICON_NUM"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "TODAY_WIND_DIRECTION"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "TODAY_WIND_SPEED"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "TODAY_WEATHER_TEXT"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "TODAY_WEATHER_URL"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "TODAY_REALFELL"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "TODAY_SUNRISE_TIME"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "TODAY_SUNSET_TIME"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "ONEDAY_HIGH_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "ONEDAY_LOW_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "ONEDAY_ICON_NUM"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "ONEDAY_URL"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "TWODAY_HIGH_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "TWODAY_LOW_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "TWODAY_ICON_NUM"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "TWODAY_URL"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "THREEDAY_HIGH_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "THREEDAY_LOW_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "THREEDAY_ICON_NUM"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "THREEDAY_URL"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "FOURDAY_HIGH_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "FOURDAY_LOW_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "FOURDAY_ICON_NUM"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "FOURDAY_URL"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "FIVEDAY_HIGH_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "FIVEDAY_LOW_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "FIVEDAY_ICON_NUM"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "FIVEDAY_URL"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "SIXDAY_HIGH_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "SIXDAY_LOW_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "SIXDAY_ICON_NUM"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "SIXDAY_URL"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, "UPDATE_DATE"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "TODAY_DAY_RAIN_PROBABILITY"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, "TODAY_DAY_SNOW_PROBABILITY"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "TODAY_DAY_HAIL_PROBABILITY"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, "TODAY_DAY_PRECIPITATION_PROBABILITY"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, "TODAY_DAY_RAIN_AMOUNT"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, "TODAY_DAY_SNOW_AMOUNT"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, "TODAY_DAY_HAIL_AMOUNT"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, "TODAY_DAY_PRECIPITATION_AMOUNT"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, "TODAY_NIGHT_RAIN_PROBABILITY"

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, "TODAY_NIGHT_SNOW_PROBABILITY"

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string v2, "TODAY_NIGHT_HAIL_PROBABILITY"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, "TODAY_NIGHT_PRECIPITATION_PROBABILITY"

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string v2, "TODAY_NIGHT_RAIN_AMOUNT"

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string v2, "TODAY_NIGHT_SNOW_AMOUNT"

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, "TODAY_NIGHT_HAIL_AMOUNT"

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string v2, "TODAY_NIGHT_PRECIPITATION_AMOUNT"

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string v2, "ONEDAY_DAY_RAIN_PROBABILITY"

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string v2, "ONEDAY_DAY_SNOW_PROBABILITY"

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string v2, "ONEDAY_DAY_HAIL_PROBABILITY"

    aput-object v2, v0, v1

    const/16 v1, 0x41

    const-string v2, "ONEDAY_DAY_PRECIPITATION_PROBABILITY"

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string v2, "ONEDAY_DAY_RAIN_AMOUNT"

    aput-object v2, v0, v1

    const/16 v1, 0x43

    const-string v2, "ONEDAY_DAY_SNOW_AMOUNT"

    aput-object v2, v0, v1

    const/16 v1, 0x44

    const-string v2, "ONEDAY_DAY_HAIL_AMOUNT"

    aput-object v2, v0, v1

    const/16 v1, 0x45

    const-string v2, "ONEDAY_DAY_PRECIPITATION_AMOUNT"

    aput-object v2, v0, v1

    const/16 v1, 0x46

    const-string v2, "ONEDAY_NIGHT_RAIN_PROBABILITY"

    aput-object v2, v0, v1

    const/16 v1, 0x47

    const-string v2, "ONEDAY_NIGHT_SNOW_PROBABILITY"

    aput-object v2, v0, v1

    const/16 v1, 0x48

    const-string v2, "ONEDAY_NIGHT_HAIL_PROBABILITY"

    aput-object v2, v0, v1

    const/16 v1, 0x49

    const-string v2, "ONEDAY_NIGHT_PRECIPITATION_PROBABILITY"

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    const-string v2, "ONEDAY_NIGHT_RAIN_AMOUNT"

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    const-string v2, "ONEDAY_NIGHT_SNOW_AMOUNT"

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    const-string v2, "ONEDAY_NIGHT_HAIL_AMOUNT"

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    const-string v2, "ONEDAY_NIGHT_PRECIPITATION_AMOUNT"

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    const-string v2, "TODAY_HUM"

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    const-string v2, "TODAY_UV_INDEX"

    aput-object v2, v0, v1

    const/16 v1, 0x50

    const-string v2, "TODAY_UV_INDEX_TEXT"

    aput-object v2, v0, v1

    const/16 v1, 0x51

    const-string v2, "TODAY_DAY_ICON"

    aput-object v2, v0, v1

    const/16 v1, 0x52

    const-string v2, "TODAY_NIGHT_ICON"

    aput-object v2, v0, v1

    const/16 v1, 0x53

    const-string v2, "ONEDAY_DAY_ICON"

    aput-object v2, v0, v1

    const/16 v1, 0x54

    const-string v2, "ONEDAY_NIGHT_ICON"

    aput-object v2, v0, v1

    const/16 v1, 0x55

    const-string v2, "ONEDAY_SUNRISE_TIME"

    aput-object v2, v0, v1

    const/16 v1, 0x56

    const-string v2, "ONEDAY_SUNSET_TIME"

    aput-object v2, v0, v1

    const/16 v1, 0x57

    const-string v2, "TWODAY_DAY_RAIN_PROBABILITY"

    aput-object v2, v0, v1

    const/16 v1, 0x58

    const-string v2, "TWODAY_DAY_SNOW_PROBABILITY"

    aput-object v2, v0, v1

    const/16 v1, 0x59

    const-string v2, "TWODAY_DAY_HAIL_PROBABILITY"

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    const-string v2, "TWODAY_DAY_PRECIPITATION_PROBABILITY"

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    const-string v2, "TWODAY_SUNRISE_TIME"

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    const-string v2, "TWODAY_SUNSET_TIME"

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    const-string v2, "THREEDAY_DAY_RAIN_PROBABILITY"

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    const-string v2, "THREEDAY_DAY_SNOW_PROBABILITY"

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    const-string v2, "THREEDAY_DAY_HAIL_PROBABILITY"

    aput-object v2, v0, v1

    const/16 v1, 0x60

    const-string v2, "THREEDAY_DAY_PRECIPITATION_PROBABILITY"

    aput-object v2, v0, v1

    const/16 v1, 0x61

    const-string v2, "THREEDAY_SUNRISE_TIME"

    aput-object v2, v0, v1

    const/16 v1, 0x62

    const-string v2, "THREEDAY_SUNSET_TIME"

    aput-object v2, v0, v1

    const/16 v1, 0x63

    const-string v2, "FOURDAY_DAY_RAIN_PROBABILITY"

    aput-object v2, v0, v1

    const/16 v1, 0x64

    const-string v2, "FOURDAY_DAY_SNOW_PROBABILITY"

    aput-object v2, v0, v1

    const/16 v1, 0x65

    const-string v2, "FOURDAY_DAY_HAIL_PROBABILITY"

    aput-object v2, v0, v1

    const/16 v1, 0x66

    const-string v2, "FOURDAY_DAY_PRECIPITATION_PROBABILITY"

    aput-object v2, v0, v1

    const/16 v1, 0x67

    const-string v2, "FOURDAY_SUNRISE_TIME"

    aput-object v2, v0, v1

    const/16 v1, 0x68

    const-string v2, "FOURDAY_SUNSET_TIME"

    aput-object v2, v0, v1

    const/16 v1, 0x69

    const-string v2, "FIVEDAY_DAY_RAIN_PROBABILITY"

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    const-string v2, "FIVEDAY_DAY_SNOW_PROBABILITY"

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    const-string v2, "FIVEDAY_DAY_HAIL_PROBABILITY"

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    const-string v2, "FIVEDAY_DAY_PRECIPITATION_PROBABILITY"

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    const-string v2, "FIVEDAY_SUNRISE_TIME"

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    const-string v2, "FIVEDAY_SUNSET_TIME"

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    const-string v2, "SIXDAY_DAY_RAIN_PROBABILITY"

    aput-object v2, v0, v1

    const/16 v1, 0x70

    const-string v2, "SIXDAY_DAY_SNOW_PROBABILITY"

    aput-object v2, v0, v1

    const/16 v1, 0x71

    const-string v2, "SIXDAY_DAY_HAIL_PROBABILITY"

    aput-object v2, v0, v1

    const/16 v1, 0x72

    const-string v2, "SIXDAY_DAY_PRECIPITATION_PROBABILITY"

    aput-object v2, v0, v1

    const/16 v1, 0x73

    const-string v2, "SIXDAY_SUNRISE_TIME"

    aput-object v2, v0, v1

    const/16 v1, 0x74

    const-string v2, "SIXDAY_SUNSET_TIME"

    aput-object v2, v0, v1

    const/16 v1, 0x75

    const-string v2, "WEATHER_ICON_NUM"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/OpenHelper;->sWeatherInfoProjection:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "ver"    # I

    .prologue
    .line 727
    const-string v0, "WeatherClock"

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1, p2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 420
    const-string v0, "create table MY_WEATHER_VIEFFECT (EFFECT_1 integer,EFFECT_2 integer,EFFECT_3 integer,EFFECT_4 integer,EFFECT_5 integer,EFFECT_6 integer,EFFECT_7 integer,EFFECT_8 integer,EFFECT_9 integer,EFFECT_10 integer,EFFECT_11 integer,EFFECT_12 integer,EFFECT_13 integer,EFFECT_14 integer,EFFECT_15 integer,EFFECT_16 integer,EFFECT_17 integer,EFFECT_18 integer,EFFECT_19 integer,EFFECT_20 integer,EFFECT_21 integer,EFFECT_22 integer,EFFECT_23 integer,EFFECT_24 integer,EFFECT_25 integer,EFFECT_26 integer,EFFECT_27 integer,EFFECT_28 integer,EFFECT_29 integer,EFFECT_30 integer,EFFECT_31 integer,EFFECT_32 integer,EFFECT_33 integer,EFFECT_34 integer,EFFECT_35 integer,EFFECT_36 integer,EFFECT_37 integer,EFFECT_38 integer,EFFECT_39 integer,EFFECT_40 integer);"

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/OpenHelper;->CREATE_WEATHER_VIEFFECT_TABLE:Ljava/lang/String;

    .line 728
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/OpenHelper;->context:Landroid/content/Context;

    .line 729
    invoke-static {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->setContext(Landroid/content/Context;)V

    .line 730
    return-void
.end method

.method private getCurrentAutoRefreshIndex(Landroid/database/sqlite/SQLiteDatabase;)I
    .locals 10
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v3, 0x0

    .line 834
    const/4 v8, 0x0

    .line 835
    .local v8, "currentAutoRefreshIdx":I
    const-string v1, "MY_WEATHER_SETTING_INFO"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v4, "AUTO_REFRESH_TIME"

    aput-object v4, v2, v0

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 838
    .local v9, "cursor":Landroid/database/Cursor;
    if-eqz v9, :cond_1

    .line 839
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 840
    const-string v0, "AUTO_REFRESH_TIME"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 842
    :cond_0
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 844
    :cond_1
    return v8
.end method

.method public static getDefaultAutoRefreshIndex(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 848
    const/4 v0, 0x3

    .line 849
    .local v0, "index":I
    sget-boolean v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Constants;->USE_THREE_HOURS_AUTO_REFRESH:Z

    if-nez v1, :cond_0

    .line 850
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 851
    :cond_0
    const/4 v0, 0x2

    .line 853
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-nez v1, :cond_2

    sget-boolean v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isVerizon:Z

    if-eqz v1, :cond_3

    .line 855
    :cond_2
    const/4 v0, 0x3

    .line 857
    :cond_3
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090003

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-nez v1, :cond_4

    sget-boolean v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isMetroPCS:Z

    if-eqz v1, :cond_5

    .line 859
    :cond_4
    const/4 v0, 0x5

    .line 861
    :cond_5
    sget-boolean v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isATT:Z

    if-eqz v1, :cond_6

    .line 862
    const/4 v0, 0x0

    .line 864
    :cond_6
    return v0
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 5
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 733
    const-string v3, ""

    const-string v4, "db helper on create"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 734
    if-eqz p1, :cond_1

    .line 735
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 737
    :try_start_0
    const-string v3, "SQL excuted..."

    const-string v4, "DB------>onCreate()"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 738
    const-string v3, "create table MY_WEATHER_VIEFFECT (EFFECT_1 integer,EFFECT_2 integer,EFFECT_3 integer,EFFECT_4 integer,EFFECT_5 integer,EFFECT_6 integer,EFFECT_7 integer,EFFECT_8 integer,EFFECT_9 integer,EFFECT_10 integer,EFFECT_11 integer,EFFECT_12 integer,EFFECT_13 integer,EFFECT_14 integer,EFFECT_15 integer,EFFECT_16 integer,EFFECT_17 integer,EFFECT_18 integer,EFFECT_19 integer,EFFECT_20 integer,EFFECT_21 integer,EFFECT_22 integer,EFFECT_23 integer,EFFECT_24 integer,EFFECT_25 integer,EFFECT_26 integer,EFFECT_27 integer,EFFECT_28 integer,EFFECT_29 integer,EFFECT_30 integer,EFFECT_31 integer,EFFECT_32 integer,EFFECT_33 integer,EFFECT_34 integer,EFFECT_35 integer,EFFECT_36 integer,EFFECT_37 integer,EFFECT_38 integer,EFFECT_39 integer,EFFECT_40 integer);"

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 740
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/OpenHelper;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070002

    .line 741
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v2

    .line 744
    .local v2, "weatherViEffect":[I
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getProductName()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    .line 745
    const-string v3, ""

    const-string v4, "fail get product name"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 756
    :cond_0
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "insert into MY_WEATHER_VIEFFECT values ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x0

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x1

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x2

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x3

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x4

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x5

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x6

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x7

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x8

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x9

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0xa

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0xb

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0xc

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0xd

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0xe

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0xf

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x10

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x11

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x12

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x13

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x14

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x15

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x16

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x17

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x18

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x19

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x1a

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x1b

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x1c

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x1d

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x1e

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x1f

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x20

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x21

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x22

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x23

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x24

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x25

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x26

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x27

    aget v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ");"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 799
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 801
    const-string v3, "SQL excuted...create table MY_WEATHER_VIEFFECT (EFFECT_1 integer,EFFECT_2 integer,EFFECT_3 integer,EFFECT_4 integer,EFFECT_5 integer,EFFECT_6 integer,EFFECT_7 integer,EFFECT_8 integer,EFFECT_9 integer,EFFECT_10 integer,EFFECT_11 integer,EFFECT_12 integer,EFFECT_13 integer,EFFECT_14 integer,EFFECT_15 integer,EFFECT_16 integer,EFFECT_17 integer,EFFECT_18 integer,EFFECT_19 integer,EFFECT_20 integer,EFFECT_21 integer,EFFECT_22 integer,EFFECT_23 integer,EFFECT_24 integer,EFFECT_25 integer,EFFECT_26 integer,EFFECT_27 integer,EFFECT_28 integer,EFFECT_29 integer,EFFECT_30 integer,EFFECT_31 integer,EFFECT_32 integer,EFFECT_33 integer,EFFECT_34 integer,EFFECT_35 integer,EFFECT_36 integer,EFFECT_37 integer,EFFECT_38 integer,EFFECT_39 integer,EFFECT_40 integer);"

    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 807
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 810
    .end local v2    # "weatherViEffect":[I
    :cond_1
    :goto_1
    return-void

    .line 747
    .restart local v2    # "weatherViEffect":[I
    :cond_2
    :try_start_1
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getProductName()Ljava/lang/String;

    move-result-object v1

    .line 748
    .local v1, "productName":Ljava/lang/String;
    const-string v3, "slte"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 751
    const/16 v3, 0xc

    const/16 v4, 0x64

    aput v4, v2, v3
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 803
    .end local v1    # "productName":Ljava/lang/String;
    .end local v2    # "weatherViEffect":[I
    :catch_0
    move-exception v0

    .line 805
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DbHelper.onCreate()"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 807
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_1

    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3
.end method

.method public onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 829
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DbHelper.onDowngrade() : rDB = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V

    .line 830
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/OpenHelper;->onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 831
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 813
    const-string v1, "DbHelper.onUpgrade() : rDB = "

    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V

    .line 814
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 816
    :try_start_0
    const-string v1, "DROP TABLE IF EXISTS MY_WEATHER_VIEFFECT"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 817
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 818
    const-string v1, "SQL excuted...DROP TABLE IF EXISTS MY_WEATHER_VIEFFECT"

    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 823
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 824
    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/OpenHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 826
    :goto_0
    return-void

    .line 819
    :catch_0
    move-exception v0

    .line 821
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DbHelper.onUpgrade()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 823
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 824
    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/OpenHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 823
    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 824
    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/OpenHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    throw v1
.end method
