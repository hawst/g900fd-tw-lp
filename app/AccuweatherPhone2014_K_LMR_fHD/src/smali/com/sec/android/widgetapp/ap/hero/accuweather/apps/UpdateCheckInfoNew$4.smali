.class Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew$4;
.super Ljava/lang/Object;
.source "UpdateCheckInfoNew.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew;->startUserDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew;

    .prologue
    .line 530
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    const/16 v2, 0xa

    const/4 v3, 0x4

    .line 533
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew;->getButtenTypeCheck()I

    move-result v1

    if-ne v1, v2, :cond_1

    .line 534
    const-string v1, ""

    const-string v2, "on dismiss"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 535
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew;->ctx:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "config"

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 536
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "CONFIRM_ASKGAGIN"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 537
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 543
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    :goto_0
    return-void

    .line 538
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew;->getButtenTypeCheck()I

    move-result v1

    if-eq v1, v2, :cond_0

    .line 539
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew;->ctx:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfoNew;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "config"

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 540
    .restart local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "Apps_Date"

    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getTimestamp()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 541
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method
