.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;
.super Landroid/view/View;
.source "RainIconView.java"

# interfaces
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;


# instance fields
.field private CloudTranslate:Landroid/animation/ValueAnimator;

.field private DropCenterTy1:Landroid/animation/ValueAnimator;

.field private DropCenterTy2:Landroid/animation/ValueAnimator;

.field private DropCenterTy3:Landroid/animation/ValueAnimator;

.field private DropCenterTy4:Landroid/animation/ValueAnimator;

.field private DropCenterTy5:Landroid/animation/ValueAnimator;

.field private IconTranslateX:Landroid/animation/ValueAnimator;

.field private IconTranslateY:Landroid/animation/ValueAnimator;

.field isStop:Z

.field private l:Landroid/animation/Animator$AnimatorListener;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mCloudLeft:Landroid/graphics/Path;

.field private mCloudRight:Landroid/graphics/Path;

.field private mCloudTop:Landroid/graphics/Path;

.field private mIsActiveAnimationThread:Z

.field private mMasking:Landroid/graphics/Path;

.field private mPaint:Landroid/graphics/Paint;

.field private mPaintColor:I

.field public mPreFixProperty1:Ljava/lang/String;

.field public mPreFixProperty2:Ljava/lang/String;

.field public mPreFixProperty3:Ljava/lang/String;

.field public mPreFixProperty4:Ljava/lang/String;

.field public mPreFixProperty5:Ljava/lang/String;

.field private mRainLine1:Landroid/graphics/Path;

.field private mRainLine2:Landroid/graphics/Path;

.field private mRainLine3:Landroid/graphics/Path;

.field private mRainLine4:Landroid/graphics/Path;

.field private mRainLine5:Landroid/graphics/Path;

.field private mRainLine6:Landroid/graphics/Path;

.field private mScale:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 91
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 92
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 87
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 88
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 82
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    .line 30
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->IconTranslateX:Landroid/animation/ValueAnimator;

    .line 32
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->IconTranslateY:Landroid/animation/ValueAnimator;

    .line 34
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy1:Landroid/animation/ValueAnimator;

    .line 36
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy2:Landroid/animation/ValueAnimator;

    .line 38
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy3:Landroid/animation/ValueAnimator;

    .line 40
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy4:Landroid/animation/ValueAnimator;

    .line 42
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy5:Landroid/animation/ValueAnimator;

    .line 44
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mIsActiveAnimationThread:Z

    .line 46
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPaint:Landroid/graphics/Paint;

    .line 48
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mMasking:Landroid/graphics/Path;

    .line 50
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudLeft:Landroid/graphics/Path;

    .line 52
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudRight:Landroid/graphics/Path;

    .line 54
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudTop:Landroid/graphics/Path;

    .line 56
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine1:Landroid/graphics/Path;

    .line 58
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine2:Landroid/graphics/Path;

    .line 60
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine3:Landroid/graphics/Path;

    .line 62
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine4:Landroid/graphics/Path;

    .line 64
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine5:Landroid/graphics/Path;

    .line 66
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine6:Landroid/graphics/Path;

    .line 68
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    .line 71
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mBitmap:Landroid/graphics/Bitmap;

    .line 75
    const v0, -0xa0a0b

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPaintColor:I

    .line 567
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPreFixProperty1:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPreFixProperty2:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPreFixProperty3:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPreFixProperty4:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPreFixProperty5:Ljava/lang/String;

    .line 573
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->l:Landroid/animation/Animator$AnimatorListener;

    .line 743
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->isStop:Z

    .line 83
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->init()V

    .line 84
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy1:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy2:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy3:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy4:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy5:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->IconTranslateX:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->IconTranslateY:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;)Landroid/animation/ValueAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mIsActiveAnimationThread:Z

    return v0
.end method

.method private drawCloud(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "paint"    # Landroid/graphics/Paint;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 383
    const/4 v0, 0x0

    .line 384
    .local v0, "dx":F
    iget-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mIsActiveAnimationThread:Z

    if-eqz v1, :cond_0

    .line 385
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 387
    :cond_0
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPaintColor:I

    invoke-virtual {p2, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 389
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 390
    neg-float v1, v0

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 391
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudLeft:Landroid/graphics/Path;

    invoke-virtual {p1, v1, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 392
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 394
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 395
    neg-float v1, v0

    invoke-virtual {p1, v2, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 396
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudTop:Landroid/graphics/Path;

    invoke-virtual {p1, v1, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 397
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 399
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 400
    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 401
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudRight:Landroid/graphics/Path;

    invoke-virtual {p1, v1, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 402
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 403
    return-void
.end method

.method private drawDrop(Landroid/graphics/Canvas;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/high16 v11, -0x3d900000    # -60.0f

    const/4 v10, 0x0

    .line 456
    const/4 v2, 0x0

    .line 457
    .local v2, "dcentery1":F
    const/4 v4, 0x0

    .line 458
    .local v4, "dcentery3":F
    const/4 v3, 0x0

    .line 459
    .local v3, "dcentery2":F
    const/4 v0, 0x0

    .line 460
    .local v0, "dcenterx4":F
    const/4 v5, 0x0

    .line 461
    .local v5, "dcentery4":F
    const/4 v1, 0x0

    .line 462
    .local v1, "dcenterx5":F
    const/4 v6, 0x0

    .line 464
    .local v6, "dcentery5":F
    iget-boolean v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mIsActiveAnimationThread:Z

    if-eqz v7, :cond_4

    .line 465
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy1:Landroid/animation/ValueAnimator;

    invoke-virtual {v7}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 466
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy1:Landroid/animation/ValueAnimator;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPreFixProperty1:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "y1"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v2

    .line 473
    :cond_0
    :goto_0
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy2:Landroid/animation/ValueAnimator;

    invoke-virtual {v7}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 474
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy2:Landroid/animation/ValueAnimator;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPreFixProperty2:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "y2"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v3

    .line 481
    :cond_1
    :goto_1
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy3:Landroid/animation/ValueAnimator;

    invoke-virtual {v7}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 482
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy3:Landroid/animation/ValueAnimator;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPreFixProperty3:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "y3"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v4

    .line 489
    :cond_2
    :goto_2
    const/high16 v7, 0x42340000    # 45.0f

    iget v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float v0, v7, v8

    .line 490
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy4:Landroid/animation/ValueAnimator;

    invoke-virtual {v7}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v7

    if-eqz v7, :cond_8

    .line 491
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy4:Landroid/animation/ValueAnimator;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPreFixProperty4:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "y4"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v5

    .line 498
    :cond_3
    :goto_3
    const/high16 v7, 0x41c80000    # 25.0f

    iget v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float v1, v7, v8

    .line 499
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy5:Landroid/animation/ValueAnimator;

    invoke-virtual {v7}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v7

    if-eqz v7, :cond_9

    .line 500
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy5:Landroid/animation/ValueAnimator;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPreFixProperty5:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "y5"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v6

    .line 508
    :cond_4
    :goto_4
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 509
    invoke-virtual {p1, v10, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 510
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine1:Landroid/graphics/Path;

    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v8}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 511
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 513
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 514
    invoke-virtual {p1, v10, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 515
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine2:Landroid/graphics/Path;

    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v8}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 516
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 518
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 519
    invoke-virtual {p1, v10, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 520
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine3:Landroid/graphics/Path;

    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v8}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 521
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 523
    iget-boolean v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mIsActiveAnimationThread:Z

    if-eqz v7, :cond_b

    .line 524
    iget-boolean v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->isStop:Z

    if-nez v7, :cond_a

    .line 525
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 526
    invoke-virtual {p1, v0, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 527
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine2:Landroid/graphics/Path;

    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v8}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 528
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 530
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 531
    invoke-virtual {p1, v1, v6}, Landroid/graphics/Canvas;->translate(FF)V

    .line 532
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine2:Landroid/graphics/Path;

    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v8}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 533
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 566
    :goto_5
    return-void

    .line 468
    :cond_5
    iget-boolean v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->isStop:Z

    if-nez v7, :cond_0

    .line 469
    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float v2, v11, v7

    goto/16 :goto_0

    .line 476
    :cond_6
    iget-boolean v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->isStop:Z

    if-nez v7, :cond_1

    .line 477
    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float v3, v11, v7

    goto/16 :goto_1

    .line 484
    :cond_7
    iget-boolean v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->isStop:Z

    if-nez v7, :cond_2

    .line 485
    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float v4, v11, v7

    goto/16 :goto_2

    .line 493
    :cond_8
    iget-boolean v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->isStop:Z

    if-nez v7, :cond_3

    .line 494
    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float v5, v11, v7

    goto/16 :goto_3

    .line 502
    :cond_9
    iget-boolean v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->isStop:Z

    if-nez v7, :cond_4

    .line 503
    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float v6, v11, v7

    goto/16 :goto_4

    .line 535
    :cond_a
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 536
    invoke-virtual {p1, v10, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 537
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine4:Landroid/graphics/Path;

    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v8}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 538
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 540
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 541
    invoke-virtual {p1, v10, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 542
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine5:Landroid/graphics/Path;

    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v8}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 543
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 545
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 546
    invoke-virtual {p1, v10, v6}, Landroid/graphics/Canvas;->translate(FF)V

    .line 547
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine6:Landroid/graphics/Path;

    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v8}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 548
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_5

    .line 551
    :cond_b
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 552
    invoke-virtual {p1, v10, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 553
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine4:Landroid/graphics/Path;

    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v8}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 554
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 556
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 557
    invoke-virtual {p1, v1, v6}, Landroid/graphics/Canvas;->translate(FF)V

    .line 558
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine5:Landroid/graphics/Path;

    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v8}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 559
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 561
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 562
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine6:Landroid/graphics/Path;

    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v8}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 563
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto/16 :goto_5
.end method

.method private drawTransparentArea(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "paint"    # Landroid/graphics/Paint;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 377
    new-instance v0, Landroid/graphics/PorterDuffXfermode;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v1}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 378
    invoke-virtual {p1, p2, v2, v2, p3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 379
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 380
    return-void
.end method

.method private init()V
    .locals 4

    .prologue
    const/high16 v3, 0x43960000    # 300.0f

    .line 95
    invoke-virtual {p0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 96
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPaint:Landroid/graphics/Paint;

    .line 97
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->initPath()V

    .line 98
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPaintColor:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 99
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 100
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 101
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x40a00000    # 5.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 103
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mBitmap:Landroid/graphics/Bitmap;

    .line 105
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 106
    .local v0, "mMaskingCanvas":Landroid/graphics/Canvas;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mMasking:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 107
    return-void
.end method

.method private initPath()V
    .locals 13

    .prologue
    const v12, 0x42f6a094

    const v11, 0x43155009

    const v10, 0x429949c3

    const v9, 0x428949c3

    const v8, 0x42f30b96

    .line 110
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mMasking:Landroid/graphics/Path;

    .line 111
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x431d0ccd    # 157.05f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42608b7b

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 112
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x43147c29    # 148.485f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42226079

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4303a979

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x41f71902

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42e3d917

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x41fa1902

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 113
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x42da45a2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41fcd56c    # 31.60421f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42d0e666    # 104.45f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42009ad7

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42c7bb64

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4206ac3f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 114
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x42c005a2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x420bc6df

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42b8a45a    # 92.321f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4212dc60

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42b1d1ec    # 88.91f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x421bae4c

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 115
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x42a41eb8    # 82.06f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x422d68aa

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42988dd3    # 76.277f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42463577

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42911d2f    # 72.557f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42635016

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x428e4396

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x426e74f3

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x428a9810

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4280bd8c

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42893958    # 68.612f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4286cdee

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 117
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x42893958    # 68.612f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4286cdee

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42832f9e

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4285adad

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42723021    # 60.547f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4285adad

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 118
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x425e0106    # 55.501f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4285adad

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x424424dd    # 49.036f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x428ac53a

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4235072b    # 45.257f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4293801c

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 119
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x42247efa    # 41.124f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x429d0be2

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x421df5c3    # 39.49f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42aa3455

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4223947b    # 40.895f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42b68434

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 120
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x4202d1ec

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42b51a38

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x41cac083    # 25.344f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42c1e994

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x41c84396    # 25.033f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42d26788

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 121
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x41c5ced9    # 24.726f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42e2b34f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x41fb7cee    # 31.436f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42f0a994

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x421e5f3b

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42f0a994

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 122
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x43264831

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42f0a994

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 123
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x4326a7f0

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42f0a994

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4327076d

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42f0a681

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x432766e9

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42f09ed3

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 124
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x433053b6

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42efef36

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4338bb64

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42e6b455

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x433dd646

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42d80e72

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 125
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x43430937

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42c92476

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43446148    # 196.38f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42b5aa9b

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4341676d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42a47b80

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x433e820c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4293c3b1

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4337b646

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42865a38

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x432f5062

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4280c953

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 127
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x432a5df4

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x427b0349

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43251439

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x427a42c6

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43200f9e

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x427ea721

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 128
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x43200fdf

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x427ea721

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431f0419

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42705b5a

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431d0ccd    # 157.05f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42608b7b

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 131
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudTop:Landroid/graphics/Path;

    .line 132
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudTop:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 133
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x42733646

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x427eef9e

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 134
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x426f5a1d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42883efa

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x426d52f2

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42915b23

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x426d52f2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x429aac8b

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 137
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x426d52f2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x429f12f2

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42747df4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42a2a24e

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x427d52f2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42a2a24e

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 140
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x4283147b    # 65.54f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a2a24e

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4286a979

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x429f10e5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4286a979

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x429aaa7f    # 77.333f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 143
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x4286a979

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42862c8b

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x428c0f5c    # 70.03f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4267926f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42957a5e

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42492b02    # 50.292f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 146
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x42a15d2f    # 80.682f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42241581    # 41.021f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42b3820c

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4207ac08    # 33.918f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42c8f021

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x41f3b852    # 30.465f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 149
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x42d18831

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41e90419    # 29.127f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42dbb958    # 109.862f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x41e124dd    # 28.143f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42e6a2d1    # 115.318f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x41e124dd    # 28.143f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 152
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x4304bf3b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41e124dd    # 28.143f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431549ba

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4218c6a8    # 38.194f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431d72f2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4253f7cf    # 52.992f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 155
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x431d6000    # 157.375f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42540b44

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431efd2f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4262b74c    # 56.679f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431eea3d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4262cac1

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 158
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x43277e35

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x425e51ec    # 55.58f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 159
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x4325c042

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x424ee873

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 160
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x431c79db

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4202c083    # 32.688f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4308f687

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x41a1d70a    # 20.23f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42e6a2d1    # 115.318f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x41a1d70a    # 20.23f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 163
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x42e11810

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41a1d70a    # 20.23f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42db92f2

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x41a3645a    # 20.424f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42d61c29    # 107.055f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x41a6a5e3    # 20.831f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 166
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x42bcfbe7

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41b42d0e    # 22.522f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42a67333

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x41de70a4    # 27.805f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42954419

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x421425e3

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 169
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x428d2d0e

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4225820c

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42869b23

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x423954fe    # 46.333f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4281a24e

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x424efcee

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 172
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x42805b23

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42543f7d    # 53.062f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x427c0419

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x425ef8d5    # 55.743f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const/high16 v5, 0x427c0000    # 63.0f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x425f051f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 175
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x4277ee98

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x426b6560    # 58.849f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42733646

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x427eef9e

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42733646

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x427eef9e

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 178
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudTop:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 180
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudLeft:Landroid/graphics/Path;

    .line 181
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudLeft:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 182
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x421f9fbe

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42ea8937

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 183
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x41fee148    # 31.86f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42ea8937

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x41cab021    # 25.336f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42dd25e3

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x41cab021    # 25.336f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42cd2042

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 186
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x41cab021    # 25.336f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42bd1ba6    # 94.554f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x41fee148    # 31.86f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42afeb02    # 87.959f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x421f9fbe

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42afeb02    # 87.959f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 189
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x42216148

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42afeb02    # 87.959f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42231db2    # 40.779f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42afdf3b

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4224d604    # 41.209f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42aff333

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 192
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x4223872b    # 40.882f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42ad199a    # 86.55f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4222d1ec

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42aa178d

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4222d1ec

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42a70ac1

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 195
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x4222d1ec

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4291178d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42469168

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x427e872b    # 63.632f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4272ae14    # 60.67f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x427e872b    # 63.632f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 198
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x4272e354    # 60.722f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x427e872b    # 63.632f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4282e148    # 65.44f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42805f3b

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4282e148    # 65.44f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42805f3b

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 201
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x4286ad0e

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42606f9e

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 202
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x42880ccd

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42606042

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4275ced9    # 61.452f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x425ea1cb    # 55.658f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4272af1b

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x425ea1cb    # 55.658f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 205
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x4238570a    # 46.085f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x425ea1cb    # 55.658f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42084ac1

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4285a8f6    # 66.83f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42034396

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42a21062

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 208
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x41beae14    # 23.835f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a7fdf4

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x418ab439    # 17.338f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42b946a8    # 92.638f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x418ab439    # 17.338f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42cd1fbe

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 211
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x418ab439    # 17.338f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42e5e76d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x41dbb22d    # 27.462f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42fa774c

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x421fa1cb    # 39.908f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42fa774c

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 214
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x431c374c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42fa774c

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 215
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x431c374c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42ea88b4

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 216
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x421f9fbe

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42ea88b4

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 217
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudLeft:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 219
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudRight:Landroid/graphics/Path;

    .line 220
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudRight:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 221
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x43267ba6

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x424f20c5

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 222
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x43263687

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x424f20c5

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4321d5c3

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x424f51ec    # 51.83f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431d3aa0

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42543646

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 225
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x43140b02    # 148.043f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x425df6c9

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x430bfc6a

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4274cbc7

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4306bae1    # 134.73f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x428ae3d7    # 69.445f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 228
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x43058b44

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x428e9aa0

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43061852

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4293872b    # 73.764f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4307f5c3    # 135.96f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4295e24e

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 231
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x4308a083

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4296b9db

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43095df4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x429720c5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x430a19db

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x429720c5

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 234
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x430b6ccd

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x429720c5

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x430cb810

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4295d168

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x430d7be7

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42936d91

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 237
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x43130c08

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4281f852    # 64.985f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431b5efa

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x426efae1

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43273e35

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x426efae1

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 240
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x4336cfdf

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42704396

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4343553f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4291d687

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4343553f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42b0fe77

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 243
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x4343553f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42d08bc7

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43367cee

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42ea8937

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4326a354    # 166.638f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42ea8937

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 246
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x42775a1d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42ea8937

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 247
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x42775a1d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42fa77cf

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 248
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x4326a354    # 166.638f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42fa77cf

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 249
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x4326a354    # 166.638f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42fa774c

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 250
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x433adf3b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42fa774c

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x434b54bc

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42d949ba

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x434b54bc

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42b0fefa

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 253
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x434b54bc

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4288b439

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x433ab78d

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x424f20c5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43267ba6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x424f20c5

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 256
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudRight:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 258
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine1:Landroid/graphics/Path;

    .line 259
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine1:Landroid/graphics/Path;

    const v1, 0x429149c3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43115009

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 260
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine1:Landroid/graphics/Path;

    const v1, 0x428cdec1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43115009

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v9

    const v4, 0x43131a87

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v9

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v11

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 263
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine1:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v9

    const v2, 0x432d5009

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 264
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine1:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v9

    const v2, 0x432f858a

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x428cdec1

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43315009

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x429149c3

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43315009

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 266
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine1:Landroid/graphics/Path;

    const v1, 0x4295b4c5

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43315009

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v10

    const v4, 0x432f858a

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v10

    const v6, 0x432d5009

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 268
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine1:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v10

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v11

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 269
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine1:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v10

    const v2, 0x43131a87

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4295b4c5

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43115009

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x429149c3

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43115009

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 272
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine2:Landroid/graphics/Path;

    .line 274
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine2:Landroid/graphics/Path;

    const v1, 0x42c749c3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 275
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine2:Landroid/graphics/Path;

    const v1, 0x42c2dec1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v8

    const v3, 0x42bf49c3

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v12

    const v5, 0x42bf49c3

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42fb0b96

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 277
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine2:Landroid/graphics/Path;

    const v1, 0x42bf49c3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42fd0b96

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 278
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine2:Landroid/graphics/Path;

    const v1, 0x42bf49c3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43085009

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 279
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine2:Landroid/graphics/Path;

    const v1, 0x42bf49c3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x431d5009

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 280
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine2:Landroid/graphics/Path;

    const v1, 0x42bf49c3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x431f858a

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42c2dec1

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43215009

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42c749c3

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43215009

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 282
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine2:Landroid/graphics/Path;

    const v1, 0x42cbb4c5

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43215009

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42cf49c3

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x431f858a

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42cf49c3

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x431d5009

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 284
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine2:Landroid/graphics/Path;

    const v1, 0x42cf49c3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43085009

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 285
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine2:Landroid/graphics/Path;

    const v1, 0x42cf49c3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42fd0b96

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 286
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine2:Landroid/graphics/Path;

    const v1, 0x42cf49c3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42fb0b96

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 287
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine2:Landroid/graphics/Path;

    const v1, 0x42cf49c3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v12

    const v3, 0x42cbb4c5

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v8

    const v5, 0x42c749c3

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v8

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 290
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine3:Landroid/graphics/Path;

    .line 292
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine3:Landroid/graphics/Path;

    const v1, 0x42f549c3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 293
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine3:Landroid/graphics/Path;

    const v1, 0x42f0dec1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v8

    const v3, 0x42ed49c3

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v12

    const v5, 0x42ed49c3

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42fb0b96

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 296
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine3:Landroid/graphics/Path;

    const v1, 0x42ed49c3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42fd0b96

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 297
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine3:Landroid/graphics/Path;

    const v1, 0x42ed49c3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43085009

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 298
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine3:Landroid/graphics/Path;

    const v1, 0x42ed49c3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43115009

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 299
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine3:Landroid/graphics/Path;

    const v1, 0x42ed49c3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4313858a

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42f0dec1

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v11

    const v5, 0x42f549c3

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v11

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 302
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine3:Landroid/graphics/Path;

    const v1, 0x42f9b4c5

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v11

    const v3, 0x42fd49c3

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4313858a

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42fd49c3

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43115009

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 305
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine3:Landroid/graphics/Path;

    const v1, 0x42fd49c3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43085009

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 306
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine3:Landroid/graphics/Path;

    const v1, 0x42fd49c3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42fd0b96

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 307
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine3:Landroid/graphics/Path;

    const v1, 0x42fd49c3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42fb0b96

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 308
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine3:Landroid/graphics/Path;

    const v1, 0x42fd49c3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v12

    const v3, 0x42f9b4c5

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v8

    const v5, 0x42f549c3

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v8

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 312
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine4:Landroid/graphics/Path;

    .line 314
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine4:Landroid/graphics/Path;

    const v1, 0x4315a4e2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 315
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine4:Landroid/graphics/Path;

    const v1, 0x43136f61

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v8

    const v3, 0x4311a4e2

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v12

    const v5, 0x4311a4e2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42fb0b96

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 318
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine4:Landroid/graphics/Path;

    const v1, 0x4311a4e2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42fd0b96

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 319
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine4:Landroid/graphics/Path;

    const v1, 0x4311a4e2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43085009

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 320
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine4:Landroid/graphics/Path;

    const v1, 0x4311a4e2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x430c5009

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 321
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine4:Landroid/graphics/Path;

    const v1, 0x4311a4e2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x430e858a

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43136f61

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43105009

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4315a4e2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43105009

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 324
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine4:Landroid/graphics/Path;

    const v1, 0x4317da63

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43105009

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4319a4e2

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x430e858a

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4319a4e2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x430c5009

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 327
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine4:Landroid/graphics/Path;

    const v1, 0x4319a4e2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43085009

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 328
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine4:Landroid/graphics/Path;

    const v1, 0x4319a4e2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42fd0b96

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 329
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine4:Landroid/graphics/Path;

    const v1, 0x4319a4e2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42fb0b96

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 330
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine4:Landroid/graphics/Path;

    const v1, 0x4319a4e2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v12

    const v3, 0x4317da63

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v8

    const v5, 0x4315a4e2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v8

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 334
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine5:Landroid/graphics/Path;

    .line 336
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine5:Landroid/graphics/Path;

    const v1, 0x4314a4e2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v11

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 337
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine5:Landroid/graphics/Path;

    const v1, 0x43126f61

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v11

    const v3, 0x4310a4e2

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43171a87

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4310a4e2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43195009

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 340
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine5:Landroid/graphics/Path;

    const v1, 0x4310a4e2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43315009

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 341
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine5:Landroid/graphics/Path;

    const v1, 0x4310a4e2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4333858a

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43126f61

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43355009

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4314a4e2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43355009

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 344
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine5:Landroid/graphics/Path;

    const v1, 0x4316da63

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43355009

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4318a4e2

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4333858a

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4318a4e2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43315009

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 347
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine5:Landroid/graphics/Path;

    const v1, 0x4318a4e2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43195009

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 348
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine5:Landroid/graphics/Path;

    const v1, 0x4318a4e2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43171a87

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4316da63

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v11

    const v5, 0x4314a4e2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v11

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 352
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine6:Landroid/graphics/Path;

    .line 354
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine6:Landroid/graphics/Path;

    const v1, 0x429149c3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 355
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine6:Landroid/graphics/Path;

    const v1, 0x428cdec1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v8

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v9

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v12

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v9

    const v6, 0x42fb0b96

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 358
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine6:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v9

    const v2, 0x42fd0b96

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 359
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine6:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v9

    const v2, 0x43085009

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 360
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine6:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v9

    const v2, 0x43095009

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 361
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine6:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v9

    const v2, 0x430b858a

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x428cdec1

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x430d5009

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x429149c3

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x430d5009

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 364
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine6:Landroid/graphics/Path;

    const v1, 0x4295b4c5

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x430d5009

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v10

    const v4, 0x430b858a

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v10

    const v6, 0x43095009

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 367
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine6:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v10

    const v2, 0x43085009

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 368
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine6:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v10

    const v2, 0x42fd0b96

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 369
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine6:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v10

    const v2, 0x42fb0b96

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 370
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine6:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v10

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v12

    const v3, 0x4295b4c5

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v8

    const v5, 0x429149c3

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v6, v8

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 374
    return-void
.end method

.method private makeAnimation()V
    .locals 14

    .prologue
    const/4 v13, -0x1

    const/high16 v12, -0x3d900000    # -60.0f

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 611
    iput-boolean v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->isStop:Z

    .line 612
    const-wide/16 v2, 0x320

    .line 613
    .local v2, "duration":J
    const-wide/16 v4, 0x4

    div-long v0, v2, v4

    .line 615
    .local v0, "delayGap":J
    const-string v4, ""

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPreFixProperty1:Ljava/lang/String;

    .line 616
    const-string v4, ""

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPreFixProperty2:Ljava/lang/String;

    .line 617
    const-string v4, ""

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPreFixProperty3:Ljava/lang/String;

    .line 618
    const-string v4, ""

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPreFixProperty4:Ljava/lang/String;

    .line 619
    const-string v4, ""

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPreFixProperty5:Ljava/lang/String;

    .line 621
    new-array v4, v11, [Landroid/animation/PropertyValuesHolder;

    const-string v5, "y1"

    new-array v6, v11, [F

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v7, v12

    aput v7, v6, v9

    const/high16 v7, 0x42c80000    # 100.0f

    iget v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v7, v8

    aput v7, v6, v10

    .line 622
    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    aput-object v5, v4, v9

    const-string v5, "e_y1"

    new-array v6, v11, [F

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v7, v12

    aput v7, v6, v9

    const/4 v7, 0x0

    aput v7, v6, v10

    .line 623
    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    aput-object v5, v4, v10

    .line 621
    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy1:Landroid/animation/ValueAnimator;

    .line 624
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy1:Landroid/animation/ValueAnimator;

    invoke-virtual {v4, v13}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 625
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy1:Landroid/animation/ValueAnimator;

    invoke-virtual {v4, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 626
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy1:Landroid/animation/ValueAnimator;

    new-instance v5, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 627
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy1:Landroid/animation/ValueAnimator;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v4, v5}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 628
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy1:Landroid/animation/ValueAnimator;

    invoke-virtual {v4}, Landroid/animation/ValueAnimator;->start()V

    .line 630
    new-array v4, v11, [Landroid/animation/PropertyValuesHolder;

    const-string v5, "y2"

    new-array v6, v11, [F

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v7, v12

    aput v7, v6, v9

    const/high16 v7, 0x42c80000    # 100.0f

    iget v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v7, v8

    aput v7, v6, v10

    .line 631
    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    aput-object v5, v4, v9

    const-string v5, "e_y2"

    new-array v6, v11, [F

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v7, v12

    aput v7, v6, v9

    const/4 v7, 0x0

    aput v7, v6, v10

    .line 632
    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    aput-object v5, v4, v10

    .line 630
    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy2:Landroid/animation/ValueAnimator;

    .line 633
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy2:Landroid/animation/ValueAnimator;

    invoke-virtual {v4, v13}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 634
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy2:Landroid/animation/ValueAnimator;

    invoke-virtual {v4, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 635
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy2:Landroid/animation/ValueAnimator;

    invoke-virtual {v4, v0, v1}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 636
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy2:Landroid/animation/ValueAnimator;

    new-instance v5, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 637
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy2:Landroid/animation/ValueAnimator;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v4, v5}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 638
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy2:Landroid/animation/ValueAnimator;

    invoke-virtual {v4}, Landroid/animation/ValueAnimator;->start()V

    .line 640
    new-array v4, v11, [Landroid/animation/PropertyValuesHolder;

    const-string v5, "y3"

    new-array v6, v11, [F

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v7, v12

    aput v7, v6, v9

    const/high16 v7, 0x42c80000    # 100.0f

    iget v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v7, v8

    aput v7, v6, v10

    .line 641
    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    aput-object v5, v4, v9

    const-string v5, "e_y3"

    new-array v6, v11, [F

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v7, v12

    aput v7, v6, v9

    const/4 v7, 0x0

    aput v7, v6, v10

    .line 642
    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    aput-object v5, v4, v10

    .line 640
    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy3:Landroid/animation/ValueAnimator;

    .line 643
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy3:Landroid/animation/ValueAnimator;

    invoke-virtual {v4, v13}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 644
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy3:Landroid/animation/ValueAnimator;

    invoke-virtual {v4, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 645
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy3:Landroid/animation/ValueAnimator;

    const-wide/16 v5, 0x2

    mul-long/2addr v5, v0

    invoke-virtual {v4, v5, v6}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 646
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy3:Landroid/animation/ValueAnimator;

    new-instance v5, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 647
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy3:Landroid/animation/ValueAnimator;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v4, v5}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 648
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy3:Landroid/animation/ValueAnimator;

    invoke-virtual {v4}, Landroid/animation/ValueAnimator;->start()V

    .line 650
    new-array v4, v11, [Landroid/animation/PropertyValuesHolder;

    const-string v5, "y4"

    new-array v6, v11, [F

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v7, v12

    aput v7, v6, v9

    const/high16 v7, 0x42c80000    # 100.0f

    iget v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v7, v8

    aput v7, v6, v10

    .line 651
    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    aput-object v5, v4, v9

    const-string v5, "e_y4"

    new-array v6, v11, [F

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v7, v12

    aput v7, v6, v9

    const/4 v7, 0x0

    aput v7, v6, v10

    .line 652
    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    aput-object v5, v4, v10

    .line 650
    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy4:Landroid/animation/ValueAnimator;

    .line 653
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy4:Landroid/animation/ValueAnimator;

    invoke-virtual {v4, v13}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 654
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy4:Landroid/animation/ValueAnimator;

    invoke-virtual {v4, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 655
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy4:Landroid/animation/ValueAnimator;

    const-wide/16 v5, 0x3

    mul-long/2addr v5, v0

    invoke-virtual {v4, v5, v6}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 656
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy4:Landroid/animation/ValueAnimator;

    new-instance v5, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 657
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy4:Landroid/animation/ValueAnimator;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v4, v5}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 658
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy4:Landroid/animation/ValueAnimator;

    invoke-virtual {v4}, Landroid/animation/ValueAnimator;->start()V

    .line 660
    new-array v4, v11, [Landroid/animation/PropertyValuesHolder;

    const-string v5, "y5"

    new-array v6, v11, [F

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v7, v12

    aput v7, v6, v9

    const/high16 v7, 0x42c80000    # 100.0f

    iget v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v7, v8

    aput v7, v6, v10

    .line 661
    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    aput-object v5, v4, v9

    const-string v5, "e_y5"

    new-array v6, v11, [F

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v7, v12

    aput v7, v6, v9

    const/4 v7, 0x0

    aput v7, v6, v10

    .line 662
    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    aput-object v5, v4, v10

    .line 660
    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy5:Landroid/animation/ValueAnimator;

    .line 663
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy5:Landroid/animation/ValueAnimator;

    invoke-virtual {v4, v13}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 664
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy5:Landroid/animation/ValueAnimator;

    invoke-virtual {v4, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 665
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy5:Landroid/animation/ValueAnimator;

    const-wide/16 v5, 0x4

    mul-long/2addr v5, v0

    invoke-virtual {v4, v5, v6}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 666
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy5:Landroid/animation/ValueAnimator;

    new-instance v5, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 667
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy5:Landroid/animation/ValueAnimator;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v4, v5}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 668
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy5:Landroid/animation/ValueAnimator;

    invoke-virtual {v4}, Landroid/animation/ValueAnimator;->start()V

    .line 670
    const/4 v4, 0x5

    new-array v4, v4, [F

    fill-array-data v4, :array_0

    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->IconTranslateX:Landroid/animation/ValueAnimator;

    .line 671
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->IconTranslateX:Landroid/animation/ValueAnimator;

    const-wide/16 v5, 0x1194

    invoke-virtual {v4, v5, v6}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 672
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->IconTranslateX:Landroid/animation/ValueAnimator;

    invoke-virtual {v4, v13}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 673
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->IconTranslateX:Landroid/animation/ValueAnimator;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v4, v5}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 674
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->IconTranslateX:Landroid/animation/ValueAnimator;

    invoke-virtual {v4}, Landroid/animation/ValueAnimator;->start()V

    .line 676
    const/4 v4, 0x3

    new-array v4, v4, [F

    fill-array-data v4, :array_1

    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->IconTranslateY:Landroid/animation/ValueAnimator;

    .line 677
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->IconTranslateY:Landroid/animation/ValueAnimator;

    const-wide/16 v5, 0xbb8

    invoke-virtual {v4, v5, v6}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 678
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->IconTranslateY:Landroid/animation/ValueAnimator;

    invoke-virtual {v4, v13}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 679
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->IconTranslateY:Landroid/animation/ValueAnimator;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v4, v5}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 680
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->IconTranslateY:Landroid/animation/ValueAnimator;

    invoke-virtual {v4}, Landroid/animation/ValueAnimator;->start()V

    .line 682
    const/4 v4, 0x3

    new-array v4, v4, [F

    const/4 v5, 0x0

    aput v5, v4, v9

    const v5, -0x3fd33333    # -2.7f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v5, v6

    aput v5, v4, v10

    const/4 v5, 0x0

    aput v5, v4, v11

    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    .line 683
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v4, v13}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 684
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    const-wide/16 v5, 0x5dc

    invoke-virtual {v4, v5, v6}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 685
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v4, v5}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 686
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v4}, Landroid/animation/ValueAnimator;->start()V

    .line 687
    return-void

    .line 670
    nop

    :array_0
    .array-data 4
        0x0
        -0x3ee00000    # -10.0f
        0x0
        0x41200000    # 10.0f
        0x0
    .end array-data

    .line 676
    :array_1
    .array-data 4
        0x0
        -0x3fc00000    # -3.0f
        0x0
    .end array-data
.end method


# virtual methods
.method public cancelAnimation()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 711
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 712
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 714
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->IconTranslateX:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->IconTranslateX:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 715
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->IconTranslateX:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 717
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->IconTranslateY:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->IconTranslateY:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 718
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->IconTranslateY:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 721
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy1:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 722
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 725
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy2:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 726
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 729
    :cond_4
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy3:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 730
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 733
    :cond_5
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy4:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy4:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 734
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy4:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 737
    :cond_6
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy5:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy5:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 738
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy5:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 740
    :cond_7
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mIsActiveAnimationThread:Z

    .line 741
    return-void
.end method

.method public getScale()F
    .locals 1

    .prologue
    .line 777
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 451
    return-object p0
.end method

.method public isRunning()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 749
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->IconTranslateX:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->IconTranslateY:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy1:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy2:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy3:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy4:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy5:Landroid/animation/ValueAnimator;

    if-nez v1, :cond_1

    .line 768
    :cond_0
    :goto_0
    return v0

    .line 761
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->IconTranslateX:Landroid/animation/ValueAnimator;

    .line 762
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->IconTranslateY:Landroid/animation/ValueAnimator;

    .line 763
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy1:Landroid/animation/ValueAnimator;

    .line 764
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy2:Landroid/animation/ValueAnimator;

    .line 765
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy3:Landroid/animation/ValueAnimator;

    .line 766
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy4:Landroid/animation/ValueAnimator;

    .line 767
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy5:Landroid/animation/ValueAnimator;

    .line 768
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v4, 0x43960000    # 300.0f

    const/4 v2, 0x0

    .line 407
    :try_start_0
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 409
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    cmpg-float v0, v0, v2

    if-gtz v0, :cond_1

    .line 410
    const-string v0, ""

    const-string v1, "scale is less then 0"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 448
    :cond_0
    :goto_0
    return-void

    .line 414
    :cond_1
    const/4 v8, 0x0

    .line 415
    .local v8, "cloudTrX":F
    const/4 v9, 0x0

    .line 416
    .local v9, "cloudTrY":F
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mIsActiveAnimationThread:Z

    if-eqz v0, :cond_2

    .line 417
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->IconTranslateX:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v8

    .line 418
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->IconTranslateY:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v9

    .line 420
    :cond_2
    const/16 v6, 0x1f

    .line 422
    .local v6, "saveFlag":I
    const/16 v6, 0xf

    .line 425
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float v1, v2, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v2, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float v3, v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v4, v0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->saveLayer(FFFFLandroid/graphics/Paint;I)I

    move-result v11

    .line 428
    .local v11, "transparentSaveLevel":I
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v0, v8

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v9

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 429
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->drawDrop(Landroid/graphics/Canvas;)V

    .line 430
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPaint:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->drawTransparentArea(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V

    .line 432
    invoke-virtual {p1, v11}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 434
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v7

    .line 435
    .local v7, "cloudSaveLevel":I
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v0, v8

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    mul-float/2addr v1, v9

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 436
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPaint:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->drawCloud(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    .line 438
    invoke-virtual {p1, v7}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 440
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 441
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->cancelAnimation()V

    .line 442
    const-string v0, ""

    const-string v1, "cancelA"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 445
    .end local v6    # "saveFlag":I
    .end local v7    # "cloudSaveLevel":I
    .end local v8    # "cloudTrX":F
    .end local v9    # "cloudTrY":F
    .end local v11    # "transparentSaveLevel":I
    :catch_0
    move-exception v10

    .line 446
    .local v10, "e":Ljava/lang/Exception;
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 852
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 786
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onViewDetachedFromWindow : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 787
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 788
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 789
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 791
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->IconTranslateX:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_1

    .line 792
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->IconTranslateX:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 793
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->IconTranslateX:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 795
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->IconTranslateY:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_2

    .line 796
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->IconTranslateY:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 797
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->IconTranslateY:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 800
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy1:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_3

    .line 801
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 802
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 805
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy2:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_4

    .line 806
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 807
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 810
    :cond_4
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy3:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_5

    .line 811
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 812
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 815
    :cond_5
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy4:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_6

    .line 816
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy4:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 817
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy4:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 820
    :cond_6
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy5:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_7

    .line 821
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy5:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 822
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy5:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 825
    :cond_7
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    .line 826
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->IconTranslateX:Landroid/animation/ValueAnimator;

    .line 827
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->IconTranslateY:Landroid/animation/ValueAnimator;

    .line 828
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy1:Landroid/animation/ValueAnimator;

    .line 829
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy2:Landroid/animation/ValueAnimator;

    .line 830
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy3:Landroid/animation/ValueAnimator;

    .line 831
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy4:Landroid/animation/ValueAnimator;

    .line 832
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->DropCenterTy5:Landroid/animation/ValueAnimator;

    .line 833
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPaint:Landroid/graphics/Paint;

    .line 834
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mMasking:Landroid/graphics/Path;

    .line 835
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudLeft:Landroid/graphics/Path;

    .line 836
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudRight:Landroid/graphics/Path;

    .line 837
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mCloudTop:Landroid/graphics/Path;

    .line 838
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine1:Landroid/graphics/Path;

    .line 839
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine2:Landroid/graphics/Path;

    .line 840
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine3:Landroid/graphics/Path;

    .line 841
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine4:Landroid/graphics/Path;

    .line 842
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine5:Landroid/graphics/Path;

    .line 843
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mRainLine6:Landroid/graphics/Path;

    .line 844
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_8

    .line 845
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 847
    :cond_8
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mBitmap:Landroid/graphics/Bitmap;

    .line 848
    return-void
.end method

.method public setPaintColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 78
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mPaintColor:I

    .line 79
    return-void
.end method

.method public setScale(F)V
    .locals 0
    .param p1, "scale"    # F

    .prologue
    .line 772
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mScale:F

    .line 773
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->init()V

    .line 774
    return-void
.end method

.method public setWidthAndHeight(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 781
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, p1, p2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 782
    .local v0, "l":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 784
    return-void
.end method

.method public startAnimation()V
    .locals 2

    .prologue
    .line 690
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->makeAnimation()V

    .line 692
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->mIsActiveAnimationThread:Z

    .line 693
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;)V

    .line 706
    .local v0, "t":Ljava/lang/Thread;
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 707
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 708
    return-void
.end method

.method public stopAnimation()V
    .locals 1

    .prologue
    .line 745
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainIconView;->isStop:Z

    .line 746
    return-void
.end method
