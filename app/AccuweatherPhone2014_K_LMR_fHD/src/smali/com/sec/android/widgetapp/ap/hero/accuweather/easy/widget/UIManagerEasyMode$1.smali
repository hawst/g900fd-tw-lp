.class Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode$1;
.super Landroid/os/Handler;
.source "UIManagerEasyMode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    .prologue
    .line 469
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 472
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getRefreshFrom()I

    move-result v0

    if-eqz v0, :cond_1

    .line 498
    :cond_0
    :goto_0
    return-void

    .line 475
    :cond_1
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "UH MW = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", SL = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->isShowLoading:Z
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", RC = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    .line 476
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getRefreshCnt()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 475
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "UH MW = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", RC = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getRefreshCnt()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 478
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 480
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->isShowLoading:Z
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 481
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->showLoading()V
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;)V

    goto :goto_0

    .line 485
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->isShowLoading:Z
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getRefreshCnt()I

    move-result v0

    if-gtz v0, :cond_2

    .line 486
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->stopLoading()V
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;)V

    .line 487
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;)Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.widgetapp.ap.hero.accuweather.widget.action.APPWIDGET_UPDATE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 490
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mIsOnRefreshing:Z
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->access$502(Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;Z)Z

    goto/16 :goto_0

    .line 478
    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_0
        0x20 -> :sswitch_1
    .end sparse-switch
.end method
