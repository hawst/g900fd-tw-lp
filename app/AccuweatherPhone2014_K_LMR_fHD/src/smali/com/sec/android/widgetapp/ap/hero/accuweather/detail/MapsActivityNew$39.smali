.class Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$39;
.super Ljava/lang/Object;
.source "MapsActivityNew.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->showResult(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

.field final synthetic val$citylist:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Ljava/util/ArrayList;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 1425
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$39;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    iput-object p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$39;->val$citylist:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 9
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v8, 0x0

    .line 1427
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$39;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->setKeyboardVisible(Z)V
    invoke-static {v3, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$1000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Z)V

    .line 1428
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$39;->val$citylist:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$39;->val$citylist:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$39;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mDialogSelected:I
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)I

    move-result v4

    if-gt v3, v4, :cond_1

    .line 1451
    :cond_0
    :goto_0
    return-void

    .line 1430
    :cond_1
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$39;->val$citylist:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$39;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mDialogSelected:I
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    .line 1431
    .local v2, "mapInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$39;->val$citylist:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 1432
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$39;->val$citylist:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1433
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$39;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->removeAllMapList()V
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    .line 1435
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$39;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$1500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 1436
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$39;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$1500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->getCurrentUint()I

    move-result v0

    .line 1437
    .local v0, "currentTempUnit":I
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$39;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$1500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->clearItems()V

    .line 1438
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$39;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    const/4 v4, 0x0

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;
    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$1502(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    .line 1440
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$39;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    new-instance v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$39;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$39;->val$citylist:Ljava/util/ArrayList;

    invoke-direct {v4, v5, v6, v0, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Ljava/util/ArrayList;II)V

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;
    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$1502(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    .line 1445
    .end local v0    # "currentTempUnit":I
    :goto_1
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$39;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getOverlays()Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$39;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$1500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1446
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$39;->val$citylist:Ljava/util/ArrayList;

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    .line 1447
    .local v1, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$39;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getController()Lcom/google/android/maps/MapController;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$39;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .line 1448
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLatitude()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v5

    .line 1449
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLongitude()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v7

    .line 1448
    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getPoint(DD)Lcom/google/android/maps/GeoPoint;
    invoke-static {v4, v5, v6, v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;DD)Lcom/google/android/maps/GeoPoint;

    move-result-object v4

    .line 1447
    invoke-virtual {v3, v4}, Lcom/google/android/maps/MapController;->animateTo(Lcom/google/android/maps/GeoPoint;)V

    .line 1450
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$39;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getController()Lcom/google/android/maps/MapController;

    move-result-object v3

    const/4 v4, 0x5

    invoke-virtual {v3, v4}, Lcom/google/android/maps/MapController;->setZoom(I)I

    goto/16 :goto_0

    .line 1442
    .end local v1    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$39;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    new-instance v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$39;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$39;->val$citylist:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$39;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mTempScale:I
    invoke-static {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)I

    move-result v7

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Ljava/util/ArrayList;II)V

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;
    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$1502(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    goto :goto_1
.end method
