.class Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3$1;
.super Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;
.source "MyCurrentLocation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;)V
    .locals 0
    .param p1, "this$1"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;

    .prologue
    .line 345
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;

    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(IILjava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "position"    # I
    .param p2, "responseCode"    # I
    .param p3, "responseStatus"    # Ljava/lang/String;
    .param p4, "responseBody"    # Ljava/lang/String;

    .prologue
    const/16 v7, 0x1b58

    const/16 v6, 0xc8

    .line 348
    invoke-super {p0, p1, p2, p3, p4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;->onReceive(IILjava/lang/String;Ljava/lang/String;)V

    .line 350
    if-ne p2, v6, :cond_3

    .line 351
    const/4 v1, 0x0

    .line 352
    .local v1, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;

    invoke-direct {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;-><init>()V

    .line 353
    .local v3, "parser":Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;

    iget-object v4, v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->ctx:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, p4, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->parseDetailWeatherLocation(Ljava/lang/String;Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    move-result-object v1

    .line 355
    if-eqz v1, :cond_2

    .line 356
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;

    iget-object v4, v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;->val$latitude:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setLatitude(Ljava/lang/String;)V

    .line 357
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;

    iget-object v4, v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;->val$longitude:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setLongitude(Ljava/lang/String;)V

    .line 358
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setProvider(I)V

    .line 359
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setRealLocation(Ljava/lang/String;)V

    .line 361
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getCity()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getCity()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 362
    :cond_0
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;

    iget-object v4, v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    iget-object v4, v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->returnHandler:Landroid/os/Handler;

    invoke-virtual {v4, v7}, Landroid/os/Handler;->removeMessages(I)V

    .line 363
    const-string v4, ""

    const-string v5, "CI N"

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    .line 365
    .local v2, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 367
    .local v0, "data":Landroid/os/Bundle;
    iput v7, v2, Landroid/os/Message;->what:I

    .line 368
    const-string v4, "cityinfo"

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 369
    invoke-virtual {v2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 370
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;

    iget-object v4, v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    iget-object v4, v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->returnHandler:Landroid/os/Handler;

    invoke-virtual {v4, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 394
    .end local v0    # "data":Landroid/os/Bundle;
    .end local v1    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .end local v2    # "msg":Landroid/os/Message;
    .end local v3    # "parser":Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;
    :goto_0
    return-void

    .line 373
    .restart local v1    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .restart local v3    # "parser":Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;
    :cond_1
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;

    iget-object v4, v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;

    iget-object v5, v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;->val$locationHandler:Landroid/os/Handler;

    invoke-virtual {v4, v1, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->addCityWeatherFromDetail(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Landroid/os/Handler;)V

    goto :goto_0

    .line 377
    :cond_2
    const-string v4, ""

    const-string v5, "adCtWthrFrmPrvdr : fail to parse"

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    const/4 p2, -0x2

    .line 381
    .end local v1    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .end local v3    # "parser":Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;
    :cond_3
    if-eq p2, v6, :cond_4

    .line 382
    const-string v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "adCtWthrFrmPrvdr : r = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    .line 384
    .restart local v2    # "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 385
    .restart local v0    # "data":Landroid/os/Bundle;
    invoke-virtual {v2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 386
    const/16 v4, 0x3f2

    iput v4, v2, Landroid/os/Message;->what:I

    .line 387
    const-string v4, "provider"

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;

    iget-object v5, v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mProvider:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    const-string v4, "responseCode"

    invoke-virtual {v0, v4, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 389
    invoke-virtual {v2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 390
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;

    iget-object v4, v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;->val$locationHandler:Landroid/os/Handler;

    invoke-virtual {v4, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 393
    .end local v0    # "data":Landroid/os/Bundle;
    .end local v2    # "msg":Landroid/os/Message;
    :cond_4
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;

    iget-object v4, v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->stopHttpThread()V

    goto :goto_0
.end method
