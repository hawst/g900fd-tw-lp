.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockScreenService;
.super Landroid/app/Service;
.source "WeatherClockScreenService.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "WeatherClockScreenService"


# instance fields
.field private mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 72
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockScreenService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockScreenService$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockScreenService;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockScreenService;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 28
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 36
    const-string v1, ""

    const-string v2, "onCreate()"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 38
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 39
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 41
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockScreenService;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockScreenService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 43
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 44
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 50
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 52
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockScreenService;->mReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockScreenService;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockScreenService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 55
    :cond_0
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 65
    const/4 v0, 0x1

    return v0
.end method
