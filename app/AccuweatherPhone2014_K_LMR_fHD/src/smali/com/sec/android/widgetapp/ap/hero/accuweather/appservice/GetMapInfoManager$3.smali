.class Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$3;
.super Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;
.source "GetMapInfoManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->getAddedMapCityInfoLocation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    .prologue
    .line 492
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(IILjava/lang/String;Ljava/lang/String;J)V
    .locals 8
    .param p1, "position"    # I
    .param p2, "responseCode"    # I
    .param p3, "responseStatus"    # Ljava/lang/String;
    .param p4, "responseBody"    # Ljava/lang/String;
    .param p5, "stamp"    # J

    .prologue
    .line 495
    invoke-super {p0, p1, p2, p3, p4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;->onReceive(IILjava/lang/String;Ljava/lang/String;)V

    .line 497
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mPerformMapCancled:Z
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->access$000()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 498
    const-string v4, ""

    const-string v5, "performMapCancled"

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 499
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->deleteHttpThreadAll()V

    .line 588
    :cond_0
    :goto_0
    return-void

    .line 502
    :cond_1
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->syncstamp:J
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;)J

    move-result-wide v4

    cmp-long v4, v4, p5

    if-eqz v4, :cond_2

    .line 503
    const-string v4, ""

    const-string v5, "syncstamp != stamp #1"

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 504
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->deleteHttpThreadAll()V

    goto :goto_0

    .line 508
    :cond_2
    const/4 v0, 0x0

    .line 510
    .local v0, "curUrlType":I
    const-string v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mHT p : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 512
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    iget-object v4, v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mHttpThreads:Ljava/util/ArrayList;

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    iget-object v4, v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v4, p1, :cond_5

    .line 513
    const-string v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mHT : s: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    iget-object v6, v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 514
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    iget-object v4, v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Thread;

    .line 515
    .local v3, "thread":Ljava/lang/Thread;
    if-eqz v3, :cond_4

    .line 516
    invoke-virtual {v3}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 527
    const-string v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "curUrlType : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 529
    const/16 v4, 0xc8

    if-ne p2, v4, :cond_7

    .line 531
    :try_start_0
    new-instance v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;

    invoke-direct {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;-><init>()V

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->access$1100()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v4, p4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->parseDetailWeatherLocationCity(Ljava/lang/String;Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    move-result-object v2

    .line 533
    .local v2, "temp":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    const-class v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    monitor-enter v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 534
    if-eqz v2, :cond_6

    .line 535
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mAddedMapCityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->access$1200(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    move-result-object v4

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLatitude()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->setLatitude(Ljava/lang/String;)V

    .line 536
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mAddedMapCityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->access$1200(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    move-result-object v4

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLongitude()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->setLongitude(Ljava/lang/String;)V

    .line 537
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    const/16 v6, 0x8

    const/4 v7, 0x1

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->setGetMapInfoResult(IZ)V
    invoke-static {v4, v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;IZ)V

    .line 553
    :cond_3
    :goto_1
    monitor-exit v5

    goto/16 :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v4
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 554
    .end local v2    # "temp":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    :catch_0
    move-exception v1

    .line 555
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 557
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    const/16 v5, 0x8

    const/4 v6, 0x0

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->setGetMapInfoResult(IZ)V
    invoke-static {v4, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;IZ)V

    .line 559
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->deleteHttpThreadAll()V

    .line 561
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mActivity:Landroid/app/Activity;
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->access$800()Landroid/app/Activity;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 562
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mActivity:Landroid/app/Activity;
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->access$800()Landroid/app/Activity;

    move-result-object v4

    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$3$2;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$3$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$3;)V

    invoke-virtual {v4, v5}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 518
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_4
    const-string v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "thread n : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 519
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->deleteHttpThreadAll()V

    goto/16 :goto_0

    .line 523
    .end local v3    # "thread":Ljava/lang/Thread;
    :cond_5
    const-string v4, ""

    const-string v5, "o f i"

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 524
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->deleteHttpThreadAll()V

    goto/16 :goto_0

    .line 539
    .restart local v2    # "temp":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .restart local v3    # "thread":Ljava/lang/Thread;
    :cond_6
    :try_start_3
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    const/16 v6, 0x8

    const/4 v7, 0x0

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->setGetMapInfoResult(IZ)V
    invoke-static {v4, v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;IZ)V

    .line 541
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->deleteHttpThreadAll()V

    .line 543
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mActivity:Landroid/app/Activity;
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->access$800()Landroid/app/Activity;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 544
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mActivity:Landroid/app/Activity;
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->access$800()Landroid/app/Activity;

    move-result-object v4

    new-instance v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$3$1;

    invoke-direct {v6, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$3$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$3;)V

    invoke-virtual {v4, v6}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 572
    .end local v2    # "temp":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    :cond_7
    const-string v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "response : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 574
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    const/16 v5, 0x8

    const/4 v6, 0x0

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->setGetMapInfoResult(IZ)V
    invoke-static {v4, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;IZ)V

    .line 576
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->deleteHttpThreadAll()V

    .line 578
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mActivity:Landroid/app/Activity;
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->access$800()Landroid/app/Activity;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 579
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->mActivity:Landroid/app/Activity;
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->access$800()Landroid/app/Activity;

    move-result-object v4

    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$3$3;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$3$3;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager$3;)V

    invoke-virtual {v4, v5}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_0
.end method
