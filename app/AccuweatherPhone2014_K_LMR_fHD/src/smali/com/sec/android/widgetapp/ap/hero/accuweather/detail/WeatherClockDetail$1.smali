.class Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$1;
.super Ljava/lang/Object;
.source "WeatherClockDetail.java"

# interfaces
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 344
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCubeViewController()Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;
    .locals 1

    .prologue
    .line 372
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCubeViewController:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentBGView()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mCurBGView:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/widget/ImageView;

    move-result-object v0

    return-object v0
.end method

.method public getViEffectSettinginfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    move-result-object v0

    return-object v0
.end method

.method public isConfigurationChanged()Z
    .locals 1

    .prologue
    .line 395
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIsChangeOrientation:Z

    return v0
.end method

.method public isCreateActivity()Z
    .locals 1

    .prologue
    .line 399
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIsCreateActivity:Z
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$900(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Z

    move-result v0

    return v0
.end method

.method public notifyEndAnimation(I)V
    .locals 2
    .param p1, "animType"    # I

    .prologue
    .line 376
    packed-switch p1, :pswitch_data_0

    .line 389
    :cond_0
    :goto_0
    return-void

    .line 378
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mIsAcceptTouch:Z
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 379
    const-string v0, ""

    const-string v1, "notify end fade out!!"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->resetTouchTimer()V
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    .line 381
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setTouchTimer(I)V
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;I)V

    goto :goto_0

    .line 376
    nop

    :pswitch_data_0
    .packed-switch 0xc8
        :pswitch_0
    .end packed-switch
.end method

.method public onPrepareComplete(Landroid/media/MediaPlayer;)V
    .locals 5
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 347
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mStartAniHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$1$1;

    invoke-direct {v2, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$1$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$1;)V

    const-wide/16 v3, 0x64

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 357
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mStartAniHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$1$2;

    invoke-direct {v2, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$1$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$1;)V

    const-wide/16 v3, 0x3e8

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 365
    :goto_0
    return-void

    .line 362
    :catch_0
    move-exception v0

    .line 363
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public setBGAlphaListener(Z)V
    .locals 1
    .param p1, "isOn"    # Z

    .prologue
    .line 403
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-virtual {v0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setBGAlpha(Z)V

    .line 404
    return-void
.end method

.method public setBGVisibilityListener(Z)V
    .locals 0
    .param p1, "isOn"    # Z

    .prologue
    .line 408
    return-void
.end method
