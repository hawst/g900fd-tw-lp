.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/WeatherClock;
.super Landroid/appwidget/AppWidgetProvider;
.source "WeatherClock.java"


# static fields
.field static final DATE_FORMAT_CHANGED:Ljava/lang/String; = "clock.date_format_changed"

.field private static mUiManger:Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;


# instance fields
.field private mCtx:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/WeatherClock;->mUiManger:Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeleted(Landroid/content/Context;[I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetIds"    # [I

    .prologue
    .line 87
    const-string v0, ""

    const-string v1, "onDleted()"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onDeleted(Landroid/content/Context;[I)V

    .line 89
    return-void
.end method

.method public onDisabled(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 92
    const-string v0, ""

    const-string v1, "onDisabled()"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onDisabled(Landroid/content/Context;)V

    .line 94
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 95
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockScreenService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 96
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/WeatherClock;->mUiManger:Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->onDisableWidget()V

    .line 97
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/WeatherClock;->mUiManger:Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    .line 98
    return-void
.end method

.method public onEnabled(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 64
    const-string v0, ""

    const-string v1, "onEnabled()"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/WeatherClock;->mCtx:Landroid/content/Context;

    .line 66
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/WeatherClock;->mUiManger:Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    if-nez v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/WeatherClock;->mCtx:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->getInstance(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/WeatherClock;->mUiManger:Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    .line 69
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 70
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockScreenService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 71
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/WeatherClock;->mUiManger:Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->onEnableWidget()V

    .line 73
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onEnabled(Landroid/content/Context;)V

    .line 74
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 24
    if-nez p2, :cond_1

    .line 25
    :try_start_0
    const-string v4, ""

    const-string v5, "action : intent = null"

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    :cond_0
    :goto_0
    return-void

    .line 29
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 30
    .local v0, "action":Ljava/lang/String;
    const-string v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "action : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/WeatherClock;->mCtx:Landroid/content/Context;

    .line 32
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/WeatherClock;->mCtx:Landroid/content/Context;

    invoke-static {v4}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    .line 33
    .local v1, "appWidgetManager":Landroid/appwidget/AppWidgetManager;
    new-instance v4, Landroid/content/ComponentName;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const-class v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/WeatherClock;

    .line 34
    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    invoke-virtual {v1, v4}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v3

    .line 36
    .local v3, "ids":[I
    sget-object v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/WeatherClock;->mUiManger:Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    if-nez v4, :cond_2

    .line 37
    const-string v4, ""

    const-string v5, "oR : create UI manager : start"

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 38
    invoke-static {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->getInstance(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    move-result-object v4

    sput-object v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/WeatherClock;->mUiManger:Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    .line 39
    sget-object v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/WeatherClock;->mUiManger:Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    invoke-virtual {v4, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->isExistWidget([I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 40
    const-string v4, ""

    const-string v5, "Restart service and update!!"

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;

    invoke-direct {v4, p1, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v4}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 42
    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockScreenService;

    invoke-direct {v4, p1, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v4}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 43
    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.sec.android.widgetapp.ap.hero.accuweather.widget.action.APPWIDGET_UPDATE"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 47
    :cond_2
    const-string v4, "android.appwidget.action.APPWIDGET_DELETED"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    const-string v4, "android.appwidget.action.APPWIDGET_DISABLED"

    .line 48
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 49
    :cond_3
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 57
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "appWidgetManager":Landroid/appwidget/AppWidgetManager;
    .end local v3    # "ids":[I
    :catch_0
    move-exception v2

    .line 58
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 60
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_1
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 53
    .restart local v0    # "action":Ljava/lang/String;
    .restart local v1    # "appWidgetManager":Landroid/appwidget/AppWidgetManager;
    .restart local v3    # "ids":[I
    :cond_4
    :try_start_1
    sget-object v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/WeatherClock;->mUiManger:Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    if-eqz v4, :cond_0

    sget-object v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/WeatherClock;->mUiManger:Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    invoke-virtual {v4, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->isExistWidget([I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 56
    sget-object v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/WeatherClock;->mUiManger:Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    invoke-virtual {v4, p2, v1, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->onRecieveWidget(Landroid/content/Intent;Landroid/appwidget/AppWidgetManager;[I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetIds"    # [I

    .prologue
    .line 77
    const-string v0, ""

    const-string v1, "onUpdate()"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/WeatherClock;->mCtx:Landroid/content/Context;

    .line 79
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 80
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockScreenService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 81
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/WeatherClock;->mUiManger:Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, p3, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->onUpdateWidget(Landroid/appwidget/AppWidgetManager;[IZ)V

    .line 83
    invoke-super {p0, p1, p2, p3}, Landroid/appwidget/AppWidgetProvider;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 84
    return-void
.end method
