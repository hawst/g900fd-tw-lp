.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/AccuBCReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AccuBCReceiver.java"


# instance fields
.field mCtx:Landroid/content/Context;

.field urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/AccuBCReceiver;->urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v10, 0x1

    .line 25
    :try_start_0
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/AccuBCReceiver;->mCtx:Landroid/content/Context;

    .line 26
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 27
    .local v2, "action":Ljava/lang/String;
    const-string v7, ""

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "action:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 29
    const-string v7, "com.sec.android.widgetapp.ap.hero.accuweatherdaemon.action.SYNC_DATA_WITH_DAEMON"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 30
    const-string v7, ""

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "=============== BR act = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    const-string v7, "MENUNUM"

    const/4 v8, 0x0

    invoke-virtual {p2, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 32
    .local v5, "menunum":I
    const/4 v7, 0x2

    if-ne v5, v7, :cond_2

    .line 33
    const-string v7, "AUTOREFRESH"

    const/4 v8, 0x0

    invoke-virtual {p2, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 34
    .local v0, "DaemonRefresh":I
    invoke-static {p1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;->setDaemonAccuRefresh(Landroid/content/Context;I)V

    .line 39
    .end local v0    # "DaemonRefresh":I
    :cond_0
    :goto_0
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/AccuBCReceiver;->mCtx:Landroid/content/Context;

    new-instance v8, Landroid/content/Intent;

    const-string v9, "com.sec.android.widgetapp.ap.hero.accuweather.action.START_DETAILS"

    invoke-direct {v8, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v8}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 40
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/AccuBCReceiver;->mCtx:Landroid/content/Context;

    new-instance v8, Landroid/content/Intent;

    const-string v9, "com.sec.android.widgetapp.ap.hero.accuweather.widget.action.APPWIDGET_UPDATE"

    invoke-direct {v8, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v8}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 57
    .end local v2    # "action":Ljava/lang/String;
    .end local v5    # "menunum":I
    :cond_1
    :goto_1
    return-void

    .line 35
    .restart local v2    # "action":Ljava/lang/String;
    .restart local v5    # "menunum":I
    :cond_2
    if-ne v5, v10, :cond_0

    .line 36
    const-string v7, "TEMP"

    const/4 v8, 0x1

    invoke-virtual {p2, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 37
    .local v1, "DaemonTemp":I
    invoke-static {p1, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;->setDaemonTemp(Landroid/content/Context;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 54
    .end local v1    # "DaemonTemp":I
    .end local v2    # "action":Ljava/lang/String;
    .end local v5    # "menunum":I
    :catch_0
    move-exception v3

    .line 55
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 42
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v2    # "action":Ljava/lang/String;
    :cond_3
    :try_start_1
    const-string v7, "com.sec.android.widgetapp.at.hero.accuweather.action.DAEMON_VERSION_CHECK"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 44
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    .line 45
    .local v4, "handler":Landroid/os/Handler;
    new-instance v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/AccuBCReceiver$1;

    invoke-direct {v6, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/AccuBCReceiver$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/AccuBCReceiver;)V

    .line 51
    .local v6, "runnable":Ljava/lang/Runnable;
    invoke-virtual {v4, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
