.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockWidget;
.super Landroid/appwidget/AppWidgetProvider;
.source "WeatherClockWidget.java"


# static fields
.field static final DATE_FORMAT_CHANGED:Ljava/lang/String; = "clock.date_format_changed"

.field private static mUiWidgetMainManger:Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;


# instance fields
.field private mCtx:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockWidget;->mUiWidgetMainManger:Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeleted(Landroid/content/Context;[I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetIds"    # [I

    .prologue
    .line 96
    const-string v0, ""

    const-string v1, "onDleted()"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    invoke-static {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->removePreferenceWidgetCountForRemoteWidget(Landroid/content/Context;)V

    .line 99
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onDeleted(Landroid/content/Context;[I)V

    .line 100
    return-void
.end method

.method public onDisabled(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 103
    const-string v1, ""

    const-string v2, "onDisabled()"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onDisabled(Landroid/content/Context;)V

    .line 105
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v1}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 106
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockScreenService;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v1}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 108
    :try_start_0
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockWidget;->mUiWidgetMainManger:Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;->onDisableWidget()V

    .line 109
    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockWidget;->mUiWidgetMainManger:Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 113
    :goto_0
    return-void

    .line 110
    :catch_0
    move-exception v0

    .line 111
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onEnabled(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 73
    const-string v0, ""

    const-string v1, "onEnabled()"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockWidget;->mCtx:Landroid/content/Context;

    .line 75
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockWidget;->mUiWidgetMainManger:Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;

    if-nez v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockWidget;->mCtx:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockWidget;->mUiWidgetMainManger:Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;

    .line 78
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 79
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockScreenService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 80
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockWidget;->mUiWidgetMainManger:Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;->onEnableWidget()V

    .line 82
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onEnabled(Landroid/content/Context;)V

    .line 83
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 24
    if-nez p2, :cond_1

    .line 25
    const-string v4, ""

    const-string v5, "onReceive : intent = null"

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    :cond_0
    :goto_0
    return-void

    .line 30
    :cond_1
    :try_start_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 31
    .local v0, "action":Ljava/lang/String;
    const-string v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "action : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 32
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockWidget;->mCtx:Landroid/content/Context;

    .line 33
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockWidget;->mCtx:Landroid/content/Context;

    invoke-static {v4}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    .line 34
    .local v1, "appWidgetManager":Landroid/appwidget/AppWidgetManager;
    new-instance v4, Landroid/content/ComponentName;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const-class v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockWidget;

    .line 35
    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    invoke-virtual {v1, v4}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v3

    .line 38
    .local v3, "ids":[I
    array-length v4, v3

    invoke-static {p1, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->addPreferenceWidgetCountForRemoteWidget(Landroid/content/Context;I)V

    .line 39
    const-string v4, "com.sec.android.widgetapp.ap.hero.accuweather.action.TIME_TICK"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "com.sec.android.widgetapp.ap.hero.accuweather.widget.action.APPWIDGET_UPDATE"

    .line 40
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 41
    :cond_2
    const-string v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "weather widget id size = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    array-length v6, v3

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    :cond_3
    const-string v4, "android.appwidget.action.APPWIDGET_DELETED"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "android.appwidget.action.APPWIDGET_DISABLED"

    .line 45
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 46
    :cond_4
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 66
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "appWidgetManager":Landroid/appwidget/AppWidgetManager;
    .end local v3    # "ids":[I
    :catch_0
    move-exception v2

    .line 67
    .local v2, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v2}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 69
    .end local v2    # "e":Ljava/lang/NullPointerException;
    :goto_1
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    .line 50
    .restart local v0    # "action":Ljava/lang/String;
    .restart local v1    # "appWidgetManager":Landroid/appwidget/AppWidgetManager;
    .restart local v3    # "ids":[I
    :cond_5
    :try_start_1
    sget-object v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockWidget;->mUiWidgetMainManger:Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;

    if-nez v4, :cond_6

    .line 51
    const-string v4, ""

    const-string v5, "oR : create UI manager : start"

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    invoke-static {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;

    move-result-object v4

    sput-object v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockWidget;->mUiWidgetMainManger:Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;

    .line 54
    sget-object v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockWidget;->mUiWidgetMainManger:Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;

    invoke-virtual {v4, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;->isExistWidget([I)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 55
    const-string v4, ""

    const-string v5, "Restart service and update!!"

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;

    invoke-direct {v4, p1, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v4}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 57
    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockScreenService;

    invoke-direct {v4, p1, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v4}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 58
    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.sec.android.widgetapp.ap.hero.accuweather.widget.action.APPWIDGET_UPDATE"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 62
    :cond_6
    sget-object v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockWidget;->mUiWidgetMainManger:Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;

    if-eqz v4, :cond_0

    sget-object v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockWidget;->mUiWidgetMainManger:Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;

    invoke-virtual {v4, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;->isExistWidget([I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 65
    sget-object v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockWidget;->mUiWidgetMainManger:Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;

    invoke-virtual {v4, p2, v1, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;->onRecieveWidget(Landroid/content/Intent;Landroid/appwidget/AppWidgetManager;[I)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetIds"    # [I

    .prologue
    .line 86
    const-string v0, ""

    const-string v1, "onUpdate()"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockWidget;->mCtx:Landroid/content/Context;

    .line 88
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 89
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockScreenService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 90
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockWidget;->mUiWidgetMainManger:Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, p3, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;->onUpdateWidget(Landroid/appwidget/AppWidgetManager;[IZ)V

    .line 92
    invoke-super {p0, p1, p2, p3}, Landroid/appwidget/AppWidgetProvider;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 93
    return-void
.end method
