.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem$MyCreator;
.super Ljava/lang/Object;
.source "CityListItem.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MyCreator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 175
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    invoke-direct {v0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 173
    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem$MyCreator;->createFromParcel(Landroid/os/Parcel;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 179
    new-array v0, p1, [Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 173
    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem$MyCreator;->newArray(I)[Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    move-result-object v0

    return-object v0
.end method
