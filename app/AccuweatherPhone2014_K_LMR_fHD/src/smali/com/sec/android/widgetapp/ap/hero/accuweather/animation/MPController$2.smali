.class Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$2;
.super Ljava/lang/Object;
.source "MPController.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->playBGFadeOut()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    .prologue
    .line 419
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 4
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 431
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BG fade out animation end MPState = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPState:I
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", isplaying = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->isPlaying()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 433
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mIsPlayingFadeOutAnim:Z
    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$102(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;Z)Z

    .line 434
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPState:I
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 435
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;->setBGAlphaListener(Z)V

    .line 439
    :goto_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    move-result-object v1

    const/16 v2, 0xc8

    invoke-interface {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;->notifyEndAnimation(I)V

    .line 445
    :goto_1
    return-void

    .line 437
    :cond_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;->setBGAlphaListener(Z)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 440
    :catch_0
    move-exception v0

    .line 441
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 442
    const-string v1, ""

    const-string v2, "pFOA NPE"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 428
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 422
    const-string v0, ""

    const-string v1, "BG fade out animation start"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mIsPlayingFadeOutAnim:Z
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$102(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;Z)Z

    .line 424
    return-void
.end method
