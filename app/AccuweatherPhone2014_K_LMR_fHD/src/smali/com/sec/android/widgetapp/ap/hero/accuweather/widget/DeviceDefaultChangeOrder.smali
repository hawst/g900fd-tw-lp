.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;
.super Lcom/sec/android/touchwiz/widget/TwListView;
.source "DeviceDefaultChangeOrder.java"


# instance fields
.field private mTwListAnimator:Lcom/sec/android/touchwiz/animator/TwDndListAnimator;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Lcom/sec/android/touchwiz/widget/TwListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;->mTwListAnimator:Lcom/sec/android/touchwiz/animator/TwDndListAnimator;

    .line 19
    new-instance v0, Lcom/sec/android/touchwiz/animator/TwDndListAnimator;

    invoke-direct {v0, p1, p0}, Lcom/sec/android/touchwiz/animator/TwDndListAnimator;-><init>(Landroid/content/Context;Lcom/sec/android/touchwiz/widget/TwListView;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;->mTwListAnimator:Lcom/sec/android/touchwiz/animator/TwDndListAnimator;

    .line 21
    return-void
.end method


# virtual methods
.method public setDndListener()V
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;->mTwListAnimator:Lcom/sec/android/touchwiz/animator/TwDndListAnimator;

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;->setDndListAnimator(Lcom/sec/android/touchwiz/animator/TwDndListAnimator;)V

    .line 25
    return-void
.end method

.method public setDndMode(Z)V
    .locals 1
    .param p1, "b"    # Z

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;->mTwListAnimator:Lcom/sec/android/touchwiz/animator/TwDndListAnimator;

    invoke-virtual {v0, p1}, Lcom/sec/android/touchwiz/animator/TwDndListAnimator;->setDndMode(Z)V

    .line 44
    return-void
.end method

.method public setDragGrabHandleDrawable(I)V
    .locals 1
    .param p1, "changeorderIconSelector"    # I

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;->mTwListAnimator:Lcom/sec/android/touchwiz/animator/TwDndListAnimator;

    invoke-virtual {v0, p1}, Lcom/sec/android/touchwiz/animator/TwDndListAnimator;->setDragGrabHandleDrawable(I)V

    .line 38
    return-void
.end method

.method public setDragGrabHandlePadding(IIII)V
    .locals 1
    .param p1, "pixelFromDP"    # I
    .param p2, "i"    # I
    .param p3, "j"    # I
    .param p4, "k"    # I

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;->mTwListAnimator:Lcom/sec/android/touchwiz/animator/TwDndListAnimator;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/android/touchwiz/animator/TwDndListAnimator;->setDragGrabHandlePadding(IIII)V

    .line 56
    return-void
.end method

.method public setDragGrabHandlePositionGravity(I)V
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;->mTwListAnimator:Lcom/sec/android/touchwiz/animator/TwDndListAnimator;

    invoke-virtual {v0, p1}, Lcom/sec/android/touchwiz/animator/TwDndListAnimator;->setDragGrabHandlePositionGravity(I)V

    .line 50
    return-void
.end method

.method public setOrderItemAdapter(Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrderAdapter;)V
    .locals 1
    .param p1, "mOrderAdapter"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrderAdapter;

    .prologue
    .line 28
    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 29
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;->mTwListAnimator:Lcom/sec/android/touchwiz/animator/TwDndListAnimator;

    invoke-virtual {v0, p1}, Lcom/sec/android/touchwiz/animator/TwDndListAnimator;->setDndController(Lcom/sec/android/touchwiz/animator/TwAbsDndAnimator$TwDndController;)V

    .line 32
    return-void
.end method
