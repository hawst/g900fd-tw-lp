.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/AnimUtils;
.super Ljava/lang/Object;
.source "AnimUtils.java"


# static fields
.field public static final NONE:Ljava/lang/String; = "none"

.field public static final RAIN:Ljava/lang/String; = "rain"

.field public static final RAIN_EFFECT_DIM_COLOR:I

.field public static final RAIN_EFFECT_NORMAL_COLOR:I

.field public static final SNOW:Ljava/lang/String; = "snow"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0xff

    const/16 v0, 0xf0

    const/16 v1, 0xc2

    .line 13
    invoke-static {v2, v0, v0, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    sput v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/AnimUtils;->RAIN_EFFECT_NORMAL_COLOR:I

    .line 15
    invoke-static {v2, v1, v1, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    sput v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/AnimUtils;->RAIN_EFFECT_DIM_COLOR:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getWeatherEffectType(I)Ljava/lang/String;
    .locals 1
    .param p0, "iconNum"    # I

    .prologue
    .line 18
    sparse-switch p0, :sswitch_data_0

    .line 40
    const-string v0, "none"

    :goto_0
    return-object v0

    .line 26
    :sswitch_0
    const-string v0, "rain"

    goto :goto_0

    .line 18
    nop

    :sswitch_data_0
    .sparse-switch
        0xc -> :sswitch_0
        0xd -> :sswitch_0
        0x12 -> :sswitch_0
        0x27 -> :sswitch_0
        0x28 -> :sswitch_0
    .end sparse-switch
.end method
