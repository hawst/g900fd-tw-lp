.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
.super Ljava/lang/Object;
.source "MapCityInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field mCity:Ljava/lang/String;

.field mDuplicateItem:Ljava/lang/String;

.field mIcon:Ljava/lang/String;

.field mIsDayTime:Ljava/lang/String;

.field mLatitude:Ljava/lang/String;

.field mLocation:Ljava/lang/String;

.field mLongitude:Ljava/lang/String;

.field mProvider:I

.field mState:Ljava/lang/String;

.field mTemp:Ljava/lang/String;

.field mUpdateDate:Ljava/lang/String;

.field zoomlevel:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 165
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo$1;

    invoke-direct {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo$1;-><init>()V

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const-string v0, "false"

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mDuplicateItem:Ljava/lang/String;

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mCity:Ljava/lang/String;

    .line 152
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mState:Ljava/lang/String;

    .line 153
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mLocation:Ljava/lang/String;

    .line 154
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mLatitude:Ljava/lang/String;

    .line 155
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mLongitude:Ljava/lang/String;

    .line 156
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mTemp:Ljava/lang/String;

    .line 157
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mIcon:Ljava/lang/String;

    .line 158
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mProvider:I

    .line 159
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->zoomlevel:I

    .line 160
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mUpdateDate:Ljava/lang/String;

    .line 161
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mIsDayTime:Ljava/lang/String;

    .line 162
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mDuplicateItem:Ljava/lang/String;

    .line 163
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p1, "city"    # Ljava/lang/String;
    .param p2, "state"    # Ljava/lang/String;
    .param p3, "location"    # Ljava/lang/String;
    .param p4, "latitude"    # Ljava/lang/String;
    .param p5, "longitude"    # Ljava/lang/String;
    .param p6, "icon"    # Ljava/lang/String;
    .param p7, "temp"    # Ljava/lang/String;
    .param p8, "provider"    # I

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mCity:Ljava/lang/String;

    .line 40
    iput-object p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mState:Ljava/lang/String;

    .line 41
    iput-object p3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mLocation:Ljava/lang/String;

    .line 42
    iput-object p4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mLatitude:Ljava/lang/String;

    .line 43
    iput-object p5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mLongitude:Ljava/lang/String;

    .line 44
    iput-object p6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mIcon:Ljava/lang/String;

    .line 45
    iput-object p7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mTemp:Ljava/lang/String;

    .line 46
    iput p8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mProvider:I

    .line 47
    const-string v0, "false"

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mDuplicateItem:Ljava/lang/String;

    .line 48
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 1
    .param p1, "mCity"    # Ljava/lang/String;
    .param p2, "mState"    # Ljava/lang/String;
    .param p3, "mLocation"    # Ljava/lang/String;
    .param p4, "mLatitude"    # Ljava/lang/String;
    .param p5, "mLongitude"    # Ljava/lang/String;
    .param p6, "mIcon"    # Ljava/lang/String;
    .param p7, "mTemp"    # Ljava/lang/String;
    .param p8, "mProvider"    # I
    .param p9, "zoomlevel"    # I

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mCity:Ljava/lang/String;

    .line 26
    iput-object p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mState:Ljava/lang/String;

    .line 27
    iput-object p3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mLocation:Ljava/lang/String;

    .line 28
    iput-object p4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mLatitude:Ljava/lang/String;

    .line 29
    iput-object p5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mLongitude:Ljava/lang/String;

    .line 30
    iput-object p7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mTemp:Ljava/lang/String;

    .line 31
    iput-object p6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mIcon:Ljava/lang/String;

    .line 32
    iput p8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mProvider:I

    .line 33
    iput p9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->zoomlevel:I

    .line 34
    const-string v0, "false"

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mDuplicateItem:Ljava/lang/String;

    .line 35
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 147
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public getCity()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mCity:Ljava/lang/String;

    return-object v0
.end method

.method public getIcon()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mIcon:Ljava/lang/String;

    return-object v0
.end method

.method public getIsDayTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mIsDayTime:Ljava/lang/String;

    return-object v0
.end method

.method public getIsDuplicateItem()Ljava/lang/String;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mDuplicateItem:Ljava/lang/String;

    return-object v0
.end method

.method public getLatitude()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mLatitude:Ljava/lang/String;

    return-object v0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mLocation:Ljava/lang/String;

    return-object v0
.end method

.method public getLongitude()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mLongitude:Ljava/lang/String;

    return-object v0
.end method

.method public getProvider()I
    .locals 1

    .prologue
    .line 123
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mProvider:I

    return v0
.end method

.method public getState()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mState:Ljava/lang/String;

    return-object v0
.end method

.method public getTemp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mTemp:Ljava/lang/String;

    return-object v0
.end method

.method public getUpdateDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mUpdateDate:Ljava/lang/String;

    return-object v0
.end method

.method public getZoomlevel()I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->zoomlevel:I

    return v0
.end method

.method public setCity(Ljava/lang/String;)V
    .locals 0
    .param p1, "mCity"    # Ljava/lang/String;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mCity:Ljava/lang/String;

    .line 72
    return-void
.end method

.method public setIcon(Ljava/lang/String;)V
    .locals 0
    .param p1, "mIcon"    # Ljava/lang/String;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mIcon:Ljava/lang/String;

    .line 112
    return-void
.end method

.method public setIsDayTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "isDayTime"    # Ljava/lang/String;

    .prologue
    .line 135
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mIsDayTime:Ljava/lang/String;

    .line 136
    return-void
.end method

.method public setIsDuplicateItem(Ljava/lang/String;)V
    .locals 0
    .param p1, "isDuplicate"    # Ljava/lang/String;

    .prologue
    .line 143
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mDuplicateItem:Ljava/lang/String;

    .line 144
    return-void
.end method

.method public setLatitude(Ljava/lang/String;)V
    .locals 0
    .param p1, "mLatitude"    # Ljava/lang/String;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mLatitude:Ljava/lang/String;

    .line 96
    return-void
.end method

.method public setLocation(Ljava/lang/String;)V
    .locals 0
    .param p1, "mLocation"    # Ljava/lang/String;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mLocation:Ljava/lang/String;

    .line 88
    return-void
.end method

.method public setLongitude(Ljava/lang/String;)V
    .locals 0
    .param p1, "mLongitude"    # Ljava/lang/String;

    .prologue
    .line 103
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mLongitude:Ljava/lang/String;

    .line 104
    return-void
.end method

.method public setProvider(I)V
    .locals 0
    .param p1, "mProvider"    # I

    .prologue
    .line 127
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mProvider:I

    .line 128
    return-void
.end method

.method public setState(Ljava/lang/String;)V
    .locals 0
    .param p1, "mState"    # Ljava/lang/String;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mState:Ljava/lang/String;

    .line 80
    return-void
.end method

.method public setTemp(Ljava/lang/String;)V
    .locals 0
    .param p1, "mTemp"    # Ljava/lang/String;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mTemp:Ljava/lang/String;

    .line 120
    return-void
.end method

.method public setUpdateDate(Ljava/lang/String;)V
    .locals 0
    .param p1, "mUpdateDate"    # Ljava/lang/String;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mUpdateDate:Ljava/lang/String;

    .line 56
    return-void
.end method

.method public setZoomlevel(I)V
    .locals 0
    .param p1, "zoomlevel"    # I

    .prologue
    .line 63
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->zoomlevel:I

    .line 64
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 176
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mCity:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 177
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mState:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 178
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mLocation:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 179
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mLatitude:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 180
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mLongitude:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 181
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mTemp:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 182
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mIcon:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 183
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mProvider:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 184
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->zoomlevel:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 185
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mUpdateDate:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 186
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mIsDayTime:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 187
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->mDuplicateItem:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 188
    return-void
.end method
