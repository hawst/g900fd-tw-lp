.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;
.super Landroid/view/View;
.source "SunnyIconView.java"

# interfaces
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;


# instance fields
.field SunRotation:Landroid/animation/ValueAnimator;

.field isStop:Z

.field l:Landroid/animation/Animator$AnimatorListener;

.field private mIsActiveAnimationThread:Z

.field private mPaint:Landroid/graphics/Paint;

.field private mPaintColor:I

.field private mScale:F

.field private mSun:Landroid/graphics/Path;

.field private mSunOuter:Landroid/graphics/Path;

.field rotateOffset:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 42
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->SunRotation:Landroid/animation/ValueAnimator;

    .line 25
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mIsActiveAnimationThread:Z

    .line 27
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mPaint:Landroid/graphics/Paint;

    .line 29
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mSun:Landroid/graphics/Path;

    .line 31
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mSunOuter:Landroid/graphics/Path;

    .line 33
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    .line 35
    const v0, -0xa0a0b

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mPaintColor:I

    .line 190
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->rotateOffset:F

    .line 191
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->l:Landroid/animation/Animator$AnimatorListener;

    .line 256
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->isStop:Z

    .line 43
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->init()V

    .line 44
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mIsActiveAnimationThread:Z

    return v0
.end method

.method private drawSun(Landroid/graphics/Canvas;Landroid/graphics/Paint;F)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "paint"    # Landroid/graphics/Paint;
    .param p3, "rotation"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/high16 v3, 0x42dc0000    # 110.0f

    .line 113
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mSun:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 115
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 116
    const/4 v0, 0x0

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 117
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mSunOuter:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 118
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 120
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 121
    const/high16 v0, 0x42b40000    # 90.0f

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 122
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mSunOuter:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 123
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 125
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 126
    const/high16 v0, 0x43340000    # 180.0f

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 127
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mSunOuter:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 128
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 130
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 131
    const/high16 v0, 0x43870000    # 270.0f

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 132
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mSunOuter:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 133
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 136
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 137
    const/high16 v0, 0x42340000    # 45.0f

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 138
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mSunOuter:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 139
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 141
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 142
    const/high16 v0, 0x43070000    # 135.0f

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 143
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mSunOuter:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 144
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 146
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 147
    const/high16 v0, 0x43610000    # 225.0f

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 148
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mSunOuter:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 149
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 151
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 152
    const v0, 0x439d8000    # 315.0f

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 153
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mSunOuter:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 154
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 157
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 55
    invoke-virtual {p0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 56
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mPaint:Landroid/graphics/Paint;

    .line 57
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->initPath()V

    .line 58
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mPaintColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 59
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 60
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 61
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40a00000    # 5.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 62
    return-void
.end method

.method private initPath()V
    .locals 13

    .prologue
    const/high16 v12, 0x42e40000    # 114.0f

    const/high16 v11, 0x42d40000    # 106.0f

    const v10, 0x42911b23

    const v9, 0x41b3fdf4    # 22.499f

    const/high16 v8, 0x42dc0000    # 110.0f

    .line 65
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mSunOuter:Landroid/graphics/Path;

    .line 66
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mSunOuter:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v1, v8

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v2, v9

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 67
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mSunOuter:Landroid/graphics/Path;

    const v1, 0x42d794fe    # 107.791f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v2, v9

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v3, v11

    const v4, 0x41c251ec    # 24.29f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v5, v11

    const v6, 0x41d3fdf4    # 26.499f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 69
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mSunOuter:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v1, v11

    const v2, 0x423551ec    # 45.33f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 70
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mSunOuter:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v1, v11

    const v2, 0x423e27f0

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42d794fe    # 107.791f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x424551ec    # 49.33f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v5, v8

    const v6, 0x424551ec    # 49.33f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 72
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mSunOuter:Landroid/graphics/Path;

    const v1, 0x42e06b02    # 112.209f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x424551ec    # 49.33f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v3, v12

    const v4, 0x423e27f0

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v5, v12

    const v6, 0x423551ec    # 45.33f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 74
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mSunOuter:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v1, v12

    const v2, 0x41d3fdf4    # 26.499f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 75
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mSunOuter:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v1, v12

    const v2, 0x41c251ec    # 24.29f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42e06b02    # 112.209f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v4, v9

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v5, v8

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v6, v9

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 78
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mSun:Landroid/graphics/Path;

    .line 79
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mSun:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v1, v8

    const v2, 0x431b722d    # 155.446f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 80
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mSun:Landroid/graphics/Path;

    const v1, 0x42a9e24e

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x431b722d    # 155.446f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42811cac    # 64.556f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43070f1b

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42811cac    # 64.556f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v6, v8

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 82
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mSun:Landroid/graphics/Path;

    const v1, 0x42811cac    # 64.556f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a9e1cb

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42a9e24e

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42811b23

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v5, v8

    const v6, 0x42811b23

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 84
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mSun:Landroid/graphics/Path;

    const v1, 0x43070ed9

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42811b23

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431b71aa    # 155.444f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42a9e148    # 84.94f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431b71aa    # 155.444f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v6, v8

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 86
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mSun:Landroid/graphics/Path;

    const v1, 0x431b71aa    # 155.444f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43070f5c    # 135.06f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43070ed9

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x431b722d    # 155.446f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v5, v8

    const v6, 0x431b722d    # 155.446f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 88
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mSun:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 89
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mSun:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v1, v8

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v2, v10

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 90
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mSun:Landroid/graphics/Path;

    const v1, 0x42b2b53f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v2, v10

    const v3, 0x42911cac    # 72.556f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42b2b439

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42911cac    # 72.556f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v6, v8

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 92
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mSun:Landroid/graphics/Path;

    const v1, 0x42911cac    # 72.556f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4302a5a2

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42b2b53f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4313722d    # 147.446f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v5, v8

    const v6, 0x4313722d    # 147.446f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 94
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mSun:Landroid/graphics/Path;

    const v1, 0x4302a560

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4313722d    # 147.446f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431371aa    # 147.444f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4302a5a2

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431371aa    # 147.444f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v6, v8

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 96
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mSun:Landroid/graphics/Path;

    const v1, 0x431371aa    # 147.444f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42b2b439

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4302a560

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v4, v10

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v5, v8

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    mul-float/2addr v6, v10

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 99
    return-void
.end method


# virtual methods
.method public cancelAnimation()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 248
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->SunRotation:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->SunRotation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->SunRotation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 252
    :cond_0
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->isStop:Z

    .line 253
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mIsActiveAnimationThread:Z

    .line 254
    return-void
.end method

.method public getScale()F
    .locals 1

    .prologue
    .line 274
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 188
    return-object p0
.end method

.method public isRunning()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 262
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->SunRotation:Landroid/animation/ValueAnimator;

    if-nez v0, :cond_0

    .line 263
    const/4 v0, 0x0

    .line 265
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->SunRotation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 162
    :try_start_0
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 164
    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    const/4 v3, 0x0

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_1

    .line 165
    const-string v2, ""

    const-string v3, "scale is less then 0"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    :cond_0
    :goto_0
    return-void

    .line 169
    :cond_1
    const/4 v1, 0x0

    .line 171
    .local v1, "rotation":F
    iget-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mIsActiveAnimationThread:Z

    if-eqz v2, :cond_2

    .line 173
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->SunRotation:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->rotateOffset:F

    add-float v1, v2, v3

    .line 176
    :cond_2
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mPaint:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v2, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->drawSun(Landroid/graphics/Canvas;Landroid/graphics/Paint;F)V

    .line 177
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->isRunning()Z

    move-result v2

    if-nez v2, :cond_0

    .line 178
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->cancelAnimation()V

    .line 179
    const-string v2, ""

    const-string v3, "cancelA"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 183
    .end local v1    # "rotation":F
    :catch_0
    move-exception v0

    .line 184
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 298
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 282
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onViewDetachedFromWindow : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->SunRotation:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 284
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->SunRotation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 285
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->SunRotation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 289
    :cond_0
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->SunRotation:Landroid/animation/ValueAnimator;

    .line 290
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mPaint:Landroid/graphics/Paint;

    .line 291
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mSun:Landroid/graphics/Path;

    .line 292
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mSunOuter:Landroid/graphics/Path;

    .line 294
    return-void
.end method

.method public setPaintColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 38
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mPaintColor:I

    .line 39
    return-void
.end method

.method public setScale(F)V
    .locals 0
    .param p1, "scale"    # F

    .prologue
    .line 269
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mScale:F

    .line 270
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->init()V

    .line 271
    return-void
.end method

.method public setWidthAndHeight(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 278
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, p1, p2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 279
    .local v0, "l":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 280
    return-void
.end method

.method public startAnimation()V
    .locals 4

    .prologue
    .line 218
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->isStop:Z

    .line 219
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->rotateOffset:F

    .line 221
    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->SunRotation:Landroid/animation/ValueAnimator;

    .line 222
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->SunRotation:Landroid/animation/ValueAnimator;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 223
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->SunRotation:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x5dc

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 224
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->SunRotation:Landroid/animation/ValueAnimator;

    new-instance v2, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v2}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 225
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->SunRotation:Landroid/animation/ValueAnimator;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 226
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->SunRotation:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    .line 228
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->mIsActiveAnimationThread:Z

    .line 229
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;)V

    .line 242
    .local v0, "t":Ljava/lang/Thread;
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 243
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 244
    return-void

    .line 221
    :array_0
    .array-data 4
        0x0
        0x42340000    # 45.0f
    .end array-data
.end method

.method public stopAnimation()V
    .locals 1

    .prologue
    .line 258
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/SunnyIconView;->isStop:Z

    .line 259
    return-void
.end method
