.class Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$13;
.super Ljava/lang/Object;
.source "MenuSettings.java"

# interfaces
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

.field final synthetic val$preference:Landroid/preference/Preference;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;Landroid/preference/Preference;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    .prologue
    .line 1662
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$13;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    iput-object p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$13;->val$preference:Landroid/preference/Preference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public click(I)V
    .locals 3
    .param p1, "buttonType"    # I

    .prologue
    const/4 v1, 0x0

    .line 1664
    const/16 v0, 0xa

    if-ne p1, v0, :cond_2

    .line 1665
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$13;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateCheckCurrentCityLocation(Landroid/content/Context;I)I

    .line 1667
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$13;->val$preference:Landroid/preference/Preference;

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 1669
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$13;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getNotificationSettings(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1670
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$13;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->unregisterNotiTime(Landroid/content/Context;)V

    .line 1673
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$13;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->init()V
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->access$800(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V

    .line 1674
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$13;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.widgetapp.ap.hero.accuweather.action.CHANGE_SETTING"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->sendBroadcast(Landroid/content/Intent;)V

    .line 1682
    :cond_1
    :goto_0
    return-void

    .line 1678
    :cond_2
    const/16 v0, 0xb

    if-ne p1, v0, :cond_1

    .line 1679
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$13;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->hideAllDialog()V
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V

    .line 1680
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$13;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->init()V
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->access$800(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V

    goto :goto_0
.end method
