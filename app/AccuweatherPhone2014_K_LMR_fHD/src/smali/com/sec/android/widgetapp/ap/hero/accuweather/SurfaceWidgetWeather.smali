.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;
.super Lcom/samsung/surfacewidget/SurfaceWidgetClient;
.source "SurfaceWidgetWeather.java"


# static fields
.field static final DATE_FORMAT_CHANGED:Ljava/lang/String; = "clock.date_format_changed"

.field private static final HANDLE_MSG_UPDATE_UI_AFTER_SHOWING_MSG:I = 0x100

.field private static final MSG_SHOWING_TIME:J = 0x5dcL

.field static final NETWORK_SET_TIMEZONE:Ljava/lang/String; = "android.intent.action.NETWORK_SET_TIMEZONE"


# instance fields
.field private mInstance:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mIsFirst:Z

.field private mIsFirstContentRequestEvt:Z

.field private mIsOnRefreshing:Z

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field protected mRefreshListener:Landroid/view/View$OnClickListener;

.field private mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

.field private mRefreshUIHandler:Landroid/os/Handler;

.field private mResumeInstnceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mRoaming:I

.field private mTTReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 34
    invoke-direct {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;-><init>()V

    .line 40
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mIsFirst:Z

    .line 41
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mIsFirstContentRequestEvt:Z

    .line 43
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mInstance:Ljava/util/ArrayList;

    .line 49
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mTTReceiver:Landroid/content/BroadcastReceiver;

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mResumeInstnceList:Ljava/util/ArrayList;

    .line 54
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mIsOnRefreshing:Z

    .line 55
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mRoaming:I

    .line 58
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$1;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 127
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$2;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mRefreshListener:Landroid/view/View$OnClickListener;

    .line 142
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$3;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$3;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mRefreshUIHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->doChangeDayOrNight()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;
    .param p1, "x1"    # Z

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->updateClock(Z)V

    return-void
.end method

.method static synthetic access$1002(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;
    .param p1, "x1"    # Z

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mIsOnRefreshing:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mIsFirstContentRequestEvt:Z

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;
    .param p1, "x1"    # I

    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->layoutSurfaceWidgetsViews(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;
    .param p1, "x1"    # Z

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->updateView(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;
    .param p1, "x1"    # I

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->showMsg(I)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;IJ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;
    .param p1, "x1"    # I
    .param p2, "x2"    # J

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->showMsg(IJ)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->stopRefreshIcon()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mRoaming:I

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;
    .param p1, "x1"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mRoaming:I

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;
    .param p1, "x1"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->startRefreshIcon()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mRefreshUIHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private doChangeDayOrNight()Z
    .locals 5

    .prologue
    .line 479
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "doChangeDayOrNight : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mInstance:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 480
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mInstance:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 481
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mInstance:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 482
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mInstance:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->getWeatherRenderer(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;

    move-result-object v1

    .line 483
    .local v1, "r":Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;
    if-eqz v1, :cond_0

    .line 484
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->doChangeDayOrNight()Z

    move-result v2

    .line 488
    .end local v0    # "index":I
    .end local v1    # "r":Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;
    :goto_1
    return v2

    .line 481
    .restart local v0    # "index":I
    .restart local v1    # "r":Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 488
    .end local v0    # "index":I
    .end local v1    # "r":Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method

.method private getWeatherRenderer(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;
    .locals 4
    .param p1, "instance"    # I

    .prologue
    .line 364
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getWeatherRenderer : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    invoke-super {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRenderer(I)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    move-result-object v0

    .line 367
    .local v0, "r":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    if-eqz v0, :cond_1

    .line 368
    instance-of v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;

    if-eqz v1, :cond_0

    .line 369
    check-cast v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;

    .line 376
    .end local v0    # "r":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    :goto_0
    return-object v0

    .line 371
    .restart local v0    # "r":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    :cond_0
    const-string v1, ""

    const-string v2, "getWeatherRenderer : r isn\'t WeatherRenderer"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 373
    :cond_1
    const-string v1, ""

    const-string v2, "getWeatherRenderer : don\'t have r"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private registerReceiver()V
    .locals 2

    .prologue
    .line 390
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 391
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.TIME_SET"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 392
    const-string v1, "android.intent.action.DATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 393
    const-string v1, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 394
    const-string v1, "clock.date_format_changed"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 396
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 397
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 398
    const-string v1, "com.sec.android.daemonapp.action.DAEMON_AUTOREFRESH_START"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 399
    const-string v1, "com.sec.android.daemonapp.action.DAEMON_AUTOREFRESH_END"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 400
    const-string v1, "com.sec.android.widgetapp.ap.hero.accuweather.action.GET_CURRENT_LOCATION_OK"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 401
    const-string v1, "com.sec.android.widgetapp.ap.hero.accuweather.action.GET_CURRENT_LOCATION_START"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 402
    const-string v1, "com.sec.android.widgetapp.ap.hero.accuweather.action.GET_CURRENT_LOCATION_ERROR"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 403
    const-string v1, "com.sec.android.widgetapp.ap.hero.accuweather.action.GET_DATA_NETWORK_ERROR"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 404
    const-string v1, "com.sec.android.widgetapp.ap.hero.accuweather.action.DISABLE_LOCATION_SERVICES"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 405
    const-string v1, "com.sec.android.widgetapp.ap.hero.accuweather.action.START_ACTIVITY_FROM_ACCOUNTING_DIALOG"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 407
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 409
    const-string v1, "com.sec.android.daemonapp.action.REMOTE_WIDGET_MANUAL_REFRESH_END"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 411
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 412
    return-void
.end method

.method private showMsg(I)V
    .locals 5
    .param p1, "id"    # I

    .prologue
    .line 415
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "showMsg : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mInstance:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mInstance:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 417
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mInstance:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 418
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mInstance:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->getWeatherRenderer(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;

    move-result-object v1

    .line 419
    .local v1, "r":Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;
    if-eqz v1, :cond_0

    .line 420
    invoke-virtual {v1, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->showMessage(I)V

    .line 417
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 424
    .end local v0    # "index":I
    .end local v1    # "r":Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;
    :cond_1
    return-void
.end method

.method private showMsg(IJ)V
    .locals 3
    .param p1, "id"    # I
    .param p2, "time"    # J

    .prologue
    const/16 v2, 0x100

    .line 427
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->showMsg(I)V

    .line 429
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-lez v0, :cond_0

    .line 430
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mRefreshUIHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 431
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mRefreshUIHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2, p2, p3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 433
    :cond_0
    return-void
.end method

.method private startRefreshIcon()V
    .locals 5

    .prologue
    .line 455
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "startRefreshIcon : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mInstance:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 456
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mInstance:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 457
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mInstance:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 458
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mInstance:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->getWeatherRenderer(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;

    move-result-object v1

    .line 459
    .local v1, "r":Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;
    if-eqz v1, :cond_0

    .line 460
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->startRefresh()V

    .line 457
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 464
    .end local v0    # "index":I
    .end local v1    # "r":Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;
    :cond_1
    return-void
.end method

.method private stopRefreshIcon()V
    .locals 5

    .prologue
    .line 467
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "stopRefreshIcon : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mInstance:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 468
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mInstance:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 469
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mInstance:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 470
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mInstance:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->getWeatherRenderer(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;

    move-result-object v1

    .line 471
    .local v1, "r":Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;
    if-eqz v1, :cond_0

    .line 472
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->stopRefresh()V

    .line 469
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 476
    .end local v0    # "index":I
    .end local v1    # "r":Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;
    :cond_1
    return-void
.end method

.method private unregisterReceiver()V
    .locals 1

    .prologue
    .line 383
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 384
    return-void
.end method

.method private updateClock(Z)V
    .locals 5
    .param p1, "isChangeTimeZone"    # Z

    .prologue
    .line 496
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "makeClock : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mInstance:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 497
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mInstance:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 498
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mInstance:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 499
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mInstance:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->getWeatherRenderer(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;

    move-result-object v1

    .line 500
    .local v1, "r":Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;
    if-eqz v1, :cond_0

    .line 501
    invoke-virtual {v1, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->updateClock(Z)V

    .line 498
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 505
    .end local v0    # "index":I
    .end local v1    # "r":Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;
    :cond_1
    return-void
.end method

.method private updateView(Z)V
    .locals 5
    .param p1, "isStartAnim"    # Z

    .prologue
    .line 436
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateView : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mInstance:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 437
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mInstance:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 438
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mInstance:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 439
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mInstance:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->getWeatherRenderer(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;

    move-result-object v1

    .line 440
    .local v1, "r":Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;
    if-eqz v1, :cond_0

    .line 441
    invoke-virtual {v1, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->updateView(Z)V

    .line 438
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 446
    .end local v1    # "r":Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;
    :cond_1
    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mRoaming:I

    if-nez v2, :cond_2

    .line 447
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->handleConnectivityUpdate(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_1
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mRoaming:I

    .line 448
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "uV mIR : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mRoaming:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 452
    .end local v0    # "index":I
    :cond_2
    return-void

    .line 447
    .restart local v0    # "index":I
    :cond_3
    const/4 v2, -0x1

    goto :goto_1
.end method


# virtual methods
.method public createRenderer(Landroid/content/Context;Landroid/content/res/Resources;ILandroid/os/Handler;)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resources"    # Landroid/content/res/Resources;
    .param p3, "instance"    # I
    .param p4, "uiHandler"    # Landroid/os/Handler;

    .prologue
    const/4 v3, -0x1

    .line 288
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[SWW] createRenderer : instance = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$4;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$4;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;)V

    .line 308
    .local v5, "listener":Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$DrawListener;
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mInstance:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mInstance:Ljava/util/ArrayList;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 309
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "createRenderer : mI = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mInstance:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mInstance:Ljava/util/ArrayList;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 312
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mInstance:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 313
    invoke-static {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->addPreferenceWidgetCountForSurfaceWidget(Landroid/content/Context;)V

    .line 317
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mResumeInstnceList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mResumeInstnceList:Ljava/util/ArrayList;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ne v0, v3, :cond_1

    .line 318
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mResumeInstnceList:Ljava/util/ArrayList;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 319
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "createRenderer : mRIL = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mResumeInstnceList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    :cond_1
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;-><init>(Landroid/content/Context;Landroid/content/res/Resources;ILandroid/os/Handler;Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$DrawListener;)V

    return-object v0
.end method

.method protected customMessage(Landroid/os/Message;)Z
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 508
    const/4 v0, 0x1

    return v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3
    .param p1, "aIntent"    # Landroid/content/Intent;

    .prologue
    .line 181
    const-string v0, ""

    const-string v1, "[SWW] Bind : "

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;->setWidgetAtDaemon(Landroid/content/Context;ZZ)V

    .line 183
    invoke-super {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->onBind(Landroid/content/Intent;)Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method protected onContentRequest(I)V
    .locals 6
    .param p1, "instance"    # I

    .prologue
    .line 326
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[SWW] onContentRequest : instance = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    :try_start_0
    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    .line 329
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->getWeatherRenderer(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;

    move-result-object v2

    .line 330
    .local v2, "wr":Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;
    if-eqz v2, :cond_0

    .line 331
    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->getSceneManager()Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    move-result-object v1

    .line 332
    .local v1, "sm":Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;
    if-eqz v1, :cond_0

    iget-boolean v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mIsFirstContentRequestEvt:Z

    if-nez v3, :cond_0

    .line 333
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->getAnimationManager()Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/AnimationManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/AnimationManager;->cancleAnimation()V

    .line 334
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->cancelIconAnimation(Landroid/view/ViewGroup;)V

    .line 335
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->clearRootView()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 341
    .end local v1    # "sm":Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;
    .end local v2    # "wr":Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;
    :cond_0
    :goto_0
    const v3, 0x7f030036

    invoke-super {p0, p1, v3}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->setContentView(II)V

    .line 343
    return-void

    .line 338
    :catch_0
    move-exception v0

    .line 339
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 169
    const-string v0, ""

    const-string v1, "[SWW] Create : "

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    invoke-super {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->onCreate()V

    .line 171
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mIsFirstContentRequestEvt:Z

    .line 173
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->registerReceiver()V

    .line 176
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mIsOnRefreshing:Z

    .line 178
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 196
    const-string v0, ""

    const-string v1, "[SWW] onDestroy "

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->unregisterReceiver()V

    .line 198
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mIsFirstContentRequestEvt:Z

    .line 200
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->onDestroy()V

    .line 204
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mIsFirst:Z

    .line 205
    invoke-super {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->onDestroy()V

    .line 206
    return-void
.end method

.method protected onDestroy(IZ)V
    .locals 4
    .param p1, "instance"    # I
    .param p2, "isRemovedFromIdle"    # Z

    .prologue
    .line 265
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[SWW] onDestroy : instance = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", fromIdle = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mInstance:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 269
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mInstance:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 270
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mInstance:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, p1, :cond_2

    .line 271
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mInstance:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 278
    .end local v0    # "i":I
    :cond_0
    if-eqz p2, :cond_1

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mInstance:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mInstance:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 279
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->removePreferenceWidgetCountForSurfaceWidget(Landroid/content/Context;)V

    .line 281
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->unRegisterTTReceiver()V

    .line 283
    invoke-super {p0, p1, p2}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->onDestroy(IZ)V

    .line 284
    return-void

    .line 269
    .restart local v0    # "i":I
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected onFinishInflate(ILandroid/view/View;)V
    .locals 5
    .param p1, "instance"    # I
    .param p2, "rootView"    # Landroid/view/View;

    .prologue
    .line 346
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[SWW] onFinishInflate : instance = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mIsFCREvt = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mIsFirstContentRequestEvt:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->getWeatherRenderer(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;

    move-result-object v1

    .line 348
    .local v1, "wr":Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;
    if-eqz v1, :cond_2

    .line 349
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/WeatherRenderer;->getSceneManager()Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    move-result-object v0

    .line 350
    .local v0, "sm":Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;
    if-eqz v0, :cond_1

    .line 351
    iget-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mIsFirstContentRequestEvt:Z

    if-nez v2, :cond_0

    .line 352
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setResizeStep(I)V

    .line 354
    :cond_0
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mIsFirstContentRequestEvt:Z

    .line 355
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mRefreshListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, p1, p2, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->onInflateView(ILandroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 360
    .end local v0    # "sm":Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;
    :cond_1
    :goto_0
    return-void

    .line 358
    :cond_2
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[SWW] onFinishInflate : instance = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", weatherRenderer is null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected onPause(I)V
    .locals 4
    .param p1, "instance"    # I

    .prologue
    .line 239
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[SWW] onPause : instance = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mResumeInstnceList:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mResumeInstnceList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 250
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mResumeInstnceList:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 251
    .local v0, "idx":I
    const/4 v1, -0x1

    if-le v0, v1, :cond_0

    .line 252
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mResumeInstnceList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 254
    :cond_0
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onPause : size = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mResumeInstnceList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    .end local v0    # "idx":I
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mResumeInstnceList:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mResumeInstnceList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_3

    .line 258
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->unRegisterTTReceiver()V

    .line 261
    :cond_3
    invoke-super {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->onPause(I)V

    .line 262
    return-void
.end method

.method protected onResume(I)V
    .locals 5
    .param p1, "instance"    # I

    .prologue
    .line 209
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[SWW] onResume : instance = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    if-nez v2, :cond_0

    .line 212
    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    .line 215
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getAllCityListCount(Landroid/content/Context;)I

    move-result v1

    .line 216
    .local v1, "weatherCount":I
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mIsFirst:Z

    if-nez v2, :cond_1

    .line 217
    sget-boolean v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isAutoCaptureToolSupportMode:Z

    if-eqz v2, :cond_4

    .line 218
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setFirstLaunch(Landroid/content/Context;Z)Z

    .line 223
    :cond_1
    :goto_0
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " onResume :: isFirst = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mIsFirst:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", cnt = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mResumeInstnceList:Ljava/util/ArrayList;

    if-eqz v2, :cond_3

    .line 226
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mResumeInstnceList:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 227
    .local v0, "idx":I
    const/4 v2, -0x1

    if-ne v0, v2, :cond_2

    .line 228
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mResumeInstnceList:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 230
    :cond_2
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onResume : size = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mResumeInstnceList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    .end local v0    # "idx":I
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->registerTTReceiver()V

    .line 235
    invoke-super {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->onResume(I)V

    .line 236
    return-void

    .line 220
    :cond_4
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    const/16 v3, 0xfa0

    invoke-virtual {v2, v1, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->onFirstUpdate(II)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mIsFirst:Z

    goto :goto_0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x0

    .line 187
    const-string v0, ""

    const-string v1, "[SWW] onUnbind "

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getPreferenceWidgetCount(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_0

    .line 190
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;->setWidgetAtDaemon(Landroid/content/Context;ZZ)V

    .line 192
    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method public registerTTReceiver()V
    .locals 3

    .prologue
    .line 512
    const-string v1, ""

    const-string v2, " registerTTReceiver ! "

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 513
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mTTReceiver:Landroid/content/BroadcastReceiver;

    if-nez v1, :cond_0

    .line 514
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 515
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.TIME_TICK"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 516
    const-string v1, "android.intent.action.NETWORK_SET_TIMEZONE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 518
    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$5;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$5;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;)V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mTTReceiver:Landroid/content/BroadcastReceiver;

    .line 539
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mTTReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 542
    .end local v0    # "filter":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method public startRefreshingNetworkChange(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x1

    .line 554
    if-nez p1, :cond_0

    .line 555
    const-string v2, ""

    const-string v3, "sRNC context n"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 587
    :goto_0
    return-void

    .line 559
    :cond_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    if-nez v2, :cond_1

    .line 560
    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    .line 563
    :cond_1
    iget-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mIsOnRefreshing:Z

    if-nez v2, :cond_5

    .line 564
    invoke-static {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getSettingDataRoaming(Landroid/content/Context;)I

    move-result v1

    .line 565
    .local v1, "settingDataRoaming":I
    invoke-static {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getRefreshRoamingSettings(Landroid/content/Context;)I

    move-result v0

    .line 567
    .local v0, "refreshRoamingSetting":I
    if-ne v1, v6, :cond_4

    if-ne v0, v6, :cond_4

    .line 569
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    if-eqz v2, :cond_3

    .line 570
    invoke-static {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getLocation(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 571
    const-string v2, ""

    const-string v3, "sRNC refresh roaming"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 573
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mRefreshUIHandler:Landroid/os/Handler;

    const-string v5, "cityId:current"

    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->doManualRefresh(ILandroid/os/Handler;Ljava/lang/String;)V

    .line 574
    iput-boolean v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mIsOnRefreshing:Z

    goto :goto_0

    .line 576
    :cond_2
    const-string v2, ""

    const-string v3, "sRNC no cur city"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 579
    :cond_3
    const-string v2, ""

    const-string v3, "sRNC mRM n"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 582
    :cond_4
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sRNC sDR : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " rRS : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 585
    .end local v0    # "refreshRoamingSetting":I
    .end local v1    # "settingDataRoaming":I
    :cond_5
    const-string v2, ""

    const-string v3, "sRNC mIsOnRefreshing true"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public unRegisterTTReceiver()V
    .locals 2

    .prologue
    .line 545
    const-string v0, ""

    const-string v1, " unRegisterTTReceiver !! "

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 546
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mTTReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 547
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mTTReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 548
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mTTReceiver:Landroid/content/BroadcastReceiver;

    .line 550
    :cond_0
    return-void
.end method
