.class Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$25;
.super Ljava/lang/Object;
.source "MenuSearch.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->showLocatingDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 1010
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$25;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    const/4 v2, 0x1

    .line 1012
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$25;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mLoadingDialog:Landroid/app/Dialog;

    .line 1013
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$25;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mRequestCanceled:Z
    invoke-static {v0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$2602(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;Z)Z

    .line 1015
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$25;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mcl:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$3000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1016
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$25;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mcl:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$3000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->cancelGetLastLocation(I)V

    .line 1018
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$25;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->stopHttpThread()V
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$2900(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    .line 1020
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$25;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->enableEditField()V
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$1300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    .line 1021
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$25;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 1022
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$25;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->gps_btn:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Landroid/widget/RelativeLayout;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1023
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$25;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->gps_btn:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 1025
    :cond_1
    return-void
.end method
