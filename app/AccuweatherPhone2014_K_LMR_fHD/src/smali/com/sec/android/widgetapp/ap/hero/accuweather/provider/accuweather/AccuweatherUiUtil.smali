.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuweatherUiUtil;
.super Ljava/lang/Object;
.source "AccuweatherUiUtil.java"


# instance fields
.field private localeArray:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const/16 v0, 0x22

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "en#en-US"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "en_US#en-us"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "en_GB#en-gb"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "es#es"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "fr#fr"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "da#da"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "pt#pt"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "nl#nl"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "nb#no"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "it#it"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "de#de"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "sv#sv"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "fi#fi"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "zh#zh-cn"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "zh_CN#zh-cn"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "zh_TW#zh-tw"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "sk#sk"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "ro#ro"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "cs#cs"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "hu#hu"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "pl#pl"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "pt_BR#pt-br"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "hi#in"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "ru#ru"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "ar#ar"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "el#el"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "ja#ja"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "ko#ka"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "tr#tr"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "fr_CA#fr-ca"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "iw#he"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuweatherUiUtil;->localeArray:[Ljava/lang/String;

    return-void
.end method

.method private findLangString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "loc"    # Ljava/lang/String;

    .prologue
    .line 37
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuweatherUiUtil;->localeArray:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 38
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuweatherUiUtil;->localeArray:[Ljava/lang/String;

    aget-object v2, v2, v0

    const-string v3, "#"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 39
    .local v1, "vals":[Ljava/lang/String;
    array-length v2, v1

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 40
    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 41
    const/4 v2, 0x1

    aget-object v2, v1, v2

    .line 46
    .end local v1    # "vals":[Ljava/lang/String;
    :goto_1
    return-object v2

    .line 37
    .restart local v1    # "vals":[Ljava/lang/String;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 46
    .end local v1    # "vals":[Ljava/lang/String;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method


# virtual methods
.method public getLanguageString(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    .line 25
    .local v0, "localeString":Ljava/lang/String;
    invoke-direct {p0, p1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuweatherUiUtil;->findLangString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 26
    .local v1, "result":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 27
    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuweatherUiUtil;->findLangString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 28
    if-nez v1, :cond_0

    .line 29
    const-string v2, "en-us"

    .line 33
    :goto_0
    return-object v2

    :cond_0
    move-object v2, v1

    goto :goto_0
.end method
