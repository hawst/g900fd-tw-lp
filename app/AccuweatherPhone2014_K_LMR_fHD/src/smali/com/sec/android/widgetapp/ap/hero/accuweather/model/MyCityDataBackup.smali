.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityDataBackup;
.super Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;
.source "MyCityDataBackup.java"


# instance fields
.field public info:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;


# direct methods
.method public constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;)V
    .locals 22
    .param p1, "d"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;

    .prologue
    .line 21
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->name:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->state:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->location:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->time:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->date:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->weatherDesc:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->tempCurrent:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->tempLow:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->tempHigh:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-boolean v11, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->isCurrentLocation:Z

    move-object/from16 v0, p1

    iget v12, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->weatherIcon:I

    .line 22
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getDst()I

    move-result v13

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->realLocation:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->summerTime:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->sunRaiseTime:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->sunSetTime:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->timeZone:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->latitude:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->longitude:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->xmldetailinfo:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v1, p0

    .line 21
    invoke-direct/range {v1 .. v21}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "state"    # Ljava/lang/String;
    .param p3, "location"    # Ljava/lang/String;
    .param p4, "time"    # Ljava/lang/String;
    .param p5, "date"    # Ljava/lang/String;
    .param p6, "weatherDesc"    # Ljava/lang/String;
    .param p7, "tempCurrent"    # Ljava/lang/String;
    .param p8, "tempLow"    # Ljava/lang/String;
    .param p9, "tempHigh"    # Ljava/lang/String;
    .param p10, "isCurrentLocation"    # Z
    .param p11, "weatherIcon"    # I
    .param p12, "dst"    # I
    .param p13, "realLocation"    # Ljava/lang/String;
    .param p14, "summerTime"    # Ljava/lang/String;
    .param p15, "sunRaiseTime"    # Ljava/lang/String;
    .param p16, "sunSetTime"    # Ljava/lang/String;
    .param p17, "timeZone"    # Ljava/lang/String;
    .param p18, "latitude"    # Ljava/lang/String;
    .param p19, "longitude"    # Ljava/lang/String;
    .param p20, "xmldetailinfo"    # Ljava/lang/String;

    .prologue
    .line 14
    invoke-direct/range {p0 .. p20}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    return-void
.end method
