.class Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$8;
.super Ljava/lang/Object;
.source "MenuSettings.java"

# interfaces
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    .prologue
    .line 1592
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$8;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public click(I)V
    .locals 3
    .param p1, "buttonType"    # I

    .prologue
    .line 1594
    const/16 v0, 0xa

    if-ne p1, v0, :cond_2

    .line 1595
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$8;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getLocationCheck(Landroid/content/Context;)I

    move-result v0

    const/16 v1, 0xbbb

    if-ne v0, v1, :cond_1

    .line 1596
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$8;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$8;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$8$1;

    invoke-direct {v2, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$8$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$8;)V

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->showGPSoffDialog(Landroid/app/Activity;Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)Landroid/app/Dialog;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->mPopupDialog:Landroid/app/Dialog;

    .line 1615
    :cond_0
    :goto_0
    return-void

    .line 1607
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$8;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    .line 1608
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getCheckCurrentCityLocation(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_0

    .line 1609
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$8;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getCurrentLocation()V
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->access$1000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V

    goto :goto_0

    .line 1611
    :cond_2
    const/16 v0, 0xb

    if-ne p1, v0, :cond_0

    .line 1612
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$8;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->hideAllDialog()V
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V

    .line 1613
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$8;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->init()V
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->access$800(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;)V

    goto :goto_0
.end method
