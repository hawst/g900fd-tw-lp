.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "CityDBHelper.java"


# static fields
.field private static DB_NAME:Ljava/lang/String; = null

.field private static final PACKAGE_DATABASE_DIR:Ljava/lang/String; = "/data/data/com.sec.android.widgetapp.ap.hero.accuweather/databases/"

.field private static mVersion:I


# instance fields
.field private final mCTContext:Landroid/content/Context;

.field private mForceCopy:Z

.field private mResultType:I

.field private mSQLDBCityDataBase:Landroid/database/sqlite/SQLiteDatabase;

.field private mStrDBPath:Ljava/lang/String;

.field myInput:Ljava/io/InputStream;

.field myOutput:Ljava/io/OutputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-string v0, "worldcity.db"

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->DB_NAME:Ljava/lang/String;

    .line 38
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->mVersion:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, -0x1

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 47
    sget-object v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->DB_NAME:Ljava/lang/String;

    sget v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->mVersion:I

    invoke-direct {p0, p1, v5, v8, v6}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 26
    const-string v5, ""

    iput-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->mStrDBPath:Ljava/lang/String;

    .line 36
    iput v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->mResultType:I

    .line 40
    iput-boolean v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->mForceCopy:Z

    .line 42
    iput-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myInput:Ljava/io/InputStream;

    .line 44
    iput-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myOutput:Ljava/io/OutputStream;

    .line 48
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->mCTContext:Landroid/content/Context;

    .line 51
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 52
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v4

    .line 53
    .local v4, "path":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->close()V

    .line 55
    const/4 v5, 0x0

    const-string v6, "/"

    invoke-virtual {v4, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->mStrDBPath:Ljava/lang/String;

    .line 56
    const/4 v5, 0x0

    iput v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->mResultType:I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 72
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v4    # "path":Ljava/lang/String;
    :goto_0
    return-void

    .line 57
    :catch_0
    move-exception v2

    .line 58
    .local v2, "fe":Landroid/database/sqlite/SQLiteFullException;
    iput v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->mResultType:I

    .line 60
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CityDBHelper()"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V

    goto :goto_0

    .line 62
    .end local v2    # "fe":Landroid/database/sqlite/SQLiteFullException;
    :catch_1
    move-exception v3

    .line 63
    .local v3, "ioe":Landroid/database/sqlite/SQLiteDiskIOException;
    iput v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->mResultType:I

    .line 65
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CityDBHelper()"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V

    goto :goto_0

    .line 67
    .end local v3    # "ioe":Landroid/database/sqlite/SQLiteDiskIOException;
    :catch_2
    move-exception v1

    .line 69
    .local v1, "e":Ljava/lang/Exception;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CityDBHelper()"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private checkDataBase()Z
    .locals 5

    .prologue
    .line 104
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->getDBFilePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 106
    .local v0, "dbFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v1

    const-wide/32 v3, 0xc350

    cmp-long v1, v1, v3

    if-gez v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myOutput:Ljava/io/OutputStream;

    if-nez v1, :cond_1

    .line 107
    const/4 v1, 0x0

    .line 109
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private copyDataBase()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 113
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->mCTContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v3

    sget-object v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->DB_NAME:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myInput:Ljava/io/InputStream;

    .line 114
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->mCTContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/databases/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->DB_NAME:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 116
    .local v2, "outFileName":Ljava/lang/String;
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myOutput:Ljava/io/OutputStream;

    .line 118
    const/16 v3, 0x400

    new-array v0, v3, [B

    .line 120
    .local v0, "buffer":[B
    :goto_0
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myInput:Ljava/io/InputStream;

    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    .local v1, "length":I
    if-lez v1, :cond_0

    .line 121
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myOutput:Ljava/io/OutputStream;

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4, v1}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_0

    .line 124
    :cond_0
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myOutput:Ljava/io/OutputStream;

    invoke-virtual {v3}, Ljava/io/OutputStream;->flush()V

    .line 125
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myOutput:Ljava/io/OutputStream;

    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V

    .line 126
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myInput:Ljava/io/InputStream;

    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 127
    return-void
.end method


# virtual methods
.method public declared-synchronized close()V
    .locals 1

    .prologue
    .line 326
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->mSQLDBCityDataBase:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_0

    .line 327
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->mSQLDBCityDataBase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 330
    :cond_0
    invoke-super {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 331
    monitor-exit p0

    return-void

    .line 326
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public createDataBase()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->checkDataBase()Z

    move-result v1

    .line 85
    .local v1, "dbExist":Z
    if-eqz v1, :cond_0

    iget-boolean v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->mForceCopy:Z

    if-nez v3, :cond_0

    .line 101
    :goto_0
    return-void

    .line 88
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 89
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 92
    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->copyDataBase()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 93
    :catch_0
    move-exception v2

    .line 94
    .local v2, "e":Ljava/io/IOException;
    const/4 v3, -0x1

    :try_start_1
    iput v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->mResultType:I

    .line 96
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CityDBHelper()"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 98
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    throw v3
.end method

.method public getAllLocCode()Ljava/util/ArrayList;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 478
    const/4 v10, 0x0

    .line 479
    .local v10, "cursor":Landroid/database/Cursor;
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 481
    .local v13, "loclist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->createDataBase()V

    .line 482
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->openDataBase()V

    .line 483
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->mSQLDBCityDataBase:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v1, 0x0

    const-string v2, "INFOALARM_WEATHER_CITY"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "LOCCODE"

    aput-object v5, v3, v4

    const-string v4, "ZOOMLV BETWEEN 5 AND 9"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 496
    if-eqz v10, :cond_2

    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 498
    :cond_0
    const/4 v0, 0x0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 499
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 500
    const/4 v0, 0x0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 526
    if-eqz v10, :cond_1

    .line 527
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 529
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->close()V

    .line 531
    :goto_0
    return-object v13

    .line 526
    :cond_2
    if-eqz v10, :cond_3

    .line 527
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 529
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->close()V

    goto :goto_0

    .line 504
    :catch_0
    move-exception v11

    .line 505
    .local v11, "e":Ljava/lang/Exception;
    if-eqz v10, :cond_4

    .line 506
    :try_start_1
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 509
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CityDBHelper.searchCity()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 512
    :try_start_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myInput:Ljava/io/InputStream;

    if-eqz v0, :cond_5

    .line 513
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myInput:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 514
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myInput:Ljava/io/InputStream;

    .line 516
    :cond_5
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myOutput:Ljava/io/OutputStream;

    if-eqz v0, :cond_6

    .line 517
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myOutput:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 518
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myOutput:Ljava/io/OutputStream;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 526
    :cond_6
    :goto_1
    if-eqz v10, :cond_7

    .line 527
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 529
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->close()V

    goto :goto_0

    .line 520
    :catch_1
    move-exception v12

    .line 522
    .local v12, "e1":Ljava/io/IOException;
    :try_start_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CityDBHelper.searchCity() FileStream close Exception"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 526
    .end local v11    # "e":Ljava/lang/Exception;
    .end local v12    # "e1":Ljava/io/IOException;
    :catchall_0
    move-exception v0

    if-eqz v10, :cond_8

    .line 527
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 529
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->close()V

    throw v0
.end method

.method public getDBFilePath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->mCTContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/databases/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->DB_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRegionByMCC(Ljava/lang/String;)I
    .locals 17
    .param p1, "mcc"    # Ljava/lang/String;

    .prologue
    .line 401
    const/16 v16, 0x7

    .line 402
    .local v16, "region":I
    const/4 v12, 0x0

    .local v12, "cursor":Landroid/database/Cursor;
    const/4 v13, 0x0

    .line 403
    .local v13, "cursor2":Landroid/database/Cursor;
    const-string v11, ""

    .line 405
    .local v11, "country":Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->createDataBase()V

    .line 406
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->openDataBase()V

    .line 407
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->mSQLDBCityDataBase:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v2, 0x0

    const-string v3, "mcc_code"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "COUNTRYNAME"

    aput-object v6, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "MCC1="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-string v10, "1"

    invoke-virtual/range {v1 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 420
    if-eqz v12, :cond_1

    .line 421
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 422
    const/4 v1, 0x0

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 425
    :cond_0
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 426
    const/4 v12, 0x0

    .line 429
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->mSQLDBCityDataBase:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v2, 0x0

    const-string v3, "INFOALARM_WEATHER_CITY"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "REGION_id"

    aput-object v6, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "COUNTRYNAME=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-string v10, "1"

    invoke-virtual/range {v1 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 442
    if-eqz v13, :cond_2

    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 443
    const/4 v1, 0x0

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 444
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "region code "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 464
    :cond_2
    if-eqz v12, :cond_3

    .line 465
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 467
    :cond_3
    if-eqz v13, :cond_4

    .line 468
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 470
    :cond_4
    const/4 v12, 0x0

    .line 471
    const/4 v13, 0x0

    .line 472
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->close()V

    .line 474
    :goto_0
    return v16

    .line 446
    :catch_0
    move-exception v14

    .line 448
    .local v14, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CityDBHelper.searchCity()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 450
    :try_start_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myInput:Ljava/io/InputStream;

    if-eqz v1, :cond_5

    .line 451
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myInput:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 452
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myInput:Ljava/io/InputStream;

    .line 454
    :cond_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myOutput:Ljava/io/OutputStream;

    if-eqz v1, :cond_6

    .line 455
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myOutput:Ljava/io/OutputStream;

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 456
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myOutput:Ljava/io/OutputStream;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 464
    :cond_6
    :goto_1
    if-eqz v12, :cond_7

    .line 465
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 467
    :cond_7
    if-eqz v13, :cond_8

    .line 468
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 470
    :cond_8
    const/4 v12, 0x0

    .line 471
    const/4 v13, 0x0

    .line 472
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->close()V

    goto :goto_0

    .line 458
    :catch_1
    move-exception v15

    .line 460
    .local v15, "e1":Ljava/io/IOException;
    :try_start_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CityDBHelper.searchCity() FileStream close Exception"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 464
    .end local v14    # "e":Ljava/lang/Exception;
    .end local v15    # "e1":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    if-eqz v12, :cond_9

    .line 465
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 467
    :cond_9
    if-eqz v13, :cond_a

    .line 468
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 470
    :cond_a
    const/4 v12, 0x0

    .line 471
    const/4 v13, 0x0

    .line 472
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->close()V

    throw v1
.end method

.method public getRegionCities(I)Ljava/util/ArrayList;
    .locals 14
    .param p1, "regioncode"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 346
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 347
    .local v13, "info":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;>;"
    const/4 v10, 0x0

    .line 350
    .local v10, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->createDataBase()V

    .line 351
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->openDataBase()V

    .line 352
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->mSQLDBCityDataBase:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v1, 0x0

    const-string v2, "INFOALARM_WEATHER_CITY"

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "LOCCODE"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "CITYNAME"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "STATE"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, "COUNTRYNAME"

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "ZOOMLV"

    aput-object v5, v3, v4

    const-string v4, "ZOOMLV between 5 and 9"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 364
    if-eqz v10, :cond_2

    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 366
    :cond_0
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    const/4 v1, 0x1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x3

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x2

    .line 367
    invoke-interface {v10, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x4

    .line 368
    invoke-interface {v10, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 366
    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 369
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 392
    if-eqz v10, :cond_1

    .line 393
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 395
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->close()V

    .line 397
    .end local v13    # "info":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;>;"
    :goto_0
    return-object v13

    .line 371
    .restart local v13    # "info":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;>;"
    :cond_2
    const/4 v13, 0x0

    .line 392
    .end local v13    # "info":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;>;"
    if-eqz v10, :cond_3

    .line 393
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 395
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->close()V

    goto :goto_0

    .line 373
    .restart local v13    # "info":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;>;"
    :catch_0
    move-exception v11

    .line 375
    .local v11, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CityDBHelper.searchCity()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 378
    :try_start_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myInput:Ljava/io/InputStream;

    if-eqz v0, :cond_4

    .line 379
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myInput:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 380
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myInput:Ljava/io/InputStream;

    .line 382
    :cond_4
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myOutput:Ljava/io/OutputStream;

    if-eqz v0, :cond_5

    .line 383
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myOutput:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 384
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myOutput:Ljava/io/OutputStream;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 392
    :cond_5
    :goto_1
    if-eqz v10, :cond_6

    .line 393
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 395
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->close()V

    goto :goto_0

    .line 386
    :catch_1
    move-exception v12

    .line 388
    .local v12, "e1":Ljava/io/IOException;
    :try_start_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CityDBHelper.searchCity() FileStream close Exception"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 392
    .end local v11    # "e":Ljava/lang/Exception;
    .end local v12    # "e1":Ljava/io/IOException;
    :catchall_0
    move-exception v0

    if-eqz v10, :cond_7

    .line 393
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 395
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->close()V

    throw v0
.end method

.method public getResultType()I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->mResultType:I

    return v0
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "arg0"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 334
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1
    .param p1, "arg0"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I

    .prologue
    .line 338
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->mForceCopy:Z

    .line 340
    return-void
.end method

.method public openDataBase()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 130
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->mCTContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/databases/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->DB_NAME:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 131
    .local v0, "myPath":Ljava/lang/String;
    const/4 v1, 0x0

    const/16 v2, 0x10

    invoke-static {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->openDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->mSQLDBCityDataBase:Landroid/database/sqlite/SQLiteDatabase;

    .line 133
    return-void
.end method

.method public searchCity(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 25
    .param p1, "city"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 136
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 137
    .local v23, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;>;"
    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    .line 138
    .local v24, "list_end":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;>;"
    const/16 v16, 0x0

    .line 139
    .local v16, "cursor":Landroid/database/Cursor;
    const/16 v17, 0x0

    .line 141
    .local v17, "cursor2":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->createDataBase()V

    .line 142
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->openDataBase()V

    .line 144
    const-string v2, "%\""

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v20

    .line 145
    .local v20, "end":I
    const/16 v2, 0xf

    move/from16 v0, v20

    if-le v0, v2, :cond_0

    .line 146
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v15

    .line 148
    .local v15, "cityname":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->mSQLDBCityDataBase:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v3, 0x0

    const-string v4, "INFOALARM_WEATHER_CITY"

    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "CITYNAME"

    aput-object v8, v5, v7

    const/4 v7, 0x1

    const-string v8, "COUNTRYNAME"

    aput-object v8, v5, v7

    const/4 v7, 0x2

    const-string v8, "STATE"

    aput-object v8, v5, v7

    const/4 v7, 0x3

    const-string v8, "LOCCODE"

    aput-object v8, v5, v7

    const/4 v7, 0x4

    const-string v8, "_id"

    aput-object v8, v5, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-string v10, "CITYNAME ASC"

    const/4 v11, 0x0

    move-object/from16 v6, p1

    invoke-virtual/range {v2 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 152
    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    .line 153
    new-instance v14, Ljava/lang/StringBuffer;

    invoke-direct {v14}, Ljava/lang/StringBuffer;-><init>()V

    .line 154
    .local v14, "buf":Ljava/lang/StringBuffer;
    const-string v2, "CITYNAME LIKE \"%"

    invoke-virtual {v14, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 155
    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 156
    const-string v2, "%\""

    invoke-virtual {v14, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 157
    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    .line 159
    .local v6, "bufs":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->mSQLDBCityDataBase:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v3, 0x0

    const-string v4, "INFOALARM_WEATHER_CITY"

    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "CITYNAME"

    aput-object v8, v5, v7

    const/4 v7, 0x1

    const-string v8, "COUNTRYNAME"

    aput-object v8, v5, v7

    const/4 v7, 0x2

    const-string v8, "STATE"

    aput-object v8, v5, v7

    const/4 v7, 0x3

    const-string v8, "LOCCODE"

    aput-object v8, v5, v7

    const/4 v7, 0x4

    const-string v8, "_id"

    aput-object v8, v5, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-string v10, "CITYNAME ASC"

    const/4 v11, 0x0

    invoke-virtual/range {v2 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    .line 166
    .end local v6    # "bufs":Ljava/lang/String;
    .end local v14    # "buf":Ljava/lang/StringBuffer;
    .end local v15    # "cityname":Ljava/lang/String;
    :cond_0
    if-eqz v16, :cond_5

    .line 167
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToFirst()Z

    .line 169
    :goto_0
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_5

    .line 170
    new-instance v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    const-string v2, "CITYNAME"

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "STATE"

    .line 171
    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "COUNTRYNAME"

    .line 172
    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v2, "LOCCODE"

    .line 173
    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-direct/range {v7 .. v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 170
    move-object/from16 v0, v23

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 175
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 200
    .end local v20    # "end":I
    :catch_0
    move-exception v18

    .line 202
    .local v18, "e":Ljava/io/IOException;
    :try_start_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CityDBHelper.searchCity()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 205
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myInput:Ljava/io/InputStream;

    if-eqz v2, :cond_1

    .line 206
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myInput:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 207
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myInput:Ljava/io/InputStream;

    .line 209
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myOutput:Ljava/io/OutputStream;

    if-eqz v2, :cond_2

    .line 210
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myOutput:Ljava/io/OutputStream;

    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    .line 211
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myOutput:Ljava/io/OutputStream;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 219
    :cond_2
    :goto_1
    if-eqz v16, :cond_3

    .line 220
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 222
    :cond_3
    if-eqz v17, :cond_4

    .line 223
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    .line 225
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->close()V

    .line 227
    .end local v18    # "e":Ljava/io/IOException;
    :goto_2
    return-object v23

    .line 179
    .restart local v20    # "end":I
    :cond_5
    if-eqz v17, :cond_c

    if-eqz v16, :cond_c

    .line 181
    :try_start_3
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToFirst()Z

    .line 182
    :goto_3
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_8

    .line 183
    new-instance v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    const-string v2, "CITYNAME"

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "STATE"

    .line 184
    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "COUNTRYNAME"

    .line 185
    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v2, "LOCCODE"

    .line 186
    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-direct/range {v7 .. v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 183
    move-object/from16 v0, v24

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 188
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 219
    .end local v20    # "end":I
    :catchall_0
    move-exception v2

    if-eqz v16, :cond_6

    .line 220
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 222
    :cond_6
    if-eqz v17, :cond_7

    .line 223
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    .line 225
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->close()V

    throw v2

    .line 190
    .restart local v20    # "end":I
    :cond_8
    const/16 v21, 0x0

    .local v21, "i":I
    :goto_4
    :try_start_4
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, v21

    if-ge v0, v2, :cond_b

    .line 191
    const/16 v22, 0x0

    .local v22, "j":I
    :goto_5
    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, v22

    if-ge v0, v2, :cond_a

    .line 192
    move-object/from16 v0, v23

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getCity()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v24

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getCity()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 193
    move-object/from16 v0, v24

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 191
    :cond_9
    add-int/lit8 v22, v22, 0x1

    goto :goto_5

    .line 190
    :cond_a
    add-int/lit8 v21, v21, 0x1

    goto :goto_4

    .line 197
    .end local v22    # "j":I
    :cond_b
    invoke-virtual/range {v23 .. v24}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 198
    const/16 v24, 0x0

    .line 219
    .end local v21    # "i":I
    :cond_c
    if-eqz v16, :cond_d

    .line 220
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 222
    :cond_d
    if-eqz v17, :cond_e

    .line 223
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    .line 225
    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->close()V

    goto/16 :goto_2

    .line 213
    .end local v20    # "end":I
    .restart local v18    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v19

    .line 215
    .local v19, "e1":Ljava/io/IOException;
    :try_start_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CityDBHelper.searchCity() FileStream close Exception"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1
.end method

.method public searchCityName(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 25
    .param p1, "city"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 231
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 232
    .local v23, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    .line 233
    .local v24, "list_end":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v14, 0x0

    .line 234
    .local v14, "cursor":Landroid/database/Cursor;
    const/4 v15, 0x0

    .line 236
    .local v15, "cursor2":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->createDataBase()V

    .line 237
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->openDataBase()V

    .line 239
    const-string v2, "%\""

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v20

    .line 240
    .local v20, "end":I
    const/16 v2, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    .line 241
    .local v13, "cityname":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->mSQLDBCityDataBase:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v3, 0x1

    const-string v4, "INFOALARM_WEATHER_CITY"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "CITYNAME"

    aput-object v8, v5, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-string v10, "CITYNAME ASC"

    const/4 v11, 0x0

    move-object/from16 v6, p1

    invoke-virtual/range {v2 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 245
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    .line 246
    new-instance v12, Ljava/lang/StringBuffer;

    invoke-direct {v12}, Ljava/lang/StringBuffer;-><init>()V

    .line 247
    .local v12, "buf":Ljava/lang/StringBuffer;
    const-string v2, "CITYNAME LIKE \"%"

    invoke-virtual {v12, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 248
    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 249
    const-string v2, "%\""

    invoke-virtual {v12, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 250
    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    .line 252
    .local v6, "bufs":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->mSQLDBCityDataBase:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v3, 0x1

    const-string v4, "INFOALARM_WEATHER_CITY"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "CITYNAME"

    aput-object v8, v5, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-string v10, "CITYNAME ASC"

    const/4 v11, 0x0

    invoke-virtual/range {v2 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 257
    .end local v6    # "bufs":Ljava/lang/String;
    .end local v12    # "buf":Ljava/lang/StringBuffer;
    :cond_0
    if-eqz v14, :cond_5

    .line 258
    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    .line 260
    :goto_0
    invoke-interface {v14}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_5

    .line 261
    const-string v2, "CITYNAME"

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 262
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 283
    .end local v13    # "cityname":Ljava/lang/String;
    .end local v20    # "end":I
    :catch_0
    move-exception v16

    .line 285
    .local v16, "e":Ljava/io/IOException;
    :try_start_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CityDBHelper.searchCity()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 288
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myInput:Ljava/io/InputStream;

    if-eqz v2, :cond_1

    .line 289
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myInput:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 290
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myInput:Ljava/io/InputStream;

    .line 292
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myOutput:Ljava/io/OutputStream;

    if-eqz v2, :cond_2

    .line 293
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myOutput:Ljava/io/OutputStream;

    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    .line 294
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->myOutput:Ljava/io/OutputStream;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 314
    :cond_2
    :goto_1
    if-eqz v14, :cond_3

    .line 315
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 317
    :cond_3
    if-eqz v15, :cond_4

    .line 318
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 320
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->close()V

    .line 322
    .end local v16    # "e":Ljava/io/IOException;
    :goto_2
    return-object v23

    .line 266
    .restart local v13    # "cityname":Ljava/lang/String;
    .restart local v20    # "end":I
    :cond_5
    if-eqz v15, :cond_c

    if-eqz v14, :cond_c

    .line 268
    :try_start_3
    invoke-interface {v15}, Landroid/database/Cursor;->moveToFirst()Z

    .line 269
    :goto_3
    invoke-interface {v15}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_8

    .line 270
    const-string v2, "CITYNAME"

    invoke-interface {v15, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v15, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 271
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 314
    .end local v13    # "cityname":Ljava/lang/String;
    .end local v20    # "end":I
    :catchall_0
    move-exception v2

    if-eqz v14, :cond_6

    .line 315
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 317
    :cond_6
    if-eqz v15, :cond_7

    .line 318
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 320
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->close()V

    throw v2

    .line 273
    .restart local v13    # "cityname":Ljava/lang/String;
    .restart local v20    # "end":I
    :cond_8
    const/16 v21, 0x0

    .local v21, "i":I
    :goto_4
    :try_start_4
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, v21

    if-ge v0, v2, :cond_b

    .line 274
    const/16 v22, 0x0

    .local v22, "j":I
    :goto_5
    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, v22

    if-ge v0, v2, :cond_a

    .line 275
    move-object/from16 v0, v23

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object/from16 v0, v24

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 276
    move-object/from16 v0, v24

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 274
    :cond_9
    add-int/lit8 v22, v22, 0x1

    goto :goto_5

    .line 273
    :cond_a
    add-int/lit8 v21, v21, 0x1

    goto :goto_4

    .line 280
    .end local v22    # "j":I
    :cond_b
    invoke-virtual/range {v23 .. v24}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 281
    const/16 v24, 0x0

    .line 314
    .end local v21    # "i":I
    :cond_c
    if-eqz v14, :cond_d

    .line 315
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 317
    :cond_d
    if-eqz v15, :cond_e

    .line 318
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 320
    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;->close()V

    goto :goto_2

    .line 296
    .end local v13    # "cityname":Ljava/lang/String;
    .end local v20    # "end":I
    .restart local v16    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v17

    .line 298
    .local v17, "e1":Ljava/io/IOException;
    :try_start_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CityDBHelper.searchCity() FileStream close Exception"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V

    .line 300
    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1

    .line 301
    .end local v17    # "e1":Ljava/io/IOException;
    :catch_2
    move-exception v18

    .line 303
    .local v18, "e2":Landroid/database/sqlite/SQLiteException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CityDBHelper.searchCity() SQLite Exception"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V

    .line 306
    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1

    .line 307
    .end local v18    # "e2":Landroid/database/sqlite/SQLiteException;
    :catch_3
    move-exception v19

    .line 309
    .local v19, "e3":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CityDBHelper.searchCity() Exception"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V

    .line 311
    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1
.end method
