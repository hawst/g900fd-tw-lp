.class Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockScreenService$1;
.super Landroid/content/BroadcastReceiver;
.source "WeatherClockScreenService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockScreenService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockScreenService;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockScreenService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockScreenService;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockScreenService$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockScreenService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 75
    if-nez p2, :cond_1

    .line 76
    const-string v1, "WeatherClockScreenService"

    const-string v2, "action : intent = null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    :cond_0
    :goto_0
    return-void

    .line 79
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 80
    .local v0, "action":Ljava/lang/String;
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "action : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 82
    invoke-static {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;->setScreenOnFlag(Z)V

    .line 83
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.widgetapp.ap.hero.accuweather.widget.action.WEATHER_SCREEN_OFF"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 84
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v1}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    goto :goto_0

    .line 85
    :cond_2
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 87
    invoke-static {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;->setScreenOnFlag(Z)V

    .line 88
    invoke-static {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;

    move-result-object v1

    const-string v2, "com.sec.android.widgetapp.ap.hero.accuweather.widget.action.WEATHER_SCREEN_ON"

    invoke-virtual {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/remoteview/UIManager;->onDirectUpdateClock(Ljava/lang/String;)V

    .line 89
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method
