.class Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$31;
.super Landroid/os/Handler;
.source "MapsActivityNew.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field resultArrived:Z

.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V
    .locals 1
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 1229
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1230
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$31;->resultArrived:Z

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const v7, 0x7f0d001f

    const v6, -0x140b4

    const/4 v5, 0x1

    .line 1233
    iget v3, p1, Landroid/os/Message;->what:I

    const v4, -0x13d30

    if-ne v3, v4, :cond_1

    .line 1234
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    iget-object v3, v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->loadingHandler:Landroid/os/Handler;

    if-eqz v3, :cond_0

    .line 1235
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    iget-object v3, v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->loadingHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x7530

    invoke-virtual {v3, v6, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1267
    :cond_0
    :goto_0
    return-void

    .line 1237
    :cond_1
    iget v3, p1, Landroid/os/Message;->what:I

    if-ne v3, v6, :cond_2

    .line 1238
    iget-boolean v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$31;->resultArrived:Z

    if-nez v3, :cond_0

    .line 1239
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->hideLoadingDialog()V
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$2300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    .line 1240
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-static {v3, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    goto :goto_0

    .line 1243
    :cond_2
    iput-boolean v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$31;->resultArrived:Z

    .line 1244
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "RESPONSE_BODY"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1245
    .local v0, "body":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "RESPONSE_CODE"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 1247
    .local v2, "result":I
    const/16 v3, 0xc8

    if-ne v2, v3, :cond_5

    .line 1248
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mParser:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$3800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-virtual {v3, v0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->parseCityList(Ljava/lang/String;Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v1

    .line 1250
    .local v1, "info":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;>;"
    if-eqz v1, :cond_0

    .line 1251
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_3

    .line 1252
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->hideLoadingDialog()V
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$2300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    .line 1253
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    const v4, 0x7f0d0027

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    goto :goto_0

    .line 1255
    :cond_3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v3, v5, :cond_4

    .line 1256
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    invoke-virtual {v4, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->perform_Acity(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;)V

    goto :goto_0

    .line 1257
    :cond_4
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-le v3, v5, :cond_0

    .line 1258
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->showCities(Ljava/util/ArrayList;)V
    invoke-static {v3, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$3900(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 1264
    .end local v1    # "info":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;>;"
    :cond_5
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->hideLoadingDialog()V
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$2300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    .line 1265
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-static {v3, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    goto :goto_0
.end method
