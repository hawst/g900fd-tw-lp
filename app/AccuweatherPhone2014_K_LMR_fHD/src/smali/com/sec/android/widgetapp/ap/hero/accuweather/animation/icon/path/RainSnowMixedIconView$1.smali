.class Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;
.super Ljava/lang/Object;
.source "RainSnowMixedIconView.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    .prologue
    .line 675
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 680
    invoke-virtual {p1}, Landroid/animation/Animator;->removeAllListeners()V

    .line 681
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 677
    invoke-virtual {p1}, Landroid/animation/Animator;->removeAllListeners()V

    .line 678
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 3
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    const/4 v2, 0x0

    .line 684
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->isStop:Z

    if-eqz v0, :cond_0

    .line 685
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 686
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    const-string v1, "e_"

    iput-object v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixT1:Ljava/lang/String;

    .line 687
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 721
    :cond_0
    :goto_0
    return-void

    .line 688
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1_1:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1_1:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 689
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    const-string v1, "e_"

    iput-object v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixT1_1:Ljava/lang/String;

    .line 690
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT1_1:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    goto :goto_0

    .line 691
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT2:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT2:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 692
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    const-string v1, "e_"

    iput-object v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixT2:Ljava/lang/String;

    .line 693
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->RainDropT2:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    goto :goto_0

    .line 694
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale1:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale1:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 695
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    const-string v1, "e_"

    iput-object v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixS1:Ljava/lang/String;

    .line 696
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale1:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    goto :goto_0

    .line 697
    :cond_4
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale2:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale2:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 698
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    const-string v1, "e_"

    iput-object v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixS2:Ljava/lang/String;

    .line 699
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale2:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    goto/16 :goto_0

    .line 700
    :cond_5
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale3:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale3:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 701
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    const-string v1, "e_"

    iput-object v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixS3:Ljava/lang/String;

    .line 702
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropScale3:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    goto/16 :goto_0

    .line 703
    :cond_6
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT1:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT1:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 704
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    const-string v1, "e_"

    iput-object v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixCT1:Ljava/lang/String;

    .line 705
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT1:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    goto/16 :goto_0

    .line 706
    :cond_7
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT2:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT2:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 707
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    const-string v1, "e_"

    iput-object v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixCT2:Ljava/lang/String;

    .line 708
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT2:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    goto/16 :goto_0

    .line 709
    :cond_8
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->access$800(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->access$800(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 710
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    const-string v1, "e_"

    iput-object v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixCT3:Ljava/lang/String;

    .line 711
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->access$800(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    goto/16 :goto_0

    .line 712
    :cond_9
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->access$900(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->access$900(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 713
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    const-string v1, "e_"

    iput-object v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->mPreFixCT3_1:Ljava/lang/String;

    .line 714
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->access$900(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    goto/16 :goto_0

    .line 715
    :cond_a
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->IconTranslate:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->access$1000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->IconTranslate:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->access$1000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 716
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->IconTranslate:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->access$1000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    goto/16 :goto_0

    .line 717
    :cond_b
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->CloudTranslate:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->access$1100(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->CloudTranslate:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->access$1100(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 718
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->CloudTranslate:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;->access$1100(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/RainSnowMixedIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    goto/16 :goto_0
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 682
    return-void
.end method
