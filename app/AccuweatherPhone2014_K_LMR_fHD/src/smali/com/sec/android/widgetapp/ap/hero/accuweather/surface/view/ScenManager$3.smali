.class Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$3;
.super Ljava/lang/Object;
.source "ScenManager.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->moveCity(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    .prologue
    .line 614
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 648
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 5
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    const/4 v4, 0x4

    .line 630
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onAnimationEnd : next view = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 631
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mNextCityInfoView:Landroid/view/ViewGroup;
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->access$100()Landroid/view/ViewGroup;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 630
    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 632
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->swapView()V

    .line 633
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mNextCityInfoView:Landroid/view/ViewGroup;
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->access$100()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 634
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mNextCityBGView:Landroid/view/ViewGroup;
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->access$200()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 635
    invoke-virtual {p1, p0}, Landroid/animation/Animator;->removeListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 636
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIsStartScrollAnimation:Z
    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->access$302(Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;Z)Z

    .line 637
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityInfoView:Landroid/view/ViewGroup;
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->access$000()Landroid/view/ViewGroup;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->startIconAnimation(Landroid/view/ViewGroup;)V

    .line 638
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090012

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    .line 639
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090013

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 640
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.widgetapp.ap.hero.accuweather.widget.action.APPWIDGET_UPDATE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 641
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "FROMSURFACE"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 642
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 644
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 627
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 3
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    const/4 v2, 0x0

    .line 616
    const-string v0, ""

    const-string v1, "onAnimationStart"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 617
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mNextCityInfoView:Landroid/view/ViewGroup;
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->access$100()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 618
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mNextCityBGView:Landroid/view/ViewGroup;
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->access$200()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 619
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mIsStartScrollAnimation:Z
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->access$302(Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;Z)Z

    .line 620
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityInfoView:Landroid/view/ViewGroup;
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->access$000()Landroid/view/ViewGroup;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 621
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mCurCityInfoView:Landroid/view/ViewGroup;
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->access$000()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->cancelIconAnimation(Landroid/view/ViewGroup;)V

    .line 623
    :cond_0
    return-void
.end method
