.class public interface abstract Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;
.super Ljava/lang/Object;
.source "IconAnimation.java"

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;


# static fields
.field public static final ORIGINAL_HEIGHT:I = 0xdc

.field public static final ORIGINAL_WIDTH:I = 0xdc


# virtual methods
.method public abstract cancelAnimation()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public abstract getScale()F
.end method

.method public abstract getView()Landroid/view/View;
.end method

.method public abstract isRunning()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public abstract setPaintColor(I)V
.end method

.method public abstract setScale(F)V
.end method

.method public abstract setWidthAndHeight(II)V
.end method

.method public abstract startAnimation()V
.end method

.method public abstract stopAnimation()V
.end method
