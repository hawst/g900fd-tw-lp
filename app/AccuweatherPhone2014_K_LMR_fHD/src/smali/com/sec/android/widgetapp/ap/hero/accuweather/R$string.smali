.class public final Lcom/sec/android/widgetapp/ap/hero/accuweather/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final Report_wrong_city_name_text:I = 0x7f0d00dc

.field public static final a_new_version_is_available:I = 0x7f0d00d6

.field public static final abbrev_wday_abbrev_month_day_no_year:I = 0x7f0d00b8

.field public static final abbrev_wday_day_abbrev_month_no_year:I = 0x7f0d00b9

.field public static final abbrev_wday_month_day_no_year:I = 0x7f0d0111

.field public static final accu_logo:I = 0x7f0d010b

.field public static final action_bar_title:I = 0x7f0d0075

.field public static final add_city_message:I = 0x7f0d007a

.field public static final add_to_list:I = 0x7f0d007b

.field public static final add_to_list_tts:I = 0x7f0d0084

.field public static final agree_txt:I = 0x7f0d00df

.field public static final already_registered_message:I = 0x7f0d0022

.field public static final app_name:I = 0x7f0d0102

.field public static final apply_daylight_saving_time:I = 0x7f0d0064

.field public static final auto_refresh_time_titles:I = 0x7f0d001b

.field public static final b_type_idle_p:I = 0x7f0d00a4

.field public static final button_tts:I = 0x7f0d008a

.field public static final calendar_text:I = 0x7f0d00ee

.field public static final cancel_tts:I = 0x7f0d0092

.field public static final celsius_tts:I = 0x7f0d0109

.field public static final centigrade:I = 0x7f0d0081

.field public static final change_message:I = 0x7f0d001d

.field public static final check_for_updates:I = 0x7f0d00d4

.field public static final checkdeteal_tts:I = 0x7f0d009b

.field public static final city_list_text:I = 0x7f0d00de

.field public static final clear:I = 0x7f0d005c

.field public static final clear_tts:I = 0x7f0d0083

.field public static final cloudy:I = 0x7f0d0046

.field public static final cold:I = 0x7f0d005a

.field public static final current_location_off_title:I = 0x7f0d0076

.field public static final current_location_tts:I = 0x7f0d0090

.field public static final current_tmep_tts:I = 0x7f0d009c

.field public static final date_text:I = 0x7f0d00da

.field public static final default_location_popup_msg:I = 0x7f0d00d2

.field public static final degree_minus_tts:I = 0x7f0d00a1

.field public static final degree_plural_tts:I = 0x7f0d00a0

.field public static final degree_tts:I = 0x7f0d009f

.field public static final delete_cities_txt:I = 0x7f0d00e8

.field public static final delete_city_txt:I = 0x7f0d00e7

.field public static final delete_message:I = 0x7f0d001c

.field public static final delete_tts:I = 0x7f0d0108

.field public static final dialog_Entire_cities_deleted:I = 0x7f0d0078

.field public static final dialog_Weather_widget_empty:I = 0x7f0d0079

.field public static final dialog_cancel_button:I = 0x7f0d0009

.field public static final dialog_check_animation_effect:I = 0x7f0d00bf

.field public static final dialog_check_animation_effect_contents:I = 0x7f0d00c0

.field public static final dialog_check_animation_effect_popup_contents:I = 0x7f0d00c1

.field public static final dialog_check_sensor_contents:I = 0x7f0d00bd

.field public static final dialog_check_sensor_set:I = 0x7f0d00be

.field public static final dialog_check_sensor_title:I = 0x7f0d00bc

.field public static final dialog_cities_deleted:I = 0x7f0d0077

.field public static final dialog_delete_message:I = 0x7f0d0028

.field public static final dialog_deleteall_button:I = 0x7f0d000d

.field public static final dialog_disable_button:I = 0x7f0d000c

.field public static final dialog_enable_button:I = 0x7f0d000b

.field public static final dialog_gps_progressing:I = 0x7f0d0038

.field public static final dialog_later_button:I = 0x7f0d000e

.field public static final dialog_motion_disabled:I = 0x7f0d0071

.field public static final dialog_ok_button:I = 0x7f0d0008

.field public static final dialog_retry_button:I = 0x7f0d006c

.field public static final dialog_skip_button:I = 0x7f0d000a

.field public static final dialog_title_charging:I = 0x7f0d0032

.field public static final dialog_title_data_roaming:I = 0x7f0d0036

.field public static final dialog_title_gps_info:I = 0x7f0d0033

.field public static final dialog_title_gps_info_edit:I = 0x7f0d0034

.field public static final dialog_title_shake_guide:I = 0x7f0d0070

.field public static final dialog_usemotion_button:I = 0x7f0d0072

.field public static final disagree_txt:I = 0x7f0d00e0

.field public static final done_button:I = 0x7f0d0063

.field public static final done_tts:I = 0x7f0d0091

.field public static final double_tap_to_go_to_website_tts:I = 0x7f0d00e3

.field public static final dreary:I = 0x7f0d0047

.field public static final easymode_subscribe:I = 0x7f0d00bb

.field public static final enable_current_location_message:I = 0x7f0d0029

.field public static final enable_refresh_when_entering_message:I = 0x7f0d007d

.field public static final enable_refresh_when_roaming_message:I = 0x7f0d007e

.field public static final error_current_location:I = 0x7f0d002f

.field public static final error_gps_off_message:I = 0x7f0d00ba

.field public static final error_gps_off_message_short:I = 0x7f0d00e9

.field public static final error_location_message:I = 0x7f0d002b

.field public static final error_network_access_check:I = 0x7f0d006a

.field public static final error_text_input_failed:I = 0x7f0d0069

.field public static final error_text_input_filled:I = 0x7f0d006e

.field public static final fahrenheit_tts:I = 0x7f0d010a

.field public static final farenheit:I = 0x7f0d0080

.field public static final find_current_location:I = 0x7f0d0035

.field public static final flurries:I = 0x7f0d0050

.field public static final fog:I = 0x7f0d0048

.field public static final freezing_rain:I = 0x7f0d0057

.field public static final full_wday_day_month_no_year:I = 0x7f0d0112

.field public static final full_wday_month_day_no_year:I = 0x7f0d010e

.field public static final goto_apps:I = 0x7f0d00c4

.field public static final goto_apps_later:I = 0x7f0d00c6

.field public static final goto_apps_now:I = 0x7f0d00c5

.field public static final goto_apps_update_title:I = 0x7f0d00c3

.field public static final graph_button_tts:I = 0x7f0d0098

.field public static final hazy:I = 0x7f0d005f

.field public static final hazy_sunshine:I = 0x7f0d0044

.field public static final hero_widget:I = 0x7f0d010c

.field public static final highest_tmep_tts:I = 0x7f0d009d

.field public static final hint_jpn_text:I = 0x7f0d0107

.field public static final hint_search_cities:I = 0x7f0d0025

.field public static final home:I = 0x7f0d00cf

.field public static final hot:I = 0x7f0d0059

.field public static final ice:I = 0x7f0d0055

.field public static final inform_enable_current_location:I = 0x7f0d002a

.field public static final inform_refresh_when_entering_k:I = 0x7f0d007f

.field public static final intermittent_clouds:I = 0x7f0d0043

.field public static final last_updated_text:I = 0x7f0d003b

.field public static final last_updated_tts:I = 0x7f0d003c

.field public static final list_tts:I = 0x7f0d0085

.field public static final list_view_tts:I = 0x7f0d0086

.field public static final loading_message:I = 0x7f0d001e

.field public static final lowest_tmep_tts:I = 0x7f0d009e

.field public static final map_all:I = 0x7f0d00ff

.field public static final map_mark:I = 0x7f0d0101

.field public static final map_my:I = 0x7f0d0100

.field public static final map_search_message:I = 0x7f0d003a

.field public static final map_search_title:I = 0x7f0d0039

.field public static final max_item_saved_message:I = 0x7f0d0023

.field public static final menu_add_title:I = 0x7f0d0001

.field public static final menu_back_tts:I = 0x7f0d0097

.field public static final menu_change_orders:I = 0x7f0d0003

.field public static final menu_change_orders_tts:I = 0x7f0d0096

.field public static final menu_city_list_title:I = 0x7f0d0000

.field public static final menu_current_city_show:I = 0x7f0d0004

.field public static final menu_delete_title:I = 0x7f0d0002

.field public static final menu_edit_title:I = 0x7f0d0007

.field public static final menu_overflow_title_tts:I = 0x7f0d008c

.field public static final menu_refresh_title:I = 0x7f0d0066

.field public static final menu_search_title:I = 0x7f0d0005

.field public static final menu_setDefaultLocation_title:I = 0x7f0d0062

.field public static final menu_settings_auto_scroll:I = 0x7f0d0010

.field public static final menu_settings_background_video:I = 0x7f0d00f5

.field public static final menu_settings_background_video_repeat:I = 0x7f0d00f0

.field public static final menu_settings_bottom_icon_animation:I = 0x7f0d00f3

.field public static final menu_settings_city_select_animation:I = 0x7f0d00fb

.field public static final menu_settings_cube_animation:I = 0x7f0d00f7

.field public static final menu_settings_edge_screen:I = 0x7f0d00eb

.field public static final menu_settings_icon_animation:I = 0x7f0d00f6

.field public static final menu_settings_icon_animation_repeat:I = 0x7f0d00f1

.field public static final menu_settings_lockscreen_and_s_view_cover:I = 0x7f0d00ec

.field public static final menu_settings_notification_message:I = 0x7f0d00aa

.field public static final menu_settings_notification_set_time:I = 0x7f0d00ab

.field public static final menu_settings_notification_setting:I = 0x7f0d00a8

.field public static final menu_settings_notifications:I = 0x7f0d00a9

.field public static final menu_settings_s_planner:I = 0x7f0d00ed

.field public static final menu_settings_scroll_change_city:I = 0x7f0d00f4

.field public static final menu_settings_set_duration_time:I = 0x7f0d00fe

.field public static final menu_settings_subtitle_Show_weather_information:I = 0x7f0d00ea

.field public static final menu_settings_subtitle_currentloaction:I = 0x7f0d0013

.field public static final menu_settings_subtitle_effect:I = 0x7f0d00ef

.field public static final menu_settings_subtitle_general:I = 0x7f0d0011

.field public static final menu_settings_title:I = 0x7f0d0006

.field public static final menu_settings_title_bg_alpha:I = 0x7f0d00fd

.field public static final menu_settings_title_tts:I = 0x7f0d008b

.field public static final menu_settings_today_infor_animation:I = 0x7f0d00f8

.field public static final menu_settings_unit:I = 0x7f0d000f

.field public static final menu_settings_update:I = 0x7f0d0012

.field public static final menu_settings_weather_effect:I = 0x7f0d00f2

.field public static final menu_settings_widget_background:I = 0x7f0d0014

.field public static final menu_settings_widget_bg_alpha_effect:I = 0x7f0d00fc

.field public static final menu_settings_widget_city_change_animation:I = 0x7f0d00fa

.field public static final menu_settings_widget_icon_animation:I = 0x7f0d00f9

.field public static final message_added_to_my_city:I = 0x7f0d0024

.field public static final message_data_connection:I = 0x7f0d0030

.field public static final message_data_roaming:I = 0x7f0d0037

.field public static final message_delete_current_city:I = 0x7f0d009a

.field public static final message_donot_show:I = 0x7f0d0031

.field public static final message_location_service_not_available:I = 0x7f0d010d

.field public static final message_network_connnection_failed:I = 0x7f0d001f

.field public static final message_network_unavailable:I = 0x7f0d0020

.field public static final message_no_search_result:I = 0x7f0d0026

.field public static final message_no_search_result_k:I = 0x7f0d0027

.field public static final message_not_enough_space:I = 0x7f0d00a5

.field public static final message_service_not_available:I = 0x7f0d0021

.field public static final message_shake_guide:I = 0x7f0d0073

.field public static final message_shake_guide_exceptkies:I = 0x7f0d0074

.field public static final message_some_city_names_are_supported_in_english_only:I = 0x7f0d00d0

.field public static final minus_tts:I = 0x7f0d008d

.field public static final more:I = 0x7f0d003e

.field public static final more_options:I = 0x7f0d003f

.field public static final more_tts:I = 0x7f0d0088

.field public static final mostly_clear:I = 0x7f0d005d

.field public static final mostly_cloudy:I = 0x7f0d0045

.field public static final mostly_cloudy_with_flurries:I = 0x7f0d0051

.field public static final mostly_cloudy_with_showers:I = 0x7f0d004a

.field public static final mostly_cloudy_with_snow:I = 0x7f0d0054

.field public static final mostly_cloudy_with_thunder_showers:I = 0x7f0d004d

.field public static final mostly_sunny:I = 0x7f0d0041

.field public static final no_new_software_updates_available:I = 0x7f0d00d5

.field public static final noti:I = 0x7f0d00d9

.field public static final notice_accurate_location_detection_msg:I = 0x7f0d002c

.field public static final notice_gps_trun_off_message_verizon:I = 0x7f0d002e

.field public static final notice_gps_turn_off_message:I = 0x7f0d002d

.field public static final notification_daytime:I = 0x7f0d00c9

.field public static final notification_nighttime:I = 0x7f0d00ca

.field public static final notification_no_set_time_msg:I = 0x7f0d00c2

.field public static final notification_touch_widget_popup_content:I = 0x7f0d00cb

.field public static final notification_touch_widget_popup_content_for_one_hour_interval:I = 0x7f0d00cc

.field public static final notimsg_Hail_Probability:I = 0x7f0d00ae

.field public static final notimsg_Precipitation_Probability:I = 0x7f0d00af

.field public static final notimsg_Rain_Probability:I = 0x7f0d00ac

.field public static final notimsg_Snow_Probability:I = 0x7f0d00ad

.field public static final notimsg_amount:I = 0x7f0d00b0

.field public static final notimsg_inch:I = 0x7f0d00b1

.field public static final off_text:I = 0x7f0d00e6

.field public static final on_text:I = 0x7f0d00e5

.field public static final partly_cloudy:I = 0x7f0d005e

.field public static final partly_cloudy_with_showers:I = 0x7f0d0061

.field public static final partly_cloudy_with_thunder_showers:I = 0x7f0d0060

.field public static final partly_sunny:I = 0x7f0d0042

.field public static final partly_sunny_with_flurries:I = 0x7f0d0052

.field public static final partly_sunny_with_showers:I = 0x7f0d004b

.field public static final partly_sunny_with_thunder_showers:I = 0x7f0d004e

.field public static final percent_tts:I = 0x7f0d00a3

.field public static final probability_of_rain_text:I = 0x7f0d00b4

.field public static final rain:I = 0x7f0d004f

.field public static final rain_and_snow_mixed:I = 0x7f0d0058

.field public static final realfeel_text:I = 0x7f0d00b3

.field public static final realfeel_tts:I = 0x7f0d00a2

.field public static final refresh_button:I = 0x7f0d0087

.field public static final refresh_message:I = 0x7f0d006b

.field public static final refresh_time_12h:I = 0x7f0d0019

.field public static final refresh_time_1h:I = 0x7f0d0016

.field public static final refresh_time_24h:I = 0x7f0d001a

.field public static final refresh_time_3h:I = 0x7f0d0017

.field public static final refresh_time_6h:I = 0x7f0d0018

.field public static final refresh_time_none:I = 0x7f0d0015

.field public static final save_button:I = 0x7f0d007c

.field public static final search_query_tts:I = 0x7f0d0082

.field public static final search_tts:I = 0x7f0d008e

.field public static final searchadd_tts:I = 0x7f0d008f

.field public static final seconds:I = 0x7f0d006f

.field public static final select_all_text:I = 0x7f0d0065

.field public static final select_button:I = 0x7f0d006d

.field public static final senset_text:I = 0x7f0d00b7

.field public static final set_default_location:I = 0x7f0d00d1

.field public static final setting_no:I = 0x7f0d0068

.field public static final setting_yes:I = 0x7f0d0067

.field public static final showers:I = 0x7f0d0049

.field public static final sleet:I = 0x7f0d0056

.field public static final slog:I = 0x7f0d0103

.field public static final slog_all_logs:I = 0x7f0d0104

.field public static final slog_err_log:I = 0x7f0d0105

.field public static final slog_log_off:I = 0x7f0d0106

.field public static final snow:I = 0x7f0d0053

.field public static final software_update:I = 0x7f0d00d3

.field public static final software_update_nitice:I = 0x7f0d00d7

.field public static final some_selected_text:I = 0x7f0d00ce

.field public static final stms_appgroup:I = 0x7f0d0110

.field public static final sunny:I = 0x7f0d0040

.field public static final sunrise_sunset:I = 0x7f0d00b2

.field public static final sunrise_text:I = 0x7f0d00b6

.field public static final table_button_tts:I = 0x7f0d0099

.field public static final temp_delete_current_location:I = 0x7f0d010f

.field public static final thunderstorms:I = 0x7f0d004c

.field public static final tick_box_tts:I = 0x7f0d0093

.field public static final ticked_tts:I = 0x7f0d0094

.field public static final time_am:I = 0x7f0d00a6

.field public static final time_pm:I = 0x7f0d00a7

.field public static final today:I = 0x7f0d00c7

.field public static final tomorrow:I = 0x7f0d00c8

.field public static final transparency_text:I = 0x7f0d00dd

.field public static final unable_to_receive_info_msg:I = 0x7f0d00e4

.field public static final unselect_all_text:I = 0x7f0d00cd

.field public static final unticked_tts:I = 0x7f0d0095

.field public static final update:I = 0x7f0d00d8

.field public static final use_current_location_agree_popup:I = 0x7f0d00e1

.field public static final use_current_location_off_alert_popup:I = 0x7f0d00e2

.field public static final uv_index_text:I = 0x7f0d00b5

.field public static final weather_map:I = 0x7f0d003d

.field public static final weather_map_tts:I = 0x7f0d0089

.field public static final windy:I = 0x7f0d005b

.field public static final year_text:I = 0x7f0d00db


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
