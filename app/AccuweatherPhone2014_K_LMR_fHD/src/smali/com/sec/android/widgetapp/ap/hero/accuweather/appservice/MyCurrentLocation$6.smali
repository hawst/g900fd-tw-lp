.class Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$6;
.super Ljava/util/TimerTask;
.source "MyCurrentLocation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->getLastLocation(Landroid/os/Handler;IZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

.field final synthetic val$isDetectingTimeLimit:Z


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    .prologue
    .line 581
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$6;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    iput-boolean p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$6;->val$isDetectingTimeLimit:Z

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 583
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$6;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->isGetLocation()Z

    move-result v3

    if-ne v5, v3, :cond_2

    .line 590
    :cond_0
    :goto_0
    iget-boolean v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$6;->val$isDetectingTimeLimit:Z

    if-eqz v3, :cond_1

    .line 591
    const-string v3, ""

    const-string v4, "dtl"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 592
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$6;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    iget-boolean v3, v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->isRetry:Z

    if-nez v3, :cond_3

    .line 593
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$6;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    iget-object v3, v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->timeoutHandler:Landroid/os/Handler;

    const/16 v4, 0x3e8

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 594
    .local v1, "msg":Landroid/os/Message;
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$6;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    iget-object v3, v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->timeoutHandler:Landroid/os/Handler;

    invoke-virtual {v3, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 612
    .end local v1    # "msg":Landroid/os/Message;
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$6;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->removeLocationListener()V

    .line 613
    return-void

    .line 585
    :cond_2
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$6;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->getCencelFlag()I

    move-result v3

    if-ne v5, v3, :cond_0

    .line 586
    const-string v3, ""

    const-string v4, "cancel flag 1"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 587
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$6;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$6;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    iget-object v4, v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->currentLocationHandler:Landroid/os/Handler;

    invoke-virtual {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->performCurrentLocation(Landroid/os/Handler;)V

    goto :goto_0

    .line 596
    :cond_3
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 597
    .local v0, "data":Landroid/os/Bundle;
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    .line 598
    .local v2, "msg2":Landroid/os/Message;
    invoke-virtual {v2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 599
    const/16 v3, 0xca

    iput v3, v2, Landroid/os/Message;->what:I

    .line 600
    const-string v3, "provider"

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$6;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mProvider:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 601
    const-string v3, "isFristRetry"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 602
    const-string v3, "isAutoRefresh"

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 603
    invoke-virtual {v2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 604
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$6;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    iget-object v3, v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->currentLocationHandler:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1
.end method
