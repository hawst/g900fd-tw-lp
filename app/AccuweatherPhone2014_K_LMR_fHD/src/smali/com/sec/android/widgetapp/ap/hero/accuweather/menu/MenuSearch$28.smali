.class Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$28;
.super Landroid/os/Handler;
.source "MenuSearch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 1195
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v5, 0xc8

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1198
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 1199
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v4, "RESPONSE_BODY"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1200
    .local v2, "responseBody":Ljava/lang/String;
    const-string v4, "RESPONSE_CODE"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 1202
    .local v3, "responseCode":I
    if-ne v3, v5, :cond_5

    if-eqz v2, :cond_5

    .line 1203
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->parser:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$3400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    invoke-virtual {v4, v2, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->parseCityList(Ljava/lang/String;Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v1

    .line 1204
    .local v1, "lstResult":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;>;"
    if-eqz v1, :cond_0

    .line 1205
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->lstCity:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$3200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 1206
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->lstCity:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$3200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 1218
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-ne v4, v8, :cond_4

    .line 1219
    :cond_1
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mNoSearchText:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$1700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1220
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mNoSearchText:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$1700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Landroid/widget/TextView;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0d0027

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1223
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->setKeyboardVisible(Z)V
    invoke-static {v4, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$1200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;Z)V

    .line 1233
    :goto_1
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getListView()Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 1234
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->cityListAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;
    invoke-static {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$3600(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 1236
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->cityListAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$3600(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;->notifyDataSetChanged()V

    .line 1237
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getListView()Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/widget/ListView;->setVisibility(I)V

    .line 1238
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->hideLoadingDialog()V
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$2000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    .line 1247
    .end local v1    # "lstResult":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;>;"
    :cond_2
    :goto_2
    return-void

    .line 1208
    .restart local v1    # "lstResult":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;>;"
    :cond_3
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->lstCity:Ljava/util/ArrayList;
    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$3202(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    goto :goto_0

    .line 1226
    :cond_4
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mNoSearchText:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$1700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Landroid/widget/TextView;

    move-result-object v4

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1227
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->setKeyboardVisible(Z)V
    invoke-static {v4, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$1200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;Z)V

    .line 1228
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->lstCity:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$3200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1230
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->disableEditField()V
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$3500(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    goto :goto_1

    .line 1239
    .end local v1    # "lstResult":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;>;"
    :cond_5
    if-eq v3, v5, :cond_6

    .line 1240
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->hideLoadingDialog()V
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$2000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    .line 1241
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->isActivityVisible()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1242
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$28;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    const v5, 0x7f0d001f

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    goto :goto_2

    .line 1245
    :cond_6
    const-string v4, ""

    const-string v5, "searchHandler : fail : location or responseBody are null"

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
