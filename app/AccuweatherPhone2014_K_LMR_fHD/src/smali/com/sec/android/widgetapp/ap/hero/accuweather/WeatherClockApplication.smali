.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;
.super Landroid/app/Application;
.source "WeatherClockApplication.java"


# static fields
.field public static ACTIONBAR_USE:Z

.field public static HARDKEY_USE:Z

.field public static SamsungNumber3L:Landroid/graphics/Typeface;

.field public static SamsungNumber3T:Landroid/graphics/Typeface;

.field public static isATT:Z

.field public static isAutoCaptureToolSupportMode:Z

.field public static isCurCityAtTopInCityList:Z

.field public static isKitKatOS:Z

.field public static isLollipopOS:Z

.field public static isMetroPCS:Z

.field public static isUSvender:Z

.field public static isVerizon:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 18
    sput-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->ACTIONBAR_USE:Z

    .line 20
    sput-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->HARDKEY_USE:Z

    .line 22
    sput-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isVerizon:Z

    .line 24
    sput-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isMetroPCS:Z

    .line 27
    sput-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isATT:Z

    .line 29
    sput-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isKitKatOS:Z

    .line 31
    sput-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isLollipopOS:Z

    .line 33
    sput-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isUSvender:Z

    .line 35
    sput-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isAutoCaptureToolSupportMode:Z

    .line 39
    sput-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->SamsungNumber3T:Landroid/graphics/Typeface;

    .line 40
    sput-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->SamsungNumber3L:Landroid/graphics/Typeface;

    .line 42
    sput-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isCurCityAtTopInCityList:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method

.method public static checkHardKey(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    .line 135
    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 136
    sput-boolean v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->HARDKEY_USE:Z

    .line 141
    :goto_0
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->replaceModelName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 142
    .local v0, "deviceId":Ljava/lang/String;
    if-nez v0, :cond_2

    .line 143
    const-string v1, ""

    const-string v2, "device ID is null!!!!"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    :cond_0
    :goto_1
    return-void

    .line 138
    .end local v0    # "deviceId":Ljava/lang/String;
    :cond_1
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->HARDKEY_USE:Z

    goto :goto_0

    .line 145
    .restart local v0    # "deviceId":Ljava/lang/String;
    :cond_2
    const-string v1, "EK-KC100"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "EK-GC100"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "EK-GC120"

    .line 146
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "EK-GC110"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "EK-GN120"

    .line 147
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "EK-GN100"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "EK-GN120A"

    .line 148
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 149
    :cond_3
    sput-boolean v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->HARDKEY_USE:Z

    goto :goto_1
.end method

.method public static isATT()Z
    .locals 2

    .prologue
    .line 181
    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 182
    .local v0, "salesCode":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 183
    :cond_0
    const-string v1, "ril.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 185
    :cond_1
    const-string v1, "ATT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 186
    const/4 v1, 0x1

    .line 189
    :goto_0
    return v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isMEA()Z
    .locals 2

    .prologue
    .line 202
    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 203
    .local v0, "salesCode":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 204
    :cond_0
    const-string v1, "ril.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 206
    :cond_1
    const-string v1, "AFG"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "BTC"

    .line 207
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "EGY"

    .line 208
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "FWD"

    .line 209
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "KSA"

    .line 210
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "LEB"

    .line 211
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "LYS"

    .line 212
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "MAT"

    .line 213
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "MID"

    .line 214
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "MWD"

    .line 215
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "PAK"

    .line 216
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "THR"

    .line 217
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "TMC"

    .line 218
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "TUN"

    .line 219
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "TUR"

    .line 220
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "WTL"

    .line 221
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "XSG"

    .line 222
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 223
    :cond_2
    const/4 v1, 0x1

    .line 225
    :goto_0
    return v1

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isMetroPCS()Z
    .locals 2

    .prologue
    .line 170
    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 171
    .local v0, "salesCode":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 172
    :cond_0
    const-string v1, "ril.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 174
    :cond_1
    const-string v1, "MTR"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 175
    const/4 v1, 0x1

    .line 177
    :goto_0
    return v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isVerizon()Z
    .locals 2

    .prologue
    .line 159
    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 160
    .local v0, "salesCode":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 161
    :cond_0
    const-string v1, "ril.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 163
    :cond_1
    const-string v1, "VZW"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 164
    const/4 v1, 0x1

    .line 166
    :goto_0
    return v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onCreate()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 45
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 46
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->setContext(Landroid/content/Context;)V

    .line 48
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0xe

    if-ge v5, v6, :cond_a

    .line 49
    sput-boolean v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->ACTIONBAR_USE:Z

    .line 54
    :goto_0
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x13

    if-lt v5, v6, :cond_0

    .line 55
    sput-boolean v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isKitKatOS:Z

    .line 58
    :cond_0
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x15

    if-lt v5, v6, :cond_1

    .line 59
    sput-boolean v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isLollipopOS:Z

    .line 62
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090011

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v5

    if-nez v5, :cond_2

    .line 63
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "aw_daemon_service_key_agree_popup_check"

    invoke-static {v5, v6, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 64
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getCurrentLocationSetting(Landroid/content/Context;)I

    move-result v5

    if-ne v5, v4, :cond_2

    .line 65
    invoke-static {p0, v4, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setIsAgreeUseLocation(Landroid/content/Context;ZZ)V

    .line 69
    :cond_2
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isVerizon()Z

    move-result v5

    sput-boolean v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isVerizon:Z

    .line 71
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->checkHardKey(Landroid/content/Context;)V

    .line 72
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isMetroPCS()Z

    move-result v5

    sput-boolean v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isMetroPCS:Z

    .line 73
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isATT()Z

    move-result v5

    sput-boolean v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isATT:Z

    .line 75
    const-string v5, "ro.csc.sales_code"

    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 76
    .local v2, "salesCode":Ljava/lang/String;
    if-eqz v2, :cond_3

    const-string v5, ""

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 77
    :cond_3
    const-string v5, "ril.sales_code"

    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 80
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090010

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v5

    sput-boolean v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isCurCityAtTopInCityList:Z

    .line 82
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090015

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 84
    .local v1, "isSupportSamsungFontFamily":Z
    const-string v5, "ATT"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    const-string v5, "VZW"

    .line 85
    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    const-string v5, "SPR"

    .line 86
    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    const-string v5, "TMB"

    .line 87
    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    const-string v5, "USC"

    .line 88
    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    const-string v5, "XAS"

    .line 89
    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    const-string v5, "VMU"

    .line 90
    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    const-string v5, "MTR"

    .line 91
    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    const-string v5, "TMB"

    .line 92
    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    const-string v5, "CRI"

    .line 93
    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    const-string v5, "XAR"

    .line 94
    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    const-string v5, "CSP"

    .line 95
    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    const-string v5, "LRA"

    .line 96
    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    const-string v5, "AIO"

    .line 97
    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    const-string v5, "TFN"

    .line 98
    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    const-string v5, "ACG"

    .line 99
    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    const-string v5, "BST"

    .line 100
    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    const-string v5, "BNN"

    .line 101
    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    const-string v5, "N03"

    .line 102
    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    :cond_5
    move v3, v4

    :cond_6
    sput-boolean v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isUSvender:Z

    .line 104
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getProductName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "slte"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 106
    :try_start_0
    sget-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->SamsungNumber3T:Landroid/graphics/Typeface;

    if-nez v3, :cond_7

    if-nez v1, :cond_7

    .line 108
    const-string v3, "/system/fonts/SamsungNeoNum_3T.ttf"

    invoke-static {v3}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v3

    sput-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->SamsungNumber3T:Landroid/graphics/Typeface;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 117
    :cond_7
    :goto_1
    :try_start_1
    sget-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->SamsungNumber3T:Landroid/graphics/Typeface;

    if-nez v3, :cond_8

    if-nez v1, :cond_8

    .line 119
    const-string v3, ""

    const-string v4, "sn3T set again"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    const-string v3, "/system/fonts/SamsungSans-Num3T.ttf"

    invoke-static {v3}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v3

    sput-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->SamsungNumber3T:Landroid/graphics/Typeface;

    .line 123
    :cond_8
    sget-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->SamsungNumber3L:Landroid/graphics/Typeface;

    if-nez v3, :cond_9

    if-nez v1, :cond_9

    .line 125
    const-string v3, "/system/fonts/SamsungSans-Num3L.ttf"

    invoke-static {v3}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v3

    sput-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->SamsungNumber3L:Landroid/graphics/Typeface;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 131
    :cond_9
    :goto_2
    return-void

    .line 51
    .end local v1    # "isSupportSamsungFontFamily":Z
    .end local v2    # "salesCode":Ljava/lang/String;
    :cond_a
    sput-boolean v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->ACTIONBAR_USE:Z

    goto/16 :goto_0

    .line 110
    .restart local v1    # "isSupportSamsungFontFamily":Z
    .restart local v2    # "salesCode":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 111
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    .line 112
    const-string v3, ""

    const-string v4, "font is not enable."

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 127
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 128
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    .line 129
    const-string v3, ""

    const-string v4, "font is not enable."

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public onTerminate()V
    .locals 0

    .prologue
    .line 193
    invoke-super {p0}, Landroid/app/Application;->onTerminate()V

    .line 194
    return-void
.end method
