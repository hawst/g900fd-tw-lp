.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;
.super Landroid/content/ContentProvider;
.source "AccuContentProvider.java"


# static fields
.field public static final ACCU_DAEMON_PACKAGENAME_URI:Landroid/net/Uri;

.field public static final ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

.field public static final ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

.field public static final ACCU_PHOTOSINFO_URI:Landroid/net/Uri;

.field public static final ACCU_WEATHERINFO_HOUR_URI:Landroid/net/Uri;

.field public static final ACCU_WEATHER_LOG_URI:Landroid/net/Uri;

.field private static final DB_VERSION:I = 0x2

.field public static final PROVIDER_DAEMON_NAME:Ljava/lang/String; = "com.sec.android.daemonapp.ap.accuweather.provider"

.field public static final PROVIDER_WEATHER_NAME:Ljava/lang/String; = "com.sec.android.widgetapp.ap.hero.accuweather"

.field private static final SETTINGS:I = 0x1

.field private static final WEATHERINFO:I = 0x2

.field private static final WEATHERLOG:I = 0x3

.field private static final WEATHERVIEFFECT:I = 0x4

.field public static final WEATHER_VIEFFECT_URI:Landroid/net/Uri;

.field private static sAccuSettingsProjection:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sAccuWeatherInfoHourProjection:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sAccuWeatherInfoProjection:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sAccuWeatherLogProjections:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sAccuWeatherViEffectProjections:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sUriMatcher:Landroid/content/UriMatcher;


# instance fields
.field private mOpenHelper:Lcom/sec/android/widgetapp/ap/hero/accuweather/db/OpenHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 29
    const-string v0, "content://com.sec.android.daemonapp.ap.accuweather.provider/settings"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    .line 32
    const-string v0, "content://com.sec.android.daemonapp.ap.accuweather.provider/weatherinfo"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    .line 35
    const-string v0, "content://com.sec.android.daemonapp.ap.accuweather.provider/packagename"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_PACKAGENAME_URI:Landroid/net/Uri;

    .line 38
    const-string v0, "content://com.sec.android.daemonapp.ap.accuweather.provider/weatherlog"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_WEATHER_LOG_URI:Landroid/net/Uri;

    .line 41
    const-string v0, "content://com.sec.android.daemonapp.ap.accuweather.provider/photosinfo"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_PHOTOSINFO_URI:Landroid/net/Uri;

    .line 44
    const-string v0, "content://com.sec.android.widgetapp.ap.hero.accuweather/vieffect"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->WEATHER_VIEFFECT_URI:Landroid/net/Uri;

    .line 47
    const-string v0, "content://com.sec.android.daemonapp.ap.accuweather.provider/weatherinfo_hour"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_WEATHERINFO_HOUR_URI:Landroid/net/Uri;

    .line 73
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    .line 75
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.daemonapp.ap.accuweather.provider"

    const-string v2, "settings"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 76
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.daemonapp.ap.accuweather.provider"

    const-string v2, "weatherinfo"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 78
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.daemonapp.ap.accuweather.provider"

    const-string v2, "weatherlog"

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 79
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.sec.android.widgetapp.ap.hero.accuweather"

    const-string v2, "vieffect"

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 81
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuSettingsProjection:Ljava/util/HashMap;

    .line 82
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuSettingsProjection:Ljava/util/HashMap;

    const-string v1, "TEMP_SCALE"

    const-string v2, "TEMP_SCALE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuSettingsProjection:Ljava/util/HashMap;

    const-string v1, "AUTO_REFRESH_TIME"

    const-string v2, "AUTO_REFRESH_TIME"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuSettingsProjection:Ljava/util/HashMap;

    const-string v1, "AUTO_REF_NEXT_TIME"

    const-string v2, "AUTO_REF_NEXT_TIME"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuSettingsProjection:Ljava/util/HashMap;

    const-string v1, "AUTO_SCROLL"

    const-string v2, "AUTO_SCROLL"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuSettingsProjection:Ljava/util/HashMap;

    const-string v1, "REFRESH_ENTERING"

    const-string v2, "REFRESH_ENTERING"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuSettingsProjection:Ljava/util/HashMap;

    const-string v1, "REFRESH_ROAMING"

    const-string v2, "REFRESH_ROAMING"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuSettingsProjection:Ljava/util/HashMap;

    const-string v1, "CHECK_CURRENT_CITY_LOCATION"

    const-string v2, "CHECK_CURRENT_CITY_LOCATION"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuSettingsProjection:Ljava/util/HashMap;

    const-string v1, "NOTIFICATION"

    const-string v2, "NOTIFICATION"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuSettingsProjection:Ljava/util/HashMap;

    const-string v1, "NOTIFICATION_SET_TIME"

    const-string v2, "NOTIFICATION_SET_TIME"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuSettingsProjection:Ljava/util/HashMap;

    const-string v1, "LAST_SEL_LOCATION"

    const-string v2, "LAST_SEL_LOCATION"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuSettingsProjection:Ljava/util/HashMap;

    const-string v1, "EFFECT_SUNNY"

    const-string v2, "EFFECT_SUNNY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuSettingsProjection:Ljava/util/HashMap;

    const-string v1, "EFFECT_CLOUDY"

    const-string v2, "EFFECT_CLOUDY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuSettingsProjection:Ljava/util/HashMap;

    const-string v1, "EFFECT_SNOW"

    const-string v2, "EFFECT_SNOW"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuSettingsProjection:Ljava/util/HashMap;

    const-string v1, "EFFECT_RAIN"

    const-string v2, "EFFECT_RAIN"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuSettingsProjection:Ljava/util/HashMap;

    const-string v1, "EFFECT_THUNDER"

    const-string v2, "EFFECT_THUNDER"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuSettingsProjection:Ljava/util/HashMap;

    const-string v1, "EFFECT_BLUR"

    const-string v2, "EFFECT_BLUR"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuSettingsProjection:Ljava/util/HashMap;

    const-string v1, "EFFECT_SCALE_DOWN"

    const-string v2, "EFFECT_SCALE_DOWN"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuSettingsProjection:Ljava/util/HashMap;

    const-string v1, "EFFECT_SEMSOR"

    const-string v2, "EFFECT_SEMSOR"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuSettingsProjection:Ljava/util/HashMap;

    const-string v1, "EFFECT_ANIM_CITY_CHANGE"

    const-string v2, "EFFECT_ANIM_CITY_CHANGE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    .line 112
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "NAME"

    const-string v2, "NAME"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "STATE"

    const-string v2, "STATE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "LOCATION"

    const-string v2, "LOCATION"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TIMEZONE"

    const-string v2, "TIMEZONE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "SUMMER_TIME"

    const-string v2, "SUMMER_TIME"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "LATITUDE"

    const-string v2, "LATITUDE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "LONGITUDE"

    const-string v2, "LONGITUDE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "REAL_LOCATION"

    const-string v2, "REAL_LOCATION"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TEMP_SCALE"

    const-string v2, "TEMP_SCALE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TODAY_DATE"

    const-string v2, "TODAY_DATE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TODAY_TEMP"

    const-string v2, "TODAY_TEMP"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TODAY_HIGH_TEMP"

    const-string v2, "TODAY_HIGH_TEMP"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TODAY_LOW_TEMP"

    const-string v2, "TODAY_LOW_TEMP"

    .line 127
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TODAY_ICON_NUM"

    const-string v2, "TODAY_ICON_NUM"

    .line 129
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TODAY_WIND_DIRECTION"

    const-string v2, "TODAY_WIND_DIRECTION"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TODAY_WIND_SPEED"

    const-string v2, "TODAY_WIND_SPEED"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TODAY_WEATHER_TEXT"

    const-string v2, "TODAY_WEATHER_TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TODAY_WEATHER_URL"

    const-string v2, "TODAY_WEATHER_URL"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TODAY_REALFELL"

    const-string v2, "TODAY_REALFELL"

    .line 139
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TODAY_SUNRISE_TIME"

    const-string v2, "TODAY_SUNRISE_TIME"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TODAY_SUNSET_TIME"

    const-string v2, "TODAY_SUNSET_TIME"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TODAY_DAY_RAIN_PROBABILITY"

    const-string v2, "TODAY_DAY_RAIN_PROBABILITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TODAY_DAY_SNOW_PROBABILITY"

    const-string v2, "TODAY_DAY_SNOW_PROBABILITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TODAY_DAY_HAIL_PROBABILITY"

    const-string v2, "TODAY_DAY_HAIL_PROBABILITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TODAY_DAY_PRECIPITATION_PROBABILITY"

    const-string v2, "TODAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TODAY_DAY_RAIN_AMOUNT"

    const-string v2, "TODAY_DAY_RAIN_AMOUNT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TODAY_DAY_SNOW_AMOUNT"

    const-string v2, "TODAY_DAY_SNOW_AMOUNT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TODAY_DAY_HAIL_AMOUNT"

    const-string v2, "TODAY_DAY_HAIL_AMOUNT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TODAY_DAY_PRECIPITATION_AMOUNT"

    const-string v2, "TODAY_DAY_PRECIPITATION_AMOUNT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TODAY_NIGHT_RAIN_PROBABILITY"

    const-string v2, "TODAY_NIGHT_RAIN_PROBABILITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TODAY_NIGHT_SNOW_PROBABILITY"

    const-string v2, "TODAY_NIGHT_SNOW_PROBABILITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TODAY_NIGHT_HAIL_PROBABILITY"

    const-string v2, "TODAY_NIGHT_HAIL_PROBABILITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TODAY_NIGHT_PRECIPITATION_PROBABILITY"

    const-string v2, "TODAY_NIGHT_PRECIPITATION_PROBABILITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TODAY_NIGHT_RAIN_AMOUNT"

    const-string v2, "TODAY_NIGHT_RAIN_AMOUNT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TODAY_NIGHT_SNOW_AMOUNT"

    const-string v2, "TODAY_NIGHT_SNOW_AMOUNT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TODAY_NIGHT_HAIL_AMOUNT"

    const-string v2, "TODAY_NIGHT_HAIL_AMOUNT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TODAY_NIGHT_PRECIPITATION_AMOUNT"

    const-string v2, "TODAY_NIGHT_PRECIPITATION_AMOUNT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "ONEDAY_DAY_RAIN_PROBABILITY"

    const-string v2, "ONEDAY_DAY_RAIN_PROBABILITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "ONEDAY_DAY_SNOW_PROBABILITY"

    const-string v2, "ONEDAY_DAY_SNOW_PROBABILITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "ONEDAY_DAY_HAIL_PROBABILITY"

    const-string v2, "ONEDAY_DAY_HAIL_PROBABILITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "ONEDAY_DAY_PRECIPITATION_PROBABILITY"

    const-string v2, "ONEDAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "ONEDAY_DAY_RAIN_AMOUNT"

    const-string v2, "ONEDAY_DAY_RAIN_AMOUNT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "ONEDAY_DAY_SNOW_AMOUNT"

    const-string v2, "ONEDAY_DAY_SNOW_AMOUNT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "ONEDAY_DAY_HAIL_AMOUNT"

    const-string v2, "ONEDAY_DAY_HAIL_AMOUNT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "ONEDAY_DAY_PRECIPITATION_AMOUNT"

    const-string v2, "ONEDAY_DAY_PRECIPITATION_AMOUNT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "ONEDAY_NIGHT_RAIN_PROBABILITY"

    const-string v2, "ONEDAY_NIGHT_RAIN_PROBABILITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "ONEDAY_NIGHT_SNOW_PROBABILITY"

    const-string v2, "ONEDAY_NIGHT_SNOW_PROBABILITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "ONEDAY_NIGHT_HAIL_PROBABILITY"

    const-string v2, "ONEDAY_NIGHT_HAIL_PROBABILITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "ONEDAY_NIGHT_PRECIPITATION_PROBABILITY"

    const-string v2, "ONEDAY_NIGHT_PRECIPITATION_PROBABILITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "ONEDAY_NIGHT_RAIN_AMOUNT"

    const-string v2, "ONEDAY_NIGHT_RAIN_AMOUNT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "ONEDAY_NIGHT_SNOW_AMOUNT"

    const-string v2, "ONEDAY_NIGHT_SNOW_AMOUNT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "ONEDAY_NIGHT_HAIL_AMOUNT"

    const-string v2, "ONEDAY_NIGHT_HAIL_AMOUNT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "ONEDAY_NIGHT_PRECIPITATION_AMOUNT"

    const-string v2, "ONEDAY_NIGHT_PRECIPITATION_AMOUNT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "ONEDAY_SUNRISE_TIME"

    const-string v2, "ONEDAY_SUNRISE_TIME"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "ONEDAY_SUNSET_TIME"

    const-string v2, "ONEDAY_SUNSET_TIME"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "ONEDAY_HIGH_TEMP"

    const-string v2, "ONEDAY_HIGH_TEMP"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "ONEDAY_LOW_TEMP"

    const-string v2, "ONEDAY_LOW_TEMP"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 224
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "ONEDAY_ICON_NUM"

    const-string v2, "ONEDAY_ICON_NUM"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "ONEDAY_URL"

    const-string v2, "ONEDAY_URL"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TWODAY_HIGH_TEMP"

    const-string v2, "TWODAY_HIGH_TEMP"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TWODAY_LOW_TEMP"

    const-string v2, "TWODAY_LOW_TEMP"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TWODAY_ICON_NUM"

    const-string v2, "TWODAY_ICON_NUM"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TWODAY_URL"

    const-string v2, "TWODAY_URL"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TWODAY_DAY_RAIN_PROBABILITY"

    const-string v2, "TWODAY_DAY_RAIN_PROBABILITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TWODAY_DAY_SNOW_PROBABILITY"

    const-string v2, "TWODAY_DAY_SNOW_PROBABILITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TWODAY_DAY_HAIL_PROBABILITY"

    const-string v2, "TWODAY_DAY_HAIL_PROBABILITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TWODAY_DAY_PRECIPITATION_PROBABILITY"

    const-string v2, "TWODAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TWODAY_SUNRISE_TIME"

    const-string v2, "TWODAY_SUNRISE_TIME"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 246
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TWODAY_SUNSET_TIME"

    const-string v2, "TWODAY_SUNSET_TIME"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 249
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "THREEDAY_HIGH_TEMP"

    const-string v2, "THREEDAY_HIGH_TEMP"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "THREEDAY_LOW_TEMP"

    const-string v2, "THREEDAY_LOW_TEMP"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 253
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "THREEDAY_ICON_NUM"

    const-string v2, "THREEDAY_ICON_NUM"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 255
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "THREEDAY_URL"

    const-string v2, "THREEDAY_URL"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "THREEDAY_DAY_RAIN_PROBABILITY"

    const-string v2, "THREEDAY_DAY_RAIN_PROBABILITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "THREEDAY_DAY_SNOW_PROBABILITY"

    const-string v2, "THREEDAY_DAY_SNOW_PROBABILITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 261
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "THREEDAY_DAY_HAIL_PROBABILITY"

    const-string v2, "THREEDAY_DAY_HAIL_PROBABILITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 263
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "THREEDAY_DAY_PRECIPITATION_PROBABILITY"

    const-string v2, "THREEDAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "THREEDAY_SUNRISE_TIME"

    const-string v2, "THREEDAY_SUNRISE_TIME"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 267
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "THREEDAY_SUNSET_TIME"

    const-string v2, "THREEDAY_SUNSET_TIME"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "FOURDAY_HIGH_TEMP"

    const-string v2, "FOURDAY_HIGH_TEMP"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 272
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "FOURDAY_LOW_TEMP"

    const-string v2, "FOURDAY_LOW_TEMP"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 274
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "FOURDAY_ICON_NUM"

    const-string v2, "FOURDAY_ICON_NUM"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "FOURDAY_URL"

    const-string v2, "FOURDAY_URL"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "FOURDAY_DAY_RAIN_PROBABILITY"

    const-string v2, "FOURDAY_DAY_RAIN_PROBABILITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 280
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "FOURDAY_DAY_SNOW_PROBABILITY"

    const-string v2, "FOURDAY_DAY_SNOW_PROBABILITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "FOURDAY_DAY_HAIL_PROBABILITY"

    const-string v2, "FOURDAY_DAY_HAIL_PROBABILITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "FOURDAY_DAY_PRECIPITATION_PROBABILITY"

    const-string v2, "FOURDAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "FOURDAY_SUNRISE_TIME"

    const-string v2, "FOURDAY_SUNRISE_TIME"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "FOURDAY_SUNSET_TIME"

    const-string v2, "FOURDAY_SUNSET_TIME"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 291
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "FIVEDAY_HIGH_TEMP"

    const-string v2, "FIVEDAY_HIGH_TEMP"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 293
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "FIVEDAY_LOW_TEMP"

    const-string v2, "FIVEDAY_LOW_TEMP"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 295
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "FIVEDAY_ICON_NUM"

    const-string v2, "FIVEDAY_ICON_NUM"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "FIVEDAY_URL"

    const-string v2, "FIVEDAY_URL"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "FIVEDAY_DAY_RAIN_PROBABILITY"

    const-string v2, "FIVEDAY_DAY_RAIN_PROBABILITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 301
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "FIVEDAY_DAY_SNOW_PROBABILITY"

    const-string v2, "FIVEDAY_DAY_SNOW_PROBABILITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 303
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "FIVEDAY_DAY_HAIL_PROBABILITY"

    const-string v2, "FIVEDAY_DAY_HAIL_PROBABILITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 305
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "FIVEDAY_DAY_PRECIPITATION_PROBABILITY"

    const-string v2, "FIVEDAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 307
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "FIVEDAY_SUNRISE_TIME"

    const-string v2, "FIVEDAY_SUNRISE_TIME"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "FIVEDAY_SUNSET_TIME"

    const-string v2, "FIVEDAY_SUNSET_TIME"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 312
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "SIXDAY_HIGH_TEMP"

    const-string v2, "SIXDAY_HIGH_TEMP"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 314
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "SIXDAY_LOW_TEMP"

    const-string v2, "SIXDAY_LOW_TEMP"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 316
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "SIXDAY_ICON_NUM"

    const-string v2, "SIXDAY_ICON_NUM"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 318
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "SIXDAY_URL"

    const-string v2, "SIXDAY_URL"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 320
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "SIXDAY_DAY_RAIN_PROBABILITY"

    const-string v2, "SIXDAY_DAY_RAIN_PROBABILITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 322
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "SIXDAY_DAY_SNOW_PROBABILITY"

    const-string v2, "SIXDAY_DAY_SNOW_PROBABILITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 324
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "SIXDAY_DAY_HAIL_PROBABILITY"

    const-string v2, "SIXDAY_DAY_HAIL_PROBABILITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 326
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "SIXDAY_DAY_PRECIPITATION_PROBABILITY"

    const-string v2, "SIXDAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 328
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "SIXDAY_SUNRISE_TIME"

    const-string v2, "SIXDAY_SUNRISE_TIME"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 330
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "SIXDAY_SUNSET_TIME"

    const-string v2, "SIXDAY_SUNSET_TIME"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 333
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "UPDATE_DATE"

    const-string v2, "UPDATE_DATE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 334
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    const-string v1, "TODAY_WIND_DIRECTION"

    const-string v2, "TODAY_WIND_DIRECTION"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 337
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherLogProjections:Ljava/util/HashMap;

    .line 338
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherLogProjections:Ljava/util/HashMap;

    const-string v1, "NOW_TIME"

    const-string v2, "NOW_TIME"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 339
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherLogProjections:Ljava/util/HashMap;

    const-string v1, "FAIL_LOG"

    const-string v2, "FAIL_LOG"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 342
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    .line 343
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_1"

    const-string v2, "EFFECT_1"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 344
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_2"

    const-string v2, "EFFECT_2"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 345
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_3"

    const-string v2, "EFFECT_3"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 346
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_4"

    const-string v2, "EFFECT_4"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 347
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_5"

    const-string v2, "EFFECT_5"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 348
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_6"

    const-string v2, "EFFECT_6"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 349
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_7"

    const-string v2, "EFFECT_7"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 350
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_8"

    const-string v2, "EFFECT_8"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 351
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_9"

    const-string v2, "EFFECT_9"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 352
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_10"

    const-string v2, "EFFECT_10"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 353
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_11"

    const-string v2, "EFFECT_11"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 354
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_12"

    const-string v2, "EFFECT_12"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 355
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_13"

    const-string v2, "EFFECT_13"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 356
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_14"

    const-string v2, "EFFECT_14"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 357
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_15"

    const-string v2, "EFFECT_15"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 358
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_16"

    const-string v2, "EFFECT_16"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 359
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_17"

    const-string v2, "EFFECT_17"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 360
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_18"

    const-string v2, "EFFECT_18"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 361
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_19"

    const-string v2, "EFFECT_19"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 362
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_20"

    const-string v2, "EFFECT_20"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 363
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_21"

    const-string v2, "EFFECT_21"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 364
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_22"

    const-string v2, "EFFECT_22"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 365
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_23"

    const-string v2, "EFFECT_23"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 366
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_24"

    const-string v2, "EFFECT_24"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 367
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_25"

    const-string v2, "EFFECT_25"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 368
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_26"

    const-string v2, "EFFECT_26"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 369
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_27"

    const-string v2, "EFFECT_27"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 370
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_28"

    const-string v2, "EFFECT_28"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 371
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_29"

    const-string v2, "EFFECT_29"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 372
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_30"

    const-string v2, "EFFECT_30"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 373
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_31"

    const-string v2, "EFFECT_31"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 374
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_32"

    const-string v2, "EFFECT_32"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 375
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_33"

    const-string v2, "EFFECT_33"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 376
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_34"

    const-string v2, "EFFECT_34"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 377
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_35"

    const-string v2, "EFFECT_35"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 378
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_36"

    const-string v2, "EFFECT_36"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 379
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_37"

    const-string v2, "EFFECT_37"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 380
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_38"

    const-string v2, "EFFECT_38"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 381
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_39"

    const-string v2, "EFFECT_39"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 382
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    const-string v1, "EFFECT_40"

    const-string v2, "EFFECT_40"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 384
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    .line 385
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "DATE_1"

    const-string v2, "DATE_1"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 386
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "HOUR_1"

    const-string v2, "HOUR_1"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 387
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "TEMP_1"

    const-string v2, "TEMP_1"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 388
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "ICON_NUM_1"

    const-string v2, "ICON_NUM_1"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "RAIN_FORECAST_1"

    const-string v2, "RAIN_FORECAST_1"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 391
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "WIND_SPEED_1"

    const-string v2, "WIND_SPEED_1"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 393
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "DATE_2"

    const-string v2, "DATE_2"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 394
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "HOUR_2"

    const-string v2, "HOUR_2"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 395
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "TEMP_2"

    const-string v2, "TEMP_2"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 396
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "ICON_NUM_2"

    const-string v2, "ICON_NUM_2"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 397
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "RAIN_FORECAST_2"

    const-string v2, "RAIN_FORECAST_2"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 399
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "WIND_SPEED_2"

    const-string v2, "WIND_SPEED_2"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 401
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "DATE_3"

    const-string v2, "DATE_3"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 402
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "HOUR_3"

    const-string v2, "HOUR_3"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 403
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "TEMP_3"

    const-string v2, "TEMP_3"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 404
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "ICON_NUM_3"

    const-string v2, "ICON_NUM_3"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 405
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "RAIN_FORECAST_3"

    const-string v2, "RAIN_FORECAST_3"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 407
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "WIND_SPEED_3"

    const-string v2, "WIND_SPEED_3"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 409
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "DATE_4"

    const-string v2, "DATE_4"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "HOUR_4"

    const-string v2, "HOUR_4"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 411
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "TEMP_4"

    const-string v2, "TEMP_4"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 412
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "ICON_NUM_4"

    const-string v2, "ICON_NUM_4"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 413
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "RAIN_FORECAST_4"

    const-string v2, "RAIN_FORECAST_4"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 415
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "WIND_SPEED_4"

    const-string v2, "WIND_SPEED_4"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 417
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "DATE_5"

    const-string v2, "DATE_5"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 418
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "HOUR_5"

    const-string v2, "HOUR_5"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 419
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "TEMP_5"

    const-string v2, "TEMP_5"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 420
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "ICON_NUM_5"

    const-string v2, "ICON_NUM_5"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 421
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "RAIN_FORECAST_5"

    const-string v2, "RAIN_FORECAST_5"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 423
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "WIND_SPEED_5"

    const-string v2, "WIND_SPEED_5"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 425
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "DATE_6"

    const-string v2, "DATE_6"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 426
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "HOUR_6"

    const-string v2, "HOUR_6"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 427
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "TEMP_6"

    const-string v2, "TEMP_6"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 428
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "ICON_NUM_6"

    const-string v2, "ICON_NUM_6"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 429
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "RAIN_FORECAST_6"

    const-string v2, "RAIN_FORECAST_6"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 431
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "WIND_SPEED_6"

    const-string v2, "WIND_SPEED_6"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 433
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "DATE_7"

    const-string v2, "DATE_7"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 434
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "HOUR_7"

    const-string v2, "HOUR_7"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 435
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "TEMP_7"

    const-string v2, "TEMP_7"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 436
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "ICON_NUM_7"

    const-string v2, "ICON_NUM_7"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 437
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "RAIN_FORECAST_7"

    const-string v2, "RAIN_FORECAST_7"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 439
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "WIND_SPEED_7"

    const-string v2, "WIND_SPEED_7"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 441
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "DATE_8"

    const-string v2, "DATE_8"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 442
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "HOUR_8"

    const-string v2, "HOUR_8"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 443
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "TEMP_8"

    const-string v2, "TEMP_8"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 444
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "ICON_NUM_8"

    const-string v2, "ICON_NUM_8"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 445
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "RAIN_FORECAST_8"

    const-string v2, "RAIN_FORECAST_8"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 447
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "WIND_SPEED_8"

    const-string v2, "WIND_SPEED_8"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 449
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "DATE_9"

    const-string v2, "DATE_9"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 450
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "HOUR_9"

    const-string v2, "HOUR_9"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 451
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "TEMP_9"

    const-string v2, "TEMP_9"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 452
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "ICON_NUM_9"

    const-string v2, "ICON_NUM_9"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 453
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "RAIN_FORECAST_9"

    const-string v2, "RAIN_FORECAST_9"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 455
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "WIND_SPEED_9"

    const-string v2, "WIND_SPEED_9"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 457
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "DATE_10"

    const-string v2, "DATE_10"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 458
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "HOUR_10"

    const-string v2, "HOUR_10"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 459
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "TEMP_10"

    const-string v2, "TEMP_10"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 460
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "ICON_NUM_10"

    const-string v2, "ICON_NUM_10"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 461
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "RAIN_FORECAST_10"

    const-string v2, "RAIN_FORECAST_10"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 463
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "WIND_SPEED_10"

    const-string v2, "WIND_SPEED_10"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 465
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "DATE_11"

    const-string v2, "DATE_11"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 466
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "HOUR_11"

    const-string v2, "HOUR_11"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 467
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "TEMP_11"

    const-string v2, "TEMP_11"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 468
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "ICON_NUM_11"

    const-string v2, "ICON_NUM_11"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 469
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "RAIN_FORECAST_11"

    const-string v2, "RAIN_FORECAST_11"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 471
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "WIND_SPEED_11"

    const-string v2, "WIND_SPEED_11"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 473
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "DATE_12"

    const-string v2, "DATE_12"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 474
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "HOUR_12"

    const-string v2, "HOUR_12"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 475
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "TEMP_12"

    const-string v2, "TEMP_12"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 476
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "ICON_NUM_12"

    const-string v2, "ICON_NUM_12"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 477
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "RAIN_FORECAST_12"

    const-string v2, "RAIN_FORECAST_12"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 479
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "WIND_SPEED_12"

    const-string v2, "WIND_SPEED_12"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 481
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "DATE_13"

    const-string v2, "DATE_13"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 482
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "HOUR_13"

    const-string v2, "HOUR_13"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 483
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "TEMP_13"

    const-string v2, "TEMP_13"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 484
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "ICON_NUM_13"

    const-string v2, "ICON_NUM_13"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 485
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "RAIN_FORECAST_13"

    const-string v2, "RAIN_FORECAST_13"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 487
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoHourProjection:Ljava/util/HashMap;

    const-string v1, "WIND_SPEED_13"

    const-string v2, "WIND_SPEED_13"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 488
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .locals 11
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # [Landroid/content/ContentValues;

    .prologue
    .line 562
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 563
    .local v6, "time":J
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->mOpenHelper:Lcom/sec/android/widgetapp/ap/hero/accuweather/db/OpenHelper;

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/OpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 564
    .local v3, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v2, -0x1

    .line 566
    .local v2, "count":I
    :try_start_0
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 568
    sget-object v8, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v8, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v8

    packed-switch v8, :pswitch_data_0

    .line 581
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 582
    new-instance v8, Ljava/lang/IllegalArgumentException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Unknown URI "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 584
    :catch_0
    move-exception v4

    .line 585
    .local v4, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v8, ""

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Exception bulkInsert()-"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 587
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 589
    .end local v4    # "e":Ljava/lang/Exception;
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long v6, v8, v6

    .line 590
    const-string v8, ""

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Provider BulkInsert requested : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", Count : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", process time : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 592
    return v2

    .line 570
    :pswitch_0
    :try_start_2
    const-string v8, "MY_WEATHER_INFO"

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v3, v8, v9, v10}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 571
    const/4 v5, 0x0

    .local v5, "index":I
    :goto_1
    array-length v8, p2

    if-ge v5, v8, :cond_1

    .line 572
    const-string v8, "MY_WEATHER_INFO"

    const/4 v9, 0x0

    aget-object v10, p2, v5

    invoke-virtual {v3, v8, v9, v10}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 573
    .local v0, "cnt":J
    const-wide/16 v8, 0x0

    cmp-long v8, v0, v8

    if-lez v8, :cond_0

    .line 574
    add-int/lit8 v2, v2, 0x1

    .line 571
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 577
    .end local v0    # "cnt":J
    :cond_1
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 587
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_0

    .end local v5    # "index":I
    :catchall_0
    move-exception v8

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v8

    .line 568
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 493
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 494
    .local v2, "time":J
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->mOpenHelper:Lcom/sec/android/widgetapp/ap/hero/accuweather/db/OpenHelper;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/OpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 496
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    sget-object v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v4, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 504
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unknown URI "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 498
    :pswitch_0
    const-string v4, "MY_WEATHER_INFO"

    invoke-virtual {v1, v4, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 506
    .local v0, "count":I
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    .line 507
    const-string v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "cp del Count : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", process time : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 508
    return v0

    .line 501
    .end local v0    # "count":I
    :pswitch_1
    const-string v4, "MY_WEATHER_LOG"

    invoke-virtual {v1, v4, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 502
    .restart local v0    # "count":I
    goto :goto_0

    .line 496
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 512
    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 12
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "initialValues"    # Landroid/content/ContentValues;

    .prologue
    const/4 v1, 0x0

    .line 516
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 519
    .local v6, "time":J
    if-eqz p2, :cond_0

    .line 520
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8, p2}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    .line 524
    .local v8, "values":Landroid/content/ContentValues;
    :goto_0
    sget-object v9, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v9, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    .line 525
    .local v2, "match":I
    packed-switch v2, :pswitch_data_0

    .line 539
    new-instance v9, Ljava/lang/IllegalArgumentException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Unknown URI "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 522
    .end local v2    # "match":I
    .end local v8    # "values":Landroid/content/ContentValues;
    :cond_0
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .restart local v8    # "values":Landroid/content/ContentValues;
    goto :goto_0

    .line 527
    .restart local v2    # "match":I
    :pswitch_0
    const-string v5, "MY_WEATHER_INFO"

    .line 542
    .local v5, "table":Ljava/lang/String;
    :goto_1
    iget-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->mOpenHelper:Lcom/sec/android/widgetapp/ap/hero/accuweather/db/OpenHelper;

    invoke-virtual {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/OpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 543
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-wide/16 v3, 0x0

    .line 544
    .local v3, "rowId":J
    if-eqz v5, :cond_1

    if-eqz v8, :cond_1

    .line 545
    invoke-virtual {v0, v5, v1, v8}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v3

    .line 548
    :cond_1
    const-wide/16 v9, 0x0

    cmp-long v9, v3, v9

    if-lez v9, :cond_2

    .line 549
    invoke-static {p1, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 550
    .local v1, "insertUri":Landroid/net/Uri;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    sub-long v6, v9, v6

    .line 551
    const-string v9, ""

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "CP insert insertUri : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", pt : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 557
    .end local v1    # "insertUri":Landroid/net/Uri;
    :goto_2
    return-object v1

    .line 530
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v3    # "rowId":J
    .end local v5    # "table":Ljava/lang/String;
    :pswitch_1
    const-string v5, "MY_WEATHER_SETTING_INFO"

    .line 531
    .restart local v5    # "table":Ljava/lang/String;
    goto :goto_1

    .line 533
    .end local v5    # "table":Ljava/lang/String;
    :pswitch_2
    const-string v5, "MY_WEATHER_LOG"

    .line 534
    .restart local v5    # "table":Ljava/lang/String;
    goto :goto_1

    .line 536
    .end local v5    # "table":Ljava/lang/String;
    :pswitch_3
    const-string v5, "MY_WEATHER_VIEFFECT"

    .line 537
    .restart local v5    # "table":Ljava/lang/String;
    goto :goto_1

    .line 556
    .restart local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v3    # "rowId":J
    :cond_2
    const-string v9, ""

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Failed to insert row into "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 525
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onCreate()Z
    .locals 6

    .prologue
    .line 597
    const-string v3, ""

    const-string v4, "Provider Created"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 598
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 599
    .local v1, "context":Landroid/content/Context;
    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/OpenHelper;

    const/4 v4, 0x2

    invoke-direct {v3, v1, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/OpenHelper;-><init>(Landroid/content/Context;I)V

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->mOpenHelper:Lcom/sec/android/widgetapp/ap/hero/accuweather/db/OpenHelper;

    .line 602
    invoke-static {v1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 603
    .local v0, "appWidgetManager":Landroid/appwidget/AppWidgetManager;
    new-instance v3, Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const-class v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/WeatherClock;

    .line 604
    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    invoke-virtual {v0, v3}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v2

    .line 606
    .local v2, "ids":[I
    if-eqz v2, :cond_0

    array-length v3, v2

    if-lez v3, :cond_0

    .line 607
    const-string v3, ""

    const-string v4, "EW_UP"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 608
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.sec.android.widgetapp.ap.hero.accuweather.widget.action.APPWIDGET_UPDATE"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 612
    :cond_0
    const/4 v3, 0x1

    return v3
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 11
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 617
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    .line 618
    .local v9, "time":J
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->mOpenHelper:Lcom/sec/android/widgetapp/ap/hero/accuweather/db/OpenHelper;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/OpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 619
    .local v1, "weatherdb":Landroid/database/sqlite/SQLiteDatabase;
    if-eqz v1, :cond_1

    .line 620
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 621
    .local v0, "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    sget-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 640
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown URI "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 623
    :pswitch_0
    const-string v2, "MY_WEATHER_INFO"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 624
    sget-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherInfoProjection:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 643
    :goto_0
    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v7, p5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 645
    .local v8, "c":Landroid/database/Cursor;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v9, v2, v9

    .line 646
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cp query  pt : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 647
    if-eqz v8, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-interface {v8, v2, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 652
    .end local v0    # "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v8    # "c":Landroid/database/Cursor;
    :cond_0
    :goto_1
    return-object v8

    .line 627
    .restart local v0    # "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    :pswitch_1
    const-string v2, "MY_WEATHER_SETTING_INFO"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 628
    sget-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuSettingsProjection:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    goto :goto_0

    .line 631
    :pswitch_2
    const-string v2, "MY_WEATHER_LOG"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 632
    sget-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherLogProjections:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    goto :goto_0

    .line 636
    :pswitch_3
    const-string v2, "MY_WEATHER_VIEFFECT"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 637
    sget-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sAccuWeatherViEffectProjections:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    goto :goto_0

    .line 651
    .end local v0    # "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    :cond_1
    const-string v2, ""

    const-string v3, "cp get db e"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 652
    const/4 v8, 0x0

    goto :goto_1

    .line 621
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 656
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 657
    .local v2, "time":J
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->mOpenHelper:Lcom/sec/android/widgetapp/ap/hero/accuweather/db/OpenHelper;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/OpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 659
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    sget-object v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v4, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 671
    :pswitch_0
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unknown URI "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 661
    :pswitch_1
    const-string v4, "MY_WEATHER_INFO"

    invoke-virtual {v1, v4, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 673
    .local v0, "count":I
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    .line 674
    const-string v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "cp update : count : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", pt : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 675
    return v0

    .line 664
    .end local v0    # "count":I
    :pswitch_2
    const-string v4, "MY_WEATHER_SETTING_INFO"

    invoke-virtual {v1, v4, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 666
    .restart local v0    # "count":I
    goto :goto_0

    .line 668
    .end local v0    # "count":I
    :pswitch_3
    const-string v4, "MY_WEATHER_VIEFFECT"

    invoke-virtual {v1, v4, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 669
    .restart local v0    # "count":I
    goto :goto_0

    .line 659
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
