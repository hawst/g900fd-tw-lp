.class Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$22;
.super Ljava/lang/Object;
.source "WeatherClockCityListAnimationAndDelete.java"

# interfaces
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/LocationGetter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->getDataRefresh(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

.field final synthetic val$cityDataList:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;Ljava/util/ArrayList;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    .prologue
    .line 1781
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$22;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    iput-object p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$22;->val$cityDataList:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getLocation()V
    .locals 4

    .prologue
    .line 1783
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$22;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->mPopupDialog:Landroid/app/Dialog;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->access$3300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 1784
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$22;->val$cityDataList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1785
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$22;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->startUpdateAnimation()V
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->access$3400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;)V

    .line 1786
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$22;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->showRefreshDialog()V
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->access$3500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;)V

    .line 1787
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$22;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->access$3700(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-result-object v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$22;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    .line 1788
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->mUiHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->access$3600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x0

    .line 1787
    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->doManualRefresh(ILandroid/os/Handler;Ljava/lang/String;)V

    .line 1790
    :cond_0
    return-void
.end method
