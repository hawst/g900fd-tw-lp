.class Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$1;
.super Landroid/os/Handler;
.source "MyCurrentLocation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    .prologue
    .line 215
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 217
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 227
    :goto_0
    return-void

    .line 219
    :pswitch_0
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "cityinfo"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    .line 220
    .local v0, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->latitude:D
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    .line 221
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->longitude:D
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;)D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    iget-object v4, v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->currentLocationHandler:Landroid/os/Handler;

    .line 220
    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->addCityWeatherFromProvider_Deutsch(Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;)V

    goto :goto_0

    .line 217
    :pswitch_data_0
    .packed-switch 0x1b58
        :pswitch_0
    .end packed-switch
.end method
