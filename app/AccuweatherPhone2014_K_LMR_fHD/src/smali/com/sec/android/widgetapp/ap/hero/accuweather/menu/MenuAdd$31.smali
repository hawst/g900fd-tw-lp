.class Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$31;
.super Landroid/os/Handler;
.source "MenuAdd.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 1413
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v5, 0xc8

    const/4 v7, 0x0

    .line 1415
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 1416
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v4, "RESPONSE_BODY"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1417
    .local v2, "responseBody":Ljava/lang/String;
    const-string v4, "RESPONSE_CODE"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 1418
    .local v3, "responseCode":I
    if-ne v3, v5, :cond_4

    if-eqz v2, :cond_4

    .line 1419
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->parser:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$3400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    invoke-virtual {v4, v2, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->parseCityList(Ljava/lang/String;Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v1

    .line 1420
    .local v1, "lstResult":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;>;"
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->lstCity:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$3200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 1421
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->lstCity:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$3200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 1435
    :goto_0
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1436
    :cond_0
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mNoSearchText:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$1700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1437
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mNoSearchText:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$1700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/widget/TextView;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0d0027

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1439
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->setKeyboardVisible(Z)V
    invoke-static {v4, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$1200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;Z)V

    .line 1449
    :goto_1
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getListView()Landroid/widget/ListView;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 1450
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->cityListAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;
    invoke-static {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$3600(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 1452
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->cityListAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$3600(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;->notifyDataSetChanged()V

    .line 1453
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getListView()Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/widget/ListView;->setVisibility(I)V

    .line 1454
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->hideLoadingDialog()V
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    .line 1463
    .end local v1    # "lstResult":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;>;"
    :cond_1
    :goto_2
    return-void

    .line 1423
    .restart local v1    # "lstResult":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;>;"
    :cond_2
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->lstCity:Ljava/util/ArrayList;
    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$3202(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    goto :goto_0

    .line 1442
    :cond_3
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mNoSearchText:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$1700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/widget/TextView;

    move-result-object v4

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1443
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->setKeyboardVisible(Z)V
    invoke-static {v4, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$1200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;Z)V

    .line 1444
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->lstCity:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$3200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1446
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->disableEditField()V
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$3500(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    goto :goto_1

    .line 1455
    .end local v1    # "lstResult":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;>;"
    :cond_4
    if-eq v3, v5, :cond_5

    .line 1456
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->hideLoadingDialog()V
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    .line 1457
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->isActivityVisible()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1458
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    const v5, 0x7f0d001f

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    goto :goto_2

    .line 1461
    :cond_5
    const-string v4, ""

    const-string v5, "searchHandler : fail : location or responseBody are null"

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
