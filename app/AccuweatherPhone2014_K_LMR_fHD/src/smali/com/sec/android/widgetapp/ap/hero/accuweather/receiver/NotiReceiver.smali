.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;
.super Landroid/content/BroadcastReceiver;
.source "NotiReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData;
    }
.end annotation


# static fields
.field public static final NOTIFICATION_BR_PERMISSIONS:Ljava/lang/String; = "com.samsung.accessory.permission.TRANSPORTING_NOTIFICATION_ITEM"


# instance fields
.field private getDataHandler:Landroid/os/Handler;

.field getLocDataHandler:Landroid/os/Handler;

.field private mCtx:Landroid/content/Context;

.field private mCurLocation:Ljava/lang/String;

.field private mcl:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 43
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    .line 45
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCurLocation:Ljava/lang/String;

    .line 47
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->getDataHandler:Landroid/os/Handler;

    .line 825
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$1;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->getLocDataHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;
    .param p1, "x1"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .param p2, "x2"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->saveData(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)V

    return-void
.end method

.method public static checkGearMode(Landroid/content/Context;)Z
    .locals 5
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 994
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "connected_wearable"

    invoke-static {v3, v4}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 995
    .local v1, "gearMode":Ljava/lang/String;
    if-eqz v1, :cond_1

    const-string v3, "Gear"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "GALAXY Gear"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_1

    .line 1001
    .end local v1    # "gearMode":Ljava/lang/String;
    :cond_0
    :goto_0
    return v2

    .line 998
    .restart local v1    # "gearMode":Ljava/lang/String;
    :cond_1
    const/4 v2, 0x1

    goto :goto_0

    .line 999
    .end local v1    # "gearMode":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1000
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private checkResultCode(I)I
    .locals 2
    .param p1, "resultCode"    # I

    .prologue
    .line 948
    const/4 v0, 0x0

    .line 950
    .local v0, "result":I
    const/4 v1, -0x1

    if-ne v1, p1, :cond_1

    .line 951
    const/16 v0, 0x3e7

    .line 956
    :cond_0
    :goto_0
    return v0

    .line 952
    :cond_1
    if-nez p1, :cond_0

    .line 953
    const/16 v0, 0x3e7

    goto :goto_0
.end method

.method public static convertDrawableToBitmap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;
    .locals 6
    .param p0, "d"    # Landroid/graphics/drawable/Drawable;

    .prologue
    const/4 v5, 0x0

    .line 981
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 982
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 983
    .local v1, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v3

    invoke-virtual {p0, v5, v5, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 984
    invoke-virtual {p0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 985
    return-object v0
.end method

.method public static convertResizeBitmapToByte(Landroid/graphics/Bitmap;)[B
    .locals 4
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/16 v3, 0x4b

    .line 961
    const/4 v2, 0x0

    invoke-static {p0, v3, v3, v2}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 962
    .local v1, "scBitmap":Landroid/graphics/Bitmap;
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 963
    .local v0, "byteArrayStream":Ljava/io/ByteArrayOutputStream;
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x32

    invoke-virtual {v1, v2, v3, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 965
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    return-object v2
.end method

.method public static getBitmapByPackageName(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 969
    const/4 v1, 0x0

    .line 971
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, p1, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 972
    .local v0, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/pm/PackageManager;->getApplicationIcon(Landroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 973
    .local v2, "icon":Landroid/graphics/drawable/Drawable;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->convertDrawableToBitmap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 977
    .end local v0    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    .end local v2    # "icon":Landroid/graphics/drawable/Drawable;
    :goto_0
    return-object v1

    .line 974
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method private getCurrentLocation(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 815
    invoke-static {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkNetworkConnected(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 819
    :goto_0
    return-void

    .line 818
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->getLocationInfo()V

    goto :goto_0
.end method

.method private getLocationInfo()V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 822
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mcl:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->getLocDataHandler:Landroid/os/Handler;

    move v4, v2

    move v5, v3

    move v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->performGetCurrentLocation(Landroid/os/Handler;IZZZZ)V

    .line 823
    return-void
.end method

.method private getPhotoToString(I)Ljava/lang/String;
    .locals 7
    .param p1, "send_Weather_Image_id"    # I

    .prologue
    .line 261
    const-string v2, ""

    .line 265
    .local v2, "contactPhoto":Ljava/lang/String;
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v5, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 266
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 267
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 268
    .local v1, "bos":Ljava/io/ByteArrayOutputStream;
    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v6, 0x64

    invoke-virtual {v0, v5, v6, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 269
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    if-eqz v5, :cond_0

    .line 270
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 272
    :try_start_1
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 281
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "bos":Ljava/io/ByteArrayOutputStream;
    :cond_0
    :goto_0
    return-object v2

    .line 273
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v1    # "bos":Ljava/io/ByteArrayOutputStream;
    :catch_0
    move-exception v4

    .line 274
    .local v4, "ie":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 278
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "bos":Ljava/io/ByteArrayOutputStream;
    .end local v4    # "ie":Ljava/lang/Exception;
    :catch_1
    move-exception v3

    .line 279
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private registerNotiAlarm(Landroid/content/Context;JJ)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "notiTime"    # J
    .param p4, "nowTime"    # J

    .prologue
    const/16 v7, 0xc

    const/16 v6, 0xb

    .line 187
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 188
    .local v0, "dbCal":Ljava/util/Calendar;
    invoke-virtual {v0, p2, p3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 189
    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 190
    .local v1, "dbHour":I
    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 192
    .local v2, "dbMinute":I
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 193
    .local v3, "nowCal":Ljava/util/Calendar;
    invoke-virtual {v3, p4, p5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 194
    invoke-virtual {v3, v6}, Ljava/util/Calendar;->get(I)I

    move-result v4

    .line 195
    .local v4, "nowHour":I
    invoke-virtual {v3, v7}, Ljava/util/Calendar;->get(I)I

    move-result v5

    .line 203
    .local v5, "nowMinute":I
    if-lt v1, v4, :cond_0

    if-ne v1, v4, :cond_1

    if-gt v2, v5, :cond_1

    .line 204
    :cond_0
    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-static {p1, v1, v2, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setNotiTime(Landroid/content/Context;IILjava/lang/Boolean;)V

    .line 208
    :goto_0
    return-void

    .line 206
    :cond_1
    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-static {p1, v1, v2, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setNotiTime(Landroid/content/Context;IILjava/lang/Boolean;)V

    goto :goto_0
.end method

.method private saveData(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)V
    .locals 10
    .param p1, "cityInfo"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .param p2, "detailInfo"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .param p3, "xmlInfo"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 883
    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    .line 884
    const/4 v4, 0x1

    .line 885
    .local v4, "result":I
    const/4 v5, 0x0

    .line 887
    .local v5, "resultCode":I
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getRealLocation(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 891
    .local v2, "oldRealLocation":Ljava/lang/String;
    const-string v0, "cityId:current"

    .line 892
    .local v0, "curLocation":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v3

    .line 893
    .local v3, "realLocation":Ljava/lang/String;
    invoke-virtual {p1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setLocation(Ljava/lang/String;)V

    .line 894
    invoke-virtual {p1, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setRealLocation(Ljava/lang/String;)V

    .line 896
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    invoke-static {v6, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->isRegisteredToCityList(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 897
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    invoke-static {v6, v0, p1, p2, p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateCity(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)I

    move-result v4

    .line 901
    :goto_0
    invoke-direct {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->checkResultCode(I)I

    move-result v5

    .line 903
    invoke-virtual {p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-virtual {p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfoSize()I

    move-result v6

    if-lez v6, :cond_2

    .line 904
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_5

    .line 905
    invoke-virtual {p2, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;->getCityId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    .line 906
    invoke-static {v6, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->isRegisteredToCityList(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 907
    :cond_0
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    invoke-static {v6, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getExistPhotos(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 908
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    invoke-static {v6, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->deletePhotosInfo(Landroid/content/Context;Ljava/lang/String;)I

    .line 916
    :cond_1
    :goto_1
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    invoke-static {v6, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->insertPhotosInfo(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)I

    .line 919
    :cond_2
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    const-string v7, "cityId:current"

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->isRegisteredToDetailHourInfo(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 920
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    const-string v7, "cityId:current"

    invoke-static {v6, v7, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateDetailHourInfo(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)I

    .line 925
    :goto_2
    const/16 v6, 0x3e7

    if-ne v6, v5, :cond_7

    .line 926
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->getDataHandler:Landroid/os/Handler;

    new-instance v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$2;

    invoke-direct {v7, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;)V

    invoke-virtual {v6, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 944
    .end local v0    # "curLocation":Ljava/lang/String;
    .end local v2    # "oldRealLocation":Ljava/lang/String;
    .end local v3    # "realLocation":Ljava/lang/String;
    .end local v4    # "result":I
    .end local v5    # "resultCode":I
    :cond_3
    :goto_3
    return-void

    .line 899
    .restart local v0    # "curLocation":Ljava/lang/String;
    .restart local v2    # "oldRealLocation":Ljava/lang/String;
    .restart local v3    # "realLocation":Ljava/lang/String;
    .restart local v4    # "result":I
    .restart local v5    # "resultCode":I
    :cond_4
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    invoke-static {v6, p1, p2, p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->insertCity(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)I

    move-result v4

    goto :goto_0

    .line 911
    :cond_5
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    invoke-virtual {p2, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;->getCityId()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getExistPhotos(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 912
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    invoke-virtual {p2, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;->getCityId()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->deletePhotosInfo(Landroid/content/Context;Ljava/lang/String;)I

    goto :goto_1

    .line 922
    :cond_6
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    const-string v7, "cityId:current"

    invoke-static {v6, v7, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->insertDetailHourInfo(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)I

    goto :goto_2

    .line 934
    :cond_7
    const-string v6, ""

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "LC : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " -> "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 935
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    invoke-static {v6, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateLastSelectedLocation(Landroid/content/Context;Ljava/lang/String;)I

    .line 937
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/BManager;->isCheckSupportClock(Landroid/content/Context;)Z

    move-result v6

    if-ne v6, v9, :cond_3

    .line 938
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    const/4 v7, 0x1

    const-string v8, "NOTI"

    invoke-static {v6, v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/BManager;->sendBroadCastToProvider(Landroid/content/Context;ZLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 940
    :catch_0
    move-exception v1

    .line 941
    .local v1, "e":Ljava/lang/Exception;
    const-string v6, ""

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "db : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method

.method private setNotiAlarm(Landroid/content/Context;Z)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "on"    # Z

    .prologue
    .line 166
    invoke-static {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getNotificationSettings(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 167
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "sNA : "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    if-nez p2, :cond_0

    .line 169
    invoke-static {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->unregisterNotiTime(Landroid/content/Context;)V

    .line 178
    :goto_0
    return-void

    .line 171
    :cond_0
    invoke-static {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getNotificationTimeSettings(Landroid/content/Context;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 172
    .local v2, "setTime":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .local v4, "nowTime":J
    move-object v0, p0

    move-object v1, p1

    .line 173
    invoke-direct/range {v0 .. v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->registerNotiAlarm(Landroid/content/Context;JJ)V

    goto :goto_0

    .line 176
    .end local v2    # "setTime":J
    .end local v4    # "nowTime":J
    :cond_1
    const-string v0, ""

    const-string v1, "sNA -> disable"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public displayNotification()V
    .locals 24

    .prologue
    .line 285
    const-string v21, ""

    const-string v22, "=========== Noti"

    invoke-static/range {v21 .. v22}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getLocation(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCurLocation:Ljava/lang/String;

    .line 289
    const/4 v6, 0x0

    .line 292
    .local v6, "getNotiData":Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->getNotiMessageAndIcon()Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData;

    move-result-object v6

    .line 294
    if-nez v6, :cond_0

    .line 295
    const-string v21, ""

    const-string v22, "noti d is n"

    invoke-static/range {v21 .. v22}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    new-instance v9, Landroid/content/Intent;

    const-string v21, "com.sec.android.widgetapp.ap.hero.accuweather.action.GET_CURRENT_LOCATION_OK"

    move-object/from16 v0, v21

    invoke-direct {v9, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 298
    .local v9, "intent":Landroid/content/Intent;
    const-string v21, "AUTO_REFRESH_WHERE_FROM"

    const/16 v22, 0x2

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 299
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v9}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 394
    :goto_0
    return-void

    .line 304
    .end local v9    # "intent":Landroid/content/Intent;
    :cond_0
    new-instance v12, Landroid/support/v4/app/NotificationCompat$Builder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-direct {v12, v0}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    .line 305
    .local v12, "notiBuilder":Landroid/support/v4/app/NotificationCompat$Builder;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    move-object/from16 v21, v0

    const-string v22, "notification"

    .line 306
    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/app/NotificationManager;

    .line 308
    .local v13, "notiManager":Landroid/app/NotificationManager;
    new-instance v8, Landroid/support/v4/app/NotificationCompat$InboxStyle;

    invoke-direct {v8}, Landroid/support/v4/app/NotificationCompat$InboxStyle;-><init>()V

    .line 310
    .local v8, "inboxStyle":Landroid/support/v4/app/NotificationCompat$InboxStyle;
    const v21, 0x7f020087

    move/from16 v0, v21

    invoke-virtual {v12, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 311
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f0d0075

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 312
    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData;->getNotiMessage()[Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x0

    aget-object v21, v21, v22

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 314
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f0d0075

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/support/v4/app/NotificationCompat$InboxStyle;->setBigContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$InboxStyle;

    .line 316
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData;->getNotiMessage()[Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v7, v0, :cond_1

    .line 320
    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData;->getNotiMessage()[Ljava/lang/String;

    move-result-object v21

    aget-object v21, v21, v7

    move-object/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/support/v4/app/NotificationCompat$InboxStyle;->addLine(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$InboxStyle;

    .line 316
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 323
    :cond_1
    invoke-virtual {v12, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setStyle(Landroid/support/v4/app/NotificationCompat$Style;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 324
    new-instance v14, Ljava/lang/StringBuffer;

    invoke-direct {v14}, Ljava/lang/StringBuffer;-><init>()V

    .line 325
    .local v14, "notiTextForH":Ljava/lang/StringBuffer;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/BManager;->isCheckSupportClock(Landroid/content/Context;)Z

    move-result v21

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_3

    .line 326
    const-string v21, ""

    const-string v22, "=========== Noti check the H"

    invoke-static/range {v21 .. v22}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v15

    .line 330
    .local v15, "notiTimeForH":J
    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData;->getIconNum()I

    move-result v20

    .line 332
    .local v20, "weatherIconForH":I
    const/4 v7, 0x0

    :goto_2
    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData;->getNotiMessage()[Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v7, v0, :cond_2

    .line 333
    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData;->getNotiMessage()[Ljava/lang/String;

    move-result-object v21

    aget-object v21, v21, v7

    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 334
    const-string v21, ", "

    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 332
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 337
    :cond_2
    const v21, 0x7f020086

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->getPhotoToString(I)Ljava/lang/String;

    move-result-object v19

    .line 339
    .local v19, "photo":Ljava/lang/String;
    new-instance v10, Landroid/content/Intent;

    const-string v21, "com.samsung.accessory.intent.action.ALERT_NOTIFICATION_ITEM"

    move-object/from16 v0, v21

    invoke-direct {v10, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 341
    .local v10, "intentForH":Landroid/content/Intent;
    const-string v21, "NOTIFICATION_PACKAGE_NAME"

    const-string v22, "com.sec.android.widgetapp.ap.hero.accuweather"

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 343
    const-string v21, "NOTIFICATION_TIME"

    move-object/from16 v0, v21

    move-wide v1, v15

    invoke-virtual {v10, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 344
    const-string v21, "NOTIFICATION_MAIN_TEXT"

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 345
    const-string v21, "NOTIFICATION_LAUNCH_INTENT"

    const-string v22, "com.samsung.sec.android.widgetapp.intent.action.MENU_DETAIL_GL"

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 347
    const-string v21, "NOTIFICATION_LAUNCH_TOACC_INTENT"

    const-string v22, "com.samsung.sec.android.widgetapp.intent.action.MENU_DETAIL"

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 349
    const-string v21, "NOTIFICATION_ID"

    const/16 v22, 0x1

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 351
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->checkGearMode(Landroid/content/Context;)Z

    move-result v21

    if-eqz v21, :cond_5

    .line 352
    const-string v21, "NOTIFICATION_VERSION"

    const/16 v22, 0x3

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 353
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->getBitmapByPackageName(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 354
    .local v4, "bitmap":Landroid/graphics/Bitmap;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->convertResizeBitmapToByte(Landroid/graphics/Bitmap;)[B

    move-result-object v5

    .line 355
    .local v5, "byteArray":[B
    const-string v21, "NOTIFICATION_APP_ICON"

    move-object/from16 v0, v21

    invoke-virtual {v10, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 360
    .end local v4    # "bitmap":Landroid/graphics/Bitmap;
    .end local v5    # "byteArray":[B
    :goto_3
    const-string v21, ""

    const-string v22, "==========================================="

    invoke-static/range {v21 .. v22}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    const-string v21, ""

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Time = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-wide v1, v15

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 362
    const-string v21, ""

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Icon = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    const-string v21, ""

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "GearMode = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->checkGearMode(Landroid/content/Context;)Z

    move-result v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    const-string v21, ""

    const-string v22, "==========================================="

    invoke-static/range {v21 .. v22}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    move-object/from16 v21, v0

    const-string v22, "com.samsung.accessory.permission.TRANSPORTING_NOTIFICATION_ITEM"

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v0, v10, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 370
    .end local v10    # "intentForH":Landroid/content/Intent;
    .end local v15    # "notiTimeForH":J
    .end local v19    # "photo":Ljava/lang/String;
    .end local v20    # "weatherIconForH":I
    :cond_3
    new-instance v17, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    move-object/from16 v21, v0

    const-class v22, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 371
    .local v17, "notificationIntent":Landroid/content/Intent;
    const-string v21, "searchlocation"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCurLocation:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 372
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/BManager;->isCheckSupportClock(Landroid/content/Context;)Z

    move-result v21

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_4

    .line 373
    const-string v21, "NOTIFICATION_CHECK"

    const/16 v22, 0x1

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 377
    :cond_4
    const-string v21, "where_from_to_details"

    const/16 v22, 0x102

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 379
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const/high16 v23, 0x8000000

    move-object/from16 v0, v21

    move/from16 v1, v22

    move-object/from16 v2, v17

    move/from16 v3, v23

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v18

    .line 382
    .local v18, "pendingIntent":Landroid/app/PendingIntent;
    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 384
    invoke-virtual {v12}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v11

    .line 385
    .local v11, "noti":Landroid/app/Notification;
    iget v0, v11, Landroid/app/Notification;->flags:I

    move/from16 v21, v0

    or-int/lit8 v21, v21, 0x10

    move/from16 v0, v21

    iput v0, v11, Landroid/app/Notification;->flags:I

    .line 386
    iget v0, v11, Landroid/app/Notification;->defaults:I

    move/from16 v21, v0

    or-int/lit8 v21, v21, 0x1

    move/from16 v0, v21

    iput v0, v11, Landroid/app/Notification;->defaults:I

    .line 388
    const/16 v21, 0x1

    move/from16 v0, v21

    invoke-virtual {v13, v0}, Landroid/app/NotificationManager;->cancel(I)V

    .line 389
    const/16 v21, 0x1

    move/from16 v0, v21

    invoke-virtual {v13, v0, v11}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 391
    new-instance v9, Landroid/content/Intent;

    const-string v21, "com.sec.android.widgetapp.ap.hero.accuweather.action.GET_CURRENT_LOCATION_OK"

    move-object/from16 v0, v21

    invoke-direct {v9, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 392
    .restart local v9    # "intent":Landroid/content/Intent;
    const-string v21, "AUTO_REFRESH_WHERE_FROM"

    const/16 v22, 0x2

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 393
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v9}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 357
    .end local v9    # "intent":Landroid/content/Intent;
    .end local v11    # "noti":Landroid/app/Notification;
    .end local v17    # "notificationIntent":Landroid/content/Intent;
    .end local v18    # "pendingIntent":Landroid/app/PendingIntent;
    .restart local v10    # "intentForH":Landroid/content/Intent;
    .restart local v15    # "notiTimeForH":J
    .restart local v19    # "photo":Ljava/lang/String;
    .restart local v20    # "weatherIconForH":I
    :cond_5
    const-string v21, "NOTIFICATION_APP_ICON"

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_3
.end method

.method public getNotiMessageAndIcon()Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData;
    .locals 49

    .prologue
    .line 404
    const/4 v12, 0x0

    .line 405
    .local v12, "focastIndex":I
    new-instance v35, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData;

    invoke-direct/range {v35 .. v35}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData;-><init>()V

    .line 406
    .local v35, "notiData":Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData;
    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    .line 407
    .local v42, "strBuilder":Ljava/lang/StringBuilder;
    const/4 v15, 0x0

    .line 409
    .local v15, "isNeedNotification":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    move-object/from16 v45, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCurLocation:Ljava/lang/String;

    move-object/from16 v46, v0

    invoke-static/range {v45 .. v46}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getDetailWeatherInfo(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    move-result-object v13

    .line 410
    .local v13, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    if-eqz v13, :cond_0

    .line 411
    const/16 v43, 0x0

    .line 412
    .local v43, "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v43

    .line 413
    const/16 v41, 0x0

    .line 414
    .local v41, "oneday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    const/16 v45, 0x0

    move/from16 v0, v45

    invoke-virtual {v13, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v41

    .line 415
    if-eqz v43, :cond_0

    .line 417
    invoke-virtual/range {v43 .. v43}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getIconNum()I

    move-result v45

    invoke-static/range {v45 .. v45}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkTodayWeatherIcon(I)Z

    move-result v45

    if-nez v45, :cond_1

    .line 418
    const-string v45, ""

    const-string v46, "noti c t I"

    invoke-static/range {v45 .. v46}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    const/4 v15, 0x0

    .line 694
    .end local v41    # "oneday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .end local v43    # "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    :cond_0
    :goto_0
    if-eqz v15, :cond_20

    .line 695
    const-string v45, ""

    const-string v46, "noti get s sUcc."

    invoke-static/range {v45 .. v46}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v45, v35

    .line 701
    :goto_1
    return-object v45

    .line 421
    .restart local v41    # "oneday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .restart local v43    # "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    :cond_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v40

    .line 422
    .local v40, "nowCal":Ljava/util/Calendar;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v45

    move-object/from16 v0, v40

    move-wide/from16 v1, v45

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 424
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    move-object/from16 v45, v0

    invoke-static/range {v45 .. v45}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getNotificationTimeSettings(Landroid/content/Context;)Ljava/lang/Long;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/Long;->longValue()J

    move-result-wide v37

    .line 425
    .local v37, "notiTime":J
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v39

    .line 426
    .local v39, "notiTimeCal":Ljava/util/Calendar;
    move-object/from16 v0, v39

    move-wide/from16 v1, v37

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 427
    const/16 v45, 0xb

    move-object/from16 v0, v39

    move/from16 v1, v45

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v36

    .line 429
    .local v36, "notiHour":I
    const/16 v8, 0x13

    .line 431
    .local v8, "baseTime":I
    const/16 v45, 0x13

    move/from16 v0, v36

    move/from16 v1, v45

    if-ge v0, v1, :cond_12

    const/4 v14, 0x1

    .line 433
    .local v14, "isBefore19Hour":Z
    :goto_2
    if-nez v14, :cond_2

    .line 434
    const/16 v45, 0x5

    const/16 v46, 0x1

    move-object/from16 v0, v40

    move/from16 v1, v45

    move/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 435
    const-string v45, ""

    const-string v46, "noti b h"

    invoke-static/range {v45 .. v46}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    :cond_2
    const/16 v45, 0x1

    move-object/from16 v0, v40

    move/from16 v1, v45

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v9

    .line 439
    .local v9, "baseYear":I
    const/16 v45, 0x2

    move-object/from16 v0, v40

    move/from16 v1, v45

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v45

    add-int/lit8 v6, v45, 0x1

    .line 440
    .local v6, "baseMonth":I
    const/16 v45, 0x5

    move-object/from16 v0, v40

    move/from16 v1, v45

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v4

    .line 442
    .local v4, "baseDay":I
    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    .line 443
    .local v10, "baseYearStr":Ljava/lang/String;
    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    .line 444
    .local v7, "baseMonthStr":Ljava/lang/String;
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    .line 446
    .local v5, "baseDayStr":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v45

    const/16 v46, 0x2

    move/from16 v0, v45

    move/from16 v1, v46

    if-ge v0, v1, :cond_3

    .line 447
    new-instance v45, Ljava/lang/StringBuilder;

    invoke-direct/range {v45 .. v45}, Ljava/lang/StringBuilder;-><init>()V

    const-string v46, "0"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    move-object/from16 v0, v45

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 450
    :cond_3
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v45

    const/16 v46, 0x2

    move/from16 v0, v45

    move/from16 v1, v46

    if-ge v0, v1, :cond_4

    .line 451
    new-instance v45, Ljava/lang/StringBuilder;

    invoke-direct/range {v45 .. v45}, Ljava/lang/StringBuilder;-><init>()V

    const-string v46, "0"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    move-object/from16 v0, v45

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 455
    :cond_4
    const/16 v45, 0x5

    const/16 v46, 0x1

    move-object/from16 v0, v40

    move/from16 v1, v45

    move/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 456
    const/16 v45, 0x1

    move-object/from16 v0, v40

    move/from16 v1, v45

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v32

    .line 457
    .local v32, "nextYear":I
    const/16 v45, 0x2

    move-object/from16 v0, v40

    move/from16 v1, v45

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v45

    add-int/lit8 v30, v45, 0x1

    .line 458
    .local v30, "nextMonth":I
    const/16 v45, 0x5

    move-object/from16 v0, v40

    move/from16 v1, v45

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v28

    .line 460
    .local v28, "nextDay":I
    invoke-static/range {v32 .. v32}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v33

    .line 461
    .local v33, "nextYearStr":Ljava/lang/String;
    invoke-static/range {v30 .. v30}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v31

    .line 462
    .local v31, "nextMonthStr":Ljava/lang/String;
    invoke-static/range {v28 .. v28}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v29

    .line 464
    .local v29, "nextDayStr":Ljava/lang/String;
    invoke-virtual/range {v31 .. v31}, Ljava/lang/String;->length()I

    move-result v45

    const/16 v46, 0x2

    move/from16 v0, v45

    move/from16 v1, v46

    if-ge v0, v1, :cond_5

    .line 465
    new-instance v45, Ljava/lang/StringBuilder;

    invoke-direct/range {v45 .. v45}, Ljava/lang/StringBuilder;-><init>()V

    const-string v46, "0"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    move-object/from16 v0, v45

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    .line 468
    :cond_5
    invoke-virtual/range {v29 .. v29}, Ljava/lang/String;->length()I

    move-result v45

    const/16 v46, 0x2

    move/from16 v0, v45

    move/from16 v1, v46

    if-ge v0, v1, :cond_6

    .line 469
    new-instance v45, Ljava/lang/StringBuilder;

    invoke-direct/range {v45 .. v45}, Ljava/lang/StringBuilder;-><init>()V

    const-string v46, "0"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    move-object/from16 v0, v45

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    .line 471
    :cond_6
    new-instance v45, Ljava/lang/StringBuilder;

    invoke-direct/range {v45 .. v45}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v45

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    move-object/from16 v0, v45

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    move-object/from16 v0, v45

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v45 .. v45}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v44

    .line 475
    .local v44, "yearMonthNextDayNext":I
    move-object/from16 v0, v42

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 476
    const-string v45, "."

    move-object/from16 v0, v42

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 477
    move-object/from16 v0, v42

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 478
    const-string v45, "."

    move-object/from16 v0, v42

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 479
    move-object/from16 v0, v42

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 480
    const-string v45, " ("

    move-object/from16 v0, v42

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 482
    if-eqz v14, :cond_13

    .line 483
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    move-object/from16 v45, v0

    invoke-virtual/range {v45 .. v45}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v45

    const v46, 0x7f0d00c7

    invoke-virtual/range {v45 .. v46}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v45

    move-object/from16 v0, v42

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 487
    :goto_3
    const-string v45, ")"

    move-object/from16 v0, v42

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 488
    sget-object v45, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;->LINE_DATE:Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v46

    move-object/from16 v0, v35

    move-object/from16 v1, v45

    move-object/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData;->setNotiMessage(Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;Ljava/lang/String;)V

    .line 489
    const/16 v45, 0x0

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->length()I

    move-result v46

    move-object/from16 v0, v42

    move/from16 v1, v45

    move/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 491
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    move-object/from16 v45, v0

    invoke-virtual/range {v45 .. v45}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v45

    const v46, 0x7f0d00c9

    .line 492
    invoke-virtual/range {v45 .. v46}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 493
    .local v20, "mDayString":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    move-object/from16 v45, v0

    invoke-virtual/range {v45 .. v45}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v45

    const v46, 0x7f0d00ca

    invoke-virtual/range {v45 .. v46}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    .line 496
    .local v26, "mNightString":Ljava/lang/String;
    const-string v45, ""

    new-instance v46, Ljava/lang/StringBuilder;

    invoke-direct/range {v46 .. v46}, Ljava/lang/StringBuilder;-><init>()V

    const-string v47, "isBefore19Hour = "

    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v46

    move-object/from16 v0, v46

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v46

    invoke-virtual/range {v46 .. v46}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v46

    invoke-static/range {v45 .. v46}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 498
    if-eqz v14, :cond_14

    .line 500
    invoke-virtual/range {v43 .. v43}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getIconNum()I

    move-result v45

    move-object/from16 v0, v35

    move/from16 v1, v45

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData;->setIconNum(I)V

    .line 503
    const-string v45, ""

    const-string v46, "noti Nd."

    invoke-static/range {v45 .. v46}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 504
    const/16 v21, 0x0

    .line 505
    .local v21, "mDaycheckValue":I
    const v19, 0x7f0d00af

    .line 506
    .local v19, "mDayStrIndex":I
    const-wide/16 v45, 0x0

    invoke-static/range {v45 .. v46}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v16

    .line 507
    .local v16, "mDayAmount":Ljava/lang/Double;
    const/16 v18, 0x0

    .line 508
    .local v18, "mDayProbability":I
    const/16 v17, 0x0

    .line 510
    .local v17, "mDayNotiString":Ljava/lang/String;
    const/16 v27, 0x0

    .line 511
    .local v27, "mNightcheckValue":I
    const v25, 0x7f0d00af

    .line 512
    .local v25, "mNightStrIndex":I
    const-wide/16 v45, 0x0

    invoke-static/range {v45 .. v46}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v22

    .line 513
    .local v22, "mNightAmount":Ljava/lang/Double;
    const/16 v24, 0x0

    .line 514
    .local v24, "mNightProbability":I
    const/16 v23, 0x0

    .line 516
    .local v23, "mNightNotiString":Ljava/lang/String;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 517
    .local v11, "dayStringBuilder":Ljava/lang/StringBuilder;
    new-instance v34, Ljava/lang/StringBuilder;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuilder;-><init>()V

    .line 519
    .local v34, "nightStringBuilder":Ljava/lang/StringBuilder;
    move-object/from16 v0, v20

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 520
    const-string v45, " : "

    move-object/from16 v0, v45

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 522
    invoke-virtual/range {v43 .. v43}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayRainProbability()I

    move-result v45

    if-eqz v45, :cond_7

    .line 523
    const v19, 0x7f0d00ac

    .line 524
    invoke-virtual/range {v43 .. v43}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayRainAmount()Ljava/lang/Double;

    move-result-object v16

    .line 525
    invoke-virtual/range {v43 .. v43}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayRainProbability()I

    move-result v18

    .line 526
    add-int/lit8 v21, v21, 0x1

    .line 528
    :cond_7
    invoke-virtual/range {v43 .. v43}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDaySnowProbability()I

    move-result v45

    if-eqz v45, :cond_8

    .line 529
    const v19, 0x7f0d00ad

    .line 530
    invoke-virtual/range {v43 .. v43}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDaySnowAmount()Ljava/lang/Double;

    move-result-object v16

    .line 531
    invoke-virtual/range {v43 .. v43}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDaySnowProbability()I

    move-result v18

    .line 532
    add-int/lit8 v21, v21, 0x1

    .line 534
    :cond_8
    invoke-virtual/range {v43 .. v43}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayHailProbability()I

    move-result v45

    if-eqz v45, :cond_9

    .line 535
    const v19, 0x7f0d00ae

    .line 536
    invoke-virtual/range {v43 .. v43}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayHailAmount()Ljava/lang/Double;

    move-result-object v16

    .line 537
    invoke-virtual/range {v43 .. v43}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayHailProbability()I

    move-result v18

    .line 538
    add-int/lit8 v21, v21, 0x1

    .line 541
    :cond_9
    const/16 v45, 0x1

    move/from16 v0, v21

    move/from16 v1, v45

    if-gt v0, v1, :cond_a

    if-nez v21, :cond_b

    .line 542
    invoke-virtual/range {v43 .. v43}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayPrecipitationProbability()I

    move-result v45

    if-eqz v45, :cond_b

    .line 543
    :cond_a
    const v19, 0x7f0d00af

    .line 544
    invoke-virtual/range {v43 .. v43}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayPrecipitationAmount()Ljava/lang/Double;

    move-result-object v16

    .line 545
    invoke-virtual/range {v43 .. v43}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayPrecipitationProbability()I

    move-result v18

    .line 548
    :cond_b
    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v18

    move-object/from16 v3, v16

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->makeNotiString(IILjava/lang/Double;)Ljava/lang/String;

    move-result-object v17

    .line 549
    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 550
    sget-object v45, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;->LINE_DAY:Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;

    .line 551
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v46

    .line 550
    move-object/from16 v0, v35

    move-object/from16 v1, v45

    move-object/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData;->setNotiMessage(Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;Ljava/lang/String;)V

    .line 553
    move-object/from16 v0, v34

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 554
    const-string v45, " : "

    move-object/from16 v0, v34

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 556
    invoke-virtual/range {v43 .. v43}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightRainProbability()I

    move-result v45

    if-eqz v45, :cond_c

    .line 557
    const v25, 0x7f0d00ac

    .line 558
    invoke-virtual/range {v43 .. v43}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightRainAmount()Ljava/lang/Double;

    move-result-object v22

    .line 559
    invoke-virtual/range {v43 .. v43}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightRainProbability()I

    move-result v24

    .line 560
    add-int/lit8 v27, v27, 0x1

    .line 562
    :cond_c
    invoke-virtual/range {v43 .. v43}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightSnowProbability()I

    move-result v45

    if-eqz v45, :cond_d

    .line 563
    const v25, 0x7f0d00ad

    .line 564
    invoke-virtual/range {v43 .. v43}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightSnowAmount()Ljava/lang/Double;

    move-result-object v22

    .line 565
    invoke-virtual/range {v43 .. v43}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightSnowProbability()I

    move-result v24

    .line 566
    add-int/lit8 v27, v27, 0x1

    .line 568
    :cond_d
    invoke-virtual/range {v43 .. v43}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightHailProbability()I

    move-result v45

    if-eqz v45, :cond_e

    .line 569
    const v25, 0x7f0d00ae

    .line 570
    invoke-virtual/range {v43 .. v43}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightHailAmount()Ljava/lang/Double;

    move-result-object v22

    .line 571
    invoke-virtual/range {v43 .. v43}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightHailProbability()I

    move-result v24

    .line 572
    add-int/lit8 v27, v27, 0x1

    .line 575
    :cond_e
    const/16 v45, 0x1

    move/from16 v0, v27

    move/from16 v1, v45

    if-gt v0, v1, :cond_f

    if-nez v27, :cond_10

    .line 577
    invoke-virtual/range {v43 .. v43}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightPrecipitationProbability()I

    move-result v45

    if-eqz v45, :cond_10

    .line 578
    :cond_f
    const v25, 0x7f0d00af

    .line 579
    invoke-virtual/range {v43 .. v43}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightPrecipitationAmount()Ljava/lang/Double;

    move-result-object v22

    .line 580
    invoke-virtual/range {v43 .. v43}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightPrecipitationProbability()I

    move-result v24

    .line 583
    :cond_10
    move-object/from16 v0, p0

    move/from16 v1, v25

    move/from16 v2, v24

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->makeNotiString(IILjava/lang/Double;)Ljava/lang/String;

    move-result-object v23

    .line 585
    move-object/from16 v0, v34

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 586
    sget-object v45, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;->LINE_NIGHT:Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;

    .line 587
    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v46

    .line 586
    move-object/from16 v0, v35

    move-object/from16 v1, v45

    move-object/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData;->setNotiMessage(Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;Ljava/lang/String;)V

    .line 589
    if-nez v18, :cond_11

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v45

    const-wide/16 v47, 0x0

    cmpl-double v45, v45, v47

    if-nez v45, :cond_11

    if-nez v24, :cond_11

    .line 590
    invoke-virtual/range {v22 .. v22}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v45

    const-wide/16 v47, 0x0

    cmpl-double v45, v45, v47

    if-eqz v45, :cond_0

    .line 591
    :cond_11
    const/4 v15, 0x1

    goto/16 :goto_0

    .line 431
    .end local v4    # "baseDay":I
    .end local v5    # "baseDayStr":Ljava/lang/String;
    .end local v6    # "baseMonth":I
    .end local v7    # "baseMonthStr":Ljava/lang/String;
    .end local v9    # "baseYear":I
    .end local v10    # "baseYearStr":Ljava/lang/String;
    .end local v11    # "dayStringBuilder":Ljava/lang/StringBuilder;
    .end local v14    # "isBefore19Hour":Z
    .end local v16    # "mDayAmount":Ljava/lang/Double;
    .end local v17    # "mDayNotiString":Ljava/lang/String;
    .end local v18    # "mDayProbability":I
    .end local v19    # "mDayStrIndex":I
    .end local v20    # "mDayString":Ljava/lang/String;
    .end local v21    # "mDaycheckValue":I
    .end local v22    # "mNightAmount":Ljava/lang/Double;
    .end local v23    # "mNightNotiString":Ljava/lang/String;
    .end local v24    # "mNightProbability":I
    .end local v25    # "mNightStrIndex":I
    .end local v26    # "mNightString":Ljava/lang/String;
    .end local v27    # "mNightcheckValue":I
    .end local v28    # "nextDay":I
    .end local v29    # "nextDayStr":Ljava/lang/String;
    .end local v30    # "nextMonth":I
    .end local v31    # "nextMonthStr":Ljava/lang/String;
    .end local v32    # "nextYear":I
    .end local v33    # "nextYearStr":Ljava/lang/String;
    .end local v34    # "nightStringBuilder":Ljava/lang/StringBuilder;
    .end local v44    # "yearMonthNextDayNext":I
    :cond_12
    const/4 v14, 0x0

    goto/16 :goto_2

    .line 485
    .restart local v4    # "baseDay":I
    .restart local v5    # "baseDayStr":Ljava/lang/String;
    .restart local v6    # "baseMonth":I
    .restart local v7    # "baseMonthStr":Ljava/lang/String;
    .restart local v9    # "baseYear":I
    .restart local v10    # "baseYearStr":Ljava/lang/String;
    .restart local v14    # "isBefore19Hour":Z
    .restart local v28    # "nextDay":I
    .restart local v29    # "nextDayStr":Ljava/lang/String;
    .restart local v30    # "nextMonth":I
    .restart local v31    # "nextMonthStr":Ljava/lang/String;
    .restart local v32    # "nextYear":I
    .restart local v33    # "nextYearStr":Ljava/lang/String;
    .restart local v44    # "yearMonthNextDayNext":I
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    move-object/from16 v45, v0

    invoke-virtual/range {v45 .. v45}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v45

    const v46, 0x7f0d00c8

    invoke-virtual/range {v45 .. v46}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v45

    move-object/from16 v0, v42

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 597
    .restart local v20    # "mDayString":Ljava/lang/String;
    .restart local v26    # "mNightString":Ljava/lang/String;
    :cond_14
    invoke-virtual/range {v41 .. v41}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v45

    move-object/from16 v0, v35

    move/from16 v1, v45

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData;->setIconNum(I)V

    .line 599
    const-string v45, ""

    const-string v46, "noti Od."

    invoke-static/range {v45 .. v46}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 600
    const/16 v21, 0x0

    .line 601
    .restart local v21    # "mDaycheckValue":I
    const v19, 0x7f0d00af

    .line 602
    .restart local v19    # "mDayStrIndex":I
    const-wide/16 v45, 0x0

    invoke-static/range {v45 .. v46}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v16

    .line 603
    .restart local v16    # "mDayAmount":Ljava/lang/Double;
    const/16 v18, 0x0

    .line 604
    .restart local v18    # "mDayProbability":I
    const/16 v17, 0x0

    .line 606
    .restart local v17    # "mDayNotiString":Ljava/lang/String;
    const/16 v27, 0x0

    .line 607
    .restart local v27    # "mNightcheckValue":I
    const v25, 0x7f0d00af

    .line 608
    .restart local v25    # "mNightStrIndex":I
    const-wide/16 v45, 0x0

    invoke-static/range {v45 .. v46}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v22

    .line 609
    .restart local v22    # "mNightAmount":Ljava/lang/Double;
    const/16 v24, 0x0

    .line 610
    .restart local v24    # "mNightProbability":I
    const/16 v23, 0x0

    .line 612
    .restart local v23    # "mNightNotiString":Ljava/lang/String;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 613
    .restart local v11    # "dayStringBuilder":Ljava/lang/StringBuilder;
    new-instance v34, Ljava/lang/StringBuilder;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuilder;-><init>()V

    .line 615
    .restart local v34    # "nightStringBuilder":Ljava/lang/StringBuilder;
    move-object/from16 v0, v20

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 616
    const-string v45, " : "

    move-object/from16 v0, v45

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 618
    invoke-virtual/range {v41 .. v41}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayRainProbability()I

    move-result v45

    if-eqz v45, :cond_15

    .line 619
    const v19, 0x7f0d00ac

    .line 620
    invoke-virtual/range {v41 .. v41}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayRainAmount()Ljava/lang/Double;

    move-result-object v16

    .line 621
    invoke-virtual/range {v41 .. v41}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayRainProbability()I

    move-result v18

    .line 622
    add-int/lit8 v21, v21, 0x1

    .line 624
    :cond_15
    invoke-virtual/range {v41 .. v41}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDaySnowProbability()I

    move-result v45

    if-eqz v45, :cond_16

    .line 625
    const v19, 0x7f0d00ad

    .line 626
    invoke-virtual/range {v41 .. v41}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDaySnowAmount()Ljava/lang/Double;

    move-result-object v16

    .line 627
    invoke-virtual/range {v41 .. v41}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDaySnowProbability()I

    move-result v18

    .line 628
    add-int/lit8 v21, v21, 0x1

    .line 630
    :cond_16
    invoke-virtual/range {v41 .. v41}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayHailProbability()I

    move-result v45

    if-eqz v45, :cond_17

    .line 631
    const v19, 0x7f0d00ae

    .line 632
    invoke-virtual/range {v41 .. v41}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayHailAmount()Ljava/lang/Double;

    move-result-object v16

    .line 633
    invoke-virtual/range {v41 .. v41}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayHailProbability()I

    move-result v18

    .line 634
    add-int/lit8 v21, v21, 0x1

    .line 637
    :cond_17
    const/16 v45, 0x1

    move/from16 v0, v21

    move/from16 v1, v45

    if-gt v0, v1, :cond_18

    if-nez v21, :cond_19

    .line 638
    invoke-virtual/range {v41 .. v41}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationProbability()I

    move-result v45

    if-eqz v45, :cond_19

    .line 639
    :cond_18
    const v19, 0x7f0d00af

    .line 640
    invoke-virtual/range {v41 .. v41}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationAmount()Ljava/lang/Double;

    move-result-object v16

    .line 641
    invoke-virtual/range {v41 .. v41}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationProbability()I

    move-result v18

    .line 644
    :cond_19
    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v18

    move-object/from16 v3, v16

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->makeNotiString(IILjava/lang/Double;)Ljava/lang/String;

    move-result-object v17

    .line 645
    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 646
    sget-object v45, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;->LINE_DAY:Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;

    .line 647
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v46

    .line 646
    move-object/from16 v0, v35

    move-object/from16 v1, v45

    move-object/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData;->setNotiMessage(Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;Ljava/lang/String;)V

    .line 649
    move-object/from16 v0, v34

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 650
    const-string v45, " : "

    move-object/from16 v0, v34

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 652
    invoke-virtual/range {v41 .. v41}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightRainProbability()I

    move-result v45

    if-eqz v45, :cond_1a

    .line 653
    const v25, 0x7f0d00ac

    .line 654
    invoke-virtual/range {v41 .. v41}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightRainAmount()Ljava/lang/Double;

    move-result-object v22

    .line 655
    invoke-virtual/range {v41 .. v41}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightRainProbability()I

    move-result v24

    .line 656
    add-int/lit8 v27, v27, 0x1

    .line 658
    :cond_1a
    invoke-virtual/range {v41 .. v41}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightSnowProbability()I

    move-result v45

    if-eqz v45, :cond_1b

    .line 659
    const v25, 0x7f0d00ad

    .line 660
    invoke-virtual/range {v41 .. v41}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightSnowAmount()Ljava/lang/Double;

    move-result-object v22

    .line 661
    invoke-virtual/range {v41 .. v41}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightSnowProbability()I

    move-result v24

    .line 662
    add-int/lit8 v27, v27, 0x1

    .line 664
    :cond_1b
    invoke-virtual/range {v41 .. v41}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightHailProbability()I

    move-result v45

    if-eqz v45, :cond_1c

    .line 665
    const v25, 0x7f0d00ae

    .line 666
    invoke-virtual/range {v41 .. v41}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightHailAmount()Ljava/lang/Double;

    move-result-object v22

    .line 667
    invoke-virtual/range {v41 .. v41}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightHailProbability()I

    move-result v24

    .line 668
    add-int/lit8 v27, v27, 0x1

    .line 671
    :cond_1c
    const/16 v45, 0x1

    move/from16 v0, v27

    move/from16 v1, v45

    if-gt v0, v1, :cond_1d

    if-nez v21, :cond_1e

    .line 673
    invoke-virtual/range {v41 .. v41}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightPrecipitationProbability()I

    move-result v45

    if-eqz v45, :cond_1e

    .line 674
    :cond_1d
    const v25, 0x7f0d00af

    .line 675
    invoke-virtual/range {v41 .. v41}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightPrecipitationAmount()Ljava/lang/Double;

    move-result-object v22

    .line 676
    invoke-virtual/range {v41 .. v41}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightPrecipitationProbability()I

    move-result v24

    .line 679
    :cond_1e
    move-object/from16 v0, p0

    move/from16 v1, v25

    move/from16 v2, v24

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->makeNotiString(IILjava/lang/Double;)Ljava/lang/String;

    move-result-object v23

    .line 681
    move-object/from16 v0, v34

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 682
    sget-object v45, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;->LINE_NIGHT:Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;

    .line 683
    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v46

    .line 682
    move-object/from16 v0, v35

    move-object/from16 v1, v45

    move-object/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData;->setNotiMessage(Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver$NotiData$GetLineNum;Ljava/lang/String;)V

    .line 685
    if-nez v18, :cond_1f

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v45

    const-wide/16 v47, 0x0

    cmpl-double v45, v45, v47

    if-nez v45, :cond_1f

    if-nez v24, :cond_1f

    .line 686
    invoke-virtual/range {v22 .. v22}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v45

    const-wide/16 v47, 0x0

    cmpl-double v45, v45, v47

    if-eqz v45, :cond_0

    .line 687
    :cond_1f
    const/4 v15, 0x1

    goto/16 :goto_0

    .line 699
    .end local v4    # "baseDay":I
    .end local v5    # "baseDayStr":Ljava/lang/String;
    .end local v6    # "baseMonth":I
    .end local v7    # "baseMonthStr":Ljava/lang/String;
    .end local v8    # "baseTime":I
    .end local v9    # "baseYear":I
    .end local v10    # "baseYearStr":Ljava/lang/String;
    .end local v11    # "dayStringBuilder":Ljava/lang/StringBuilder;
    .end local v14    # "isBefore19Hour":Z
    .end local v16    # "mDayAmount":Ljava/lang/Double;
    .end local v17    # "mDayNotiString":Ljava/lang/String;
    .end local v18    # "mDayProbability":I
    .end local v19    # "mDayStrIndex":I
    .end local v20    # "mDayString":Ljava/lang/String;
    .end local v21    # "mDaycheckValue":I
    .end local v22    # "mNightAmount":Ljava/lang/Double;
    .end local v23    # "mNightNotiString":Ljava/lang/String;
    .end local v24    # "mNightProbability":I
    .end local v25    # "mNightStrIndex":I
    .end local v26    # "mNightString":Ljava/lang/String;
    .end local v27    # "mNightcheckValue":I
    .end local v28    # "nextDay":I
    .end local v29    # "nextDayStr":Ljava/lang/String;
    .end local v30    # "nextMonth":I
    .end local v31    # "nextMonthStr":Ljava/lang/String;
    .end local v32    # "nextYear":I
    .end local v33    # "nextYearStr":Ljava/lang/String;
    .end local v34    # "nightStringBuilder":Ljava/lang/StringBuilder;
    .end local v36    # "notiHour":I
    .end local v37    # "notiTime":J
    .end local v39    # "notiTimeCal":Ljava/util/Calendar;
    .end local v40    # "nowCal":Ljava/util/Calendar;
    .end local v41    # "oneday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .end local v43    # "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    .end local v44    # "yearMonthNextDayNext":I
    :cond_20
    const-string v45, ""

    const-string v46, "noti get s fAil."

    invoke-static/range {v45 .. v46}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 700
    const/16 v35, 0x0

    .line 701
    const/16 v45, 0x0

    goto/16 :goto_1
.end method

.method public makeNotiString(IILjava/lang/Double;)Ljava/lang/String;
    .locals 7
    .param p1, "stringIndex"    # I
    .param p2, "value"    # I
    .param p3, "amount"    # Ljava/lang/Double;

    .prologue
    .line 397
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d00b0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 398
    .local v0, "mAmoString":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 399
    .local v2, "mProString":Ljava/lang/String;
    const-string v3, "%s%s%d%s  %s%s%s %.1f%s"

    const/16 v4, 0x9

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    const/4 v5, 0x1

    const-string v6, ": "

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "%"

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, " ("

    aput-object v6, v4, v5

    const/4 v5, 0x5

    aput-object v0, v4, v5

    const/4 v5, 0x6

    const-string v6, ":"

    aput-object v6, v4, v5

    const/4 v5, 0x7

    aput-object p3, v4, v5

    const/16 v5, 0x8

    const-string v6, ")"

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 400
    .local v1, "mNotiString":Ljava/lang/String;
    return-object v1
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 17
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 53
    const/4 v10, 0x0

    .line 54
    .local v10, "easyMode":Z
    :try_start_0
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    .line 55
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    .line 56
    .local v8, "action":Ljava/lang/String;
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "NotiReciiver action:"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    if-eqz v8, :cond_2

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.android.launcher.action.EASY_MODE_CHANGE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 58
    const-string v2, "easymode"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getExtra(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    .line 59
    const-string v2, ""

    const-string v3, " Noti eMC"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    invoke-static/range {p1 .. p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getNotificationSettings(Landroid/content/Context;)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 80
    const/4 v12, 0x1

    .line 81
    .local v12, "isOn":Z
    if-eqz v10, :cond_0

    .line 82
    const/4 v12, 0x0

    .line 84
    :cond_0
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->setNotiAlarm(Landroid/content/Context;Z)V

    .line 158
    .end local v8    # "action":Ljava/lang/String;
    .end local v12    # "isOn":Z
    :cond_1
    :goto_0
    return-void

    .line 87
    .restart local v8    # "action":Ljava/lang/String;
    :cond_2
    if-eqz v8, :cond_3

    .line 88
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.samsung.accessory.intent.action.UPDATE_NOTIFICATION_ITEM"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 89
    invoke-static/range {p1 .. p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/BManager;->isCheckSupportClock(Landroid/content/Context;)Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    .line 90
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    const-string v3, "notification"

    .line 91
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/app/NotificationManager;

    .line 92
    .local v14, "notiManager":Landroid/app/NotificationManager;
    const/4 v2, 0x1

    invoke-virtual {v14, v2}, Landroid/app/NotificationManager;->cancel(I)V

    .line 93
    const-string v2, ""

    const-string v3, " Noti cnl"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 155
    .end local v8    # "action":Ljava/lang/String;
    .end local v14    # "notiManager":Landroid/app/NotificationManager;
    :catch_0
    move-exception v9

    .line 156
    .local v9, "e":Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 95
    .end local v9    # "e":Ljava/lang/Exception;
    .restart local v8    # "action":Ljava/lang/String;
    :cond_3
    if-eqz v8, :cond_6

    :try_start_1
    const-string v2, "com.sec.android.widgetapp.ap.accuweatherdaemon.action.WEATHER_NOTIFICATION_SETTING_SYNC"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 96
    const-string v2, "NOTIONOFF"

    const/4 v3, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    .line 97
    .local v11, "extra":I
    const/4 v2, 0x1

    if-eq v11, v2, :cond_4

    if-nez v11, :cond_1

    .line 98
    :cond_4
    const/4 v2, 0x1

    if-ne v11, v2, :cond_5

    const/4 v2, 0x1

    :goto_1
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->setNotiAlarm(Landroid/content/Context;Z)V

    goto :goto_0

    :cond_5
    const/4 v2, 0x0

    goto :goto_1

    .line 104
    .end local v11    # "extra":I
    :cond_6
    if-eqz v8, :cond_1

    const-string v2, "com.sec.android.widgetapp.ap.hero.accuweather.action.ALERT_NOTIFICATION"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 105
    invoke-static/range {p1 .. p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getNotificationSettings(Landroid/content/Context;)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 107
    const-string v2, ""

    const-string v3, ">>>>>>>>>> Noti se"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    invoke-static/range {p1 .. p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getNotificationTimeSettings(Landroid/content/Context;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 110
    .local v4, "setTime":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 112
    .local v6, "nowTime":J
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v13

    .line 113
    .local v13, "notiCal":Ljava/util/Calendar;
    invoke-virtual {v13, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 117
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v15

    .line 118
    .local v15, "nowCal":Ljava/util/Calendar;
    invoke-virtual {v15, v6, v7}, Ljava/util/Calendar;->setTimeInMillis(J)V

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    .line 145
    invoke-direct/range {v2 .. v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->registerNotiAlarm(Landroid/content/Context;JJ)V

    .line 149
    const-string v2, ""

    const-string v3, "Nt sm"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mCtx:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->mcl:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    .line 151
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/receiver/NotiReceiver;->getCurrentLocation(Landroid/content/Context;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
