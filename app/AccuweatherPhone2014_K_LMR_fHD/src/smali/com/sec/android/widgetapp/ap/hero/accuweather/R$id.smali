.class public final Lcom/sec/android/widgetapp/ap/hero/accuweather/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final ListBtnText:I = 0x7f08000f

.field public static final TopBar:I = 0x7f080088

.field public static final action_settings:I = 0x7f08013a

.field public static final actionbarBack:I = 0x7f080006

.field public static final actionbarCheck:I = 0x7f080010

.field public static final actionbarLayout:I = 0x7f080005

.field public static final actionbarLogo:I = 0x7f080007

.field public static final actionbarTitleText:I = 0x7f080008

.field public static final addBtnText:I = 0x7f080004

.field public static final addrowcity:I = 0x7f080111

.field public static final addrowstate:I = 0x7f080112

.field public static final animationlistview:I = 0x7f08006c

.field public static final backIcon:I = 0x7f080009

.field public static final background:I = 0x7f080033

.field public static final bgImage:I = 0x7f080016

.field public static final bottomCityInfo:I = 0x7f0800b4

.field public static final bottomDivider:I = 0x7f080061

.field public static final bottomForcastLayout:I = 0x7f0800c7

.field public static final cancelBtnImg:I = 0x7f08000a

.field public static final centerCityInfo:I = 0x7f0800a9

.field public static final chb_selectall:I = 0x7f08006e

.field public static final checkbox:I = 0x7f080073

.field public static final checkbox_check:I = 0x7f080066

.field public static final cityLayout:I = 0x7f080057

.field public static final cityNameAndStateLayout:I = 0x7f0800a6

.field public static final cityNameLayout:I = 0x7f0800a7

.field public static final cityOtherInfoLayout:I = 0x7f0800b5

.field public static final cityOtherValueLayout:I = 0x7f0800b6

.field public static final cityTempAndIconLayout:I = 0x7f0800ac

.field public static final cityTempLayout:I = 0x7f0800af

.field public static final cityTimeAndDateLayout:I = 0x7f0800aa

.field public static final cityWeatherTextLayout:I = 0x7f0800b2

.field public static final city_text:I = 0x7f080027

.field public static final cityinfo_layout:I = 0x7f08002e

.field public static final cityname:I = 0x7f080053

.field public static final cityname_layout:I = 0x7f080025

.field public static final cityname_zone:I = 0x7f080044

.field public static final contentsLayout:I = 0x7f080072

.field public static final contentsTempLayout:I = 0x7f08007b

.field public static final context_menu_delete:I = 0x7f08012a

.field public static final cube_page1:I = 0x7f08008e

.field public static final cube_page2:I = 0x7f08008f

.field public static final cube_pager:I = 0x7f08008d

.field public static final curLocImg:I = 0x7f080058

.field public static final currentBGLayout:I = 0x7f080126

.field public static final currentCityInfoLayout:I = 0x7f080128

.field public static final current_bg:I = 0x7f080083

.field public static final current_temper:I = 0x7f080043

.field public static final currentlocationicon:I = 0x7f080075

.field public static final currenttemp:I = 0x7f080056

.field public static final deleteBtnText:I = 0x7f08000b

.field public static final detailMainLayout:I = 0x7f080087

.field public static final detail_logo:I = 0x7f0800a3

.field public static final detailupdatebtn:I = 0x7f080069

.field public static final detailupdateflipper:I = 0x7f08006a

.field public static final detailupdatetext:I = 0x7f08006b

.field public static final doneBtnImg:I = 0x7f08000d

.field public static final doneBtnTxt:I = 0x7f08000e

.field public static final dontshow:I = 0x7f08003c

.field public static final dontshowtext:I = 0x7f08003b

.field public static final dropdown_title:I = 0x7f080013

.field public static final dst_icon:I = 0x7f080020

.field public static final dst_icon_kr:I = 0x7f08001b

.field public static final effectLayout:I = 0x7f080084

.field public static final empty_city:I = 0x7f080049

.field public static final emptyselect:I = 0x7f080050

.field public static final error:I = 0x7f08004d

.field public static final error_message:I = 0x7f08004e

.field public static final error_message_gps_off:I = 0x7f08004f

.field public static final error_text:I = 0x7f080017

.field public static final finedust_content:I = 0x7f080039

.field public static final finedust_img:I = 0x7f080037

.field public static final finedust_layout:I = 0x7f080036

.field public static final finedust_title:I = 0x7f080038

.field public static final fiveDayDateText:I = 0x7f0800f5

.field public static final fiveDayFocastInfoLayout:I = 0x7f0800f3

.field public static final fiveDayHiTemp:I = 0x7f0800f9

.field public static final fiveDayImg:I = 0x7f0800f6

.field public static final fiveDayLowTemp:I = 0x7f0800fa

.field public static final fiveDayProbability:I = 0x7f0800fc

.field public static final fiveDayRainImg:I = 0x7f0800fb

.field public static final fiveDayText:I = 0x7f0800f4

.field public static final fivetempslash:I = 0x7f0800f7

.field public static final fivetempunitH:I = 0x7f0800f8

.field public static final focastInfos:I = 0x7f0800ca

.field public static final focastInfosLayout:I = 0x7f0800c8

.field public static final forcast_line:I = 0x7f0800c9

.field public static final fourDayDateText:I = 0x7f0800eb

.field public static final fourDayFocastInfoLayout:I = 0x7f0800e9

.field public static final fourDayHiTemp:I = 0x7f0800ef

.field public static final fourDayImg:I = 0x7f0800ec

.field public static final fourDayLowTemp:I = 0x7f0800f0

.field public static final fourDayProbability:I = 0x7f0800f2

.field public static final fourDayRainImg:I = 0x7f0800f1

.field public static final fourDayText:I = 0x7f0800ea

.field public static final fourtempslash:I = 0x7f0800ed

.field public static final fourtempunitH:I = 0x7f0800ee

.field public static final frame_mask_shadow:I = 0x7f080123

.field public static final frame_masking:I = 0x7f080124

.field public static final frame_shadow:I = 0x7f080125

.field public static final frameani:I = 0x7f08002f

.field public static final gps_btn:I = 0x7f08010e

.field public static final highLowTemper:I = 0x7f08007f

.field public static final highlow_layout:I = 0x7f080029

.field public static final iconani_layout:I = 0x7f080030

.field public static final image:I = 0x7f080001

.field public static final invisiblePopupMenuHolder:I = 0x7f080089

.field public static final lblforselectall:I = 0x7f08006f

.field public static final list_btn:I = 0x7f080120

.field public static final list_logo:I = 0x7f080068

.field public static final logo_zone:I = 0x7f080048

.field public static final mainLayout:I = 0x7f0800a4

.field public static final mapView:I = 0x7f080121

.field public static final map_btn:I = 0x7f08010f

.field public static final map_search_clear_text:I = 0x7f08011f

.field public static final map_search_layout_outline:I = 0x7f08011e

.field public static final map_search_message:I = 0x7f08005a

.field public static final map_search_title:I = 0x7f080059

.field public static final map_top:I = 0x7f080052

.field public static final mapupdatetext:I = 0x7f080122

.field public static final maxval:I = 0x7f080060

.field public static final media_play_view:I = 0x7f080081

.field public static final menu_Add:I = 0x7f080132

.field public static final menu_Cancel:I = 0x7f08013b

.field public static final menu_ChangeOrders:I = 0x7f08012e

.field public static final menu_CityList:I = 0x7f080136

.field public static final menu_Delete:I = 0x7f080134

.field public static final menu_Delete2:I = 0x7f080135

.field public static final menu_Map:I = 0x7f080137

.field public static final menu_More:I = 0x7f080138

.field public static final menu_OverflowMenu:I = 0x7f080133

.field public static final menu_Report_wrong_city_name:I = 0x7f080139

.field public static final menu_Save:I = 0x7f08013c

.field public static final menu_Search:I = 0x7f08012f

.field public static final menu_Select:I = 0x7f08012d

.field public static final menu_SetDefaultCity:I = 0x7f080130

.field public static final menu_Settings:I = 0x7f080131

.field public static final menu_add_clear_text:I = 0x7f08010d

.field public static final menu_add_search_edittext:I = 0x7f08010a

.field public static final menu_add_search_icon:I = 0x7f080109

.field public static final menu_add_search_layout:I = 0x7f08010b

.field public static final menu_add_search_layout_outline:I = 0x7f080108

.field public static final menu_add_search_text:I = 0x7f08010c

.field public static final menu_search_clear_text:I = 0x7f080117

.field public static final menu_search_search_layout_outline:I = 0x7f080115

.field public static final menu_settings_title:I = 0x7f08003a

.field public static final menu_switch:I = 0x7f08012b

.field public static final menuadd:I = 0x7f08008b

.field public static final menuaddtolist:I = 0x7f08008c

.field public static final menugolist:I = 0x7f080082

.field public static final menutopbutton:I = 0x7f08008a

.field public static final middleLayout:I = 0x7f08005c

.field public static final minval:I = 0x7f08005f

.field public static final msg_toast:I = 0x7f080063

.field public static final mycitycurrentcentigrade:I = 0x7f08007e

.field public static final mycitydatacurrenttemp:I = 0x7f08007d

.field public static final mycitydataname:I = 0x7f080076

.field public static final mycitydatanameLayout:I = 0x7f080074

.field public static final mycitydatastatename:I = 0x7f080077

.field public static final mycitydatatime:I = 0x7f080079

.field public static final mycitydatatimeLayout:I = 0x7f080078

.field public static final mycitydatatimeampm:I = 0x7f08007a

.field public static final mycitydataweathericon:I = 0x7f08007c

.field public static final nextBGLayout:I = 0x7f080127

.field public static final nextCityInfoLayout:I = 0x7f080129

.field public static final nosearch:I = 0x7f080110

.field public static final notempty_city:I = 0x7f08003e

.field public static final oneDayDateText:I = 0x7f0800cd

.field public static final oneDayFocastInfoLayout:I = 0x7f0800cb

.field public static final oneDayHiTemp:I = 0x7f0800d1

.field public static final oneDayImg:I = 0x7f0800ce

.field public static final oneDayLowTemp:I = 0x7f0800d2

.field public static final oneDayProbability:I = 0x7f0800d4

.field public static final oneDayRainImg:I = 0x7f0800d3

.field public static final oneDayText:I = 0x7f0800cc

.field public static final onetempslash:I = 0x7f0800cf

.field public static final onetempunitH:I = 0x7f0800d0

.field public static final pageindicator01:I = 0x7f080092

.field public static final pageindicator02:I = 0x7f080093

.field public static final pageindicator03:I = 0x7f080094

.field public static final pageindicator04:I = 0x7f080095

.field public static final pageindicator05:I = 0x7f080096

.field public static final pageindicator06:I = 0x7f080097

.field public static final pageindicator07:I = 0x7f080098

.field public static final pageindicator08:I = 0x7f080099

.field public static final pageindicator09:I = 0x7f08009a

.field public static final pageindicator10:I = 0x7f08009b

.field public static final pageindicator11:I = 0x7f08009c

.field public static final pageindicator12:I = 0x7f08009d

.field public static final pageindicator13:I = 0x7f08009e

.field public static final pageindicator14:I = 0x7f08009f

.field public static final pageindicator15:I = 0x7f0800a0

.field public static final pageindicator16:I = 0x7f0800a1

.field public static final pageindicator17:I = 0x7f0800a2

.field public static final pageindicatorLayout:I = 0x7f080091

.field public static final positivebtn_item:I = 0x7f08012c

.field public static final precipitaionValue:I = 0x7f0800bd

.field public static final precipitaionValue2:I = 0x7f0800c0

.field public static final probability:I = 0x7f0800bc

.field public static final probability2:I = 0x7f0800bf

.field public static final probability_1line:I = 0x7f0800bb

.field public static final probability_2line:I = 0x7f0800be

.field public static final radiobutton:I = 0x7f080114

.field public static final rainIcon:I = 0x7f0800ba

.field public static final realfeel:I = 0x7f0800b8

.field public static final realfeelIcon:I = 0x7f0800b7

.field public static final realfeelValue:I = 0x7f0800b9

.field public static final refresh:I = 0x7f080032

.field public static final refreshLayout:I = 0x7f080051

.field public static final refreshflipper:I = 0x7f080034

.field public static final refreshtest:I = 0x7f080035

.field public static final refreshtime:I = 0x7f080047

.field public static final refreshtime_zone:I = 0x7f080046

.field public static final rowcitylistname:I = 0x7f080064

.field public static final rowstatelistname:I = 0x7f080065

.field public static final searchBoxLayout:I = 0x7f08011d

.field public static final search_gps_btn:I = 0x7f080118

.field public static final search_layout:I = 0x7f080107

.field public static final search_map_btn:I = 0x7f080119

.field public static final search_menu_add_search_edittext:I = 0x7f080116

.field public static final search_nosearch:I = 0x7f08011a

.field public static final searchrowcity:I = 0x7f08011b

.field public static final searchrowstate:I = 0x7f08011c

.field public static final seekbar:I = 0x7f08005e

.field public static final selectallblock:I = 0x7f08006d

.field public static final selectedText:I = 0x7f080011

.field public static final selector:I = 0x7f080070

.field public static final settings_list:I = 0x7f080113

.field public static final sixDayDateText:I = 0x7f0800ff

.field public static final sixDayFocastInfoLayout:I = 0x7f0800fd

.field public static final sixDayHiTemp:I = 0x7f080103

.field public static final sixDayImg:I = 0x7f080100

.field public static final sixDayLowTemp:I = 0x7f080104

.field public static final sixDayProbability:I = 0x7f080106

.field public static final sixDayRainImg:I = 0x7f080105

.field public static final sixDayText:I = 0x7f0800fe

.field public static final sixtempslash:I = 0x7f080101

.field public static final sixtempunitH:I = 0x7f080102

.field public static final space:I = 0x7f080003

.field public static final spinner:I = 0x7f080012

.field public static final stroke:I = 0x7f080071

.field public static final sunIcon:I = 0x7f0800c4

.field public static final sunState:I = 0x7f0800c5

.field public static final switchForActionBar:I = 0x7f080015

.field public static final taptoaddcity:I = 0x7f08004b

.field public static final taptoaddcitysub:I = 0x7f08004c

.field public static final temper_unit:I = 0x7f080042

.field public static final temper_zone:I = 0x7f080041

.field public static final temperature_icon:I = 0x7f08002b

.field public static final temperature_layout:I = 0x7f080026

.field public static final temperature_text:I = 0x7f08002a

.field public static final tempscale:I = 0x7f080055

.field public static final text:I = 0x7f080000

.field public static final text_and_image:I = 0x7f080002

.field public static final threeDayDateText:I = 0x7f0800e1

.field public static final threeDayFocastInfoLayout:I = 0x7f0800df

.field public static final threeDayHiTemp:I = 0x7f0800e5

.field public static final threeDayImg:I = 0x7f0800e2

.field public static final threeDayLowTemp:I = 0x7f0800e6

.field public static final threeDayProbability:I = 0x7f0800e8

.field public static final threeDayRainImg:I = 0x7f0800e7

.field public static final threeDayText:I = 0x7f0800e0

.field public static final threetempslash:I = 0x7f0800e3

.field public static final threetempunitH:I = 0x7f0800e4

.field public static final timeValue:I = 0x7f0800c6

.field public static final title:I = 0x7f080014

.field public static final toast_layout_root:I = 0x7f080062

.field public static final todayImg:I = 0x7f080080

.field public static final topCityInfo:I = 0x7f0800a5

.field public static final topLayout:I = 0x7f08000c

.field public static final transparency_val:I = 0x7f08005d

.field public static final twoDayDateText:I = 0x7f0800d7

.field public static final twoDayFocastInfoLayout:I = 0x7f0800d5

.field public static final twoDayHiTemp:I = 0x7f0800db

.field public static final twoDayImg:I = 0x7f0800d8

.field public static final twoDayLowTemp:I = 0x7f0800dc

.field public static final twoDayProbability:I = 0x7f0800de

.field public static final twoDayRainImg:I = 0x7f0800dd

.field public static final twoDayText:I = 0x7f0800d6

.field public static final twotempslash:I = 0x7f0800d9

.field public static final twotempunitH:I = 0x7f0800da

.field public static final updateLayout:I = 0x7f080067

.field public static final update_layout:I = 0x7f080028

.field public static final updatedate_text:I = 0x7f080031

.field public static final uvIcon:I = 0x7f0800c1

.field public static final uvIndex:I = 0x7f0800c2

.field public static final uvIndexValue:I = 0x7f0800c3

.field public static final weatherAmPm_zone:I = 0x7f08001f

.field public static final weatherAmPm_zone_kr:I = 0x7f08001a

.field public static final weatherCity:I = 0x7f0800a8

.field public static final weatherDate:I = 0x7f0800ab

.field public static final weatherDate_zone:I = 0x7f080022

.field public static final weatherIcon:I = 0x7f0800ae

.field public static final weatherIconLayout:I = 0x7f0800ad

.field public static final weatherTemp:I = 0x7f0800b0

.field public static final weatherTempHigh:I = 0x7f08002c

.field public static final weatherTempLow:I = 0x7f08002d

.field public static final weatherTemper:I = 0x7f0800b1

.field public static final weatherText:I = 0x7f0800b3

.field public static final weatherTimaAndDate_zone:I = 0x7f080018

.field public static final weatherTimeAndAmPm_zone:I = 0x7f080019

.field public static final weatherTime_zone:I = 0x7f08001d

.field public static final weather_AmPm:I = 0x7f080021

.field public static final weather_AmPm_kr:I = 0x7f08001c

.field public static final weather_Date:I = 0x7f080023

.field public static final weather_bg:I = 0x7f08003d

.field public static final weather_city:I = 0x7f080045

.field public static final weather_effect_frame:I = 0x7f080085

.field public static final weather_effect_view:I = 0x7f080086

.field public static final weather_info:I = 0x7f08005b

.field public static final weather_time:I = 0x7f08001e

.field public static final weathericon:I = 0x7f080040

.field public static final weathericonzone:I = 0x7f08003f

.field public static final weatherimg:I = 0x7f080054

.field public static final weatherinfo_group:I = 0x7f080024

.field public static final weathertitle:I = 0x7f08004a

.field public static final weeklyLayout:I = 0x7f080090


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 724
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
