.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;
.super Ljava/lang/Object;
.source "RainParticleObj.java"

# interfaces
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/ParticleObj;


# instance fields
.field private final FADEIN_SPEED:I

.field private final MAX_ALPHA:I

.field private final MOVE_X_SPEED:F

.field private final MOVE_Y_SPEED:F

.field private final X_DEFAULT_GAP:F

.field private final Y_DEFAULT_GAP:F

.field mAlpha:I

.field mAnimX:F

.field mAnimY:F

.field private mDelayCnt:I

.field private mInterval:I

.field mSX:F

.field mSY:F

.field mXGap:F

.field mYGap:F


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/16 v5, 0xff

    const/4 v4, 0x0

    const/high16 v3, 0x43160000    # 150.0f

    const/high16 v2, 0x42700000    # 60.0f

    const/4 v1, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->X_DEFAULT_GAP:F

    .line 12
    iput v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->Y_DEFAULT_GAP:F

    .line 14
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->FADEIN_SPEED:I

    .line 16
    iput v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->MAX_ALPHA:I

    .line 18
    const v0, 0x3fdba5e3    # 1.716f

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->MOVE_X_SPEED:F

    .line 20
    const v0, 0x408b645a    # 4.356f

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->MOVE_Y_SPEED:F

    .line 22
    iput v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mAlpha:I

    .line 24
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mSX:F

    .line 26
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mSY:F

    .line 28
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mAnimX:F

    .line 30
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mAnimY:F

    .line 32
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mXGap:F

    .line 34
    iput v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mYGap:F

    .line 54
    iput v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mInterval:I

    .line 60
    iput v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mDelayCnt:I

    .line 38
    return-void
.end method


# virtual methods
.method public Animate(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "paint"    # Landroid/graphics/Paint;

    .prologue
    const/4 v2, 0x0

    .line 63
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mDelayCnt:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mDelayCnt:I

    .line 65
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mDelayCnt:I

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mInterval:I

    if-ge v0, v1, :cond_1

    .line 96
    :cond_0
    :goto_0
    return-void

    .line 68
    :cond_1
    invoke-virtual {p2}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    sget v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/AnimUtils;->RAIN_EFFECT_DIM_COLOR:I

    if-ne v0, v1, :cond_3

    .line 69
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mAlpha:I

    const/4 v1, 0x4

    if-le v0, v1, :cond_2

    .line 70
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mAlpha:I

    add-int/lit8 v0, v0, -0x4

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mAlpha:I

    .line 81
    :goto_1
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mAlpha:I

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 82
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mAnimX:F

    const v1, 0x3fdba5e3    # 1.716f

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mAnimX:F

    .line 83
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mAnimY:F

    const v1, 0x408b645a    # 4.356f

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mAnimY:F

    .line 84
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    .line 86
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mAlpha:I

    if-gtz v0, :cond_0

    .line 87
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mDelayCnt:I

    .line 88
    invoke-virtual {p2}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    sget v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/AnimUtils;->RAIN_EFFECT_DIM_COLOR:I

    if-ne v0, v1, :cond_5

    .line 89
    const/16 v0, 0x7f

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mAlpha:I

    .line 93
    :goto_2
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mSX:F

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mAnimX:F

    .line 94
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mSY:F

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mAnimY:F

    goto :goto_0

    .line 72
    :cond_2
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mAlpha:I

    goto :goto_1

    .line 75
    :cond_3
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mAlpha:I

    const/16 v1, 0x8

    if-le v0, v1, :cond_4

    .line 76
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mAlpha:I

    add-int/lit8 v0, v0, -0x8

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mAlpha:I

    goto :goto_1

    .line 78
    :cond_4
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mAlpha:I

    goto :goto_1

    .line 91
    :cond_5
    const/16 v0, 0xff

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mAlpha:I

    goto :goto_2
.end method

.method public draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 99
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mAnimX:F

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mAnimY:F

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mAnimX:F

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mXGap:F

    sub-float v3, v0, v3

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mAnimY:F

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mYGap:F

    add-float/2addr v4, v0

    move-object v0, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 100
    return-void
.end method

.method public setInterval(I)V
    .locals 0
    .param p1, "interval"    # I

    .prologue
    .line 57
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mInterval:I

    .line 58
    return-void
.end method

.method public setPos(FFF)V
    .locals 3
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "val"    # F

    .prologue
    const/high16 v2, 0x43160000    # 150.0f

    const/high16 v1, 0x42700000    # 60.0f

    .line 41
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mSX:F

    .line 42
    iput p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mSY:F

    .line 43
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mSX:F

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mAnimX:F

    .line 44
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mSY:F

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mAnimY:F

    .line 45
    const/4 v0, 0x0

    cmpl-float v0, p3, v0

    if-nez v0, :cond_0

    .line 46
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mXGap:F

    .line 47
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mYGap:F

    .line 52
    :goto_0
    return-void

    .line 49
    :cond_0
    iput p3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mXGap:F

    .line 50
    div-float v0, p3, v1

    mul-float/2addr v0, v2

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->mYGap:F

    goto :goto_0
.end method
