.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;
.super Ljava/lang/Object;
.source "AccuWeatherUrlManager.java"


# static fields
.field static final JSON_API_KEY:Ljava/lang/String; = "0460650BB2524F84BAECAA9381D79EFC"

.field static final JSON_LANGUAGES_CODE:Ljava/lang/String; = "&language=%s"

.field static final JSON_PROVIDER_HOME_URL:Ljava/lang/String; = "http://apidev.accuweather.com"

.field private static final KEY_UNIT:Ljava/lang/String; = "unit"

.field static final PORT:I = 0x50

.field static final PORT_SSL:I = 0x1bb

.field static final PROTOCOL:Ljava/lang/String; = "http"

.field static final PROTOCOL_SSL:Ljava/lang/String; = "https"

.field static final PROVIDER_HOME_URL:Ljava/lang/String; = "http://www.accuweather.com"

.field private static final UNIT_VALUE_CENTIGRADE:Ljava/lang/String; = "c"

.field private static final UNIT_VALUE_FAHRENHEIT:Ljava/lang/String; = "f"

.field static final garbage:Ljava/lang/String; = "&"

.field static final new_BASE_URL:Ljava/lang/String; = "samsungmobile.accu-weather.com"

.field static final new_FILE_GET_DATA_LOCATION:Ljava/lang/String; = "/widget/samsungmobile/city-find.asp?returnGeoPosition=1&latitude=%s&longitude=%s"

.field static final new_FILE_GET_DATA_MULTI:Ljava/lang/String; = "/widget/samsungmobile/briefing_weather.asp?metric=%d&location=%s"

.field static final new_FILE_GET_DATA_SINGLE:Ljava/lang/String; = "/widget/samsungmobile/weather-data.asp?metric=%d&location=%s"

.field static final new_JSON_BASE_URL:Ljava/lang/String; = "api.accuweather.com"

.field static final new_JSON_FILE_GET_DATA_AUTOCOMPLETE:Ljava/lang/String; = "/locations/v1/cities/autocomplete.json?q=%s&apikey=%s&language=%s"

.field static final new_JSON_FILE_GET_DATA_FREE_TEXT_SEARCH:Ljava/lang/String; = "/locations/v1/search?q=%s&apikey=%s&language=%s&details=true"

.field static final new_JSON_FILE_GET_DATA_LOCATION:Ljava/lang/String; = "/locations/v1/cities/geoposition/search.json?q=%s,%s&apikey=%s&language=%s"

.field static final new_JSON_FILE_GET_DATA_MULTI:Ljava/lang/String; = "/currentconditions/v1/topcities/150?apikey=%s&language=%s"

.field static final new_JSON_FILE_GET_DATA_SINGLE:Ljava/lang/String; = "/localweather/v1/%s?apikey=%s&language=%s&details=true&getPhotos=true&metric=%s"

.field static final new_JSON_FILE_GET_DATA_ZOOM5:Ljava/lang/String; = "/currentconditions/v1/topcities/50?apikey=%s&language=%s"

.field static final new_JSON_FILE_GET_DATA_ZOOM6:Ljava/lang/String; = "/currentconditions/v1/topcities/100?apikey=%s&language=%s"

.field static final new_JSON_FILE_GET_DATA_ZOOM7:Ljava/lang/String; = "/currentconditions/v1/topcities/150?apikey=%s&language=%s"


# instance fields
.field private context:Landroid/content/Context;

.field lang:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    const/16 v0, 0x22

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "en"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "es"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "fr"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "da"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "pt"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "nl"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "no"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "it"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "de"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "sv"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "fi"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "HKUTF"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "SIMUTF"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "TAIUTF"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "es-ar"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "LATIN SPANISH"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "sk"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "ro"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "cs"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "hu"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "pl"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "ca"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "pt-br"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "hi"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "ru"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "ar"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "el"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "en-gb"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "ja"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "ko"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "tr"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "fr-ca"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "he"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->lang:[Ljava/lang/String;

    .line 74
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->context:Landroid/content/Context;

    .line 75
    return-void
.end method

.method public static appendPartnerCodeAt(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 2
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 345
    const-string v0, "partner"

    const-string v1, "samand"

    invoke-static {p0, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->appendQueryAt(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static appendQueryAt(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 10
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 370
    :try_start_0
    const-string v7, ""

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[aQA] key : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", value : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 374
    .local v0, "builder":Landroid/net/Uri$Builder;
    invoke-virtual {p0, p1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 375
    invoke-virtual {p0}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 377
    .local v2, "keyIt":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->clearQuery()Landroid/net/Uri$Builder;

    .line 379
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 380
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 381
    .local v5, "tempKey":Ljava/lang/String;
    invoke-virtual {p0, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 382
    .local v6, "tempValue":Ljava/lang/String;
    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 383
    move-object v6, p2

    .line 386
    :cond_0
    const-string v7, ""

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[aQA] tempKey : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", tempValue : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    invoke-virtual {v0, v5, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 395
    .end local v0    # "builder":Landroid/net/Uri$Builder;
    .end local v2    # "keyIt":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v5    # "tempKey":Ljava/lang/String;
    .end local v6    # "tempValue":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 396
    .local v1, "e":Ljava/lang/UnsupportedOperationException;
    invoke-virtual {v1}, Ljava/lang/UnsupportedOperationException;->printStackTrace()V

    .line 400
    .end local v1    # "e":Ljava/lang/UnsupportedOperationException;
    .end local p0    # "uri":Landroid/net/Uri;
    :goto_1
    return-object p0

    .line 390
    .restart local v0    # "builder":Landroid/net/Uri$Builder;
    .restart local p0    # "uri":Landroid/net/Uri;
    :cond_1
    :try_start_1
    invoke-virtual {v0, p1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 393
    :cond_2
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;
    :try_end_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    .local v3, "newUri":Landroid/net/Uri;
    move-object p0, v3

    .line 394
    goto :goto_1

    .line 398
    .end local v0    # "builder":Landroid/net/Uri$Builder;
    .end local v3    # "newUri":Landroid/net/Uri;
    :catch_1
    move-exception v4

    .line 399
    .local v4, "npe":Ljava/lang/NullPointerException;
    invoke-virtual {v4}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1
.end method

.method public static appendUnitCodeAt(Landroid/net/Uri;I)Landroid/net/Uri;
    .locals 2
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "scale"    # I

    .prologue
    .line 355
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    const-string v0, "c"

    .line 358
    .local v0, "value":Ljava/lang/String;
    :goto_0
    const-string v1, "unit"

    invoke-static {p0, v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->appendQueryAt(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    return-object v1

    .line 355
    .end local v0    # "value":Ljava/lang/String;
    :cond_0
    const-string v0, "f"

    goto :goto_0
.end method


# virtual methods
.method public checkDeutch()Ljava/lang/String;
    .locals 4

    .prologue
    .line 107
    const-string v1, ""

    .line 108
    .local v1, "result":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 109
    .local v0, "lnaguage":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 110
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->lang:[Ljava/lang/String;

    const/16 v3, 0x9

    aget-object v2, v2, v3

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 111
    const-string v1, "&langid=9"

    .line 113
    :cond_0
    return-object v1
.end method

.method public getHomeUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 131
    const-string v0, "http://www.accuweather.com/?p=samand"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getLangCode(Ljava/lang/String;)I
    .locals 3
    .param p1, "code"    # Ljava/lang/String;

    .prologue
    .line 123
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->lang:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 124
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->lang:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 127
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 123
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 127
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public makeForecastUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 4
    .param p1, "lang"    # Ljava/lang/String;
    .param p2, "location"    # Ljava/lang/String;

    .prologue
    .line 334
    invoke-static {p2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 335
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "http://www.accuweather.com"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/m/forecast.aspx?p=samand&lang=%s&loc=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    .line 336
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 335
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public makeHeader()Lorg/apache/http/message/HeaderGroup;
    .locals 4

    .prologue
    .line 78
    new-instance v0, Lorg/apache/http/message/HeaderGroup;

    invoke-direct {v0}, Lorg/apache/http/message/HeaderGroup;-><init>()V

    .line 79
    .local v0, "headerGroup":Lorg/apache/http/message/HeaderGroup;
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "User-Agent"

    const-string v3, "SAMSUNG-Android"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/apache/http/message/HeaderGroup;->addHeader(Lorg/apache/http/Header;)V

    .line 80
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "Accept"

    const-string v3, "*,*/*"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/apache/http/message/HeaderGroup;->addHeader(Lorg/apache/http/Header;)V

    .line 81
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "Content-Type"

    const-string v3, "text/xml"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/apache/http/message/HeaderGroup;->addHeader(Lorg/apache/http/Header;)V

    .line 82
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "X-Client-Info"

    const/4 v3, 0x0

    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getPackagename(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/apache/http/message/HeaderGroup;->addHeader(Lorg/apache/http/Header;)V

    .line 83
    return-object v0
.end method

.method public makeHeader(Ljava/lang/String;)Lorg/apache/http/message/HeaderGroup;
    .locals 4
    .param p1, "mPackagename"    # Ljava/lang/String;

    .prologue
    .line 87
    new-instance v0, Lorg/apache/http/message/HeaderGroup;

    invoke-direct {v0}, Lorg/apache/http/message/HeaderGroup;-><init>()V

    .line 88
    .local v0, "headerGroup":Lorg/apache/http/message/HeaderGroup;
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "User-Agent"

    const-string v3, "SAMSUNG-Android"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/apache/http/message/HeaderGroup;->addHeader(Lorg/apache/http/Header;)V

    .line 89
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "Accept"

    const-string v3, "*,*/*"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/apache/http/message/HeaderGroup;->addHeader(Lorg/apache/http/Header;)V

    .line 90
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "Content-Type"

    const-string v3, "text/xml"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/apache/http/message/HeaderGroup;->addHeader(Lorg/apache/http/Header;)V

    .line 91
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "X-Client-Info"

    invoke-direct {v1, v2, p1}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/apache/http/message/HeaderGroup;->addHeader(Lorg/apache/http/Header;)V

    .line 92
    return-object v0
.end method

.method public makeHourViewUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 4
    .param p1, "lang"    # Ljava/lang/String;
    .param p2, "location"    # Ljava/lang/String;

    .prologue
    .line 322
    invoke-static {p2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 323
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "http://www.accuweather.com"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/m/hourly.aspx?p=samand&lang=%s&loc=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    .line 324
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 323
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public makeLangId(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "pre"    # Ljava/lang/String;

    .prologue
    .line 96
    move-object v2, p1

    .line 97
    .local v2, "post":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v3, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 98
    .local v1, "lang":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 99
    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->getLangCode(Ljava/lang/String;)I

    move-result v0

    .line 100
    .local v0, "code":I
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 101
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "&langid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 103
    .end local v0    # "code":I
    :cond_0
    return-object v2
.end method

.method public makeMainUri()Landroid/net/Uri;
    .locals 3

    .prologue
    .line 150
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "http://www.accuweather.com"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/m"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public makeMainUri(Ljava/lang/String;)Landroid/net/Uri;
    .locals 4
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    .line 144
    const-string v0, ":"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 145
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "http://www.accuweather.com"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/m/Quick-Look.aspx?p=samand&cityId=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    .line 146
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 145
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public makeQuickViewUri(Ljava/lang/String;Ljava/lang/String;I)Landroid/net/Uri;
    .locals 5
    .param p1, "lang"    # Ljava/lang/String;
    .param p2, "location"    # Ljava/lang/String;
    .param p3, "tempScale"    # I

    .prologue
    .line 135
    invoke-static {p2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 137
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "http://www.accuweather.com"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/m/Quick-Look.aspx?p=samand&lang=%s&loc=cityid:%s&metric=%d"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    const/4 v3, 0x2

    .line 140
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 139
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 138
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public makeRadarSatViewUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 4
    .param p1, "lang"    # Ljava/lang/String;
    .param p2, "location"    # Ljava/lang/String;

    .prologue
    .line 328
    invoke-static {p2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 329
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "http://www.accuweather.com"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/m/radar.aspx?p=samand&lang=%s&loc=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    .line 330
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 329
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public makeUrlForGetAutoComplete(Ljava/lang/String;Ljava/lang/String;)Ljava/net/URL;
    .locals 10
    .param p1, "location"    # Ljava/lang/String;
    .param p2, "language"    # Ljava/lang/String;

    .prologue
    .line 205
    const/4 v7, 0x0

    .line 207
    .local v7, "url":Ljava/net/URL;
    :try_start_0
    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 208
    invoke-static {p2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 209
    new-instance v0, Ljava/net/URL;

    const-string v1, "https"

    const-string v2, "api.accuweather.com"

    const/16 v3, 0x1bb

    const-string v4, "/locations/v1/cities/autocomplete.json?q=%s&apikey=%s&language=%s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p1, v5, v8

    const/4 v8, 0x1

    const-string v9, "0460650BB2524F84BAECAA9381D79EFC"

    aput-object v9, v5, v8

    const/4 v8, 0x2

    aput-object p2, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Ljava/net/URL;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/net/URLStreamHandler;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 214
    .end local v7    # "url":Ljava/net/URL;
    .local v0, "url":Ljava/net/URL;
    :goto_0
    return-object v0

    .line 211
    .end local v0    # "url":Ljava/net/URL;
    .restart local v7    # "url":Ljava/net/URL;
    :catch_0
    move-exception v6

    .line 212
    .local v6, "e":Ljava/net/MalformedURLException;
    invoke-virtual {v6}, Ljava/net/MalformedURLException;->printStackTrace()V

    move-object v0, v7

    .end local v7    # "url":Ljava/net/URL;
    .restart local v0    # "url":Ljava/net/URL;
    goto :goto_0
.end method

.method public makeUrlForGetCityList(Ljava/lang/String;Ljava/lang/String;)Ljava/net/URL;
    .locals 10
    .param p1, "location"    # Ljava/lang/String;
    .param p2, "language"    # Ljava/lang/String;

    .prologue
    .line 191
    const/4 v7, 0x0

    .line 193
    .local v7, "url":Ljava/net/URL;
    :try_start_0
    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 194
    invoke-static {p2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 195
    new-instance v0, Ljava/net/URL;

    const-string v1, "https"

    const-string v2, "api.accuweather.com"

    const/16 v3, 0x1bb

    const-string v4, "/locations/v1/search?q=%s&apikey=%s&language=%s&details=true"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p1, v5, v8

    const/4 v8, 0x1

    const-string v9, "0460650BB2524F84BAECAA9381D79EFC"

    aput-object v9, v5, v8

    const/4 v8, 0x2

    aput-object p2, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Ljava/net/URL;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/net/URLStreamHandler;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 201
    .end local v7    # "url":Ljava/net/URL;
    .local v0, "url":Ljava/net/URL;
    :goto_0
    return-object v0

    .line 198
    .end local v0    # "url":Ljava/net/URL;
    .restart local v7    # "url":Ljava/net/URL;
    :catch_0
    move-exception v6

    .line 199
    .local v6, "e":Ljava/net/MalformedURLException;
    invoke-virtual {v6}, Ljava/net/MalformedURLException;->printStackTrace()V

    move-object v0, v7

    .end local v7    # "url":Ljava/net/URL;
    .restart local v0    # "url":Ljava/net/URL;
    goto :goto_0
.end method

.method public makeUrlForGetData(Ljava/lang/String;I)Ljava/net/URL;
    .locals 12
    .param p1, "location"    # Ljava/lang/String;
    .param p2, "tempScale"    # I

    .prologue
    .line 276
    const/4 v8, 0x0

    .line 278
    .local v8, "url":Ljava/net/URL;
    :try_start_0
    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 279
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->checkDeutch()Ljava/lang/String;

    move-result-object v7

    .line 280
    .local v7, "langId":Ljava/lang/String;
    new-instance v0, Ljava/net/URL;

    const-string v1, "http"

    const-string v2, "samsungmobile.accu-weather.com"

    const/16 v3, 0x50

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/widget/samsungmobile/weather-data.asp?metric=%d&location=%s"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    .line 281
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    aput-object p1, v9, v10

    .line 280
    invoke-static {v4, v5, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Ljava/net/URL;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/net/URLStreamHandler;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 285
    .end local v7    # "langId":Ljava/lang/String;
    .end local v8    # "url":Ljava/net/URL;
    .local v0, "url":Ljava/net/URL;
    :goto_0
    return-object v0

    .line 282
    .end local v0    # "url":Ljava/net/URL;
    .restart local v8    # "url":Ljava/net/URL;
    :catch_0
    move-exception v6

    .line 283
    .local v6, "e":Ljava/net/MalformedURLException;
    invoke-virtual {v6}, Ljava/net/MalformedURLException;->printStackTrace()V

    move-object v0, v8

    .end local v8    # "url":Ljava/net/URL;
    .restart local v0    # "url":Ljava/net/URL;
    goto :goto_0
.end method

.method public makeUrlForGetDetailData(Ljava/lang/String;ILjava/lang/String;)Ljava/net/URL;
    .locals 11
    .param p1, "location"    # Ljava/lang/String;
    .param p2, "tempScale"    # I
    .param p3, "language"    # Ljava/lang/String;

    .prologue
    .line 301
    const/4 v7, 0x0

    .line 303
    .local v7, "url":Ljava/net/URL;
    :try_start_0
    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 304
    invoke-static {p3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    .line 305
    new-instance v0, Ljava/net/URL;

    const-string v1, "https"

    const-string v2, "api.accuweather.com"

    const/16 v3, 0x1bb

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "/localweather/v1/%s?apikey=%s&language=%s&details=true&getPhotos=true&metric=%s"

    const/4 v4, 0x4

    new-array v9, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v9, v4

    const/4 v4, 0x1

    const-string v10, "0460650BB2524F84BAECAA9381D79EFC"

    aput-object v10, v9, v4

    const/4 v4, 0x2

    aput-object p3, v9, v4

    const/4 v10, 0x3

    if-nez p2, :cond_0

    const/4 v4, 0x0

    .line 307
    :goto_0
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v9, v10

    .line 305
    invoke-static {v5, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Ljava/net/URL;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/net/URLStreamHandler;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 311
    .end local v7    # "url":Ljava/net/URL;
    .local v0, "url":Ljava/net/URL;
    :goto_1
    return-object v0

    .line 305
    .end local v0    # "url":Ljava/net/URL;
    .restart local v7    # "url":Ljava/net/URL;
    :cond_0
    const/4 v4, 0x1

    goto :goto_0

    .line 308
    :catch_0
    move-exception v6

    .line 309
    .local v6, "e":Ljava/net/MalformedURLException;
    invoke-virtual {v6}, Ljava/net/MalformedURLException;->printStackTrace()V

    move-object v0, v7

    .end local v7    # "url":Ljava/net/URL;
    .restart local v0    # "url":Ljava/net/URL;
    goto :goto_1
.end method

.method public makeUrlForGetMultiData(Ljava/lang/String;)Ljava/net/URL;
    .locals 10
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    .line 232
    const/4 v7, 0x0

    .line 234
    .local v7, "url":Ljava/net/URL;
    :try_start_0
    new-instance v0, Ljava/net/URL;

    const-string v1, "https"

    const-string v2, "api.accuweather.com"

    const/16 v3, 0x1bb

    const-string v4, "/currentconditions/v1/topcities/150?apikey=%s&language=%s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string v9, "0460650BB2524F84BAECAA9381D79EFC"

    aput-object v9, v5, v8

    const/4 v8, 0x1

    aput-object p1, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Ljava/net/URL;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/net/URLStreamHandler;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 239
    .end local v7    # "url":Ljava/net/URL;
    .local v0, "url":Ljava/net/URL;
    :goto_0
    return-object v0

    .line 236
    .end local v0    # "url":Ljava/net/URL;
    .restart local v7    # "url":Ljava/net/URL;
    :catch_0
    move-exception v6

    .line 237
    .local v6, "e":Ljava/net/MalformedURLException;
    invoke-virtual {v6}, Ljava/net/MalformedURLException;->printStackTrace()V

    move-object v0, v7

    .end local v7    # "url":Ljava/net/URL;
    .restart local v0    # "url":Ljava/net/URL;
    goto :goto_0
.end method

.method public makeUrlForGetMultiData(Ljava/lang/String;I)Ljava/net/URL;
    .locals 11
    .param p1, "location"    # Ljava/lang/String;
    .param p2, "tempScale"    # I

    .prologue
    .line 289
    const/4 v7, 0x0

    .line 291
    .local v7, "url":Ljava/net/URL;
    :try_start_0
    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 292
    new-instance v0, Ljava/net/URL;

    const-string v1, "http"

    const-string v2, "samsungmobile.accu-weather.com"

    const/16 v3, 0x50

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "/widget/samsungmobile/briefing_weather.asp?metric=%d&location=%s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    .line 293
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    aput-object p1, v8, v9

    .line 292
    invoke-static {v4, v5, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Ljava/net/URL;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/net/URLStreamHandler;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 297
    .end local v7    # "url":Ljava/net/URL;
    .local v0, "url":Ljava/net/URL;
    :goto_0
    return-object v0

    .line 294
    .end local v0    # "url":Ljava/net/URL;
    .restart local v7    # "url":Ljava/net/URL;
    :catch_0
    move-exception v6

    .line 295
    .local v6, "e":Ljava/net/MalformedURLException;
    invoke-virtual {v6}, Ljava/net/MalformedURLException;->printStackTrace()V

    move-object v0, v7

    .end local v7    # "url":Ljava/net/URL;
    .restart local v0    # "url":Ljava/net/URL;
    goto :goto_0
.end method

.method public makeUrlForGetTextSearching(Ljava/lang/String;Ljava/lang/String;)Ljava/net/URL;
    .locals 10
    .param p1, "location"    # Ljava/lang/String;
    .param p2, "language"    # Ljava/lang/String;

    .prologue
    .line 218
    const/4 v7, 0x0

    .line 220
    .local v7, "url":Ljava/net/URL;
    :try_start_0
    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 221
    invoke-static {p2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 222
    new-instance v0, Ljava/net/URL;

    const-string v1, "https"

    const-string v2, "api.accuweather.com"

    const/16 v3, 0x1bb

    const-string v4, "/locations/v1/search?q=%s&apikey=%s&language=%s&details=true"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p1, v5, v8

    const/4 v8, 0x1

    const-string v9, "0460650BB2524F84BAECAA9381D79EFC"

    aput-object v9, v5, v8

    const/4 v8, 0x2

    aput-object p2, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Ljava/net/URL;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/net/URLStreamHandler;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 228
    .end local v7    # "url":Ljava/net/URL;
    .local v0, "url":Ljava/net/URL;
    :goto_0
    return-object v0

    .line 225
    .end local v0    # "url":Ljava/net/URL;
    .restart local v7    # "url":Ljava/net/URL;
    :catch_0
    move-exception v6

    .line 226
    .local v6, "e":Ljava/net/MalformedURLException;
    invoke-virtual {v6}, Ljava/net/MalformedURLException;->printStackTrace()V

    move-object v0, v7

    .end local v7    # "url":Ljava/net/URL;
    .restart local v0    # "url":Ljava/net/URL;
    goto :goto_0
.end method

.method public makeUrlForLocGetData(Ljava/lang/String;Ljava/lang/String;Z)Ljava/net/URL;
    .locals 16
    .param p1, "latitude"    # Ljava/lang/String;
    .param p2, "longitude"    # Ljava/lang/String;
    .param p3, "dontUseLangId"    # Z

    .prologue
    .line 160
    const/4 v13, 0x0

    .line 161
    .local v13, "url":Ljava/net/URL;
    const-string v10, ""

    .line 163
    .local v10, "langId":Ljava/lang/String;
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    .line 165
    :try_start_0
    invoke-static/range {p1 .. p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 166
    invoke-static/range {p2 .. p2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 167
    new-instance v7, Ljava/math/BigDecimal;

    move-object/from16 v0, p1

    invoke-direct {v7, v0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 168
    .local v7, "b_latitude":Ljava/math/BigDecimal;
    new-instance v8, Ljava/math/BigDecimal;

    move-object/from16 v0, p2

    invoke-direct {v8, v0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 171
    .local v8, "b_longitude":Ljava/math/BigDecimal;
    const/4 v2, 0x3

    sget-object v3, Ljava/math/RoundingMode;->HALF_DOWN:Ljava/math/RoundingMode;

    invoke-virtual {v7, v2, v3}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v11

    .line 172
    .local v11, "setLatitude":Ljava/math/BigDecimal;
    const/4 v2, 0x3

    sget-object v3, Ljava/math/RoundingMode;->HALF_DOWN:Ljava/math/RoundingMode;

    invoke-virtual {v8, v2, v3}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v12

    .line 175
    .local v12, "setLongitude":Ljava/math/BigDecimal;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkLanguage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    .line 176
    new-instance v1, Ljava/net/URL;

    const-string v2, "https"

    const-string v3, "api.accuweather.com"

    const/16 v4, 0x1bb

    const-string v5, "/locations/v1/cities/geoposition/search.json?q=%s,%s&apikey=%s&language=%s"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v14, 0x0

    .line 177
    invoke-virtual {v11}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v6, v14

    const/4 v14, 0x1

    .line 178
    invoke-virtual {v12}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v6, v14

    const/4 v14, 0x2

    const-string v15, "0460650BB2524F84BAECAA9381D79EFC"

    aput-object v15, v6, v14

    const/4 v14, 0x3

    aput-object v10, v6, v14

    .line 176
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, Ljava/net/URL;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/net/URLStreamHandler;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 183
    .end local v7    # "b_latitude":Ljava/math/BigDecimal;
    .end local v8    # "b_longitude":Ljava/math/BigDecimal;
    .end local v11    # "setLatitude":Ljava/math/BigDecimal;
    .end local v12    # "setLongitude":Ljava/math/BigDecimal;
    .end local v13    # "url":Ljava/net/URL;
    .local v1, "url":Ljava/net/URL;
    :goto_0
    if-eqz v1, :cond_0

    .line 187
    :cond_0
    return-object v1

    .line 180
    .end local v1    # "url":Ljava/net/URL;
    .restart local v13    # "url":Ljava/net/URL;
    :catch_0
    move-exception v9

    .line 181
    .local v9, "e":Ljava/net/MalformedURLException;
    invoke-virtual {v9}, Ljava/net/MalformedURLException;->printStackTrace()V

    .end local v9    # "e":Ljava/net/MalformedURLException;
    :cond_1
    move-object v1, v13

    .end local v13    # "url":Ljava/net/URL;
    .restart local v1    # "url":Ljava/net/URL;
    goto :goto_0
.end method

.method public makeUrlForMapZoom5(Ljava/lang/String;)Ljava/net/URL;
    .locals 10
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    .line 243
    const/4 v7, 0x0

    .line 245
    .local v7, "url":Ljava/net/URL;
    :try_start_0
    new-instance v0, Ljava/net/URL;

    const-string v1, "https"

    const-string v2, "api.accuweather.com"

    const/16 v3, 0x1bb

    const-string v4, "/currentconditions/v1/topcities/50?apikey=%s&language=%s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string v9, "0460650BB2524F84BAECAA9381D79EFC"

    aput-object v9, v5, v8

    const/4 v8, 0x1

    aput-object p1, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Ljava/net/URL;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/net/URLStreamHandler;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 250
    .end local v7    # "url":Ljava/net/URL;
    .local v0, "url":Ljava/net/URL;
    :goto_0
    return-object v0

    .line 247
    .end local v0    # "url":Ljava/net/URL;
    .restart local v7    # "url":Ljava/net/URL;
    :catch_0
    move-exception v6

    .line 248
    .local v6, "e":Ljava/net/MalformedURLException;
    invoke-virtual {v6}, Ljava/net/MalformedURLException;->printStackTrace()V

    move-object v0, v7

    .end local v7    # "url":Ljava/net/URL;
    .restart local v0    # "url":Ljava/net/URL;
    goto :goto_0
.end method

.method public makeUrlForMapZoom6(Ljava/lang/String;)Ljava/net/URL;
    .locals 10
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    .line 254
    const/4 v7, 0x0

    .line 256
    .local v7, "url":Ljava/net/URL;
    :try_start_0
    new-instance v0, Ljava/net/URL;

    const-string v1, "https"

    const-string v2, "api.accuweather.com"

    const/16 v3, 0x1bb

    const-string v4, "/currentconditions/v1/topcities/100?apikey=%s&language=%s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string v9, "0460650BB2524F84BAECAA9381D79EFC"

    aput-object v9, v5, v8

    const/4 v8, 0x1

    aput-object p1, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Ljava/net/URL;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/net/URLStreamHandler;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 261
    .end local v7    # "url":Ljava/net/URL;
    .local v0, "url":Ljava/net/URL;
    :goto_0
    return-object v0

    .line 258
    .end local v0    # "url":Ljava/net/URL;
    .restart local v7    # "url":Ljava/net/URL;
    :catch_0
    move-exception v6

    .line 259
    .local v6, "e":Ljava/net/MalformedURLException;
    invoke-virtual {v6}, Ljava/net/MalformedURLException;->printStackTrace()V

    move-object v0, v7

    .end local v7    # "url":Ljava/net/URL;
    .restart local v0    # "url":Ljava/net/URL;
    goto :goto_0
.end method

.method public makeUrlForMapZoom7(Ljava/lang/String;)Ljava/net/URL;
    .locals 10
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    .line 265
    const/4 v7, 0x0

    .line 267
    .local v7, "url":Ljava/net/URL;
    :try_start_0
    new-instance v0, Ljava/net/URL;

    const-string v1, "https"

    const-string v2, "api.accuweather.com"

    const/16 v3, 0x1bb

    const-string v4, "/currentconditions/v1/topcities/150?apikey=%s&language=%s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string v9, "0460650BB2524F84BAECAA9381D79EFC"

    aput-object v9, v5, v8

    const/4 v8, 0x1

    aput-object p1, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Ljava/net/URL;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/net/URLStreamHandler;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 272
    .end local v7    # "url":Ljava/net/URL;
    .local v0, "url":Ljava/net/URL;
    :goto_0
    return-object v0

    .line 269
    .end local v0    # "url":Ljava/net/URL;
    .restart local v7    # "url":Ljava/net/URL;
    :catch_0
    move-exception v6

    .line 270
    .local v6, "e":Ljava/net/MalformedURLException;
    invoke-virtual {v6}, Ljava/net/MalformedURLException;->printStackTrace()V

    move-object v0, v7

    .end local v7    # "url":Ljava/net/URL;
    .restart local v0    # "url":Ljava/net/URL;
    goto :goto_0
.end method
