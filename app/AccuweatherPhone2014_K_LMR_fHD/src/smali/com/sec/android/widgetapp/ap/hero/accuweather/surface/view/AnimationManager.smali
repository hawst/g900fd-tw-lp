.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/AnimationManager;
.super Ljava/lang/Object;
.source "AnimationManager.java"


# static fields
.field public static final ANI_TYPE_SCROLL:I = 0x1001

.field public static final FLICK_UP:I = 0x1


# instance fields
.field private mScenManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

.field private mScrollAni:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;)V
    .locals 1
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "sm"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/AnimationManager;->mScrollAni:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;

    .line 29
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/AnimationManager;->mScenManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    .line 36
    iput-object p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/AnimationManager;->mScenManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    .line 37
    return-void
.end method


# virtual methods
.method public cancleAnimation()V
    .locals 3

    .prologue
    .line 118
    :try_start_0
    const-string v1, ""

    const-string v2, "cancel scr anim"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/AnimationManager;->mScrollAni:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;

    if-eqz v1, :cond_0

    .line 120
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/AnimationManager;->mScrollAni:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->cancelAnimation()V

    .line 123
    :cond_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/AnimationManager;->mScenManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    if-eqz v1, :cond_1

    .line 124
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/AnimationManager;->mScenManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->setIsStartScrollAnimation(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 129
    :cond_1
    :goto_0
    return-void

    .line 126
    :catch_0
    move-exception v0

    .line 127
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public destroyAllAnimation()V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/AnimationManager;->mScrollAni:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;

    if-eqz v0, :cond_0

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/AnimationManager;->mScrollAni:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;

    .line 66
    :cond_0
    return-void
.end method

.method public getAnimation(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/Animation;
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 46
    const/4 v0, 0x0

    .line 47
    .local v0, "ani":Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/Animation;
    packed-switch p1, :pswitch_data_0

    .line 55
    :goto_0
    return-object v0

    .line 49
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/AnimationManager;->mScrollAni:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;

    if-nez v1, :cond_0

    .line 50
    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;

    invoke-direct {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;-><init>()V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/AnimationManager;->mScrollAni:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/AnimationManager;->mScrollAni:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;

    goto :goto_0

    .line 47
    nop

    :pswitch_data_0
    .packed-switch 0x1001
        :pswitch_0
    .end packed-switch
.end method

.method public startScrollAnimation(ILandroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/animation/Animator$AnimatorListener;)V
    .locals 4
    .param p1, "direction"    # I
    .param p2, "curCityInfoView"    # Landroid/view/View;
    .param p3, "nextCityInfoView"    # Landroid/view/View;
    .param p4, "curCityBGView"    # Landroid/view/View;
    .param p5, "nextCityBGView"    # Landroid/view/View;
    .param p6, "l"    # Landroid/animation/Animator$AnimatorListener;

    .prologue
    .line 80
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 81
    .local v1, "views":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/AnimationManager;->mScrollAni:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;

    if-nez v2, :cond_0

    .line 82
    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;

    invoke-direct {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;-><init>()V

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/AnimationManager;->mScrollAni:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;

    .line 85
    :cond_0
    const/4 v2, 0x2

    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {p2, v2, v3}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 86
    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {p3, v2, v3}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 87
    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {p4, v2, v3}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 88
    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {p5, v2, v3}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 90
    invoke-virtual {p2}, Landroid/view/View;->buildLayer()V

    .line 91
    invoke-virtual {p3}, Landroid/view/View;->buildLayer()V

    .line 92
    invoke-virtual {p4}, Landroid/view/View;->buildLayer()V

    .line 93
    invoke-virtual {p5}, Landroid/view/View;->buildLayer()V

    .line 96
    invoke-virtual {v1, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 97
    invoke-virtual {v1, p5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 98
    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 99
    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 100
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/AnimationManager;->mScrollAni:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;

    invoke-virtual {v2, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->setViews(Ljava/util/ArrayList;)V

    .line 102
    const/4 v2, 0x1

    if-ne p1, v2, :cond_1

    .line 103
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/AnimationManager;->mScrollAni:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;

    const/16 v3, 0x201

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->setDirection(I)V

    .line 108
    :goto_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/AnimationManager;->mScrollAni:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;

    invoke-virtual {v2, p6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->setOnListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 109
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/AnimationManager;->mScrollAni:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->animate()V

    .line 114
    :goto_1
    return-void

    .line 105
    :cond_1
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/AnimationManager;->mScrollAni:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;

    const/16 v3, 0x202

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/ScrollAnimation;->setDirection(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 110
    :catch_0
    move-exception v0

    .line 111
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 112
    const-string v2, ""

    const-string v3, "view buildlayer: gone"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
