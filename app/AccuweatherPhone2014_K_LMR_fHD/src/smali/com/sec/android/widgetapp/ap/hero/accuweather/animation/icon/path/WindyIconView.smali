.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;
.super Landroid/view/View;
.source "WindyIconView.java"

# interfaces
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$ResultPathAlpha;,
        Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;
    }
.end annotation


# instance fields
.field TranslateAni:Landroid/animation/ValueAnimator;

.field isStop:Z

.field private mCloudPath_left:Landroid/graphics/Path;

.field private mCloudPath_right:Landroid/graphics/Path;

.field private mCloudPath_top:Landroid/graphics/Path;

.field private mPaint:Landroid/graphics/Paint;

.field private mPaint1:Landroid/graphics/Paint;

.field private mPaintColor:I

.field private mPathMaskAnim1:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

.field private mPathMaskAnim2:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

.field private mPathMaskAnim3:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

.field private mScale:F

.field private mTempBitmap:Landroid/graphics/Bitmap;

.field private mTempCanvas:Landroid/graphics/Canvas;

.field private mWindUnder1Path:Landroid/graphics/Path;

.field private mWindUnder2Path:Landroid/graphics/Path;

.field private mWindUnder3Path:Landroid/graphics/Path;

.field private misActiveAnimationThread:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attributeset"    # Landroid/util/AttributeSet;

    .prologue
    .line 27
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attributeset"    # Landroid/util/AttributeSet;
    .param p3, "i"    # I

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    .line 59
    const v0, -0xa0a0b

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPaintColor:I

    .line 246
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->isStop:Z

    .line 266
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->misActiveAnimationThread:Z

    .line 268
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    .line 32
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->init()V

    .line 33
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;

    .prologue
    .line 21
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->misActiveAnimationThread:Z

    return v0
.end method

.method private init()V
    .locals 13

    .prologue
    const v12, 0x4300ffbe

    const v11, 0x4298daa0

    const v10, 0x425e3e77    # 55.561f

    const/high16 v9, 0x42400000    # 48.0f

    const/high16 v8, 0x42200000    # 40.0f

    .line 66
    invoke-virtual {p0, p0}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 68
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->initPaint()V

    .line 70
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mWindUnder1Path:Landroid/graphics/Path;

    .line 71
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mWindUnder1Path:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 72
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mWindUnder1Path:Landroid/graphics/Path;

    const v1, 0x42a9d062

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42fdc419

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 73
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mWindUnder1Path:Landroid/graphics/Path;

    const v1, 0x42a9d062

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42fdc419

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42d86560

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42dc78d5    # 110.236f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4303a000    # 131.625f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42f08000    # 120.25f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 75
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mWindUnder1Path:Landroid/graphics/Path;

    const v1, 0x43090b44

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42f52148

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4310a000    # 144.625f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4300e000    # 128.875f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431b8000    # 155.5f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4305e000    # 133.875f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 77
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mWindUnder1Path:Landroid/graphics/Path;

    const v1, 0x4322e000    # 162.875f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4308c000    # 136.75f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x433063d7    # 176.39f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x430b46a8    # 139.276f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4346a000    # 198.625f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42fcc000    # 126.375f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 80
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mWindUnder2Path:Landroid/graphics/Path;

    .line 81
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mWindUnder2Path:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 83
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mWindUnder2Path:Landroid/graphics/Path;

    const v1, 0x4304a000    # 132.625f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x43100000    # 144.0f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 84
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mWindUnder2Path:Landroid/graphics/Path;

    const v1, 0x4304a000    # 132.625f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x43100000    # 144.0f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4326a000    # 166.625f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x432dc000    # 173.75f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43502000    # 208.125f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43102000    # 144.125f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 87
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mWindUnder3Path:Landroid/graphics/Path;

    .line 88
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mWindUnder3Path:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 90
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mWindUnder3Path:Landroid/graphics/Path;

    const v1, 0x42807127    # 64.221f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43297917

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 91
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mWindUnder3Path:Landroid/graphics/Path;

    const v1, 0x42807127    # 64.221f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43297917

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x429f1e35

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x431d20c5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42c05581    # 96.167f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x431c2ac1

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 93
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mWindUnder3Path:Landroid/graphics/Path;

    const/high16 v1, 0x42cf0000    # 103.5f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x431c2000    # 156.125f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42e14625

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x431d5810

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const/high16 v5, 0x42fd0000    # 126.5f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43278000    # 167.5f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 95
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mWindUnder3Path:Landroid/graphics/Path;

    const v1, 0x4303a560

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432a96c9

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43088000    # 136.5f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x432f4000    # 175.25f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43182ac1

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x432fd53f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 98
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_left:Landroid/graphics/Path;

    .line 99
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_left:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 100
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_left:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v10

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v11

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 101
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x4257ba5e    # 53.932f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x429829fc    # 76.082f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42511168

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4297d26f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x424a4ac1

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4297d26f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 103
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x42156560    # 37.349f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4297d26f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x41d4ba5e    # 26.591f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42ad578d

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x41d4ba5e    # 26.591f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42c7c9ba

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 105
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x41d4ba5e    # 26.591f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42c83a5e

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x41d4c083    # 26.594f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42c8aa7f    # 100.333f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x41d4cccd    # 26.6f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42c91b23

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 107
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x418ef5c3    # 17.87f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42cc23d7    # 102.07f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x41335c29    # 11.21f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42daf6c9

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x41335c29    # 11.21f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42ed46a8    # 118.638f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 109
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x41335c29    # 11.21f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4300e24e

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x419c5c29    # 19.545f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4308ff7d

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x41ee53f8    # 29.791f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4308ff7d

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 111
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x42928831

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4308ff7d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 112
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x4296f2b0    # 75.474f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4308ffbe

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x429a87ae    # 77.265f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4307353f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x429a87ae    # 77.265f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4304ffbe

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 114
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x429a87ae    # 77.265f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4302ca3d    # 130.79f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4296f2b0    # 75.474f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v12

    const v5, 0x429287ae    # 73.265f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v12

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x41ee51ec    # 29.79f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v12

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 117
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x41bfa5e3    # 23.956f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v12

    const v3, 0x4199ac08    # 19.209f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42f8f2b0    # 124.474f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4199ac08    # 19.209f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42ed47ae    # 118.64f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 119
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x4199ac08    # 19.209f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42e19cac    # 112.806f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x41bfa5e3    # 23.956f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42d85687

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x41ee51ec    # 29.79f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42d85687

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 121
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x41f0eb85    # 30.115f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42d85687

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x41f37ae1    # 30.435f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42d87e77

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x41f6020c    # 30.751f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42d88c4a

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 123
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x4210db23    # 36.214f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42d98c4a

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 124
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x420bf9db    # 34.994f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42cee1cb

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 125
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x420ae666

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42cc86a8    # 102.263f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x420a5c29    # 34.59f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42ca24dd

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x420a5c29    # 34.59f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42c7c625

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 127
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x420a5c29    # 34.59f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42b625e3

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42270a3d    # 41.76f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42a7d168

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x424a49ba

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42a7d168

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 129
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x424e820c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a7d168

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4252a9fc    # 52.666f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42a8072b    # 84.014f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4256ba5e    # 53.682f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42a86e98

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 131
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x4256b53f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a87b64

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42688000    # 58.125f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42aa14fe    # 85.041f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42688000    # 58.125f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42aa14fe    # 85.041f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 133
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_left:Landroid/graphics/Path;

    const/high16 v1, 0x42700000    # 60.0f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x429b14fe    # 77.541f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 134
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_left:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v10

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v11

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 136
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_right:Landroid/graphics/Path;

    .line 137
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_right:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 138
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x431cfae1    # 156.98f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x429005a2

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 139
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x431cf4fe    # 156.957f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x429005a2

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431cef5c

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4290020c

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431ce937

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4290020c

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 141
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x431ce148    # 156.88f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4290020c

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431cd917

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42900396

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431cd0e5

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42900396

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 143
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x431cbd71    # 156.74f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42900312

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431ca9fc

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42900083    # 72.001f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431c9604

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42900083    # 72.001f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 145
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x431b7a1d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42900083    # 72.001f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431a5cee

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x428fe0c5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4319424e

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42901aa0

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 147
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x43194106

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4290147b    # 72.04f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43193f7d

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42900ed9    # 72.029f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43193e35

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x429008b4

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 149
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x43193e35

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x429008b4

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4314eac1

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const/high16 v4, 0x42910000    # 72.5f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4311374c

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x429354fe    # 73.666f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 151
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x431133b6

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4293570a    # 73.67f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43113aa0

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x429361cb

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43113c29    # 145.235f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x429368f6

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 153
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x4308daa0

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4298c49c

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4301922d    # 129.571f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42a3f5c3    # 81.98f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42f9624e

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42b3578d

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 155
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x42f70312

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42b7126f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42f81e35

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42bc12f2

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42fbd917

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42be70a4    # 95.22f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 157
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x42fd2d91

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42bf48b4

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42fea8f6    # 127.33f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42bfb74c

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x430010a4

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42bfb74c

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 159
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x43016396

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42bfb74c

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4302af1b

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42be6b02    # 95.209f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4303722d    # 131.446f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42bc0419

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 161
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x43079333

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42af0189

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x430de0c5

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42a5e76d

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431516c9

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42a20e56    # 81.028f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 163
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x43177efa

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a10d50    # 80.526f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 164
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x43181efa

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a0ca3d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4318c148

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42a09810

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431963d7    # 153.39f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42a06d0e

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 166
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x431a84dd

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a029fc    # 80.082f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431ba937

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42a0049c    # 80.009f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431cd168

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42a00312    # 80.006f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 168
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x432a574c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a04312

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43354e56    # 181.306f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42b65581    # 91.167f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43354e56    # 181.306f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42d170a4    # 104.72f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 170
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x43354e56    # 181.306f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42dac72b    # 109.389f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4333ff7d    # 179.998f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42e3ddb2

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x433186e9

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42ebb9db

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 172
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x43305958    # 176.349f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42ef77cf

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4330e9ba

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42f467f0

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4332c873

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42f6c28f    # 123.38f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 174
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x4334a6e9

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42f91d2f    # 124.557f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43371f7d

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42f7fc6a

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43384c8b

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42f43f7d    # 122.124f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 176
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x433b9333

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42e9d581    # 116.917f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x433d4e98

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42ddcbc7

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x433d4e98

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42d17127    # 104.721f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 178
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x433d4e14

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42ad9fbe

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x432ed74c

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42907333

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431cfae1    # 156.98f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x429005a2

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 181
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_top:Landroid/graphics/Path;

    .line 182
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_top:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 183
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x431b6a7f    # 155.416f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x429b8000    # 77.75f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 184
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x43193e35

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x429008b4

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 185
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x43112a7f    # 145.166f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42533646

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42fc8ed9    # 126.279f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v8

    const v5, 0x42d271aa    # 105.222f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v8

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 187
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x42d26f9e

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 188
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x42d26f1b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 189
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x42d2645a    # 105.196f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 190
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x42d0c7ae    # 104.39f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v8

    const v3, 0x42cf2e14    # 103.59f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42209062

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42cd9604    # 102.793f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4220b53f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 192
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x42cca24e

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4220cac1

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42cbb1aa    # 101.847f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x422129fc    # 40.291f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42cac083    # 101.376f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x422149ba

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 194
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x42ca4e56    # 101.153f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42215a1d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42c9dcac    # 100.931f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4221872b    # 40.382f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42c96b02    # 100.709f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42219ba6    # 40.402f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 196
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x42ae676d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4225ea7f    # 41.479f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x429721cb

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x423d8f5c    # 47.39f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42863439

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4262e148    # 56.72f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 198
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x4277de35    # 61.967f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4278ef9e

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4267f9db    # 57.994f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4289d687

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x425e70a4    # 55.61f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4298df3b

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 200
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x425e5f3b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4298ddb2

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x425e4ed9    # 55.577f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4298dc29    # 76.43f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v10

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v11

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 202
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_top:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v10

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v11

    const v3, 0x425b0f5c    # 54.765f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x429e8312

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x425a20c5

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42a07021

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 204
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x425925e3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a2753f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4256ae14    # 53.67f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42a8970a

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4256ae14    # 53.67f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42a8970a

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 206
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x425354fe    # 52.833f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42b121cb

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4251851f    # 52.38f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42ba1917

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4251851f    # 52.38f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42c37127    # 97.721f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 208
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x4251851f    # 52.38f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42c7dc29    # 99.93f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4258af1b

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42cb71aa    # 101.722f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4261851f    # 56.38f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42cb71aa    # 101.722f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 210
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x426a5b23    # 58.589f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42cb71aa    # 101.722f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4271851f    # 60.38f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42c7dcac    # 99.931f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4271851f    # 60.38f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42c371aa    # 97.722f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 212
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x4271851f    # 60.38f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a166e9

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42860ccd

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4284e6e9

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x429bb7cf

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4266b74c    # 57.679f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 214
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x42a8d78d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4251fdf4

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42b8f127    # 92.471f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4244cac1

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42ca6873

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4241ab02    # 48.417f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 216
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x42cad581    # 101.417f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42419893

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42cb4396

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42418937

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42cbb0a4

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x424179db    # 48.369f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 218
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x42cc872b    # 102.264f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42415917

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42cd5db2

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4240c083    # 48.188f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42ce35c3

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4240ad0e

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 220
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x42cf970a

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42408f5c    # 48.14f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42d0f9db

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v9

    const v5, 0x42d26148    # 105.19f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v9

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 222
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x42d27127    # 105.221f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v9

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 223
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x42f50396

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v9

    const v3, 0x430a0e56    # 138.056f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4268e24e    # 58.221f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4311374c

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42935581    # 73.667f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 225
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x43138a7f    # 147.541f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x429e9581    # 79.292f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 226
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x431b6a7f    # 155.416f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x429b8000    # 77.75f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 229
    return-void
.end method

.method private initPaint()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 232
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPaint:Landroid/graphics/Paint;

    .line 233
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 234
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPaintColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 235
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41400000    # 12.0f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 236
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 237
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 239
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPaint1:Landroid/graphics/Paint;

    .line 240
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPaint1:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 241
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPaint1:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 242
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPaint1:Landroid/graphics/Paint;

    const/high16 v1, 0x41000000    # 8.0f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 243
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPaint1:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPaintColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 244
    return-void
.end method


# virtual methods
.method public cancelAnimation()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 414
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->misActiveAnimationThread:Z

    .line 415
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 416
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    .line 418
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim1:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim1:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->setIsActivate(Z)V

    .line 419
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim2:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim2:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->setIsActivate(Z)V

    .line 420
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim3:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim3:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->setIsActivate(Z)V

    .line 421
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mTempBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_4

    .line 422
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mTempBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 423
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mTempBitmap:Landroid/graphics/Bitmap;

    .line 425
    :cond_4
    return-void
.end method

.method public getIsPalyAnimation()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 324
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim1:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim2:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim3:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    if-nez v1, :cond_1

    .line 328
    :cond_0
    :goto_0
    return v0

    .line 327
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim1:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->getIsActivate()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim2:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->getIsActivate()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim3:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    .line 328
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->getIsActivate()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getScale()F
    .locals 1

    .prologue
    .line 437
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 410
    return-object p0
.end method

.method public isRunning()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 428
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->getIsPalyAnimation()Z

    move-result v0

    return v0
.end method

.method public makeBitmap(I)V
    .locals 9
    .param p1, "delta"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 332
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim1:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    invoke-virtual {v4, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->animate2(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$ResultPathAlpha;

    move-result-object v0

    .line 333
    .local v0, "a1":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$ResultPathAlpha;
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim2:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    invoke-virtual {v4, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->animate2(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$ResultPathAlpha;

    move-result-object v1

    .line 334
    .local v1, "a2":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$ResultPathAlpha;
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim3:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    invoke-virtual {v4, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->animate2(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$ResultPathAlpha;

    move-result-object v2

    .line 336
    .local v2, "a3":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$ResultPathAlpha;
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v4}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v3

    .line 338
    .local v3, "dx":F
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mTempCanvas:Landroid/graphics/Canvas;

    const/4 v5, 0x0

    sget-object v6, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 339
    if-eqz v0, :cond_0

    .line 340
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPaint1:Landroid/graphics/Paint;

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$ResultPathAlpha;->alpha:I

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 341
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mTempCanvas:Landroid/graphics/Canvas;

    iget-object v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$ResultPathAlpha;->result:Landroid/graphics/Path;

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPaint1:Landroid/graphics/Paint;

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 344
    :cond_0
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim1:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->getIsActivate()Z

    move-result v4

    if-nez v4, :cond_1

    .line 345
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mTempCanvas:Landroid/graphics/Canvas;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mWindUnder1Path:Landroid/graphics/Path;

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPaint1:Landroid/graphics/Paint;

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 348
    :cond_1
    if-eqz v1, :cond_2

    .line 349
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPaint1:Landroid/graphics/Paint;

    iget v5, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$ResultPathAlpha;->alpha:I

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 350
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mTempCanvas:Landroid/graphics/Canvas;

    iget-object v5, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$ResultPathAlpha;->result:Landroid/graphics/Path;

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPaint1:Landroid/graphics/Paint;

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 352
    :cond_2
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim2:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->getIsActivate()Z

    move-result v4

    if-nez v4, :cond_3

    .line 353
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mTempCanvas:Landroid/graphics/Canvas;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mWindUnder2Path:Landroid/graphics/Path;

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPaint1:Landroid/graphics/Paint;

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 355
    :cond_3
    if-eqz v2, :cond_4

    .line 356
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPaint1:Landroid/graphics/Paint;

    iget v5, v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$ResultPathAlpha;->alpha:I

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 357
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mTempCanvas:Landroid/graphics/Canvas;

    iget-object v5, v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$ResultPathAlpha;->result:Landroid/graphics/Path;

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPaint1:Landroid/graphics/Paint;

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 359
    :cond_4
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim3:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->getIsActivate()Z

    move-result v4

    if-nez v4, :cond_5

    .line 360
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mTempCanvas:Landroid/graphics/Canvas;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mWindUnder3Path:Landroid/graphics/Path;

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPaint1:Landroid/graphics/Paint;

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 363
    :cond_5
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPaint1:Landroid/graphics/Paint;

    const/16 v5, 0xff

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 364
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mTempCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v4}, Landroid/graphics/Canvas;->save()I

    .line 365
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mTempCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v4, v3, v7}, Landroid/graphics/Canvas;->translate(FF)V

    .line 366
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mTempCanvas:Landroid/graphics/Canvas;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_left:Landroid/graphics/Path;

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 367
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mTempCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v4}, Landroid/graphics/Canvas;->restore()V

    .line 369
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mTempCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v4}, Landroid/graphics/Canvas;->save()I

    .line 370
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mTempCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v4, v7, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 371
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mTempCanvas:Landroid/graphics/Canvas;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_top:Landroid/graphics/Path;

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 372
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mTempCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v4}, Landroid/graphics/Canvas;->restore()V

    .line 374
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mTempCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v4}, Landroid/graphics/Canvas;->save()I

    .line 375
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mTempCanvas:Landroid/graphics/Canvas;

    neg-float v5, v3

    const v6, 0x43444560

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v6, v7

    const v7, 0x43101db2

    iget v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v7, v8

    invoke-virtual {v4, v5, v6, v7}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 376
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mTempCanvas:Landroid/graphics/Canvas;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_right:Landroid/graphics/Path;

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 377
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mTempCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v4}, Landroid/graphics/Canvas;->restore()V

    .line 378
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v2, 0x0

    .line 382
    :try_start_0
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_1

    .line 383
    const-string v1, ""

    const-string v2, "scale is less then 0"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 407
    :cond_0
    :goto_0
    return-void

    .line 387
    :cond_1
    iget-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->misActiveAnimationThread:Z

    if-eqz v1, :cond_2

    .line 388
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->update(I)V

    .line 389
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mTempBitmap:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 390
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->getIsPalyAnimation()Z

    move-result v1

    if-nez v1, :cond_0

    .line 391
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->cancelAnimation()V

    .line 392
    const-string v1, ""

    const-string v2, "cancelA"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 404
    :catch_0
    move-exception v0

    .line 405
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 395
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPaint1:Landroid/graphics/Paint;

    const/16 v2, 0xff

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 396
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_left:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 397
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_right:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 398
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_top:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 399
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mWindUnder1Path:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPaint1:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 400
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mWindUnder2Path:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPaint1:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 401
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mWindUnder3Path:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPaint1:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 585
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 552
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onViewDetachedFromWindow : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 553
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPaint:Landroid/graphics/Paint;

    .line 554
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPaint1:Landroid/graphics/Paint;

    .line 555
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_left:Landroid/graphics/Path;

    .line 556
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_right:Landroid/graphics/Path;

    .line 557
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mCloudPath_top:Landroid/graphics/Path;

    .line 558
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mWindUnder1Path:Landroid/graphics/Path;

    .line 559
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mWindUnder2Path:Landroid/graphics/Path;

    .line 560
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mWindUnder3Path:Landroid/graphics/Path;

    .line 561
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim1:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    if-eqz v0, :cond_0

    .line 562
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim1:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->setIconStop()V

    .line 565
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim2:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    if-eqz v0, :cond_1

    .line 566
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim2:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->setIconStop()V

    .line 569
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim3:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    if-eqz v0, :cond_2

    .line 570
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim3:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->setIconStop()V

    .line 572
    :cond_2
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim1:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    .line 573
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim2:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    .line 574
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim3:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    .line 575
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mTempCanvas:Landroid/graphics/Canvas;

    .line 576
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    .line 577
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mTempBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    .line 578
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mTempBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 580
    :cond_3
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mTempBitmap:Landroid/graphics/Bitmap;

    .line 581
    return-void
.end method

.method public setPaintColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 62
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPaintColor:I

    .line 63
    return-void
.end method

.method public setScale(F)V
    .locals 0
    .param p1, "scale"    # F

    .prologue
    .line 432
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    .line 433
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->init()V

    .line 434
    return-void
.end method

.method public setWidthAndHeight(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 441
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, p1, p2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 442
    .local v0, "l":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 444
    return-void
.end method

.method public startAnimation()V
    .locals 7

    .prologue
    const/16 v6, 0x1e

    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 303
    iput-boolean v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->isStop:Z

    .line 304
    new-array v0, v5, [F

    const v1, -0x3fd33333    # -2.7f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v2

    aput v1, v0, v4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    aput v2, v0, v1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    .line 305
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 306
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v5}, Landroid/animation/ValueAnimator;->setRepeatMode(I)V

    .line 307
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 308
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 311
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    const/16 v1, 0x1b

    invoke-direct {v0, p0, v4, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;II)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim1:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    .line 312
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    const/4 v1, 0x5

    invoke-direct {v0, p0, v1, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;II)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim2:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    .line 313
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    const/16 v1, 0x14

    invoke-direct {v0, p0, v1, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;II)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim3:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    .line 314
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim1:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mWindUnder1Path:Landroid/graphics/Path;

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->setPath(Landroid/graphics/Path;)V

    .line 315
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim2:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mWindUnder2Path:Landroid/graphics/Path;

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->setPath(Landroid/graphics/Path;)V

    .line 316
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim3:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mWindUnder3Path:Landroid/graphics/Path;

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->setPath(Landroid/graphics/Path;)V

    .line 317
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim1:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->restartAnimation()V

    .line 318
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim2:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->restartAnimation()V

    .line 319
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim3:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->restartAnimation()V

    .line 320
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->startAnimationThread()V

    .line 321
    return-void
.end method

.method public startAnimationThread()V
    .locals 4

    .prologue
    const/high16 v3, 0x43960000    # 300.0f

    .line 271
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mTempBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 272
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mTempBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 273
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mTempBitmap:Landroid/graphics/Bitmap;

    .line 275
    :cond_0
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mTempBitmap:Landroid/graphics/Bitmap;

    .line 277
    new-instance v1, Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mTempBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v1, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mTempCanvas:Landroid/graphics/Canvas;

    .line 279
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->misActiveAnimationThread:Z

    .line 280
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;)V

    .line 294
    .local v0, "t":Ljava/lang/Thread;
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 295
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 296
    return-void
.end method

.method public stopAnimation()V
    .locals 1

    .prologue
    .line 248
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->isStop:Z

    .line 249
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim1:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    if-eqz v0, :cond_0

    .line 250
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim1:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->setIconStop()V

    .line 253
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim2:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    if-eqz v0, :cond_1

    .line 254
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim2:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->setIconStop()V

    .line 257
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim3:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    if-eqz v0, :cond_2

    .line 258
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->mPathMaskAnim3:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->setIconStop()V

    .line 260
    :cond_2
    return-void
.end method

.method public update(I)V
    .locals 0
    .param p1, "delta"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 299
    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;->makeBitmap(I)V

    .line 300
    return-void
.end method
