.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
.super Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
.source "TodayWeatherInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field mCurrentTemp:F

.field mDayHailAmount:Ljava/lang/Double;

.field mDayHailProbability:I

.field mDayIcon:I

.field mDayPrecipitationAmount:Ljava/lang/Double;

.field mDayPrecipitationProbability:I

.field mDayRainAmount:Ljava/lang/Double;

.field mDayRainProbability:I

.field mDaySnowAmount:Ljava/lang/Double;

.field mDaySnowProbability:I

.field mNightHailAmount:Ljava/lang/Double;

.field mNightHailProbability:I

.field mNightIcon:I

.field mNightPrecipitationAmount:Ljava/lang/Double;

.field mNightPrecipitationProbability:I

.field mNightRainAmount:Ljava/lang/Double;

.field mNightRainProbability:I

.field mNightSnowAmount:Ljava/lang/Double;

.field mNightSnowProbability:I

.field mRealFeel:F

.field mRelativeHumidity:Ljava/lang/String;

.field mSummerTime:Ljava/lang/String;

.field mSunRiseTime:Ljava/lang/String;

.field mSunSetTime:Ljava/lang/String;

.field mTimeZone:Ljava/lang/String;

.field mUVIndex:I

.field mUVIndexText:Ljava/lang/String;

.field mUpdateDate:Ljava/lang/String;

.field mUrl:Ljava/lang/String;

.field mWeatherText:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 432
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo$1;

    invoke-direct {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo$1;-><init>()V

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;-><init>()V

    .line 70
    return-void
.end method

.method public constructor <init>(IFFIFLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "tempScale"    # I
    .param p2, "lowTemp"    # F
    .param p3, "highTemp"    # F
    .param p4, "iconNum"    # I
    .param p5, "currentTemp"    # F
    .param p6, "timezone"    # Ljava/lang/String;
    .param p7, "weathertext"    # Ljava/lang/String;
    .param p8, "url"    # Ljava/lang/String;

    .prologue
    .line 74
    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p8

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;-><init>(IFFILjava/lang/String;)V

    .line 75
    iput p5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mCurrentTemp:F

    .line 76
    iput-object p6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mTimeZone:Ljava/lang/String;

    .line 77
    iput-object p7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mWeatherText:Ljava/lang/String;

    .line 78
    iput-object p8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mUrl:Ljava/lang/String;

    .line 79
    return-void
.end method

.method public constructor <init>(ILcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;FLjava/lang/String;)V
    .locals 9
    .param p1, "tempScale"    # I
    .param p2, "generalInfo"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .param p3, "currentTemp"    # F
    .param p4, "timezone"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 83
    invoke-virtual {p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v2

    invoke-virtual {p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v3

    .line 84
    invoke-virtual {p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v4

    move-object v0, p0

    move v1, p1

    move v5, p3

    move-object v6, p4

    move-object v8, v7

    .line 83
    invoke-direct/range {v0 .. v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;-><init>(IFFIFLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    return-void
.end method

.method public constructor <init>(ILcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;FLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1, "tempScale"    # I
    .param p2, "generalInfo"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .param p3, "currentTemp"    # F
    .param p4, "timezone"    # Ljava/lang/String;
    .param p5, "weathertext"    # Ljava/lang/String;
    .param p6, "url"    # Ljava/lang/String;

    .prologue
    .line 89
    invoke-virtual {p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v2

    invoke-virtual {p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v3

    .line 90
    invoke-virtual {p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v4

    move-object v0, p0

    move v1, p1

    move v5, p3

    move-object v6, p4

    move-object v7, p5

    move-object v8, p6

    .line 89
    invoke-direct/range {v0 .. v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;-><init>(IFFIFLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    return-void
.end method

.method public constructor <init>(ILcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;FLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1, "tempScale"    # I
    .param p2, "generalInfo"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .param p3, "currentTemp"    # F
    .param p4, "timezone"    # Ljava/lang/String;
    .param p5, "summertime"    # Ljava/lang/String;
    .param p6, "weathertext"    # Ljava/lang/String;
    .param p7, "url"    # Ljava/lang/String;

    .prologue
    .line 95
    invoke-virtual {p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v2

    invoke-virtual {p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v3

    .line 96
    invoke-virtual {p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v4

    move-object v0, p0

    move v1, p1

    move v5, p3

    move-object v6, p4

    move-object v7, p6

    move-object/from16 v8, p7

    .line 95
    invoke-direct/range {v0 .. v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;-><init>(IFFIFLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 392
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;-><init>()V

    .line 393
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mCurrentTemp:F

    .line 394
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mHighTemp:F

    .line 395
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mLowTemp:F

    .line 396
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mTimeZone:Ljava/lang/String;

    .line 397
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mUpdateDate:Ljava/lang/String;

    .line 398
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mSummerTime:Ljava/lang/String;

    .line 399
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mWeatherText:Ljava/lang/String;

    .line 400
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mUrl:Ljava/lang/String;

    .line 401
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mRealFeel:F

    .line 402
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mSunRiseTime:Ljava/lang/String;

    .line 403
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mSunSetTime:Ljava/lang/String;

    .line 404
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mRelativeHumidity:Ljava/lang/String;

    .line 405
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mUVIndex:I

    .line 406
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mUVIndexText:Ljava/lang/String;

    .line 407
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDayIcon:I

    .line 408
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightIcon:I

    .line 409
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mOnedayDayIcon:I

    .line 410
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mOnedayNightIcon:I

    .line 412
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDayRainProbability:I

    .line 413
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDaySnowProbability:I

    .line 414
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDayHailProbability:I

    .line 415
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDayPrecipitationProbability:I

    .line 416
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDayRainAmount:Ljava/lang/Double;

    .line 417
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDaySnowAmount:Ljava/lang/Double;

    .line 418
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDayHailAmount:Ljava/lang/Double;

    .line 419
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDayPrecipitationAmount:Ljava/lang/Double;

    .line 421
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightRainProbability:I

    .line 422
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightSnowProbability:I

    .line 423
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightHailProbability:I

    .line 424
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightPrecipitationProbability:I

    .line 425
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightRainAmount:Ljava/lang/Double;

    .line 426
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightSnowAmount:Ljava/lang/Double;

    .line 427
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightHailAmount:Ljava/lang/Double;

    .line 428
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightPrecipitationAmount:Ljava/lang/Double;

    .line 430
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 172
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public getCurrentTemp()F
    .locals 1

    .prologue
    .line 116
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mCurrentTemp:F

    return v0
.end method

.method public getDayHailAmount()Ljava/lang/Double;
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDayHailAmount:Ljava/lang/Double;

    return-object v0
.end method

.method public getDayHailProbability()I
    .locals 1

    .prologue
    .line 269
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDayHailProbability:I

    return v0
.end method

.method public getDayIcon()I
    .locals 1

    .prologue
    .line 337
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDayIcon:I

    return v0
.end method

.method public getDayPrecipitationAmount()Ljava/lang/Double;
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDayPrecipitationAmount:Ljava/lang/Double;

    return-object v0
.end method

.method public getDayPrecipitationProbability()I
    .locals 1

    .prologue
    .line 277
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDayPrecipitationProbability:I

    return v0
.end method

.method public getDayRainAmount()Ljava/lang/Double;
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDayRainAmount:Ljava/lang/Double;

    return-object v0
.end method

.method public getDayRainProbability()I
    .locals 1

    .prologue
    .line 253
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDayRainProbability:I

    return v0
.end method

.method public getDaySnowAmount()Ljava/lang/Double;
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDaySnowAmount:Ljava/lang/Double;

    return-object v0
.end method

.method public getDaySnowProbability()I
    .locals 1

    .prologue
    .line 261
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDaySnowProbability:I

    return v0
.end method

.method public getHighTemp()F
    .locals 1

    .prologue
    .line 124
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mHighTemp:F

    return v0
.end method

.method public getNightHailAmount()Ljava/lang/Double;
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightHailAmount:Ljava/lang/Double;

    return-object v0
.end method

.method public getNightHailProbability()I
    .locals 1

    .prologue
    .line 205
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightHailProbability:I

    return v0
.end method

.method public getNightIcon()I
    .locals 1

    .prologue
    .line 345
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightIcon:I

    return v0
.end method

.method public getNightPrecipitationAmount()Ljava/lang/Double;
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightPrecipitationAmount:Ljava/lang/Double;

    return-object v0
.end method

.method public getNightPrecipitationProbability()I
    .locals 1

    .prologue
    .line 213
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightPrecipitationProbability:I

    return v0
.end method

.method public getNightRainAmount()Ljava/lang/Double;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightRainAmount:Ljava/lang/Double;

    return-object v0
.end method

.method public getNightRainProbability()I
    .locals 1

    .prologue
    .line 189
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightRainProbability:I

    return v0
.end method

.method public getNightSnowAmount()Ljava/lang/Double;
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightSnowAmount:Ljava/lang/Double;

    return-object v0
.end method

.method public getNightSnowProbability()I
    .locals 1

    .prologue
    .line 197
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightSnowProbability:I

    return v0
.end method

.method public getRealFeel()F
    .locals 1

    .prologue
    .line 176
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mRealFeel:F

    return v0
.end method

.method public getRelativeHumidity()Ljava/lang/String;
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mRelativeHumidity:Ljava/lang/String;

    return-object v0
.end method

.method public getSummerTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mSummerTime:Ljava/lang/String;

    return-object v0
.end method

.method public getSunRiseTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mSunRiseTime:Ljava/lang/String;

    return-object v0
.end method

.method public getSunSetTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mSunSetTime:Ljava/lang/String;

    return-object v0
.end method

.method public getTimeZone()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mTimeZone:Ljava/lang/String;

    return-object v0
.end method

.method public getUVIndex()I
    .locals 1

    .prologue
    .line 321
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mUVIndex:I

    return v0
.end method

.method public getUVIndexText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 329
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mUVIndexText:Ljava/lang/String;

    return-object v0
.end method

.method public getUpdateDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mUpdateDate:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getWeatherText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mWeatherText:Ljava/lang/String;

    return-object v0
.end method

.method public setCurrentTemp(F)V
    .locals 0
    .param p1, "temperature"    # F

    .prologue
    .line 120
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mCurrentTemp:F

    .line 121
    return-void
.end method

.method public setDayHailAmount(D)V
    .locals 1
    .param p1, "hailamount"    # D

    .prologue
    .line 297
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDayHailAmount:Ljava/lang/Double;

    .line 298
    return-void
.end method

.method public setDayHailProbability(I)V
    .locals 0
    .param p1, "hailprob"    # I

    .prologue
    .line 265
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDayHailProbability:I

    .line 266
    return-void
.end method

.method public setDayIcon(I)V
    .locals 0
    .param p1, "mTodayDayIcon"    # I

    .prologue
    .line 341
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDayIcon:I

    .line 342
    return-void
.end method

.method public setDayPrecipitationAmount(D)V
    .locals 1
    .param p1, "precipitationamount"    # D

    .prologue
    .line 305
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDayPrecipitationAmount:Ljava/lang/Double;

    .line 306
    return-void
.end method

.method public setDayPrecipitationProbability(I)V
    .locals 0
    .param p1, "precipitationprob"    # I

    .prologue
    .line 273
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDayPrecipitationProbability:I

    .line 274
    return-void
.end method

.method public setDayRainAmount(D)V
    .locals 1
    .param p1, "rainamount"    # D

    .prologue
    .line 281
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDayRainAmount:Ljava/lang/Double;

    .line 282
    return-void
.end method

.method public setDayRainProbability(I)V
    .locals 0
    .param p1, "rainprob"    # I

    .prologue
    .line 249
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDayRainProbability:I

    .line 250
    return-void
.end method

.method public setDaySnowAmount(D)V
    .locals 1
    .param p1, "snowamount"    # D

    .prologue
    .line 289
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDaySnowAmount:Ljava/lang/Double;

    .line 290
    return-void
.end method

.method public setDaySnowProbability(I)V
    .locals 0
    .param p1, "snowprob"    # I

    .prologue
    .line 257
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDaySnowProbability:I

    .line 258
    return-void
.end method

.method public setLowTemp(F)V
    .locals 0
    .param p1, "temperature"    # F

    .prologue
    .line 128
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mLowTemp:F

    .line 129
    return-void
.end method

.method public setNightHailAmount(D)V
    .locals 1
    .param p1, "hailamount"    # D

    .prologue
    .line 233
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightHailAmount:Ljava/lang/Double;

    .line 234
    return-void
.end method

.method public setNightHailProbability(I)V
    .locals 0
    .param p1, "hailprob"    # I

    .prologue
    .line 201
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightHailProbability:I

    .line 202
    return-void
.end method

.method public setNightIcon(I)V
    .locals 0
    .param p1, "mTodayNightIcon"    # I

    .prologue
    .line 349
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightIcon:I

    .line 350
    return-void
.end method

.method public setNightPrecipitationAmount(D)V
    .locals 1
    .param p1, "precipitationamount"    # D

    .prologue
    .line 241
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightPrecipitationAmount:Ljava/lang/Double;

    .line 242
    return-void
.end method

.method public setNightPrecipitationProbability(I)V
    .locals 0
    .param p1, "precipitationprob"    # I

    .prologue
    .line 209
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightPrecipitationProbability:I

    .line 210
    return-void
.end method

.method public setNightRainAmount(D)V
    .locals 1
    .param p1, "rainamount"    # D

    .prologue
    .line 217
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightRainAmount:Ljava/lang/Double;

    .line 218
    return-void
.end method

.method public setNightRainProbability(I)V
    .locals 0
    .param p1, "rainprob"    # I

    .prologue
    .line 185
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightRainProbability:I

    .line 186
    return-void
.end method

.method public setNightSnowAmount(D)V
    .locals 1
    .param p1, "snowamount"    # D

    .prologue
    .line 225
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightSnowAmount:Ljava/lang/Double;

    .line 226
    return-void
.end method

.method public setNightSnowProbability(I)V
    .locals 0
    .param p1, "snowprob"    # I

    .prologue
    .line 193
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightSnowProbability:I

    .line 194
    return-void
.end method

.method public setRealFeel(F)V
    .locals 0
    .param p1, "realfeel"    # F

    .prologue
    .line 180
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mRealFeel:F

    .line 181
    return-void
.end method

.method public setRelativeHumidity(Ljava/lang/String;)V
    .locals 0
    .param p1, "mRelativeHumidity"    # Ljava/lang/String;

    .prologue
    .line 317
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mRelativeHumidity:Ljava/lang/String;

    .line 318
    return-void
.end method

.method public setSummerTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "summertime"    # Ljava/lang/String;

    .prologue
    .line 152
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mSummerTime:Ljava/lang/String;

    .line 153
    return-void
.end method

.method public setSunRiseTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "time"    # Ljava/lang/String;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mSunRiseTime:Ljava/lang/String;

    .line 105
    return-void
.end method

.method public setSunSetTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "time"    # Ljava/lang/String;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mSunSetTime:Ljava/lang/String;

    .line 113
    return-void
.end method

.method public setTimeZone(Ljava/lang/String;)V
    .locals 0
    .param p1, "timezone"    # Ljava/lang/String;

    .prologue
    .line 136
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mTimeZone:Ljava/lang/String;

    .line 137
    return-void
.end method

.method public setUVIndex(I)V
    .locals 0
    .param p1, "mUVIndex"    # I

    .prologue
    .line 325
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mUVIndex:I

    .line 326
    return-void
.end method

.method public setUVIndexText(Ljava/lang/String;)V
    .locals 0
    .param p1, "mUVIndexText"    # Ljava/lang/String;

    .prologue
    .line 333
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mUVIndexText:Ljava/lang/String;

    .line 334
    return-void
.end method

.method public setUpdateDate(Ljava/lang/String;)V
    .locals 0
    .param p1, "timestamp"    # Ljava/lang/String;

    .prologue
    .line 144
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mUpdateDate:Ljava/lang/String;

    .line 145
    return-void
.end method

.method public setUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 168
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mUrl:Ljava/lang/String;

    .line 169
    return-void
.end method

.method public setWeatherText(Ljava/lang/String;)V
    .locals 0
    .param p1, "mWeatherText"    # Ljava/lang/String;

    .prologue
    .line 160
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mWeatherText:Ljava/lang/String;

    .line 161
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 353
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mCurrentTemp:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 354
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mHighTemp:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 355
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mLowTemp:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 356
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mTimeZone:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 357
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mUpdateDate:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 358
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mSummerTime:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 359
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mWeatherText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 360
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 361
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mRealFeel:F

    float-to-double v0, v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 362
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mSunRiseTime:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 363
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mSunSetTime:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 364
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mRelativeHumidity:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 365
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mUVIndex:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 366
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mUVIndexText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 367
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDayIcon:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 368
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightIcon:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 369
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mOnedayDayIcon:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 370
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mOnedayNightIcon:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 372
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDayRainProbability:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 373
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDaySnowProbability:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 374
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDayHailProbability:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 375
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDayPrecipitationProbability:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 376
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDayRainAmount:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 377
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDaySnowAmount:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 378
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDayHailAmount:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 379
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mDayPrecipitationAmount:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 381
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightRainProbability:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 382
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightSnowProbability:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 383
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightHailProbability:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 384
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightPrecipitationProbability:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 385
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightRainAmount:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 386
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightSnowAmount:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 387
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightHailAmount:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 388
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->mNightPrecipitationAmount:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 390
    return-void
.end method
