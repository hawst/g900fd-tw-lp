.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;
.super Ljava/lang/Object;
.source "DaemonInterface.java"


# static fields
.field public static sendPakageNames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 173
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;->sendPakageNames:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkDaemonServerApp(Landroid/content/Context;)Ljava/lang/Boolean;
    .locals 4
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 162
    const-string v2, ""

    const-string v3, "DmSvApp !!!!"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 165
    .local v1, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    const-string v2, "com.sec.android.daemonapp"

    const/16 v3, 0x80

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 170
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    :goto_0
    return-object v2

    .line 166
    :catch_0
    move-exception v0

    .line 167
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v2, ""

    const-string v3, "Don`t have Daemon Server : Not Found!!!!"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0
.end method

.method public static checkSameDaemonCityIdAtDate(Landroid/content/Context;)V
    .locals 10
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x1

    .line 70
    const/4 v1, 0x0

    .line 71
    .local v1, "cityId":Ljava/lang/String;
    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 72
    .local v3, "flag":Ljava/lang/Boolean;
    const/4 v2, 0x0

    .line 74
    .local v2, "citylist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;>;"
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;->getSettingDaemonCityId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 75
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkLanucherMode(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 76
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getCurrentCity(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v2

    .line 81
    :goto_0
    if-eqz v1, :cond_5

    const-string v4, ""

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 82
    const-string v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ChkDmCt = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    .line 84
    .local v0, "city":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;
    const-string v5, ""

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "=== Widget CURRENT_LOCATION= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 85
    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v7

    const-string v8, "cityId:current"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 84
    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v5

    const-string v6, "cityId:current"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 87
    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getRealLocation()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 88
    invoke-static {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;->setDaemonUpdateDate(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;)V

    .line 89
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 101
    .end local v0    # "city":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;
    :cond_1
    :goto_1
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-nez v4, :cond_2

    .line 102
    invoke-static {v2, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;->doDaemonUpdateDate(Ljava/util/ArrayList;Landroid/content/Context;)V

    .line 108
    :cond_2
    :goto_2
    return-void

    .line 78
    :cond_3
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getAllCityList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v2

    goto :goto_0

    .line 93
    .restart local v0    # "city":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;
    :cond_4
    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 94
    invoke-static {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;->setDaemonUpdateDate(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;)V

    .line 95
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 96
    goto :goto_1

    .line 105
    .end local v0    # "city":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;
    :cond_5
    const-string v4, ""

    const-string v5, "D`t Ct Dm!!!!!!!!!!!"

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    invoke-static {v2, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;->doDaemonUpdateDate(Ljava/util/ArrayList;Landroid/content/Context;)V

    goto :goto_2
.end method

.method public static checkSameDaemonCityIdAtStting(Landroid/content/Context;Landroid/preference/Preference;)V
    .locals 5
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 138
    const/4 v0, 0x0

    .line 139
    .local v0, "cityId":Ljava/lang/String;
    const/4 v1, 0x0

    .line 141
    .local v1, "citylist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;>;"
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;->getSettingDaemonCityId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 143
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkLanucherMode(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 144
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getCurrentCity(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v1

    .line 149
    :goto_0
    if-eqz v0, :cond_2

    .line 150
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cityId = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    :goto_1
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 156
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "list.size : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    invoke-static {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;->setDaemonUpdateSetting(Landroid/content/Context;Landroid/preference/Preference;)V

    .line 159
    :cond_0
    return-void

    .line 147
    :cond_1
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getAllCityList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_0

    .line 152
    :cond_2
    const-string v2, ""

    const-string v3, "cityId is null!!! "

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private static doDaemonUpdateDate(Ljava/util/ArrayList;Landroid/content/Context;)V
    .locals 9
    .param p1, "ctx"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, "citylist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;>;"
    const/4 v8, 0x1

    .line 111
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-eqz v4, :cond_0

    .line 112
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    .line 113
    .local v2, "city":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;
    invoke-static {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getCheckCurrentCityLocation(Landroid/content/Context;)I

    move-result v1

    .line 114
    .local v1, "checkCurrentLoc":I
    const-string v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "=== widget CURRENT_LOCATION= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 116
    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v6

    const-string v7, "cityId:current"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 114
    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v4

    const-string v5, "cityId:current"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 118
    invoke-static {p1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;->setDaemonUpdateDate(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;)V

    .line 135
    .end local v1    # "checkCurrentLoc":I
    .end local v2    # "city":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;
    :cond_0
    :goto_0
    return-void

    .line 120
    .restart local v1    # "checkCurrentLoc":I
    .restart local v2    # "city":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;
    :cond_1
    if-ne v1, v8, :cond_0

    .line 121
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 122
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v4, "KEY_UPDATE_DAEMON_FROM_WIDGET"

    const-string v5, "widget"

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.sec.android.widgetapp.ap.accuweatherdaemon.action.CURRENT_LOCATION_WEATHER_DATA"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 125
    .local v3, "intent":Landroid/content/Intent;
    const-string v4, "PACKAGE"

    const-string v5, "com.sec.android.widgetapp.ap.hero.accuweather"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 127
    const-string v4, "START"

    invoke-virtual {v3, v4, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 128
    const-string v4, "CP"

    const-string v5, "Accuweather"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 129
    invoke-virtual {v3, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 130
    const-string v4, "com.sec.android.daemonapp"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 131
    invoke-virtual {p1, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static getDaemonStatus(Landroid/content/Context;)I
    .locals 5
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 176
    const/4 v0, 0x0

    .line 177
    .local v0, "Service_Status":I
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getAllPackageNameList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v2

    sput-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;->sendPakageNames:Ljava/util/ArrayList;

    .line 178
    sget-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;->sendPakageNames:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;->sendPakageNames:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 179
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "PakNme size = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;->sendPakageNames:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    const/4 v0, 0x1

    move v1, v0

    .line 183
    .end local v0    # "Service_Status":I
    .local v1, "Service_Status":I
    :goto_0
    return v1

    .end local v1    # "Service_Status":I
    .restart local v0    # "Service_Status":I
    :cond_0
    move v1, v0

    .end local v0    # "Service_Status":I
    .restart local v1    # "Service_Status":I
    goto :goto_0
.end method

.method public static getSettingDaemonCityId(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 241
    const/4 v0, 0x0

    .line 242
    .local v0, "CityId":Ljava/lang/String;
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getRealLocation(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 243
    if-eqz v0, :cond_0

    .line 247
    .end local v0    # "CityId":Ljava/lang/String;
    :goto_0
    return-object v0

    .restart local v0    # "CityId":Ljava/lang/String;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isWaitingDaemonRefreshTime(Landroid/content/Context;)Z
    .locals 15
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v11, 0x0

    .line 194
    const-string v12, ""

    const-string v13, "iWDRT !!!!"

    invoke-static {v12, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    if-nez p0, :cond_1

    .line 236
    :cond_0
    :goto_0
    return v11

    .line 201
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    const-string v13, "aw_daemon_service_key_autorefresh_next_time"

    invoke-static {v12, v13}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;)J

    move-result-wide v9

    .line 203
    .local v9, "refreshTime":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    .line 205
    .local v7, "now":J
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    .line 206
    .local v4, "cal":Ljava/util/Calendar;
    invoke-virtual {v4, v7, v8}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 207
    const/16 v12, 0xc

    const/4 v13, 0x2

    invoke-virtual {v4, v12, v13}, Ljava/util/Calendar;->add(II)V

    .line 208
    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    .line 210
    .local v0, "after2Min":J
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    .line 211
    .local v5, "cal2":Ljava/util/Calendar;
    invoke-virtual {v5, v7, v8}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 212
    const/16 v12, 0xc

    const/4 v13, -0x2

    invoke-virtual {v5, v12, v13}, Ljava/util/Calendar;->add(II)V

    .line 213
    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    .line 215
    .local v2, "before2Min":J
    const-string v12, ""

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "iWDRT [r/n/a/b] = ["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "]"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 227
    cmp-long v12, v9, v7

    if-ltz v12, :cond_2

    cmp-long v12, v9, v0

    if-lez v12, :cond_3

    :cond_2
    cmp-long v12, v9, v7

    if-gtz v12, :cond_0

    cmp-long v12, v9, v2

    if-ltz v12, :cond_0

    .line 230
    :cond_3
    const/4 v11, 0x1

    goto :goto_0

    .line 234
    .end local v0    # "after2Min":J
    .end local v2    # "before2Min":J
    .end local v4    # "cal":Ljava/util/Calendar;
    .end local v5    # "cal2":Ljava/util/Calendar;
    .end local v7    # "now":J
    .end local v9    # "refreshTime":J
    :catch_0
    move-exception v6

    .line 235
    .local v6, "e1":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v6}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto/16 :goto_0
.end method

.method public static notifyAgreementToDaemon(Landroid/content/Context;Z)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "isOn"    # Z

    .prologue
    .line 282
    const-string v1, ""

    const-string v2, "notifyAgreementToDaemon"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.widgetapp.ap.accuweatherdaemon.action.WEATHER_SHOW_USER_CURRENTLOCATION_POPUP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 284
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.daemonapp"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 285
    const-string v1, "IS_AGREEMENT_ON"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 286
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 287
    return-void
.end method

.method public static sendDate(Landroid/content/Context;)V
    .locals 3
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 32
    const-string v1, ""

    const-string v2, "sndDateDm !!!!"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.widgetapp.ap.accuweatherdaemon.action.SYNC_DATA_WITH_WIDGET"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 34
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.daemonapp"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 35
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 36
    return-void
.end method

.method public static sendSetting(Landroid/content/Context;)V
    .locals 3
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 25
    const-string v1, ""

    const-string v2, "sndSetDm !!!!"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 26
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.widgetapp.ap.accuweatherdaemon.action.SYNC_SETTING_WITH_WIDGET"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 27
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.daemonapp"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 28
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 29
    return-void
.end method

.method public static setDaemonAccuRefresh(Landroid/content/Context;I)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "value"    # I

    .prologue
    .line 251
    invoke-static {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateAutoRefreshTime(Landroid/content/Context;I)I

    .line 253
    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getNextTime(Landroid/content/Context;ZZ)J

    move-result-wide v0

    .line 254
    .local v0, "nextTime":J
    invoke-static {p0, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateNextTime(Landroid/content/Context;J)I

    .line 255
    return-void
.end method

.method public static setDaemonTemp(Landroid/content/Context;I)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "value"    # I

    .prologue
    const/4 v2, 0x1

    .line 258
    if-ne p1, v2, :cond_0

    move v1, v2

    :goto_0
    invoke-static {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateTempScale(Landroid/content/Context;I)I

    .line 261
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.widgetapp.ap.hero.accuweather.action.CHANGE_SETTING"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 262
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "SEND_DAEMON_SETTING_UPDATE_WIDGET"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 263
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 264
    return-void

    .line 258
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static setDaemonUpdateDate(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;)V
    .locals 3
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "city"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    .prologue
    .line 39
    const-string v1, ""

    const-string v2, "setDmUdD !!!!"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getDetailWeatherInfo(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    move-result-object v0

    .line 42
    .local v0, "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;->sendSetting(Landroid/content/Context;)V

    .line 43
    if-eqz v0, :cond_0

    .line 44
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;->sendDate(Landroid/content/Context;)V

    .line 46
    :cond_0
    return-void
.end method

.method public static setDaemonUpdateSetting(Landroid/content/Context;Landroid/preference/Preference;)V
    .locals 7
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 49
    const-string v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setDmUdSet !!!! preference:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    const/4 v3, 0x0

    .local v3, "tempScale":I
    const/4 v0, 0x0

    .line 52
    .local v0, "autoRefresh":I
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getTempScaleSetting(Landroid/content/Context;)I

    move-result v3

    .line 53
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getAutoRefreshTime(Landroid/content/Context;)I

    move-result v0

    .line 55
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 57
    .local v1, "bundle":Landroid/os/Bundle;
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v4

    const-string v5, "unit"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 58
    const-string v4, "TEMPSCALE"

    invoke-virtual {v1, v4, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 63
    :cond_0
    :goto_0
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.sec.android.widgetapp.ap.accuweatherdaemon.action.SYNC_SETTING_WITH_WIDGET"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 64
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {v2, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 65
    const-string v4, "com.sec.android.daemonapp"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 66
    invoke-virtual {p0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 67
    return-void

    .line 59
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_1
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v4

    const-string v5, "autorefresh"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 60
    const-string v4, "AUTOREFRESH"

    invoke-virtual {v1, v4, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public static setWidgetAtDaemon(Landroid/content/Context;ZZ)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "isEnable"    # Z
    .param p2, "autorefreshSet"    # Z

    .prologue
    .line 272
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setWidgetAtDaemon() isEnable : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", autorefreshSet : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.widgetapp.ap.accuweatherdaemon.action.CURRENT_LOCATION_WEATHER_DATA"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 274
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "START"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 275
    const-string v1, "PACKAGE"

    const-string v2, "com.sec.android.widgetapp.ap.hero.accuweather"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 276
    const-string v1, "CP"

    const-string v2, "Accuweather"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 277
    const-string v1, "AUTOREFRESHSET"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 278
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 279
    return-void
.end method
