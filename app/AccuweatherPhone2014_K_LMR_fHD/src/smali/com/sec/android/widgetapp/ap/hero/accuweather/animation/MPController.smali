.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;
.super Ljava/lang/Object;
.source "MPController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;
    }
.end annotation


# static fields
.field public static final FADE_IN_ANIM:I = 0x64

.field public static final FADE_OUT_ANIM:I = 0xc8

.field public static final INVAILD:I = -0x1

.field private static final MAX_BG_RES_SIZE:I = 0x10

.field public static final PAUSE:I = 0x4

.field public static final PLAY:I = 0x2

.field public static final PLAY_AFTER_FLICK:I = 0x3

.field public static final READY:I = 0x5

.field public static final STOP:I = 0x0

.field public static final STOP_BY_FLICK:I = 0x1


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCurrentPos:I

.field private mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private mIsLoadCompletedRes:Z

.field private mIsPlayingFadeOutAnim:Z

.field private mIsVisibleActivity:Z

.field private mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

.field private mMPCompleteListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field public mMPHandler:Landroid/os/Handler;

.field private mMPPrepareListener:Landroid/media/MediaPlayer$OnPreparedListener;

.field private mMPResUri:Landroid/net/Uri;

.field private mMPState:I

.field private mMPView:Landroid/widget/VideoView;

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private mViEffectSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

.field private mVideofileName:[Ljava/lang/String;

.field private mWeatherVideoBGUri:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "settingsViEffect"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    .prologue
    const/4 v3, 0x1

    const/16 v6, 0x10

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mContext:Landroid/content/Context;

    .line 26
    iput-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPView:Landroid/widget/VideoView;

    .line 28
    iput-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPResUri:Landroid/net/Uri;

    .line 48
    iput v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPState:I

    .line 53
    new-array v1, v6, [Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mWeatherVideoBGUri:[Ljava/lang/String;

    .line 55
    iput-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mViEffectSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    .line 57
    iput-boolean v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mIsLoadCompletedRes:Z

    .line 59
    iput-boolean v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mIsVisibleActivity:Z

    .line 61
    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "weather_2nd_bg_rain"

    aput-object v2, v1, v4

    const-string v2, "weather_2nd_bg_snow"

    aput-object v2, v1, v3

    const/4 v2, 0x2

    const-string v3, "weather_2nd_bg_sunny"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "weather_2nd_bg_cloudy"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "weather_2nd_bg_thunderstorms"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "weather_2nd_bg_partly_sunny"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "weather_2nd_bg_rain_n"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "weather_2nd_bg_snow_n"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "weather_2nd_bg_clear"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "weather_2nd_bg_cloudy_n"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "weather_2nd_bg_thunderstorms_n"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "weather_2nd_bg_mostly_clear"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "weather_2nd_bg_fog"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "weather_2nd_bg_fog_n"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "weather_2nd_bg_hot"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "weather_2nd_bg_cold"

    aput-object v3, v1, v2

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mVideofileName:[Ljava/lang/String;

    .line 113
    iput-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    .line 402
    iput-boolean v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mIsPlayingFadeOutAnim:Z

    .line 620
    iput v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mCurrentPos:I

    .line 684
    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$4;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$4;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPCompleteListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 692
    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$5;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$5;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPPrepareListener:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 739
    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$6;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$6;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    .line 746
    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$7;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$7;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPHandler:Landroid/os/Handler;

    .line 130
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mContext:Landroid/content/Context;

    .line 131
    iput-object p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mViEffectSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    .line 132
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v6, :cond_0

    .line 133
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mWeatherVideoBGUri:[Ljava/lang/String;

    const-string v2, ""

    aput-object v2, v1, v0

    .line 132
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 136
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mIsPlayingFadeOutAnim:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    .prologue
    .line 22
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPState:I

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;
    .param p1, "x1"    # I

    .prologue
    .line 22
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPState:I

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)Landroid/widget/VideoView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPView:Landroid/widget/VideoView;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;
    .param p1, "x1"    # Landroid/media/MediaPlayer;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMediaPlayer:Landroid/media/MediaPlayer;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPResUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mIsVisibleActivity:Z

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)Landroid/media/MediaPlayer$OnCompletionListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPCompleteListener:Landroid/media/MediaPlayer$OnCompletionListener;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)Landroid/media/MediaPlayer$OnPreparedListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPPrepareListener:Landroid/media/MediaPlayer$OnPreparedListener;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)Landroid/media/MediaPlayer$OnErrorListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    return-object v0
.end method

.method private getVideoUri(I)Landroid/net/Uri;
    .locals 3
    .param p1, "index"    # I

    .prologue
    const/4 v0, 0x0

    .line 350
    const/16 v1, 0xf

    if-le p1, v1, :cond_1

    .line 358
    :cond_0
    :goto_0
    return-object v0

    .line 354
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mWeatherVideoBGUri:[Ljava/lang/String;

    aget-object v1, v1, p1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 358
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mWeatherVideoBGUri:[Ljava/lang/String;

    aget-object v0, v0, p1

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public finish()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 779
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->stop()V

    .line 781
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 782
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 783
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPHandler:Landroid/os/Handler;

    .line 786
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPCompleteListener:Landroid/media/MediaPlayer$OnCompletionListener;

    if-eqz v0, :cond_1

    .line 787
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPCompleteListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 789
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPPrepareListener:Landroid/media/MediaPlayer$OnPreparedListener;

    if-eqz v0, :cond_2

    .line 790
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPPrepareListener:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 792
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPCompleteListener:Landroid/media/MediaPlayer$OnCompletionListener;

    if-eqz v0, :cond_3

    .line 793
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPCompleteListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 796
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPView:Landroid/widget/VideoView;

    if-eqz v0, :cond_4

    .line 797
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPView:Landroid/widget/VideoView;

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 798
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPView:Landroid/widget/VideoView;

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 799
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPView:Landroid/widget/VideoView;

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 800
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->clearFocus()V

    .line 801
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->destroyDrawingCache()V

    .line 803
    :cond_4
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPResUri:Landroid/net/Uri;

    if-eqz v0, :cond_5

    .line 804
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPResUri:Landroid/net/Uri;

    .line 807
    :cond_5
    return-void
.end method

.method public getMPState()I
    .locals 1

    .prologue
    .line 473
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPState:I

    return v0
.end method

.method public getVideoHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 775
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public getVideoRes()V
    .locals 8

    .prologue
    const/16 v7, 0x10

    const/4 v6, 0x1

    .line 156
    iget-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mIsLoadCompletedRes:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mViEffectSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getBackgroundVideo()I

    move-result v2

    if-ne v2, v6, :cond_2

    .line 157
    const-string v2, ""

    const-string v3, "get video res!!"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    new-array v1, v7, [I

    .line 161
    .local v1, "resId":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v7, :cond_1

    .line 165
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mVideofileName:[Ljava/lang/String;

    aget-object v3, v3, v0

    const-string v4, "raw"

    const-string v5, "com.sec.android.widgetapp.ap.hero.accuweather"

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    aput v2, v1, v0

    .line 168
    aget v2, v1, v0

    if-nez v2, :cond_0

    .line 169
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mWeatherVideoBGUri:[Ljava/lang/String;

    const-string v3, ""

    aput-object v3, v2, v0

    .line 170
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "No such file, weather index = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 172
    :cond_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mWeatherVideoBGUri:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "android.resource://com.sec.android.widgetapp.ap.hero.accuweather/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget v4, v1, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_1

    .line 176
    :cond_1
    iput-boolean v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mIsLoadCompletedRes:Z

    .line 179
    .end local v0    # "i":I
    .end local v1    # "resId":[I
    :cond_2
    return-void
.end method

.method public isIsLoadCompletedRes()Z
    .locals 1

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mIsLoadCompletedRes:Z

    return v0
.end method

.method public isPlaying()Z
    .locals 1

    .prologue
    .line 678
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPView:Landroid/widget/VideoView;

    if-nez v0, :cond_0

    .line 679
    const/4 v0, 0x0

    .line 681
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->isPlaying()Z

    move-result v0

    goto :goto_0
.end method

.method public pause()V
    .locals 3

    .prologue
    .line 626
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPView:Landroid/widget/VideoView;

    if-nez v0, :cond_1

    .line 639
    :cond_0
    :goto_0
    return-void

    .line 630
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mViEffectSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mViEffectSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    .line 631
    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getBackgroundVideo()I

    move-result v0

    if-eqz v0, :cond_0

    .line 634
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->getCurrentPosition()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mCurrentPos:I

    .line 635
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mp pause!! current pos = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mCurrentPos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 637
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPState:I

    .line 638
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->pause()V

    goto :goto_0
.end method

.method public playBGFadeIn()V
    .locals 4

    .prologue
    .line 364
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mViEffectSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getFadeInAnimation()I

    move-result v1

    if-nez v1, :cond_1

    .line 400
    :cond_0
    :goto_0
    return-void

    .line 368
    :cond_1
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BG Fade In mMPState = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 369
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPState:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 373
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mContext:Landroid/content/Context;

    const/high16 v2, 0x7f040000

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 376
    .local v0, "animationFadeIn":Landroid/view/animation/Animation;
    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$1;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 396
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    if-eqz v1, :cond_0

    .line 397
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    invoke-interface {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;->getCurrentBGView()Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public playBGFadeOut()V
    .locals 4

    .prologue
    .line 409
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BG Fade Out mMPState = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 410
    iget-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mIsPlayingFadeOutAnim:Z

    if-eqz v1, :cond_1

    .line 411
    const-string v1, ""

    const-string v2, "already running~"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 452
    :cond_0
    :goto_0
    return-void

    .line 415
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->resetFirstFrameAnim()V

    .line 416
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mContext:Landroid/content/Context;

    const v2, 0x7f040001

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 418
    .local v0, "animationFadeOut":Landroid/view/animation/Animation;
    const-wide/16 v1, 0xfa

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 419
    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$2;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 448
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    if-eqz v1, :cond_0

    .line 449
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    invoke-interface {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;->getCurrentBGView()Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public resetFirstFrameAnim()V
    .locals 3

    .prologue
    .line 456
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    if-nez v1, :cond_0

    .line 457
    const-string v1, ""

    const-string v2, "mL is null!!"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    :goto_0
    return-void

    .line 459
    :cond_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    invoke-interface {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;->getCurrentBGView()Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ImageView;->clearAnimation()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 461
    :catch_0
    move-exception v0

    .line 462
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 463
    const-string v1, ""

    const-string v2, "rFFA NPE"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public resume()V
    .locals 2

    .prologue
    .line 645
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPView:Landroid/widget/VideoView;

    if-nez v0, :cond_1

    .line 670
    :cond_0
    :goto_0
    return-void

    .line 649
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mViEffectSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mViEffectSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    .line 650
    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getBackgroundVideo()I

    move-result v0

    if-eqz v0, :cond_0

    .line 654
    :cond_2
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPState:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 658
    const-string v0, ""

    const-string v1, "mp resume !!"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 660
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPView:Landroid/widget/VideoView;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mCurrentPos:I

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->seekTo(I)V

    .line 661
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMediaPlayer:Landroid/media/MediaPlayer;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$3;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$3;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    goto :goto_0
.end method

.method public setIsLoadCompletedRes(Z)V
    .locals 0
    .param p1, "mIsLoadCompletedRes"    # Z

    .prologue
    .line 149
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mIsLoadCompletedRes:Z

    .line 150
    return-void
.end method

.method public setIsVisibleActivity(Z)V
    .locals 0
    .param p1, "mIsVisibleActivity"    # Z

    .prologue
    .line 86
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mIsVisibleActivity:Z

    .line 87
    return-void
.end method

.method public setMPState(I)V
    .locals 0
    .param p1, "mpState"    # I

    .prologue
    .line 482
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPState:I

    .line 483
    return-void
.end method

.method public setMPVideoView(Landroid/widget/VideoView;)V
    .locals 0
    .param p1, "videoView"    # Landroid/widget/VideoView;

    .prologue
    .line 187
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPView:Landroid/widget/VideoView;

    .line 188
    return-void
.end method

.method public setOnMediaPlayerListener(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    .line 122
    return-void
.end method

.method public setSetting(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;)V
    .locals 0
    .param p1, "viEffectSetting"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    .prologue
    .line 194
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mViEffectSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    .line 195
    return-void
.end method

.method public setUri(IZ)V
    .locals 5
    .param p1, "iconNum"    # I
    .param p2, "isDay"    # Z

    .prologue
    const/16 v4, 0x9

    const/16 v3, 0x8

    const/4 v2, 0x5

    const/4 v1, 0x3

    const/4 v0, 0x2

    .line 203
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->getVideoRes()V

    .line 204
    packed-switch p1, :pswitch_data_0

    .line 340
    :pswitch_0
    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->getVideoUri(I)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPResUri:Landroid/net/Uri;

    .line 343
    :goto_0
    return-void

    .line 208
    :pswitch_1
    if-eqz p2, :cond_0

    .line 209
    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->getVideoUri(I)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPResUri:Landroid/net/Uri;

    goto :goto_0

    .line 211
    :cond_0
    invoke-direct {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->getVideoUri(I)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPResUri:Landroid/net/Uri;

    goto :goto_0

    .line 221
    :pswitch_2
    if-eqz p2, :cond_1

    .line 222
    invoke-direct {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->getVideoUri(I)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPResUri:Landroid/net/Uri;

    goto :goto_0

    .line 224
    :cond_1
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->getVideoUri(I)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPResUri:Landroid/net/Uri;

    goto :goto_0

    .line 232
    :pswitch_3
    if-eqz p2, :cond_2

    .line 233
    invoke-direct {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->getVideoUri(I)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPResUri:Landroid/net/Uri;

    goto :goto_0

    .line 235
    :cond_2
    invoke-direct {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->getVideoUri(I)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPResUri:Landroid/net/Uri;

    goto :goto_0

    .line 241
    :pswitch_4
    if-eqz p2, :cond_3

    .line 242
    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->getVideoUri(I)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPResUri:Landroid/net/Uri;

    goto :goto_0

    .line 244
    :cond_3
    const/16 v0, 0xd

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->getVideoUri(I)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPResUri:Landroid/net/Uri;

    goto :goto_0

    .line 257
    :pswitch_5
    if-eqz p2, :cond_4

    .line 258
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->getVideoUri(I)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPResUri:Landroid/net/Uri;

    goto :goto_0

    .line 260
    :cond_4
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->getVideoUri(I)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPResUri:Landroid/net/Uri;

    goto :goto_0

    .line 271
    :pswitch_6
    if-eqz p2, :cond_5

    .line 272
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->getVideoUri(I)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPResUri:Landroid/net/Uri;

    goto :goto_0

    .line 274
    :cond_5
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->getVideoUri(I)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPResUri:Landroid/net/Uri;

    goto :goto_0

    .line 292
    :pswitch_7
    if-eqz p2, :cond_6

    .line 293
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->getVideoUri(I)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPResUri:Landroid/net/Uri;

    goto :goto_0

    .line 295
    :cond_6
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->getVideoUri(I)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPResUri:Landroid/net/Uri;

    goto :goto_0

    .line 301
    :pswitch_8
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->getVideoUri(I)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPResUri:Landroid/net/Uri;

    goto/16 :goto_0

    .line 306
    :pswitch_9
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->getVideoUri(I)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPResUri:Landroid/net/Uri;

    goto/16 :goto_0

    .line 313
    :pswitch_a
    if-eqz p2, :cond_7

    .line 314
    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->getVideoUri(I)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPResUri:Landroid/net/Uri;

    goto/16 :goto_0

    .line 316
    :cond_7
    invoke-direct {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->getVideoUri(I)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPResUri:Landroid/net/Uri;

    goto/16 :goto_0

    .line 323
    :pswitch_b
    if-eqz p2, :cond_8

    .line 324
    invoke-direct {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->getVideoUri(I)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPResUri:Landroid/net/Uri;

    goto/16 :goto_0

    .line 326
    :cond_8
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->getVideoUri(I)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPResUri:Landroid/net/Uri;

    goto/16 :goto_0

    .line 332
    :pswitch_c
    if-eqz p2, :cond_9

    .line 333
    invoke-direct {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->getVideoUri(I)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPResUri:Landroid/net/Uri;

    goto/16 :goto_0

    .line 335
    :cond_9
    invoke-direct {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->getVideoUri(I)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPResUri:Landroid/net/Uri;

    goto/16 :goto_0

    .line 204
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_5
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_8
        :pswitch_9
        :pswitch_2
        :pswitch_a
        :pswitch_a
        :pswitch_b
        :pswitch_b
        :pswitch_b
        :pswitch_c
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_7
    .end packed-switch
.end method

.method public start()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 489
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPView:Landroid/widget/VideoView;

    if-nez v0, :cond_1

    .line 516
    :cond_0
    :goto_0
    return-void

    .line 492
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mViEffectSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mViEffectSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    .line 493
    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getBackgroundVideo()I

    move-result v0

    if-eqz v0, :cond_0

    .line 496
    :cond_2
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPState:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPState:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 500
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPState:I

    .line 502
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPHandler:Landroid/os/Handler;

    if-eqz v0, :cond_5

    .line 503
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    invoke-interface {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;->isConfigurationChanged()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 504
    const-string v0, ""

    const-string v1, "weather mp start after 100"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 505
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPHandler:Landroid/os/Handler;

    const-wide/16 v1, 0x64

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 506
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    invoke-interface {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;->isCreateActivity()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 507
    const-string v0, ""

    const-string v1, "weather mp start after 800"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 508
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPHandler:Landroid/os/Handler;

    const-wide/16 v1, 0x320

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 510
    :cond_4
    const-string v0, ""

    const-string v1, "weather mp start after 400"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 511
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPHandler:Landroid/os/Handler;

    const-wide/16 v1, 0x190

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 514
    :cond_5
    const-string v0, ""

    const-string v1, "handler null!!"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public startAfterFlick()V
    .locals 2

    .prologue
    .line 522
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPView:Landroid/widget/VideoView;

    if-nez v0, :cond_1

    .line 543
    :cond_0
    :goto_0
    return-void

    .line 525
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mViEffectSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mViEffectSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    .line 526
    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getBackgroundVideo()I

    move-result v0

    if-eqz v0, :cond_0

    .line 530
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    .line 531
    invoke-interface {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;->getCubeViewController()Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    .line 532
    invoke-interface {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;->getCubeViewController()Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->isRunningAni()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 533
    const-string v0, ""

    const-string v1, "running scroll ani "

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 536
    :cond_3
    const-string v0, ""

    const-string v1, "weather mp start by flick after 0 "

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 537
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPState:I

    .line 538
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPHandler:Landroid/os/Handler;

    if-eqz v0, :cond_4

    .line 539
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 541
    :cond_4
    const-string v0, ""

    const-string v1, "handler null!!"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public stop()V
    .locals 3

    .prologue
    .line 550
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPView:Landroid/widget/VideoView;

    if-nez v1, :cond_1

    .line 551
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPState:I

    .line 552
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    invoke-interface {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;->getCurrentBGView()Landroid/widget/ImageView;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 553
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->resetFirstFrameAnim()V

    .line 554
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;->setBGAlphaListener(Z)V

    .line 587
    :cond_0
    :goto_0
    return-void

    .line 559
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mViEffectSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mViEffectSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    .line 560
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getBackgroundVideo()I

    move-result v1

    if-nez v1, :cond_2

    .line 561
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPState:I

    .line 562
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    invoke-interface {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;->getCurrentBGView()Landroid/widget/ImageView;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 563
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->resetFirstFrameAnim()V

    .line 564
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    invoke-interface {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;->getCurrentBGView()Landroid/widget/ImageView;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setAlpha(F)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 583
    :catch_0
    move-exception v0

    .line 584
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 585
    const-string v1, ""

    const-string v2, "mp sT NPE"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 569
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :cond_2
    :try_start_1
    const-string v1, ""

    const-string v2, "mp stop!!"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 571
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPHandler:Landroid/os/Handler;

    if-eqz v1, :cond_3

    .line 572
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 575
    :cond_3
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPView:Landroid/widget/VideoView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/VideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 577
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPState:I

    .line 578
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPView:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->stopPlayback()V

    .line 579
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    invoke-interface {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;->getCurrentBGView()Landroid/widget/ImageView;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 580
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->resetFirstFrameAnim()V

    .line 581
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    invoke-interface {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;->getCurrentBGView()Landroid/widget/ImageView;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setAlpha(F)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public stopByFlick()V
    .locals 3

    .prologue
    .line 594
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPView:Landroid/widget/VideoView;

    if-nez v1, :cond_1

    .line 618
    :cond_0
    :goto_0
    return-void

    .line 598
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mViEffectSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mViEffectSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    .line 599
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getBackgroundVideo()I

    move-result v1

    if-eqz v1, :cond_0

    .line 603
    :cond_2
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPHandler:Landroid/os/Handler;

    if-eqz v1, :cond_3

    .line 604
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 606
    :cond_3
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPView:Landroid/widget/VideoView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/VideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 607
    const-string v1, ""

    const-string v2, "mp stop by flick!!"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 608
    const/4 v1, 0x1

    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPState:I

    .line 609
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPView:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->stopPlayback()V

    .line 610
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    invoke-interface {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;->getCurrentBGView()Landroid/widget/ImageView;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 611
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->resetFirstFrameAnim()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 613
    :catch_0
    move-exception v0

    .line 614
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 615
    const-string v1, ""

    const-string v2, "mp sT NPE"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
