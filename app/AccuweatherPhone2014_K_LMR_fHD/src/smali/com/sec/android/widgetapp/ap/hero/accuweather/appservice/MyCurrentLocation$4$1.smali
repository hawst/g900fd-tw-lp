.class Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4$1;
.super Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;
.source "MyCurrentLocation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;)V
    .locals 0
    .param p1, "this$1"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;

    .prologue
    .line 426
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;

    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(IILjava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1, "position"    # I
    .param p2, "responseCode"    # I
    .param p3, "responseStatus"    # Ljava/lang/String;
    .param p4, "responseBody"    # Ljava/lang/String;

    .prologue
    const/16 v8, 0xc8

    .line 429
    invoke-super {p0, p1, p2, p3, p4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;->onReceive(IILjava/lang/String;Ljava/lang/String;)V

    .line 431
    if-ne p2, v8, :cond_2

    .line 432
    const/4 v1, 0x0

    .line 433
    .local v1, "detailinfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    new-instance v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;

    invoke-direct {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;-><init>()V

    .line 434
    .local v4, "parser":Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;

    iget v6, v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;->val$tempScaleSetting:I

    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;

    iget-object v7, v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;->val$updateDate:Ljava/lang/String;

    invoke-virtual {v4, v6, p4, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->parseDetailWeather(ILjava/lang/String;Ljava/lang/String;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    move-result-object v1

    .line 437
    if-eqz v1, :cond_1

    .line 438
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v3

    .line 439
    .local v3, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 440
    .local v0, "data":Landroid/os/Bundle;
    const-string v6, "cityinfo"

    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;

    iget-object v7, v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;->val$info:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 441
    const-string v6, "today"

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 442
    const-string v6, "cityxmlinfo"

    invoke-virtual {p4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    const-string v6, "detailinfo"

    invoke-virtual {v0, v6, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 444
    invoke-virtual {v3, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 445
    iput v8, v3, Landroid/os/Message;->what:I

    .line 446
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v5

    .line 447
    .local v5, "todayInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;

    iget-object v6, v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;->val$info:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSummerTime()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setSummerTime(Ljava/lang/String;)V

    .line 451
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForcastSize()I

    move-result v6

    if-ge v2, v6, :cond_0

    .line 452
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    .line 453
    invoke-virtual {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v7

    .line 452
    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 451
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 455
    :cond_0
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;

    iget-object v6, v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;->val$locationHandler:Landroid/os/Handler;

    invoke-virtual {v6, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 457
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;

    iget-object v6, v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->ctx:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;)Landroid/content/Context;

    move-result-object v6

    const/4 v7, 0x1

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setUpdateTimeToPref(Landroid/content/Context;Z)V

    .line 458
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;

    iget-object v6, v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->ctx:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;)Landroid/content/Context;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setOtherRefreshOKToPref(Landroid/content/Context;Z)V

    .line 478
    .end local v0    # "data":Landroid/os/Bundle;
    .end local v1    # "detailinfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .end local v2    # "i":I
    .end local v3    # "msg":Landroid/os/Message;
    .end local v4    # "parser":Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;
    .end local v5    # "todayInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    :goto_1
    return-void

    .line 461
    .restart local v1    # "detailinfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .restart local v4    # "parser":Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;
    :cond_1
    const-string v6, ""

    const-string v7, "adCtWthrFrmDetail : fail to parse"

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 462
    const/4 p2, -0x2

    .line 465
    .end local v1    # "detailinfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .end local v4    # "parser":Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;
    :cond_2
    if-eq p2, v8, :cond_3

    .line 466
    const-string v6, ""

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "adCtWthrFrmDetail : r = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v3

    .line 468
    .restart local v3    # "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 469
    .restart local v0    # "data":Landroid/os/Bundle;
    invoke-virtual {v3, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 470
    const/16 v6, 0x3f2

    iput v6, v3, Landroid/os/Message;->what:I

    .line 471
    const-string v6, "provider"

    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;

    iget-object v7, v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mProvider:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 472
    const-string v6, "responseCode"

    invoke-virtual {v0, v6, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 473
    invoke-virtual {v3, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 474
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;

    iget-object v6, v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;->val$locationHandler:Landroid/os/Handler;

    invoke-virtual {v6, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 477
    .end local v0    # "data":Landroid/os/Bundle;
    .end local v3    # "msg":Landroid/os/Message;
    :cond_3
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;

    iget-object v6, v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->stopHttpThread()V

    goto :goto_1
.end method
