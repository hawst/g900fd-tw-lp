.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;
.super Ljava/lang/Object;
.source "SettingsViEffect.java"


# instance fields
.field mBackgroundVideo:I

.field mBackgroundVideoRepeat:I

.field mBottomIconAnimtion:I

.field mCityChangeScrollAnimation:I

.field mCitySelectAnimation:I

.field mFadeInAnimation:I

.field mIconAnimation:I

.field mIconAnimationRepeat:I

.field mIconDurationVal:I

.field mInforAnimation:I

.field mTouchDuration:I

.field mWeatherEffect:I

.field mWidgetBGTransparencyVal:I

.field mWidgetBGonoff:I

.field mWidgetCityChangeAnimation:I

.field mWidgetIconAnimation:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    return-void
.end method

.method public constructor <init>(IIIIIIIIIIIIIIII)V
    .locals 1
    .param p1, "backgroundVideo"    # I
    .param p2, "backgroundVideoRepeat"    # I
    .param p3, "iconAnimation"    # I
    .param p4, "iconAnimationRepeat"    # I
    .param p5, "fadeInAnimation"    # I
    .param p6, "cityChangeScrollAnimation"    # I
    .param p7, "weatherEffect"    # I
    .param p8, "inforAnimation"    # I
    .param p9, "bottomIconAnimtion"    # I
    .param p10, "widgetIconAnimation"    # I
    .param p11, "widgetCityChangeAnimation"    # I
    .param p12, "citySelectAnimation"    # I
    .param p13, "widgetBGTransparencyVal"    # I
    .param p14, "widgetBGonoff"    # I
    .param p15, "iconDurationVal"    # I
    .param p16, "touchDuration"    # I

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mBackgroundVideo:I

    .line 24
    iput p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mBackgroundVideoRepeat:I

    .line 25
    iput p3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mIconAnimation:I

    .line 26
    iput p4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mIconAnimationRepeat:I

    .line 27
    iput p5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mFadeInAnimation:I

    .line 28
    iput p6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mCityChangeScrollAnimation:I

    .line 29
    iput p7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mWeatherEffect:I

    .line 30
    iput p8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mInforAnimation:I

    .line 31
    iput p9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mBottomIconAnimtion:I

    .line 32
    iput p10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mWidgetIconAnimation:I

    .line 33
    iput p11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mWidgetCityChangeAnimation:I

    .line 34
    iput p12, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mCitySelectAnimation:I

    .line 35
    iput p13, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mWidgetBGTransparencyVal:I

    .line 36
    iput p14, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mWidgetBGonoff:I

    .line 37
    move/from16 v0, p15

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mIconDurationVal:I

    .line 38
    move/from16 v0, p16

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mTouchDuration:I

    .line 39
    return-void
.end method


# virtual methods
.method public getBackgroundVideo()I
    .locals 1

    .prologue
    .line 42
    sget-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isKitKatOS:Z

    if-nez v0, :cond_0

    .line 43
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mBackgroundVideo:I

    .line 45
    :cond_0
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mBackgroundVideo:I

    return v0
.end method

.method public getBackgroundVideoRepeat()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mBackgroundVideoRepeat:I

    return v0
.end method

.method public getBottomIconAnimtion()I
    .locals 1

    .prologue
    .line 117
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mBottomIconAnimtion:I

    return v0
.end method

.method public getCityChangeScrollAnimation()I
    .locals 1

    .prologue
    .line 87
    sget-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isKitKatOS:Z

    if-nez v0, :cond_0

    .line 88
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mCityChangeScrollAnimation:I

    .line 90
    :cond_0
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mCityChangeScrollAnimation:I

    return v0
.end method

.method public getCitySelectAnimation()I
    .locals 1

    .prologue
    .line 147
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mCitySelectAnimation:I

    .line 148
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mCitySelectAnimation:I

    return v0
.end method

.method public getFadeInAnimation()I
    .locals 1

    .prologue
    .line 80
    sget-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isKitKatOS:Z

    if-nez v0, :cond_0

    .line 81
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mFadeInAnimation:I

    .line 83
    :cond_0
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mFadeInAnimation:I

    return v0
.end method

.method public getIconAnimation()I
    .locals 1

    .prologue
    .line 61
    sget-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isKitKatOS:Z

    if-nez v0, :cond_0

    .line 62
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mIconAnimation:I

    .line 64
    :cond_0
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mIconAnimation:I

    return v0
.end method

.method public getIconAnimationRepeat()I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mIconAnimationRepeat:I

    return v0
.end method

.method public getIconDurationVal()I
    .locals 1

    .prologue
    .line 160
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mIconDurationVal:I

    return v0
.end method

.method public getInforAnimation()I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mInforAnimation:I

    return v0
.end method

.method public getTouchDuration()I
    .locals 2

    .prologue
    .line 164
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mTouchDuration:I

    const/16 v1, 0x1f4

    if-ge v0, v1, :cond_0

    .line 165
    const/16 v0, 0x44c

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mTouchDuration:I

    .line 167
    :cond_0
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mTouchDuration:I

    return v0
.end method

.method public getWeatherEffect()I
    .locals 1

    .prologue
    .line 98
    sget-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isKitKatOS:Z

    if-nez v0, :cond_0

    .line 99
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mWeatherEffect:I

    .line 101
    :cond_0
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mWeatherEffect:I

    return v0
.end method

.method public getWidgetBGOnOff()I
    .locals 1

    .prologue
    .line 156
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mWidgetBGonoff:I

    return v0
.end method

.method public getWidgetBGTransparencyVal()I
    .locals 1

    .prologue
    .line 152
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mWidgetBGTransparencyVal:I

    return v0
.end method

.method public getWidgetCityChangeAnimation()I
    .locals 1

    .prologue
    .line 136
    sget-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isKitKatOS:Z

    if-nez v0, :cond_0

    .line 137
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mWidgetCityChangeAnimation:I

    .line 139
    :cond_0
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mWidgetCityChangeAnimation:I

    return v0
.end method

.method public getWidgetIconAnimation()I
    .locals 1

    .prologue
    .line 125
    sget-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isKitKatOS:Z

    if-nez v0, :cond_0

    .line 126
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mWidgetIconAnimation:I

    .line 128
    :cond_0
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mWidgetIconAnimation:I

    return v0
.end method

.method public setBackgroundVideo(I)V
    .locals 0
    .param p1, "backgroundVideo"    # I

    .prologue
    .line 49
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mBackgroundVideo:I

    .line 50
    return-void
.end method

.method public setBackgroundVideoRepeat(I)V
    .locals 0
    .param p1, "backgroundVideoRepeat"    # I

    .prologue
    .line 57
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mBackgroundVideoRepeat:I

    .line 58
    return-void
.end method

.method public setBottomIconAnimtion(I)V
    .locals 0
    .param p1, "bottomIconAnimtion"    # I

    .prologue
    .line 121
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mBottomIconAnimtion:I

    .line 122
    return-void
.end method

.method public setCityChangeScrollAnimation(I)V
    .locals 0
    .param p1, "scrollAnimation"    # I

    .prologue
    .line 94
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mCityChangeScrollAnimation:I

    .line 95
    return-void
.end method

.method public setIconAnimation(I)V
    .locals 0
    .param p1, "iconAnimation"    # I

    .prologue
    .line 68
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mIconAnimation:I

    .line 69
    return-void
.end method

.method public setIconAnimationRepeat(I)V
    .locals 0
    .param p1, "iconAnimationRepeat"    # I

    .prologue
    .line 76
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mIconAnimationRepeat:I

    .line 77
    return-void
.end method

.method public setInforAnimation(I)V
    .locals 0
    .param p1, "inforAnimation"    # I

    .prologue
    .line 113
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mInforAnimation:I

    .line 114
    return-void
.end method

.method public setWeatherEffect(I)V
    .locals 0
    .param p1, "weatherEffect"    # I

    .prologue
    .line 105
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mWeatherEffect:I

    .line 106
    return-void
.end method

.method public setWidgetCityChangeAnimation(I)V
    .locals 0
    .param p1, "widgetCityChangeAnimation"    # I

    .prologue
    .line 143
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mWidgetCityChangeAnimation:I

    .line 144
    return-void
.end method

.method public setWidgetIconAnimation(I)V
    .locals 0
    .param p1, "widgetIconAnimation"    # I

    .prologue
    .line 132
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->mWidgetIconAnimation:I

    .line 133
    return-void
.end method
