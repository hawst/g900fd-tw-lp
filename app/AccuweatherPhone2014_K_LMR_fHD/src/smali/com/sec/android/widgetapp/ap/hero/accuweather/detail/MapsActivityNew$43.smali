.class Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$43;
.super Ljava/lang/Object;
.source "MapsActivityNew.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->showLoadingDialog(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 1494
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$43;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 1496
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$43;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLoadingDialog:Landroid/app/Dialog;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4302(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Landroid/app/Dialog;)Landroid/app/Dialog;

    .line 1497
    const-string v0, ""

    const-string v1, "===stopHttpThread==="

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1498
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$43;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->stopHttpThread()V
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$4400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    .line 1500
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$43;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->locHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 1501
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$43;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->locHandler:Landroid/os/Handler;

    const v1, -0x140b4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1504
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$43;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLocationListener:Landroid/location/LocationListener;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$2100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Landroid/location/LocationListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1505
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$43;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLocationManager:Landroid/location/LocationManager;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$2200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Landroid/location/LocationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$43;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLocationListener:Landroid/location/LocationListener;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$2100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Landroid/location/LocationListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 1507
    :cond_1
    return-void
.end method
