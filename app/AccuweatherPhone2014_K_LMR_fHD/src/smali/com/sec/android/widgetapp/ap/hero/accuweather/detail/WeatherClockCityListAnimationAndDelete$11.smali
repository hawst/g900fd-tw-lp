.class Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$11;
.super Ljava/lang/Object;
.source "WeatherClockCityListAnimationAndDelete.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    .prologue
    .line 729
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$11;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "motionevent"    # Landroid/view/MotionEvent;

    .prologue
    .line 732
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/16 v3, 0x9

    if-ne v2, v3, :cond_1

    .line 733
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 735
    .local v1, "sb":Ljava/lang/StringBuffer;
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$11;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->mIsDeleteMode:Z
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->access$900(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 736
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$11;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0065

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 738
    const v2, 0x7f08006e

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultCheckBox;

    .line 739
    .local v0, "checkbox":Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultCheckBox;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$11;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d0093

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 741
    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultCheckBox;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 742
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$11;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0094

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 748
    .end local v0    # "checkbox":Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultCheckBox;
    :cond_0
    :goto_0
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 749
    const/4 v2, 0x0

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 752
    .end local v1    # "sb":Ljava/lang/StringBuffer;
    :cond_1
    const/4 v2, 0x1

    return v2

    .line 744
    .restart local v0    # "checkbox":Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultCheckBox;
    .restart local v1    # "sb":Ljava/lang/StringBuffer;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$11;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0095

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method
