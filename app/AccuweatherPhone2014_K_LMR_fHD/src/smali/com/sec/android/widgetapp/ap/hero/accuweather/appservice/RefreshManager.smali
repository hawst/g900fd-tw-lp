.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;
.super Ljava/lang/Object;
.source "RefreshManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;
    }
.end annotation


# static fields
.field public static final INVISIBLE_ERROR_MSG:I = 0x1

.field public static final VISIBLE_ERROR_MSG:I

.field private static mFirstUpdate:Z


# instance fields
.field private errLog:Ljava/lang/String;

.field private getLocDataHandler:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;

.field private httpHandler:Landroid/os/Handler;

.field private httpThreads:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Thread;",
            ">;"
        }
    .end annotation
.end field

.field private lastTime:J

.field private mCityList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mCtx:Landroid/content/Context;

.field private mIsFirstCurrentLocation:Z

.field private mMcl:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

.field private mRefreshCanceled:Z

.field private mRefreshCnt:I

.field private mRefreshCompleted:Z

.field private mRefreshFrom:I

.field private mResponseCnt:I

.field private mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

.field private mUiHandler:Landroid/os/Handler;

.field private mUrlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

.field private mUseCurrentLocation:Z

.field private nowTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mFirstUpdate:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    .line 50
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mIsFirstCurrentLocation:Z

    .line 52
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mMcl:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    .line 54
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    .line 56
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mUseCurrentLocation:Z

    .line 58
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mUrlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    .line 60
    iput-wide v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->nowTime:J

    .line 62
    iput-wide v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->lastTime:J

    .line 64
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->errLog:Ljava/lang/String;

    .line 619
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->httpThreads:Ljava/util/ArrayList;

    .line 854
    iput-boolean v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCompleted:Z

    .line 856
    iput-boolean v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCanceled:Z

    .line 858
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mResponseCnt:I

    .line 860
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCnt:I

    .line 1037
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshFrom:I

    .line 1039
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mUiHandler:Landroid/os/Handler;

    .line 1450
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$3;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$3;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->httpHandler:Landroid/os/Handler;

    .line 73
    const-string v0, ""

    const-string v1, "RefreshManager : create"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    .line 75
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->init()V

    .line 77
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSettingsForMain(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    .line 79
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 82
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->updateCityList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    .line 83
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    invoke-direct {v0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mMcl:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    .line 84
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mUrlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    .line 85
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mUseCurrentLocation:Z

    .line 91
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mMcl:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    .prologue
    .line 38
    iget-wide v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->nowTime:J

    return-wide v0
.end method

.method static synthetic access$1100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCanceled:Z

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->httpHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshFrom:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCnt:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mUiHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mIsFirstCurrentLocation:Z

    return v0
.end method

.method static synthetic access$502(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;
    .param p1, "x1"    # Z

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mIsFirstCurrentLocation:Z

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;
    .param p1, "x1"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    return-object p1
.end method

.method static synthetic access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->errLog:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->errLog:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$900(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    .prologue
    .line 38
    iget-wide v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->lastTime:J

    return-wide v0
.end method

.method private cleanResource()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 242
    const-string v0, ""

    const-string v1, " V cleanResource "

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getLocDataHandler:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getLocDataHandler:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;

    invoke-virtual {v0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 245
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getLocDataHandler:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;

    .line 248
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->httpHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 249
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->httpHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 250
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->httpHandler:Landroid/os/Handler;

    .line 255
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 256
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 257
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    .line 260
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mMcl:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    if-eqz v0, :cond_3

    .line 261
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mMcl:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->removeLocationListener()V

    .line 262
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mMcl:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->removePassiveLocationListener()V

    .line 264
    :cond_3
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    .line 265
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    .line 266
    return-void
.end method

.method private getCurrentLocation(Landroid/content/Context;ZZZ)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "networkOnly"    # Z
    .param p3, "needRefresh"    # Z
    .param p4, "retryRefresh"    # Z

    .prologue
    const/16 v4, 0x3f2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 278
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getLocDataHandler:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getLocDataHandler:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;

    invoke-virtual {v0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->removeMessages(I)V

    .line 280
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getLocDataHandler:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;

    const/16 v1, 0x3f3

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->removeMessages(I)V

    .line 281
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getLocDataHandler:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;

    invoke-virtual {v0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->removeMessages(I)V

    .line 282
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getLocDataHandler:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;

    const/16 v1, 0xca

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->removeMessages(I)V

    .line 283
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getLocDataHandler:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;

    const/16 v1, 0xc8

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->removeMessages(I)V

    .line 284
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getLocDataHandler:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;

    const/16 v1, 0xc9

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->removeMessages(I)V

    .line 285
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getLocDataHandler:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;

    const/16 v1, 0x3e7

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->removeMessages(I)V

    .line 287
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getLocDataHandler:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;

    .line 289
    :cond_0
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getLocDataHandler:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;

    .line 295
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mMcl:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getLocDataHandler:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;

    move v4, v2

    move v5, v3

    move v6, p3

    move v7, p4

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->performGetCurrentLocation(Landroid/os/Handler;IZZZZZ)V

    .line 298
    return-void
.end method

.method private makeHttpClient(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;ZZI)V
    .locals 13
    .param p1, "detailWeatherInfo"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .param p2, "bool"    # Z
    .param p3, "needRefresh"    # Z
    .param p4, "visibleError"    # I

    .prologue
    .line 755
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->httpThreads:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 756
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->httpThreads:Ljava/util/ArrayList;

    .line 759
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    if-nez v0, :cond_1

    .line 760
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSettingsForMain(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    .line 763
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    if-eqz v0, :cond_2

    .line 765
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getTempScaleSetting()I

    move-result v3

    .line 767
    .local v3, "tempScaleSetting":I
    const/4 v7, 0x0

    .line 768
    .local v7, "url":Ljava/net/URL;
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getLocation()Ljava/lang/String;

    move-result-object v0

    const-string v1, "cityId:current"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 769
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mUrlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getRealLocation()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    .line 770
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkLanguage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 769
    invoke-virtual {v0, v1, v3, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeUrlForGetDetailData(Ljava/lang/String;ILjava/lang/String;)Ljava/net/URL;

    move-result-object v7

    .line 776
    :goto_0
    if-nez v7, :cond_4

    .line 777
    const-string v0, ""

    const-string v1, "[mhc]url is null!!! "

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 823
    .end local v3    # "tempScaleSetting":I
    .end local v7    # "url":Ljava/net/URL;
    :cond_2
    :goto_1
    return-void

    .line 772
    .restart local v3    # "tempScaleSetting":I
    .restart local v7    # "url":Ljava/net/URL;
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mUrlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getLocation()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    .line 773
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkLanguage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 772
    invoke-virtual {v0, v1, v3, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeUrlForGetDetailData(Ljava/lang/String;ILjava/lang/String;)Ljava/net/URL;

    move-result-object v7

    goto :goto_0

    .line 784
    :cond_4
    new-instance v10, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    invoke-direct {v10, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;-><init>(Landroid/content/Context;)V

    .line 785
    .local v10, "httpClient":Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->httpThreads:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v9

    iget-object v12, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mUrlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeHeader()Lorg/apache/http/message/HeaderGroup;

    move-result-object v8

    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$1;

    move-object v1, p0

    move-object v2, p1

    move v4, p2

    move/from16 v5, p4

    move/from16 v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;IZIZ)V

    move-object v4, v10

    move v5, v9

    move-object v6, v12

    move-object v9, v0

    invoke-virtual/range {v4 .. v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->get(ILandroid/content/Context;Ljava/net/URL;Lorg/apache/http/message/HeaderGroup;Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;)Ljava/lang/Thread;

    move-result-object v11

    .line 819
    .local v11, "t":Ljava/lang/Thread;
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->httpThreads:Ljava/util/ArrayList;

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private performCurrentLocateForWidget(ZZZZ)V
    .locals 9
    .param p1, "firstUpdate"    # Z
    .param p2, "networkOnly"    # Z
    .param p3, "needRefresh"    # Z
    .param p4, "retryRefresh"    # Z

    .prologue
    const/16 v8, 0x300

    const/16 v7, 0x20

    const/4 v6, 0x0

    .line 667
    iput-boolean v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCanceled:Z

    .line 669
    if-eqz p3, :cond_0

    .line 670
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    const/4 v4, 0x1

    invoke-static {v3, v4, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getNextTime(Landroid/content/Context;ZZ)J

    move-result-wide v0

    .line 671
    .local v0, "nextTime":J
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    invoke-static {v3, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateNextTime(Landroid/content/Context;J)I

    .line 674
    .end local v0    # "nextTime":J
    :cond_0
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    invoke-static {v3, p1, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getLocationCheck(Landroid/content/Context;ZZ)I

    move-result v2

    .line 676
    .local v2, "result":I
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[pL] fU = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", nO = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", nR = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", r = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 679
    const/16 v3, 0xbb8

    if-ne v2, v3, :cond_4

    .line 680
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mIsFirstCurrentLocation:Z

    .line 682
    if-nez p1, :cond_1

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshFrom:I

    const/4 v4, 0x5

    if-ne v3, v4, :cond_2

    .line 683
    :cond_1
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.sec.android.widgetapp.ap.hero.accuweather.action.GET_CURRENT_LOCATION_START"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 687
    :cond_2
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    invoke-direct {p0, v3, p2, p3, p4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getCurrentLocation(Landroid/content/Context;ZZZ)V

    .line 743
    :cond_3
    :goto_0
    sput-boolean v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mFirstUpdate:Z

    .line 744
    return-void

    .line 689
    :cond_4
    const/16 v3, 0xbbb

    if-ne v2, v3, :cond_6

    .line 690
    iput-boolean v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mIsFirstCurrentLocation:Z

    .line 692
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.sec.android.widgetapp.ap.hero.accuweather.action.DISABLE_LOCATION_SERVICES"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 695
    invoke-virtual {p0, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->setRefreshCnt(I)V

    .line 697
    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshFrom:I

    if-nez v3, :cond_5

    .line 698
    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCnt:I

    if-gtz v3, :cond_5

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mUiHandler:Landroid/os/Handler;

    if-eqz v3, :cond_5

    .line 699
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {v3, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 702
    :cond_5
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getFirstLaunch(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 703
    const-string v3, ""

    const-string v4, "PCLFW ---> fst lanuch set f DLS"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 704
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    const-string v4, "EFFECT_40"

    invoke-static {v3, v4, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateViEffectSettings(Landroid/content/Context;Ljava/lang/String;I)I

    .line 705
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    invoke-static {v3, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setFirstLaunch(Landroid/content/Context;Z)Z

    goto :goto_0

    .line 707
    :cond_6
    const/16 v3, 0xbb9

    if-ne v2, v3, :cond_8

    .line 708
    iput-boolean v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mIsFirstCurrentLocation:Z

    .line 710
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.sec.android.widgetapp.ap.hero.accuweather.action.GET_DATA_NETWORK_ERROR"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 712
    invoke-virtual {p0, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->setRefreshCnt(I)V

    .line 714
    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshFrom:I

    if-nez v3, :cond_7

    .line 715
    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCnt:I

    if-gtz v3, :cond_7

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mUiHandler:Landroid/os/Handler;

    if-eqz v3, :cond_7

    .line 716
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {v3, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 719
    :cond_7
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getFirstLaunch(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 720
    const-string v3, ""

    const-string v4, "PCLFW ---> fst lanuch set f NU"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 721
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    const-string v4, "EFFECT_40"

    invoke-static {v3, v4, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateViEffectSettings(Landroid/content/Context;Ljava/lang/String;I)I

    .line 722
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    invoke-static {v3, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setFirstLaunch(Landroid/content/Context;Z)Z

    goto/16 :goto_0

    .line 724
    :cond_8
    const/16 v3, 0xbc1

    if-ne v2, v3, :cond_3

    .line 729
    if-nez p1, :cond_9

    .line 730
    invoke-virtual {p0, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->setRefreshCnt(I)V

    .line 732
    :cond_9
    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshFrom:I

    if-nez v3, :cond_a

    .line 733
    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCnt:I

    if-gtz v3, :cond_a

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mUiHandler:Landroid/os/Handler;

    if-eqz v3, :cond_a

    .line 734
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {v3, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 737
    :cond_a
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getFirstLaunch(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 738
    const-string v3, ""

    const-string v4, "PCLFW ---> fst lanuch set f EFM"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 739
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    const-string v4, "EFFECT_40"

    invoke-static {v3, v4, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateViEffectSettings(Landroid/content/Context;Ljava/lang/String;I)I

    .line 740
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    invoke-static {v3, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setFirstLaunch(Landroid/content/Context;Z)Z

    goto/16 :goto_0
.end method

.method private performNetworkAccess(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;IZZZ)V
    .locals 9
    .param p1, "detailWeatherInfo"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .param p2, "CityListIndex"    # I
    .param p3, "bool"    # Z
    .param p4, "needRefresh"    # Z
    .param p5, "retryRefresh"    # Z

    .prologue
    const/16 v8, 0x200

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1182
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " [pNA] : bool = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", nR = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1183
    iput-boolean v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCanceled:Z

    .line 1184
    iput-boolean v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCompleted:Z

    .line 1185
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    if-nez v3, :cond_0

    .line 1186
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->updateCityList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    .line 1188
    :cond_0
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_2

    .line 1243
    :cond_1
    :goto_0
    return-void

    .line 1191
    :cond_2
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[pNA] : size= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1193
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getLocation()Ljava/lang/String;

    move-result-object v3

    const-string v4, "cityId:current"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    sget-boolean v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isAutoCaptureToolSupportMode:Z

    if-nez v3, :cond_4

    .line 1214
    const-string v3, ""

    const-string v4, "request new current location"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1215
    if-eqz p4, :cond_3

    .line 1216
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    invoke-static {v3, v7, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getNextTime(Landroid/content/Context;ZZ)J

    move-result-wide v0

    .line 1217
    .local v0, "nextTime":J
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    invoke-static {v3, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateNextTime(Landroid/content/Context;J)I

    .line 1220
    .end local v0    # "nextTime":J
    :cond_3
    invoke-direct {p0, v6, v6, p4, p5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->performCurrentLocateForWidget(ZZZZ)V

    .line 1221
    invoke-virtual {p0, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->setRefreshCnt(I)V

    goto :goto_0

    .line 1224
    :cond_4
    const-string v3, ""

    const-string v4, "request refresh others"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1226
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getLocationCheck(Landroid/content/Context;)I

    move-result v2

    .line 1227
    .local v2, "result":I
    const/16 v3, 0xbb9

    if-ne v2, v3, :cond_5

    .line 1228
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.sec.android.widgetapp.ap.hero.accuweather.action.GET_DATA_NETWORK_ERROR"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1229
    const/16 v3, 0x300

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->setRefreshCnt(I)V

    .line 1230
    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshFrom:I

    if-nez v3, :cond_1

    .line 1231
    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCnt:I

    if-gtz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mUiHandler:Landroid/os/Handler;

    if-eqz v3, :cond_1

    .line 1232
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mUiHandler:Landroid/os/Handler;

    const/16 v4, 0x20

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 1237
    :cond_5
    invoke-virtual {p0, p1, p3, p4, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->performRefreshOtherCities(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;ZZI)V

    .line 1239
    invoke-virtual {p0, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->setRefreshCnt(I)V

    goto/16 :goto_0
.end method

.method private performNetworkAccess(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Z)V
    .locals 4
    .param p1, "detailWeatherInfo"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .param p2, "retryRefresh"    # Z

    .prologue
    const/16 v3, 0x200

    const/4 v2, 0x0

    .line 1252
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mUseCurrentLocation:Z

    if-eqz v0, :cond_1

    .line 1253
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getLocation()Ljava/lang/String;

    move-result-object v0

    const-string v1, "cityId:current"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1254
    const-string v0, ""

    const-string v1, "[pNA]from list, current location"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1256
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    invoke-static {v0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->isLocationAvailable(Landroid/content/Context;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1257
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    invoke-direct {p0, v0, v2, v2, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getCurrentLocation(Landroid/content/Context;ZZZ)V

    .line 1258
    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->setRefreshCnt(I)V

    .line 1270
    :cond_0
    :goto_0
    return-void

    .line 1261
    :cond_1
    const-string v0, ""

    const-string v1, "[pNA]from list, request refresh others"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1263
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getLocationCheck(Landroid/content/Context;)I

    move-result v0

    const/16 v1, 0xbb9

    if-ne v0, v1, :cond_2

    .line 1264
    const/16 v0, 0x300

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->setRefreshCnt(I)V

    goto :goto_0

    .line 1266
    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v2, v2, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->performRefreshOtherCities(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;ZZI)V

    .line 1267
    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->setRefreshCnt(I)V

    goto :goto_0
.end method

.method private performRefreshSearchMode(Landroid/os/Handler;Ljava/lang/String;)V
    .locals 8
    .param p1, "uiHandler"    # Landroid/os/Handler;
    .param p2, "location"    # Ljava/lang/String;

    .prologue
    .line 1278
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->httpThreads:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 1279
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->httpThreads:Ljava/util/ArrayList;

    .line 1282
    :cond_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    if-nez v1, :cond_1

    .line 1283
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSettingsForMain(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    .line 1286
    :cond_1
    if-eqz p1, :cond_2

    .line 1287
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mUiHandler:Landroid/os/Handler;

    .line 1290
    :cond_2
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    if-eqz v1, :cond_3

    .line 1291
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getTempScaleSetting()I

    move-result v7

    .line 1293
    .local v7, "tempScaleSetting":I
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mUrlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    .line 1294
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkLanguage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 1293
    invoke-virtual {v1, p2, v7, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeUrlForGetDetailData(Ljava/lang/String;ILjava/lang/String;)Ljava/net/URL;

    move-result-object v3

    .line 1296
    .local v3, "url":Ljava/net/URL;
    if-eqz v3, :cond_3

    .line 1297
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;-><init>(Landroid/content/Context;)V

    .line 1298
    .local v0, "httpClient":Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->httpThreads:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mUrlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeHeader()Lorg/apache/http/message/HeaderGroup;

    move-result-object v4

    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$2;

    invoke-direct {v5, p0, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;Ljava/lang/String;)V

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->get(ILandroid/content/Context;Ljava/net/URL;Lorg/apache/http/message/HeaderGroup;Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;)Ljava/lang/Thread;

    move-result-object v6

    .line 1323
    .local v6, "t":Ljava/lang/Thread;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->httpThreads:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1327
    .end local v0    # "httpClient":Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;
    .end local v3    # "url":Ljava/net/URL;
    .end local v6    # "t":Ljava/lang/Thread;
    .end local v7    # "tempScaleSetting":I
    :cond_3
    return-void
.end method

.method private stopAllThreads()V
    .locals 3

    .prologue
    .line 646
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->httpThreads:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    .line 647
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->httpThreads:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Thread;

    .line 648
    .local v0, "t":Ljava/lang/Thread;
    if-eqz v0, :cond_0

    .line 649
    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 652
    .end local v0    # "t":Ljava/lang/Thread;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->httpThreads:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 654
    :cond_2
    return-void
.end method


# virtual methods
.method public cancleRefresh()V
    .locals 3

    .prologue
    const/16 v2, 0x3f2

    .line 625
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->setRefreshCanceled(Z)V

    .line 626
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->stopAllThreads()V

    .line 627
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mMcl:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    if-eqz v0, :cond_0

    .line 628
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mMcl:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->cancelGetLastLocation(I)V

    .line 631
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getLocDataHandler:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;

    if-eqz v0, :cond_1

    .line 632
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getLocDataHandler:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;

    invoke-virtual {v0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->removeMessages(I)V

    .line 633
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getLocDataHandler:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;

    const/16 v1, 0x3f3

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->removeMessages(I)V

    .line 634
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getLocDataHandler:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;

    invoke-virtual {v0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->removeMessages(I)V

    .line 635
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getLocDataHandler:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;

    const/16 v1, 0xca

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->removeMessages(I)V

    .line 636
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getLocDataHandler:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;

    const/16 v1, 0xc8

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->removeMessages(I)V

    .line 637
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getLocDataHandler:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;

    const/16 v1, 0xc9

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->removeMessages(I)V

    .line 638
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getLocDataHandler:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;

    const/16 v1, 0x3e7

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->removeMessages(I)V

    .line 640
    :cond_1
    return-void
.end method

.method public deleteMe(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 843
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->httpThreads:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 844
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->httpThreads:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 845
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->httpThreads:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 848
    :cond_0
    return-void
.end method

.method public doManualRefresh(ILandroid/os/Handler;Ljava/lang/String;)V
    .locals 11
    .param p1, "from"    # I
    .param p2, "uiHandler"    # Landroid/os/Handler;
    .param p3, "location"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const-wide/16 v9, 0x0

    const/16 v8, 0x100

    const/4 v4, 0x0

    .line 1056
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 1057
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1059
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->updateCityList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    .line 1061
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 1062
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[dmr] from : "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ", size = "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1064
    :cond_1
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshFrom:I

    .line 1066
    if-eqz p2, :cond_3

    .line 1067
    iput-object p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mUiHandler:Landroid/os/Handler;

    .line 1072
    :goto_0
    packed-switch p1, :pswitch_data_0

    .line 1169
    :cond_2
    :goto_1
    return-void

    .line 1069
    :cond_3
    const-string v0, ""

    const-string v1, "[dmr] ui h is n."

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1075
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSettingsForMain(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    .line 1077
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_6

    .line 1078
    iput-boolean v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCanceled:Z

    .line 1079
    iput-boolean v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCompleted:Z

    .line 1083
    iput v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mResponseCnt:I

    .line 1084
    invoke-virtual {p0, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->setRefreshCnt(I)V

    .line 1086
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->nowTime:J

    .line 1087
    iput-wide v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->lastTime:J

    .line 1088
    const-string v0, "W_MR"

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->errLog:Ljava/lang/String;

    .line 1091
    if-eqz p3, :cond_5

    const-string v0, "cityId:current"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1092
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 1093
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getLocation()Ljava/lang/String;

    move-result-object v0

    const-string v1, "cityId:current"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1094
    const-string v0, ""

    const-string v1, "REFRESH_FROM_NETWORK_CHANGE update"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1095
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    move-object v0, p0

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->performNetworkAccess(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;IZZZ)V

    .line 1092
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1102
    .end local v2    # "i":I
    :cond_5
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 1103
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    move-object v0, p0

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->performNetworkAccess(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;IZZZ)V

    .line 1102
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1107
    .end local v2    # "i":I
    :cond_6
    const-string v0, ""

    const-string v1, "dMR : WIDGET : city list is null"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 1112
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_7

    .line 1113
    iput-boolean v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCanceled:Z

    .line 1114
    iput-boolean v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCompleted:Z

    .line 1116
    iput v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mResponseCnt:I

    .line 1117
    invoke-virtual {p0, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->setRefreshCnt(I)V

    .line 1119
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->nowTime:J

    .line 1120
    iput-wide v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->lastTime:J

    .line 1121
    const-string v0, "L_MR"

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->errLog:Ljava/lang/String;

    .line 1123
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_4
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 1124
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    invoke-direct {p0, v0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->performNetworkAccess(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Z)V

    .line 1123
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 1127
    .end local v2    # "i":I
    :cond_7
    const-string v0, ""

    const-string v1, "dMR : LIST : city list is null"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 1132
    :pswitch_2
    if-eqz p3, :cond_9

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_9

    .line 1133
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dMR : DETAILS : location : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1134
    iput-boolean v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCanceled:Z

    .line 1135
    iput-boolean v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCompleted:Z

    .line 1137
    iput v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mResponseCnt:I

    .line 1138
    invoke-virtual {p0, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->setRefreshCnt(I)V

    .line 1140
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->nowTime:J

    .line 1141
    iput-wide v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->lastTime:J

    .line 1142
    const-string v0, "D_MR"

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->errLog:Ljava/lang/String;

    .line 1144
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .line 1145
    .local v7, "cityIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;>;"
    :cond_8
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1146
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    .line 1147
    .local v6, "city":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getLocation()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1149
    invoke-direct {p0, v6, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->performNetworkAccess(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Z)V

    goto/16 :goto_1

    .line 1154
    .end local v6    # "city":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .end local v7    # "cityIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;>;"
    :cond_9
    const-string v0, ""

    const-string v1, "dMR : DETAILS : location or city list is null"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 1158
    :pswitch_3
    if-eqz p3, :cond_a

    .line 1159
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dMR : SEARCH_DETAILS : location : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1160
    invoke-direct {p0, p2, p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->performRefreshSearchMode(Landroid/os/Handler;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1162
    :cond_a
    const-string v0, ""

    const-string v1, "dMR : SEARCH_DETAILS : location is null"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 1072
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public getLocation()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 223
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCanceled:Z

    .line 224
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCompleted:Z

    .line 233
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshFrom:I

    .line 234
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->setRefreshCnt(I)V

    .line 235
    invoke-direct {p0, v1, v1, v1, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->performCurrentLocateForWidget(ZZZZ)V

    .line 236
    return-void
.end method

.method public getRefreshCnt()I
    .locals 1

    .prologue
    .line 896
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCnt:I

    return v0
.end method

.method public getRefreshFrom()I
    .locals 1

    .prologue
    .line 1046
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshFrom:I

    return v0
.end method

.method public httpResponse(ILjava/lang/String;IZIZLjava/lang/String;)V
    .locals 20
    .param p1, "responseCode"    # I
    .param p2, "location"    # Ljava/lang/String;
    .param p3, "tempScaleSetting"    # I
    .param p4, "bool"    # Z
    .param p5, "visibleError"    # I
    .param p6, "needRefresh"    # Z
    .param p7, "responseBody"    # Ljava/lang/String;

    .prologue
    .line 1342
    const/4 v5, 0x0

    .line 1343
    .local v5, "detailInfos":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    const/4 v4, 0x0

    .line 1344
    .local v4, "cityInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v9

    .line 1345
    .local v9, "msg":Landroid/os/Message;
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1347
    .local v3, "bundle":Landroid/os/Bundle;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mResponseCnt:I

    move/from16 v17, v0

    add-int/lit8 v17, v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mResponseCnt:I

    .line 1348
    const/16 v17, 0x300

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->setRefreshCnt(I)V

    .line 1350
    const-string v17, ""

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "[pNA!!!!!!!!] RC= "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCanceled:Z

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", sC = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "mRefreshFrom = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshFrom:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1353
    if-nez p7, :cond_0

    .line 1354
    const-string v17, ""

    const-string v18, "responseBody is null !!"

    invoke-static/range {v17 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1357
    :cond_0
    const/16 v17, 0xc8

    move/from16 v0, p1

    move/from16 v1, v17

    if-ne v0, v1, :cond_1

    if-eqz p7, :cond_1

    .line 1358
    new-instance v15, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;

    invoke-direct {v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;-><init>()V

    .line 1359
    .local v15, "parser":Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getTimestamp()Ljava/lang/String;

    move-result-object v16

    .line 1360
    .local v16, "updateDate":Ljava/lang/String;
    move/from16 v0, p3

    move-object/from16 v1, p7

    move-object/from16 v2, v16

    invoke-virtual {v15, v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->parseDetailWeather(ILjava/lang/String;Ljava/lang/String;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    move-result-object v5

    .line 1361
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    move-object/from16 v17, v0

    move-object/from16 v0, p7

    move-object/from16 v1, v17

    invoke-virtual {v15, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->parseDetailWeatherLocationCity(Ljava/lang/String;Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    move-result-object v4

    .line 1364
    .end local v15    # "parser":Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;
    .end local v16    # "updateDate":Ljava/lang/String;
    :cond_1
    if-eqz v5, :cond_8

    if-eqz v4, :cond_8

    .line 1365
    move-object v6, v5

    .line 1366
    .local v6, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setProvider(I)V

    .line 1367
    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setRealLocation(Ljava/lang/String;)V

    .line 1369
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    move-object/from16 v2, p7

    invoke-static {v0, v1, v4, v6, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateCity(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)I

    .line 1370
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    invoke-static {v0, v1, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateDetailHourInfo(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)I

    .line 1371
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->isRegisteredToDetailHourInfo(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 1372
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateDetailHourInfo(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)I

    .line 1376
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getRefreshCnt()I

    move-result v17

    if-gtz v17, :cond_2

    .line 1377
    const-string v17, ""

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "ct "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getRefreshCnt()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1378
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    move-object/from16 v17, v0

    const/16 v18, -0x1

    const-string v19, "RM HM 3"

    invoke-static/range {v17 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/BManager;->sendBroadCastToProviderAfterCheckingSupportClock(Landroid/content/Context;ILjava/lang/String;)V

    .line 1408
    :cond_2
    const-string v17, ""

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "mRC "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mResponseCnt:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1409
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    if-eqz v17, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v17

    if-lez v17, :cond_3

    .line 1410
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->clear()V

    .line 1412
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->updateCityList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    .line 1414
    if-eqz p4, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    if-eqz v17, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mResponseCnt:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_4

    .line 1416
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;->checkSameDaemonCityIdAtDate(Landroid/content/Context;)V

    .line 1422
    :cond_4
    if-eqz p6, :cond_5

    .line 1423
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v13

    .line 1424
    .local v13, "now":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getNextRefreshTime(Landroid/content/Context;)J

    move-result-wide v7

    .line 1425
    .local v7, "lasttime":J
    cmp-long v17, v7, v13

    if-gtz v17, :cond_e

    const/4 v10, 0x1

    .line 1426
    .local v10, "need_refresh":Z
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v0, v1, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getNextTime(Landroid/content/Context;ZZ)J

    move-result-wide v11

    .line 1427
    .local v11, "nextTime":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-static {v0, v11, v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateNextTime(Landroid/content/Context;J)I

    .line 1430
    .end local v7    # "lasttime":J
    .end local v10    # "need_refresh":Z
    .end local v11    # "nextTime":J
    .end local v13    # "now":J
    :cond_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshFrom:I

    move/from16 v17, v0

    if-nez v17, :cond_f

    .line 1431
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCnt:I

    move/from16 v17, v0

    if-gtz v17, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mUiHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    if-eqz v17, :cond_6

    .line 1432
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mUiHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    const/16 v18, 0x20

    invoke-virtual/range {v17 .. v18}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1445
    .end local v6    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    :cond_6
    :goto_2
    return-void

    .line 1374
    .restart local v6    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->insertDetailHourInfo(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)I

    goto/16 :goto_0

    .line 1381
    .end local v6    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    :cond_8
    if-eqz p6, :cond_9

    .line 1382
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v13

    .line 1383
    .restart local v13    # "now":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getNextRefreshTime(Landroid/content/Context;)J

    move-result-wide v7

    .line 1384
    .restart local v7    # "lasttime":J
    cmp-long v17, v7, v13

    if-gtz v17, :cond_b

    const/4 v10, 0x1

    .line 1385
    .restart local v10    # "need_refresh":Z
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v0, v1, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getNextTime(Landroid/content/Context;ZZ)J

    move-result-wide v11

    .line 1386
    .restart local v11    # "nextTime":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-static {v0, v11, v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateNextTime(Landroid/content/Context;J)I

    .line 1389
    .end local v7    # "lasttime":J
    .end local v10    # "need_refresh":Z
    .end local v11    # "nextTime":J
    .end local v13    # "now":J
    :cond_9
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshFrom:I

    move/from16 v17, v0

    if-eqz v17, :cond_a

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshFrom:I

    move/from16 v17, v0

    const/16 v18, 0x5

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_c

    .line 1391
    :cond_a
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCnt:I

    move/from16 v17, v0

    if-gtz v17, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mUiHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    if-eqz v17, :cond_6

    .line 1392
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mUiHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    const/16 v18, 0x20

    invoke-virtual/range {v17 .. v18}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_2

    .line 1384
    .restart local v7    # "lasttime":J
    .restart local v13    # "now":J
    :cond_b
    const/4 v10, 0x0

    goto :goto_3

    .line 1396
    .end local v7    # "lasttime":J
    .end local v13    # "now":J
    :cond_c
    const/16 v17, 0xc8

    move/from16 v0, p1

    move/from16 v1, v17

    if-ne v0, v1, :cond_d

    .line 1397
    const/16 p1, -0x1

    .line 1399
    :cond_d
    const-string v17, "RESPONSE_CODE"

    move-object/from16 v0, v17

    move/from16 v1, p1

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1400
    const-string v17, "LOCATION_NAME"

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1401
    const-string v17, "SEARCH_MODE"

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1402
    invoke-virtual {v9, v3}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1403
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mUiHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_2

    .line 1425
    .restart local v6    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .restart local v7    # "lasttime":J
    .restart local v13    # "now":J
    :cond_e
    const/4 v10, 0x0

    goto/16 :goto_1

    .line 1436
    .end local v7    # "lasttime":J
    .end local v13    # "now":J
    :cond_f
    const-string v17, "RESPONSE_CODE"

    move-object/from16 v0, v17

    move/from16 v1, p1

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1437
    const-string v17, "LOCATION_NAME"

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1438
    const-string v17, "SEARCH_MODE"

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1439
    invoke-virtual {v9, v3}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1440
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mUiHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_2
.end method

.method public init()V
    .locals 2

    .prologue
    .line 136
    const-string v0, ""

    const-string v1, " V init "

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    return-void
.end method

.method public isRefreshCanceled()Z
    .locals 1

    .prologue
    .line 920
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCanceled:Z

    return v0
.end method

.method public isRefreshCompleted()Z
    .locals 1

    .prologue
    .line 904
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCompleted:Z

    return v0
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    .line 97
    const-string v1, ""

    const-string v2, " onDestroy "

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mMcl:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    if-eqz v1, :cond_0

    .line 99
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mMcl:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->cancelGetLastLocation(I)V

    .line 100
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getRefreshCnt()I

    move-result v1

    if-lez v1, :cond_0

    .line 101
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ct "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getRefreshCnt()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getRefreshCnt()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 103
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "lit sze "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    const/4 v2, -0x1

    const-string v3, "RM DES"

    invoke-static {v1, v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/BManager;->sendBroadCastToProviderAfterCheckingSupportClock(Landroid/content/Context;ILjava/lang/String;)V

    .line 110
    :cond_0
    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->stopAllThreads()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 113
    :try_start_1
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->cleanResource()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 120
    :goto_0
    return-void

    .line 114
    :catch_0
    move-exception v0

    .line 115
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v1, ""

    invoke-virtual {v0}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 117
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 118
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v1, ""

    invoke-virtual {v0}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onFirstUpdate(II)Z
    .locals 10
    .param p1, "weatherCount"    # I
    .param p2, "widgetType"    # I

    .prologue
    const-wide/16 v8, 0x0

    const/16 v7, 0xbbb

    const/16 v6, 0xbb9

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 147
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getFirstLaunch(Landroid/content/Context;)Z

    move-result v3

    sput-boolean v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mFirstUpdate:Z

    .line 148
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshFrom:I

    .line 150
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " onFirstUpdate :: mFU = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-boolean v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mFirstUpdate:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    sget-boolean v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mFirstUpdate:Z

    if-nez v3, :cond_1

    .line 219
    :cond_0
    :goto_0
    return v1

    .line 156
    :cond_1
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    if-nez v3, :cond_2

    .line 157
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSettingsForMain(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    .line 160
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->nowTime:J

    .line 161
    iput-wide v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->lastTime:J

    .line 162
    const-string v3, "FR"

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->errLog:Ljava/lang/String;

    .line 164
    packed-switch p2, :pswitch_data_0

    goto :goto_0

    .line 166
    :pswitch_0
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    if-eqz v3, :cond_0

    if-nez p1, :cond_0

    .line 167
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCanceled:Z

    .line 169
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->nowTime:J

    .line 170
    iput-wide v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->lastTime:J

    .line 171
    const-string v3, "FR"

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->errLog:Ljava/lang/String;

    .line 173
    invoke-direct {p0, v2, v1, v1, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->performCurrentLocateForWidget(ZZZZ)V

    .line 174
    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->setRefreshCnt(I)V

    move v1, v2

    .line 177
    goto :goto_0

    .line 181
    :pswitch_1
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    if-eqz v3, :cond_0

    if-nez p1, :cond_0

    .line 182
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCanceled:Z

    .line 183
    sput-boolean v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mFirstUpdate:Z

    .line 184
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    sget-boolean v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mFirstUpdate:Z

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setFirstLaunch(Landroid/content/Context;Z)Z

    .line 186
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    invoke-static {v3, v2, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getLocationCheck(Landroid/content/Context;ZZ)I

    move-result v0

    .line 187
    .local v0, "result":I
    if-eq v0, v6, :cond_3

    if-ne v0, v7, :cond_4

    .line 189
    :cond_3
    const-string v2, ""

    const-string v3, "ey Fst NOTNET"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 194
    :cond_4
    invoke-direct {p0, v2, v1, v1, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->performCurrentLocateForWidget(ZZZZ)V

    move v1, v2

    .line 195
    goto :goto_0

    .line 199
    .end local v0    # "result":I
    :pswitch_2
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    if-eqz v3, :cond_0

    if-nez p1, :cond_0

    .line 200
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCanceled:Z

    .line 201
    sput-boolean v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mFirstUpdate:Z

    .line 202
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    sget-boolean v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mFirstUpdate:Z

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setFirstLaunch(Landroid/content/Context;Z)Z

    .line 204
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    invoke-static {v3, v2, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getLocationCheck(Landroid/content/Context;ZZ)I

    move-result v0

    .line 205
    .restart local v0    # "result":I
    if-eq v0, v6, :cond_5

    if-ne v0, v7, :cond_6

    .line 207
    :cond_5
    const-string v2, ""

    const-string v3, "ey Fst NOTNET"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.sec.android.widgetapp.ap.hero.accuweather.widget.action.APPWIDGET_UPDATE"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 211
    :cond_6
    invoke-direct {p0, v2, v1, v1, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->performCurrentLocateForWidget(ZZZZ)V

    move v1, v2

    .line 212
    goto/16 :goto_0

    .line 164
    nop

    :pswitch_data_0
    .packed-switch 0xfa0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public performRefreshOtherCities(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;ZZI)V
    .locals 1
    .param p1, "detailWeatherInfo"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .param p2, "bool"    # Z
    .param p3, "needRefresh"    # Z
    .param p4, "visibleError"    # I

    .prologue
    .line 834
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCanceled:Z

    .line 835
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->makeHttpClient(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;ZZI)V

    .line 836
    return-void
.end method

.method public setRefreshCanceled(Z)V
    .locals 0
    .param p1, "refreshCanceled"    # Z

    .prologue
    .line 928
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCanceled:Z

    .line 929
    return-void
.end method

.method public setRefreshCnt(I)V
    .locals 3
    .param p1, "setVal"    # I

    .prologue
    .line 872
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[RM] sRC : setVal = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",  previous cnt = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCnt:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", from = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshFrom:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 874
    sparse-switch p1, :sswitch_data_0

    .line 888
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCnt:I

    .line 889
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[RM]set R cnt = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCnt:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 893
    :goto_0
    return-void

    .line 876
    :sswitch_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCnt:I

    .line 877
    const-string v0, ""

    const-string v1, "[RM]Init R cnt = 0"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 880
    :sswitch_1
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCnt:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCnt:I

    .line 881
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[RM]IR R cnt = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCnt:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 884
    :sswitch_2
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCnt:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCnt:I

    .line 885
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[RM]DR R cnt = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCnt:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 874
    nop

    :sswitch_data_0
    .sparse-switch
        0x100 -> :sswitch_0
        0x200 -> :sswitch_1
        0x300 -> :sswitch_2
    .end sparse-switch
.end method

.method public setRefreshCompleted(Z)V
    .locals 0
    .param p1, "refreshCompleted"    # Z

    .prologue
    .line 912
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCompleted:Z

    .line 913
    return-void
.end method

.method public updateCityList(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 128
    const-string v0, ""

    const-string v1, "updateCityList"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    invoke-static {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getAllDetailWeatherInfo(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method
