.class Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;
.super Ljava/lang/Object;
.source "MenuSettingsUseCurrentLocation.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->onCreateOptionsMenu(Landroid/view/Menu;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;

    .prologue
    .line 213
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4
    .param p1, "view"    # Landroid/widget/CompoundButton;
    .param p2, "value"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 215
    if-eqz p2, :cond_1

    .line 216
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->isAgreeUseLocation(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 230
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setIsAgreeUseLocation(Landroid/content/Context;Z)V

    .line 231
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->setSwitchStatus()V

    .line 337
    :cond_0
    :goto_0
    return-void

    .line 306
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getCheckCurrentCityLocation(Landroid/content/Context;)I

    move-result v1

    if-ne v1, v3, :cond_2

    .line 307
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;)Landroid/content/Context;

    move-result-object v1

    const/16 v2, 0x3fc

    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2$6;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2$6;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;)V

    invoke-static {v1, v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->showDialog(Landroid/content/Context;ILcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)Landroid/app/Dialog;

    move-result-object v0

    .line 321
    .local v0, "mPopupDialog":Landroid/app/Dialog;
    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2$7;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2$7;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    goto :goto_0

    .line 327
    .end local v0    # "mPopupDialog":Landroid/app/Dialog;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateCheckCurrentCityLocation(Landroid/content/Context;I)I

    .line 329
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setIsAgreeUseLocation(Landroid/content/Context;Z)V

    .line 330
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;

    iget-object v1, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->switchView:Landroid/widget/Switch;

    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setChecked(Z)V

    .line 331
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getNotificationSettings(Landroid/content/Context;)I

    move-result v1

    if-ne v1, v3, :cond_3

    .line 332
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->unregisterNotiTime(Landroid/content/Context;)V

    .line 334
    :cond_3
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.android.widgetapp.ap.hero.accuweather.action.CHANGE_SETTING"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method
