.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$ImageGetter;
.super Ljava/lang/Object;
.source "ScenManager.java"

# interfaces
.implements Landroid/text/Html$ImageGetter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ImageGetter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;


# direct methods
.method public constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    .prologue
    .line 812
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$ImageGetter;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 5
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 814
    const/4 v1, 0x0

    .line 815
    .local v1, "id":I
    const-string v2, "icon"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 816
    const v1, 0x7f02013a

    .line 818
    :cond_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager$ImageGetter;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/ScenManager;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 819
    .local v0, "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v0, v4, v4, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 820
    return-object v0
.end method
