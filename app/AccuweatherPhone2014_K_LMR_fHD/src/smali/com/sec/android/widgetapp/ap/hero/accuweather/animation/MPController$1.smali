.class Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$1;
.super Ljava/lang/Object;
.source "MPController.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->playBGFadeIn()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    .prologue
    .line 376
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 392
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;->setBGAlphaListener(Z)V

    .line 393
    const-string v0, ""

    const-string v1, "BG fade in animation end"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 389
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 379
    const-string v0, ""

    const-string v1, "BG fade in animation start"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 382
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;->setBGVisibilityListener(Z)V

    .line 385
    :cond_0
    return-void
.end method
