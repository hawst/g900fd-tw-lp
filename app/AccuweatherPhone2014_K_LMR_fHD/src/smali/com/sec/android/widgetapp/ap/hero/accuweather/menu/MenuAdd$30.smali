.class Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;
.super Landroid/os/Handler;
.source "MenuAdd.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 1311
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    const/4 v10, 0x0

    const v9, 0x7f03002e

    const/16 v8, 0x3e7

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1313
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "LDH Gbtn "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1314
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "LDH rc "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1315
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x3f3

    if-ne v2, v3, :cond_1

    .line 1316
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    const-class v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1318
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "flags"

    const/16 v3, -0x7530

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1319
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->hideLoadingDialog()V
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    .line 1320
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    invoke-virtual {v2, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->startActivity(Landroid/content/Intent;)V

    .line 1321
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->finish()V

    .line 1410
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 1322
    :cond_1
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0xca

    if-eq v2, v3, :cond_2

    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0x3f2

    if-ne v2, v3, :cond_3

    .line 1325
    :cond_2
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->hideLoadingDialog()V
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$2100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    .line 1326
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->isActivityVisible()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1327
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    const/16 v4, 0x3f7

    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30$1;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;)V

    invoke-static {v3, v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->showDialog(Landroid/content/Context;ILcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)Landroid/app/Dialog;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mPopupDialog:Landroid/app/Dialog;

    goto :goto_0

    .line 1340
    :cond_3
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0xc8

    if-ne v2, v3, :cond_a

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->isActivityVisible()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1341
    const-string v2, ""

    const-string v3, "GET_CURRENT_LOCATION_OK"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1342
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "cityinfo"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    .line 1344
    .local v0, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->COLLAB_MODE:Z
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$3100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1346
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->lstCity:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$3200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 1347
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->lstCity:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$3200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 1351
    :goto_1
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->lstCity:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$3200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1353
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->lstCity:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$3200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_5

    .line 1354
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .line 1355
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->lstCity:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$3200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-direct {v3, v4, v9, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    .line 1354
    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->cityListAdapter2:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;
    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$3302(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;)Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;

    .line 1356
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->setKeyboardVisible(Z)V
    invoke-static {v2, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$1200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;Z)V

    .line 1357
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getListView()Landroid/widget/ListView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->cityListAdapter2:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$3300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1358
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getListView()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 1359
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getListView()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/widget/ListView;->setVisibility(I)V

    .line 1360
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->cityListAdapter2:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$3300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;->notifyDataSetChanged()V

    .line 1370
    :goto_2
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->hideAllDialog()V
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$3000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    goto/16 :goto_0

    .line 1349
    :cond_4
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->lstCity:Ljava/util/ArrayList;
    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$3202(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    goto :goto_1

    .line 1363
    :cond_5
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mNoSearchText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$1700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1364
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mNoSearchText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$1700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d0027

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1367
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getListView()Landroid/widget/ListView;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 1368
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->setKeyboardVisible(Z)V
    invoke-static {v2, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$1200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;Z)V

    goto :goto_2

    .line 1374
    :cond_6
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->lstCity:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$3200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_9

    .line 1375
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->lstCity:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$3200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 1378
    :goto_3
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->lstCity:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$3200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1380
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mNoSearchText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$1700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/widget/TextView;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 1381
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mNoSearchText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$1700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/widget/TextView;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1387
    :cond_7
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/widget/AutoCompleteTextView;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 1388
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/widget/AutoCompleteTextView;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1391
    :cond_8
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .line 1392
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->lstCity:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$3200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-direct {v3, v4, v9, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    .line 1391
    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->cityListAdapter2:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;
    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$3302(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;)Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;

    .line 1393
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getListView()Landroid/widget/ListView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->cityListAdapter2:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$3300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1394
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getListView()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 1395
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getListView()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/widget/ListView;->setVisibility(I)V

    .line 1397
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->cityListAdapter2:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$3300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;->notifyDataSetChanged()V

    .line 1398
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->hideAllDialog()V
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$3000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    goto/16 :goto_0

    .line 1377
    :cond_9
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->lstCity:Ljava/util/ArrayList;
    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$3202(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    goto :goto_3

    .line 1399
    .end local v0    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    :cond_a
    iget v2, p1, Landroid/os/Message;->what:I

    const/16 v3, 0xc9

    if-ne v2, v3, :cond_b

    .line 1400
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->hideAllDialog()V
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$3000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    .line 1401
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->isActivityVisible()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1402
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->showGPSoffDialog(Landroid/app/Activity;)Landroid/app/Dialog;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mPopupDialog:Landroid/app/Dialog;

    goto/16 :goto_0

    .line 1405
    :cond_b
    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v2, v8, :cond_0

    .line 1406
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->hideAllDialog()V
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$3000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    .line 1407
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    invoke-virtual {v2, v8, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->setResult(ILandroid/content/Intent;)V

    .line 1408
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->finish()V

    goto/16 :goto_0
.end method
