.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;
.super Landroid/app/ListActivity;
.source "MenuDefaultLocation.java"

# interfaces
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/model/IActivityVisibleState;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$WeatherClockListAdapter;
    }
.end annotation


# instance fields
.field private actionbar:Landroid/app/ActionBar;

.field private actionbarBack:Landroid/widget/LinearLayout;

.field private actionbarLayout:Landroid/widget/RelativeLayout;

.field private actionbarTitle:Landroid/widget/TextView;

.field private backKey:Landroid/widget/ImageView;

.field private defaultLocation:Ljava/lang/String;

.field private doneView:Landroid/view/View;

.field private mContext:Landroid/content/Context;

.field private mDefaultLocationAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$WeatherClockListAdapter;

.field private mInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;",
            ">;"
        }
    .end annotation
.end field

.field private mIsVisible:Z

.field private title:Landroid/widget/TextView;

.field private topMenu:Landroid/widget/RelativeLayout;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 44
    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    .line 54
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->doneView:Landroid/view/View;

    .line 60
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->actionbarTitle:Landroid/widget/TextView;

    .line 274
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->defaultLocation:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->defaultLocation:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->mInfoList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;)Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$WeatherClockListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->mDefaultLocationAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$WeatherClockListAdapter;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public cleanResource()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 256
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->title:Landroid/widget/TextView;

    .line 257
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->actionbar:Landroid/app/ActionBar;

    .line 258
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->topMenu:Landroid/widget/RelativeLayout;

    .line 259
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->mDefaultLocationAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$WeatherClockListAdapter;

    if-eqz v0, :cond_0

    .line 260
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->mDefaultLocationAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$WeatherClockListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$WeatherClockListAdapter;->clear()V

    .line 261
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->mDefaultLocationAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$WeatherClockListAdapter;

    .line 263
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->mInfoList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 264
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->mInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 265
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->mInfoList:Ljava/util/ArrayList;

    .line 267
    :cond_1
    return-void
.end method

.method public isActivityVisible()Z
    .locals 1

    .prologue
    .line 241
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->mIsVisible:Z

    return v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 249
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 250
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v3, 0x400

    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 70
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 71
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v3, v3}, Landroid/view/Window;->setFlags(II)V

    .line 73
    const v2, 0x7f030030

    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->setContentView(I)V

    .line 74
    iput-object p0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->mContext:Landroid/content/Context;

    .line 76
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->actionbar:Landroid/app/ActionBar;

    if-nez v2, :cond_0

    .line 77
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->actionbar:Landroid/app/ActionBar;

    .line 78
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->actionbar:Landroid/app/ActionBar;

    if-eqz v2, :cond_0

    .line 79
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02008f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 81
    const v2, 0x7f030005

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->actionbarLayout:Landroid/widget/RelativeLayout;

    .line 83
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->actionbar:Landroid/app/ActionBar;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->actionbarLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 84
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->actionbarLayout:Landroid/widget/RelativeLayout;

    const v3, 0x7f080008

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->actionbarTitle:Landroid/widget/TextView;

    .line 85
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->actionbarLayout:Landroid/widget/RelativeLayout;

    const v3, 0x7f080006

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->actionbarBack:Landroid/widget/LinearLayout;

    .line 87
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->actionbarBack:Landroid/widget/LinearLayout;

    const v3, 0x7f080009

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 88
    .local v1, "v":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200aa

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 89
    .local v0, "backIcon":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0, v6}, Landroid/graphics/drawable/Drawable;->setAutoMirrored(Z)V

    .line 90
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 92
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v5}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 93
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v5}, Landroid/app/ActionBar;->setDisplayUseLogoEnabled(Z)V

    .line 94
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v5}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 95
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v6}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 97
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->actionbarTitle:Landroid/widget/TextView;

    const v3, 0x7f0d00d1

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 99
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->actionbarBack:Landroid/widget/LinearLayout;

    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$1;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;)V

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->actionbarBack:Landroid/widget/LinearLayout;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d008b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->mContext:Landroid/content/Context;

    .line 107
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d0097

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 105
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 111
    .end local v0    # "backIcon":Landroid/graphics/drawable/Drawable;
    .end local v1    # "v":Landroid/widget/ImageView;
    :cond_0
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->setContext(Landroid/content/Context;)V

    .line 112
    invoke-virtual {p0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->setActivityVisibleState(Z)V

    .line 114
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSelectedDefaultLocation(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->defaultLocation:Ljava/lang/String;

    .line 115
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->getListView()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 116
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getAllCityList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->mInfoList:Ljava/util/ArrayList;

    .line 117
    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$WeatherClockListAdapter;

    const v3, 0x7f030031

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->mInfoList:Ljava/util/ArrayList;

    invoke-direct {v2, p0, p0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$WeatherClockListAdapter;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->mDefaultLocationAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$WeatherClockListAdapter;

    .line 120
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->mDefaultLocationAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$WeatherClockListAdapter;

    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 122
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->getListView()Landroid/widget/ListView;

    move-result-object v2

    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$2;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 128
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 136
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 137
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 148
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f0002

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 151
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->doneView:Landroid/view/View;

    .line 154
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->doneView:Landroid/view/View;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$3;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$3;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 201
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 224
    invoke-super {p0}, Landroid/app/ListActivity;->onDestroy()V

    .line 225
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->cleanResource()V

    .line 226
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 216
    invoke-super {p0}, Landroid/app/ListActivity;->onPause()V

    .line 217
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->setActivityVisibleState(Z)V

    .line 218
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 208
    invoke-super {p0}, Landroid/app/ListActivity;->onResume()V

    .line 209
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->setActivityVisibleState(Z)V

    .line 210
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 337
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 338
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->finish()V

    .line 340
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public setActivityVisibleState(Z)V
    .locals 0
    .param p1, "onoff"    # Z

    .prologue
    .line 233
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->mIsVisible:Z

    .line 234
    return-void
.end method
