.class Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView$1;
.super Ljava/lang/Object;
.source "ThunderShowerIconView.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;

    .prologue
    .line 541
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 547
    invoke-virtual {p1}, Landroid/animation/Animator;->removeAllListeners()V

    .line 548
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 543
    invoke-virtual {p1}, Landroid/animation/Animator;->removeAllListeners()V

    .line 544
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    const/4 v1, 0x0

    .line 552
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->isStop:Z

    if-eqz v0, :cond_0

    .line 553
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->DropTy1:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->DropTy1:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 554
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->DropTy1:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 555
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;

    const-string v1, "e_"

    iput-object v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->mPreFixProperty1:Ljava/lang/String;

    .line 574
    :cond_0
    :goto_0
    return-void

    .line 556
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->DropTy2:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->DropTy2:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 557
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->DropTy2:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 558
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;

    const-string v1, "e_"

    iput-object v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->mPreFixProperty2:Ljava/lang/String;

    goto :goto_0

    .line 559
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->DropTy4:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->DropTy4:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 560
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->DropTy4:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 561
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;

    const-string v1, "e_"

    iput-object v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->mPreFixProperty4:Ljava/lang/String;

    goto :goto_0

    .line 562
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->DropTy5:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->DropTy5:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 563
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->DropTy5:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 564
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;

    const-string v1, "e_"

    iput-object v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->mPreFixProperty5:Ljava/lang/String;

    goto :goto_0

    .line 565
    :cond_4
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->DropThunderTy3:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->DropThunderTy3:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 566
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->DropThunderTy3:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 567
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;

    const-string v1, "e_"

    iput-object v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->mPreFixthunder:Ljava/lang/String;

    goto/16 :goto_0

    .line 568
    :cond_5
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->IconTranslateX:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->IconTranslateX:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 569
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->IconTranslateX:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    goto/16 :goto_0

    .line 570
    :cond_6
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->CloudTranslate:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->CloudTranslate:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 571
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->CloudTranslate:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;->access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ThunderShowerIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    goto/16 :goto_0
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 549
    return-void
.end method
