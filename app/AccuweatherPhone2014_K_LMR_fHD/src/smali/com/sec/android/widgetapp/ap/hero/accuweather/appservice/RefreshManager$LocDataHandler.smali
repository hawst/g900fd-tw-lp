.class Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;
.super Landroid/os/Handler;
.source "RefreshManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "LocDataHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    .prologue
    .line 305
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 27
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 307
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    const/16 v24, 0x300

    invoke-virtual/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->setRefreshCnt(I)V

    .line 308
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mMcl:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    move-result-object v23

    if-eqz v23, :cond_0

    .line 309
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mMcl:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->removeLocationListener()V

    .line 310
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mMcl:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->removePassiveLocationListener()V

    .line 312
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    if-eqz v23, :cond_4

    .line 313
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getFirstLaunch(Landroid/content/Context;)Z

    move-result v23

    if-eqz v23, :cond_1

    .line 314
    const-string v23, ""

    const-string v24, "LocDH ---> fst lanuch set f"

    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    const-string v24, "EFFECT_40"

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v25, v0

    invoke-static/range {v23 .. v25}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateViEffectSettings(Landroid/content/Context;Ljava/lang/String;I)I

    .line 316
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    const/16 v24, 0x0

    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setFirstLaunch(Landroid/content/Context;Z)Z

    .line 319
    :cond_1
    const-string v23, ""

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, " [LocHd] >>> "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, ", from = "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshFrom:I
    invoke-static/range {v25 .. v25}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)I

    move-result v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    const/4 v9, 0x0

    .line 321
    .local v9, "intent":Landroid/content/Intent;
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v23, v0

    sparse-switch v23, :sswitch_data_0

    .line 587
    :cond_2
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshFrom:I
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)I

    move-result v23

    if-nez v23, :cond_24

    .line 588
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v23, v0

    const/16 v24, 0xc8

    move/from16 v0, v23

    move/from16 v1, v24

    if-eq v0, v1, :cond_3

    .line 589
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v23

    const-string v24, "responseCode"

    invoke-virtual/range {v23 .. v24}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v18

    .line 590
    .local v18, "responseCode":I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v23

    const-string v24, "provider"

    invoke-virtual/range {v23 .. v24}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 591
    .local v17, "provider":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "lT:"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->lastTime:J
    invoke-static/range {v25 .. v25}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$900(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)J

    move-result-wide v25

    invoke-virtual/range {v24 .. v26}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, ", "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->errLog:Ljava/lang/String;
    invoke-static/range {v25 .. v25}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$800(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, ", P:"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, ", rC:"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, ", msg:"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->errLog:Ljava/lang/String;
    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$802(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;Ljava/lang/String;)Ljava/lang/String;

    .line 593
    const-string v23, ""

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "CL failed : "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 594
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->nowTime:J
    invoke-static/range {v24 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$1000(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)J

    move-result-wide v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v26, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->errLog:Ljava/lang/String;
    invoke-static/range {v26 .. v26}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$800(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v23 .. v26}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->insertLog(Landroid/content/Context;JLjava/lang/String;)I

    .line 595
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCnt:I
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)I

    move-result v23

    if-gtz v23, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mUiHandler:Landroid/os/Handler;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/os/Handler;

    move-result-object v23

    if-eqz v23, :cond_3

    .line 596
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mUiHandler:Landroid/os/Handler;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/os/Handler;

    move-result-object v23

    const/16 v24, 0x20

    invoke-virtual/range {v23 .. v24}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 610
    .end local v17    # "provider":Ljava/lang/String;
    .end local v18    # "responseCode":I
    :cond_3
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getRefreshCnt()I

    move-result v23

    if-gtz v23, :cond_4

    .line 611
    const-string v23, ""

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "ct "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getRefreshCnt()I

    move-result v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 612
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    const/16 v24, -0x1

    const-string v25, "RM HM 1"

    invoke-static/range {v23 .. v25}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/BManager;->sendBroadCastToProviderAfterCheckingSupportClock(Landroid/content/Context;ILjava/lang/String;)V

    .line 616
    .end local v9    # "intent":Landroid/content/Intent;
    :cond_4
    :goto_2
    return-void

    .line 323
    .restart local v9    # "intent":Landroid/content/Intent;
    :sswitch_0
    new-instance v9, Landroid/content/Intent;

    .end local v9    # "intent":Landroid/content/Intent;
    const-string v23, "com.sec.android.widgetapp.ap.hero.accuweather.action.GET_CURRENT_LOCATION_ERROR"

    move-object/from16 v0, v23

    invoke-direct {v9, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 324
    .restart local v9    # "intent":Landroid/content/Intent;
    const-string v23, "AUTO_REFRESH_WHERE_FROM"

    const/16 v24, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 326
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 329
    :sswitch_1
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v23

    const-string v24, "isFristRetry"

    invoke-virtual/range {v23 .. v24}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v11

    .line 330
    .local v11, "isFristRetry":Z
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v23

    const-string v24, "isAutoRefresh"

    invoke-virtual/range {v23 .. v24}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v10

    .line 331
    .local v10, "isAutoRefresh":Z
    const-string v23, ""

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "GET_DATA_NETWORK_ERROR : isFristRetry : "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, ", isAutoRefresh : "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    if-eqz v10, :cond_5

    .line 334
    if-eqz v11, :cond_6

    .line 335
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    const/16 v24, 0x1

    const/16 v25, 0x0

    invoke-static/range {v23 .. v25}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setUpdateTimeToPref(Landroid/content/Context;ZZ)V

    .line 341
    :cond_5
    :goto_3
    new-instance v9, Landroid/content/Intent;

    .end local v9    # "intent":Landroid/content/Intent;
    const-string v23, "com.sec.android.widgetapp.ap.hero.accuweather.action.GET_CURRENT_LOCATION_ERROR"

    move-object/from16 v0, v23

    invoke-direct {v9, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 342
    .restart local v9    # "intent":Landroid/content/Intent;
    const-string v23, "AUTO_REFRESH_WHERE_FROM"

    const/16 v24, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 344
    const-string v23, "isFristRetry"

    move-object/from16 v0, v23

    invoke-virtual {v9, v0, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 345
    const-string v23, "isAutoRefresh"

    move-object/from16 v0, v23

    invoke-virtual {v9, v0, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 346
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v23

    const-string v24, "responseCode"

    invoke-virtual/range {v23 .. v24}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v18

    .line 347
    .restart local v18    # "responseCode":I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v23

    const-string v24, "provider"

    invoke-virtual/range {v23 .. v24}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 348
    .restart local v17    # "provider":Ljava/lang/String;
    const-string v23, "responseCode"

    move-object/from16 v0, v23

    move/from16 v1, v18

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 349
    const-string v23, "provider"

    move-object/from16 v0, v23

    move-object/from16 v1, v17

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 350
    const-string v23, "msg"

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v24, v0

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 351
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 337
    .end local v17    # "provider":Ljava/lang/String;
    .end local v18    # "responseCode":I
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    const/16 v24, 0x0

    const/16 v25, 0x0

    invoke-static/range {v23 .. v25}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setUpdateTimeToPref(Landroid/content/Context;ZZ)V

    goto :goto_3

    .line 354
    .end local v10    # "isAutoRefresh":Z
    .end local v11    # "isFristRetry":Z
    :sswitch_2
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v23

    const-string v24, "cityinfo"

    invoke-virtual/range {v23 .. v24}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v8

    check-cast v8, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    .line 355
    .local v8, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v23

    const-string v24, "cityxmlinfo"

    invoke-virtual/range {v23 .. v24}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 356
    .local v21, "xmlinfo":Ljava/lang/String;
    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    invoke-direct {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;-><init>()V

    .line 357
    .local v5, "dinfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v23

    const-string v24, "today"

    .line 358
    invoke-virtual/range {v23 .. v24}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v23

    check-cast v23, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    .line 357
    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->setTodayWeatherInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;)V

    .line 360
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_4
    const/16 v23, 0x7

    move/from16 v0, v23

    if-ge v7, v0, :cond_8

    .line 361
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v23

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v23

    if-eqz v23, :cond_7

    .line 362
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v23

    .line 363
    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v23

    check-cast v23, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    .line 362
    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addForecastInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;)V

    .line 360
    :cond_7
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 367
    :cond_8
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v23

    const-string v24, "detailinfo"

    invoke-virtual/range {v23 .. v24}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v23

    check-cast v23, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    .line 368
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfoSize()I

    move-result v20

    .line 369
    .local v20, "size":I
    const/4 v12, 0x0

    .local v12, "j":I
    :goto_5
    move/from16 v0, v20

    if-ge v12, v0, :cond_a

    .line 370
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v23

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v23

    if-eqz v23, :cond_9

    .line 371
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v23

    const-string v24, "detailinfo"

    .line 372
    invoke-virtual/range {v23 .. v24}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v23

    check-cast v23, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    move-object/from16 v0, v23

    invoke-virtual {v0, v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;

    move-result-object v23

    .line 371
    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addPhotosInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;)V

    .line 369
    :cond_9
    add-int/lit8 v12, v12, 0x1

    goto :goto_5

    .line 376
    :cond_a
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v23

    const-string v24, "detailinfo"

    invoke-virtual/range {v23 .. v24}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v23

    check-cast v23, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    .line 377
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getHourSize()I

    move-result v15

    .line 378
    .local v15, "num":I
    const/16 v22, 0x0

    .local v22, "z":I
    :goto_6
    move/from16 v0, v22

    if-ge v0, v15, :cond_b

    .line 379
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v23

    const-string v24, "detailinfo"

    .line 380
    invoke-virtual/range {v23 .. v24}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v23

    check-cast v23, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    move-object/from16 v0, v23

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getHourInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;

    move-result-object v23

    .line 379
    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addHourInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 378
    add-int/lit8 v22, v22, 0x1

    goto :goto_6

    .line 382
    .end local v12    # "j":I
    .end local v15    # "num":I
    .end local v20    # "size":I
    .end local v22    # "z":I
    :catch_0
    move-exception v6

    .line 383
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    .line 384
    const-string v23, ""

    const-string v24, "LDH no data"

    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 386
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_b
    if-nez v8, :cond_c

    .line 387
    const-string v23, ""

    const-string v24, "cityInfo == null"

    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshFrom:I
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)I

    move-result v23

    if-nez v23, :cond_4

    .line 389
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCnt:I
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)I

    move-result v23

    if-gtz v23, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mUiHandler:Landroid/os/Handler;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/os/Handler;

    move-result-object v23

    if-eqz v23, :cond_4

    .line 390
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mUiHandler:Landroid/os/Handler;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/os/Handler;

    move-result-object v23

    const/16 v24, 0x20

    invoke-virtual/range {v23 .. v24}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_2

    .line 397
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getRealLocation(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v16

    .line 398
    .local v16, "oldRealLocation":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getAllCityListCount(Landroid/content/Context;)I

    move-result v3

    .line 399
    .local v3, "CityListCount":I
    const-string v23, ""

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, " RM@CityListCount : "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setRealLocation(Ljava/lang/String;)V

    .line 406
    const-string v23, "cityId:current"

    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setLocation(Ljava/lang/String;)V

    .line 408
    const-string v23, ""

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, " [LocHd] isFirst = "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mIsFirstCurrentLocation:Z
    invoke-static/range {v25 .. v25}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Z

    move-result v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshFrom:I
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)I

    move-result v23

    if-nez v23, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    .line 410
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mIsFirstCurrentLocation:Z
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Z

    move-result v23

    if-nez v23, :cond_e

    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    .line 411
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshFrom:I
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)I

    move-result v23

    const/16 v24, 0x5

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_10

    .line 412
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getCheckCurrentCityLocation(Landroid/content/Context;)I

    move-result v23

    if-nez v23, :cond_f

    .line 413
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    const/16 v24, 0x1

    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateCheckCurrentCityLocation(Landroid/content/Context;I)I

    .line 414
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-result-object v23

    if-eqz v23, :cond_18

    .line 415
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-result-object v23

    const/16 v24, 0x1

    invoke-virtual/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->setCurrentLoccation(I)V

    .line 421
    :cond_f
    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mIsFirstCurrentLocation:Z
    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$502(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;Z)Z

    .line 427
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshFrom:I
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)I

    move-result v23

    if-eqz v23, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    .line 428
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshFrom:I
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)I

    move-result v23

    const/16 v24, 0x5

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_1d

    .line 430
    :cond_11
    const/16 v19, 0x0

    .line 431
    .local v19, "result":I
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->isRegisteredToCityList(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_19

    .line 432
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move-object/from16 v2, v21

    invoke-static {v0, v1, v8, v5, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateCity(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)I

    move-result v19

    .line 434
    const-string v23, ""

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "CurLocSet Update : "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 440
    :goto_8
    if-nez v3, :cond_12

    .line 441
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    const/16 v24, 0x1

    const/16 v25, 0x1

    invoke-static/range {v23 .. v25}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;->setWidgetAtDaemon(Landroid/content/Context;ZZ)V

    .line 443
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->isRegisteredToDetailHourInfo(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_1a

    .line 444
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-static {v0, v1, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateDetailHourInfo(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)I

    .line 448
    :goto_9
    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v23

    if-eqz v23, :cond_15

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfoSize()I

    move-result v23

    if-lez v23, :cond_15

    .line 449
    if-eqz v16, :cond_1c

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v23

    if-lez v23, :cond_1c

    .line 466
    const/16 v23, 0x0

    move/from16 v0, v23

    invoke-virtual {v5, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;->getCityId()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    .line 467
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->isRegisteredToCityList(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v23

    if-nez v23, :cond_1b

    .line 468
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getExistPhotos(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_14

    .line 469
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->deletePhotosInfo(Landroid/content/Context;Ljava/lang/String;)I

    .line 483
    :cond_14
    :goto_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-static {v0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->insertPhotosInfo(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 495
    :cond_15
    :goto_b
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->arg1:I

    move/from16 v23, v0

    const/16 v24, 0x401

    move/from16 v0, v23

    move/from16 v1, v24

    if-eq v0, v1, :cond_16

    .line 496
    const-string v23, ""

    const-string v24, "LocHd >>> request to update daemon"

    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 497
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;->checkSameDaemonCityIdAtDate(Landroid/content/Context;)V

    .line 500
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Ljava/util/ArrayList;

    move-result-object v23

    if-eqz v23, :cond_17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Ljava/util/ArrayList;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    move-result v23

    if-lez v23, :cond_17

    .line 501
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Ljava/util/ArrayList;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->clear()V

    .line 504
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v25 .. v25}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->updateCityList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v24

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCityList:Ljava/util/ArrayList;
    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$702(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 506
    new-instance v9, Landroid/content/Intent;

    .end local v9    # "intent":Landroid/content/Intent;
    const-string v23, "com.sec.android.widgetapp.ap.hero.accuweather.action.GET_CURRENT_LOCATION_OK"

    move-object/from16 v0, v23

    invoke-direct {v9, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 507
    .restart local v9    # "intent":Landroid/content/Intent;
    const-string v23, "AUTO_REFRESH_WHERE_FROM"

    const/16 v24, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 509
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 511
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCnt:I
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)I

    move-result v23

    if-gtz v23, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mUiHandler:Landroid/os/Handler;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/os/Handler;

    move-result-object v23

    if-eqz v23, :cond_2

    .line 512
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mUiHandler:Landroid/os/Handler;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/os/Handler;

    move-result-object v23

    const/16 v24, 0x20

    invoke-virtual/range {v23 .. v24}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 417
    .end local v19    # "result":I
    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v24 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSettingsForMain(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-result-object v24

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$602(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    goto/16 :goto_7

    .line 436
    .restart local v19    # "result":I
    :cond_19
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-static {v0, v8, v5, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->insertCity(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)I

    move-result v19

    .line 437
    const-string v23, ""

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "CurLocSet Insert : "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_8

    .line 485
    :catch_1
    move-exception v6

    .line 486
    .restart local v6    # "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    .line 487
    new-instance v9, Landroid/content/Intent;

    .end local v9    # "intent":Landroid/content/Intent;
    const-string v23, "com.sec.android.widgetapp.ap.hero.accuweather.action.GET_CURRENT_LOCATION_ERROR"

    move-object/from16 v0, v23

    invoke-direct {v9, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 488
    .restart local v9    # "intent":Landroid/content/Intent;
    const-string v23, "AUTO_REFRESH_WHERE_FROM"

    const/16 v24, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 490
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 491
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 492
    .local v13, "locs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 493
    const-string v23, ""

    const-string v24, "CurLocSet Delete City "

    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_b

    .line 446
    .end local v6    # "e":Ljava/lang/Exception;
    .end local v13    # "locs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1a
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-static {v0, v1, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->insertDetailHourInfo(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)I

    goto/16 :goto_9

    .line 471
    :cond_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    const/16 v24, 0x0

    move/from16 v0, v24

    invoke-virtual {v5, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;

    move-result-object v24

    .line 472
    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;->getCityId()Ljava/lang/String;

    move-result-object v24

    .line 471
    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getExistPhotos(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_14

    .line 473
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    const/16 v24, 0x0

    move/from16 v0, v24

    invoke-virtual {v5, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;

    move-result-object v24

    .line 474
    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;->getCityId()Ljava/lang/String;

    move-result-object v24

    .line 473
    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->deletePhotosInfo(Landroid/content/Context;Ljava/lang/String;)I

    goto/16 :goto_a

    .line 477
    :cond_1c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    const/16 v24, 0x0

    move/from16 v0, v24

    invoke-virtual {v5, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;

    move-result-object v24

    .line 478
    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;->getCityId()Ljava/lang/String;

    move-result-object v24

    .line 477
    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getExistPhotos(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_14

    .line 479
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    const/16 v24, 0x0

    move/from16 v0, v24

    invoke-virtual {v5, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;

    move-result-object v24

    .line 480
    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;->getCityId()Ljava/lang/String;

    move-result-object v24

    .line 479
    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->deletePhotosInfo(Landroid/content/Context;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_a

    .line 518
    .end local v19    # "result":I
    :cond_1d
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->isRegisteredToCityList(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_1f

    .line 519
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move-object/from16 v2, v21

    invoke-static {v0, v1, v8, v5, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateCity(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)I

    .line 524
    :goto_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->isRegisteredToDetailHourInfo(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_20

    .line 525
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-static {v0, v1, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateDetailHourInfo(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)I

    .line 530
    :goto_d
    if-eqz v16, :cond_23

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v23

    if-lez v23, :cond_23

    .line 531
    const/16 v23, 0x0

    move/from16 v0, v23

    invoke-virtual {v5, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;->getCityId()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_21

    .line 532
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getExistPhotos(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_1e

    .line 533
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->deletePhotosInfo(Landroid/content/Context;Ljava/lang/String;)I

    .line 554
    :cond_1e
    :goto_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-static {v0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->insertPhotosInfo(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    .line 562
    :goto_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;->checkSameDaemonCityIdAtDate(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 522
    :cond_1f
    :try_start_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-static {v0, v8, v5, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->insertCity(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto/16 :goto_c

    .line 556
    :catch_2
    move-exception v6

    .line 557
    .restart local v6    # "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    .line 559
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 560
    .restart local v13    # "locs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_f

    .line 528
    .end local v6    # "e":Ljava/lang/Exception;
    .end local v13    # "locs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_20
    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-static {v0, v1, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->insertDetailHourInfo(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)I

    goto/16 :goto_d

    .line 536
    :cond_21
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->isRegisteredToCityList(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v23

    if-nez v23, :cond_22

    .line 537
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getExistPhotos(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_22

    .line 538
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->deletePhotosInfo(Landroid/content/Context;Ljava/lang/String;)I

    .line 541
    :cond_22
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    const/16 v24, 0x0

    move/from16 v0, v24

    invoke-virtual {v5, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;

    move-result-object v24

    .line 542
    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;->getCityId()Ljava/lang/String;

    move-result-object v24

    .line 541
    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getExistPhotos(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_1e

    .line 543
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    const/16 v24, 0x0

    move/from16 v0, v24

    invoke-virtual {v5, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;

    move-result-object v24

    .line 544
    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;->getCityId()Ljava/lang/String;

    move-result-object v24

    .line 543
    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->deletePhotosInfo(Landroid/content/Context;Ljava/lang/String;)I

    goto/16 :goto_e

    .line 548
    :cond_23
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    const/16 v24, 0x0

    move/from16 v0, v24

    invoke-virtual {v5, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;

    move-result-object v24

    .line 549
    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;->getCityId()Ljava/lang/String;

    move-result-object v24

    .line 548
    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getExistPhotos(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_1e

    .line 550
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    const/16 v24, 0x0

    move/from16 v0, v24

    invoke-virtual {v5, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;

    move-result-object v24

    .line 551
    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;->getCityId()Ljava/lang/String;

    move-result-object v24

    .line 550
    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->deletePhotosInfo(Landroid/content/Context;Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    goto/16 :goto_e

    .line 572
    .end local v3    # "CityListCount":I
    .end local v5    # "dinfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .end local v7    # "i":I
    .end local v8    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .end local v16    # "oldRealLocation":Ljava/lang/String;
    .end local v21    # "xmlinfo":Ljava/lang/String;
    :sswitch_3
    new-instance v9, Landroid/content/Intent;

    .end local v9    # "intent":Landroid/content/Intent;
    const-string v23, "com.sec.android.widgetapp.ap.hero.accuweather.action.GET_CURRENT_LOCATION_PROVIDER_ERR"

    move-object/from16 v0, v23

    invoke-direct {v9, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 573
    .restart local v9    # "intent":Landroid/content/Intent;
    const-string v23, "AUTO_REFRESH_WHERE_FROM"

    const/16 v24, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 575
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 578
    :sswitch_4
    new-instance v9, Landroid/content/Intent;

    .end local v9    # "intent":Landroid/content/Intent;
    const-string v23, "com.sec.android.widgetapp.ap.hero.accuweather.action.GET_CURRENT_LOCATION_OK"

    move-object/from16 v0, v23

    invoke-direct {v9, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 579
    .restart local v9    # "intent":Landroid/content/Intent;
    const-string v23, "AUTO_REFRESH_WHERE_FROM"

    const/16 v24, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 581
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mCtx:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/content/Context;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 600
    :cond_24
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshFrom:I
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)I

    move-result v23

    const/16 v24, 0x5

    move/from16 v0, v23

    move/from16 v1, v24

    if-eq v0, v1, :cond_3

    .line 601
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v14

    .line 602
    .local v14, "msg":Landroid/os/Message;
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 603
    .local v4, "bundle":Landroid/os/Bundle;
    const-string v23, "RESPONSE_CODE"

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v24, v0

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 604
    const-string v23, "LOCATION_NAME"

    const-string v24, "cityId:current"

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 605
    const-string v23, "SEARCH_MODE"

    const/16 v24, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 606
    invoke-virtual {v14, v4}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 607
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$LocDataHandler;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mUiHandler:Landroid/os/Handler;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/os/Handler;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v14}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_1

    .line 321
    nop

    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_2
        0xc9 -> :sswitch_3
        0xca -> :sswitch_1
        0x3e7 -> :sswitch_4
        0x3f2 -> :sswitch_0
    .end sparse-switch
.end method
