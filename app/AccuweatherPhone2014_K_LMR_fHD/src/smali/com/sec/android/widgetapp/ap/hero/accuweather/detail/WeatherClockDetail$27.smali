.class Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$27;
.super Ljava/lang/Object;
.source "WeatherClockDetail.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->showRefreshDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .prologue
    .line 3732
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 3734
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRefreshDialog:Landroid/app/Dialog;
    invoke-static {v0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$8002(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Landroid/app/Dialog;)Landroid/app/Dialog;

    .line 3735
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$7900(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3736
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$7900(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->cancleRefresh()V

    .line 3737
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$7900(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->setRefreshCanceled(Z)V

    .line 3739
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    iput-boolean v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->performMapCancled:Z

    .line 3741
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$8100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 3742
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$8100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->clearResource()V

    .line 3743
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;
    invoke-static {v0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$8102(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    .line 3747
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mapHandler:Landroid/os/Handler;

    if-eqz v0, :cond_2

    .line 3748
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mapHandler:Landroid/os/Handler;

    const v1, -0x140b4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 3754
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->stopRefreshAnimation()V
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$8200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    .line 3755
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 3756
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$8300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    .line 3757
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$8300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->getMPState()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    .line 3758
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$8300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->start()V

    .line 3760
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->resetTouchTimer()V
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;)V

    .line 3761
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail$27;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    const/16 v1, 0x3e8

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->setTouchTimer(I)V
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;->access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;I)V

    .line 3762
    return-void
.end method
