.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CustTextView;
.super Landroid/widget/TextView;
.source "CustTextView.java"


# instance fields
.field private stroke:Z

.field private strokeColor:I

.field private strokeWidth:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 15
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CustTextView;->stroke:Z

    .line 17
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CustTextView;->strokeWidth:F

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 15
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CustTextView;->stroke:Z

    .line 17
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CustTextView;->strokeWidth:F

    .line 30
    invoke-direct {p0, p1, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CustTextView;->initView(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 15
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CustTextView;->stroke:Z

    .line 17
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CustTextView;->strokeWidth:F

    .line 24
    invoke-direct {p0, p1, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CustTextView;->initView(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    return-void
.end method

.method private initView(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x1

    .line 38
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/R$styleable;->CustTextView:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 40
    .local v0, "a":Landroid/content/res/TypedArray;
    if-eqz v0, :cond_0

    .line 41
    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CustTextView;->stroke:Z

    .line 42
    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CustTextView;->strokeWidth:F

    .line 43
    const/4 v1, 0x2

    const v2, 0x4cffffff    # 1.3421772E8f

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CustTextView;->strokeColor:I

    .line 46
    :cond_0
    if-eqz v0, :cond_1

    .line 47
    const/4 v0, 0x0

    .line 49
    :cond_1
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 52
    iget-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CustTextView;->stroke:Z

    if-eqz v1, :cond_0

    .line 53
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CustTextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    .line 54
    .local v0, "states":Landroid/content/res/ColorStateList;
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CustTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 55
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CustTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CustTextView;->strokeWidth:F

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setStrokeWidth(F)V

    .line 56
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CustTextView;->strokeColor:I

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CustTextView;->setTextColor(I)V

    .line 57
    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 59
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CustTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 60
    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CustTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 63
    .end local v0    # "states":Landroid/content/res/ColorStateList;
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 64
    return-void
.end method
