.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;
.super Ljava/lang/Object;
.source "HttpResponseHandler.java"

# interfaces
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpClient;


# instance fields
.field private response:Lorg/apache/http/HttpResponse;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getResponse()Lorg/apache/http/HttpResponse;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;->response:Lorg/apache/http/HttpResponse;

    return-object v0
.end method

.method public onReceive(IILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "position"    # I
    .param p2, "responseCode"    # I
    .param p3, "responseStatus"    # Ljava/lang/String;
    .param p4, "responseBody"    # Ljava/lang/String;

    .prologue
    .line 10
    return-void
.end method

.method public onReceive(IILjava/lang/String;Ljava/lang/String;J)V
    .locals 0
    .param p1, "position"    # I
    .param p2, "responseCode"    # I
    .param p3, "responseStatus"    # Ljava/lang/String;
    .param p4, "responseBody"    # Ljava/lang/String;
    .param p5, "stamp"    # J

    .prologue
    .line 14
    return-void
.end method

.method public setResponse(Lorg/apache/http/HttpResponse;)V
    .locals 0
    .param p1, "response"    # Lorg/apache/http/HttpResponse;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;->response:Lorg/apache/http/HttpResponse;

    .line 22
    return-void
.end method
