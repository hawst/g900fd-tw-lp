.class Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$3;
.super Lcom/google/android/maps/Overlay;
.source "MapsActivityNew.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->setupMapView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 519
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-direct {p0}, Lcom/google/android/maps/Overlay;-><init>()V

    return-void
.end method

.method private setZoom(Lcom/google/android/maps/MapView;)V
    .locals 12
    .param p1, "mapView"    # Lcom/google/android/maps/MapView;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x3

    const/4 v9, 0x0

    .line 521
    invoke-virtual {p1}, Lcom/google/android/maps/MapView;->getMapCenter()Lcom/google/android/maps/GeoPoint;

    move-result-object v3

    .line 522
    .local v3, "gp":Lcom/google/android/maps/GeoPoint;
    invoke-virtual {p1}, Lcom/google/android/maps/MapView;->getZoomLevel()I

    move-result v6

    if-ge v6, v10, :cond_1

    .line 523
    invoke-virtual {p1}, Lcom/google/android/maps/MapView;->getController()Lcom/google/android/maps/MapController;

    move-result-object v6

    invoke-virtual {v6, v10}, Lcom/google/android/maps/MapController;->setZoom(I)I

    .line 524
    invoke-virtual {p1}, Lcom/google/android/maps/MapView;->getController()Lcom/google/android/maps/MapController;

    move-result-object v6

    invoke-virtual {v6, v3}, Lcom/google/android/maps/MapController;->setCenter(Lcom/google/android/maps/GeoPoint;)V

    .line 539
    :goto_0
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;
    invoke-static {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getZoomButtonsController()Landroid/widget/ZoomButtonsController;

    move-result-object v5

    .line 540
    .local v5, "zbc":Landroid/widget/ZoomButtonsController;
    invoke-virtual {v5}, Landroid/widget/ZoomButtonsController;->getContainer()Landroid/view/ViewGroup;

    move-result-object v1

    .line 541
    .local v1, "container":Landroid/view/ViewGroup;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v6

    if-ge v4, v6, :cond_0

    .line 542
    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 543
    .local v0, "child":Landroid/view/View;
    instance-of v6, v0, Landroid/widget/ZoomControls;

    if-eqz v6, :cond_4

    .line 544
    invoke-virtual {v0}, Landroid/view/View;->getLayoutDirection()I

    move-result v2

    .line 545
    .local v2, "diration":I
    if-ne v2, v11, :cond_0

    .line 546
    invoke-virtual {v0, v9}, Landroid/view/View;->setLayoutDirection(I)V

    .line 547
    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 552
    .end local v0    # "child":Landroid/view/View;
    .end local v2    # "diration":I
    :cond_0
    return-void

    .line 526
    .end local v1    # "container":Landroid/view/ViewGroup;
    .end local v4    # "i":I
    .end local v5    # "zbc":Landroid/widget/ZoomButtonsController;
    :cond_1
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    iget v6, v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mPreviousZoomLevel:I

    invoke-virtual {p1}, Lcom/google/android/maps/MapView;->getZoomLevel()I

    move-result v7

    if-eq v6, v7, :cond_2

    .line 527
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-virtual {p1}, Lcom/google/android/maps/MapView;->getZoomLevel()I

    move-result v7

    iput v7, v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mPreviousZoomLevel:I

    .line 528
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    iget-object v6, v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->zoomHandler:Landroid/os/Handler;

    invoke-virtual {v6, v9}, Landroid/os/Handler;->removeMessages(I)V

    .line 529
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    iget-object v6, v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->zoomHandler:Landroid/os/Handler;

    const-wide/16 v7, 0xc8

    invoke-virtual {v6, v9, v7, v8}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 532
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/maps/MapView;->getZoomLevel()I

    move-result v6

    if-ne v6, v10, :cond_3

    .line 533
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;
    invoke-static {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getZoomButtonsController()Landroid/widget/ZoomButtonsController;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/widget/ZoomButtonsController;->setZoomOutEnabled(Z)V

    goto :goto_0

    .line 535
    :cond_3
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;
    invoke-static {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getZoomButtonsController()Landroid/widget/ZoomButtonsController;

    move-result-object v6

    invoke-virtual {v6, v11}, Landroid/widget/ZoomButtonsController;->setZoomOutEnabled(Z)V

    goto :goto_0

    .line 541
    .restart local v0    # "child":Landroid/view/View;
    .restart local v1    # "container":Landroid/view/ViewGroup;
    .restart local v4    # "i":I
    .restart local v5    # "zbc":Landroid/widget/ZoomButtonsController;
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;Lcom/google/android/maps/MapView;Z)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "mapView"    # Lcom/google/android/maps/MapView;
    .param p3, "shadow"    # Z

    .prologue
    .line 559
    invoke-direct {p0, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$3;->setZoom(Lcom/google/android/maps/MapView;)V

    .line 560
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/maps/Overlay;->draw(Landroid/graphics/Canvas;Lcom/google/android/maps/MapView;Z)V

    .line 561
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;Lcom/google/android/maps/MapView;ZJ)Z
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "mapView"    # Lcom/google/android/maps/MapView;
    .param p3, "shadow"    # Z
    .param p4, "when"    # J

    .prologue
    .line 554
    invoke-direct {p0, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$3;->setZoom(Lcom/google/android/maps/MapView;)V

    .line 555
    invoke-super/range {p0 .. p5}, Lcom/google/android/maps/Overlay;->draw(Landroid/graphics/Canvas;Lcom/google/android/maps/MapView;ZJ)Z

    move-result v0

    return v0
.end method
