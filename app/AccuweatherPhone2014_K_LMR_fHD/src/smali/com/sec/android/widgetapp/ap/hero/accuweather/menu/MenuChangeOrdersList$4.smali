.class Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$4;
.super Ljava/lang/Object;
.source "MenuChangeOrdersList.java"

# interfaces
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/LocationGetter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->getDataRefresh(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

.field final synthetic val$cityDataList:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;Ljava/util/ArrayList;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    .prologue
    .line 403
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    iput-object p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$4;->val$cityDataList:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getLocation()V
    .locals 4

    .prologue
    .line 405
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mPopupDialog:Landroid/app/Dialog;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 406
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$4;->val$cityDataList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 407
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->startUpdateAnimation()V
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)V

    .line 408
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->showRefreshDialog()V
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)V

    .line 409
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-result-object v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    .line 410
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mUiHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x0

    .line 409
    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->doManualRefresh(ILandroid/os/Handler;Ljava/lang/String;)V

    .line 412
    :cond_0
    return-void
.end method
