.class Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$8;
.super Ljava/lang/Object;
.source "WeatherClockCityListAnimationAndDelete.java"

# interfaces
.implements Lcom/sec/android/touchwiz/widget/TwAdapterView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    .prologue
    .line 647
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$8;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Lcom/sec/android/touchwiz/widget/TwAdapterView;Landroid/view/View;IJ)Z
    .locals 5
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/touchwiz/widget/TwAdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .local p1, "av":Lcom/sec/android/touchwiz/widget/TwAdapterView;, "Lcom/sec/android/touchwiz/widget/TwAdapterView<*>;"
    const/4 v4, 0x1

    .line 649
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$8;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->mIsDeleteMode:Z
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->access$900(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 651
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$8;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$8;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->mActionModeCallback:Landroid/view/ActionMode$Callback;
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->access$1000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;)Landroid/view/ActionMode$Callback;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    .line 653
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$8;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->mListView:Lcom/sec/android/touchwiz/widget/TwListView;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;)Lcom/sec/android/touchwiz/widget/TwListView;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/sec/android/touchwiz/widget/TwListView;->getPositionForView(Landroid/view/View;)I

    move-result v1

    .line 654
    .local v1, "pos":I
    const v2, 0x7f080073

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultCheckBox;

    .line 655
    .local v0, "chb":Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultCheckBox;
    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultCheckBox;->toggle()V

    .line 656
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$8;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->mListAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$MyListAdapter;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$MyListAdapter;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$MyListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultCheckBox;->isChecked()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->setChecked(Z)V

    .line 658
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$8;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->updateSelect()V

    .line 659
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete$8;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->setDeleteMenuVisible(Z)V
    invoke-static {v2, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;->access$1100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockCityListAnimationAndDelete;Z)V

    .line 661
    .end local v0    # "chb":Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultCheckBox;
    .end local v1    # "pos":I
    :cond_0
    return v4
.end method
