.class Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView$1;
.super Ljava/lang/Object;
.source "CloudyIconView.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;

    .prologue
    .line 202
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 207
    invoke-virtual {p1}, Landroid/animation/Animator;->removeAllListeners()V

    .line 208
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 204
    invoke-virtual {p1}, Landroid/animation/Animator;->removeAllListeners()V

    .line 205
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 211
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->isStop:Z

    if-eqz v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->TranslateAni2:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->TranslateAni2:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->TranslateAni2:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/CloudyIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    .line 216
    :cond_0
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 209
    return-void
.end method
