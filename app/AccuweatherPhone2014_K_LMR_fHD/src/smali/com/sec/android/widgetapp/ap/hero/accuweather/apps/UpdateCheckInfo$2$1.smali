.class Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo$2$1;
.super Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;
.source "UpdateCheckInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo$2;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo$2;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo$2;)V
    .locals 0
    .param p1, "this$1"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo$2;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo$2$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo$2;

    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(IILjava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "position"    # I
    .param p2, "responseCode"    # I
    .param p3, "responseStatus"    # Ljava/lang/String;
    .param p4, "responseBody"    # Ljava/lang/String;

    .prologue
    const/16 v4, 0xc8

    .line 151
    invoke-super {p0, p1, p2, p3, p4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;->onReceive(IILjava/lang/String;Ljava/lang/String;)V

    .line 152
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[gC] rspC : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    if-ne p2, v4, :cond_0

    .line 155
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo$2$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo$2;

    iget-object v1, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->parseUpdateVersionInfo(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->access$300(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->response:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->access$202(Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 163
    :goto_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo$2$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo$2;

    iget-object v1, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->response:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    .line 164
    const-string v1, ""

    const-string v2, "resp is null !!!"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    :cond_0
    :goto_1
    if-eq p2, v4, :cond_1

    .line 176
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo$2$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo$2;

    iget-object v1, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->stopHttpThread()V

    .line 177
    return-void

    .line 156
    :catch_0
    move-exception v0

    .line 157
    .local v0, "e":Lorg/xmlpull/v1/XmlPullParserException;
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    goto :goto_0

    .line 158
    .end local v0    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catch_1
    move-exception v0

    .line 159
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 160
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 161
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 166
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[gC] rsp = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo$2$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo$2;

    iget-object v3, v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->response:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo$2$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo$2;

    iget-object v1, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->response:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 168
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo$2$1;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo$2;

    iget-object v1, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/UpdateCheckInfo$2;->val$handler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1
.end method
