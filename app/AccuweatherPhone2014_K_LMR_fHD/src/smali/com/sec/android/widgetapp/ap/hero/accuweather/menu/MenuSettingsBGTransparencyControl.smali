.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;
.super Landroid/app/Activity;
.source "MenuSettingsBGTransparencyControl.java"

# interfaces
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/model/IActivityVisibleState;


# instance fields
.field private actionbar:Landroid/app/ActionBar;

.field private actionbarBack:Landroid/widget/LinearLayout;

.field private actionbarLayout:Landroid/widget/RelativeLayout;

.field private actionbarTitle:Landroid/widget/TextView;

.field private mBGImageView:Landroid/widget/ImageView;

.field private mBGInfoView:Landroid/widget/ImageView;

.field private mContext:Landroid/content/Context;

.field private mSeekbar:Landroid/widget/SeekBar;

.field private mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

.field private mTransparencyProgressVal:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 26
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 27
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->mSeekbar:Landroid/widget/SeekBar;

    .line 29
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->mBGImageView:Landroid/widget/ImageView;

    .line 30
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->mBGInfoView:Landroid/widget/ImageView;

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->mTransparencyProgressVal:I

    .line 33
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->mContext:Landroid/content/Context;

    .line 34
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    .line 42
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->actionbarTitle:Landroid/widget/TextView;

    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;
    .param p1, "x1"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->mTransparencyProgressVal:I

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->mBGImageView:Landroid/widget/ImageView;

    return-object v0
.end method

.method private setupActionbar()V
    .locals 8

    .prologue
    const v7, 0x7f0d0014

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 106
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->actionbar:Landroid/app/ActionBar;

    if-nez v2, :cond_0

    .line 107
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->actionbar:Landroid/app/ActionBar;

    .line 110
    :cond_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->actionbar:Landroid/app/ActionBar;

    if-eqz v2, :cond_1

    .line 111
    sget-boolean v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isLollipopOS:Z

    if-nez v2, :cond_2

    .line 112
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02008f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 114
    const v2, 0x7f030005

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->actionbarLayout:Landroid/widget/RelativeLayout;

    .line 116
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->actionbar:Landroid/app/ActionBar;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->actionbarLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 117
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->actionbarLayout:Landroid/widget/RelativeLayout;

    const v3, 0x7f080008

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->actionbarTitle:Landroid/widget/TextView;

    .line 118
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->actionbarLayout:Landroid/widget/RelativeLayout;

    const v3, 0x7f080006

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->actionbarBack:Landroid/widget/LinearLayout;

    .line 120
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->actionbarBack:Landroid/widget/LinearLayout;

    const v3, 0x7f080009

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 121
    .local v1, "v":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200aa

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 122
    .local v0, "backIcon":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0, v6}, Landroid/graphics/drawable/Drawable;->setAutoMirrored(Z)V

    .line 123
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 125
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v5}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 126
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v5}, Landroid/app/ActionBar;->setDisplayUseLogoEnabled(Z)V

    .line 127
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v5}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 128
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v6}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 130
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->actionbarTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setText(I)V

    .line 132
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->actionbarBack:Landroid/widget/LinearLayout;

    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl$2;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;)V

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 146
    .end local v0    # "backIcon":Landroid/graphics/drawable/Drawable;
    .end local v1    # "v":Landroid/widget/ImageView;
    :cond_1
    :goto_0
    return-void

    .line 138
    :cond_2
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v5}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 139
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v5}, Landroid/app/ActionBar;->setDisplayUseLogoEnabled(Z)V

    .line 140
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v6}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 141
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v5}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 142
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v6}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 143
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v7}, Landroid/app/ActionBar;->setTitle(I)V

    goto :goto_0
.end method


# virtual methods
.method public isActivityVisible()Z
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    return v0
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 193
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 194
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 184
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 185
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->setupActionbar()V

    .line 186
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 53
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 54
    const v0, 0x7f030022

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->setContentView(I)V

    .line 55
    iput-object p0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->mContext:Landroid/content/Context;

    .line 56
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutDirection(I)V

    .line 57
    const v0, 0x7f08005e

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->mSeekbar:Landroid/widget/SeekBar;

    .line 59
    const v0, 0x7f08003d

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->mBGImageView:Landroid/widget/ImageView;

    .line 60
    const v0, 0x7f08005b

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->mBGInfoView:Landroid/widget/ImageView;

    .line 63
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getProductName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "slte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->mBGInfoView:Landroid/widget/ImageView;

    const v1, 0x7f02011c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 66
    :cond_0
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getViEffectSettings(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    .line 67
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->mSettingsViEffect:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getWidgetBGTransparencyVal()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->mTransparencyProgressVal:I

    .line 68
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->mSeekbar:Landroid/widget/SeekBar;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->mTransparencyProgressVal:I

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 70
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->mBGImageView:Landroid/widget/ImageView;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->mTransparencyProgressVal:I

    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->calAlphaVal(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageAlpha(I)V

    .line 71
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Transparency Val = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->mTransparencyProgressVal:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->mSeekbar:Landroid/widget/SeekBar;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl$1;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 90
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->setupActionbar()V

    .line 103
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 163
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 164
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->mSeekbar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    .line 165
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->mSeekbar:Landroid/widget/SeekBar;

    .line 168
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->mBGImageView:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 169
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->mBGImageView:Landroid/widget/ImageView;

    .line 172
    :cond_1
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 149
    sget-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isLollipopOS:Z

    if-eqz v0, :cond_0

    .line 150
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 157
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 153
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->finish()V

    .line 154
    const/4 v0, 0x1

    goto :goto_0

    .line 150
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 175
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 176
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->mContext:Landroid/content/Context;

    const-string v1, "EFFECT_13"

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsBGTransparencyControl;->mTransparencyProgressVal:I

    invoke-static {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateViEffectSettings(Landroid/content/Context;Ljava/lang/String;I)I

    .line 177
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 180
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 181
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 0

    .prologue
    .line 189
    invoke-super {p0}, Landroid/app/Activity;->onUserLeaveHint()V

    .line 190
    return-void
.end method

.method public setActivityVisibleState(Z)V
    .locals 0
    .param p1, "onoff"    # Z

    .prologue
    .line 46
    return-void
.end method
