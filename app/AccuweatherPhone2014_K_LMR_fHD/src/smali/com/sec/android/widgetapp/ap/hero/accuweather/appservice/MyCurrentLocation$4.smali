.class Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;
.super Ljava/lang/Thread;
.source "MyCurrentLocation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->addCityWeatherFromDetail(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Landroid/os/Handler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

.field final synthetic val$headerGroup:Lorg/apache/http/message/HeaderGroup;

.field final synthetic val$info:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

.field final synthetic val$locationHandler:Landroid/os/Handler;

.field final synthetic val$tempScaleSetting:I

.field final synthetic val$updateDate:Ljava/lang/String;

.field final synthetic val$url:Ljava/net/URL;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;Ljava/net/URL;Lorg/apache/http/message/HeaderGroup;ILjava/lang/String;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    .prologue
    .line 419
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    iput-object p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;->val$url:Ljava/net/URL;

    iput-object p3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;->val$headerGroup:Lorg/apache/http/message/HeaderGroup;

    iput p4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;->val$tempScaleSetting:I

    iput-object p5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;->val$updateDate:Ljava/lang/String;

    iput-object p6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;->val$info:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    iput-object p7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;->val$locationHandler:Landroid/os/Handler;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 421
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;->val$url:Ljava/net/URL;

    if-eqz v0, :cond_1

    .line 422
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mHttpThread:Ljava/lang/Thread;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;)Ljava/lang/Thread;

    move-result-object v0

    if-nez v0, :cond_0

    .line 423
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1}, Ljava/lang/Thread;-><init>()V

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mHttpThread:Ljava/lang/Thread;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->access$202(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;Ljava/lang/Thread;)Ljava/lang/Thread;

    .line 426
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->httpClient:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;)Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->ctx:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;->val$url:Ljava/net/URL;

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;->val$headerGroup:Lorg/apache/http/message/HeaderGroup;

    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4$1;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;)V

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->get(ILandroid/content/Context;Ljava/net/URL;Lorg/apache/http/message/HeaderGroup;Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;)Ljava/lang/Thread;

    move-result-object v6

    .line 480
    .local v6, "t":Ljava/lang/Thread;
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mHttpThread:Ljava/lang/Thread;
    invoke-static {v0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->access$202(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;Ljava/lang/Thread;)Ljava/lang/Thread;

    .line 482
    .end local v6    # "t":Ljava/lang/Thread;
    :cond_1
    return-void
.end method
