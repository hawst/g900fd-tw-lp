.class Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;
.super Ljava/lang/Thread;
.source "MyCurrentLocation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->addCityWeatherFromProvider(Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

.field final synthetic val$headerGroup:Lorg/apache/http/message/HeaderGroup;

.field final synthetic val$latitude:Ljava/lang/String;

.field final synthetic val$locationHandler:Landroid/os/Handler;

.field final synthetic val$longitude:Ljava/lang/String;

.field final synthetic val$url:Ljava/net/URL;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;Ljava/net/URL;Lorg/apache/http/message/HeaderGroup;Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    .prologue
    .line 339
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    iput-object p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;->val$url:Ljava/net/URL;

    iput-object p3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;->val$headerGroup:Lorg/apache/http/message/HeaderGroup;

    iput-object p4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;->val$latitude:Ljava/lang/String;

    iput-object p5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;->val$longitude:Ljava/lang/String;

    iput-object p6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;->val$locationHandler:Landroid/os/Handler;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 341
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;->val$url:Ljava/net/URL;

    if-eqz v0, :cond_1

    .line 342
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mHttpThread:Ljava/lang/Thread;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;)Ljava/lang/Thread;

    move-result-object v0

    if-nez v0, :cond_0

    .line 343
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1}, Ljava/lang/Thread;-><init>()V

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mHttpThread:Ljava/lang/Thread;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->access$202(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;Ljava/lang/Thread;)Ljava/lang/Thread;

    .line 345
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->httpClient:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;)Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->ctx:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;->val$url:Ljava/net/URL;

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;->val$headerGroup:Lorg/apache/http/message/HeaderGroup;

    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3$1;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;)V

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->get(ILandroid/content/Context;Ljava/net/URL;Lorg/apache/http/message/HeaderGroup;Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;)Ljava/lang/Thread;

    move-result-object v6

    .line 396
    .local v6, "t":Ljava/lang/Thread;
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->mHttpThread:Ljava/lang/Thread;
    invoke-static {v0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->access$202(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;Ljava/lang/Thread;)Ljava/lang/Thread;

    .line 398
    .end local v6    # "t":Ljava/lang/Thread;
    :cond_1
    return-void
.end method
