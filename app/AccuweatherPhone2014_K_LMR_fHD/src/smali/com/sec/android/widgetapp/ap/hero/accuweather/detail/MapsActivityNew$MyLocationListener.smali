.class Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MyLocationListener;
.super Ljava/lang/Object;
.source "MapsActivityNew.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyLocationListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;


# direct methods
.method private constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V
    .locals 0

    .prologue
    .line 675
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MyLocationListener;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;
    .param p2, "x1"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;

    .prologue
    .line 675
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MyLocationListener;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .locals 11
    .param p1, "arg0"    # Landroid/location/Location;

    .prologue
    const/4 v10, 0x0

    const-wide v8, 0x412e848000000000L    # 1000000.0

    .line 689
    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v3

    const-string v4, "gps"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 690
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MyLocationListener;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    new-instance v4, Lcom/google/android/maps/GeoPoint;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v5

    mul-double/2addr v5, v8

    double-to-int v5, v5

    .line 691
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    mul-double/2addr v6, v8

    double-to-int v6, v6

    invoke-direct {v4, v5, v6}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    .line 690
    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mInitGeoPoint:Lcom/google/android/maps/GeoPoint;
    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$1802(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Lcom/google/android/maps/GeoPoint;)Lcom/google/android/maps/GeoPoint;

    .line 699
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MyLocationListener;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mInitGeoPoint:Lcom/google/android/maps/GeoPoint;
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$1800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/google/android/maps/GeoPoint;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 700
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MyLocationListener;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getZoomLevel()I

    move-result v0

    .line 701
    .local v0, "currentZoomLevel":I
    rsub-int/lit8 v2, v0, 0x8

    .line 702
    .local v2, "zoomDiff":I
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cur : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " zoomDiff : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 704
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MyLocationListener;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MyLocationListener;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mInitGeoPoint:Lcom/google/android/maps/GeoPoint;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$1800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/google/android/maps/GeoPoint;

    move-result-object v4

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->centerLocation(Lcom/google/android/maps/GeoPoint;)V
    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$1900(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Lcom/google/android/maps/GeoPoint;)V

    .line 706
    if-lez v2, :cond_3

    .line 707
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v2, :cond_3

    .line 708
    if-nez v1, :cond_2

    .line 709
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MyLocationListener;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->moveHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$2000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Landroid/os/Handler;

    move-result-object v3

    const-wide/16 v4, 0x5dc

    invoke-virtual {v3, v10, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 707
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 693
    .end local v0    # "currentZoomLevel":I
    .end local v1    # "i":I
    .end local v2    # "zoomDiff":I
    :cond_1
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MyLocationListener;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mInitGeoPoint:Lcom/google/android/maps/GeoPoint;
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$1800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/google/android/maps/GeoPoint;

    move-result-object v3

    if-nez v3, :cond_0

    .line 694
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MyLocationListener;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    new-instance v4, Lcom/google/android/maps/GeoPoint;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v5

    mul-double/2addr v5, v8

    double-to-int v5, v5

    .line 695
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    mul-double/2addr v6, v8

    double-to-int v6, v6

    invoke-direct {v4, v5, v6}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    .line 694
    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mInitGeoPoint:Lcom/google/android/maps/GeoPoint;
    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$1802(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Lcom/google/android/maps/GeoPoint;)Lcom/google/android/maps/GeoPoint;

    goto :goto_0

    .line 711
    .restart local v0    # "currentZoomLevel":I
    .restart local v1    # "i":I
    .restart local v2    # "zoomDiff":I
    :cond_2
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MyLocationListener;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->moveHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$2000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Landroid/os/Handler;

    move-result-object v3

    const-wide/16 v4, 0x7d0

    invoke-virtual {v3, v10, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_2

    .line 716
    .end local v1    # "i":I
    :cond_3
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MyLocationListener;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLocationManager:Landroid/location/LocationManager;
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$2200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Landroid/location/LocationManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MyLocationListener;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLocationListener:Landroid/location/LocationListener;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->access$2100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Landroid/location/LocationListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 718
    .end local v0    # "currentZoomLevel":I
    .end local v2    # "zoomDiff":I
    :cond_4
    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 678
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 682
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;
    .param p2, "status"    # I
    .param p3, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 686
    return-void
.end method
