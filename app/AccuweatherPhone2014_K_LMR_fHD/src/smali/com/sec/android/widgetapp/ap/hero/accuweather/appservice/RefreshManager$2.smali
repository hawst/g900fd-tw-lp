.class Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$2;
.super Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;
.source "RefreshManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->performRefreshSearchMode(Landroid/os/Handler;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

.field final synthetic val$location:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    .prologue
    .line 1299
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    iput-object p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$2;->val$location:Ljava/lang/String;

    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(IILjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "position"    # I
    .param p2, "responseCode"    # I
    .param p3, "responseStatus"    # Ljava/lang/String;
    .param p4, "responseBody"    # Ljava/lang/String;

    .prologue
    .line 1302
    invoke-super {p0, p1, p2, p3, p4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;->onReceive(IILjava/lang/String;Ljava/lang/String;)V

    .line 1304
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mRefreshCanceled:Z
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$1100(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1305
    const-string v2, ""

    const-string v3, " [mhc]rfh srt !!!"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1321
    :goto_0
    return-void

    .line 1309
    :cond_0
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 1311
    .local v1, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1312
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "RESPONSE_CODE"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1313
    const-string v2, "LOCATION"

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$2;->val$location:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1314
    const-string v2, "RESPONSE_BODY"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1315
    const-string v2, "SEARCH_MODE"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1317
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1319
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->mUiHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1320
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    invoke-virtual {v2, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->deleteMe(I)V

    goto :goto_0
.end method
