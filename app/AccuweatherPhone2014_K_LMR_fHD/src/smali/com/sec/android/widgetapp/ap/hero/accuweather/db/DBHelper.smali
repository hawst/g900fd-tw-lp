.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;
.super Ljava/lang/Object;
.source "DBHelper.java"


# static fields
.field public static ALL_CITY_LIST_PROJ:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 673
    const/16 v0, 0x11

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "NAME"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "STATE"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "LOCATION"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "TIMEZONE"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "REAL_LOCATION"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "TEMP_SCALE"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "TODAY_TEMP"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "TODAY_LOW_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "TODAY_HIGH_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "TODAY_ICON_NUM"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "TODAY_WEATHER_TEXT"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "SUMMER_TIME"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "TODAY_SUNRISE_TIME"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "TODAY_SUNSET_TIME"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "LATITUDE"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "LONGITUDE"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "XML_DETAIL_INFO"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->ALL_CITY_LIST_PROJ:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static UndoDB(Landroid/content/Context;Ljava/util/ArrayList;Z)I
    .locals 22
    .param p0, "ctx"    # Landroid/content/Context;
    .param p2, "isCurrentLocation"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityDataBackup;",
            ">;Z)I"
        }
    .end annotation

    .prologue
    .line 2840
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityDataBackup;>;"
    const/4 v13, 0x0

    .line 2841
    .local v13, "result":I
    if-nez p0, :cond_0

    .line 2842
    const-string v19, ""

    const-string v20, "uCO: context is null"

    invoke-static/range {v19 .. v20}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v14, v13

    .line 3037
    .end local v13    # "result":I
    .local v14, "result":I
    :goto_0
    return v14

    .line 2845
    .end local v14    # "result":I
    .restart local v13    # "result":I
    :cond_0
    if-eqz p2, :cond_1

    .line 2846
    const/16 v19, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateCheckCurrentCityLocation(Landroid/content/Context;I)I

    .line 2848
    :cond_1
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    .line 2849
    .local v5, "cp":Landroid/content/ContentResolver;
    if-eqz v5, :cond_5

    .line 2850
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v19

    move/from16 v0, v19

    new-array v3, v0, [Landroid/content/ContentValues;

    .line 2851
    .local v3, "arrVal":[Landroid/content/ContentValues;
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v19

    move/from16 v0, v19

    if-ge v9, v0, :cond_3

    .line 2852
    new-instance v19, Landroid/content/ContentValues;

    invoke-direct/range {v19 .. v19}, Landroid/content/ContentValues;-><init>()V

    aput-object v19, v3, v9

    .line 2853
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityDataBackup;

    .line 2854
    .local v11, "item":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityDataBackup;
    iget-object v10, v11, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityDataBackup;->info:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    .line 2856
    .local v10, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    aget-object v19, v3, v9

    const-string v20, "NAME"

    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityDataBackup;->getName()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2857
    aget-object v19, v3, v9

    const-string v20, "STATE"

    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityDataBackup;->getState()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2858
    aget-object v19, v3, v9

    const-string v20, "LOCATION"

    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityDataBackup;->getLocation()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2859
    aget-object v19, v3, v9

    const-string v20, "TIMEZONE"

    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityDataBackup;->getTimeZone()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2860
    aget-object v19, v3, v9

    const-string v20, "SUMMER_TIME"

    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityDataBackup;->getSummerTime()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2862
    aget-object v19, v3, v9

    const-string v20, "LATITUDE"

    const-string v21, ""

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2863
    aget-object v19, v3, v9

    const-string v20, "LONGITUDE"

    const-string v21, ""

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2865
    aget-object v19, v3, v9

    const-string v20, "REAL_LOCATION"

    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityDataBackup;->getRealLocation()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2867
    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v17

    .line 2868
    .local v17, "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    aget-object v19, v3, v9

    const-string v20, "TIMEZONE"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2869
    aget-object v19, v3, v9

    const-string v20, "TEMP_SCALE"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2870
    aget-object v19, v3, v9

    const-string v20, "TODAY_DATE"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDate()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2871
    aget-object v19, v3, v9

    const-string v20, "TODAY_TEMP"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getCurrentTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2872
    aget-object v19, v3, v9

    const-string v20, "TODAY_HIGH_TEMP"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getHighTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2873
    aget-object v19, v3, v9

    const-string v20, "TODAY_LOW_TEMP"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getLowTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2874
    aget-object v19, v3, v9

    const-string v20, "TODAY_ICON_NUM"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getIconNum()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2875
    aget-object v19, v3, v9

    const-string v20, "UPDATE_DATE"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getUpdateDate()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2876
    aget-object v19, v3, v9

    const-string v20, "TODAY_WEATHER_TEXT"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getWeatherText()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2877
    aget-object v19, v3, v9

    const-string v20, "TODAY_WEATHER_URL"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2878
    aget-object v19, v3, v9

    const-string v20, "TODAY_REALFELL"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getRealFeel()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2879
    aget-object v19, v3, v9

    const-string v20, "TODAY_SUNRISE_TIME"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2880
    aget-object v19, v3, v9

    const-string v20, "TODAY_SUNSET_TIME"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2882
    aget-object v19, v3, v9

    const-string v20, "UPDATE_DATE"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getUpdateDate()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2885
    aget-object v19, v3, v9

    const-string v20, "TODAY_DAY_RAIN_PROBABILITY"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayRainProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2886
    aget-object v19, v3, v9

    const-string v20, "TODAY_DAY_SNOW_PROBABILITY"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDaySnowProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2887
    aget-object v19, v3, v9

    const-string v20, "TODAY_DAY_HAIL_PROBABILITY"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayHailProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2888
    aget-object v19, v3, v9

    const-string v20, "TODAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayPrecipitationProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2889
    aget-object v19, v3, v9

    const-string v20, "TODAY_DAY_RAIN_AMOUNT"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayRainAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2890
    aget-object v19, v3, v9

    const-string v20, "TODAY_DAY_SNOW_AMOUNT"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDaySnowAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2891
    aget-object v19, v3, v9

    const-string v20, "TODAY_DAY_HAIL_AMOUNT"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayHailAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2892
    aget-object v19, v3, v9

    const-string v20, "TODAY_DAY_PRECIPITATION_AMOUNT"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayPrecipitationAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2894
    aget-object v19, v3, v9

    const-string v20, "TODAY_NIGHT_RAIN_PROBABILITY"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightRainProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2895
    aget-object v19, v3, v9

    const-string v20, "TODAY_NIGHT_SNOW_PROBABILITY"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightSnowProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2896
    aget-object v19, v3, v9

    const-string v20, "TODAY_NIGHT_HAIL_PROBABILITY"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightHailProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2897
    aget-object v19, v3, v9

    const-string v20, "TODAY_NIGHT_PRECIPITATION_PROBABILITY"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightPrecipitationProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2898
    aget-object v19, v3, v9

    const-string v20, "TODAY_NIGHT_RAIN_AMOUNT"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightRainAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2899
    aget-object v19, v3, v9

    const-string v20, "TODAY_NIGHT_SNOW_AMOUNT"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightSnowAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2900
    aget-object v19, v3, v9

    const-string v20, "TODAY_NIGHT_HAIL_AMOUNT"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightHailAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2901
    aget-object v19, v3, v9

    const-string v20, "TODAY_NIGHT_PRECIPITATION_AMOUNT"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightPrecipitationAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2903
    aget-object v19, v3, v9

    const-string v20, "TODAY_HUM"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getRelativeHumidity()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2904
    aget-object v19, v3, v9

    const-string v20, "TODAY_UV_INDEX"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getUVIndex()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2905
    aget-object v19, v3, v9

    const-string v20, "TODAY_UV_INDEX_TEXT"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getUVIndexText()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2906
    aget-object v19, v3, v9

    const-string v20, "TODAY_DAY_ICON"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayIcon()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2907
    aget-object v19, v3, v9

    const-string v20, "TODAY_NIGHT_ICON"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightIcon()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2908
    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityDataBackup;->getLocation()Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_2

    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityDataBackup;->getLocation()Ljava/lang/String;

    move-result-object v19

    const-string v20, "cityId:current"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_2

    .line 2909
    aget-object v19, v3, v9

    const-string v20, "XML_DETAIL_INFO"

    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityDataBackup;->getXmlDetailInfo()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2914
    :goto_2
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v10, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v12

    .line 2915
    .local v12, "oneday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_DAY_ICON"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getOnedayDayIcon()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2916
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_NIGHT_ICON"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getOnedayNightIcon()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2917
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_HIGH_TEMP"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2918
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_LOW_TEMP"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2919
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_ICON_NUM"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2920
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_URL"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2922
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_DAY_RAIN_PROBABILITY"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayRainProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2923
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_DAY_SNOW_PROBABILITY"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDaySnowProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2924
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_DAY_HAIL_PROBABILITY"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayHailProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2925
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2926
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_DAY_RAIN_AMOUNT"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayRainAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2927
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_DAY_SNOW_AMOUNT"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDaySnowAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2928
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_DAY_HAIL_AMOUNT"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayHailAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2929
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_DAY_PRECIPITATION_AMOUNT"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2931
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_NIGHT_RAIN_PROBABILITY"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightRainProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2932
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_NIGHT_SNOW_PROBABILITY"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightSnowProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2933
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_NIGHT_HAIL_PROBABILITY"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightHailProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2934
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_NIGHT_PRECIPITATION_PROBABILITY"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightPrecipitationProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2935
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_NIGHT_RAIN_AMOUNT"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightRainAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2936
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_NIGHT_SNOW_AMOUNT"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightSnowAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2937
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_NIGHT_HAIL_AMOUNT"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightHailAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2938
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_NIGHT_PRECIPITATION_AMOUNT"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightPrecipitationAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2940
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_SUNRISE_TIME"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2941
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_SUNSET_TIME"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2943
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v10, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v18

    .line 2944
    .local v18, "twoday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    aget-object v19, v3, v9

    const-string v20, "TWODAY_HIGH_TEMP"

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2945
    aget-object v19, v3, v9

    const-string v20, "TWODAY_LOW_TEMP"

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2946
    aget-object v19, v3, v9

    const-string v20, "TWODAY_ICON_NUM"

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2947
    aget-object v19, v3, v9

    const-string v20, "TWODAY_URL"

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2949
    aget-object v19, v3, v9

    const-string v20, "TWODAY_DAY_RAIN_PROBABILITY"

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayRainProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2950
    aget-object v19, v3, v9

    const-string v20, "TWODAY_DAY_SNOW_PROBABILITY"

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDaySnowProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2951
    aget-object v19, v3, v9

    const-string v20, "TWODAY_DAY_HAIL_PROBABILITY"

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayHailProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2952
    aget-object v19, v3, v9

    const-string v20, "TWODAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayHailProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2953
    aget-object v19, v3, v9

    const-string v20, "TWODAY_SUNRISE_TIME"

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2954
    aget-object v19, v3, v9

    const-string v20, "TWODAY_SUNSET_TIME"

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2956
    const/16 v19, 0x2

    move/from16 v0, v19

    invoke-virtual {v10, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v16

    .line 2957
    .local v16, "threeday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    aget-object v19, v3, v9

    const-string v20, "THREEDAY_HIGH_TEMP"

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2958
    aget-object v19, v3, v9

    const-string v20, "THREEDAY_LOW_TEMP"

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2959
    aget-object v19, v3, v9

    const-string v20, "THREEDAY_ICON_NUM"

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2960
    aget-object v19, v3, v9

    const-string v20, "THREEDAY_URL"

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2962
    aget-object v19, v3, v9

    const-string v20, "THREEDAY_DAY_RAIN_PROBABILITY"

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayRainProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2963
    aget-object v19, v3, v9

    const-string v20, "THREEDAY_DAY_SNOW_PROBABILITY"

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDaySnowProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2964
    aget-object v19, v3, v9

    const-string v20, "THREEDAY_DAY_HAIL_PROBABILITY"

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayHailProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2965
    aget-object v19, v3, v9

    const-string v20, "THREEDAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2966
    aget-object v19, v3, v9

    const-string v20, "THREEDAY_SUNRISE_TIME"

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2967
    aget-object v19, v3, v9

    const-string v20, "THREEDAY_SUNSET_TIME"

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2969
    const/16 v19, 0x3

    move/from16 v0, v19

    invoke-virtual {v10, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v8

    .line 2970
    .local v8, "fourday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    aget-object v19, v3, v9

    const-string v20, "FOURDAY_HIGH_TEMP"

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2971
    aget-object v19, v3, v9

    const-string v20, "FOURDAY_LOW_TEMP"

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2972
    aget-object v19, v3, v9

    const-string v20, "FOURDAY_ICON_NUM"

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2973
    aget-object v19, v3, v9

    const-string v20, "FOURDAY_URL"

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2975
    aget-object v19, v3, v9

    const-string v20, "FOURDAY_DAY_RAIN_PROBABILITY"

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayRainProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2976
    aget-object v19, v3, v9

    const-string v20, "FOURDAY_DAY_SNOW_PROBABILITY"

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDaySnowProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2977
    aget-object v19, v3, v9

    const-string v20, "FOURDAY_DAY_HAIL_PROBABILITY"

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayHailProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2978
    aget-object v19, v3, v9

    const-string v20, "FOURDAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2979
    aget-object v19, v3, v9

    const-string v20, "FOURDAY_SUNRISE_TIME"

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2980
    aget-object v19, v3, v9

    const-string v20, "FOURDAY_SUNSET_TIME"

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2982
    const/16 v19, 0x4

    move/from16 v0, v19

    invoke-virtual {v10, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v7

    .line 2983
    .local v7, "fiveday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    aget-object v19, v3, v9

    const-string v20, "FIVEDAY_HIGH_TEMP"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2984
    aget-object v19, v3, v9

    const-string v20, "FIVEDAY_LOW_TEMP"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2985
    aget-object v19, v3, v9

    const-string v20, "FIVEDAY_ICON_NUM"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2986
    aget-object v19, v3, v9

    const-string v20, "FIVEDAY_URL"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2988
    aget-object v19, v3, v9

    const-string v20, "FIVEDAY_DAY_RAIN_PROBABILITY"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayRainProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2989
    aget-object v19, v3, v9

    const-string v20, "FIVEDAY_DAY_SNOW_PROBABILITY"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDaySnowProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2990
    aget-object v19, v3, v9

    const-string v20, "FIVEDAY_DAY_HAIL_PROBABILITY"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayHailProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2991
    aget-object v19, v3, v9

    const-string v20, "FIVEDAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2992
    aget-object v19, v3, v9

    const-string v20, "FIVEDAY_SUNRISE_TIME"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2993
    aget-object v19, v3, v9

    const-string v20, "FIVEDAY_SUNSET_TIME"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2995
    const/16 v19, 0x5

    move/from16 v0, v19

    invoke-virtual {v10, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v15

    .line 2996
    .local v15, "sixday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    aget-object v19, v3, v9

    const-string v20, "SIXDAY_HIGH_TEMP"

    invoke-virtual {v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2997
    aget-object v19, v3, v9

    const-string v20, "SIXDAY_LOW_TEMP"

    invoke-virtual {v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2998
    aget-object v19, v3, v9

    const-string v20, "SIXDAY_ICON_NUM"

    invoke-virtual {v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2999
    aget-object v19, v3, v9

    const-string v20, "SIXDAY_URL"

    invoke-virtual {v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3001
    aget-object v19, v3, v9

    const-string v20, "SIXDAY_DAY_RAIN_PROBABILITY"

    invoke-virtual {v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayRainProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3002
    aget-object v19, v3, v9

    const-string v20, "SIXDAY_DAY_SNOW_PROBABILITY"

    invoke-virtual {v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDaySnowProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3003
    aget-object v19, v3, v9

    const-string v20, "SIXDAY_DAY_HAIL_PROBABILITY"

    invoke-virtual {v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayHailProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3004
    aget-object v19, v3, v9

    const-string v20, "SIXDAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-virtual {v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3005
    aget-object v19, v3, v9

    const-string v20, "SIXDAY_SUNRISE_TIME"

    invoke-virtual {v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3006
    aget-object v19, v3, v9

    const-string v20, "SIXDAY_SUNSET_TIME"

    invoke-virtual {v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2851
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_1

    .line 2911
    .end local v7    # "fiveday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .end local v8    # "fourday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .end local v12    # "oneday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .end local v15    # "sixday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .end local v16    # "threeday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .end local v18    # "twoday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    :cond_2
    aget-object v19, v3, v9

    const-string v20, "XML_DETAIL_INFO"

    const-string v21, ""

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 3009
    .end local v10    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .end local v11    # "item":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityDataBackup;
    .end local v17    # "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    :cond_3
    const/4 v4, 0x0

    .line 3011
    .local v4, "count":I
    :try_start_0
    sget-object v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    move-object/from16 v0, v19

    invoke-virtual {v5, v0, v3}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 3016
    :goto_3
    const-string v19, ""

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "DBHELPER updateChangeOrder "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3018
    if-lez v4, :cond_6

    .line 3020
    :try_start_1
    invoke-static/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/BManager;->isCheckSupportClock(Landroid/content/Context;)Z

    move-result v19

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_4

    .line 3021
    sget-object v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    move-object/from16 v0, v19

    invoke-static {v5, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 3022
    const/16 v19, 0x1

    const-string v20, "DBH UCO"

    move-object/from16 v0, p0

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-static {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/BManager;->sendBroadCastToProvider(Landroid/content/Context;ZLjava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 3028
    :cond_4
    :goto_4
    const/4 v13, 0x1

    .end local v3    # "arrVal":[Landroid/content/ContentValues;
    .end local v4    # "count":I
    .end local v9    # "i":I
    :cond_5
    :goto_5
    move v14, v13

    .line 3037
    .end local v13    # "result":I
    .restart local v14    # "result":I
    goto/16 :goto_0

    .line 3012
    .end local v14    # "result":I
    .restart local v3    # "arrVal":[Landroid/content/ContentValues;
    .restart local v4    # "count":I
    .restart local v9    # "i":I
    .restart local v13    # "result":I
    :catch_0
    move-exception v6

    .line 3013
    .local v6, "e":Ljava/lang/Exception;
    const-string v19, ""

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Can\'t updateChangeOrder "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3014
    const/4 v13, -0x1

    goto :goto_3

    .line 3025
    .end local v6    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v6

    .line 3026
    .restart local v6    # "e":Ljava/lang/Exception;
    :try_start_2
    const-string v19, ""

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "db : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_4

    .line 3032
    :catch_2
    move-exception v6

    .line 3033
    const-string v19, ""

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Exception "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3034
    const/4 v13, -0x1

    goto :goto_5

    .line 3030
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_6
    const/4 v13, -0x1

    goto :goto_5
.end method

.method private static WeatherTempScaleValues(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)Landroid/content/ContentValues;
    .locals 11
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "detailInfo"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    .prologue
    .line 2131
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2133
    .local v0, "data":Landroid/content/ContentValues;
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v6

    .line 2134
    .local v6, "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    const-string v8, "TEMP_SCALE"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2135
    const-string v8, "TODAY_TEMP"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getCurrentTemp()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2136
    const-string v8, "TODAY_HIGH_TEMP"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getHighTemp()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2137
    const-string v8, "TODAY_LOW_TEMP"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getLowTemp()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2138
    const-string v8, "TODAY_REALFELL"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getRealFeel()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2139
    const-string v8, ""

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "CT : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getCurrentTemp()F

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2142
    const/4 v8, 0x0

    invoke-virtual {p1, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v3

    .line 2143
    .local v3, "oneday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    const-string v8, "ONEDAY_HIGH_TEMP"

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2144
    const-string v8, "ONEDAY_LOW_TEMP"

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2147
    const/4 v8, 0x1

    invoke-virtual {p1, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v7

    .line 2148
    .local v7, "twoday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    const-string v8, "TWODAY_HIGH_TEMP"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2149
    const-string v8, "TWODAY_LOW_TEMP"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2152
    const/4 v8, 0x2

    invoke-virtual {p1, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v5

    .line 2153
    .local v5, "threeday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    const-string v8, "THREEDAY_HIGH_TEMP"

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2154
    const-string v8, "THREEDAY_LOW_TEMP"

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2157
    const/4 v8, 0x3

    invoke-virtual {p1, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v2

    .line 2158
    .local v2, "fourday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    const-string v8, "FOURDAY_HIGH_TEMP"

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2159
    const-string v8, "FOURDAY_LOW_TEMP"

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2162
    const/4 v8, 0x4

    invoke-virtual {p1, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v1

    .line 2163
    .local v1, "fiveday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    const-string v8, "FIVEDAY_HIGH_TEMP"

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2164
    const-string v8, "FIVEDAY_LOW_TEMP"

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2167
    const/4 v8, 0x5

    invoke-virtual {p1, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v4

    .line 2168
    .local v4, "sixday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    const-string v8, "SIXDAY_HIGH_TEMP"

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2169
    const-string v8, "SIXDAY_LOW_TEMP"

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2171
    return-object v0
.end method

.method private static buildDetailWeatherContentValues(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)Landroid/content/ContentValues;
    .locals 10
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "info"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    .prologue
    .line 3065
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 3067
    .local v0, "data":Landroid/content/ContentValues;
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v6

    .line 3069
    .local v6, "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    const-string v8, "LATITUDE"

    const-string v9, ""

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3070
    const-string v8, "LONGITUDE"

    const-string v9, ""

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3072
    const-string v8, "SUMMER_TIME"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSummerTime()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3074
    const-string v8, "TIMEZONE"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3075
    const-string v8, "TEMP_SCALE"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getTempScale()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3076
    const-string v8, "TODAY_DATE"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDate()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3077
    const-string v8, "TODAY_TEMP"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getCurrentTemp()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 3078
    const-string v8, "TODAY_HIGH_TEMP"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getHighTemp()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 3079
    const-string v8, "TODAY_LOW_TEMP"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 3080
    const-string v8, "TODAY_ICON_NUM"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3081
    const-string v8, "UPDATE_DATE"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getUpdateDate()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3082
    const-string v8, "TODAY_WEATHER_TEXT"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getWeatherText()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3083
    const-string v8, "TODAY_WEATHER_URL"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3084
    const-string v8, "TODAY_REALFELL"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getRealFeel()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 3085
    const-string v8, "TODAY_SUNRISE_TIME"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3086
    const-string v8, "TODAY_SUNSET_TIME"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3089
    const-string v8, "TODAY_DAY_RAIN_PROBABILITY"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayRainProbability()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3090
    const-string v8, "TODAY_DAY_SNOW_PROBABILITY"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDaySnowProbability()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3091
    const-string v8, "TODAY_DAY_HAIL_PROBABILITY"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayHailProbability()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3092
    const-string v8, "TODAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayPrecipitationProbability()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3093
    const-string v8, "TODAY_DAY_RAIN_AMOUNT"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayRainAmount()Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 3094
    const-string v8, "TODAY_DAY_SNOW_AMOUNT"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDaySnowAmount()Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 3095
    const-string v8, "TODAY_DAY_HAIL_AMOUNT"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayHailAmount()Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 3096
    const-string v8, "TODAY_DAY_PRECIPITATION_AMOUNT"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayPrecipitationAmount()Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 3098
    const-string v8, "TODAY_NIGHT_RAIN_PROBABILITY"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightRainProbability()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3099
    const-string v8, "TODAY_NIGHT_SNOW_PROBABILITY"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightSnowProbability()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3100
    const-string v8, "TODAY_NIGHT_HAIL_PROBABILITY"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightHailProbability()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3101
    const-string v8, "TODAY_NIGHT_PRECIPITATION_PROBABILITY"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightPrecipitationProbability()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3102
    const-string v8, "TODAY_NIGHT_RAIN_AMOUNT"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightRainAmount()Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 3103
    const-string v8, "TODAY_NIGHT_SNOW_AMOUNT"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightSnowAmount()Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 3104
    const-string v8, "TODAY_NIGHT_HAIL_AMOUNT"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightHailAmount()Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 3105
    const-string v8, "TODAY_NIGHT_PRECIPITATION_AMOUNT"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightPrecipitationAmount()Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 3107
    const/4 v8, 0x0

    invoke-virtual {p1, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v3

    .line 3108
    .local v3, "oneday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    const-string v8, "ONEDAY_HIGH_TEMP"

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 3109
    const-string v8, "ONEDAY_LOW_TEMP"

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 3110
    const-string v8, "ONEDAY_ICON_NUM"

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3111
    const-string v8, "ONEDAY_URL"

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3114
    const-string v8, "ONEDAY_DAY_RAIN_PROBABILITY"

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayRainProbability()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3115
    const-string v8, "ONEDAY_DAY_SNOW_PROBABILITY"

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDaySnowProbability()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3116
    const-string v8, "ONEDAY_DAY_HAIL_PROBABILITY"

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayHailProbability()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3117
    const-string v8, "ONEDAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationProbability()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3118
    const-string v8, "ONEDAY_DAY_RAIN_AMOUNT"

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayRainAmount()Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 3119
    const-string v8, "ONEDAY_DAY_SNOW_AMOUNT"

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDaySnowAmount()Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 3120
    const-string v8, "ONEDAY_DAY_HAIL_AMOUNT"

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayHailAmount()Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 3121
    const-string v8, "ONEDAY_DAY_PRECIPITATION_AMOUNT"

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationAmount()Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 3123
    const-string v8, "ONEDAY_NIGHT_RAIN_PROBABILITY"

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightRainProbability()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3124
    const-string v8, "ONEDAY_NIGHT_SNOW_PROBABILITY"

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightSnowProbability()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3125
    const-string v8, "ONEDAY_NIGHT_HAIL_PROBABILITY"

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightHailProbability()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3126
    const-string v8, "ONEDAY_NIGHT_PRECIPITATION_PROBABILITY"

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightPrecipitationProbability()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3127
    const-string v8, "ONEDAY_NIGHT_RAIN_AMOUNT"

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightRainAmount()Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 3128
    const-string v8, "ONEDAY_NIGHT_SNOW_AMOUNT"

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightSnowAmount()Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 3129
    const-string v8, "ONEDAY_NIGHT_HAIL_AMOUNT"

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightHailAmount()Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 3130
    const-string v8, "ONEDAY_NIGHT_PRECIPITATION_AMOUNT"

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightPrecipitationAmount()Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 3132
    const-string v8, "ONEDAY_SUNRISE_TIME"

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3133
    const-string v8, "ONEDAY_SUNSET_TIME"

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3135
    const/4 v8, 0x1

    invoke-virtual {p1, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v7

    .line 3136
    .local v7, "twoday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    const-string v8, "TWODAY_HIGH_TEMP"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 3137
    const-string v8, "TWODAY_LOW_TEMP"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 3138
    const-string v8, "TWODAY_ICON_NUM"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3139
    const-string v8, "TWODAY_URL"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3141
    const-string v8, "TWODAY_DAY_RAIN_PROBABILITY"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayRainProbability()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3142
    const-string v8, "TWODAY_DAY_SNOW_PROBABILITY"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDaySnowProbability()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3143
    const-string v8, "TWODAY_DAY_HAIL_PROBABILITY"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayHailProbability()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3144
    const-string v8, "TWODAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayHailProbability()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3145
    const-string v8, "TWODAY_SUNRISE_TIME"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3146
    const-string v8, "TWODAY_SUNSET_TIME"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3148
    const/4 v8, 0x2

    invoke-virtual {p1, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v5

    .line 3149
    .local v5, "threeday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    const-string v8, "THREEDAY_HIGH_TEMP"

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 3150
    const-string v8, "THREEDAY_LOW_TEMP"

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 3151
    const-string v8, "THREEDAY_ICON_NUM"

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3152
    const-string v8, "THREEDAY_URL"

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3154
    const-string v8, "THREEDAY_DAY_RAIN_PROBABILITY"

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayRainProbability()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3155
    const-string v8, "THREEDAY_DAY_SNOW_PROBABILITY"

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDaySnowProbability()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3156
    const-string v8, "THREEDAY_DAY_HAIL_PROBABILITY"

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayHailProbability()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3157
    const-string v8, "THREEDAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationProbability()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3158
    const-string v8, "THREEDAY_SUNRISE_TIME"

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3159
    const-string v8, "THREEDAY_SUNSET_TIME"

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3161
    const/4 v8, 0x3

    invoke-virtual {p1, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v2

    .line 3162
    .local v2, "fourday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    const-string v8, "FOURDAY_HIGH_TEMP"

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 3163
    const-string v8, "FOURDAY_LOW_TEMP"

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 3164
    const-string v8, "FOURDAY_ICON_NUM"

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3165
    const-string v8, "FOURDAY_URL"

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3167
    const-string v8, "FOURDAY_DAY_RAIN_PROBABILITY"

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayRainProbability()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3168
    const-string v8, "FOURDAY_DAY_SNOW_PROBABILITY"

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDaySnowProbability()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3169
    const-string v8, "FOURDAY_DAY_HAIL_PROBABILITY"

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayHailProbability()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3170
    const-string v8, "FOURDAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationProbability()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3171
    const-string v8, "FOURDAY_SUNRISE_TIME"

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3172
    const-string v8, "FOURDAY_SUNSET_TIME"

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3174
    const/4 v8, 0x4

    invoke-virtual {p1, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v1

    .line 3175
    .local v1, "fiveday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    const-string v8, "FIVEDAY_HIGH_TEMP"

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 3176
    const-string v8, "FIVEDAY_LOW_TEMP"

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 3177
    const-string v8, "FIVEDAY_ICON_NUM"

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3178
    const-string v8, "FIVEDAY_URL"

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3180
    const-string v8, "FIVEDAY_DAY_RAIN_PROBABILITY"

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayRainProbability()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3181
    const-string v8, "FIVEDAY_DAY_SNOW_PROBABILITY"

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDaySnowProbability()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3182
    const-string v8, "FIVEDAY_DAY_HAIL_PROBABILITY"

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayHailProbability()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3183
    const-string v8, "FIVEDAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationProbability()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3184
    const-string v8, "FIVEDAY_SUNRISE_TIME"

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3185
    const-string v8, "FIVEDAY_SUNSET_TIME"

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3187
    const/4 v8, 0x5

    invoke-virtual {p1, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v4

    .line 3188
    .local v4, "sixday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    const-string v8, "SIXDAY_HIGH_TEMP"

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 3189
    const-string v8, "SIXDAY_LOW_TEMP"

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 3190
    const-string v8, "SIXDAY_ICON_NUM"

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3191
    const-string v8, "SIXDAY_URL"

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3193
    const-string v8, "SIXDAY_DAY_RAIN_PROBABILITY"

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayRainProbability()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3194
    const-string v8, "SIXDAY_DAY_SNOW_PROBABILITY"

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDaySnowProbability()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3195
    const-string v8, "SIXDAY_DAY_HAIL_PROBABILITY"

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayHailProbability()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3196
    const-string v8, "SIXDAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationProbability()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3197
    const-string v8, "SIXDAY_SUNRISE_TIME"

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3198
    const-string v8, "SIXDAY_SUNSET_TIME"

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3200
    return-object v0
.end method

.method private static buildDetailWeatherHourContentValues(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)Landroid/content/ContentValues;
    .locals 7
    .param p0, "info"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    .prologue
    const/4 v6, 0x0

    .line 4209
    const/4 v3, 0x0

    .line 4210
    .local v3, "mHourCnt":I
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 4212
    .local v0, "data":Landroid/content/ContentValues;
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getHourSize()I

    move-result v3

    .line 4214
    add-int/lit8 v3, v3, 0x1

    .line 4215
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_0

    .line 4216
    add-int/lit8 v4, v2, -0x1

    invoke-virtual {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getHourInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;

    move-result-object v1

    .line 4217
    .local v1, "hour":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DATE_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->getHourDate()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4218
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "HOUR_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->getHour()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4219
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "TEMP_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->getCurrentTemp()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 4220
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ICON_NUM_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->getIconNum()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4221
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RAIN_FORECAST_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->getRainForecast()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4222
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "WIND_SPEED_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->getWindSpeed()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4215
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 4225
    .end local v1    # "hour":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;
    :cond_0
    const-string v4, "TEMP_SCALE"

    invoke-virtual {p0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getHourInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->getTempScale()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4226
    const-string v4, "UPDATE_DATE"

    invoke-virtual {p0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getHourInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->getUpdateDate()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4228
    return-object v0
.end method

.method private static buildPhotosInofContentValues(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;)Landroid/content/ContentValues;
    .locals 3
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "photosInfo"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;

    .prologue
    .line 3986
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 3988
    .local v0, "data":Landroid/content/ContentValues;
    const-string v1, "LOCATION"

    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;->getCityId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3989
    const-string v1, "PHOTO_SOURCE"

    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;->getSource()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3990
    const-string v1, "PHOTO_DESCRIPTION"

    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;->getDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3991
    const-string v1, "PHOTO_PORTRAIT_LINK"

    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;->getPortraitLink()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3992
    const-string v1, "PHOTO_LANDSCAPE_LINK"

    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;->getLandscapeLink()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3993
    return-object v0
.end method

.method private static buildWeatherContentValues(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)Landroid/content/ContentValues;
    .locals 12
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "cityInfo"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .param p2, "detailInfo"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .param p3, "body"    # Ljava/lang/String;

    .prologue
    .line 1822
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1825
    .local v0, "data":Landroid/content/ContentValues;
    const-string v9, "NAME"

    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getCity()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1826
    const-string v9, "STATE"

    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getState()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1827
    const-string v9, "LOCATION"

    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1828
    const-string v9, "LATITUDE"

    const-string v10, ""

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1829
    const-string v9, "LONGITUDE"

    const-string v10, ""

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1830
    const-string v9, "REAL_LOCATION"

    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getRealLocation()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1833
    invoke-virtual {p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v7

    .line 1834
    .local v7, "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    const-string v9, "SUMMER_TIME"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSummerTime()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1835
    const-string v9, "TIMEZONE"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1836
    const-string v9, "TEMP_SCALE"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1837
    const-string v9, "TODAY_DATE"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDate()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1838
    const-string v9, "TODAY_TEMP"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getCurrentTemp()F

    move-result v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1839
    const-string v9, "TODAY_HIGH_TEMP"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getHighTemp()F

    move-result v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1840
    const-string v9, "TODAY_LOW_TEMP"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getLowTemp()F

    move-result v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1841
    const-string v9, "TODAY_ICON_NUM"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getIconNum()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1842
    const-string v9, "UPDATE_DATE"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getUpdateDate()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1843
    const-string v9, "TODAY_WEATHER_TEXT"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getWeatherText()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1844
    const-string v9, "TODAY_WEATHER_URL"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1845
    const-string v9, "TODAY_REALFELL"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getRealFeel()F

    move-result v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1846
    const-string v9, "TODAY_SUNRISE_TIME"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1847
    const-string v9, "TODAY_SUNSET_TIME"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1850
    const-string v9, "TODAY_DAY_RAIN_PROBABILITY"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayRainProbability()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1851
    const-string v9, "TODAY_DAY_SNOW_PROBABILITY"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDaySnowProbability()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1852
    const-string v9, "TODAY_DAY_HAIL_PROBABILITY"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayHailProbability()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1853
    const-string v9, "TODAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayPrecipitationProbability()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1854
    const-string v9, "TODAY_DAY_RAIN_AMOUNT"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayRainAmount()Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1855
    const-string v9, "TODAY_DAY_SNOW_AMOUNT"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDaySnowAmount()Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1856
    const-string v9, "TODAY_DAY_HAIL_AMOUNT"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayHailAmount()Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1857
    const-string v9, "TODAY_DAY_PRECIPITATION_AMOUNT"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayPrecipitationAmount()Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1859
    const-string v9, "TODAY_NIGHT_RAIN_PROBABILITY"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightRainProbability()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1860
    const-string v9, "TODAY_NIGHT_SNOW_PROBABILITY"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightSnowProbability()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1861
    const-string v9, "TODAY_NIGHT_HAIL_PROBABILITY"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightHailProbability()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1862
    const-string v9, "TODAY_NIGHT_PRECIPITATION_PROBABILITY"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightPrecipitationProbability()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1863
    const-string v9, "TODAY_NIGHT_RAIN_AMOUNT"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightRainAmount()Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1864
    const-string v9, "TODAY_NIGHT_SNOW_AMOUNT"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightSnowAmount()Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1865
    const-string v9, "TODAY_NIGHT_HAIL_AMOUNT"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightHailAmount()Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1866
    const-string v9, "TODAY_NIGHT_PRECIPITATION_AMOUNT"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightPrecipitationAmount()Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1869
    const/4 v9, 0x0

    invoke-virtual {p2, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v4

    .line 1870
    .local v4, "oneday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    const-string v9, "ONEDAY_HIGH_TEMP"

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1871
    const-string v9, "ONEDAY_LOW_TEMP"

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1872
    const-string v9, "ONEDAY_ICON_NUM"

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1873
    const-string v9, "ONEDAY_URL"

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1875
    const-string v9, "ONEDAY_DAY_RAIN_PROBABILITY"

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayRainProbability()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1876
    const-string v9, "ONEDAY_DAY_SNOW_PROBABILITY"

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDaySnowProbability()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1877
    const-string v9, "ONEDAY_DAY_HAIL_PROBABILITY"

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayHailProbability()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1878
    const-string v9, "ONEDAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationProbability()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1879
    const-string v9, "ONEDAY_DAY_RAIN_AMOUNT"

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayRainAmount()Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1880
    const-string v9, "ONEDAY_DAY_SNOW_AMOUNT"

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDaySnowAmount()Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1881
    const-string v9, "ONEDAY_DAY_HAIL_AMOUNT"

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayHailAmount()Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1882
    const-string v9, "ONEDAY_DAY_PRECIPITATION_AMOUNT"

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationAmount()Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1884
    const-string v9, "ONEDAY_NIGHT_RAIN_PROBABILITY"

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightRainProbability()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1885
    const-string v9, "ONEDAY_NIGHT_SNOW_PROBABILITY"

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightSnowProbability()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1886
    const-string v9, "ONEDAY_NIGHT_HAIL_PROBABILITY"

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightHailProbability()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1887
    const-string v9, "ONEDAY_NIGHT_PRECIPITATION_PROBABILITY"

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightPrecipitationProbability()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1888
    const-string v9, "ONEDAY_NIGHT_RAIN_AMOUNT"

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightRainAmount()Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1889
    const-string v9, "ONEDAY_NIGHT_SNOW_AMOUNT"

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightSnowAmount()Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1890
    const-string v9, "ONEDAY_NIGHT_HAIL_AMOUNT"

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightHailAmount()Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1891
    const-string v9, "ONEDAY_NIGHT_PRECIPITATION_AMOUNT"

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightPrecipitationAmount()Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1893
    const-string v9, "ONEDAY_SUNRISE_TIME"

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1894
    const-string v9, "ONEDAY_SUNSET_TIME"

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1897
    const/4 v9, 0x1

    invoke-virtual {p2, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v8

    .line 1898
    .local v8, "twoday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    const-string v9, "TWODAY_HIGH_TEMP"

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1899
    const-string v9, "TWODAY_LOW_TEMP"

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1900
    const-string v9, "TWODAY_ICON_NUM"

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1901
    const-string v9, "TWODAY_URL"

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1903
    const-string v9, "TWODAY_DAY_RAIN_PROBABILITY"

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayRainProbability()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1904
    const-string v9, "TWODAY_DAY_SNOW_PROBABILITY"

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDaySnowProbability()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1905
    const-string v9, "TWODAY_DAY_HAIL_PROBABILITY"

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayHailProbability()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1906
    const-string v9, "TWODAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationProbability()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1907
    const-string v9, "TWODAY_SUNRISE_TIME"

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1908
    const-string v9, "TWODAY_SUNSET_TIME"

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1911
    const/4 v9, 0x2

    invoke-virtual {p2, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v6

    .line 1912
    .local v6, "threeday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    const-string v9, "THREEDAY_HIGH_TEMP"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1913
    const-string v9, "THREEDAY_LOW_TEMP"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1914
    const-string v9, "THREEDAY_ICON_NUM"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1915
    const-string v9, "THREEDAY_URL"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1917
    const-string v9, "THREEDAY_DAY_RAIN_PROBABILITY"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayRainProbability()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1918
    const-string v9, "THREEDAY_DAY_SNOW_PROBABILITY"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDaySnowProbability()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1919
    const-string v9, "THREEDAY_DAY_HAIL_PROBABILITY"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayHailProbability()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1920
    const-string v9, "THREEDAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationProbability()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1921
    const-string v9, "THREEDAY_SUNRISE_TIME"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1922
    const-string v9, "THREEDAY_SUNSET_TIME"

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1925
    const/4 v9, 0x3

    invoke-virtual {p2, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v2

    .line 1926
    .local v2, "fourday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    const-string v9, "FOURDAY_HIGH_TEMP"

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1927
    const-string v9, "FOURDAY_LOW_TEMP"

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1928
    const-string v9, "FOURDAY_ICON_NUM"

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1929
    const-string v9, "FOURDAY_URL"

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1931
    const-string v9, "FOURDAY_DAY_RAIN_PROBABILITY"

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayRainProbability()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1932
    const-string v9, "FOURDAY_DAY_SNOW_PROBABILITY"

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDaySnowProbability()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1933
    const-string v9, "FOURDAY_DAY_HAIL_PROBABILITY"

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayHailProbability()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1934
    const-string v9, "FOURDAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationProbability()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1935
    const-string v9, "FOURDAY_SUNRISE_TIME"

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1936
    const-string v9, "FOURDAY_SUNSET_TIME"

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1939
    const/4 v9, 0x4

    invoke-virtual {p2, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v1

    .line 1940
    .local v1, "fiveday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    const-string v9, "FIVEDAY_HIGH_TEMP"

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1941
    const-string v9, "FIVEDAY_LOW_TEMP"

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1942
    const-string v9, "FIVEDAY_ICON_NUM"

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1943
    const-string v9, "FIVEDAY_URL"

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1945
    const-string v9, "FIVEDAY_DAY_RAIN_PROBABILITY"

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayRainProbability()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1946
    const-string v9, "FIVEDAY_DAY_SNOW_PROBABILITY"

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDaySnowProbability()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1947
    const-string v9, "FIVEDAY_DAY_HAIL_PROBABILITY"

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayHailProbability()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1948
    const-string v9, "FIVEDAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationProbability()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1949
    const-string v9, "FIVEDAY_SUNRISE_TIME"

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1950
    const-string v9, "FIVEDAY_SUNSET_TIME"

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1953
    const/4 v9, 0x5

    invoke-virtual {p2, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v5

    .line 1954
    .local v5, "sixday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    const-string v9, "SIXDAY_HIGH_TEMP"

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1955
    const-string v9, "SIXDAY_LOW_TEMP"

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1956
    const-string v9, "SIXDAY_ICON_NUM"

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1957
    const-string v9, "SIXDAY_URL"

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1959
    const-string v9, "SIXDAY_DAY_RAIN_PROBABILITY"

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayRainProbability()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1960
    const-string v9, "SIXDAY_DAY_SNOW_PROBABILITY"

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDaySnowProbability()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1961
    const-string v9, "SIXDAY_DAY_HAIL_PROBABILITY"

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayHailProbability()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1962
    const-string v9, "SIXDAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationProbability()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1963
    const-string v9, "SIXDAY_SUNRISE_TIME"

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1964
    const-string v9, "SIXDAY_SUNSET_TIME"

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1967
    const-string v9, "TODAY_HUM"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getRelativeHumidity()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1968
    const-string v9, "TODAY_UV_INDEX"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getUVIndex()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1969
    const-string v9, "TODAY_UV_INDEX_TEXT"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getUVIndexText()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1970
    const-string v9, "TODAY_DAY_ICON"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayIcon()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1971
    const-string v9, "TODAY_NIGHT_ICON"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightIcon()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1972
    const-string v9, "ONEDAY_DAY_ICON"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getOnedayDayIcon()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1973
    const-string v9, "ONEDAY_NIGHT_ICON"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getOnedayNightIcon()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1975
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v9

    const-string v10, "cityId:current"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    if-eqz p3, :cond_1

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_1

    .line 1976
    const-string v9, "XML_DETAIL_INFO"

    invoke-virtual {v0, v9, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1981
    :goto_0
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v9

    const-string v10, "cityId:current"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1982
    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v9

    .line 1983
    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v11

    .line 1982
    invoke-static {v9, v10, v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkDayOrNight(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    .line 1986
    .local v3, "isDay":Z
    const-string v9, "WEATHER_ICON_NUM"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getIconNum()I

    move-result v10

    invoke-static {v10, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getWeatherInfoIcon(IZ)I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1989
    .end local v3    # "isDay":Z
    :cond_0
    return-object v0

    .line 1978
    :cond_1
    const-string v9, "XML_DETAIL_INFO"

    const-string v10, ""

    invoke-virtual {v0, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static deleteCitys(Landroid/content/Context;Ljava/util/ArrayList;Z)I
    .locals 12
    .param p0, "ctx"    # Landroid/content/Context;
    .param p2, "forceDeleteCurrentCity"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;Z)I"
        }
    .end annotation

    .prologue
    .local p1, "deleteItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 1177
    const/4 v5, 0x0

    .line 1178
    .local v5, "result":I
    if-nez p0, :cond_0

    .line 1179
    const-string v7, ""

    const-string v8, "dC: context is null"

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v6, v5

    .line 1219
    .end local v5    # "result":I
    .local v6, "result":I
    :goto_0
    return v6

    .line 1182
    .end local v6    # "result":I
    .restart local v5    # "result":I
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1184
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_5

    .line 1185
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v2, v7, :cond_3

    .line 1186
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1187
    .local v3, "location":Ljava/lang/String;
    const-string v7, "Location=\"%s\""

    new-array v8, v10, [Ljava/lang/Object;

    aput-object v3, v8, v11

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1190
    .local v4, "qry":Ljava/lang/String;
    :try_start_0
    sget-object v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    invoke-static {v0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1192
    if-eqz v3, :cond_1

    const-string v7, "cityId:current"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1193
    const/4 v7, 0x0

    invoke-static {p0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateCheckCurrentCityLocation(Landroid/content/Context;I)I

    .line 1194
    if-nez p2, :cond_1

    .line 1185
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1199
    :cond_1
    sget-object v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v4, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1200
    sget-object v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_WEATHERINFO_HOUR_URI:Landroid/net/Uri;

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v4, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 1204
    :catch_0
    move-exception v1

    .line 1205
    .local v1, "e":Ljava/lang/Exception;
    const-string v7, ""

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Exception "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1202
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_2
    :try_start_1
    const-string v7, ""

    const-string v8, "SETTING_URI==null"

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 1209
    .end local v3    # "location":Ljava/lang/String;
    .end local v4    # "qry":Ljava/lang/String;
    :cond_3
    :try_start_2
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/BManager;->isCheckSupportClock(Landroid/content/Context;)Z

    move-result v7

    if-ne v7, v10, :cond_4

    .line 1210
    sget-object v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    invoke-static {v0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1211
    const/4 v7, 0x1

    const-string v8, "DBH DC"

    invoke-static {p0, v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/BManager;->sendBroadCastToProvider(Landroid/content/Context;ZLjava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 1217
    :cond_4
    :goto_3
    const/4 v5, 0x1

    .end local v2    # "i":I
    :cond_5
    move v6, v5

    .line 1219
    .end local v5    # "result":I
    .restart local v6    # "result":I
    goto/16 :goto_0

    .line 1214
    .end local v6    # "result":I
    .restart local v2    # "i":I
    .restart local v5    # "result":I
    :catch_1
    move-exception v1

    .line 1215
    .restart local v1    # "e":Ljava/lang/Exception;
    const-string v7, ""

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "db : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method

.method public static deletePhotosInfo(Landroid/content/Context;Ljava/lang/String;)I
    .locals 7
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "cityId"    # Ljava/lang/String;

    .prologue
    .line 3858
    const/4 v3, 0x0

    .line 3860
    .local v3, "result":I
    if-nez p0, :cond_0

    .line 3861
    const-string v5, ""

    const-string v6, "dPI: context is null"

    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v3

    .line 3882
    .end local v3    # "result":I
    .local v4, "result":I
    :goto_0
    return v4

    .line 3865
    .end local v4    # "result":I
    .restart local v3    # "result":I
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 3866
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_1

    .line 3868
    :try_start_0
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "LOCATION=\'"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 3869
    .local v2, "query":Ljava/lang/String;
    sget-object v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_PHOTOSINFO_URI:Landroid/net/Uri;

    invoke-static {v0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 3870
    sget-object v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_PHOTOSINFO_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v2, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .end local v2    # "query":Ljava/lang/String;
    :cond_1
    :goto_1
    move v4, v3

    .line 3882
    .end local v3    # "result":I
    .restart local v4    # "result":I
    goto :goto_0

    .line 3872
    .end local v4    # "result":I
    .restart local v2    # "query":Ljava/lang/String;
    .restart local v3    # "result":I
    :cond_2
    const-string v5, ""

    const-string v6, "SETTING_URI==null"

    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 3873
    const/4 v3, 0x0

    goto :goto_1

    .line 3875
    .end local v2    # "query":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 3876
    .local v1, "e":Landroid/database/sqlite/SQLiteFullException;
    const/16 v3, -0x5a

    .line 3879
    goto :goto_1

    .line 3877
    .end local v1    # "e":Landroid/database/sqlite/SQLiteFullException;
    :catch_1
    move-exception v1

    .line 3878
    .local v1, "e":Landroid/database/sqlite/SQLiteDiskIOException;
    const/16 v3, -0x5a

    goto :goto_1
.end method

.method public static deletePhotosInfo(Landroid/content/Context;Ljava/util/ArrayList;)I
    .locals 8
    .param p0, "ctx"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 3886
    .local p1, "deleteItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v4, 0x0

    .line 3888
    .local v4, "result":I
    if-nez p0, :cond_0

    .line 3889
    const-string v6, ""

    const-string v7, "dPI: context is null"

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v5, v4

    .line 3914
    .end local v4    # "result":I
    .local v5, "result":I
    :goto_0
    return v5

    .line 3893
    .end local v5    # "result":I
    .restart local v4    # "result":I
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 3894
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_3

    .line 3895
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v2, v6, :cond_3

    .line 3897
    :try_start_0
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getRealLocation(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 3898
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v6, "LOCATION=\'"

    invoke-direct {v7, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 3899
    .local v3, "query":Ljava/lang/String;
    sget-object v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_PHOTOSINFO_URI:Landroid/net/Uri;

    invoke-static {v0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 3900
    sget-object v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_PHOTOSINFO_URI:Landroid/net/Uri;

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v3, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3895
    .end local v3    # "query":Ljava/lang/String;
    :cond_1
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 3902
    .restart local v3    # "query":Ljava/lang/String;
    :cond_2
    const-string v6, ""

    const-string v7, "SETTING_URI==null"

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 3903
    const/4 v4, 0x0

    goto :goto_2

    .line 3906
    .end local v3    # "query":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 3907
    .local v1, "e":Landroid/database/sqlite/SQLiteFullException;
    const/16 v4, -0x5a

    .line 3910
    goto :goto_2

    .line 3908
    .end local v1    # "e":Landroid/database/sqlite/SQLiteFullException;
    :catch_1
    move-exception v1

    .line 3909
    .local v1, "e":Landroid/database/sqlite/SQLiteDiskIOException;
    const/16 v4, -0x5a

    goto :goto_2

    .end local v1    # "e":Landroid/database/sqlite/SQLiteDiskIOException;
    .end local v2    # "i":I
    :cond_3
    move v5, v4

    .line 3914
    .end local v4    # "result":I
    .restart local v5    # "result":I
    goto :goto_0
.end method

.method public static getAllCityList(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 17
    .param p0, "ctx"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 981
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 982
    .local v12, "aryCity":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;>;"
    if-nez p0, :cond_1

    .line 983
    const-string v1, ""

    const-string v2, "gACL: context is null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1052
    :cond_0
    :goto_0
    return-object v12

    .line 987
    :cond_1
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 989
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_4

    .line 990
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 991
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    const/16 v2, 0xa

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "NAME"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "STATE"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "LOCATION"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "SUMMER_TIME"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "LATITUDE"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "LONGITUDE"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "REAL_LOCATION"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "TIMEZONE"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "TODAY_SUNRISE_TIME"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "TODAY_SUNSET_TIME"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 1002
    .local v13, "cursor":Landroid/database/Cursor;
    if-eqz v13, :cond_4

    .line 1003
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1005
    :cond_2
    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    const-string v2, "NAME"

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "STATE"

    .line 1006
    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "LOCATION"

    .line 1007
    invoke-interface {v13, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v13, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "SUMMER_TIME"

    .line 1008
    invoke-interface {v13, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "LATITUDE"

    .line 1009
    invoke-interface {v13, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v13, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, "LONGITUDE"

    .line 1010
    invoke-interface {v13, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v13, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, "REAL_LOCATION"

    .line 1011
    invoke-interface {v13, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v13, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, "TIMEZONE"

    .line 1012
    invoke-interface {v13, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v13, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const-string v10, "TODAY_SUNRISE_TIME"

    .line 1013
    invoke-interface {v13, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v13, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    const-string v11, "TODAY_SUNSET_TIME"

    .line 1014
    invoke-interface {v13, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {v13, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-direct/range {v1 .. v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1005
    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1016
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1018
    :cond_3
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 1025
    .end local v13    # "cursor":Landroid/database/Cursor;
    :cond_4
    :goto_1
    invoke-static/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSettingsForMain(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-result-object v16

    .line 1026
    .local v16, "setting":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_9

    .line 1027
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_2
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v14, v1, :cond_0

    .line 1028
    invoke-virtual {v12, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v1

    const-string v2, "cityId:current"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1029
    invoke-virtual {v12, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    .line 1031
    .local v15, "mCLItem":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;
    if-eqz v16, :cond_8

    .line 1032
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getCurrentLoccation()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_7

    .line 1033
    sget-boolean v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isCurCityAtTopInCityList:Z

    if-eqz v1, :cond_5

    .line 1034
    invoke-virtual {v12, v14}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1035
    const/4 v1, 0x0

    invoke-virtual {v12, v1, v15}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1027
    .end local v15    # "mCLItem":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;
    :cond_5
    :goto_3
    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    .line 1021
    .end local v14    # "i":I
    .end local v16    # "setting":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    :cond_6
    const-string v1, ""

    const-string v2, "SETTING_URI==null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1037
    .restart local v14    # "i":I
    .restart local v15    # "mCLItem":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;
    .restart local v16    # "setting":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    :cond_7
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getCurrentLoccation()I

    move-result v1

    if-nez v1, :cond_5

    .line 1038
    invoke-virtual {v12, v14}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_3

    .line 1041
    :cond_8
    invoke-virtual {v12, v14}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_3

    .line 1045
    .end local v14    # "i":I
    .end local v15    # "mCLItem":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;
    :cond_9
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 1046
    const/4 v1, 0x0

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v1

    const-string v2, "cityId:current"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1047
    if-eqz v16, :cond_a

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getCurrentLoccation()I

    move-result v1

    if-nez v1, :cond_0

    .line 1048
    :cond_a
    const/4 v1, 0x0

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto/16 :goto_0
.end method

.method public static getAllCityList2(Landroid/content/Context;Ljava/util/Calendar;I)Ljava/util/ArrayList;
    .locals 13
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "cal"    # Ljava/util/Calendar;
    .param p2, "dbScale"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Calendar;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 736
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 737
    .local v6, "aryCity":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;>;"
    if-nez p0, :cond_1

    .line 738
    const-string v1, ""

    const-string v2, "gACL: context is null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 789
    :cond_0
    :goto_0
    return-object v6

    .line 742
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 744
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_4

    .line 745
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 746
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->ALL_CITY_LIST_PROJ:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 748
    .local v7, "c":Landroid/database/Cursor;
    if-eqz v7, :cond_4

    .line 749
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 751
    :cond_2
    invoke-static {p0, v7, p1, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->makeCityData(Landroid/content/Context;Landroid/database/Cursor;Ljava/util/Calendar;I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 752
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_2

    .line 754
    :cond_3
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 761
    .end local v7    # "c":Landroid/database/Cursor;
    :cond_4
    :goto_1
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSettingsForMain(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-result-object v10

    .line 763
    .local v10, "setting":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, v12, :cond_9

    .line 764
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_2
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v8, v1, :cond_0

    .line 765
    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getLocation()Ljava/lang/String;

    move-result-object v1

    const-string v2, "cityId:current"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 766
    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;

    .line 768
    .local v9, "mCLItem":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;
    if-eqz v10, :cond_8

    .line 769
    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getCurrentLoccation()I

    move-result v1

    if-ne v1, v12, :cond_7

    .line 770
    sget-boolean v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isCurCityAtTopInCityList:Z

    if-eqz v1, :cond_5

    .line 771
    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 772
    invoke-virtual {v6, v11, v9}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 764
    .end local v9    # "mCLItem":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;
    :cond_5
    :goto_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 757
    .end local v8    # "i":I
    .end local v10    # "setting":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    :cond_6
    const-string v1, ""

    const-string v2, "SETTING_URI==null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 774
    .restart local v8    # "i":I
    .restart local v9    # "mCLItem":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;
    .restart local v10    # "setting":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    :cond_7
    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getCurrentLoccation()I

    move-result v1

    if-nez v1, :cond_5

    .line 775
    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_3

    .line 778
    :cond_8
    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_3

    .line 782
    .end local v8    # "i":I
    .end local v9    # "mCLItem":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;
    :cond_9
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v1, v12, :cond_0

    .line 783
    invoke-virtual {v6, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getLocation()Ljava/lang/String;

    move-result-object v1

    const-string v2, "cityId:current"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 784
    if-eqz v10, :cond_a

    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getCurrentLoccation()I

    move-result v1

    if-nez v1, :cond_0

    .line 785
    :cond_a
    invoke-virtual {v6, v11}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto/16 :goto_0
.end method

.method public static getAllCityListCount(Landroid/content/Context;)I
    .locals 11
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 936
    const/4 v6, 0x0

    .line 937
    .local v6, "count":I
    if-nez p0, :cond_0

    .line 938
    const-string v1, ""

    const-string v2, "gACL: context is null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v4

    .line 977
    :goto_0
    return v1

    .line 942
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 944
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_5

    .line 945
    const/4 v8, 0x0

    .line 946
    .local v8, "hasCurrentLocation":Z
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 947
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v5, "LOCATION"

    aput-object v5, v2, v4

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 949
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_3

    .line 950
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 951
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v6

    .line 953
    if-lez v6, :cond_2

    .line 955
    :cond_1
    const-string v1, "LOCATION"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 956
    .local v9, "location":Ljava/lang/String;
    const-string v1, "cityId:current"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 957
    const/4 v8, 0x1

    .line 963
    .end local v9    # "location":Ljava/lang/String;
    :cond_2
    :goto_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 966
    :cond_3
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSettingsForMain(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-result-object v10

    .line 967
    .local v10, "setting":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    if-eqz v10, :cond_4

    .line 968
    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getCurrentLoccation()I

    move-result v1

    if-nez v1, :cond_5

    if-eqz v8, :cond_5

    .line 970
    :cond_4
    add-int/lit8 v6, v6, -0x1

    .end local v7    # "cursor":Landroid/database/Cursor;
    .end local v8    # "hasCurrentLocation":Z
    .end local v10    # "setting":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    :cond_5
    :goto_2
    move v1, v6

    .line 977
    goto :goto_0

    .line 960
    .restart local v7    # "cursor":Landroid/database/Cursor;
    .restart local v8    # "hasCurrentLocation":Z
    .restart local v9    # "location":Ljava/lang/String;
    :cond_6
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_1

    goto :goto_1

    .line 973
    .end local v7    # "cursor":Landroid/database/Cursor;
    .end local v9    # "location":Ljava/lang/String;
    :cond_7
    const-string v1, ""

    const-string v2, "SETTING_URI==null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public static getAllCityListForBackup(Landroid/content/Context;Ljava/util/Calendar;I)Ljava/util/ArrayList;
    .locals 12
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "cal"    # Ljava/util/Calendar;
    .param p2, "dbScale"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Calendar;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityDataBackup;",
            ">;"
        }
    .end annotation

    .prologue
    .line 793
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 794
    .local v6, "aryCity":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityDataBackup;>;"
    if-nez p0, :cond_1

    .line 795
    const-string v1, ""

    const-string v2, "gACL: context is null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 848
    :cond_0
    :goto_0
    return-object v6

    .line 799
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 801
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_4

    .line 802
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 803
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->ALL_CITY_LIST_PROJ:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 805
    .local v7, "c":Landroid/database/Cursor;
    if-eqz v7, :cond_4

    .line 806
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 808
    :cond_2
    new-instance v8, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityDataBackup;

    invoke-static {p0, v7, p1, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->makeCityData(Landroid/content/Context;Landroid/database/Cursor;Ljava/util/Calendar;I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;

    move-result-object v1

    invoke-direct {v8, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityDataBackup;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;)V

    .line 809
    .local v8, "data":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityDataBackup;
    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityDataBackup;->getLocation()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getDetailWeatherInfoForChangeOrder(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    move-result-object v1

    iput-object v1, v8, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityDataBackup;->info:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    .line 810
    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 811
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_2

    .line 813
    .end local v8    # "data":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityDataBackup;
    :cond_3
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 820
    .end local v7    # "c":Landroid/database/Cursor;
    :cond_4
    :goto_1
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSettingsForMain(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-result-object v11

    .line 822
    .local v11, "setting":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_9

    .line 823
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_2
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v9, v1, :cond_0

    .line 824
    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityDataBackup;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityDataBackup;->getLocation()Ljava/lang/String;

    move-result-object v1

    const-string v2, "cityId:current"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 825
    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityDataBackup;

    .line 827
    .local v10, "mCLItem":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityDataBackup;
    if-eqz v11, :cond_8

    .line 828
    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getCurrentLoccation()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_7

    .line 829
    sget-boolean v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isCurCityAtTopInCityList:Z

    if-eqz v1, :cond_5

    .line 830
    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 831
    const/4 v1, 0x0

    invoke-virtual {v6, v1, v10}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 823
    .end local v10    # "mCLItem":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityDataBackup;
    :cond_5
    :goto_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 816
    .end local v9    # "i":I
    .end local v11    # "setting":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    :cond_6
    const-string v1, ""

    const-string v2, "SETTING_URI==null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 833
    .restart local v9    # "i":I
    .restart local v10    # "mCLItem":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityDataBackup;
    .restart local v11    # "setting":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    :cond_7
    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getCurrentLoccation()I

    move-result v1

    if-nez v1, :cond_5

    .line 834
    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_3

    .line 837
    :cond_8
    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_3

    .line 841
    .end local v9    # "i":I
    .end local v10    # "mCLItem":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityDataBackup;
    :cond_9
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 842
    const/4 v1, 0x0

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityDataBackup;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityDataBackup;->getLocation()Ljava/lang/String;

    move-result-object v1

    const-string v2, "cityId:current"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 843
    if-eqz v11, :cond_a

    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getCurrentLoccation()I

    move-result v1

    if-nez v1, :cond_0

    .line 844
    :cond_a
    const/4 v1, 0x0

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto/16 :goto_0
.end method

.method public static getAllCityListForChangeOrder(Landroid/content/Context;I)Ljava/util/ArrayList;
    .locals 6
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "checkCurrentCitySetting"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1131
    const/4 v0, 0x0

    .line 1133
    .local v0, "aryCity":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;>;"
    if-nez p0, :cond_0

    .line 1134
    const-string v4, ""

    const-string v5, "gACLWSC: context is null"

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v0

    .line 1173
    .end local v0    # "aryCity":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;>;"
    .local v1, "aryCity":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;>;"
    :goto_0
    return-object v1

    .line 1138
    .end local v1    # "aryCity":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;>;"
    .restart local v0    # "aryCity":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;>;"
    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 1140
    .local v2, "calendar":Ljava/util/Calendar;
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getTempScaleSetting(Landroid/content/Context;)I

    move-result v4

    invoke-static {p0, v2, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getAllCityList2(Landroid/content/Context;Ljava/util/Calendar;I)Ljava/util/ArrayList;

    move-result-object v0

    .line 1146
    if-eqz v0, :cond_2

    .line 1147
    if-nez p1, :cond_2

    .line 1148
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v3, v4, :cond_2

    .line 1149
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getLocation()Ljava/lang/String;

    move-result-object v4

    const-string v5, "cityId:current"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1150
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1148
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .end local v3    # "i":I
    :cond_2
    move-object v1, v0

    .line 1173
    .end local v0    # "aryCity":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;>;"
    .restart local v1    # "aryCity":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;>;"
    goto :goto_0
.end method

.method public static getAllCityListForDelete(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 4
    .param p0, "ctx"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1056
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getAllCityList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1057
    .local v0, "aryCity":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 1058
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v2

    const-string v3, "cityId:current"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1059
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1057
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1062
    :cond_1
    return-object v0
.end method

.method public static getAllCityListForDeleteForGrid(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 6
    .param p0, "ctx"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1078
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 1079
    .local v1, "calendar":Ljava/util/Calendar;
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getTempScaleSetting(Landroid/content/Context;)I

    move-result v3

    .line 1081
    .local v3, "tempScale":I
    invoke-static {p0, v1, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getAllCityList2(Landroid/content/Context;Ljava/util/Calendar;I)Ljava/util/ArrayList;

    move-result-object v0

    .line 1082
    .local v0, "aryCity":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v2, v4, :cond_1

    .line 1083
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getLocation()Ljava/lang/String;

    move-result-object v4

    const-string v5, "cityId:current"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1084
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1082
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1087
    :cond_1
    return-object v0
.end method

.method public static getAllCityListForMenuVisibility(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 4
    .param p0, "ctx"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1066
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getAllCityList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1067
    .local v0, "aryCity":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 1068
    sget-boolean v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isCurCityAtTopInCityList:Z

    if-eqz v2, :cond_0

    .line 1069
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v2

    const-string v3, "cityId:current"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1070
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1067
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1074
    :cond_1
    return-object v0
.end method

.method public static getAllCityListIncludedCurrentLocation(Landroid/content/Context;Ljava/util/Calendar;I)Ljava/util/ArrayList;
    .locals 29
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "cal"    # Ljava/util/Calendar;
    .param p2, "dbScale"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Calendar;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 852
    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    .line 853
    .local v24, "aryCity":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;>;"
    if-nez p0, :cond_1

    .line 854
    const-string v3, ""

    const-string v4, "gACL: context is null"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 932
    :cond_0
    :goto_0
    return-object v24

    .line 858
    :cond_1
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 860
    .local v2, "cp":Landroid/content/ContentResolver;
    if-eqz v2, :cond_0

    .line 861
    sget-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 862
    sget-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    const/16 v4, 0x11

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "NAME"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "STATE"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "LOCATION"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "TIMEZONE"

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "REAL_LOCATION"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    const-string v6, "TEMP_SCALE"

    aput-object v6, v4, v5

    const/4 v5, 0x6

    const-string v6, "TODAY_TEMP"

    aput-object v6, v4, v5

    const/4 v5, 0x7

    const-string v6, "TODAY_LOW_TEMP"

    aput-object v6, v4, v5

    const/16 v5, 0x8

    const-string v6, "TODAY_HIGH_TEMP"

    aput-object v6, v4, v5

    const/16 v5, 0x9

    const-string v6, "TODAY_ICON_NUM"

    aput-object v6, v4, v5

    const/16 v5, 0xa

    const-string v6, "TODAY_WEATHER_TEXT"

    aput-object v6, v4, v5

    const/16 v5, 0xb

    const-string v6, "SUMMER_TIME"

    aput-object v6, v4, v5

    const/16 v5, 0xc

    const-string v6, "TODAY_SUNRISE_TIME"

    aput-object v6, v4, v5

    const/16 v5, 0xd

    const-string v6, "TODAY_SUNSET_TIME"

    aput-object v6, v4, v5

    const/16 v5, 0xe

    const-string v6, "LATITUDE"

    aput-object v6, v4, v5

    const/16 v5, 0xf

    const-string v6, "LONGITUDE"

    aput-object v6, v4, v5

    const/16 v5, 0x10

    const-string v6, "XML_DETAIL_INFO"

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v25

    .line 882
    .local v25, "c":Landroid/database/Cursor;
    if-eqz v25, :cond_0

    .line 883
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 885
    :cond_2
    const-string v3, "TEMP_SCALE"

    move-object/from16 v0, v25

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v25

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v28

    .line 886
    .local v28, "tempScale":I
    const/4 v15, 0x0

    .line 887
    .local v15, "dst":I
    const/4 v13, 0x0

    .line 888
    .local v13, "isCurrentLocation":Z
    const-string v3, "REAL_LOCATION"

    move-object/from16 v0, v25

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v25

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 889
    .local v16, "realLocation":Ljava/lang/String;
    if-eqz v16, :cond_3

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_3

    .line 890
    const-string v3, "cityId:current"

    const-string v4, "LOCATION"

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 891
    const/4 v13, 0x1

    .line 895
    :cond_3
    :try_start_0
    const-string v3, "SUMMER_TIME"

    move-object/from16 v0, v25

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v25

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    .line 896
    .local v27, "summerTime":Ljava/lang/String;
    if-eqz v27, :cond_4

    .line 897
    const-string v3, "SUMMER_TIME"

    move-object/from16 v0, v25

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v25

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v15

    .line 902
    .end local v27    # "summerTime":Ljava/lang/String;
    :cond_4
    :goto_1
    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;

    const-string v4, "NAME"

    .line 903
    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "STATE"

    .line 904
    move-object/from16 v0, v25

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v25

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "LOCATION"

    .line 905
    move-object/from16 v0, v25

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v25

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, "TIMEZONE"

    .line 906
    move-object/from16 v0, v25

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    move-object/from16 v0, v25

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getCurrentTime(Landroid/content/Context;Ljava/util/Calendar;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 907
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v8, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->dateFormat(Landroid/content/Context;Ljava/util/Calendar;ZZ)Ljava/lang/String;

    move-result-object v8

    const-string v9, "TODAY_WEATHER_TEXT"

    .line 908
    move-object/from16 v0, v25

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, v25

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const-string v10, "TODAY_TEMP"

    .line 909
    move-object/from16 v0, v25

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    move-object/from16 v0, v25

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getFloat(I)F

    move-result v10

    const/4 v11, 0x0

    move/from16 v0, v28

    move/from16 v1, p2

    invoke-static {v0, v1, v10, v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIFZ)Ljava/lang/String;

    move-result-object v10

    const-string v11, "TODAY_LOW_TEMP"

    .line 910
    move-object/from16 v0, v25

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    move-object/from16 v0, v25

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getFloat(I)F

    move-result v11

    const/4 v12, 0x0

    move/from16 v0, v28

    move/from16 v1, p2

    invoke-static {v0, v1, v11, v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIFZ)Ljava/lang/String;

    move-result-object v11

    const-string v12, "TODAY_HIGH_TEMP"

    .line 911
    move-object/from16 v0, v25

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    move-object/from16 v0, v25

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getFloat(I)F

    move-result v12

    const/4 v14, 0x0

    move/from16 v0, v28

    move/from16 v1, p2

    invoke-static {v0, v1, v12, v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIFZ)Ljava/lang/String;

    move-result-object v12

    const-string v14, "TODAY_ICON_NUM"

    .line 913
    move-object/from16 v0, v25

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, v25

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    const-string v17, "SUMMER_TIME"

    .line 916
    move-object/from16 v0, v25

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    move-object/from16 v0, v25

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    const-string v18, "TODAY_SUNRISE_TIME"

    .line 917
    move-object/from16 v0, v25

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    move-object/from16 v0, v25

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    const-string v19, "TODAY_SUNSET_TIME"

    .line 918
    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    move-object/from16 v0, v25

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    const-string v20, "TIMEZONE"

    .line 919
    move-object/from16 v0, v25

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move-object/from16 v0, v25

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    const-string v21, "LATITUDE"

    .line 920
    move-object/from16 v0, v25

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    move-object/from16 v0, v25

    move/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    const-string v22, "LONGITUDE"

    .line 921
    move-object/from16 v0, v25

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    move-object/from16 v0, v25

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    const-string v23, "XML_DETAIL_INFO"

    .line 922
    move-object/from16 v0, v25

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v23

    move-object/from16 v0, v25

    move/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    invoke-direct/range {v3 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 902
    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 923
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 925
    .end local v13    # "isCurrentLocation":Z
    .end local v15    # "dst":I
    .end local v16    # "realLocation":Ljava/lang/String;
    .end local v28    # "tempScale":I
    :cond_5
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 899
    .restart local v13    # "isCurrentLocation":Z
    .restart local v15    # "dst":I
    .restart local v16    # "realLocation":Ljava/lang/String;
    .restart local v28    # "tempScale":I
    :catch_0
    move-exception v26

    .line 900
    .local v26, "e":Ljava/lang/Exception;
    invoke-virtual/range {v26 .. v26}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    .line 928
    .end local v13    # "isCurrentLocation":Z
    .end local v15    # "dst":I
    .end local v16    # "realLocation":Ljava/lang/String;
    .end local v25    # "c":Landroid/database/Cursor;
    .end local v26    # "e":Ljava/lang/Exception;
    .end local v28    # "tempScale":I
    :cond_6
    const-string v3, ""

    const-string v4, "SETTING_URI==null"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public static getAllCityListWithoutSameCity(Landroid/content/Context;I)Ljava/util/ArrayList;
    .locals 7
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "checkCurrentCitySetting"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1092
    const/4 v0, 0x0

    .line 1094
    .local v0, "aryCity":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;>;"
    if-nez p0, :cond_0

    .line 1095
    const-string v5, ""

    const-string v6, "gACLWSC: context is null"

    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v0

    .line 1126
    .end local v0    # "aryCity":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;>;"
    .local v1, "aryCity":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;>;"
    :goto_0
    return-object v1

    .line 1099
    .end local v1    # "aryCity":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;>;"
    .restart local v0    # "aryCity":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;>;"
    :cond_0
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getAllCityList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1100
    const-string v2, ""

    .line 1101
    .local v2, "currentCityId":Ljava/lang/String;
    const/4 v4, -0x1

    .line 1102
    .local v4, "mCityIDIndex":I
    if-eqz v0, :cond_7

    .line 1103
    if-nez p1, :cond_2

    .line 1104
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v3, v5, :cond_7

    .line 1105
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v5

    const-string v6, "cityId:current"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1106
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1104
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1110
    .end local v3    # "i":I
    :cond_2
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v3, v5, :cond_4

    .line 1111
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v5

    const-string v6, "cityId:current"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1112
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getRealLocation()Ljava/lang/String;

    move-result-object v2

    .line 1113
    move v4, v3

    .line 1110
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1116
    :cond_4
    const/4 v3, 0x0

    :goto_3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v3, v5, :cond_7

    .line 1117
    if-ne v3, v4, :cond_6

    .line 1116
    :cond_5
    :goto_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 1120
    :cond_6
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1121
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_4

    .end local v3    # "i":I
    :cond_7
    move-object v1, v0

    .line 1126
    .end local v0    # "aryCity":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;>;"
    .restart local v1    # "aryCity":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;>;"
    goto :goto_0
.end method

.method public static getAllDetailWeatherInfo(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 15
    .param p0, "ctx"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3340
    if-nez p0, :cond_1

    .line 3341
    const-string v1, ""

    const-string v3, "gDWI: context is null"

    invoke-static {v1, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3342
    const/4 v10, 0x0

    .line 3501
    :cond_0
    :goto_0
    return-object v10

    .line 3344
    :cond_1
    const/4 v10, 0x0

    .line 3345
    .local v10, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;>;"
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 3347
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_4

    .line 3348
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 3349
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/OpenHelper;->sWeatherInfoProjection:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 3352
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_4

    .line 3353
    new-instance v10, Ljava/util/ArrayList;

    .end local v10    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;>;"
    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 3354
    .restart local v10    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;>;"
    const-string v1, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "gADWI : count = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3355
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 3357
    :cond_2
    new-instance v9, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    invoke-direct {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;-><init>()V

    .line 3358
    .local v9, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    new-instance v14, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    invoke-direct {v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;-><init>()V

    .line 3360
    .local v14, "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    const-string v1, "TEMP_SCALE"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 3362
    .local v2, "tempScale":I
    invoke-virtual {v14, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setTempScale(I)V

    .line 3363
    const-string v1, "SUMMER_TIME"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v14, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setSummerTime(Ljava/lang/String;)V

    .line 3364
    const-string v1, "TODAY_DATE"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v14, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDate(Ljava/lang/String;)V

    .line 3365
    const-string v1, "TODAY_TEMP"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v1

    invoke-virtual {v14, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setCurrentTemp(F)V

    .line 3366
    const-string v1, "TODAY_HIGH_TEMP"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v1

    invoke-virtual {v14, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setHighTemp(F)V

    .line 3367
    const-string v1, "TODAY_LOW_TEMP"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v1

    invoke-virtual {v14, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setLowTemp(F)V

    .line 3368
    const-string v1, "TODAY_ICON_NUM"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v14, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setIconNum(I)V

    .line 3369
    const-string v1, "TODAY_WEATHER_TEXT"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v14, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setWeatherText(Ljava/lang/String;)V

    .line 3370
    const-string v1, "TODAY_WEATHER_URL"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v14, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setUrl(Ljava/lang/String;)V

    .line 3371
    const-string v1, "TODAY_REALFELL"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v1

    invoke-virtual {v14, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setRealFeel(F)V

    .line 3372
    const-string v1, "UPDATE_DATE"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v14, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setUpdateDate(Ljava/lang/String;)V

    .line 3373
    const-string v1, "TIMEZONE"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v14, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setTimeZone(Ljava/lang/String;)V

    .line 3374
    const-string v1, "TODAY_SUNRISE_TIME"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v14, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setSunRiseTime(Ljava/lang/String;)V

    .line 3375
    const-string v1, "TODAY_SUNSET_TIME"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v14, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setSunSetTime(Ljava/lang/String;)V

    .line 3378
    const-string v1, "TODAY_DAY_RAIN_PROBABILITY"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v14, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDayRainProbability(I)V

    .line 3379
    const-string v1, "TODAY_DAY_SNOW_PROBABILITY"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v14, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDaySnowProbability(I)V

    .line 3380
    const-string v1, "TODAY_DAY_HAIL_PROBABILITY"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v14, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDayHailProbability(I)V

    .line 3381
    const-string v1, "TODAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v14, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDayPrecipitationProbability(I)V

    .line 3382
    const-string v1, "TODAY_DAY_RAIN_AMOUNT"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    invoke-virtual {v14, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDayRainAmount(D)V

    .line 3383
    const-string v1, "TODAY_DAY_SNOW_AMOUNT"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    invoke-virtual {v14, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDaySnowAmount(D)V

    .line 3384
    const-string v1, "TODAY_DAY_HAIL_AMOUNT"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    invoke-virtual {v14, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDayHailAmount(D)V

    .line 3385
    const-string v1, "TODAY_DAY_PRECIPITATION_AMOUNT"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    invoke-virtual {v14, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDayPrecipitationAmount(D)V

    .line 3387
    const-string v1, "TODAY_NIGHT_RAIN_PROBABILITY"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v14, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightRainProbability(I)V

    .line 3388
    const-string v1, "TODAY_NIGHT_SNOW_PROBABILITY"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v14, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightSnowProbability(I)V

    .line 3389
    const-string v1, "TODAY_NIGHT_HAIL_PROBABILITY"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v14, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightHailProbability(I)V

    .line 3390
    const-string v1, "TODAY_NIGHT_PRECIPITATION_PROBABILITY"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v14, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightPrecipitationProbability(I)V

    .line 3391
    const-string v1, "TODAY_NIGHT_RAIN_AMOUNT"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    invoke-virtual {v14, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightRainAmount(D)V

    .line 3392
    const-string v1, "TODAY_NIGHT_SNOW_AMOUNT"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    invoke-virtual {v14, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightSnowAmount(D)V

    .line 3393
    const-string v1, "TODAY_NIGHT_HAIL_AMOUNT"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    invoke-virtual {v14, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightHailAmount(D)V

    .line 3394
    const-string v1, "TODAY_NIGHT_PRECIPITATION_AMOUNT"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    invoke-virtual {v14, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightPrecipitationAmount(D)V

    .line 3396
    invoke-virtual {v9, v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->setTodayWeatherInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;)V

    .line 3398
    new-instance v12, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    invoke-direct {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;-><init>()V

    .line 3399
    .local v12, "onedayinfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    const-string v1, "ONEDAY_HIGH_TEMP"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v1

    invoke-virtual {v12, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setHighTemp(F)V

    .line 3400
    const-string v1, "ONEDAY_LOW_TEMP"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v1

    invoke-virtual {v12, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setLowTemp(F)V

    .line 3401
    const-string v1, "ONEDAY_ICON_NUM"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v12, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setIconNum(I)V

    .line 3402
    const-string v1, "ONEDAY_URL"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v12, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setUrl(Ljava/lang/String;)V

    .line 3404
    const-string v1, "ONEDAY_DAY_RAIN_PROBABILITY"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v12, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDayRainProbability(I)V

    .line 3405
    const-string v1, "ONEDAY_DAY_SNOW_PROBABILITY"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v12, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDaySnowProbability(I)V

    .line 3406
    const-string v1, "ONEDAY_DAY_HAIL_PROBABILITY"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v12, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDayHailProbability(I)V

    .line 3407
    const-string v1, "ONEDAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v12, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDayPrecipitationProbability(I)V

    .line 3408
    const-string v1, "ONEDAY_DAY_RAIN_AMOUNT"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    invoke-virtual {v12, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDayRainAmount(D)V

    .line 3409
    const-string v1, "ONEDAY_DAY_SNOW_AMOUNT"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    invoke-virtual {v12, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDaySnowAmount(D)V

    .line 3410
    const-string v1, "ONEDAY_DAY_HAIL_AMOUNT"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    invoke-virtual {v12, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDayHailAmount(D)V

    .line 3411
    const-string v1, "ONEDAY_DAY_PRECIPITATION_AMOUNT"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    invoke-virtual {v12, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDayPrecipitationAmount(D)V

    .line 3413
    const-string v1, "ONEDAY_NIGHT_RAIN_PROBABILITY"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v12, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightRainProbability(I)V

    .line 3414
    const-string v1, "ONEDAY_NIGHT_SNOW_PROBABILITY"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v12, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightSnowProbability(I)V

    .line 3415
    const-string v1, "ONEDAY_NIGHT_HAIL_PROBABILITY"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v12, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightHailProbability(I)V

    .line 3416
    const-string v1, "ONEDAY_NIGHT_PRECIPITATION_PROBABILITY"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v12, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightPrecipitationProbability(I)V

    .line 3417
    const-string v1, "ONEDAY_NIGHT_RAIN_AMOUNT"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    invoke-virtual {v12, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightRainAmount(D)V

    .line 3418
    const-string v1, "ONEDAY_NIGHT_SNOW_AMOUNT"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    invoke-virtual {v12, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightSnowAmount(D)V

    .line 3419
    const-string v1, "ONEDAY_NIGHT_HAIL_AMOUNT"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    invoke-virtual {v12, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightHailAmount(D)V

    .line 3420
    const-string v1, "ONEDAY_NIGHT_PRECIPITATION_AMOUNT"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    invoke-virtual {v12, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightPrecipitationAmount(D)V

    .line 3422
    invoke-virtual {v9, v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addForecastInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;)V

    .line 3424
    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    const-string v3, "TWODAY_LOW_TEMP"

    .line 3426
    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    const-string v4, "TWODAY_HIGH_TEMP"

    .line 3427
    invoke-interface {v7, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v7, v4}, Landroid/database/Cursor;->getFloat(I)F

    move-result v4

    const-string v5, "TWODAY_ICON_NUM"

    .line 3428
    invoke-interface {v7, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v7, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    const-string v6, "TWODAY_URL"

    .line 3429
    invoke-interface {v7, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v7, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;-><init>(IFFILjava/lang/String;)V

    .line 3424
    invoke-virtual {v9, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addForecastInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;)V

    .line 3430
    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    const-string v3, "THREEDAY_LOW_TEMP"

    .line 3432
    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    const-string v4, "THREEDAY_HIGH_TEMP"

    .line 3433
    invoke-interface {v7, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v7, v4}, Landroid/database/Cursor;->getFloat(I)F

    move-result v4

    const-string v5, "THREEDAY_ICON_NUM"

    .line 3434
    invoke-interface {v7, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v7, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    const-string v6, "THREEDAY_URL"

    .line 3435
    invoke-interface {v7, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v7, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;-><init>(IFFILjava/lang/String;)V

    .line 3430
    invoke-virtual {v9, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addForecastInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;)V

    .line 3436
    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    const-string v3, "FOURDAY_LOW_TEMP"

    .line 3438
    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    const-string v4, "FOURDAY_HIGH_TEMP"

    .line 3439
    invoke-interface {v7, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v7, v4}, Landroid/database/Cursor;->getFloat(I)F

    move-result v4

    const-string v5, "FOURDAY_ICON_NUM"

    .line 3440
    invoke-interface {v7, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v7, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    const-string v6, "FOURDAY_URL"

    .line 3441
    invoke-interface {v7, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v7, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;-><init>(IFFILjava/lang/String;)V

    .line 3436
    invoke-virtual {v9, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addForecastInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;)V

    .line 3442
    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    const-string v3, "FIVEDAY_LOW_TEMP"

    .line 3444
    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    const-string v4, "FIVEDAY_HIGH_TEMP"

    .line 3445
    invoke-interface {v7, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v7, v4}, Landroid/database/Cursor;->getFloat(I)F

    move-result v4

    const-string v5, "FIVEDAY_ICON_NUM"

    .line 3446
    invoke-interface {v7, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v7, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    const-string v6, "FIVEDAY_URL"

    .line 3447
    invoke-interface {v7, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v7, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;-><init>(IFFILjava/lang/String;)V

    .line 3442
    invoke-virtual {v9, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addForecastInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;)V

    .line 3448
    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    const-string v3, "SIXDAY_LOW_TEMP"

    .line 3450
    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    const-string v4, "SIXDAY_HIGH_TEMP"

    .line 3451
    invoke-interface {v7, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v7, v4}, Landroid/database/Cursor;->getFloat(I)F

    move-result v4

    const-string v5, "SIXDAY_ICON_NUM"

    .line 3452
    invoke-interface {v7, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v7, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    const-string v6, "SIXDAY_URL"

    .line 3453
    invoke-interface {v7, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v7, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;-><init>(IFFILjava/lang/String;)V

    .line 3448
    invoke-virtual {v9, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addForecastInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;)V

    .line 3455
    const-string v1, "NAME"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->setCityName(Ljava/lang/String;)V

    .line 3456
    const-string v1, "LOCATION"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->setLocation(Ljava/lang/String;)V

    .line 3457
    const-string v1, "REAL_LOCATION"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->setRealLocation(Ljava/lang/String;)V

    .line 3459
    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3460
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_2

    .line 3462
    .end local v2    # "tempScale":I
    .end local v9    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .end local v12    # "onedayinfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .end local v14    # "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    :cond_3
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 3470
    .end local v7    # "cursor":Landroid/database/Cursor;
    :cond_4
    :goto_1
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSettingsForMain(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-result-object v13

    .line 3472
    .local v13, "setting":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    if-eqz v10, :cond_0

    .line 3473
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v3, 0x1

    if-le v1, v3, :cond_9

    .line 3474
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_2
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v8, v1, :cond_0

    .line 3475
    invoke-virtual {v10, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getLocation()Ljava/lang/String;

    move-result-object v1

    const-string v3, "cityId:current"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 3476
    invoke-virtual {v10, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    .line 3478
    .local v11, "mCLItem":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    if-eqz v13, :cond_8

    .line 3479
    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getCurrentLoccation()I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_7

    .line 3480
    sget-boolean v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isCurCityAtTopInCityList:Z

    if-eqz v1, :cond_5

    .line 3481
    invoke-virtual {v10, v8}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 3482
    const/4 v1, 0x0

    invoke-virtual {v10, v1, v11}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 3474
    .end local v11    # "mCLItem":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    :cond_5
    :goto_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 3465
    .end local v8    # "i":I
    .end local v13    # "setting":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    :cond_6
    const-string v1, ""

    const-string v3, "SETTING_URI==null"

    invoke-static {v1, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 3484
    .restart local v8    # "i":I
    .restart local v11    # "mCLItem":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .restart local v13    # "setting":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    :cond_7
    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getCurrentLoccation()I

    move-result v1

    if-nez v1, :cond_5

    .line 3485
    invoke-virtual {v10, v8}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_3

    .line 3488
    :cond_8
    invoke-virtual {v10, v8}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_3

    .line 3492
    .end local v8    # "i":I
    .end local v11    # "mCLItem":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    :cond_9
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_0

    .line 3493
    const/4 v1, 0x0

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getLocation()Ljava/lang/String;

    move-result-object v1

    const-string v3, "cityId:current"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3494
    if-eqz v13, :cond_a

    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getCurrentLoccation()I

    move-result v1

    if-nez v1, :cond_0

    .line 3495
    :cond_a
    const/4 v1, 0x0

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto/16 :goto_0
.end method

.method public static getAllPackageNameList(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 9
    .param p0, "ctx"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v3, 0x0

    .line 3680
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 3682
    .local v7, "packageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 3683
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_2

    .line 3684
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_PACKAGENAME_URI:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 3685
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_PACKAGENAME_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "PACKAGENAME"

    aput-object v4, v2, v8

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 3687
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    .line 3688
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3690
    :cond_0
    invoke-interface {v6, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3691
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3693
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 3699
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_2
    :goto_0
    return-object v7

    .line 3696
    :cond_3
    const-string v1, ""

    const-string v2, "SETTING_URI==null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getAutoRefreshTime(Landroid/content/Context;)I
    .locals 10
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x0

    const/4 v3, 0x0

    .line 2334
    const/4 v7, 0x0

    .line 2335
    .local v7, "time":I
    if-nez p0, :cond_0

    .line 2336
    const-string v1, ""

    const-string v2, "gART: context is null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v8, v7

    .line 2354
    .end local v7    # "time":I
    .local v8, "time":I
    :goto_0
    return v8

    .line 2339
    .end local v8    # "time":I
    .restart local v7    # "time":I
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2340
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_2

    .line 2341
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2342
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "AUTO_REFRESH_TIME"

    aput-object v4, v2, v9

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2344
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    .line 2345
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2346
    invoke-interface {v6, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 2348
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_2
    :goto_1
    move v8, v7

    .line 2354
    .end local v7    # "time":I
    .restart local v8    # "time":I
    goto :goto_0

    .line 2351
    .end local v8    # "time":I
    .restart local v7    # "time":I
    :cond_3
    const-string v1, ""

    const-string v2, "SETTING_URI==null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static getAutoscrollSettings(Landroid/content/Context;)I
    .locals 10
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x0

    const/4 v3, 0x0

    .line 194
    const/4 v7, 0x0

    .line 195
    .local v7, "time":I
    if-nez p0, :cond_0

    .line 196
    const-string v1, ""

    const-string v2, "gAS: context is null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v8, v7

    .line 214
    .end local v7    # "time":I
    .local v8, "time":I
    :goto_0
    return v8

    .line 199
    .end local v8    # "time":I
    .restart local v7    # "time":I
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 200
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_2

    .line 201
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 202
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "AUTO_SCROLL"

    aput-object v4, v2, v9

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 204
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    .line 205
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 206
    invoke-interface {v6, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 208
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_2
    :goto_1
    move v8, v7

    .line 214
    .end local v7    # "time":I
    .restart local v8    # "time":I
    goto :goto_0

    .line 211
    .end local v8    # "time":I
    .restart local v7    # "time":I
    :cond_3
    const-string v1, ""

    const-string v2, "SETTING_URI==null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static getCheckCurrentCityLocation(Landroid/content/Context;)I
    .locals 10
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 274
    const/4 v8, 0x0

    .line 275
    .local v8, "result":I
    if-nez p0, :cond_0

    .line 276
    const-string v1, ""

    const-string v2, "gCCCL: context is null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v9, v8

    .line 298
    .end local v8    # "result":I
    .local v9, "result":I
    :goto_0
    return v9

    .line 279
    .end local v9    # "result":I
    .restart local v8    # "result":I
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 280
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_2

    .line 281
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 282
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "CHECK_CURRENT_CITY_LOCATION"

    aput-object v4, v2, v5

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 284
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    .line 285
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 287
    const/4 v1, 0x0

    :try_start_0
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    .line 292
    :cond_1
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_2
    :goto_2
    move v9, v8

    .line 298
    .end local v8    # "result":I
    .restart local v9    # "result":I
    goto :goto_0

    .line 288
    .end local v9    # "result":I
    .restart local v6    # "cursor":Landroid/database/Cursor;
    .restart local v8    # "result":I
    :catch_0
    move-exception v7

    .line 289
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 295
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_3
    const-string v1, ""

    const-string v2, "SETTING_URI==null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public static getCurrentCity(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 19
    .param p0, "ctx"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3506
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 3507
    .local v16, "aryCity":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;>;"
    if-nez p0, :cond_1

    .line 3508
    const-string v2, ""

    const-string v3, "gACL: context is null"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3554
    :cond_0
    :goto_0
    return-object v16

    .line 3512
    :cond_1
    invoke-static/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSettingsForMain(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-result-object v18

    .line 3513
    .local v18, "setting":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    if-eqz v18, :cond_0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getCurrentLoccation()I

    move-result v2

    if-eqz v2, :cond_0

    .line 3517
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 3519
    .local v1, "cp":Landroid/content/ContentResolver;
    if-eqz v1, :cond_0

    .line 3520
    const-string v2, "Location=\"%s\""

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "cityId:current"

    aput-object v6, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 3521
    .local v4, "loc":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 3522
    sget-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    const/16 v3, 0xa

    new-array v3, v3, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "NAME"

    aput-object v6, v3, v5

    const/4 v5, 0x1

    const-string v6, "STATE"

    aput-object v6, v3, v5

    const/4 v5, 0x2

    const-string v6, "LOCATION"

    aput-object v6, v3, v5

    const/4 v5, 0x3

    const-string v6, "SUMMER_TIME"

    aput-object v6, v3, v5

    const/4 v5, 0x4

    const-string v6, "LATITUDE"

    aput-object v6, v3, v5

    const/4 v5, 0x5

    const-string v6, "LONGITUDE"

    aput-object v6, v3, v5

    const/4 v5, 0x6

    const-string v6, "REAL_LOCATION"

    aput-object v6, v3, v5

    const/4 v5, 0x7

    const-string v6, "TIMEZONE"

    aput-object v6, v3, v5

    const/16 v5, 0x8

    const-string v6, "TODAY_SUNRISE_TIME"

    aput-object v6, v3, v5

    const/16 v5, 0x9

    const-string v6, "TODAY_SUNSET_TIME"

    aput-object v6, v3, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    .line 3533
    .local v17, "cursor":Landroid/database/Cursor;
    if-eqz v17, :cond_0

    .line 3534
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 3536
    :cond_2
    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    const-string v2, "NAME"

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v2, "STATE"

    .line 3537
    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v2, "LOCATION"

    .line 3538
    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const-string v2, "SUMMER_TIME"

    .line 3539
    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const-string v2, "LATITUDE"

    .line 3540
    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    const-string v2, "LONGITUDE"

    .line 3541
    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const-string v2, "REAL_LOCATION"

    .line 3542
    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    const-string v2, "TIMEZONE"

    .line 3543
    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    const-string v2, "TODAY_SUNRISE_TIME"

    .line 3544
    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    const-string v2, "TODAY_SUNSET_TIME"

    .line 3545
    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-direct/range {v5 .. v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3536
    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3546
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 3548
    :cond_3
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 3551
    .end local v17    # "cursor":Landroid/database/Cursor;
    :cond_4
    const-string v2, ""

    const-string v3, "SETTING_URI==null"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public static getCurrentCityinfo(Landroid/content/Context;)Ljava/lang/String;
    .locals 10
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 3304
    const-string v6, ""

    .line 3305
    .local v6, "curName":Ljava/lang/String;
    const-string v7, ""

    .line 3307
    .local v7, "curState":Ljava/lang/String;
    if-nez p0, :cond_0

    .line 3308
    const-string v1, ""

    const-string v2, "gRL: context is null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3336
    :goto_0
    return-object v4

    .line 3311
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 3312
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_2

    .line 3313
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 3314
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "NAME"

    aput-object v5, v2, v3

    const/4 v3, 0x1

    const-string v5, "STATE"

    aput-object v5, v2, v3

    const-string v3, "LOCATION=\"cityId:current\""

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 3318
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_2

    .line 3319
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3320
    const-string v1, "NAME"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 3321
    const-string v1, "STATE"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 3323
    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 3331
    .end local v8    # "cursor":Landroid/database/Cursor;
    :cond_2
    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    .line 3332
    .local v9, "result":Ljava/lang/StringBuffer;
    invoke-virtual {v9, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3333
    const-string v1, "\n"

    invoke-virtual {v9, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3334
    invoke-virtual {v9, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3336
    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 3326
    .end local v9    # "result":Ljava/lang/StringBuffer;
    :cond_3
    const-string v1, ""

    const-string v2, "SETTING_URI==null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getCurrentLocationSetting(Landroid/content/Context;)I
    .locals 9
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v8, 0x0

    .line 2200
    if-nez p0, :cond_1

    move v7, v8

    .line 2221
    :cond_0
    :goto_0
    return v7

    .line 2203
    :cond_1
    const/4 v7, 0x0

    .line 2204
    .local v7, "value":I
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2206
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_0

    .line 2207
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2208
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "CHECK_CURRENT_CITY_LOCATION"

    aput-object v4, v2, v8

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2211
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    .line 2212
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2213
    invoke-interface {v6, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 2215
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 2218
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_3
    const-string v1, ""

    const-string v2, "SETTING_URI==null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getCurrentLocationTodayInfo(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    .locals 12
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 3558
    const/4 v9, 0x0

    .line 3559
    .local v9, "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    const/4 v7, 0x0

    .line 3560
    .local v7, "result":I
    if-nez p0, :cond_0

    .line 3561
    const-string v1, ""

    const-string v2, "gDDFM: context is null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3562
    const/4 v7, 0x0

    .line 3617
    :goto_0
    return-object v9

    .line 3564
    :cond_0
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSettingsForMain(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-result-object v8

    .line 3566
    .local v8, "setting":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    const-string v1, "cityId:current"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz v8, :cond_1

    .line 3567
    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getCurrentLoccation()I

    move-result v1

    if-nez v1, :cond_2

    .line 3568
    :cond_1
    const/4 v7, 0x1

    goto :goto_0

    .line 3570
    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 3572
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_6

    .line 3573
    const-string v1, "Location=\"%s\""

    new-array v2, v11, [Ljava/lang/Object;

    aput-object p1, v2, v10

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 3574
    .local v3, "loc":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 3575
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    const/16 v2, 0xb

    new-array v2, v2, [Ljava/lang/String;

    const-string v5, "TEMP_SCALE"

    aput-object v5, v2, v10

    const-string v5, "TODAY_TEMP"

    aput-object v5, v2, v11

    const/4 v5, 0x2

    const-string v10, "TODAY_HIGH_TEMP"

    aput-object v10, v2, v5

    const/4 v5, 0x3

    const-string v10, "TODAY_LOW_TEMP"

    aput-object v10, v2, v5

    const/4 v5, 0x4

    const-string v10, "TODAY_ICON_NUM"

    aput-object v10, v2, v5

    const/4 v5, 0x5

    const-string v10, "TODAY_WEATHER_TEXT"

    aput-object v10, v2, v5

    const/4 v5, 0x6

    const-string v10, "UPDATE_DATE"

    aput-object v10, v2, v5

    const/4 v5, 0x7

    const-string v10, "TODAY_SUNRISE_TIME"

    aput-object v10, v2, v5

    const/16 v5, 0x8

    const-string v10, "TODAY_SUNSET_TIME"

    aput-object v10, v2, v5

    const/16 v5, 0x9

    const-string v10, "TIMEZONE"

    aput-object v10, v2, v5

    const/16 v5, 0xa

    const-string v10, "SUMMER_TIME"

    aput-object v10, v2, v5

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 3589
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_4

    .line 3590
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 3591
    new-instance v9, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    .end local v9    # "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    invoke-direct {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;-><init>()V

    .line 3592
    .restart local v9    # "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    const-string v1, "TEMP_SCALE"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v9, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setTempScale(I)V

    .line 3593
    const-string v1, "TODAY_TEMP"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v1

    invoke-virtual {v9, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setCurrentTemp(F)V

    .line 3594
    const-string v1, "TODAY_HIGH_TEMP"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v1

    invoke-virtual {v9, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setHighTemp(F)V

    .line 3595
    const-string v1, "TODAY_LOW_TEMP"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v1

    invoke-virtual {v9, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setLowTemp(F)V

    .line 3596
    const-string v1, "TODAY_ICON_NUM"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v9, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setIconNum(I)V

    .line 3597
    const-string v1, "TODAY_WEATHER_TEXT"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setWeatherText(Ljava/lang/String;)V

    .line 3598
    const-string v1, "UPDATE_DATE"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setUpdateDate(Ljava/lang/String;)V

    .line 3599
    const-string v1, "TODAY_SUNRISE_TIME"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setSunRiseTime(Ljava/lang/String;)V

    .line 3600
    const-string v1, "TODAY_SUNSET_TIME"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setSunSetTime(Ljava/lang/String;)V

    .line 3601
    const-string v1, "TIMEZONE"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setTimeZone(Ljava/lang/String;)V

    .line 3602
    const-string v1, "SUMMER_TIME"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setSummerTime(Ljava/lang/String;)V

    .line 3604
    :cond_3
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 3607
    :cond_4
    const/4 v7, 0x1

    .line 3608
    goto/16 :goto_0

    .line 3609
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_5
    const-string v1, ""

    const-string v2, "SETTING_URI==null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 3613
    .end local v3    # "loc":Ljava/lang/String;
    :cond_6
    const/4 v7, 0x0

    goto/16 :goto_0
.end method

.method public static getDaemonDetailWeatherInfo(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .locals 22
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    .line 1589
    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    invoke-direct/range {v19 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;-><init>()V

    .line 1590
    .local v19, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    if-nez p0, :cond_1

    .line 1591
    const-string v3, ""

    const-string v4, "gDWI: context is null"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1759
    :cond_0
    :goto_0
    return-object v19

    .line 1595
    :cond_1
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 1597
    .local v2, "cp":Landroid/content/ContentResolver;
    if-eqz v2, :cond_0

    .line 1598
    const-string v3, "Location=\"%s\""

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 1599
    .local v5, "loc":Ljava/lang/String;
    sget-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1600
    sget-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    sget-object v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/OpenHelper;->sWeatherInfoProjection:[Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 1602
    .local v18, "cursor":Landroid/database/Cursor;
    if-eqz v18, :cond_0

    .line 1603
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1604
    const-string v3, "TEMP_SCALE"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 1605
    .local v7, "tempScale":I
    const-string v3, "NAME"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->setCityName(Ljava/lang/String;)V

    .line 1606
    const-string v3, "STATE"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->setState(Ljava/lang/String;)V

    .line 1607
    const-string v3, "LOCATION"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->setLocation(Ljava/lang/String;)V

    .line 1610
    new-instance v21, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    invoke-direct/range {v21 .. v21}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;-><init>()V

    .line 1611
    .local v21, "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setTempScale(I)V

    .line 1612
    const-string v3, "TODAY_DATE"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDate(Ljava/lang/String;)V

    .line 1613
    const-string v3, "TODAY_TEMP"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setCurrentTemp(F)V

    .line 1614
    const-string v3, "TODAY_HIGH_TEMP"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setHighTemp(F)V

    .line 1615
    const-string v3, "TODAY_LOW_TEMP"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setLowTemp(F)V

    .line 1616
    const-string v3, "TODAY_ICON_NUM"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setIconNum(I)V

    .line 1617
    const-string v3, "TODAY_WEATHER_TEXT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setWeatherText(Ljava/lang/String;)V

    .line 1618
    const-string v3, "TODAY_WEATHER_URL"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setUrl(Ljava/lang/String;)V

    .line 1619
    const-string v3, "TODAY_REALFELL"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setRealFeel(F)V

    .line 1620
    const-string v3, "UPDATE_DATE"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setUpdateDate(Ljava/lang/String;)V

    .line 1621
    const-string v3, "TIMEZONE"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setTimeZone(Ljava/lang/String;)V

    .line 1622
    const-string v3, "TODAY_SUNRISE_TIME"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setSunRiseTime(Ljava/lang/String;)V

    .line 1623
    const-string v3, "TODAY_SUNSET_TIME"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setSunSetTime(Ljava/lang/String;)V

    .line 1626
    const-string v3, "TODAY_DAY_RAIN_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDayRainProbability(I)V

    .line 1627
    const-string v3, "TODAY_DAY_SNOW_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDaySnowProbability(I)V

    .line 1628
    const-string v3, "TODAY_DAY_HAIL_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDayHailProbability(I)V

    .line 1629
    const-string v3, "TODAY_DAY_PRECIPITATION_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDayPrecipitationProbability(I)V

    .line 1630
    const-string v3, "TODAY_DAY_RAIN_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDayRainAmount(D)V

    .line 1631
    const-string v3, "TODAY_DAY_SNOW_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDaySnowAmount(D)V

    .line 1632
    const-string v3, "TODAY_DAY_HAIL_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDayHailAmount(D)V

    .line 1633
    const-string v3, "TODAY_DAY_PRECIPITATION_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDayPrecipitationAmount(D)V

    .line 1635
    const-string v3, "TODAY_NIGHT_RAIN_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightRainProbability(I)V

    .line 1636
    const-string v3, "TODAY_NIGHT_SNOW_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightSnowProbability(I)V

    .line 1637
    const-string v3, "TODAY_NIGHT_HAIL_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightHailProbability(I)V

    .line 1638
    const-string v3, "TODAY_NIGHT_PRECIPITATION_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightPrecipitationProbability(I)V

    .line 1639
    const-string v3, "TODAY_NIGHT_RAIN_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightRainAmount(D)V

    .line 1640
    const-string v3, "TODAY_NIGHT_SNOW_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightSnowAmount(D)V

    .line 1641
    const-string v3, "TODAY_NIGHT_HAIL_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightHailAmount(D)V

    .line 1642
    const-string v3, "TODAY_NIGHT_PRECIPITATION_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightPrecipitationAmount(D)V

    .line 1644
    const-string v3, "TODAY_HUM"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setRelativeHumidity(Ljava/lang/String;)V

    .line 1645
    const-string v3, "TODAY_UV_INDEX"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setUVIndex(I)V

    .line 1646
    const-string v3, "TODAY_UV_INDEX_TEXT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setUVIndexText(Ljava/lang/String;)V

    .line 1647
    const-string v3, "TODAY_DAY_ICON"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDayIcon(I)V

    .line 1648
    const-string v3, "TODAY_NIGHT_ICON"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightIcon(I)V

    .line 1650
    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->setTodayWeatherInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;)V

    .line 1652
    new-instance v20, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    invoke-direct/range {v20 .. v20}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;-><init>()V

    .line 1653
    .local v20, "onedayinfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    const-string v3, "ONEDAY_HIGH_TEMP"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setHighTemp(F)V

    .line 1654
    const-string v3, "ONEDAY_LOW_TEMP"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setLowTemp(F)V

    .line 1655
    const-string v3, "ONEDAY_ICON_NUM"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setIconNum(I)V

    .line 1656
    const-string v3, "ONEDAY_URL"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setUrl(Ljava/lang/String;)V

    .line 1657
    const-string v3, "ONEDAY_DAY_ICON"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setOnedayDayIcon(I)V

    .line 1658
    const-string v3, "ONEDAY_NIGHT_ICON"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setOnedayNightIcon(I)V

    .line 1660
    const-string v3, "ONEDAY_DAY_RAIN_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDayRainProbability(I)V

    .line 1661
    const-string v3, "ONEDAY_DAY_SNOW_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDaySnowProbability(I)V

    .line 1662
    const-string v3, "ONEDAY_DAY_HAIL_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDayHailProbability(I)V

    .line 1663
    const-string v3, "ONEDAY_DAY_PRECIPITATION_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDayPrecipitationProbability(I)V

    .line 1664
    const-string v3, "ONEDAY_DAY_RAIN_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDayRainAmount(D)V

    .line 1665
    const-string v3, "ONEDAY_DAY_SNOW_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDaySnowAmount(D)V

    .line 1666
    const-string v3, "ONEDAY_DAY_HAIL_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDayHailAmount(D)V

    .line 1667
    const-string v3, "ONEDAY_DAY_PRECIPITATION_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDayPrecipitationAmount(D)V

    .line 1669
    const-string v3, "ONEDAY_NIGHT_RAIN_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightRainProbability(I)V

    .line 1670
    const-string v3, "ONEDAY_NIGHT_SNOW_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightSnowProbability(I)V

    .line 1671
    const-string v3, "ONEDAY_NIGHT_HAIL_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightHailProbability(I)V

    .line 1672
    const-string v3, "ONEDAY_NIGHT_PRECIPITATION_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightPrecipitationProbability(I)V

    .line 1673
    const-string v3, "ONEDAY_NIGHT_RAIN_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightRainAmount(D)V

    .line 1674
    const-string v3, "ONEDAY_NIGHT_SNOW_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightSnowAmount(D)V

    .line 1675
    const-string v3, "ONEDAY_NIGHT_HAIL_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightHailAmount(D)V

    .line 1676
    const-string v3, "ONEDAY_NIGHT_PRECIPITATION_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightPrecipitationAmount(D)V

    .line 1678
    const-string v3, "ONEDAY_SUNRISE_TIME"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setSunRiseTime(Ljava/lang/String;)V

    .line 1679
    const-string v3, "ONEDAY_SUNSET_TIME"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setSunSetTime(Ljava/lang/String;)V

    .line 1681
    invoke-virtual/range {v19 .. v20}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addForecastInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;)V

    .line 1683
    new-instance v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    const-string v3, "TWODAY_LOW_TEMP"

    .line 1685
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v8

    const-string v3, "TWODAY_HIGH_TEMP"

    .line 1686
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v9

    const-string v3, "TWODAY_ICON_NUM"

    .line 1687
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    const-string v3, "TWODAY_URL"

    .line 1688
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const-string v3, "TWODAY_DAY_RAIN_PROBABILITY"

    .line 1690
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    const-string v3, "TWODAY_DAY_SNOW_PROBABILITY"

    .line 1691
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    const-string v3, "TWODAY_DAY_HAIL_PROBABILITY"

    .line 1692
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    const-string v3, "TWODAY_DAY_PRECIPITATION_PROBABILITY"

    .line 1693
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    const-string v3, "TWODAY_SUNRISE_TIME"

    .line 1694
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    const-string v3, "TWODAY_SUNSET_TIME"

    .line 1695
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v6 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;-><init>(IFFILjava/lang/String;IIIILjava/lang/String;Ljava/lang/String;)V

    .line 1683
    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addForecastInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;)V

    .line 1697
    new-instance v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    const-string v3, "THREEDAY_LOW_TEMP"

    .line 1699
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v8

    const-string v3, "THREEDAY_HIGH_TEMP"

    .line 1700
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v9

    const-string v3, "THREEDAY_ICON_NUM"

    .line 1701
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    const-string v3, "THREEDAY_URL"

    .line 1702
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const-string v3, "THREEDAY_DAY_RAIN_PROBABILITY"

    .line 1704
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    const-string v3, "THREEDAY_DAY_SNOW_PROBABILITY"

    .line 1705
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    const-string v3, "THREEDAY_DAY_HAIL_PROBABILITY"

    .line 1706
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    const-string v3, "THREEDAY_DAY_PRECIPITATION_PROBABILITY"

    .line 1707
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    const-string v3, "THREEDAY_SUNRISE_TIME"

    .line 1708
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    const-string v3, "THREEDAY_SUNSET_TIME"

    .line 1709
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v6 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;-><init>(IFFILjava/lang/String;IIIILjava/lang/String;Ljava/lang/String;)V

    .line 1697
    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addForecastInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;)V

    .line 1711
    new-instance v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    const-string v3, "FOURDAY_LOW_TEMP"

    .line 1713
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v8

    const-string v3, "FOURDAY_HIGH_TEMP"

    .line 1714
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v9

    const-string v3, "FOURDAY_ICON_NUM"

    .line 1715
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    const-string v3, "FOURDAY_URL"

    .line 1716
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const-string v3, "FOURDAY_DAY_RAIN_PROBABILITY"

    .line 1718
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    const-string v3, "FOURDAY_DAY_SNOW_PROBABILITY"

    .line 1719
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    const-string v3, "FOURDAY_DAY_HAIL_PROBABILITY"

    .line 1720
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    const-string v3, "FOURDAY_DAY_PRECIPITATION_PROBABILITY"

    .line 1721
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    const-string v3, "FOURDAY_SUNRISE_TIME"

    .line 1722
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    const-string v3, "FOURDAY_SUNSET_TIME"

    .line 1723
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v6 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;-><init>(IFFILjava/lang/String;IIIILjava/lang/String;Ljava/lang/String;)V

    .line 1711
    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addForecastInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;)V

    .line 1725
    new-instance v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    const-string v3, "FIVEDAY_LOW_TEMP"

    .line 1727
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v8

    const-string v3, "FIVEDAY_HIGH_TEMP"

    .line 1728
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v9

    const-string v3, "FIVEDAY_ICON_NUM"

    .line 1729
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    const-string v3, "FIVEDAY_URL"

    .line 1730
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const-string v3, "FIVEDAY_DAY_RAIN_PROBABILITY"

    .line 1732
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    const-string v3, "FIVEDAY_DAY_SNOW_PROBABILITY"

    .line 1733
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    const-string v3, "FIVEDAY_DAY_HAIL_PROBABILITY"

    .line 1734
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    const-string v3, "FIVEDAY_DAY_PRECIPITATION_PROBABILITY"

    .line 1735
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    const-string v3, "FIVEDAY_SUNRISE_TIME"

    .line 1736
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    const-string v3, "FIVEDAY_SUNSET_TIME"

    .line 1737
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v6 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;-><init>(IFFILjava/lang/String;IIIILjava/lang/String;Ljava/lang/String;)V

    .line 1725
    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addForecastInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;)V

    .line 1739
    new-instance v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    const-string v3, "SIXDAY_LOW_TEMP"

    .line 1741
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v8

    const-string v3, "SIXDAY_HIGH_TEMP"

    .line 1742
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v9

    const-string v3, "SIXDAY_ICON_NUM"

    .line 1743
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    const-string v3, "SIXDAY_URL"

    .line 1744
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const-string v3, "SIXDAY_DAY_RAIN_PROBABILITY"

    .line 1746
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    const-string v3, "SIXDAY_DAY_SNOW_PROBABILITY"

    .line 1747
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    const-string v3, "SIXDAY_DAY_HAIL_PROBABILITY"

    .line 1748
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    const-string v3, "SIXDAY_DAY_PRECIPITATION_PROBABILITY"

    .line 1749
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    const-string v3, "SIXDAY_SUNRISE_TIME"

    .line 1750
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    const-string v3, "SIXDAY_SUNSET_TIME"

    .line 1751
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v6 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;-><init>(IFFILjava/lang/String;IIIILjava/lang/String;Ljava/lang/String;)V

    .line 1739
    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addForecastInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;)V

    .line 1753
    .end local v7    # "tempScale":I
    .end local v20    # "onedayinfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .end local v21    # "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    :cond_2
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 1756
    .end local v18    # "cursor":Landroid/database/Cursor;
    :cond_3
    const-string v3, ""

    const-string v4, "SETTING_URI==null"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public static getDaemonWeatherIconNum(Landroid/content/Context;)I
    .locals 10
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 3997
    const/4 v6, 0x0

    .line 3998
    .local v6, "convertIcon":I
    if-nez p0, :cond_0

    .line 3999
    const-string v1, ""

    const-string v2, "gDCLWR: context is null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v7, v6

    .line 4023
    .end local v6    # "convertIcon":I
    .local v7, "convertIcon":I
    :goto_0
    return v7

    .line 4002
    .end local v7    # "convertIcon":I
    .restart local v6    # "convertIcon":I
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 4003
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_2

    .line 4005
    :try_start_0
    const-string v1, "Location=\"%s\""

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "cityId:current"

    aput-object v5, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 4006
    .local v3, "loc":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 4007
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "WEATHER_ICON_NUM"

    aput-object v5, v2, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 4010
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_2

    .line 4011
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4012
    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 4014
    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 4022
    .end local v3    # "loc":Ljava/lang/String;
    .end local v8    # "cursor":Landroid/database/Cursor;
    :cond_2
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ci : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v7, v6

    .line 4023
    .end local v6    # "convertIcon":I
    .restart local v7    # "convertIcon":I
    goto :goto_0

    .line 4017
    .end local v7    # "convertIcon":I
    .restart local v6    # "convertIcon":I
    :catch_0
    move-exception v9

    .line 4018
    .local v9, "e":Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    move v7, v6

    .line 4019
    .end local v6    # "convertIcon":I
    .restart local v7    # "convertIcon":I
    goto :goto_0
.end method

.method public static getDateTimeString(JLandroid/content/Context;)Ljava/lang/String;
    .locals 6
    .param p0, "timemills"    # J
    .param p2, "ctx"    # Landroid/content/Context;

    .prologue
    .line 146
    :try_start_0
    invoke-static {p2}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    .line 147
    .local v1, "dateformat":Ljava/text/DateFormat;
    invoke-static {p2}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v3

    .line 149
    .local v3, "timeformat":Ljava/text/DateFormat;
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p0, p1}, Ljava/util/Date;-><init>(J)V

    .line 150
    .local v0, "date":Ljava/util/Date;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 152
    .end local v0    # "date":Ljava/util/Date;
    .end local v1    # "dateformat":Ljava/text/DateFormat;
    .end local v3    # "timeformat":Ljava/text/DateFormat;
    :goto_0
    return-object v4

    .line 151
    :catch_0
    move-exception v2

    .line 152
    .local v2, "ex":Ljava/lang/Exception;
    const-string v4, ""

    goto :goto_0
.end method

.method public static getDetailWeatherHourInfo(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .locals 14
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/16 v13, 0xc

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 4233
    new-instance v9, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    invoke-direct {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;-><init>()V

    .line 4234
    .local v9, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    if-nez p0, :cond_1

    .line 4235
    const-string v1, ""

    const-string v2, "gDWHI: context is null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4320
    :cond_0
    :goto_0
    return-object v9

    .line 4238
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 4241
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_0

    .line 4242
    const-string v1, "LOCATION=\"%s\""

    new-array v2, v12, [Ljava/lang/Object;

    aput-object p1, v2, v11

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 4243
    .local v3, "loc":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_WEATHERINFO_HOUR_URI:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 4244
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_WEATHERINFO_HOUR_URI:Landroid/net/Uri;

    const/16 v2, 0x4a

    new-array v2, v2, [Ljava/lang/String;

    const-string v5, "DATE_1"

    aput-object v5, v2, v11

    const-string v5, "HOUR_1"

    aput-object v5, v2, v12

    const/4 v5, 0x2

    const-string v11, "TEMP_1"

    aput-object v11, v2, v5

    const/4 v5, 0x3

    const-string v11, "ICON_NUM_1"

    aput-object v11, v2, v5

    const/4 v5, 0x4

    const-string v11, "RAIN_FORECAST_1"

    aput-object v11, v2, v5

    const/4 v5, 0x5

    const-string v11, "WIND_SPEED_1"

    aput-object v11, v2, v5

    const/4 v5, 0x6

    const-string v11, "DATE_2"

    aput-object v11, v2, v5

    const/4 v5, 0x7

    const-string v11, "HOUR_2"

    aput-object v11, v2, v5

    const/16 v5, 0x8

    const-string v11, "TEMP_2"

    aput-object v11, v2, v5

    const/16 v5, 0x9

    const-string v11, "ICON_NUM_2"

    aput-object v11, v2, v5

    const/16 v5, 0xa

    const-string v11, "RAIN_FORECAST_2"

    aput-object v11, v2, v5

    const/16 v5, 0xb

    const-string v11, "WIND_SPEED_2"

    aput-object v11, v2, v5

    const-string v5, "DATE_3"

    aput-object v5, v2, v13

    const/16 v5, 0xd

    const-string v11, "HOUR_3"

    aput-object v11, v2, v5

    const/16 v5, 0xe

    const-string v11, "TEMP_3"

    aput-object v11, v2, v5

    const/16 v5, 0xf

    const-string v11, "ICON_NUM_3"

    aput-object v11, v2, v5

    const/16 v5, 0x10

    const-string v11, "RAIN_FORECAST_3"

    aput-object v11, v2, v5

    const/16 v5, 0x11

    const-string v11, "WIND_SPEED_3"

    aput-object v11, v2, v5

    const/16 v5, 0x12

    const-string v11, "DATE_4"

    aput-object v11, v2, v5

    const/16 v5, 0x13

    const-string v11, "HOUR_4"

    aput-object v11, v2, v5

    const/16 v5, 0x14

    const-string v11, "TEMP_4"

    aput-object v11, v2, v5

    const/16 v5, 0x15

    const-string v11, "ICON_NUM_4"

    aput-object v11, v2, v5

    const/16 v5, 0x16

    const-string v11, "RAIN_FORECAST_4"

    aput-object v11, v2, v5

    const/16 v5, 0x17

    const-string v11, "WIND_SPEED_4"

    aput-object v11, v2, v5

    const/16 v5, 0x18

    const-string v11, "DATE_5"

    aput-object v11, v2, v5

    const/16 v5, 0x19

    const-string v11, "HOUR_5"

    aput-object v11, v2, v5

    const/16 v5, 0x1a

    const-string v11, "TEMP_5"

    aput-object v11, v2, v5

    const/16 v5, 0x1b

    const-string v11, "ICON_NUM_5"

    aput-object v11, v2, v5

    const/16 v5, 0x1c

    const-string v11, "RAIN_FORECAST_5"

    aput-object v11, v2, v5

    const/16 v5, 0x1d

    const-string v11, "WIND_SPEED_5"

    aput-object v11, v2, v5

    const/16 v5, 0x1e

    const-string v11, "DATE_6"

    aput-object v11, v2, v5

    const/16 v5, 0x1f

    const-string v11, "HOUR_6"

    aput-object v11, v2, v5

    const/16 v5, 0x20

    const-string v11, "TEMP_6"

    aput-object v11, v2, v5

    const/16 v5, 0x21

    const-string v11, "ICON_NUM_6"

    aput-object v11, v2, v5

    const/16 v5, 0x22

    const-string v11, "RAIN_FORECAST_6"

    aput-object v11, v2, v5

    const/16 v5, 0x23

    const-string v11, "WIND_SPEED_6"

    aput-object v11, v2, v5

    const/16 v5, 0x24

    const-string v11, "DATE_7"

    aput-object v11, v2, v5

    const/16 v5, 0x25

    const-string v11, "HOUR_7"

    aput-object v11, v2, v5

    const/16 v5, 0x26

    const-string v11, "TEMP_7"

    aput-object v11, v2, v5

    const/16 v5, 0x27

    const-string v11, "ICON_NUM_7"

    aput-object v11, v2, v5

    const/16 v5, 0x28

    const-string v11, "RAIN_FORECAST_7"

    aput-object v11, v2, v5

    const/16 v5, 0x29

    const-string v11, "WIND_SPEED_7"

    aput-object v11, v2, v5

    const/16 v5, 0x2a

    const-string v11, "DATE_8"

    aput-object v11, v2, v5

    const/16 v5, 0x2b

    const-string v11, "HOUR_8"

    aput-object v11, v2, v5

    const/16 v5, 0x2c

    const-string v11, "TEMP_8"

    aput-object v11, v2, v5

    const/16 v5, 0x2d

    const-string v11, "ICON_NUM_8"

    aput-object v11, v2, v5

    const/16 v5, 0x2e

    const-string v11, "RAIN_FORECAST_8"

    aput-object v11, v2, v5

    const/16 v5, 0x2f

    const-string v11, "WIND_SPEED_8"

    aput-object v11, v2, v5

    const/16 v5, 0x30

    const-string v11, "DATE_9"

    aput-object v11, v2, v5

    const/16 v5, 0x31

    const-string v11, "HOUR_9"

    aput-object v11, v2, v5

    const/16 v5, 0x32

    const-string v11, "TEMP_9"

    aput-object v11, v2, v5

    const/16 v5, 0x33

    const-string v11, "ICON_NUM_9"

    aput-object v11, v2, v5

    const/16 v5, 0x34

    const-string v11, "RAIN_FORECAST_9"

    aput-object v11, v2, v5

    const/16 v5, 0x35

    const-string v11, "WIND_SPEED_9"

    aput-object v11, v2, v5

    const/16 v5, 0x36

    const-string v11, "DATE_10"

    aput-object v11, v2, v5

    const/16 v5, 0x37

    const-string v11, "HOUR_10"

    aput-object v11, v2, v5

    const/16 v5, 0x38

    const-string v11, "TEMP_10"

    aput-object v11, v2, v5

    const/16 v5, 0x39

    const-string v11, "ICON_NUM_10"

    aput-object v11, v2, v5

    const/16 v5, 0x3a

    const-string v11, "RAIN_FORECAST_10"

    aput-object v11, v2, v5

    const/16 v5, 0x3b

    const-string v11, "WIND_SPEED_10"

    aput-object v11, v2, v5

    const/16 v5, 0x3c

    const-string v11, "DATE_11"

    aput-object v11, v2, v5

    const/16 v5, 0x3d

    const-string v11, "HOUR_11"

    aput-object v11, v2, v5

    const/16 v5, 0x3e

    const-string v11, "TEMP_11"

    aput-object v11, v2, v5

    const/16 v5, 0x3f

    const-string v11, "ICON_NUM_11"

    aput-object v11, v2, v5

    const/16 v5, 0x40

    const-string v11, "RAIN_FORECAST_11"

    aput-object v11, v2, v5

    const/16 v5, 0x41

    const-string v11, "WIND_SPEED_11"

    aput-object v11, v2, v5

    const/16 v5, 0x42

    const-string v11, "DATE_12"

    aput-object v11, v2, v5

    const/16 v5, 0x43

    const-string v11, "HOUR_12"

    aput-object v11, v2, v5

    const/16 v5, 0x44

    const-string v11, "TEMP_12"

    aput-object v11, v2, v5

    const/16 v5, 0x45

    const-string v11, "ICON_NUM_12"

    aput-object v11, v2, v5

    const/16 v5, 0x46

    const-string v11, "RAIN_FORECAST_12"

    aput-object v11, v2, v5

    const/16 v5, 0x47

    const-string v11, "WIND_SPEED_12"

    aput-object v11, v2, v5

    const/16 v5, 0x48

    const-string v11, "UPDATE_DATE"

    aput-object v11, v2, v5

    const/16 v5, 0x49

    const-string v11, "TEMP_SCALE"

    aput-object v11, v2, v5

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 4286
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    .line 4287
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 4289
    const-string v1, "TEMP_SCALE"

    .line 4290
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 4289
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 4293
    .local v10, "tempScale":I
    const/4 v8, 0x1

    .local v8, "i":I
    :goto_1
    if-gt v8, v13, :cond_3

    .line 4294
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DATE_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_2

    .line 4293
    :goto_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 4296
    :cond_2
    new-instance v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;

    invoke-direct {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;-><init>()V

    .line 4297
    .local v7, "hour":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DATE_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v7, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->setHourDate(I)V

    .line 4298
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HOUR_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v7, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->setHour(I)V

    .line 4299
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TEMP_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v1

    invoke-virtual {v7, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->setCurrentTemp(F)V

    .line 4301
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ICON_NUM_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 4302
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 4301
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v7, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->setIconNum(I)V

    .line 4303
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "RAIN_FORECAST_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 4304
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 4303
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v7, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->setRainForecast(I)V

    .line 4305
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "WIND_SPEED_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v7, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->setWindSpeed(I)V

    .line 4307
    const-string v1, "UPDATE_DATE"

    .line 4308
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 4307
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->setUpdateDate(Ljava/lang/String;)V

    .line 4309
    invoke-virtual {v7, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->setTempScale(I)V

    .line 4310
    invoke-virtual {v9, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addHourInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;)V

    goto/16 :goto_2

    .line 4313
    .end local v7    # "hour":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;
    .end local v8    # "i":I
    .end local v10    # "tempScale":I
    :cond_3
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 4316
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_4
    const-string v1, ""

    const-string v2, "SETTING_URI==null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public static getDetailWeatherInfo(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .locals 23
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    .line 1223
    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    invoke-direct/range {v19 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;-><init>()V

    .line 1224
    .local v19, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    if-nez p1, :cond_1

    .line 1225
    const-string v3, ""

    const-string v4, "city id is null!!!!!!"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1407
    :cond_0
    :goto_0
    return-object v19

    .line 1228
    :cond_1
    if-nez p0, :cond_2

    .line 1229
    const-string v3, ""

    const-string v4, "gDWI: context is null"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1233
    :cond_2
    const-string v3, "cityId:current"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1234
    invoke-static/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSettingsForMain(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-result-object v21

    .line 1235
    .local v21, "setting":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    if-eqz v21, :cond_0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getCurrentLoccation()I

    move-result v3

    if-eqz v3, :cond_0

    .line 1240
    .end local v21    # "setting":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    :cond_3
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 1242
    .local v2, "cp":Landroid/content/ContentResolver;
    if-eqz v2, :cond_0

    .line 1243
    const-string v3, "Location=\"%s\""

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 1244
    .local v5, "loc":Ljava/lang/String;
    sget-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1245
    sget-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    sget-object v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/OpenHelper;->sWeatherInfoProjection:[Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 1247
    .local v18, "cursor":Landroid/database/Cursor;
    if-eqz v18, :cond_0

    .line 1248
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1249
    const-string v3, "TEMP_SCALE"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 1250
    .local v7, "tempScale":I
    const-string v3, "NAME"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->setCityName(Ljava/lang/String;)V

    .line 1251
    const-string v3, "STATE"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->setState(Ljava/lang/String;)V

    .line 1252
    const-string v3, "LOCATION"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->setLocation(Ljava/lang/String;)V

    .line 1255
    new-instance v22, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    invoke-direct/range {v22 .. v22}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;-><init>()V

    .line 1256
    .local v22, "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setTempScale(I)V

    .line 1257
    const-string v3, "TODAY_DATE"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDate(Ljava/lang/String;)V

    .line 1258
    const-string v3, "TODAY_TEMP"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setCurrentTemp(F)V

    .line 1259
    const-string v3, "TODAY_HIGH_TEMP"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setHighTemp(F)V

    .line 1260
    const-string v3, "TODAY_LOW_TEMP"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setLowTemp(F)V

    .line 1261
    const-string v3, "TODAY_ICON_NUM"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setIconNum(I)V

    .line 1262
    const-string v3, "TODAY_WEATHER_TEXT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setWeatherText(Ljava/lang/String;)V

    .line 1263
    const-string v3, "TODAY_WEATHER_URL"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setUrl(Ljava/lang/String;)V

    .line 1264
    const-string v3, "TODAY_REALFELL"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setRealFeel(F)V

    .line 1265
    const-string v3, "UPDATE_DATE"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setUpdateDate(Ljava/lang/String;)V

    .line 1266
    const-string v3, "TIMEZONE"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setTimeZone(Ljava/lang/String;)V

    .line 1267
    const-string v3, "TODAY_SUNRISE_TIME"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setSunRiseTime(Ljava/lang/String;)V

    .line 1268
    const-string v3, "TODAY_SUNSET_TIME"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setSunSetTime(Ljava/lang/String;)V

    .line 1271
    const-string v3, "WEATHER_ICON_NUM"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setConverterWeatherIconNum(I)V

    .line 1274
    const-string v3, "TODAY_DAY_RAIN_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDayRainProbability(I)V

    .line 1275
    const-string v3, "TODAY_DAY_SNOW_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDaySnowProbability(I)V

    .line 1276
    const-string v3, "TODAY_DAY_HAIL_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDayHailProbability(I)V

    .line 1277
    const-string v3, "TODAY_DAY_PRECIPITATION_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDayPrecipitationProbability(I)V

    .line 1278
    const-string v3, "TODAY_DAY_RAIN_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDayRainAmount(D)V

    .line 1279
    const-string v3, "TODAY_DAY_SNOW_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDaySnowAmount(D)V

    .line 1280
    const-string v3, "TODAY_DAY_HAIL_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDayHailAmount(D)V

    .line 1281
    const-string v3, "TODAY_DAY_PRECIPITATION_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDayPrecipitationAmount(D)V

    .line 1283
    const-string v3, "TODAY_NIGHT_RAIN_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightRainProbability(I)V

    .line 1284
    const-string v3, "TODAY_NIGHT_SNOW_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightSnowProbability(I)V

    .line 1285
    const-string v3, "TODAY_NIGHT_HAIL_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightHailProbability(I)V

    .line 1286
    const-string v3, "TODAY_NIGHT_PRECIPITATION_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightPrecipitationProbability(I)V

    .line 1287
    const-string v3, "TODAY_NIGHT_RAIN_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightRainAmount(D)V

    .line 1288
    const-string v3, "TODAY_NIGHT_SNOW_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightSnowAmount(D)V

    .line 1289
    const-string v3, "TODAY_NIGHT_HAIL_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightHailAmount(D)V

    .line 1290
    const-string v3, "TODAY_NIGHT_PRECIPITATION_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightPrecipitationAmount(D)V

    .line 1292
    const-string v3, "TODAY_HUM"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setRelativeHumidity(Ljava/lang/String;)V

    .line 1293
    const-string v3, "TODAY_UV_INDEX"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setUVIndex(I)V

    .line 1294
    const-string v3, "TODAY_UV_INDEX_TEXT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setUVIndexText(Ljava/lang/String;)V

    .line 1295
    const-string v3, "TODAY_DAY_ICON"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDayIcon(I)V

    .line 1296
    const-string v3, "TODAY_NIGHT_ICON"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightIcon(I)V

    .line 1298
    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->setTodayWeatherInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;)V

    .line 1300
    new-instance v20, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    invoke-direct/range {v20 .. v20}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;-><init>()V

    .line 1301
    .local v20, "onedayinfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    const-string v3, "ONEDAY_HIGH_TEMP"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setHighTemp(F)V

    .line 1302
    const-string v3, "ONEDAY_LOW_TEMP"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setLowTemp(F)V

    .line 1303
    const-string v3, "ONEDAY_ICON_NUM"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setIconNum(I)V

    .line 1304
    const-string v3, "ONEDAY_URL"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setUrl(Ljava/lang/String;)V

    .line 1305
    const-string v3, "ONEDAY_DAY_ICON"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setOnedayDayIcon(I)V

    .line 1306
    const-string v3, "ONEDAY_NIGHT_ICON"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setOnedayNightIcon(I)V

    .line 1308
    const-string v3, "ONEDAY_DAY_RAIN_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDayRainProbability(I)V

    .line 1309
    const-string v3, "ONEDAY_DAY_SNOW_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDaySnowProbability(I)V

    .line 1310
    const-string v3, "ONEDAY_DAY_HAIL_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDayHailProbability(I)V

    .line 1311
    const-string v3, "ONEDAY_DAY_PRECIPITATION_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDayPrecipitationProbability(I)V

    .line 1312
    const-string v3, "ONEDAY_DAY_RAIN_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDayRainAmount(D)V

    .line 1313
    const-string v3, "ONEDAY_DAY_SNOW_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDaySnowAmount(D)V

    .line 1314
    const-string v3, "ONEDAY_DAY_HAIL_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDayHailAmount(D)V

    .line 1315
    const-string v3, "ONEDAY_DAY_PRECIPITATION_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDayPrecipitationAmount(D)V

    .line 1317
    const-string v3, "ONEDAY_NIGHT_RAIN_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightRainProbability(I)V

    .line 1318
    const-string v3, "ONEDAY_NIGHT_SNOW_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightSnowProbability(I)V

    .line 1319
    const-string v3, "ONEDAY_NIGHT_HAIL_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightHailProbability(I)V

    .line 1320
    const-string v3, "ONEDAY_NIGHT_PRECIPITATION_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightPrecipitationProbability(I)V

    .line 1321
    const-string v3, "ONEDAY_NIGHT_RAIN_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightRainAmount(D)V

    .line 1322
    const-string v3, "ONEDAY_NIGHT_SNOW_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightSnowAmount(D)V

    .line 1323
    const-string v3, "ONEDAY_NIGHT_HAIL_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightHailAmount(D)V

    .line 1324
    const-string v3, "ONEDAY_NIGHT_PRECIPITATION_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightPrecipitationAmount(D)V

    .line 1326
    const-string v3, "ONEDAY_SUNRISE_TIME"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setSunRiseTime(Ljava/lang/String;)V

    .line 1327
    const-string v3, "ONEDAY_SUNSET_TIME"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setSunSetTime(Ljava/lang/String;)V

    .line 1329
    invoke-virtual/range {v19 .. v20}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addForecastInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;)V

    .line 1331
    new-instance v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    const-string v3, "TWODAY_LOW_TEMP"

    .line 1333
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v8

    const-string v3, "TWODAY_HIGH_TEMP"

    .line 1334
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v9

    const-string v3, "TWODAY_ICON_NUM"

    .line 1335
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    const-string v3, "TWODAY_URL"

    .line 1336
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const-string v3, "TWODAY_DAY_RAIN_PROBABILITY"

    .line 1338
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    const-string v3, "TWODAY_DAY_SNOW_PROBABILITY"

    .line 1339
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    const-string v3, "TWODAY_DAY_HAIL_PROBABILITY"

    .line 1340
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    const-string v3, "TWODAY_DAY_PRECIPITATION_PROBABILITY"

    .line 1341
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    const-string v3, "TWODAY_SUNRISE_TIME"

    .line 1342
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    const-string v3, "TWODAY_SUNSET_TIME"

    .line 1343
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v6 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;-><init>(IFFILjava/lang/String;IIIILjava/lang/String;Ljava/lang/String;)V

    .line 1331
    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addForecastInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;)V

    .line 1345
    new-instance v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    const-string v3, "THREEDAY_LOW_TEMP"

    .line 1347
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v8

    const-string v3, "THREEDAY_HIGH_TEMP"

    .line 1348
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v9

    const-string v3, "THREEDAY_ICON_NUM"

    .line 1349
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    const-string v3, "THREEDAY_URL"

    .line 1350
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const-string v3, "THREEDAY_DAY_RAIN_PROBABILITY"

    .line 1352
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    const-string v3, "THREEDAY_DAY_SNOW_PROBABILITY"

    .line 1353
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    const-string v3, "THREEDAY_DAY_HAIL_PROBABILITY"

    .line 1354
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    const-string v3, "THREEDAY_DAY_PRECIPITATION_PROBABILITY"

    .line 1355
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    const-string v3, "THREEDAY_SUNRISE_TIME"

    .line 1356
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    const-string v3, "THREEDAY_SUNSET_TIME"

    .line 1357
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v6 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;-><init>(IFFILjava/lang/String;IIIILjava/lang/String;Ljava/lang/String;)V

    .line 1345
    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addForecastInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;)V

    .line 1359
    new-instance v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    const-string v3, "FOURDAY_LOW_TEMP"

    .line 1361
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v8

    const-string v3, "FOURDAY_HIGH_TEMP"

    .line 1362
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v9

    const-string v3, "FOURDAY_ICON_NUM"

    .line 1363
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    const-string v3, "FOURDAY_URL"

    .line 1364
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const-string v3, "FOURDAY_DAY_RAIN_PROBABILITY"

    .line 1366
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    const-string v3, "FOURDAY_DAY_SNOW_PROBABILITY"

    .line 1367
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    const-string v3, "FOURDAY_DAY_HAIL_PROBABILITY"

    .line 1368
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    const-string v3, "FOURDAY_DAY_PRECIPITATION_PROBABILITY"

    .line 1369
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    const-string v3, "FOURDAY_SUNRISE_TIME"

    .line 1370
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    const-string v3, "FOURDAY_SUNSET_TIME"

    .line 1371
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v6 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;-><init>(IFFILjava/lang/String;IIIILjava/lang/String;Ljava/lang/String;)V

    .line 1359
    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addForecastInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;)V

    .line 1373
    new-instance v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    const-string v3, "FIVEDAY_LOW_TEMP"

    .line 1375
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v8

    const-string v3, "FIVEDAY_HIGH_TEMP"

    .line 1376
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v9

    const-string v3, "FIVEDAY_ICON_NUM"

    .line 1377
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    const-string v3, "FIVEDAY_URL"

    .line 1378
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const-string v3, "FIVEDAY_DAY_RAIN_PROBABILITY"

    .line 1380
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    const-string v3, "FIVEDAY_DAY_SNOW_PROBABILITY"

    .line 1381
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    const-string v3, "FIVEDAY_DAY_HAIL_PROBABILITY"

    .line 1382
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    const-string v3, "FIVEDAY_DAY_PRECIPITATION_PROBABILITY"

    .line 1383
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    const-string v3, "FIVEDAY_SUNRISE_TIME"

    .line 1384
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    const-string v3, "FIVEDAY_SUNSET_TIME"

    .line 1385
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v6 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;-><init>(IFFILjava/lang/String;IIIILjava/lang/String;Ljava/lang/String;)V

    .line 1373
    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addForecastInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;)V

    .line 1387
    new-instance v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    const-string v3, "SIXDAY_LOW_TEMP"

    .line 1389
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v8

    const-string v3, "SIXDAY_HIGH_TEMP"

    .line 1390
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v9

    const-string v3, "SIXDAY_ICON_NUM"

    .line 1391
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    const-string v3, "SIXDAY_URL"

    .line 1392
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const-string v3, "SIXDAY_DAY_RAIN_PROBABILITY"

    .line 1394
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    const-string v3, "SIXDAY_DAY_SNOW_PROBABILITY"

    .line 1395
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    const-string v3, "SIXDAY_DAY_HAIL_PROBABILITY"

    .line 1396
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    const-string v3, "SIXDAY_DAY_PRECIPITATION_PROBABILITY"

    .line 1397
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    const-string v3, "SIXDAY_SUNRISE_TIME"

    .line 1398
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    const-string v3, "SIXDAY_SUNSET_TIME"

    .line 1399
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v6 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;-><init>(IFFILjava/lang/String;IIIILjava/lang/String;Ljava/lang/String;)V

    .line 1387
    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addForecastInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;)V

    .line 1401
    .end local v7    # "tempScale":I
    .end local v20    # "onedayinfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .end local v22    # "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    :cond_4
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 1404
    .end local v18    # "cursor":Landroid/database/Cursor;
    :cond_5
    const-string v3, ""

    const-string v4, "SETTING_URI==null"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public static getDetailWeatherInfoForChangeOrder(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .locals 22
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    .line 1411
    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    invoke-direct/range {v19 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;-><init>()V

    .line 1412
    .local v19, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    if-nez p1, :cond_1

    .line 1413
    const-string v3, ""

    const-string v4, "city id is null!!!!!!"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1585
    :cond_0
    :goto_0
    return-object v19

    .line 1416
    :cond_1
    if-nez p0, :cond_2

    .line 1417
    const-string v3, ""

    const-string v4, "gDWI: context is null"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1421
    :cond_2
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 1423
    .local v2, "cp":Landroid/content/ContentResolver;
    if-eqz v2, :cond_0

    .line 1424
    const-string v3, "Location=\"%s\""

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 1425
    .local v5, "loc":Ljava/lang/String;
    sget-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1426
    sget-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    sget-object v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/OpenHelper;->sWeatherInfoProjection:[Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 1428
    .local v18, "cursor":Landroid/database/Cursor;
    if-eqz v18, :cond_0

    .line 1429
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1430
    const-string v3, "TEMP_SCALE"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 1431
    .local v7, "tempScale":I
    const-string v3, "NAME"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->setCityName(Ljava/lang/String;)V

    .line 1432
    const-string v3, "STATE"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->setState(Ljava/lang/String;)V

    .line 1433
    const-string v3, "LOCATION"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->setLocation(Ljava/lang/String;)V

    .line 1436
    new-instance v21, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    invoke-direct/range {v21 .. v21}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;-><init>()V

    .line 1437
    .local v21, "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setTempScale(I)V

    .line 1438
    const-string v3, "TODAY_DATE"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDate(Ljava/lang/String;)V

    .line 1439
    const-string v3, "TODAY_TEMP"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setCurrentTemp(F)V

    .line 1440
    const-string v3, "TODAY_HIGH_TEMP"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setHighTemp(F)V

    .line 1441
    const-string v3, "TODAY_LOW_TEMP"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setLowTemp(F)V

    .line 1442
    const-string v3, "TODAY_ICON_NUM"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setIconNum(I)V

    .line 1443
    const-string v3, "TODAY_WEATHER_TEXT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setWeatherText(Ljava/lang/String;)V

    .line 1444
    const-string v3, "TODAY_WEATHER_URL"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setUrl(Ljava/lang/String;)V

    .line 1445
    const-string v3, "TODAY_REALFELL"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setRealFeel(F)V

    .line 1446
    const-string v3, "UPDATE_DATE"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setUpdateDate(Ljava/lang/String;)V

    .line 1447
    const-string v3, "TIMEZONE"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setTimeZone(Ljava/lang/String;)V

    .line 1448
    const-string v3, "TODAY_SUNRISE_TIME"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setSunRiseTime(Ljava/lang/String;)V

    .line 1449
    const-string v3, "TODAY_SUNSET_TIME"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setSunSetTime(Ljava/lang/String;)V

    .line 1452
    const-string v3, "TODAY_DAY_RAIN_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDayRainProbability(I)V

    .line 1453
    const-string v3, "TODAY_DAY_SNOW_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDaySnowProbability(I)V

    .line 1454
    const-string v3, "TODAY_DAY_HAIL_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDayHailProbability(I)V

    .line 1455
    const-string v3, "TODAY_DAY_PRECIPITATION_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDayPrecipitationProbability(I)V

    .line 1456
    const-string v3, "TODAY_DAY_RAIN_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDayRainAmount(D)V

    .line 1457
    const-string v3, "TODAY_DAY_SNOW_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDaySnowAmount(D)V

    .line 1458
    const-string v3, "TODAY_DAY_HAIL_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDayHailAmount(D)V

    .line 1459
    const-string v3, "TODAY_DAY_PRECIPITATION_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDayPrecipitationAmount(D)V

    .line 1461
    const-string v3, "TODAY_NIGHT_RAIN_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightRainProbability(I)V

    .line 1462
    const-string v3, "TODAY_NIGHT_SNOW_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightSnowProbability(I)V

    .line 1463
    const-string v3, "TODAY_NIGHT_HAIL_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightHailProbability(I)V

    .line 1464
    const-string v3, "TODAY_NIGHT_PRECIPITATION_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightPrecipitationProbability(I)V

    .line 1465
    const-string v3, "TODAY_NIGHT_RAIN_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightRainAmount(D)V

    .line 1466
    const-string v3, "TODAY_NIGHT_SNOW_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightSnowAmount(D)V

    .line 1467
    const-string v3, "TODAY_NIGHT_HAIL_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightHailAmount(D)V

    .line 1468
    const-string v3, "TODAY_NIGHT_PRECIPITATION_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightPrecipitationAmount(D)V

    .line 1470
    const-string v3, "TODAY_HUM"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setRelativeHumidity(Ljava/lang/String;)V

    .line 1471
    const-string v3, "TODAY_UV_INDEX"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setUVIndex(I)V

    .line 1472
    const-string v3, "TODAY_UV_INDEX_TEXT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setUVIndexText(Ljava/lang/String;)V

    .line 1473
    const-string v3, "TODAY_DAY_ICON"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDayIcon(I)V

    .line 1474
    const-string v3, "TODAY_NIGHT_ICON"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightIcon(I)V

    .line 1476
    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->setTodayWeatherInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;)V

    .line 1478
    new-instance v20, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    invoke-direct/range {v20 .. v20}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;-><init>()V

    .line 1479
    .local v20, "onedayinfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    const-string v3, "ONEDAY_HIGH_TEMP"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setHighTemp(F)V

    .line 1480
    const-string v3, "ONEDAY_LOW_TEMP"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setLowTemp(F)V

    .line 1481
    const-string v3, "ONEDAY_ICON_NUM"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setIconNum(I)V

    .line 1482
    const-string v3, "ONEDAY_URL"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setUrl(Ljava/lang/String;)V

    .line 1483
    const-string v3, "ONEDAY_DAY_ICON"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setOnedayDayIcon(I)V

    .line 1484
    const-string v3, "ONEDAY_NIGHT_ICON"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setOnedayNightIcon(I)V

    .line 1486
    const-string v3, "ONEDAY_DAY_RAIN_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDayRainProbability(I)V

    .line 1487
    const-string v3, "ONEDAY_DAY_SNOW_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDaySnowProbability(I)V

    .line 1488
    const-string v3, "ONEDAY_DAY_HAIL_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDayHailProbability(I)V

    .line 1489
    const-string v3, "ONEDAY_DAY_PRECIPITATION_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDayPrecipitationProbability(I)V

    .line 1490
    const-string v3, "ONEDAY_DAY_RAIN_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDayRainAmount(D)V

    .line 1491
    const-string v3, "ONEDAY_DAY_SNOW_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDaySnowAmount(D)V

    .line 1492
    const-string v3, "ONEDAY_DAY_HAIL_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDayHailAmount(D)V

    .line 1493
    const-string v3, "ONEDAY_DAY_PRECIPITATION_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDayPrecipitationAmount(D)V

    .line 1495
    const-string v3, "ONEDAY_NIGHT_RAIN_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightRainProbability(I)V

    .line 1496
    const-string v3, "ONEDAY_NIGHT_SNOW_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightSnowProbability(I)V

    .line 1497
    const-string v3, "ONEDAY_NIGHT_HAIL_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightHailProbability(I)V

    .line 1498
    const-string v3, "ONEDAY_NIGHT_PRECIPITATION_PROBABILITY"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightPrecipitationProbability(I)V

    .line 1499
    const-string v3, "ONEDAY_NIGHT_RAIN_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightRainAmount(D)V

    .line 1500
    const-string v3, "ONEDAY_NIGHT_SNOW_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightSnowAmount(D)V

    .line 1501
    const-string v3, "ONEDAY_NIGHT_HAIL_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightHailAmount(D)V

    .line 1502
    const-string v3, "ONEDAY_NIGHT_PRECIPITATION_AMOUNT"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightPrecipitationAmount(D)V

    .line 1504
    const-string v3, "ONEDAY_SUNRISE_TIME"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setSunRiseTime(Ljava/lang/String;)V

    .line 1505
    const-string v3, "ONEDAY_SUNSET_TIME"

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setSunSetTime(Ljava/lang/String;)V

    .line 1507
    invoke-virtual/range {v19 .. v20}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addForecastInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;)V

    .line 1509
    new-instance v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    const-string v3, "TWODAY_LOW_TEMP"

    .line 1511
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v8

    const-string v3, "TWODAY_HIGH_TEMP"

    .line 1512
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v9

    const-string v3, "TWODAY_ICON_NUM"

    .line 1513
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    const-string v3, "TWODAY_URL"

    .line 1514
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const-string v3, "TWODAY_DAY_RAIN_PROBABILITY"

    .line 1516
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    const-string v3, "TWODAY_DAY_SNOW_PROBABILITY"

    .line 1517
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    const-string v3, "TWODAY_DAY_HAIL_PROBABILITY"

    .line 1518
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    const-string v3, "TWODAY_DAY_PRECIPITATION_PROBABILITY"

    .line 1519
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    const-string v3, "TWODAY_SUNRISE_TIME"

    .line 1520
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    const-string v3, "TWODAY_SUNSET_TIME"

    .line 1521
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v6 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;-><init>(IFFILjava/lang/String;IIIILjava/lang/String;Ljava/lang/String;)V

    .line 1509
    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addForecastInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;)V

    .line 1523
    new-instance v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    const-string v3, "THREEDAY_LOW_TEMP"

    .line 1525
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v8

    const-string v3, "THREEDAY_HIGH_TEMP"

    .line 1526
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v9

    const-string v3, "THREEDAY_ICON_NUM"

    .line 1527
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    const-string v3, "THREEDAY_URL"

    .line 1528
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const-string v3, "THREEDAY_DAY_RAIN_PROBABILITY"

    .line 1530
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    const-string v3, "THREEDAY_DAY_SNOW_PROBABILITY"

    .line 1531
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    const-string v3, "THREEDAY_DAY_HAIL_PROBABILITY"

    .line 1532
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    const-string v3, "THREEDAY_DAY_PRECIPITATION_PROBABILITY"

    .line 1533
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    const-string v3, "THREEDAY_SUNRISE_TIME"

    .line 1534
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    const-string v3, "THREEDAY_SUNSET_TIME"

    .line 1535
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v6 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;-><init>(IFFILjava/lang/String;IIIILjava/lang/String;Ljava/lang/String;)V

    .line 1523
    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addForecastInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;)V

    .line 1537
    new-instance v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    const-string v3, "FOURDAY_LOW_TEMP"

    .line 1539
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v8

    const-string v3, "FOURDAY_HIGH_TEMP"

    .line 1540
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v9

    const-string v3, "FOURDAY_ICON_NUM"

    .line 1541
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    const-string v3, "FOURDAY_URL"

    .line 1542
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const-string v3, "FOURDAY_DAY_RAIN_PROBABILITY"

    .line 1544
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    const-string v3, "FOURDAY_DAY_SNOW_PROBABILITY"

    .line 1545
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    const-string v3, "FOURDAY_DAY_HAIL_PROBABILITY"

    .line 1546
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    const-string v3, "FOURDAY_DAY_PRECIPITATION_PROBABILITY"

    .line 1547
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    const-string v3, "FOURDAY_SUNRISE_TIME"

    .line 1548
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    const-string v3, "FOURDAY_SUNSET_TIME"

    .line 1549
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v6 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;-><init>(IFFILjava/lang/String;IIIILjava/lang/String;Ljava/lang/String;)V

    .line 1537
    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addForecastInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;)V

    .line 1551
    new-instance v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    const-string v3, "FIVEDAY_LOW_TEMP"

    .line 1553
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v8

    const-string v3, "FIVEDAY_HIGH_TEMP"

    .line 1554
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v9

    const-string v3, "FIVEDAY_ICON_NUM"

    .line 1555
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    const-string v3, "FIVEDAY_URL"

    .line 1556
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const-string v3, "FIVEDAY_DAY_RAIN_PROBABILITY"

    .line 1558
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    const-string v3, "FIVEDAY_DAY_SNOW_PROBABILITY"

    .line 1559
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    const-string v3, "FIVEDAY_DAY_HAIL_PROBABILITY"

    .line 1560
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    const-string v3, "FIVEDAY_DAY_PRECIPITATION_PROBABILITY"

    .line 1561
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    const-string v3, "FIVEDAY_SUNRISE_TIME"

    .line 1562
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    const-string v3, "FIVEDAY_SUNSET_TIME"

    .line 1563
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v6 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;-><init>(IFFILjava/lang/String;IIIILjava/lang/String;Ljava/lang/String;)V

    .line 1551
    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addForecastInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;)V

    .line 1565
    new-instance v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    const-string v3, "SIXDAY_LOW_TEMP"

    .line 1567
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v8

    const-string v3, "SIXDAY_HIGH_TEMP"

    .line 1568
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v9

    const-string v3, "SIXDAY_ICON_NUM"

    .line 1569
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    const-string v3, "SIXDAY_URL"

    .line 1570
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const-string v3, "SIXDAY_DAY_RAIN_PROBABILITY"

    .line 1572
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    const-string v3, "SIXDAY_DAY_SNOW_PROBABILITY"

    .line 1573
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    const-string v3, "SIXDAY_DAY_HAIL_PROBABILITY"

    .line 1574
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    const-string v3, "SIXDAY_DAY_PRECIPITATION_PROBABILITY"

    .line 1575
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    const-string v3, "SIXDAY_SUNRISE_TIME"

    .line 1576
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    const-string v3, "SIXDAY_SUNSET_TIME"

    .line 1577
    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v6 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;-><init>(IFFILjava/lang/String;IIIILjava/lang/String;Ljava/lang/String;)V

    .line 1565
    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addForecastInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;)V

    .line 1579
    .end local v7    # "tempScale":I
    .end local v20    # "onedayinfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .end local v21    # "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    :cond_3
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 1582
    .end local v18    # "cursor":Landroid/database/Cursor;
    :cond_4
    const-string v3, ""

    const-string v4, "SETTING_URI==null"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public static getExistPhotos(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 10
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "cityId"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v9, 0x0

    .line 3837
    const/4 v7, 0x0

    .line 3838
    .local v7, "result":Z
    if-nez p0, :cond_0

    move v8, v7

    .line 3854
    .end local v7    # "result":Z
    .local v8, "result":I
    :goto_0
    return v8

    .line 3841
    .end local v8    # "result":I
    .restart local v7    # "result":Z
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 3842
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_2

    .line 3843
    const-string v1, "LOCATION=\"%s\""

    new-array v2, v5, [Ljava/lang/Object;

    aput-object p1, v2, v9

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 3844
    .local v3, "selection":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_PHOTOSINFO_URI:Landroid/net/Uri;

    new-array v2, v5, [Ljava/lang/String;

    const-string v5, "LOCATION"

    aput-object v5, v2, v9

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 3846
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    .line 3847
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3848
    const/4 v7, 0x1

    .line 3850
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .end local v3    # "selection":Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_2
    move v8, v7

    .line 3854
    .restart local v8    # "result":I
    goto :goto_0
.end method

.method public static getFaildLog(Landroid/content/Context;)V
    .locals 13
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v12, 0x1

    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 157
    const-wide/16 v9, 0x0

    .line 158
    .local v9, "time":J
    if-nez p0, :cond_1

    .line 159
    const-string v1, ""

    const-string v2, "gAS: context is null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    :cond_0
    :goto_0
    return-void

    .line 162
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 163
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_0

    .line 164
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_WEATHER_LOG_URI:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 165
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_WEATHER_LOG_URI:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "NOW_TIME"

    aput-object v4, v2, v5

    const-string v4, "FAIL_LOG"

    aput-object v4, v2, v12

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 167
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    .line 169
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 171
    :cond_2
    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 173
    invoke-static {v9, v10, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getDateTimeString(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    .line 174
    .local v11, "timeString2":Ljava/lang/String;
    const/4 v1, 0x1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 176
    .local v8, "errorLog":Ljava/lang/String;
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", eLog : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_2

    .line 181
    .end local v8    # "errorLog":Ljava/lang/String;
    .end local v11    # "timeString2":Ljava/lang/String;
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 182
    :catch_0
    move-exception v7

    .line 183
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 179
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_3
    :try_start_1
    const-string v1, ""

    const-string v2, "endofFailLog  "

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 187
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_4
    const-string v1, ""

    const-string v2, "SETTING_URI==null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getLastSelectedLocation(Landroid/content/Context;)Ljava/lang/String;
    .locals 10
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x0

    const/4 v3, 0x0

    .line 42
    const-string v7, ""

    .line 43
    .local v7, "result":Ljava/lang/String;
    if-nez p0, :cond_0

    .line 44
    const-string v1, ""

    const-string v2, "gLSL: context is null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v8, v7

    .line 62
    .end local v7    # "result":Ljava/lang/String;
    .local v8, "result":Ljava/lang/String;
    :goto_0
    return-object v8

    .line 47
    .end local v8    # "result":Ljava/lang/String;
    .restart local v7    # "result":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 48
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_2

    .line 49
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 50
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "LAST_SEL_LOCATION"

    aput-object v4, v2, v9

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 52
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    .line 53
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 54
    invoke-interface {v6, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 56
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_2
    :goto_1
    move-object v8, v7

    .line 62
    .end local v7    # "result":Ljava/lang/String;
    .restart local v8    # "result":Ljava/lang/String;
    goto :goto_0

    .line 59
    .end local v8    # "result":Ljava/lang/String;
    .restart local v7    # "result":Ljava/lang/String;
    :cond_3
    const-string v1, ""

    const-string v2, "SETTING_URI==null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static getLocation(Landroid/content/Context;)Ljava/lang/String;
    .locals 12
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 3272
    const/4 v6, 0x0

    .line 3273
    .local v6, "curLoc":Ljava/lang/String;
    if-nez p0, :cond_0

    .line 3274
    const-string v1, ""

    const-string v2, "gRL: context is null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v7, v6

    .line 3300
    .end local v6    # "curLoc":Ljava/lang/String;
    .local v7, "curLoc":Ljava/lang/String;
    :goto_0
    return-object v7

    .line 3278
    .end local v7    # "curLoc":Ljava/lang/String;
    .restart local v6    # "curLoc":Ljava/lang/String;
    :cond_0
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSettingsForMain(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-result-object v9

    .line 3279
    .local v9, "setting":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    if-eqz v9, :cond_1

    invoke-virtual {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getCurrentLoccation()I

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    move-object v7, v6

    .line 3280
    .end local v6    # "curLoc":Ljava/lang/String;
    .restart local v7    # "curLoc":Ljava/lang/String;
    goto :goto_0

    .line 3283
    .end local v7    # "curLoc":Ljava/lang/String;
    .restart local v6    # "curLoc":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 3284
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_4

    .line 3285
    const-string v1, "LOCATION=\"%s\""

    new-array v2, v11, [Ljava/lang/Object;

    const-string v5, "cityId:current"

    aput-object v5, v2, v10

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 3286
    .local v3, "loc":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 3287
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    new-array v2, v11, [Ljava/lang/String;

    const-string v5, "LOCATION"

    aput-object v5, v2, v10

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 3289
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_4

    .line 3290
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 3291
    invoke-interface {v8, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 3293
    :cond_3
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .end local v3    # "loc":Ljava/lang/String;
    .end local v8    # "cursor":Landroid/database/Cursor;
    :cond_4
    :goto_1
    move-object v7, v6

    .line 3300
    .end local v6    # "curLoc":Ljava/lang/String;
    .restart local v7    # "curLoc":Ljava/lang/String;
    goto :goto_0

    .line 3296
    .end local v7    # "curLoc":Ljava/lang/String;
    .restart local v3    # "loc":Ljava/lang/String;
    .restart local v6    # "curLoc":Ljava/lang/String;
    :cond_5
    const-string v1, ""

    const-string v2, "SETTING_URI==null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static getMapLocation(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    .locals 12
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 3703
    const/4 v7, 0x0

    .line 3704
    .local v7, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    const/4 v8, 0x0

    .line 3705
    .local v8, "result":I
    if-nez p0, :cond_0

    .line 3706
    const-string v1, ""

    const-string v2, "gDDFM: context is null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3707
    const/4 v8, 0x0

    .line 3752
    :goto_0
    return-object v7

    .line 3709
    :cond_0
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSettingsForMain(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-result-object v9

    .line 3710
    .local v9, "setting":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    const-string v1, "cityId:current"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz v9, :cond_1

    .line 3711
    invoke-virtual {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getCurrentLoccation()I

    move-result v1

    if-nez v1, :cond_2

    .line 3712
    :cond_1
    const/4 v8, 0x1

    goto :goto_0

    .line 3714
    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 3716
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_6

    .line 3717
    const-string v1, "Location=\"%s\""

    new-array v2, v11, [Ljava/lang/Object;

    aput-object p1, v2, v10

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 3718
    .local v3, "loc":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 3719
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/String;

    const-string v5, "NAME"

    aput-object v5, v2, v10

    const-string v5, "STATE"

    aput-object v5, v2, v11

    const/4 v5, 0x2

    const-string v10, "LOCATION"

    aput-object v10, v2, v5

    const/4 v5, 0x3

    const-string v10, "LATITUDE"

    aput-object v10, v2, v5

    const/4 v5, 0x4

    const-string v10, "LONGITUDE"

    aput-object v10, v2, v5

    const/4 v5, 0x5

    const-string v10, "TODAY_TEMP"

    aput-object v10, v2, v5

    const/4 v5, 0x6

    const-string v10, "TODAY_ICON_NUM"

    aput-object v10, v2, v5

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 3729
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_4

    .line 3730
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 3731
    new-instance v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    .end local v7    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    invoke-direct {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;-><init>()V

    .line 3732
    .restart local v7    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    const-string v1, "NAME"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->setCity(Ljava/lang/String;)V

    .line 3733
    const-string v1, "STATE"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->setState(Ljava/lang/String;)V

    .line 3734
    const-string v1, "LOCATION"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->setLocation(Ljava/lang/String;)V

    .line 3735
    const-string v1, "LATITUDE"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->setLatitude(Ljava/lang/String;)V

    .line 3736
    const-string v1, "LONGITUDE"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->setLongitude(Ljava/lang/String;)V

    .line 3737
    const-string v1, "TODAY_TEMP"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->setTemp(Ljava/lang/String;)V

    .line 3738
    const-string v1, "TODAY_ICON_NUM"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->setIcon(Ljava/lang/String;)V

    .line 3740
    :cond_3
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 3742
    :cond_4
    const/4 v8, 0x1

    .line 3743
    goto/16 :goto_0

    .line 3744
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_5
    const-string v1, ""

    const-string v2, "SETTING_URI==null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3745
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 3748
    .end local v3    # "loc":Ljava/lang/String;
    :cond_6
    const/4 v8, 0x0

    goto/16 :goto_0
.end method

.method public static getNextRefreshTime(Landroid/content/Context;)J
    .locals 12
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v11, 0x0

    const/4 v3, 0x0

    .line 2358
    const-wide/16 v7, 0x0

    .line 2359
    .local v7, "time":J
    if-nez p0, :cond_0

    .line 2360
    const-string v1, ""

    const-string v2, "gNRT: context is null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v9, v7

    .line 2378
    .end local v7    # "time":J
    .local v9, "time":J
    :goto_0
    return-wide v9

    .line 2363
    .end local v9    # "time":J
    .restart local v7    # "time":J
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2364
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_2

    .line 2365
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2366
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "AUTO_REF_NEXT_TIME"

    aput-object v4, v2, v11

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2368
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    .line 2369
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2370
    invoke-interface {v6, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 2372
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_2
    :goto_1
    move-wide v9, v7

    .line 2378
    .end local v7    # "time":J
    .restart local v9    # "time":J
    goto :goto_0

    .line 2375
    .end local v9    # "time":J
    .restart local v7    # "time":J
    :cond_3
    const-string v1, ""

    const-string v2, "SETTING_URI==null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static getNotificationSettings(Landroid/content/Context;)I
    .locals 10
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x0

    const/4 v3, 0x0

    .line 497
    const-string v1, ""

    const-string v2, "g N "

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 498
    const/4 v7, 0x0

    .line 499
    .local v7, "timestamp":I
    if-nez p0, :cond_0

    .line 500
    const-string v1, ""

    const-string v2, "gUT: context is null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v8, v7

    .line 518
    .end local v7    # "timestamp":I
    .local v8, "timestamp":I
    :goto_0
    return v8

    .line 503
    .end local v8    # "timestamp":I
    .restart local v7    # "timestamp":I
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 504
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_2

    .line 505
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 506
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "NOTIFICATION"

    aput-object v4, v2, v9

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 508
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    .line 509
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 510
    invoke-interface {v6, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 512
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_2
    :goto_1
    move v8, v7

    .line 518
    .end local v7    # "timestamp":I
    .restart local v8    # "timestamp":I
    goto :goto_0

    .line 515
    .end local v8    # "timestamp":I
    .restart local v7    # "timestamp":I
    :cond_3
    const-string v1, ""

    const-string v2, "SETTING_URI==null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static getNotificationTimeSettings(Landroid/content/Context;)Ljava/lang/Long;
    .locals 10
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x0

    const/4 v3, 0x0

    .line 522
    const-string v1, ""

    const-string v2, "g NT "

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 523
    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 524
    .local v7, "timestamp":Ljava/lang/Long;
    if-nez p0, :cond_0

    .line 525
    const-string v1, ""

    const-string v2, "gUT: context is null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v8, v7

    .line 543
    .end local v7    # "timestamp":Ljava/lang/Long;
    .local v8, "timestamp":Ljava/lang/Long;
    :goto_0
    return-object v8

    .line 528
    .end local v8    # "timestamp":Ljava/lang/Long;
    .restart local v7    # "timestamp":Ljava/lang/Long;
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 529
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_2

    .line 530
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 531
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "NOTIFICATION_SET_TIME"

    aput-object v4, v2, v9

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 533
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    .line 534
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 535
    invoke-interface {v6, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 537
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_2
    :goto_1
    move-object v8, v7

    .line 543
    .end local v7    # "timestamp":Ljava/lang/Long;
    .restart local v8    # "timestamp":Ljava/lang/Long;
    goto :goto_0

    .line 540
    .end local v8    # "timestamp":Ljava/lang/Long;
    .restart local v7    # "timestamp":Ljava/lang/Long;
    :cond_3
    const-string v1, ""

    const-string v2, "SETTING_URI==null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static getRealLocation(Landroid/content/Context;)Ljava/lang/String;
    .locals 11
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 3247
    const/4 v7, 0x0

    .line 3248
    .local v7, "realLoc":Ljava/lang/String;
    if-nez p0, :cond_0

    .line 3249
    const-string v1, ""

    const-string v2, "gRL: context is null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v8, v7

    .line 3268
    .end local v7    # "realLoc":Ljava/lang/String;
    .local v8, "realLoc":Ljava/lang/String;
    :goto_0
    return-object v8

    .line 3252
    .end local v8    # "realLoc":Ljava/lang/String;
    .restart local v7    # "realLoc":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 3253
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_2

    .line 3254
    const-string v1, "LOCATION=\"%s\""

    new-array v2, v10, [Ljava/lang/Object;

    const-string v5, "cityId:current"

    aput-object v5, v2, v9

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 3255
    .local v3, "loc":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 3256
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    new-array v2, v10, [Ljava/lang/String;

    const-string v5, "REAL_LOCATION"

    aput-object v5, v2, v9

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 3258
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    .line 3259
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3260
    invoke-interface {v6, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 3262
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .end local v3    # "loc":Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_2
    :goto_1
    move-object v8, v7

    .line 3268
    .end local v7    # "realLoc":Ljava/lang/String;
    .restart local v8    # "realLoc":Ljava/lang/String;
    goto :goto_0

    .line 3265
    .end local v8    # "realLoc":Ljava/lang/String;
    .restart local v3    # "loc":Ljava/lang/String;
    .restart local v7    # "realLoc":Ljava/lang/String;
    :cond_3
    const-string v1, ""

    const-string v2, "SETTING_URI==null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static getRefreshEnteringSettings(Landroid/content/Context;)I
    .locals 10
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 218
    const/4 v8, 0x0

    .line 219
    .local v8, "result":I
    if-nez p0, :cond_0

    .line 220
    const-string v1, ""

    const-string v2, "gCBS: context is null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v9, v8

    .line 242
    .end local v8    # "result":I
    .local v9, "result":I
    :goto_0
    return v9

    .line 223
    .end local v9    # "result":I
    .restart local v8    # "result":I
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 224
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_2

    .line 225
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 226
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "REFRESH_ENTERING"

    aput-object v4, v2, v5

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 228
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    .line 229
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 231
    const/4 v1, 0x0

    :try_start_0
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    .line 236
    :cond_1
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_2
    :goto_2
    move v9, v8

    .line 242
    .end local v8    # "result":I
    .restart local v9    # "result":I
    goto :goto_0

    .line 232
    .end local v9    # "result":I
    .restart local v6    # "cursor":Landroid/database/Cursor;
    .restart local v8    # "result":I
    :catch_0
    move-exception v7

    .line 233
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 239
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_3
    const-string v1, ""

    const-string v2, "SETTING_URI==null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public static getRefreshRoamingSettings(Landroid/content/Context;)I
    .locals 10
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 246
    const/4 v8, 0x0

    .line 247
    .local v8, "result":I
    if-nez p0, :cond_0

    .line 248
    const-string v1, ""

    const-string v2, "gCBS: context is null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v9, v8

    .line 270
    .end local v8    # "result":I
    .local v9, "result":I
    :goto_0
    return v9

    .line 251
    .end local v9    # "result":I
    .restart local v8    # "result":I
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 252
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_2

    .line 253
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 254
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "REFRESH_ROAMING"

    aput-object v4, v2, v5

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 256
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    .line 257
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 259
    const/4 v1, 0x0

    :try_start_0
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    .line 264
    :cond_1
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_2
    :goto_2
    move v9, v8

    .line 270
    .end local v8    # "result":I
    .restart local v9    # "result":I
    goto :goto_0

    .line 260
    .end local v9    # "result":I
    .restart local v6    # "cursor":Landroid/database/Cursor;
    .restart local v8    # "result":I
    :catch_0
    move-exception v7

    .line 261
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 267
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_3
    const-string v1, ""

    const-string v2, "SETTING_URI==null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public static getRegisteredCityCount(Landroid/content/Context;)I
    .locals 11
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 629
    const/4 v6, 0x0

    .line 630
    .local v6, "count":I
    if-nez p0, :cond_0

    .line 631
    const-string v1, ""

    const-string v2, "gRCC: context is null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 632
    const/4 v1, -0x1

    .line 670
    :goto_0
    return v1

    .line 634
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 635
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_7

    .line 636
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 637
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "LOCATION"

    aput-object v5, v2, v4

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 639
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_4

    .line 640
    const/4 v8, 0x0

    .line 641
    .local v8, "hasCurrentLocation":Z
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 642
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v6

    .line 645
    :cond_1
    const-string v1, "LOCATION"

    .line 646
    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 645
    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 647
    .local v9, "location":Ljava/lang/String;
    const-string v1, "cityId:current"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 648
    const/4 v8, 0x1

    .line 653
    .end local v9    # "location":Ljava/lang/String;
    :cond_2
    :goto_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 655
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSettingsForMain(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-result-object v10

    .line 656
    .local v10, "setting":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    if-eqz v10, :cond_3

    .line 657
    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getCurrentLoccation()I

    move-result v1

    if-nez v1, :cond_4

    if-eqz v8, :cond_4

    .line 659
    :cond_3
    add-int/lit8 v6, v6, -0x1

    .end local v7    # "cursor":Landroid/database/Cursor;
    .end local v8    # "hasCurrentLocation":Z
    .end local v10    # "setting":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    :cond_4
    :goto_2
    move v1, v6

    .line 670
    goto :goto_0

    .line 651
    .restart local v7    # "cursor":Landroid/database/Cursor;
    .restart local v8    # "hasCurrentLocation":Z
    .restart local v9    # "location":Ljava/lang/String;
    :cond_5
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_1

    goto :goto_1

    .line 663
    .end local v7    # "cursor":Landroid/database/Cursor;
    .end local v8    # "hasCurrentLocation":Z
    .end local v9    # "location":Ljava/lang/String;
    :cond_6
    const-string v1, ""

    const-string v2, "SETTING_URI==null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 664
    const/4 v6, -0x1

    goto :goto_2

    .line 667
    :cond_7
    const/4 v6, -0x1

    goto :goto_2
.end method

.method public static getSelectedDefaultLocation(Landroid/content/Context;)Ljava/lang/String;
    .locals 13
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v12, 0x1

    const/4 v3, 0x0

    const/4 v11, 0x0

    .line 96
    const-string v9, ""

    .line 97
    .local v9, "result":Ljava/lang/String;
    if-nez p0, :cond_0

    .line 98
    const-string v1, ""

    const-string v2, "gLSL: context is null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v10, v9

    .line 141
    .end local v9    # "result":Ljava/lang/String;
    .local v10, "result":Ljava/lang/String;
    :goto_0
    return-object v10

    .line 101
    .end local v10    # "result":Ljava/lang/String;
    .restart local v9    # "result":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 102
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_2

    .line 103
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 104
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    new-array v2, v12, [Ljava/lang/String;

    const-string v4, "DEFAULT_LOCATION"

    aput-object v4, v2, v11

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 107
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    .line 108
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 109
    invoke-interface {v6, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 111
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 117
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_2
    :goto_1
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 118
    .local v8, "mInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;>;"
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getAllCityList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v8

    .line 119
    if-eqz v9, :cond_3

    const-string v1, ""

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v1, v12, :cond_6

    .line 120
    :cond_3
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_5

    .line 121
    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v9

    .line 122
    invoke-static {p0, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateSelectDefaultdLocation(Landroid/content/Context;Ljava/lang/String;)I

    :goto_2
    move-object v10, v9

    .line 141
    .end local v9    # "result":Ljava/lang/String;
    .restart local v10    # "result":Ljava/lang/String;
    goto :goto_0

    .line 114
    .end local v8    # "mInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;>;"
    .end local v10    # "result":Ljava/lang/String;
    .restart local v9    # "result":Ljava/lang/String;
    :cond_4
    const-string v1, ""

    const-string v2, "SETTING_URI==null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 124
    .restart local v8    # "mInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;>;"
    :cond_5
    const-string v9, ""

    .line 125
    invoke-static {p0, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateSelectDefaultdLocation(Landroid/content/Context;Ljava/lang/String;)I

    goto :goto_2

    .line 128
    :cond_6
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_3
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v7, v1, :cond_8

    .line 129
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    move-object v10, v9

    .line 130
    .end local v9    # "result":Ljava/lang/String;
    .restart local v10    # "result":Ljava/lang/String;
    goto :goto_0

    .line 128
    .end local v10    # "result":Ljava/lang/String;
    .restart local v9    # "result":Ljava/lang/String;
    :cond_7
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 133
    :cond_8
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_9

    .line 134
    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v9

    .line 135
    invoke-static {p0, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateSelectDefaultdLocation(Landroid/content/Context;Ljava/lang/String;)I

    goto :goto_2

    .line 137
    :cond_9
    const-string v9, ""

    .line 138
    invoke-static {p0, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateSelectDefaultdLocation(Landroid/content/Context;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public static getSettingsForMain(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    .locals 17
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 2225
    const/16 v16, 0x0

    .line 2226
    .local v16, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    if-nez p0, :cond_0

    .line 2227
    const-string v2, ""

    const-string v3, "gS: context is null"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v1, v16

    .line 2267
    .end local v16    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    :goto_0
    return-object v16

    .line 2230
    .restart local v16    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    :cond_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2231
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_2

    .line 2232
    sget-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    invoke-static {v0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2233
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    const/16 v2, 0xb

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "TEMP_SCALE"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "AUTO_REFRESH_TIME"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "AUTO_REF_NEXT_TIME"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "AUTO_SCROLL"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "REFRESH_ENTERING"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "REFRESH_ROAMING"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "CHECK_CURRENT_CITY_LOCATION"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "NOTIFICATION"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "NOTIFICATION_SET_TIME"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "LAST_SEL_LOCATION"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "DEFAULT_LOCATION"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 2246
    .local v15, "cursor":Landroid/database/Cursor;
    if-eqz v15, :cond_2

    .line 2247
    invoke-interface {v15}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2248
    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    const-string v2, "TEMP_SCALE"

    .line 2249
    invoke-interface {v15, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v15, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const-string v3, "AUTO_REFRESH_TIME"

    .line 2250
    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const-string v4, "AUTO_REF_NEXT_TIME"

    .line 2251
    invoke-interface {v15, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v15, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const-string v6, "AUTO_SCROLL"

    .line 2252
    invoke-interface {v15, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    const-string v7, "REFRESH_ENTERING"

    .line 2253
    invoke-interface {v15, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v15, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    const-string v8, "REFRESH_ROAMING"

    .line 2254
    invoke-interface {v15, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v15, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    const-string v9, "CHECK_CURRENT_CITY_LOCATION"

    .line 2255
    invoke-interface {v15, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v15, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    const-string v10, "NOTIFICATION"

    .line 2256
    invoke-interface {v15, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v15, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    const-string v11, "NOTIFICATION_SET_TIME"

    .line 2257
    invoke-interface {v15, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {v15, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    const-string v13, "LAST_SEL_LOCATION"

    .line 2258
    invoke-interface {v15, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    invoke-interface {v15, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    const-string v14, "DEFAULT_LOCATION"

    .line 2259
    invoke-interface {v15, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    invoke-interface {v15, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-direct/range {v1 .. v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;-><init>(IIJIIIIIJLjava/lang/String;Ljava/lang/String;)V

    .line 2261
    .end local v16    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    .local v1, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    :goto_1
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .end local v15    # "cursor":Landroid/database/Cursor;
    :goto_2
    move-object/from16 v16, v1

    .line 2267
    .end local v1    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    .restart local v16    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    goto/16 :goto_0

    .line 2264
    :cond_1
    const-string v2, ""

    const-string v3, "SETTING_URI==null"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    move-object/from16 v1, v16

    .end local v16    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    .restart local v1    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    goto :goto_2

    .end local v1    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    .restart local v15    # "cursor":Landroid/database/Cursor;
    .restart local v16    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    :cond_3
    move-object/from16 v1, v16

    .end local v16    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    .restart local v1    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;
    goto :goto_1
.end method

.method public static getShowUseLocationPopup(Landroid/content/Context;)I
    .locals 10
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 4072
    const/4 v8, 0x0

    .line 4073
    .local v8, "result":I
    if-nez p0, :cond_0

    .line 4074
    const-string v1, ""

    const-string v2, "gSULP: context is null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v9, v8

    .line 4098
    .end local v8    # "result":I
    .local v9, "result":I
    :goto_0
    return v9

    .line 4078
    .end local v9    # "result":I
    .restart local v8    # "result":I
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 4079
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_2

    .line 4081
    :try_start_0
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "SHOW_USE_LOCATION_POPUP"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 4083
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    .line 4084
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    if-eqz v1, :cond_1

    .line 4086
    const/4 v1, 0x0

    :try_start_1
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v8

    .line 4091
    :cond_1
    :goto_1
    :try_start_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_2
    move v9, v8

    .line 4098
    .end local v8    # "result":I
    .restart local v9    # "result":I
    goto :goto_0

    .line 4087
    .end local v9    # "result":I
    .restart local v6    # "cursor":Landroid/database/Cursor;
    .restart local v8    # "result":I
    :catch_0
    move-exception v7

    .line 4088
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 4093
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v7    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v7

    .line 4094
    .restart local v7    # "e":Ljava/lang/Exception;
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v9, v8

    .line 4095
    .end local v8    # "result":I
    .restart local v9    # "result":I
    goto :goto_0
.end method

.method public static getTempScaleSetting(Landroid/content/Context;)I
    .locals 10
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x0

    const/4 v3, 0x0

    .line 3041
    const/4 v7, 0x0

    .line 3042
    .local v7, "tempScale":I
    if-nez p0, :cond_0

    .line 3043
    const-string v1, ""

    const-string v2, "gTSS: context is null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v8, v7

    .line 3061
    .end local v7    # "tempScale":I
    .local v8, "tempScale":I
    :goto_0
    return v8

    .line 3046
    .end local v8    # "tempScale":I
    .restart local v7    # "tempScale":I
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 3047
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_2

    .line 3048
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 3049
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "TEMP_SCALE"

    aput-object v4, v2, v9

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 3051
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    .line 3052
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3053
    invoke-interface {v6, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 3055
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_2
    :goto_1
    move v8, v7

    .line 3061
    .end local v7    # "tempScale":I
    .restart local v8    # "tempScale":I
    goto :goto_0

    .line 3058
    .end local v8    # "tempScale":I
    .restart local v7    # "tempScale":I
    :cond_3
    const-string v1, ""

    const-string v2, "SETTING_URI==null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static getUpdateTimestamp(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v9, 0x0

    .line 562
    const-string v7, "0"

    .line 563
    .local v7, "timestamp":Ljava/lang/String;
    if-nez p0, :cond_0

    .line 564
    const-string v1, ""

    const-string v2, "gUT: context is null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v8, v7

    .line 583
    .end local v7    # "timestamp":Ljava/lang/String;
    .local v8, "timestamp":Ljava/lang/String;
    :goto_0
    return-object v8

    .line 567
    .end local v8    # "timestamp":Ljava/lang/String;
    .restart local v7    # "timestamp":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 568
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_2

    .line 569
    const-string v1, "Location=\"%s\""

    new-array v2, v5, [Ljava/lang/Object;

    aput-object p1, v2, v9

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 570
    .local v3, "loc":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 571
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    new-array v2, v5, [Ljava/lang/String;

    const-string v5, "UPDATE_DATE"

    aput-object v5, v2, v9

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 573
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    .line 574
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 575
    invoke-interface {v6, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 577
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .end local v3    # "loc":Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_2
    :goto_1
    move-object v8, v7

    .line 583
    .end local v7    # "timestamp":Ljava/lang/String;
    .restart local v8    # "timestamp":Ljava/lang/String;
    goto :goto_0

    .line 580
    .end local v8    # "timestamp":Ljava/lang/String;
    .restart local v3    # "loc":Ljava/lang/String;
    .restart local v7    # "timestamp":Ljava/lang/String;
    :cond_3
    const-string v1, ""

    const-string v2, "SETTING_URI==null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static getUri(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "day"    # I
    .param p2, "Location"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v10, 0x0

    .line 3756
    const-string v8, ""

    .line 3757
    .local v8, "uri":Ljava/lang/String;
    const-string v6, ""

    .line 3758
    .local v6, "col":Ljava/lang/String;
    if-nez p0, :cond_0

    .line 3759
    const-string v1, ""

    const-string v2, "gU: context is null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v9, v8

    .line 3804
    .end local v8    # "uri":Ljava/lang/String;
    .local v9, "uri":Ljava/lang/String;
    :goto_0
    return-object v9

    .line 3762
    .end local v9    # "uri":Ljava/lang/String;
    .restart local v8    # "uri":Ljava/lang/String;
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 3785
    :pswitch_0
    const-string v6, "TODAY_WEATHER_URL"

    .line 3787
    :goto_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 3788
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_2

    .line 3789
    const-string v1, "LOCATION=\"%s\""

    new-array v2, v5, [Ljava/lang/Object;

    aput-object p2, v2, v10

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 3790
    .local v3, "loc":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 3791
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    new-array v2, v5, [Ljava/lang/String;

    aput-object v6, v2, v10

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 3793
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_2

    .line 3794
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3795
    invoke-interface {v7, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 3797
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .end local v3    # "loc":Ljava/lang/String;
    .end local v7    # "cursor":Landroid/database/Cursor;
    :cond_2
    :goto_2
    move-object v9, v8

    .line 3804
    .end local v8    # "uri":Ljava/lang/String;
    .restart local v9    # "uri":Ljava/lang/String;
    goto :goto_0

    .line 3764
    .end local v0    # "cp":Landroid/content/ContentResolver;
    .end local v9    # "uri":Ljava/lang/String;
    .restart local v8    # "uri":Ljava/lang/String;
    :pswitch_1
    const-string v6, "TODAY_WEATHER_URL"

    .line 3765
    goto :goto_1

    .line 3767
    :pswitch_2
    const-string v6, "ONEDAY_URL"

    .line 3768
    goto :goto_1

    .line 3770
    :pswitch_3
    const-string v6, "TWODAY_URL"

    .line 3771
    goto :goto_1

    .line 3773
    :pswitch_4
    const-string v6, "THREEDAY_URL"

    .line 3774
    goto :goto_1

    .line 3776
    :pswitch_5
    const-string v6, "FOURDAY_URL"

    .line 3777
    goto :goto_1

    .line 3779
    :pswitch_6
    const-string v6, "FIVEDAY_URL"

    .line 3780
    goto :goto_1

    .line 3782
    :pswitch_7
    const-string v6, "SIXDAY_URL"

    .line 3783
    goto :goto_1

    .line 3800
    .restart local v0    # "cp":Landroid/content/ContentResolver;
    .restart local v3    # "loc":Ljava/lang/String;
    :cond_3
    const-string v1, ""

    const-string v2, "SETTING_URI==null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 3762
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static getViEffectSettings(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;
    .locals 23
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 2271
    const/16 v21, 0x0

    .line 2272
    .local v21, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;
    if-nez p0, :cond_0

    .line 2273
    const-string v5, ""

    const-string v6, "gS: context is null"

    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v3, v21

    .line 2330
    .end local v21    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;
    :goto_0
    return-object v21

    .line 2276
    .restart local v21    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;
    :cond_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 2277
    .local v2, "cp":Landroid/content/ContentResolver;
    if-eqz v2, :cond_3

    .line 2278
    sget-object v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->WEATHER_VIEFFECT_URI:Landroid/net/Uri;

    invoke-static {v2, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2279
    sget-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->WEATHER_VIEFFECT_URI:Landroid/net/Uri;

    const/16 v5, 0x10

    new-array v4, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "EFFECT_1"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "EFFECT_2"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "EFFECT_3"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "EFFECT_4"

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "EFFECT_5"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    const-string v6, "EFFECT_6"

    aput-object v6, v4, v5

    const/4 v5, 0x6

    const-string v6, "EFFECT_7"

    aput-object v6, v4, v5

    const/4 v5, 0x7

    const-string v6, "EFFECT_8"

    aput-object v6, v4, v5

    const/16 v5, 0x8

    const-string v6, "EFFECT_9"

    aput-object v6, v4, v5

    const/16 v5, 0x9

    const-string v6, "EFFECT_10"

    aput-object v6, v4, v5

    const/16 v5, 0xa

    const-string v6, "EFFECT_11"

    aput-object v6, v4, v5

    const/16 v5, 0xb

    const-string v6, "EFFECT_12"

    aput-object v6, v4, v5

    const/16 v5, 0xc

    const-string v6, "EFFECT_13"

    aput-object v6, v4, v5

    const/16 v5, 0xd

    const-string v6, "EFFECT_14"

    aput-object v6, v4, v5

    const/16 v5, 0xe

    const-string v6, "EFFECT_15"

    aput-object v6, v4, v5

    const/16 v5, 0xf

    const-string v6, "EFFECT_16"

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v20

    .line 2297
    .local v20, "cursor":Landroid/database/Cursor;
    if-eqz v20, :cond_3

    .line 2298
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 2299
    const-string v5, "EFFECT_1"

    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 2300
    .local v4, "landMpPlaySetting":I
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090014

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v22

    .line 2301
    .local v22, "isSupportVideoOnLand":Z
    if-nez v22, :cond_1

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->orientation:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_1

    .line 2302
    const/4 v4, 0x0

    .line 2304
    :cond_1
    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    const-string v5, "EFFECT_2"

    .line 2306
    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v20

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    const-string v6, "EFFECT_3"

    .line 2307
    move-object/from16 v0, v20

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v20

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    const-string v7, "EFFECT_4"

    .line 2308
    move-object/from16 v0, v20

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    move-object/from16 v0, v20

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    const-string v8, "EFFECT_5"

    .line 2309
    move-object/from16 v0, v20

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    move-object/from16 v0, v20

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    const-string v9, "EFFECT_6"

    .line 2310
    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    const-string v10, "EFFECT_7"

    .line 2311
    move-object/from16 v0, v20

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    move-object/from16 v0, v20

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    const-string v11, "EFFECT_8"

    .line 2312
    move-object/from16 v0, v20

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    move-object/from16 v0, v20

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    const-string v12, "EFFECT_9"

    .line 2313
    move-object/from16 v0, v20

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    move-object/from16 v0, v20

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    const-string v13, "EFFECT_10"

    .line 2314
    move-object/from16 v0, v20

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    move-object/from16 v0, v20

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    const-string v14, "EFFECT_11"

    .line 2315
    move-object/from16 v0, v20

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, v20

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    const-string v15, "EFFECT_12"

    .line 2316
    move-object/from16 v0, v20

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    move-object/from16 v0, v20

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    const-string v16, "EFFECT_13"

    .line 2317
    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    move-object/from16 v0, v20

    move/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    const-string v17, "EFFECT_14"

    .line 2318
    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    move-object/from16 v0, v20

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    const-string v18, "EFFECT_15"

    .line 2319
    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    move-object/from16 v0, v20

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    const-string v19, "EFFECT_16"

    .line 2320
    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    move-object/from16 v0, v20

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    invoke-direct/range {v3 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;-><init>(IIIIIIIIIIIIIIII)V

    .line 2324
    .end local v4    # "landMpPlaySetting":I
    .end local v21    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;
    .end local v22    # "isSupportVideoOnLand":Z
    .local v3, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;
    :goto_1
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    .end local v20    # "cursor":Landroid/database/Cursor;
    :goto_2
    move-object/from16 v21, v3

    .line 2330
    .end local v3    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;
    .restart local v21    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;
    goto/16 :goto_0

    .line 2327
    :cond_2
    const-string v5, ""

    const-string v6, "SETTING_URI==null"

    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    move-object/from16 v3, v21

    .end local v21    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;
    .restart local v3    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;
    goto :goto_2

    .end local v3    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;
    .restart local v20    # "cursor":Landroid/database/Cursor;
    .restart local v21    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;
    :cond_4
    move-object/from16 v3, v21

    .end local v21    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;
    .restart local v3    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;
    goto :goto_1
.end method

.method public static insertCity(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)I
    .locals 9
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "cityInfo"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .param p2, "detailInfo"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .param p3, "body"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    .line 1993
    const/4 v4, 0x0

    .line 1994
    .local v4, "result":I
    if-nez p0, :cond_0

    .line 1995
    const-string v6, ""

    const-string v7, "iC: context is null"

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v5, v4

    .line 2030
    .end local v4    # "result":I
    .local v5, "result":I
    :goto_0
    return v5

    .line 1999
    .end local v5    # "result":I
    .restart local v4    # "result":I
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2000
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz p1, :cond_2

    .line 2001
    if-eqz v0, :cond_2

    .line 2002
    invoke-static {p0, p1, p2, p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->buildWeatherContentValues(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v3

    .line 2004
    .local v3, "listValues":Landroid/content/ContentValues;
    :try_start_0
    sget-object v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    invoke-static {v0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2005
    sget-object v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    invoke-virtual {v0, v6, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 2006
    .local v2, "insertUri":Landroid/net/Uri;
    if-eqz v2, :cond_3

    .line 2008
    :try_start_1
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/BManager;->isCheckSupportClock(Landroid/content/Context;)Z

    move-result v6

    if-ne v6, v7, :cond_1

    .line 2009
    sget-object v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    invoke-static {v0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2010
    const/4 v6, 0x1

    const-string v7, "DBH IC"

    invoke-static {p0, v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/BManager;->sendBroadCastToProvider(Landroid/content/Context;ZLjava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 2016
    :cond_1
    :goto_1
    const/4 v4, 0x1

    .end local v2    # "insertUri":Landroid/net/Uri;
    .end local v3    # "listValues":Landroid/content/ContentValues;
    :cond_2
    :goto_2
    move v5, v4

    .line 2030
    .end local v4    # "result":I
    .restart local v5    # "result":I
    goto :goto_0

    .line 2013
    .end local v5    # "result":I
    .restart local v2    # "insertUri":Landroid/net/Uri;
    .restart local v3    # "listValues":Landroid/content/ContentValues;
    .restart local v4    # "result":I
    :catch_0
    move-exception v1

    .line 2014
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v6, ""

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "db : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 2023
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "insertUri":Landroid/net/Uri;
    :catch_1
    move-exception v1

    .line 2024
    .restart local v1    # "e":Ljava/lang/Exception;
    const-string v6, ""

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Exception "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2025
    const/4 v4, -0x1

    goto :goto_2

    .line 2018
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v2    # "insertUri":Landroid/net/Uri;
    :cond_3
    const/4 v4, -0x1

    goto :goto_2

    .line 2021
    .end local v2    # "insertUri":Landroid/net/Uri;
    :cond_4
    :try_start_3
    const-string v6, ""

    const-string v7, "SETTING_URI==null"

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2
.end method

.method public static insertCityCurrentLocation(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)I
    .locals 9
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "cityInfo"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .param p2, "detailInfo"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .param p3, "body"    # Ljava/lang/String;

    .prologue
    .line 2034
    const/4 v4, 0x0

    .line 2035
    .local v4, "result":I
    if-nez p0, :cond_0

    .line 2036
    const-string v6, ""

    const-string v7, "iC: context is null"

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v5, v4

    .line 2063
    .end local v4    # "result":I
    .local v5, "result":I
    :goto_0
    return v5

    .line 2040
    .end local v5    # "result":I
    .restart local v4    # "result":I
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2041
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz p1, :cond_1

    .line 2042
    if-eqz v0, :cond_1

    .line 2043
    invoke-static {p0, p1, p2, p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->buildWeatherContentValues(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v3

    .line 2045
    .local v3, "listValues":Landroid/content/ContentValues;
    :try_start_0
    sget-object v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    invoke-static {v0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2046
    sget-object v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    invoke-virtual {v0, v6, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    .line 2047
    .local v2, "insertUri":Landroid/net/Uri;
    if-eqz v2, :cond_2

    .line 2048
    const/4 v4, 0x1

    .end local v2    # "insertUri":Landroid/net/Uri;
    .end local v3    # "listValues":Landroid/content/ContentValues;
    :cond_1
    :goto_1
    move v5, v4

    .line 2063
    .end local v4    # "result":I
    .restart local v5    # "result":I
    goto :goto_0

    .line 2050
    .end local v5    # "result":I
    .restart local v2    # "insertUri":Landroid/net/Uri;
    .restart local v3    # "listValues":Landroid/content/ContentValues;
    .restart local v4    # "result":I
    :cond_2
    const/4 v4, -0x1

    goto :goto_1

    .line 2053
    .end local v2    # "insertUri":Landroid/net/Uri;
    :cond_3
    const-string v6, ""

    const-string v7, "SETTING_URI==null"

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 2055
    :catch_0
    move-exception v1

    .line 2056
    .local v1, "e":Ljava/lang/Exception;
    const-string v6, ""

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Exception "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2057
    const/4 v4, -0x1

    goto :goto_1
.end method

.method public static insertDetailHourInfo(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)I
    .locals 8
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "location"    # Ljava/lang/String;
    .param p2, "info"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    .prologue
    .line 4180
    const/4 v3, 0x0

    .line 4181
    .local v3, "result":I
    if-nez p0, :cond_0

    .line 4182
    const-string v6, ""

    const-string v7, "iDHI: context is null"

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v3

    .line 4205
    .end local v3    # "result":I
    .local v4, "result":I
    :goto_0
    return v4

    .line 4186
    .end local v4    # "result":I
    .restart local v3    # "result":I
    :cond_0
    if-nez p2, :cond_1

    .line 4187
    const-string v6, ""

    const-string v7, "iDHI: info is null"

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v3

    .line 4188
    .end local v3    # "result":I
    .restart local v4    # "result":I
    goto :goto_0

    .line 4191
    .end local v4    # "result":I
    .restart local v3    # "result":I
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 4192
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_2

    .line 4193
    invoke-static {p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->buildDetailWeatherHourContentValues(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)Landroid/content/ContentValues;

    move-result-object v2

    .line 4194
    .local v2, "data":Landroid/content/ContentValues;
    const-string v6, "LOCATION"

    invoke-virtual {v2, v6, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4196
    const/4 v1, 0x0

    .line 4197
    .local v1, "daemonrui":Landroid/net/Uri;
    sget-object v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_WEATHERINFO_HOUR_URI:Landroid/net/Uri;

    invoke-virtual {v0, v6, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v5

    .line 4199
    .local v5, "resultUri":Landroid/net/Uri;
    if-eqz v5, :cond_3

    .line 4200
    const/4 v3, 0x1

    .end local v1    # "daemonrui":Landroid/net/Uri;
    .end local v2    # "data":Landroid/content/ContentValues;
    .end local v5    # "resultUri":Landroid/net/Uri;
    :cond_2
    :goto_1
    move v4, v3

    .line 4205
    .end local v3    # "result":I
    .restart local v4    # "result":I
    goto :goto_0

    .line 4202
    .end local v4    # "result":I
    .restart local v1    # "daemonrui":Landroid/net/Uri;
    .restart local v2    # "data":Landroid/content/ContentValues;
    .restart local v3    # "result":I
    .restart local v5    # "resultUri":Landroid/net/Uri;
    :cond_3
    const/4 v3, -0x1

    goto :goto_1
.end method

.method public static insertDetailInfo(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)I
    .locals 9
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "location"    # Ljava/lang/String;
    .param p2, "info"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    .prologue
    .line 1790
    const/4 v3, 0x0

    .line 1791
    .local v3, "result":I
    if-nez p0, :cond_0

    .line 1792
    const-string v6, ""

    const-string v7, "iDI: context is null"

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v3

    .line 1817
    .end local v3    # "result":I
    .local v4, "result":I
    :goto_0
    return v4

    .line 1795
    .end local v4    # "result":I
    .restart local v3    # "result":I
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1797
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_1

    .line 1798
    invoke-static {p0, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->buildDetailWeatherContentValues(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)Landroid/content/ContentValues;

    move-result-object v1

    .line 1799
    .local v1, "data":Landroid/content/ContentValues;
    const-string v6, "LOCATION"

    invoke-virtual {v1, v6, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1801
    :try_start_0
    sget-object v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    invoke-static {v0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1802
    sget-object v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    invoke-virtual {v0, v6, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v5

    .line 1803
    .local v5, "rui":Landroid/net/Uri;
    if-eqz v5, :cond_2

    .line 1804
    const/4 v3, 0x1

    .end local v1    # "data":Landroid/content/ContentValues;
    .end local v5    # "rui":Landroid/net/Uri;
    :cond_1
    :goto_1
    move v4, v3

    .line 1817
    .end local v3    # "result":I
    .restart local v4    # "result":I
    goto :goto_0

    .line 1806
    .end local v4    # "result":I
    .restart local v1    # "data":Landroid/content/ContentValues;
    .restart local v3    # "result":I
    .restart local v5    # "rui":Landroid/net/Uri;
    :cond_2
    const/4 v3, -0x1

    goto :goto_1

    .line 1809
    .end local v5    # "rui":Landroid/net/Uri;
    :cond_3
    const-string v6, ""

    const-string v7, "SETTING_URI==null"

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1811
    :catch_0
    move-exception v2

    .line 1812
    .local v2, "e":Ljava/lang/Exception;
    const-string v6, ""

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Exception "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1813
    const/4 v3, -0x1

    goto :goto_1
.end method

.method public static insertLog(Landroid/content/Context;JLjava/lang/String;)I
    .locals 21
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "nowtime"    # J
    .param p3, "errlog"    # Ljava/lang/String;

    .prologue
    .line 3623
    const-wide/32 v7, 0x48190800

    .line 3624
    .local v7, "TWO_WEEK":J
    const/16 v18, 0x0

    .line 3625
    .local v18, "result":I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    .line 3626
    .local v9, "currentTime":J
    if-eqz p0, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-nez v2, :cond_1

    .line 3627
    :cond_0
    const-string v2, ""

    const-string v3, "iC: context is null or nT is 0"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move/from16 v19, v18

    .line 3676
    .end local v18    # "result":I
    .local v19, "result":I
    :goto_0
    return v19

    .line 3631
    .end local v19    # "result":I
    .restart local v18    # "result":I
    :cond_1
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 3632
    .local v1, "cp":Landroid/content/ContentResolver;
    const/4 v11, 0x0

    .line 3634
    .local v11, "cursor":Landroid/database/Cursor;
    if-eqz v1, :cond_5

    .line 3635
    :try_start_0
    sget-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_WEATHER_LOG_URI:Landroid/net/Uri;

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 3636
    sget-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_WEATHER_LOG_URI:Landroid/net/Uri;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "NOW_TIME"

    aput-object v5, v3, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 3639
    if-eqz v11, :cond_4

    .line 3640
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 3643
    :cond_2
    const-string v2, "NOW_TIME"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v15

    .line 3644
    .local v15, "nowTime":J
    const-wide/32 v2, 0x48190800

    add-long/2addr v2, v15

    cmp-long v2, v9, v2

    if-lez v2, :cond_3

    .line 3645
    const-string v2, "NOW_TIME"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 3646
    .local v20, "time":Ljava/lang/String;
    const-string v2, "NOW_TIME=\"%s\""

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v20, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    .line 3647
    .local v17, "qry":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_WEATHER_LOG_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3649
    .end local v17    # "qry":Ljava/lang/String;
    .end local v20    # "time":Ljava/lang/String;
    :cond_3
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 3653
    .end local v15    # "nowTime":J
    :cond_4
    new-instance v14, Landroid/content/ContentValues;

    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 3655
    .local v14, "listValues":Landroid/content/ContentValues;
    const-string v2, "NOW_TIME"

    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v14, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3656
    const-string v2, "FAIL_LOG"

    move-object/from16 v0, p3

    invoke-virtual {v14, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3658
    sget-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_WEATHER_LOG_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v14}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v13

    .line 3660
    .local v13, "insertUri":Landroid/net/Uri;
    if-eqz v13, :cond_7

    .line 3661
    const/16 v18, 0x1

    .line 3672
    .end local v13    # "insertUri":Landroid/net/Uri;
    .end local v14    # "listValues":Landroid/content/ContentValues;
    :cond_5
    :goto_1
    if-eqz v11, :cond_6

    .line 3673
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_6
    :goto_2
    move/from16 v19, v18

    .line 3676
    .end local v18    # "result":I
    .restart local v19    # "result":I
    goto/16 :goto_0

    .line 3663
    .end local v19    # "result":I
    .restart local v13    # "insertUri":Landroid/net/Uri;
    .restart local v14    # "listValues":Landroid/content/ContentValues;
    .restart local v18    # "result":I
    :cond_7
    const/16 v18, -0x1

    goto :goto_1

    .line 3666
    .end local v13    # "insertUri":Landroid/net/Uri;
    .end local v14    # "listValues":Landroid/content/ContentValues;
    :cond_8
    :try_start_1
    const-string v2, ""

    const-string v3, "WEATHER_LOG_URI == null"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 3669
    :catch_0
    move-exception v12

    .line 3670
    .local v12, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getAllCityList failed :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3672
    if-eqz v11, :cond_6

    .line 3673
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 3672
    .end local v12    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v11, :cond_9

    .line 3673
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_9
    throw v2
.end method

.method public static insertPhotosInfo(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)I
    .locals 9
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "info"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    .prologue
    .line 3918
    const/4 v3, 0x0

    .line 3919
    .local v3, "result":I
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfoSize()I

    move-result v6

    .line 3921
    .local v6, "size":I
    if-nez p0, :cond_0

    move v4, v3

    .line 3948
    .end local v3    # "result":I
    .local v4, "result":I
    :goto_0
    return v4

    .line 3925
    .end local v4    # "result":I
    .restart local v3    # "result":I
    :cond_0
    if-nez v6, :cond_1

    move v4, v3

    .line 3926
    .end local v3    # "result":I
    .restart local v4    # "result":I
    goto :goto_0

    .line 3929
    .end local v4    # "result":I
    .restart local v3    # "result":I
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 3931
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_4

    .line 3933
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v6, :cond_4

    .line 3934
    invoke-virtual {p1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->buildPhotosInofContentValues(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;)Landroid/content/ContentValues;

    move-result-object v1

    .line 3935
    .local v1, "data":Landroid/content/ContentValues;
    sget-object v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_PHOTOSINFO_URI:Landroid/net/Uri;

    invoke-static {v0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 3936
    sget-object v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_PHOTOSINFO_URI:Landroid/net/Uri;

    invoke-virtual {v0, v7, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v5

    .line 3938
    .local v5, "rui":Landroid/net/Uri;
    if-eqz v5, :cond_2

    .line 3939
    const/4 v3, 0x1

    .line 3933
    .end local v5    # "rui":Landroid/net/Uri;
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 3941
    .restart local v5    # "rui":Landroid/net/Uri;
    :cond_2
    const/4 v3, -0x1

    goto :goto_2

    .line 3944
    .end local v5    # "rui":Landroid/net/Uri;
    :cond_3
    const-string v7, ""

    const-string v8, "SETTING_URI==null"

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .end local v1    # "data":Landroid/content/ContentValues;
    .end local v2    # "i":I
    :cond_4
    move v4, v3

    .line 3948
    .end local v3    # "result":I
    .restart local v4    # "result":I
    goto :goto_0
.end method

.method public static insertPhotosInfo(Landroid/content/Context;Ljava/util/ArrayList;)I
    .locals 9
    .param p0, "ctx"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 3952
    .local p1, "info":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;>;"
    const/4 v3, 0x0

    .line 3953
    .local v3, "result":I
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 3955
    .local v6, "size":I
    if-nez p0, :cond_0

    move v4, v3

    .line 3982
    .end local v3    # "result":I
    .local v4, "result":I
    :goto_0
    return v4

    .line 3959
    .end local v4    # "result":I
    .restart local v3    # "result":I
    :cond_0
    if-nez v6, :cond_1

    move v4, v3

    .line 3960
    .end local v3    # "result":I
    .restart local v4    # "result":I
    goto :goto_0

    .line 3963
    .end local v4    # "result":I
    .restart local v3    # "result":I
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 3965
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_4

    .line 3967
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v6, :cond_4

    .line 3968
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;

    invoke-static {p0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->buildPhotosInofContentValues(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;)Landroid/content/ContentValues;

    move-result-object v1

    .line 3969
    .local v1, "data":Landroid/content/ContentValues;
    sget-object v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_PHOTOSINFO_URI:Landroid/net/Uri;

    invoke-static {v0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 3970
    sget-object v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_PHOTOSINFO_URI:Landroid/net/Uri;

    invoke-virtual {v0, v7, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v5

    .line 3972
    .local v5, "rui":Landroid/net/Uri;
    if-eqz v5, :cond_2

    .line 3973
    const/4 v3, 0x1

    .line 3967
    .end local v5    # "rui":Landroid/net/Uri;
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 3975
    .restart local v5    # "rui":Landroid/net/Uri;
    :cond_2
    const/4 v3, -0x1

    goto :goto_2

    .line 3978
    .end local v5    # "rui":Landroid/net/Uri;
    :cond_3
    const-string v7, ""

    const-string v8, "SETTING_URI==null"

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .end local v1    # "data":Landroid/content/ContentValues;
    .end local v2    # "i":I
    :cond_4
    move v4, v3

    .line 3982
    .end local v3    # "result":I
    .restart local v4    # "result":I
    goto :goto_0
.end method

.method public static isMaxCityListAdded(Landroid/content/Context;)Z
    .locals 4
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 613
    const/4 v1, 0x0

    .line 615
    .local v1, "result":Z
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getAllCityListForDelete(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 617
    .local v0, "count":I
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09000d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 618
    const/16 v2, 0x10

    if-lt v0, v2, :cond_0

    .line 619
    const/4 v1, 0x1

    .line 625
    :cond_0
    :goto_0
    return v1

    .line 621
    :cond_1
    const/16 v2, 0xa

    if-lt v0, v2, :cond_0

    .line 622
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isRegisteredToCityList(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 10
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v9, 0x0

    .line 587
    const/4 v7, 0x0

    .line 588
    .local v7, "result":Z
    if-nez p0, :cond_0

    .line 589
    const-string v1, ""

    const-string v2, "iRTCL: context is null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v8, v7

    .line 609
    .end local v7    # "result":Z
    .local v8, "result":I
    :goto_0
    return v8

    .line 592
    .end local v8    # "result":I
    .restart local v7    # "result":Z
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 593
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_2

    .line 594
    const-string v1, "Location=\"%s\""

    new-array v2, v5, [Ljava/lang/Object;

    aput-object p1, v2, v9

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 595
    .local v3, "loc":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 596
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    new-array v2, v5, [Ljava/lang/String;

    const-string v5, "NAME"

    aput-object v5, v2, v9

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 598
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    .line 599
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 600
    const/4 v7, 0x1

    .line 602
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .end local v3    # "loc":Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_2
    :goto_1
    move v8, v7

    .line 609
    .restart local v8    # "result":I
    goto :goto_0

    .line 605
    .end local v8    # "result":I
    .restart local v3    # "loc":Ljava/lang/String;
    :cond_3
    const-string v1, ""

    const-string v2, "SETTING_URI==null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static isRegisteredToDetailHourInfo(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 10
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v9, 0x0

    .line 4116
    const/4 v7, 0x0

    .line 4117
    .local v7, "result":Z
    if-nez p0, :cond_0

    .line 4118
    const-string v1, ""

    const-string v2, "iDHI: context is null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v8, v7

    .line 4141
    .end local v7    # "result":Z
    .local v8, "result":I
    :goto_0
    return v8

    .line 4121
    .end local v8    # "result":I
    .restart local v7    # "result":Z
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 4123
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_2

    .line 4124
    const-string v1, "LOCATION=\"%s\""

    new-array v2, v5, [Ljava/lang/Object;

    aput-object p1, v2, v9

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 4125
    .local v3, "loc":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_WEATHERINFO_HOUR_URI:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 4126
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_WEATHERINFO_HOUR_URI:Landroid/net/Uri;

    new-array v2, v5, [Ljava/lang/String;

    const-string v5, "LOCATION"

    aput-object v5, v2, v9

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 4131
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    .line 4132
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4133
    const/4 v7, 0x1

    .line 4135
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .end local v3    # "loc":Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_2
    :goto_1
    move v8, v7

    .line 4141
    .restart local v8    # "result":I
    goto :goto_0

    .line 4138
    .end local v8    # "result":I
    .restart local v3    # "loc":Ljava/lang/String;
    :cond_3
    const-string v1, ""

    const-string v2, "SETTING_URI==null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static isRegisteredToDetailInfo(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 10
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v9, 0x0

    .line 2175
    const/4 v7, 0x0

    .line 2176
    .local v7, "result":Z
    if-nez p0, :cond_0

    .line 2177
    const-string v1, ""

    const-string v2, "iRTDI: context is null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v8, v7

    .line 2196
    .end local v7    # "result":Z
    .local v8, "result":I
    :goto_0
    return v8

    .line 2180
    .end local v8    # "result":I
    .restart local v7    # "result":Z
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2181
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_2

    .line 2182
    const-string v1, "Location=\"%s\""

    new-array v2, v5, [Ljava/lang/Object;

    aput-object p1, v2, v9

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2183
    .local v3, "loc":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2184
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    new-array v2, v5, [Ljava/lang/String;

    const-string v5, "LOCATION"

    aput-object v5, v2, v9

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2186
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    .line 2187
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2188
    const/4 v7, 0x1

    .line 2190
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .end local v3    # "loc":Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_2
    :goto_1
    move v8, v7

    .line 2196
    .restart local v8    # "result":I
    goto :goto_0

    .line 2193
    .end local v8    # "result":I
    .restart local v3    # "loc":Ljava/lang/String;
    :cond_3
    const-string v1, ""

    const-string v2, "SETTING_URI==null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private static isSummerTime(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 11
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 3220
    const/4 v7, 0x0

    .line 3221
    .local v7, "result":Z
    if-nez p0, :cond_0

    .line 3222
    const-string v1, ""

    const-string v2, "iST: context is null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v8, v7

    .line 3243
    .end local v7    # "result":Z
    .local v8, "result":I
    :goto_0
    return v8

    .line 3225
    .end local v8    # "result":I
    .restart local v7    # "result":Z
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 3226
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_2

    .line 3227
    const-string v1, "Location=\"%s\""

    new-array v2, v10, [Ljava/lang/Object;

    aput-object p1, v2, v9

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 3228
    .local v3, "loc":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 3229
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    new-array v2, v10, [Ljava/lang/String;

    const-string v5, "SUMMER_TIME"

    aput-object v5, v2, v9

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 3231
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    .line 3232
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3233
    invoke-interface {v6, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-ne v1, v10, :cond_1

    .line 3234
    const/4 v7, 0x1

    .line 3237
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .end local v3    # "loc":Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_2
    :goto_1
    move v8, v7

    .line 3243
    .restart local v8    # "result":I
    goto :goto_0

    .line 3240
    .end local v8    # "result":I
    .restart local v3    # "loc":Ljava/lang/String;
    :cond_3
    const-string v1, ""

    const-string v2, "SETTING_URI==null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private static makeCityData(Landroid/content/Context;Landroid/database/Cursor;Ljava/util/Calendar;I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;
    .locals 26
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "c"    # Landroid/database/Cursor;
    .param p2, "cal"    # Ljava/util/Calendar;
    .param p3, "dbScale"    # I

    .prologue
    .line 695
    const-string v2, "TEMP_SCALE"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v25

    .line 696
    .local v25, "tempScale":I
    const/4 v14, 0x0

    .line 697
    .local v14, "dst":I
    const/4 v12, 0x0

    .line 698
    .local v12, "isCurrentLocation":Z
    const-string v2, "REAL_LOCATION"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 699
    .local v15, "realLocation":Ljava/lang/String;
    if-eqz v15, :cond_0

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 700
    const-string v2, "cityId:current"

    const-string v3, "LOCATION"

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 701
    const/4 v12, 0x1

    .line 705
    :cond_0
    :try_start_0
    const-string v2, "SUMMER_TIME"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v24

    .line 706
    .local v24, "summerTime":Ljava/lang/String;
    if-eqz v24, :cond_1

    .line 707
    const-string v2, "SUMMER_TIME"

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v14

    .line 712
    .end local v24    # "summerTime":Ljava/lang/String;
    :cond_1
    :goto_0
    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;

    const-string v3, "NAME"

    .line 713
    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "STATE"

    .line 714
    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "LOCATION"

    .line 715
    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "TIMEZONE"

    .line 716
    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v1, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getCurrentTime(Landroid/content/Context;Ljava/util/Calendar;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 717
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v1, v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->dateFormat(Landroid/content/Context;Ljava/util/Calendar;ZZ)Ljava/lang/String;

    move-result-object v7

    const-string v8, "TODAY_WEATHER_TEXT"

    .line 718
    move-object/from16 v0, p1

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    move-object/from16 v0, p1

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, "TODAY_TEMP"

    .line 719
    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getFloat(I)F

    move-result v9

    const/4 v10, 0x0

    move/from16 v0, v25

    move/from16 v1, p3

    invoke-static {v0, v1, v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIFZ)Ljava/lang/String;

    move-result-object v9

    const-string v10, "TODAY_LOW_TEMP"

    .line 720
    move-object/from16 v0, p1

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    move-object/from16 v0, p1

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getFloat(I)F

    move-result v10

    const/4 v11, 0x0

    move/from16 v0, v25

    move/from16 v1, p3

    invoke-static {v0, v1, v10, v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIFZ)Ljava/lang/String;

    move-result-object v10

    const-string v11, "TODAY_HIGH_TEMP"

    .line 721
    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getFloat(I)F

    move-result v11

    const/4 v13, 0x0

    move/from16 v0, v25

    move/from16 v1, p3

    invoke-static {v0, v1, v11, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIFZ)Ljava/lang/String;

    move-result-object v11

    const-string v13, "TODAY_ICON_NUM"

    .line 723
    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    const-string v16, "SUMMER_TIME"

    .line 726
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    const-string v17, "TODAY_SUNRISE_TIME"

    .line 727
    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    const-string v18, "TODAY_SUNSET_TIME"

    .line 728
    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    const-string v19, "TIMEZONE"

    .line 729
    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    const-string v20, "LATITUDE"

    .line 730
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    const-string v21, "LONGITUDE"

    .line 731
    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    const-string v22, "XML_DETAIL_INFO"

    .line 732
    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-direct/range {v2 .. v22}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v2

    .line 709
    :catch_0
    move-exception v23

    .line 710
    .local v23, "e":Ljava/lang/Exception;
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0
.end method

.method public static updateAllTempScaleCity(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)I
    .locals 9
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "location"    # Ljava/lang/String;
    .param p2, "detailInfo"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    .prologue
    .line 2102
    const/4 v3, 0x0

    .line 2103
    .local v3, "result":I
    if-nez p0, :cond_0

    .line 2104
    const-string v6, ""

    const-string v7, "iC: context is null"

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v3

    .line 2127
    .end local v3    # "result":I
    .local v4, "result":I
    :goto_0
    return v4

    .line 2108
    .end local v4    # "result":I
    .restart local v3    # "result":I
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2109
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz p2, :cond_1

    .line 2110
    if-eqz v0, :cond_1

    .line 2112
    const-string v6, "Location=\"%s\""

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p1, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2114
    .local v2, "loc":Ljava/lang/String;
    invoke-static {p0, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->WeatherTempScaleValues(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)Landroid/content/ContentValues;

    move-result-object v1

    .line 2115
    .local v1, "listValues":Landroid/content/ContentValues;
    sget-object v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    invoke-static {v0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2116
    sget-object v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v1, v2, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    .line 2117
    .local v5, "updateResult":I
    if-lez v5, :cond_2

    .line 2118
    const/4 v3, 0x1

    .end local v1    # "listValues":Landroid/content/ContentValues;
    .end local v2    # "loc":Ljava/lang/String;
    .end local v5    # "updateResult":I
    :cond_1
    :goto_1
    move v4, v3

    .line 2127
    .end local v3    # "result":I
    .restart local v4    # "result":I
    goto :goto_0

    .line 2120
    .end local v4    # "result":I
    .restart local v1    # "listValues":Landroid/content/ContentValues;
    .restart local v2    # "loc":Ljava/lang/String;
    .restart local v3    # "result":I
    .restart local v5    # "updateResult":I
    :cond_2
    const/4 v3, -0x1

    goto :goto_1

    .line 2123
    .end local v5    # "updateResult":I
    :cond_3
    const-string v6, ""

    const-string v7, "SETTING_URI==null"

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static updateAutoRefreshTime(Landroid/content/Context;I)I
    .locals 10
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "time"    # I

    .prologue
    const/4 v9, 0x1

    .line 2382
    const-string v6, ""

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ud AF "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2383
    const/4 v2, 0x0

    .line 2384
    .local v2, "result":I
    if-nez p0, :cond_0

    .line 2385
    const-string v6, ""

    const-string v7, "uART: context is null"

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 2420
    .end local v2    # "result":I
    .local v3, "result":I
    :goto_0
    return v3

    .line 2388
    .end local v3    # "result":I
    .restart local v2    # "result":I
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2389
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_2

    .line 2390
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 2391
    .local v5, "vals":Landroid/content/ContentValues;
    const-string v6, "AUTO_REFRESH_TIME"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2393
    :try_start_0
    sget-object v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    invoke-static {v0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2394
    sget-object v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v0, v6, v5, v7, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    .line 2396
    .local v4, "updateResult":I
    if-lez v4, :cond_3

    .line 2398
    :try_start_1
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/BManager;->isCheckSupportClock(Landroid/content/Context;)Z

    move-result v6

    if-ne v6, v9, :cond_1

    .line 2399
    sget-object v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    invoke-static {v0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2400
    const/4 v6, 0x1

    const-string v7, "DBH UART"

    invoke-static {p0, v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/BManager;->sendBroadCastToProvider(Landroid/content/Context;ZLjava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 2406
    :cond_1
    :goto_1
    const/4 v2, 0x1

    .line 2419
    .end local v4    # "updateResult":I
    .end local v5    # "vals":Landroid/content/ContentValues;
    :cond_2
    :goto_2
    invoke-static {p0, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setUpdateTimeToPref(Landroid/content/Context;Z)V

    move v3, v2

    .line 2420
    .end local v2    # "result":I
    .restart local v3    # "result":I
    goto :goto_0

    .line 2403
    .end local v3    # "result":I
    .restart local v2    # "result":I
    .restart local v4    # "updateResult":I
    .restart local v5    # "vals":Landroid/content/ContentValues;
    :catch_0
    move-exception v1

    .line 2404
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v6, ""

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "db : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 2413
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v4    # "updateResult":I
    :catch_1
    move-exception v1

    .line 2414
    .restart local v1    # "e":Ljava/lang/Exception;
    const-string v6, ""

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Exception "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2415
    const/4 v2, -0x1

    goto :goto_2

    .line 2408
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v4    # "updateResult":I
    :cond_3
    const/4 v2, -0x1

    goto :goto_2

    .line 2411
    .end local v4    # "updateResult":I
    :cond_4
    :try_start_3
    const-string v6, ""

    const-string v7, "SETTING_URI==null"

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2
.end method

.method public static updateAutoScrollSettings(Landroid/content/Context;I)I
    .locals 5
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "time"    # I

    .prologue
    .line 364
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ud st "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    const/4 v0, 0x0

    .line 367
    .local v0, "result":I
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 368
    .local v1, "vals":Landroid/content/ContentValues;
    const-string v2, "AUTO_SCROLL"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 370
    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateSettingTable(Landroid/content/Context;Landroid/content/ContentValues;Z)I

    move-result v0

    .line 371
    if-lez v0, :cond_0

    .line 372
    const/4 v0, 0x1

    .line 377
    :goto_0
    return v0

    .line 374
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static updateChangeOrder(Landroid/content/Context;Ljava/util/ArrayList;)I
    .locals 22
    .param p0, "ctx"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 2496
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;>;"
    const/4 v13, 0x0

    .line 2497
    .local v13, "result":I
    if-nez p0, :cond_0

    .line 2498
    const-string v19, ""

    const-string v20, "uCO: context is null"

    invoke-static/range {v19 .. v20}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v14, v13

    .line 2638
    .end local v13    # "result":I
    .local v14, "result":I
    :goto_0
    return v14

    .line 2501
    .end local v14    # "result":I
    .restart local v13    # "result":I
    :cond_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    .line 2502
    .local v5, "cp":Landroid/content/ContentResolver;
    if-eqz v5, :cond_3

    .line 2503
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v19

    move/from16 v0, v19

    new-array v3, v0, [Landroid/content/ContentValues;

    .line 2504
    .local v3, "arrVal":[Landroid/content/ContentValues;
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v19

    move/from16 v0, v19

    if-ge v9, v0, :cond_1

    .line 2505
    new-instance v19, Landroid/content/ContentValues;

    invoke-direct/range {v19 .. v19}, Landroid/content/ContentValues;-><init>()V

    aput-object v19, v3, v9

    .line 2506
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    .line 2507
    .local v11, "item":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;
    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getDetailWeatherInfoForChangeOrder(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    move-result-object v10

    .line 2509
    .local v10, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    aget-object v19, v3, v9

    const-string v20, "NAME"

    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getCityName()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2510
    aget-object v19, v3, v9

    const-string v20, "STATE"

    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getState()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2511
    aget-object v19, v3, v9

    const-string v20, "LOCATION"

    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2512
    aget-object v19, v3, v9

    const-string v20, "TIMEZONE"

    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getTimeZone()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2513
    aget-object v19, v3, v9

    const-string v20, "SUMMER_TIME"

    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getSummerTime()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2515
    aget-object v19, v3, v9

    const-string v20, "LATITUDE"

    const-string v21, ""

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2516
    aget-object v19, v3, v9

    const-string v20, "LONGITUDE"

    const-string v21, ""

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2518
    aget-object v19, v3, v9

    const-string v20, "REAL_LOCATION"

    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getRealLocation()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2520
    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v17

    .line 2521
    .local v17, "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    aget-object v19, v3, v9

    const-string v20, "TIMEZONE"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2522
    aget-object v19, v3, v9

    const-string v20, "TEMP_SCALE"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2523
    aget-object v19, v3, v9

    const-string v20, "TODAY_DATE"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDate()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2524
    aget-object v19, v3, v9

    const-string v20, "TODAY_TEMP"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getCurrentTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2525
    aget-object v19, v3, v9

    const-string v20, "TODAY_HIGH_TEMP"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getHighTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2526
    aget-object v19, v3, v9

    const-string v20, "TODAY_LOW_TEMP"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getLowTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2527
    aget-object v19, v3, v9

    const-string v20, "TODAY_ICON_NUM"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getIconNum()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2528
    aget-object v19, v3, v9

    const-string v20, "UPDATE_DATE"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getUpdateDate()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2529
    aget-object v19, v3, v9

    const-string v20, "TODAY_WEATHER_TEXT"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getWeatherText()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2530
    aget-object v19, v3, v9

    const-string v20, "TODAY_WEATHER_URL"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2531
    aget-object v19, v3, v9

    const-string v20, "TODAY_REALFELL"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getRealFeel()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2532
    aget-object v19, v3, v9

    const-string v20, "TODAY_SUNRISE_TIME"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2533
    aget-object v19, v3, v9

    const-string v20, "TODAY_SUNSET_TIME"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2535
    aget-object v19, v3, v9

    const-string v20, "UPDATE_DATE"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getUpdateDate()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2538
    aget-object v19, v3, v9

    const-string v20, "TODAY_DAY_RAIN_PROBABILITY"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayRainProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2539
    aget-object v19, v3, v9

    const-string v20, "TODAY_DAY_SNOW_PROBABILITY"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDaySnowProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2540
    aget-object v19, v3, v9

    const-string v20, "TODAY_DAY_HAIL_PROBABILITY"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayHailProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2541
    aget-object v19, v3, v9

    const-string v20, "TODAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayPrecipitationProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2542
    aget-object v19, v3, v9

    const-string v20, "TODAY_DAY_RAIN_AMOUNT"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayRainAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2543
    aget-object v19, v3, v9

    const-string v20, "TODAY_DAY_SNOW_AMOUNT"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDaySnowAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2544
    aget-object v19, v3, v9

    const-string v20, "TODAY_DAY_HAIL_AMOUNT"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayHailAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2545
    aget-object v19, v3, v9

    const-string v20, "TODAY_DAY_PRECIPITATION_AMOUNT"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayPrecipitationAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2547
    aget-object v19, v3, v9

    const-string v20, "TODAY_NIGHT_RAIN_PROBABILITY"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightRainProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2548
    aget-object v19, v3, v9

    const-string v20, "TODAY_NIGHT_SNOW_PROBABILITY"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightSnowProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2549
    aget-object v19, v3, v9

    const-string v20, "TODAY_NIGHT_HAIL_PROBABILITY"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightHailProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2550
    aget-object v19, v3, v9

    const-string v20, "TODAY_NIGHT_PRECIPITATION_PROBABILITY"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightPrecipitationProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2551
    aget-object v19, v3, v9

    const-string v20, "TODAY_NIGHT_RAIN_AMOUNT"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightRainAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2552
    aget-object v19, v3, v9

    const-string v20, "TODAY_NIGHT_SNOW_AMOUNT"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightSnowAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2553
    aget-object v19, v3, v9

    const-string v20, "TODAY_NIGHT_HAIL_AMOUNT"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightHailAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2554
    aget-object v19, v3, v9

    const-string v20, "TODAY_NIGHT_PRECIPITATION_AMOUNT"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightPrecipitationAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2556
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v10, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v12

    .line 2557
    .local v12, "oneday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_HIGH_TEMP"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2558
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_LOW_TEMP"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2559
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_ICON_NUM"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2560
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_URL"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2562
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_DAY_RAIN_PROBABILITY"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayRainProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2563
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_DAY_SNOW_PROBABILITY"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDaySnowProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2564
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_DAY_HAIL_PROBABILITY"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayHailProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2565
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2566
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_DAY_RAIN_AMOUNT"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayRainAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2567
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_DAY_SNOW_AMOUNT"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDaySnowAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2568
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_DAY_HAIL_AMOUNT"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayHailAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2569
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_DAY_PRECIPITATION_AMOUNT"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2571
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_NIGHT_RAIN_PROBABILITY"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightRainProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2572
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_NIGHT_SNOW_PROBABILITY"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightSnowProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2573
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_NIGHT_HAIL_PROBABILITY"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightHailProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2574
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_NIGHT_PRECIPITATION_PROBABILITY"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightPrecipitationProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2575
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_NIGHT_RAIN_AMOUNT"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightRainAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2576
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_NIGHT_SNOW_AMOUNT"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightSnowAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2577
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_NIGHT_HAIL_AMOUNT"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightHailAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2578
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_NIGHT_PRECIPITATION_AMOUNT"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightPrecipitationAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2580
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v10, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v18

    .line 2581
    .local v18, "twoday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    aget-object v19, v3, v9

    const-string v20, "TWODAY_HIGH_TEMP"

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2582
    aget-object v19, v3, v9

    const-string v20, "TWODAY_LOW_TEMP"

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2583
    aget-object v19, v3, v9

    const-string v20, "TWODAY_ICON_NUM"

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2584
    aget-object v19, v3, v9

    const-string v20, "TWODAY_URL"

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2586
    const/16 v19, 0x2

    move/from16 v0, v19

    invoke-virtual {v10, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v16

    .line 2587
    .local v16, "threeday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    aget-object v19, v3, v9

    const-string v20, "THREEDAY_HIGH_TEMP"

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2588
    aget-object v19, v3, v9

    const-string v20, "THREEDAY_LOW_TEMP"

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2589
    aget-object v19, v3, v9

    const-string v20, "THREEDAY_ICON_NUM"

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2590
    aget-object v19, v3, v9

    const-string v20, "THREEDAY_URL"

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2592
    const/16 v19, 0x3

    move/from16 v0, v19

    invoke-virtual {v10, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v8

    .line 2593
    .local v8, "fourday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    aget-object v19, v3, v9

    const-string v20, "FOURDAY_HIGH_TEMP"

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2594
    aget-object v19, v3, v9

    const-string v20, "FOURDAY_LOW_TEMP"

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2595
    aget-object v19, v3, v9

    const-string v20, "FOURDAY_ICON_NUM"

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2596
    aget-object v19, v3, v9

    const-string v20, "FOURDAY_URL"

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2598
    const/16 v19, 0x4

    move/from16 v0, v19

    invoke-virtual {v10, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v7

    .line 2599
    .local v7, "fiveday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    aget-object v19, v3, v9

    const-string v20, "FIVEDAY_HIGH_TEMP"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2600
    aget-object v19, v3, v9

    const-string v20, "FIVEDAY_LOW_TEMP"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2601
    aget-object v19, v3, v9

    const-string v20, "FIVEDAY_ICON_NUM"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2602
    aget-object v19, v3, v9

    const-string v20, "FIVEDAY_URL"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2604
    const/16 v19, 0x5

    move/from16 v0, v19

    invoke-virtual {v10, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v15

    .line 2605
    .local v15, "sixday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    aget-object v19, v3, v9

    const-string v20, "SIXDAY_HIGH_TEMP"

    invoke-virtual {v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2606
    aget-object v19, v3, v9

    const-string v20, "SIXDAY_LOW_TEMP"

    invoke-virtual {v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2607
    aget-object v19, v3, v9

    const-string v20, "SIXDAY_ICON_NUM"

    invoke-virtual {v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2608
    aget-object v19, v3, v9

    const-string v20, "SIXDAY_URL"

    invoke-virtual {v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2504
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_1

    .line 2610
    .end local v7    # "fiveday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .end local v8    # "fourday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .end local v10    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .end local v11    # "item":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;
    .end local v12    # "oneday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .end local v15    # "sixday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .end local v16    # "threeday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .end local v17    # "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    .end local v18    # "twoday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    :cond_1
    const/4 v4, 0x0

    .line 2612
    .local v4, "count":I
    :try_start_0
    sget-object v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    move-object/from16 v0, v19

    invoke-virtual {v5, v0, v3}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 2617
    :goto_2
    const-string v19, ""

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "DBHELPER updateChangeOrder "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2619
    if-lez v4, :cond_4

    .line 2621
    :try_start_1
    invoke-static/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/BManager;->isCheckSupportClock(Landroid/content/Context;)Z

    move-result v19

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_2

    .line 2622
    sget-object v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    move-object/from16 v0, v19

    invoke-static {v5, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v19

    if-eqz v19, :cond_2

    .line 2623
    const/16 v19, 0x1

    const-string v20, "DBH UCO"

    move-object/from16 v0, p0

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-static {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/BManager;->sendBroadCastToProvider(Landroid/content/Context;ZLjava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 2629
    :cond_2
    :goto_3
    const/4 v13, 0x1

    .end local v3    # "arrVal":[Landroid/content/ContentValues;
    .end local v4    # "count":I
    .end local v9    # "i":I
    :cond_3
    :goto_4
    move v14, v13

    .line 2638
    .end local v13    # "result":I
    .restart local v14    # "result":I
    goto/16 :goto_0

    .line 2613
    .end local v14    # "result":I
    .restart local v3    # "arrVal":[Landroid/content/ContentValues;
    .restart local v4    # "count":I
    .restart local v9    # "i":I
    .restart local v13    # "result":I
    :catch_0
    move-exception v6

    .line 2614
    .local v6, "e":Ljava/lang/Exception;
    const-string v19, ""

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Can\'t updateChangeOrder "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2615
    const/4 v13, -0x1

    goto :goto_2

    .line 2626
    .end local v6    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v6

    .line 2627
    .restart local v6    # "e":Ljava/lang/Exception;
    :try_start_2
    const-string v19, ""

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "db : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_3

    .line 2633
    :catch_2
    move-exception v6

    .line 2634
    const-string v19, ""

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Exception "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2635
    const/4 v13, -0x1

    goto :goto_4

    .line 2631
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_4
    const/4 v13, -0x1

    goto :goto_4
.end method

.method public static updateChangeOrderForGrid(Landroid/content/Context;Ljava/util/ArrayList;)I
    .locals 22
    .param p0, "ctx"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 2642
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;>;"
    const/4 v13, 0x0

    .line 2643
    .local v13, "result":I
    if-nez p0, :cond_0

    .line 2644
    const-string v19, ""

    const-string v20, "uCO: context is null"

    invoke-static/range {v19 .. v20}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v14, v13

    .line 2836
    .end local v13    # "result":I
    .local v14, "result":I
    :goto_0
    return v14

    .line 2647
    .end local v14    # "result":I
    .restart local v13    # "result":I
    :cond_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    .line 2648
    .local v5, "cp":Landroid/content/ContentResolver;
    if-eqz v5, :cond_4

    .line 2649
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v19

    move/from16 v0, v19

    new-array v3, v0, [Landroid/content/ContentValues;

    .line 2650
    .local v3, "arrVal":[Landroid/content/ContentValues;
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v19

    move/from16 v0, v19

    if-ge v9, v0, :cond_2

    .line 2651
    new-instance v19, Landroid/content/ContentValues;

    invoke-direct/range {v19 .. v19}, Landroid/content/ContentValues;-><init>()V

    aput-object v19, v3, v9

    .line 2652
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;

    .line 2653
    .local v11, "item":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;
    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getLocation()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getDetailWeatherInfoForChangeOrder(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    move-result-object v10

    .line 2655
    .local v10, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    aget-object v19, v3, v9

    const-string v20, "NAME"

    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getName()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2656
    aget-object v19, v3, v9

    const-string v20, "STATE"

    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getState()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2657
    aget-object v19, v3, v9

    const-string v20, "LOCATION"

    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getLocation()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2658
    aget-object v19, v3, v9

    const-string v20, "TIMEZONE"

    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getTimeZone()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2659
    aget-object v19, v3, v9

    const-string v20, "SUMMER_TIME"

    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getSummerTime()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2661
    aget-object v19, v3, v9

    const-string v20, "LATITUDE"

    const-string v21, ""

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2662
    aget-object v19, v3, v9

    const-string v20, "LONGITUDE"

    const-string v21, ""

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2664
    aget-object v19, v3, v9

    const-string v20, "REAL_LOCATION"

    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getRealLocation()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2666
    invoke-virtual {v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v17

    .line 2667
    .local v17, "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    aget-object v19, v3, v9

    const-string v20, "TIMEZONE"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2668
    aget-object v19, v3, v9

    const-string v20, "TEMP_SCALE"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2669
    aget-object v19, v3, v9

    const-string v20, "TODAY_DATE"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDate()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2670
    aget-object v19, v3, v9

    const-string v20, "TODAY_TEMP"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getCurrentTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2671
    aget-object v19, v3, v9

    const-string v20, "TODAY_HIGH_TEMP"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getHighTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2672
    aget-object v19, v3, v9

    const-string v20, "TODAY_LOW_TEMP"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getLowTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2673
    aget-object v19, v3, v9

    const-string v20, "TODAY_ICON_NUM"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getIconNum()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2674
    aget-object v19, v3, v9

    const-string v20, "UPDATE_DATE"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getUpdateDate()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2675
    aget-object v19, v3, v9

    const-string v20, "TODAY_WEATHER_TEXT"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getWeatherText()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2676
    aget-object v19, v3, v9

    const-string v20, "TODAY_WEATHER_URL"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2677
    aget-object v19, v3, v9

    const-string v20, "TODAY_REALFELL"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getRealFeel()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2678
    aget-object v19, v3, v9

    const-string v20, "TODAY_SUNRISE_TIME"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2679
    aget-object v19, v3, v9

    const-string v20, "TODAY_SUNSET_TIME"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2681
    aget-object v19, v3, v9

    const-string v20, "UPDATE_DATE"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getUpdateDate()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2684
    aget-object v19, v3, v9

    const-string v20, "TODAY_DAY_RAIN_PROBABILITY"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayRainProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2685
    aget-object v19, v3, v9

    const-string v20, "TODAY_DAY_SNOW_PROBABILITY"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDaySnowProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2686
    aget-object v19, v3, v9

    const-string v20, "TODAY_DAY_HAIL_PROBABILITY"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayHailProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2687
    aget-object v19, v3, v9

    const-string v20, "TODAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayPrecipitationProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2688
    aget-object v19, v3, v9

    const-string v20, "TODAY_DAY_RAIN_AMOUNT"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayRainAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2689
    aget-object v19, v3, v9

    const-string v20, "TODAY_DAY_SNOW_AMOUNT"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDaySnowAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2690
    aget-object v19, v3, v9

    const-string v20, "TODAY_DAY_HAIL_AMOUNT"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayHailAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2691
    aget-object v19, v3, v9

    const-string v20, "TODAY_DAY_PRECIPITATION_AMOUNT"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayPrecipitationAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2693
    aget-object v19, v3, v9

    const-string v20, "TODAY_NIGHT_RAIN_PROBABILITY"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightRainProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2694
    aget-object v19, v3, v9

    const-string v20, "TODAY_NIGHT_SNOW_PROBABILITY"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightSnowProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2695
    aget-object v19, v3, v9

    const-string v20, "TODAY_NIGHT_HAIL_PROBABILITY"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightHailProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2696
    aget-object v19, v3, v9

    const-string v20, "TODAY_NIGHT_PRECIPITATION_PROBABILITY"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightPrecipitationProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2697
    aget-object v19, v3, v9

    const-string v20, "TODAY_NIGHT_RAIN_AMOUNT"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightRainAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2698
    aget-object v19, v3, v9

    const-string v20, "TODAY_NIGHT_SNOW_AMOUNT"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightSnowAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2699
    aget-object v19, v3, v9

    const-string v20, "TODAY_NIGHT_HAIL_AMOUNT"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightHailAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2700
    aget-object v19, v3, v9

    const-string v20, "TODAY_NIGHT_PRECIPITATION_AMOUNT"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightPrecipitationAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2702
    aget-object v19, v3, v9

    const-string v20, "TODAY_HUM"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getRelativeHumidity()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2703
    aget-object v19, v3, v9

    const-string v20, "TODAY_UV_INDEX"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getUVIndex()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2704
    aget-object v19, v3, v9

    const-string v20, "TODAY_UV_INDEX_TEXT"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getUVIndexText()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2705
    aget-object v19, v3, v9

    const-string v20, "TODAY_DAY_ICON"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayIcon()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2706
    aget-object v19, v3, v9

    const-string v20, "TODAY_NIGHT_ICON"

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightIcon()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2707
    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getLocation()Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_1

    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getLocation()Ljava/lang/String;

    move-result-object v19

    const-string v20, "cityId:current"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_1

    .line 2708
    aget-object v19, v3, v9

    const-string v20, "XML_DETAIL_INFO"

    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getXmlDetailInfo()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2713
    :goto_2
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v10, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v12

    .line 2714
    .local v12, "oneday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_DAY_ICON"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getOnedayDayIcon()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2715
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_NIGHT_ICON"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getOnedayNightIcon()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2716
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_HIGH_TEMP"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2717
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_LOW_TEMP"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2718
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_ICON_NUM"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2719
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_URL"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2721
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_DAY_RAIN_PROBABILITY"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayRainProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2722
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_DAY_SNOW_PROBABILITY"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDaySnowProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2723
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_DAY_HAIL_PROBABILITY"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayHailProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2724
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2725
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_DAY_RAIN_AMOUNT"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayRainAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2726
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_DAY_SNOW_AMOUNT"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDaySnowAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2727
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_DAY_HAIL_AMOUNT"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayHailAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2728
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_DAY_PRECIPITATION_AMOUNT"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2730
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_NIGHT_RAIN_PROBABILITY"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightRainProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2731
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_NIGHT_SNOW_PROBABILITY"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightSnowProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2732
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_NIGHT_HAIL_PROBABILITY"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightHailProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2733
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_NIGHT_PRECIPITATION_PROBABILITY"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightPrecipitationProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2734
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_NIGHT_RAIN_AMOUNT"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightRainAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2735
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_NIGHT_SNOW_AMOUNT"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightSnowAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2736
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_NIGHT_HAIL_AMOUNT"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightHailAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2737
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_NIGHT_PRECIPITATION_AMOUNT"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getNightPrecipitationAmount()Ljava/lang/Double;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2739
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_SUNRISE_TIME"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2740
    aget-object v19, v3, v9

    const-string v20, "ONEDAY_SUNSET_TIME"

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2742
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v10, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v18

    .line 2743
    .local v18, "twoday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    aget-object v19, v3, v9

    const-string v20, "TWODAY_HIGH_TEMP"

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2744
    aget-object v19, v3, v9

    const-string v20, "TWODAY_LOW_TEMP"

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2745
    aget-object v19, v3, v9

    const-string v20, "TWODAY_ICON_NUM"

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2746
    aget-object v19, v3, v9

    const-string v20, "TWODAY_URL"

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2748
    aget-object v19, v3, v9

    const-string v20, "TWODAY_DAY_RAIN_PROBABILITY"

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayRainProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2749
    aget-object v19, v3, v9

    const-string v20, "TWODAY_DAY_SNOW_PROBABILITY"

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDaySnowProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2750
    aget-object v19, v3, v9

    const-string v20, "TWODAY_DAY_HAIL_PROBABILITY"

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayHailProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2751
    aget-object v19, v3, v9

    const-string v20, "TWODAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayHailProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2752
    aget-object v19, v3, v9

    const-string v20, "TWODAY_SUNRISE_TIME"

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2753
    aget-object v19, v3, v9

    const-string v20, "TWODAY_SUNSET_TIME"

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2755
    const/16 v19, 0x2

    move/from16 v0, v19

    invoke-virtual {v10, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v16

    .line 2756
    .local v16, "threeday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    aget-object v19, v3, v9

    const-string v20, "THREEDAY_HIGH_TEMP"

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2757
    aget-object v19, v3, v9

    const-string v20, "THREEDAY_LOW_TEMP"

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2758
    aget-object v19, v3, v9

    const-string v20, "THREEDAY_ICON_NUM"

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2759
    aget-object v19, v3, v9

    const-string v20, "THREEDAY_URL"

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2761
    aget-object v19, v3, v9

    const-string v20, "THREEDAY_DAY_RAIN_PROBABILITY"

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayRainProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2762
    aget-object v19, v3, v9

    const-string v20, "THREEDAY_DAY_SNOW_PROBABILITY"

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDaySnowProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2763
    aget-object v19, v3, v9

    const-string v20, "THREEDAY_DAY_HAIL_PROBABILITY"

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayHailProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2764
    aget-object v19, v3, v9

    const-string v20, "THREEDAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2765
    aget-object v19, v3, v9

    const-string v20, "THREEDAY_SUNRISE_TIME"

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2766
    aget-object v19, v3, v9

    const-string v20, "THREEDAY_SUNSET_TIME"

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2768
    const/16 v19, 0x3

    move/from16 v0, v19

    invoke-virtual {v10, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v8

    .line 2769
    .local v8, "fourday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    aget-object v19, v3, v9

    const-string v20, "FOURDAY_HIGH_TEMP"

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2770
    aget-object v19, v3, v9

    const-string v20, "FOURDAY_LOW_TEMP"

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2771
    aget-object v19, v3, v9

    const-string v20, "FOURDAY_ICON_NUM"

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2772
    aget-object v19, v3, v9

    const-string v20, "FOURDAY_URL"

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2774
    aget-object v19, v3, v9

    const-string v20, "FOURDAY_DAY_RAIN_PROBABILITY"

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayRainProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2775
    aget-object v19, v3, v9

    const-string v20, "FOURDAY_DAY_SNOW_PROBABILITY"

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDaySnowProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2776
    aget-object v19, v3, v9

    const-string v20, "FOURDAY_DAY_HAIL_PROBABILITY"

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayHailProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2777
    aget-object v19, v3, v9

    const-string v20, "FOURDAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2778
    aget-object v19, v3, v9

    const-string v20, "FOURDAY_SUNRISE_TIME"

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2779
    aget-object v19, v3, v9

    const-string v20, "FOURDAY_SUNSET_TIME"

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2781
    const/16 v19, 0x4

    move/from16 v0, v19

    invoke-virtual {v10, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v7

    .line 2782
    .local v7, "fiveday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    aget-object v19, v3, v9

    const-string v20, "FIVEDAY_HIGH_TEMP"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2783
    aget-object v19, v3, v9

    const-string v20, "FIVEDAY_LOW_TEMP"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2784
    aget-object v19, v3, v9

    const-string v20, "FIVEDAY_ICON_NUM"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2785
    aget-object v19, v3, v9

    const-string v20, "FIVEDAY_URL"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2787
    aget-object v19, v3, v9

    const-string v20, "FIVEDAY_DAY_RAIN_PROBABILITY"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayRainProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2788
    aget-object v19, v3, v9

    const-string v20, "FIVEDAY_DAY_SNOW_PROBABILITY"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDaySnowProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2789
    aget-object v19, v3, v9

    const-string v20, "FIVEDAY_DAY_HAIL_PROBABILITY"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayHailProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2790
    aget-object v19, v3, v9

    const-string v20, "FIVEDAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2791
    aget-object v19, v3, v9

    const-string v20, "FIVEDAY_SUNRISE_TIME"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2792
    aget-object v19, v3, v9

    const-string v20, "FIVEDAY_SUNSET_TIME"

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2794
    const/16 v19, 0x5

    move/from16 v0, v19

    invoke-virtual {v10, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v15

    .line 2795
    .local v15, "sixday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    aget-object v19, v3, v9

    const-string v20, "SIXDAY_HIGH_TEMP"

    invoke-virtual {v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getHighTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2796
    aget-object v19, v3, v9

    const-string v20, "SIXDAY_LOW_TEMP"

    invoke-virtual {v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getLowTemp()F

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2797
    aget-object v19, v3, v9

    const-string v20, "SIXDAY_ICON_NUM"

    invoke-virtual {v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getIconNum()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2798
    aget-object v19, v3, v9

    const-string v20, "SIXDAY_URL"

    invoke-virtual {v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2800
    aget-object v19, v3, v9

    const-string v20, "SIXDAY_DAY_RAIN_PROBABILITY"

    invoke-virtual {v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayRainProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2801
    aget-object v19, v3, v9

    const-string v20, "SIXDAY_DAY_SNOW_PROBABILITY"

    invoke-virtual {v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDaySnowProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2802
    aget-object v19, v3, v9

    const-string v20, "SIXDAY_DAY_HAIL_PROBABILITY"

    invoke-virtual {v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayHailProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2803
    aget-object v19, v3, v9

    const-string v20, "SIXDAY_DAY_PRECIPITATION_PROBABILITY"

    invoke-virtual {v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getDayPrecipitationProbability()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2804
    aget-object v19, v3, v9

    const-string v20, "SIXDAY_SUNRISE_TIME"

    invoke-virtual {v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2805
    aget-object v19, v3, v9

    const-string v20, "SIXDAY_SUNSET_TIME"

    invoke-virtual {v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2650
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_1

    .line 2710
    .end local v7    # "fiveday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .end local v8    # "fourday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .end local v12    # "oneday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .end local v15    # "sixday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .end local v16    # "threeday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .end local v18    # "twoday":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    :cond_1
    aget-object v19, v3, v9

    const-string v20, "XML_DETAIL_INFO"

    const-string v21, ""

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 2808
    .end local v10    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .end local v11    # "item":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;
    .end local v17    # "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    :cond_2
    const/4 v4, 0x0

    .line 2810
    .local v4, "count":I
    :try_start_0
    sget-object v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    move-object/from16 v0, v19

    invoke-virtual {v5, v0, v3}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 2815
    :goto_3
    const-string v19, ""

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "DBHELPER updateChangeOrder "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2817
    if-lez v4, :cond_5

    .line 2819
    :try_start_1
    invoke-static/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/BManager;->isCheckSupportClock(Landroid/content/Context;)Z

    move-result v19

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_3

    .line 2820
    sget-object v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    move-object/from16 v0, v19

    invoke-static {v5, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 2821
    const/16 v19, 0x1

    const-string v20, "DBH UCO"

    move-object/from16 v0, p0

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-static {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/BManager;->sendBroadCastToProvider(Landroid/content/Context;ZLjava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 2827
    :cond_3
    :goto_4
    const/4 v13, 0x1

    .end local v3    # "arrVal":[Landroid/content/ContentValues;
    .end local v4    # "count":I
    .end local v9    # "i":I
    :cond_4
    :goto_5
    move v14, v13

    .line 2836
    .end local v13    # "result":I
    .restart local v14    # "result":I
    goto/16 :goto_0

    .line 2811
    .end local v14    # "result":I
    .restart local v3    # "arrVal":[Landroid/content/ContentValues;
    .restart local v4    # "count":I
    .restart local v9    # "i":I
    .restart local v13    # "result":I
    :catch_0
    move-exception v6

    .line 2812
    .local v6, "e":Ljava/lang/Exception;
    const-string v19, ""

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Can\'t updateChangeOrder "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2813
    const/4 v13, -0x1

    goto :goto_3

    .line 2824
    .end local v6    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v6

    .line 2825
    .restart local v6    # "e":Ljava/lang/Exception;
    :try_start_2
    const-string v19, ""

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "db : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_4

    .line 2831
    :catch_2
    move-exception v6

    .line 2832
    const-string v19, ""

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Exception "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2833
    const/4 v13, -0x1

    goto :goto_5

    .line 2829
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_5
    const/4 v13, -0x1

    goto :goto_5
.end method

.method public static updateCheckCurrentCityLocation(Landroid/content/Context;I)I
    .locals 5
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "isEnable"    # I

    .prologue
    .line 547
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ud CCL"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 548
    const/4 v0, 0x0

    .line 549
    .local v0, "result":I
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 550
    .local v1, "vals":Landroid/content/ContentValues;
    const-string v2, "CHECK_CURRENT_CITY_LOCATION"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 552
    const/4 v2, 0x1

    invoke-static {p0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateSettingTable(Landroid/content/Context;Landroid/content/ContentValues;Z)I

    move-result v0

    .line 553
    if-lez v0, :cond_0

    .line 554
    const/4 v0, 0x1

    .line 558
    :goto_0
    return v0

    .line 556
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static updateCity(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)I
    .locals 10
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "location"    # Ljava/lang/String;
    .param p2, "cityInfo"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .param p3, "detailInfo"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .param p4, "body"    # Ljava/lang/String;

    .prologue
    .line 2067
    const/4 v4, 0x0

    .line 2068
    .local v4, "result":I
    if-nez p0, :cond_0

    .line 2069
    const-string v7, ""

    const-string v8, "iC: context is null"

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v5, v4

    .line 2098
    .end local v4    # "result":I
    .local v5, "result":I
    :goto_0
    return v5

    .line 2073
    .end local v5    # "result":I
    .restart local v4    # "result":I
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2074
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz p2, :cond_1

    .line 2076
    if-eqz v0, :cond_1

    .line 2078
    const-string v7, "Location=\"%s\""

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object p1, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2080
    .local v3, "loc":Ljava/lang/String;
    invoke-static {p0, p2, p3, p4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->buildWeatherContentValues(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v2

    .line 2082
    .local v2, "listValues":Landroid/content/ContentValues;
    :try_start_0
    sget-object v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    invoke-static {v0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 2083
    sget-object v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v2, v3, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    .line 2084
    .local v6, "updateResult":I
    if-lez v6, :cond_2

    .line 2085
    const/4 v4, 0x1

    .end local v2    # "listValues":Landroid/content/ContentValues;
    .end local v3    # "loc":Ljava/lang/String;
    .end local v6    # "updateResult":I
    :cond_1
    :goto_1
    move v5, v4

    .line 2098
    .end local v4    # "result":I
    .restart local v5    # "result":I
    goto :goto_0

    .line 2087
    .end local v5    # "result":I
    .restart local v2    # "listValues":Landroid/content/ContentValues;
    .restart local v3    # "loc":Ljava/lang/String;
    .restart local v4    # "result":I
    .restart local v6    # "updateResult":I
    :cond_2
    const/4 v4, -0x1

    goto :goto_1

    .line 2090
    .end local v6    # "updateResult":I
    :cond_3
    const-string v7, ""

    const-string v8, "SETTING_URI==null"

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 2092
    :catch_0
    move-exception v1

    .line 2093
    .local v1, "e":Ljava/lang/Exception;
    const-string v7, ""

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Exception "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2094
    const/4 v4, -0x1

    goto :goto_1
.end method

.method public static updateDaemonWeatherIconNum(Landroid/content/Context;I)I
    .locals 5
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "convertIcon"    # I

    .prologue
    .line 4027
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ud ci "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4028
    const/4 v0, 0x0

    .line 4030
    .local v0, "result":I
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 4031
    .local v1, "vals":Landroid/content/ContentValues;
    const-string v2, "WEATHER_ICON_NUM"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4033
    invoke-static {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateWeatherIconNum(Landroid/content/Context;Landroid/content/ContentValues;)I

    move-result v0

    .line 4034
    if-lez v0, :cond_0

    .line 4035
    const/4 v0, 0x1

    .line 4040
    :goto_0
    return v0

    .line 4037
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static updateDetailHourInfo(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)I
    .locals 9
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "location"    # Ljava/lang/String;
    .param p2, "info"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    .prologue
    .line 4145
    const/4 v3, 0x0

    .line 4146
    .local v3, "result":I
    if-nez p0, :cond_0

    .line 4147
    const-string v6, ""

    const-string v7, "uDHI: context is null"

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v3

    .line 4176
    .end local v3    # "result":I
    .local v4, "result":I
    :goto_0
    return v4

    .line 4151
    .end local v4    # "result":I
    .restart local v3    # "result":I
    :cond_0
    if-nez p2, :cond_1

    .line 4152
    const-string v6, ""

    const-string v7, "uDHI: info is null"

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v3

    .line 4153
    .end local v3    # "result":I
    .restart local v4    # "result":I
    goto :goto_0

    .line 4156
    .end local v4    # "result":I
    .restart local v3    # "result":I
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 4159
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_2

    .line 4160
    invoke-static {p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->buildDetailWeatherHourContentValues(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)Landroid/content/ContentValues;

    move-result-object v1

    .line 4162
    .local v1, "data":Landroid/content/ContentValues;
    const-string v6, "LOCATION=\"%s\""

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p1, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 4163
    .local v2, "loc":Ljava/lang/String;
    sget-object v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_WEATHERINFO_HOUR_URI:Landroid/net/Uri;

    invoke-static {v0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 4164
    sget-object v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_WEATHERINFO_HOUR_URI:Landroid/net/Uri;

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v1, v2, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    .line 4167
    .local v5, "updateResult":I
    if-lez v5, :cond_3

    .line 4168
    const/4 v3, 0x1

    .end local v1    # "data":Landroid/content/ContentValues;
    .end local v2    # "loc":Ljava/lang/String;
    .end local v5    # "updateResult":I
    :cond_2
    :goto_1
    move v4, v3

    .line 4176
    .end local v3    # "result":I
    .restart local v4    # "result":I
    goto :goto_0

    .line 4170
    .end local v4    # "result":I
    .restart local v1    # "data":Landroid/content/ContentValues;
    .restart local v2    # "loc":Ljava/lang/String;
    .restart local v3    # "result":I
    .restart local v5    # "updateResult":I
    :cond_3
    const/4 v3, -0x1

    goto :goto_1

    .line 4173
    .end local v5    # "updateResult":I
    :cond_4
    const-string v6, ""

    const-string v7, "SETTING_URI==null"

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static updateDetailInfo(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)I
    .locals 9
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "location"    # Ljava/lang/String;
    .param p2, "info"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    .prologue
    .line 1763
    const/4 v3, 0x0

    .line 1764
    .local v3, "result":I
    if-nez p0, :cond_0

    .line 1765
    const-string v6, ""

    const-string v7, "uDI: context is null"

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v3

    .line 1786
    .end local v3    # "result":I
    .local v4, "result":I
    :goto_0
    return v4

    .line 1768
    .end local v4    # "result":I
    .restart local v3    # "result":I
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1770
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_1

    .line 1771
    invoke-static {p0, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->buildDetailWeatherContentValues(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;)Landroid/content/ContentValues;

    move-result-object v1

    .line 1773
    .local v1, "data":Landroid/content/ContentValues;
    const-string v6, "Location=\"%s\""

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p1, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1774
    .local v2, "loc":Ljava/lang/String;
    sget-object v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    invoke-static {v0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1775
    sget-object v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v1, v2, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    .line 1777
    .local v5, "updateResult":I
    if-lez v5, :cond_2

    .line 1778
    const/4 v3, 0x1

    .end local v1    # "data":Landroid/content/ContentValues;
    .end local v2    # "loc":Ljava/lang/String;
    .end local v5    # "updateResult":I
    :cond_1
    :goto_1
    move v4, v3

    .line 1786
    .end local v3    # "result":I
    .restart local v4    # "result":I
    goto :goto_0

    .line 1780
    .end local v4    # "result":I
    .restart local v1    # "data":Landroid/content/ContentValues;
    .restart local v2    # "loc":Ljava/lang/String;
    .restart local v3    # "result":I
    .restart local v5    # "updateResult":I
    :cond_2
    const/4 v3, -0x1

    goto :goto_1

    .line 1783
    .end local v5    # "updateResult":I
    :cond_3
    const-string v6, ""

    const-string v7, "SETTING_URI==null"

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static updateLastSelectedLocation(Landroid/content/Context;Ljava/lang/String;)I
    .locals 5
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    .line 381
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ud lsl "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    const/4 v0, 0x0

    .line 384
    .local v0, "result":I
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 385
    .local v1, "vals":Landroid/content/ContentValues;
    const-string v2, "LAST_SEL_LOCATION"

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateSettingTable(Landroid/content/Context;Landroid/content/ContentValues;Z)I

    move-result v0

    .line 388
    if-lez v0, :cond_0

    .line 389
    const/4 v0, 0x1

    .line 395
    :goto_0
    invoke-static {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/ClientUtil;->sendPackages(Landroid/content/Context;Ljava/lang/String;)V

    .line 397
    return v0

    .line 391
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static updateLocationInfo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I
    .locals 9
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "cityName"    # Ljava/lang/String;
    .param p2, "location"    # Ljava/lang/String;

    .prologue
    .line 3808
    const/4 v2, 0x0

    .line 3809
    .local v2, "result":I
    if-nez p0, :cond_0

    .line 3810
    const-string v6, ""

    const-string v7, "uDI: context is null"

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 3833
    .end local v2    # "result":I
    .local v3, "result":I
    :goto_0
    return v3

    .line 3813
    .end local v3    # "result":I
    .restart local v2    # "result":I
    :cond_0
    const-string v6, ""

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "updateLocationInfo loc:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3814
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 3816
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_1

    .line 3817
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 3818
    .local v1, "data":Landroid/content/ContentValues;
    const-string v6, "LOCATION"

    invoke-virtual {v1, v6, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3820
    const-string v6, "NAME=\"%s\""

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p1, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 3821
    .local v4, "selection":Ljava/lang/String;
    sget-object v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    invoke-static {v0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 3822
    sget-object v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v1, v4, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    .line 3824
    .local v5, "updateResult":I
    if-lez v5, :cond_2

    .line 3825
    const/4 v2, 0x1

    .end local v1    # "data":Landroid/content/ContentValues;
    .end local v4    # "selection":Ljava/lang/String;
    .end local v5    # "updateResult":I
    :cond_1
    :goto_1
    move v3, v2

    .line 3833
    .end local v2    # "result":I
    .restart local v3    # "result":I
    goto :goto_0

    .line 3827
    .end local v3    # "result":I
    .restart local v1    # "data":Landroid/content/ContentValues;
    .restart local v2    # "result":I
    .restart local v4    # "selection":Ljava/lang/String;
    .restart local v5    # "updateResult":I
    :cond_2
    const/4 v2, -0x1

    goto :goto_1

    .line 3830
    .end local v5    # "updateResult":I
    :cond_3
    const-string v6, ""

    const-string v7, "SETTING_URI==null"

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static updateNextTime(Landroid/content/Context;J)I
    .locals 9
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "nextTime"    # J

    .prologue
    const/4 v8, 0x0

    .line 2424
    const-string v5, ""

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ud NT"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2425
    const/4 v1, 0x0

    .line 2426
    .local v1, "result":I
    if-nez p0, :cond_0

    .line 2427
    const-string v5, ""

    const-string v6, "gNT: context is null"

    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    .line 2448
    .end local v1    # "result":I
    .local v2, "result":I
    :goto_0
    return v2

    .line 2430
    .end local v2    # "result":I
    .restart local v1    # "result":I
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2432
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_1

    .line 2433
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 2434
    .local v4, "vals":Landroid/content/ContentValues;
    const-string v5, "AUTO_REF_NEXT_TIME"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2435
    sget-object v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    invoke-static {v0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2436
    sget-object v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    invoke-virtual {v0, v5, v4, v8, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    .line 2438
    .local v3, "updateResult":I
    if-lez v3, :cond_2

    .line 2439
    const/4 v1, 0x1

    .end local v3    # "updateResult":I
    .end local v4    # "vals":Landroid/content/ContentValues;
    :cond_1
    :goto_1
    move v2, v1

    .line 2448
    .end local v1    # "result":I
    .restart local v2    # "result":I
    goto :goto_0

    .line 2441
    .end local v2    # "result":I
    .restart local v1    # "result":I
    .restart local v3    # "updateResult":I
    .restart local v4    # "vals":Landroid/content/ContentValues;
    :cond_2
    const/4 v1, -0x1

    goto :goto_1

    .line 2444
    .end local v3    # "updateResult":I
    :cond_3
    const-string v5, ""

    const-string v6, "SETTING_URI==null"

    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static updateNotificationSettings(Landroid/content/Context;I)I
    .locals 5
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "isEnable"    # I

    .prologue
    .line 465
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ud UN "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 466
    const/4 v0, 0x0

    .line 468
    .local v0, "result":I
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 469
    .local v1, "vals":Landroid/content/ContentValues;
    const-string v2, "NOTIFICATION"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 471
    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateSettingTable(Landroid/content/Context;Landroid/content/ContentValues;Z)I

    move-result v0

    .line 472
    if-lez v0, :cond_0

    .line 473
    const/4 v0, 0x1

    .line 477
    :goto_0
    return v0

    .line 475
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static updateNotificationTimeSettings(Landroid/content/Context;J)I
    .locals 5
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "isEnable"    # J

    .prologue
    .line 481
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ud NTS "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 482
    const/4 v0, 0x0

    .line 484
    .local v0, "result":I
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 485
    .local v1, "vals":Landroid/content/ContentValues;
    const-string v2, "NOTIFICATION_SET_TIME"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 487
    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateSettingTable(Landroid/content/Context;Landroid/content/ContentValues;Z)I

    move-result v0

    .line 488
    if-lez v0, :cond_0

    .line 489
    const/4 v0, 0x1

    .line 493
    :goto_0
    return v0

    .line 491
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static updateRefreshEnteringSettings(Landroid/content/Context;I)I
    .locals 5
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "isEnable"    # I

    .prologue
    .line 401
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ud CB "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 402
    const/4 v0, 0x0

    .line 404
    .local v0, "result":I
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 405
    .local v1, "vals":Landroid/content/ContentValues;
    const-string v2, "REFRESH_ENTERING"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 407
    const/4 v2, 0x1

    invoke-static {p0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateSettingTable(Landroid/content/Context;Landroid/content/ContentValues;Z)I

    move-result v0

    .line 408
    if-lez v0, :cond_0

    .line 409
    const/4 v0, 0x1

    .line 413
    :goto_0
    return v0

    .line 411
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static updateRefreshRoamingSettings(Landroid/content/Context;I)I
    .locals 5
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "isEnable"    # I

    .prologue
    .line 417
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ud CB "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 418
    const/4 v0, 0x0

    .line 420
    .local v0, "result":I
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 421
    .local v1, "vals":Landroid/content/ContentValues;
    const-string v2, "REFRESH_ROAMING"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 423
    const/4 v2, 0x1

    invoke-static {p0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateSettingTable(Landroid/content/Context;Landroid/content/ContentValues;Z)I

    move-result v0

    .line 424
    if-lez v0, :cond_0

    .line 425
    const/4 v0, 0x1

    .line 429
    :goto_0
    return v0

    .line 427
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static updateSelectDefaultdLocation(Landroid/content/Context;Ljava/lang/String;)I
    .locals 5
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    .line 73
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ud sdl "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    const/4 v0, 0x0

    .line 76
    .local v0, "result":I
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 77
    .local v1, "vals":Landroid/content/ContentValues;
    const-string v2, "DEFAULT_LOCATION"

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateSettingTable(Landroid/content/Context;Landroid/content/ContentValues;Z)I

    move-result v0

    .line 80
    if-lez v0, :cond_0

    .line 81
    const/4 v0, 0x1

    .line 86
    :goto_0
    return v0

    .line 83
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private static updateSettingTable(Landroid/content/Context;Landroid/content/ContentValues;Z)I
    .locals 6
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "vals"    # Landroid/content/ContentValues;
    .param p2, "sendBroadCastforClock"    # Z

    .prologue
    .line 303
    const/4 v2, 0x0

    .line 305
    .local v2, "result":I
    if-nez p0, :cond_0

    .line 306
    const-string v3, ""

    const-string v4, "uST: context is null"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 335
    :goto_0
    return v3

    .line 310
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 311
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_4

    .line 313
    :try_start_0
    sget-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    invoke-static {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 314
    sget-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v0, v3, p1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    .line 316
    :try_start_1
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/BManager;->isCheckSupportClock(Landroid/content/Context;)Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 317
    sget-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    invoke-static {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 318
    const-string v3, "DBH UST"

    invoke-static {p0, p2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/BManager;->sendBroadCastToProvider(Landroid/content/Context;ZLjava/lang/String;)V

    :cond_1
    :goto_1
    move v3, v2

    .line 333
    goto :goto_0

    .line 320
    :cond_2
    const-string v3, ""

    const-string v4, "Content resolve is null"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 323
    :catch_0
    move-exception v1

    .line 324
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "db : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 329
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v1

    .line 330
    .restart local v1    # "e":Ljava/lang/Exception;
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 327
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_3
    :try_start_3
    const-string v3, ""

    const-string v4, "SETTING_URI==null"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 335
    :cond_4
    const/4 v3, -0x1

    goto :goto_0
.end method

.method public static updateSettings(Landroid/content/Context;Ljava/lang/String;I)I
    .locals 5
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "column"    # Ljava/lang/String;
    .param p2, "isEnable"    # I

    .prologue
    .line 433
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ud CB "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    const/4 v0, 0x0

    .line 436
    .local v0, "result":I
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 437
    .local v1, "vals":Landroid/content/ContentValues;
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 439
    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateSettingTable(Landroid/content/Context;Landroid/content/ContentValues;Z)I

    move-result v0

    .line 440
    if-lez v0, :cond_0

    .line 441
    const/4 v0, 0x1

    .line 445
    :goto_0
    return v0

    .line 443
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static updateShowUseLocationPopup(Landroid/content/Context;I)I
    .locals 5
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "isEnable"    # I

    .prologue
    .line 4102
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ud uSUL"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4103
    const/4 v0, 0x0

    .line 4104
    .local v0, "result":I
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 4105
    .local v1, "vals":Landroid/content/ContentValues;
    const-string v2, "SHOW_USE_LOCATION_POPUP"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4107
    const/4 v2, 0x1

    invoke-static {p0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateSettingTable(Landroid/content/Context;Landroid/content/ContentValues;Z)I

    move-result v0

    .line 4108
    if-lez v0, :cond_0

    .line 4109
    const/4 v0, 0x1

    .line 4113
    :goto_0
    return v0

    .line 4111
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static updateTempScale(Landroid/content/Context;I)I
    .locals 10
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "scale"    # I

    .prologue
    const/4 v9, 0x1

    .line 2452
    const-string v6, ""

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "updateTS = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2453
    const/4 v2, 0x0

    .line 2454
    .local v2, "result":I
    if-nez p0, :cond_0

    .line 2455
    const-string v6, ""

    const-string v7, "uTS: context is null"

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 2492
    .end local v2    # "result":I
    .local v3, "result":I
    :goto_0
    return v3

    .line 2458
    .end local v3    # "result":I
    .restart local v2    # "result":I
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2460
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_2

    .line 2461
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 2462
    .local v5, "vals":Landroid/content/ContentValues;
    const-string v6, "TEMP_SCALE"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2465
    :try_start_0
    sget-object v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    invoke-static {v0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2466
    sget-object v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v0, v6, v5, v7, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    .line 2468
    .local v4, "updateResult":I
    if-lez v4, :cond_3

    .line 2470
    :try_start_1
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/BManager;->isCheckSupportClock(Landroid/content/Context;)Z

    move-result v6

    if-ne v6, v9, :cond_1

    .line 2471
    sget-object v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    invoke-static {v0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2472
    const/4 v6, 0x1

    const-string v7, "DBH UTS"

    invoke-static {p0, v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/BManager;->sendBroadCastToProvider(Landroid/content/Context;ZLjava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 2478
    :cond_1
    :goto_1
    const/4 v2, 0x1

    .line 2491
    .end local v4    # "updateResult":I
    .end local v5    # "vals":Landroid/content/ContentValues;
    :cond_2
    :goto_2
    const-string v6, ""

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "result = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 2492
    .end local v2    # "result":I
    .restart local v3    # "result":I
    goto :goto_0

    .line 2475
    .end local v3    # "result":I
    .restart local v2    # "result":I
    .restart local v4    # "updateResult":I
    .restart local v5    # "vals":Landroid/content/ContentValues;
    :catch_0
    move-exception v1

    .line 2476
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v6, ""

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "db : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 2485
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v4    # "updateResult":I
    :catch_1
    move-exception v1

    .line 2486
    .restart local v1    # "e":Ljava/lang/Exception;
    const-string v6, ""

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Exception "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2487
    const/4 v2, -0x1

    goto :goto_2

    .line 2480
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v4    # "updateResult":I
    :cond_3
    const/4 v2, -0x1

    goto :goto_2

    .line 2483
    .end local v4    # "updateResult":I
    :cond_4
    :try_start_3
    const-string v6, ""

    const-string v7, "SETTING_URI==null"

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2
.end method

.method private static updateViEffectSettingTable(Landroid/content/Context;Landroid/content/ContentValues;)I
    .locals 6
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "vals"    # Landroid/content/ContentValues;

    .prologue
    .line 339
    const/4 v2, 0x0

    .line 341
    .local v2, "result":I
    if-nez p0, :cond_0

    .line 342
    const-string v3, ""

    const-string v4, "uST: context is null"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 360
    :goto_0
    return v3

    .line 346
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 347
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_2

    .line 349
    :try_start_0
    sget-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->WEATHER_VIEFFECT_URI:Landroid/net/Uri;

    invoke-static {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 350
    sget-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->WEATHER_VIEFFECT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v0, v3, p1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    :goto_1
    move v3, v2

    .line 358
    goto :goto_0

    .line 352
    :cond_1
    const-string v3, ""

    const-string v4, "SETTING_URI==null"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 354
    :catch_0
    move-exception v1

    .line 355
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 360
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_2
    const/4 v3, -0x1

    goto :goto_0
.end method

.method public static updateViEffectSettings(Landroid/content/Context;Ljava/lang/String;I)I
    .locals 5
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "column"    # Ljava/lang/String;
    .param p2, "isEnable"    # I

    .prologue
    .line 449
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "uvs CB "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 450
    const/4 v0, 0x0

    .line 452
    .local v0, "result":I
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 453
    .local v1, "vals":Landroid/content/ContentValues;
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 455
    invoke-static {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateViEffectSettingTable(Landroid/content/Context;Landroid/content/ContentValues;)I

    move-result v0

    .line 456
    if-lez v0, :cond_0

    .line 457
    const/4 v0, 0x1

    .line 461
    :goto_0
    return v0

    .line 459
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private static updateWeatherIconNum(Landroid/content/Context;Landroid/content/ContentValues;)I
    .locals 10
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "vals"    # Landroid/content/ContentValues;

    .prologue
    const/4 v9, 0x1

    const/4 v4, 0x0

    .line 4043
    if-nez p0, :cond_1

    .line 4044
    const-string v5, ""

    const-string v6, "uWI: context is null"

    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v4

    .line 4068
    :cond_0
    :goto_0
    return v3

    .line 4047
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 4048
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_3

    .line 4049
    const/4 v3, 0x0

    .line 4051
    .local v3, "result":I
    :try_start_0
    const-string v5, "Location=\"%s\""

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "cityId:current"

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 4052
    .local v2, "loc":Ljava/lang/String;
    sget-object v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    invoke-static {v0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 4053
    sget-object v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_WEATHERINFO_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    invoke-virtual {v0, v5, p1, v2, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    .line 4055
    :cond_2
    if-eqz v3, :cond_0

    .line 4056
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/BManager;->isCheckSupportClock(Landroid/content/Context;)Z

    move-result v5

    if-ne v5, v9, :cond_0

    .line 4057
    sget-object v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuContentProvider;->ACCU_DAEMON_SETTING_URI:Landroid/net/Uri;

    invoke-static {v0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkAcquireContentProviderClient(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 4058
    const/4 v5, 0x1

    const-string v6, "DBH UWIN"

    invoke-static {p0, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/BManager;->sendBroadCastToProvider(Landroid/content/Context;ZLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 4062
    .end local v2    # "loc":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 4063
    .local v1, "e":Ljava/lang/Exception;
    const-string v5, ""

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exception "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v4

    .line 4064
    goto :goto_0

    .end local v1    # "e":Ljava/lang/Exception;
    .end local v3    # "result":I
    :cond_3
    move v3, v4

    .line 4068
    goto :goto_0
.end method
