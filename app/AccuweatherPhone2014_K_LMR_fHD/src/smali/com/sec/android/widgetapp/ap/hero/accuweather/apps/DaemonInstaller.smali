.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/DaemonInstaller;
.super Ljava/lang/Object;
.source "DaemonInstaller.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/DaemonInstaller$PackageInstallObserver;
    }
.end annotation


# static fields
.field private static final APK_NAME:Ljava/lang/String; = "UnifiedDaemon.apk"

.field private static final DAEMON_APK_VERSION:F = 141212.02f

.field private static final DAEMON_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.daemonapp"


# instance fields
.field private ob:Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/DaemonInstaller$PackageInstallObserver;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 140
    return-void
.end method

.method private installPackageInAsset(Landroid/content/Context;)Z
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 71
    const-string v1, ""

    const-string v2, "iPIA"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/DaemonInstaller;->makeFile(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v0

    .line 74
    .local v0, "uri":Landroid/net/Uri;
    if-nez v0, :cond_0

    .line 75
    const/4 v1, 0x1

    .line 87
    :goto_0
    return v1

    .line 80
    :cond_0
    const-string v1, ""

    const-string v2, "iPIA - ni"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/DaemonInstaller$PackageInstallObserver;

    invoke-direct {v1, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/DaemonInstaller$PackageInstallObserver;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/DaemonInstaller$1;)V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/DaemonInstaller;->ob:Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/DaemonInstaller$PackageInstallObserver;

    .line 84
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/DaemonInstaller;->ob:Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/DaemonInstaller$PackageInstallObserver;

    const/4 v3, 0x2

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/content/pm/PackageManager;->installPackage(Landroid/net/Uri;Landroid/content/pm/IPackageInstallObserver;ILjava/lang/String;)V

    .line 87
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private makeFile(Landroid/content/Context;)Landroid/net/Uri;
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 99
    const/4 v5, 0x0

    .line 100
    .local v5, "fos":Ljava/io/FileOutputStream;
    const/4 v0, 0x0

    .line 102
    .local v0, "apkFile":Ljava/io/File;
    :try_start_0
    new-instance v4, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v7

    const-string v8, "UnifiedDaemon.apk"

    invoke-direct {v4, v7, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 103
    .local v4, "f":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 104
    const-string v7, ""

    const-string v8, "file exist"

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 108
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v7

    const-string v8, "UnifiedDaemon.apk"

    invoke-virtual {v7, v8}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v6

    .line 109
    .local v6, "is":Ljava/io/InputStream;
    const-string v7, "UnifiedDaemon.apk"

    const/4 v8, 0x1

    invoke-virtual {p1, v7, v8}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v5

    .line 110
    const/16 v7, 0x400

    new-array v1, v7, [B

    .line 111
    .local v1, "buffer":[B
    const/4 v2, 0x0

    .line 112
    .local v2, "byteRead":I
    :goto_0
    invoke-virtual {v6, v1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    const/4 v7, -0x1

    if-eq v2, v7, :cond_2

    .line 113
    const/4 v7, 0x0

    invoke-virtual {v5, v1, v7, v2}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 116
    .end local v1    # "buffer":[B
    .end local v2    # "byteRead":I
    .end local v4    # "f":Ljava/io/File;
    .end local v6    # "is":Ljava/io/InputStream;
    :catch_0
    move-exception v3

    .line 117
    .local v3, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 119
    if-eqz v5, :cond_1

    .line 121
    :try_start_2
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 127
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_1
    if-eqz v0, :cond_4

    .line 128
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v7

    .line 130
    :goto_2
    return-object v7

    .line 115
    .restart local v1    # "buffer":[B
    .restart local v2    # "byteRead":I
    .restart local v4    # "f":Ljava/io/File;
    .restart local v6    # "is":Ljava/io/InputStream;
    :cond_2
    :try_start_3
    const-string v7, "UnifiedDaemon.apk"

    invoke-virtual {p1, v7}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    .line 119
    if-eqz v5, :cond_1

    .line 121
    :try_start_4
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 122
    :catch_1
    move-exception v3

    .line 123
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 122
    .end local v1    # "buffer":[B
    .end local v2    # "byteRead":I
    .end local v4    # "f":Ljava/io/File;
    .end local v6    # "is":Ljava/io/InputStream;
    .local v3, "e":Ljava/lang/Exception;
    :catch_2
    move-exception v3

    .line 123
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 119
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    if-eqz v5, :cond_3

    .line 121
    :try_start_5
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 124
    :cond_3
    :goto_3
    throw v7

    .line 122
    :catch_3
    move-exception v3

    .line 123
    .restart local v3    # "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 130
    .end local v3    # "e":Ljava/io/IOException;
    :cond_4
    const/4 v7, 0x0

    goto :goto_2
.end method


# virtual methods
.method public checkMainWidget(Landroid/content/Context;)Z
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.sec.android.daemonapp"

    const/4 v4, 0x4

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 43
    .local v1, "pi":Landroid/content/pm/PackageInfo;
    if-eqz v1, :cond_0

    const v2, 0x4809e701

    iget-object v3, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    .line 44
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cMW: hvwbi : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/apps/DaemonInstaller;->installPackageInAsset(Landroid/content/Context;)Z

    move-result v2

    .line 61
    .end local v1    # "pi":Landroid/content/pm/PackageInfo;
    :goto_0
    return v2

    .line 48
    .restart local v1    # "pi":Landroid/content/pm/PackageInfo;
    :cond_0
    if-eqz v1, :cond_1

    .line 49
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cMW: MII : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    .end local v1    # "pi":Landroid/content/pm/PackageInfo;
    :cond_1
    :goto_1
    const/4 v2, 0x1

    goto :goto_0

    .line 51
    :catch_0
    move-exception v0

    .line 52
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 58
    const-string v2, ""

    const-string v3, "cMW: MIni"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
