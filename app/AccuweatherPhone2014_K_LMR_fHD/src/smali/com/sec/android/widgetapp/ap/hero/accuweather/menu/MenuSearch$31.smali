.class Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$31;
.super Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;
.source "MenuSearch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->performGetData(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

.field final synthetic val$cityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

.field final synthetic val$tempScaleSetting:I


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;ILcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 1471
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    iput p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$31;->val$tempScaleSetting:I

    iput-object p3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$31;->val$cityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(IILjava/lang/String;Ljava/lang/String;)V
    .locals 17
    .param p1, "position"    # I
    .param p2, "responseCode"    # I
    .param p3, "responseStatus"    # Ljava/lang/String;
    .param p4, "responseBody"    # Ljava/lang/String;

    .prologue
    .line 1474
    invoke-super/range {p0 .. p4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;->onReceive(IILjava/lang/String;Ljava/lang/String;)V

    .line 1476
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mRequestCanceled:Z
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$2600(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 1599
    :goto_0
    return-void

    .line 1479
    :cond_0
    const/16 v13, 0xc8

    move/from16 v0, p2

    if-ne v0, v13, :cond_b

    .line 1480
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->parser:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$3400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    move-object/from16 v0, p4

    invoke-virtual {v13, v0, v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->parseDetailWeatherLocationCity(Ljava/lang/String;Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    move-result-object v7

    .line 1481
    .local v7, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->parser:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$3400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;

    move-result-object v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$31;->val$tempScaleSetting:I

    .line 1483
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v15

    invoke-static/range {v15 .. v16}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v15

    .line 1481
    move-object/from16 v0, p4

    invoke-virtual {v13, v14, v0, v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->parseDetailWeather(ILjava/lang/String;Ljava/lang/String;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    move-result-object v2

    .line 1484
    .local v2, "detailInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    const/4 v12, 0x0

    .line 1485
    .local v12, "twi":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    if-eqz v2, :cond_1

    .line 1486
    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getTodayWeatherInfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v12

    .line 1488
    :cond_1
    new-instance v11, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;

    invoke-direct {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;-><init>()V

    .line 1489
    .local v11, "sinfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;
    if-eqz v12, :cond_2

    .line 1490
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getCurrentTemp()F

    move-result v13

    invoke-virtual {v11, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setCurrentTemp(F)V

    .line 1491
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDate()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setDate(Ljava/lang/String;)V

    .line 1492
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getHighTemp()F

    move-result v13

    invoke-virtual {v11, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setHighTemp(F)V

    .line 1493
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getIconNum()I

    move-result v13

    invoke-virtual {v11, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setIconNum(I)V

    .line 1494
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getLowTemp()F

    move-result v13

    invoke-virtual {v11, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setLowTemp(F)V

    .line 1495
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v13

    invoke-virtual {v11, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setTempScale(I)V

    .line 1496
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setTimeZone(Ljava/lang/String;)V

    .line 1497
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getUpdateDate()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setUpdateDate(Ljava/lang/String;)V

    .line 1498
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getUrl()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setUrl(Ljava/lang/String;)V

    .line 1499
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v13

    .line 1500
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v15

    .line 1499
    invoke-static {v13, v14, v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkDayOrNight(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    .line 1501
    .local v8, "isDay":Z
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getWeatherText()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setWeatherText(Ljava/lang/String;)V

    .line 1502
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setSunRiseTime(Ljava/lang/String;)V

    .line 1503
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setSunSetTime(Ljava/lang/String;)V

    .line 1504
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getRealFeel()F

    move-result v13

    invoke-virtual {v11, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setRealFeel(F)V

    .line 1505
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayRainProbability()I

    move-result v13

    invoke-virtual {v11, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setDayRainProbability(I)V

    .line 1506
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDaySnowProbability()I

    move-result v13

    invoke-virtual {v11, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setDaySnowProbability(I)V

    .line 1507
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayHailProbability()I

    move-result v13

    invoke-virtual {v11, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setDayHailProbability(I)V

    .line 1509
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getDayPrecipitationProbability()I

    move-result v13

    .line 1508
    invoke-virtual {v11, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setDayPrecipitationProbability(I)V

    .line 1510
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightRainProbability()I

    move-result v13

    invoke-virtual {v11, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setNightRainProbability(I)V

    .line 1511
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightSnowProbability()I

    move-result v13

    invoke-virtual {v11, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setNightSnowProbability(I)V

    .line 1512
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightHailProbability()I

    move-result v13

    invoke-virtual {v11, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setNightHailProbability(I)V

    .line 1514
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getNightPrecipitationProbability()I

    move-result v13

    .line 1513
    invoke-virtual {v11, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setNightPrecipitationProbability(I)V

    .line 1516
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getRelativeHumidity()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setRelativeHumidity(Ljava/lang/String;)V

    .line 1517
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getUVIndex()I

    move-result v13

    invoke-virtual {v11, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setUVIndex(I)V

    .line 1518
    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getUVIndexText()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;->setUVIndexText(Ljava/lang/String;)V

    .line 1520
    if-eqz v7, :cond_2

    .line 1521
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$31;->val$cityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    invoke-virtual {v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSummerTime()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setSummerTime(Ljava/lang/String;)V

    .line 1524
    .end local v8    # "isDay":Z
    :cond_2
    if-eqz v7, :cond_3

    .line 1525
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$31;->val$cityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLatitude()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setLatitude(Ljava/lang/String;)V

    .line 1526
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$31;->val$cityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLongitude()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setLongitude(Ljava/lang/String;)V

    .line 1527
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$31;->val$cityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    const/4 v14, 0x5

    invoke-virtual {v13, v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setZoomlevel(I)V

    .line 1530
    :cond_3
    new-instance v3, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    const-class v14, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-direct {v3, v13, v14}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1533
    .local v3, "detailIntent":Landroid/content/Intent;
    const-string v13, "todayInfo"

    invoke-virtual {v3, v13, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1534
    const-string v13, "cityInfo"

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$31;->val$cityInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    invoke-virtual {v3, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1536
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 1538
    .local v10, "photosInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;>;"
    if-eqz v2, :cond_4

    .line 1539
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfoSize()I

    move-result v13

    if-ge v6, v13, :cond_5

    .line 1540
    invoke-virtual {v2, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getPhotosInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;

    move-result-object v13

    invoke-virtual {v10, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1539
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 1543
    .end local v6    # "i":I
    :cond_4
    const-string v13, ""

    const-string v14, "dI is n"

    invoke-static {v13, v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1546
    :cond_5
    const-string v13, "photosInfo"

    invoke-virtual {v3, v13, v10}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 1548
    const/4 v9, 0x0

    .line 1549
    .local v9, "isResultByGPS":Z
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    const v14, 0x7f08011c

    invoke-virtual {v13, v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1551
    .local v1, "addRowState":Landroid/widget/TextView;
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v13

    .line 1552
    invoke-interface {v13}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .line 1553
    invoke-virtual {v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0d0033

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 1555
    const/4 v9, 0x1

    .line 1556
    :cond_6
    if-eqz v9, :cond_7

    .line 1557
    const-string v13, "resultbygps"

    const/4 v14, 0x1

    invoke-virtual {v3, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1560
    :cond_7
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1561
    .local v4, "forcastList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1563
    .local v5, "hourlyForcast":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;>;"
    if-eqz v2, :cond_8

    .line 1564
    const/4 v6, 0x0

    .restart local v6    # "i":I
    :goto_2
    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForcastSize()I

    move-result v13

    if-ge v6, v13, :cond_8

    .line 1565
    invoke-virtual {v2, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getForecastInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    move-result-object v13

    invoke-virtual {v4, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1564
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 1569
    .end local v6    # "i":I
    :cond_8
    const-string v13, "forcastList"

    .line 1570
    invoke-virtual {v3, v13, v4}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 1571
    const-string v13, "flags"

    const v14, 0xf22f

    invoke-virtual {v3, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1573
    if-eqz v2, :cond_9

    .line 1574
    const/4 v6, 0x0

    .restart local v6    # "i":I
    :goto_3
    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getHourSize()I

    move-result v13

    if-ge v6, v13, :cond_9

    .line 1575
    invoke-virtual {v2, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->getHourInfo(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;

    move-result-object v13

    invoke-virtual {v5, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1574
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 1578
    .end local v6    # "i":I
    :cond_9
    const-string v13, ""

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "-------hourlyForcast-------"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1579
    const-string v13, "hourlyweather"

    .line 1580
    invoke-virtual {v3, v13, v5}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 1581
    const-string v13, "flags"

    const v14, 0xf22f

    invoke-virtual {v3, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1584
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->isActivityVisible()Z

    move-result v13

    if-eqz v13, :cond_a

    .line 1585
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    const v14, 0xf22f

    invoke-virtual {v13, v3, v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1588
    :cond_a
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->hideLoadingDialog()V
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$2000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    .line 1591
    .end local v1    # "addRowState":Landroid/widget/TextView;
    .end local v2    # "detailInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .end local v3    # "detailIntent":Landroid/content/Intent;
    .end local v4    # "forcastList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;>;"
    .end local v5    # "hourlyForcast":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;>;"
    .end local v7    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .end local v9    # "isResultByGPS":Z
    .end local v10    # "photosInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;>;"
    .end local v11    # "sinfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SearchWeatherInfo;
    .end local v12    # "twi":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    :cond_b
    const/16 v13, 0xc8

    move/from16 v0, p2

    if-eq v0, v13, :cond_c

    .line 1592
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->hideLoadingDialog()V
    invoke-static {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$2000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    .line 1593
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    invoke-virtual {v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->isActivityVisible()Z

    move-result v13

    if-eqz v13, :cond_c

    .line 1594
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    const v14, 0x7f0d001f

    invoke-static {v13, v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    .line 1598
    :cond_c
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$31;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    move/from16 v0, p1

    invoke-virtual {v13, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->deleteMe(I)V

    goto/16 :goto_0
.end method
