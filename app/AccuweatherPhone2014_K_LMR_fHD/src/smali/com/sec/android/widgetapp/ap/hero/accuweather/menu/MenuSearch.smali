.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;
.super Landroid/app/ListActivity;
.source "MenuSearch.java"

# interfaces
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/model/IActivityVisibleState;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;,
        Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$SearchListAdapter;
    }
.end annotation


# instance fields
.field private COLLAB_MODE:Z

.field private cityListAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;

.field private cityListAdapter2:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;

.field private citydb:Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;

.field citynames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private ctx:Landroid/content/Context;

.field private getDataHandler:Landroid/os/Handler;

.field getLocDataHandler:Landroid/os/Handler;

.field private gps_btn:Landroid/widget/RelativeLayout;

.field private httpClient:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

.field private inputManager:Landroid/view/inputmethod/InputMethodManager;

.field private intent:Landroid/content/Intent;

.field private lstCity:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;",
            ">;"
        }
    .end annotation
.end field

.field mBackKeyHandler:Landroid/os/Handler;

.field private mClearTextBtn:Landroid/widget/ImageView;

.field private mDropBoxHeight:I

.field private mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

.field public mHttpThreads:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Thread;",
            ">;"
        }
    .end annotation
.end field

.field private mIsKeyboardShown:Z

.field private mIsLanguageHindiorBengali:Z

.field mIsVisible:Z

.field public mKeyboardHanlder:Landroid/os/Handler;

.field public mLoadingDialog:Landroid/app/Dialog;

.field private mMapInfoPostErrorHandler:Landroid/os/Handler;

.field private mMapInfoPostProcHandler:Landroid/os/Handler;

.field public mNetworkErrorDialog:Landroid/app/Dialog;

.field private mNoSearchText:Landroid/widget/TextView;

.field public mPopupDialog:Landroid/app/Dialog;

.field private volatile mRequestCanceled:Z

.field private mSearchLayout:Landroid/widget/LinearLayout;

.field private mSearchLayoutOutLine:Landroid/widget/RelativeLayout;

.field private mSearchText:Landroid/widget/TextView;

.field private mTextHighlight:Z

.field private mTextWatcher:Landroid/text/TextWatcher;

.field private mTimer:Ljava/util/Timer;

.field private mTopLayout:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;

.field mapHandler:Landroid/os/Handler;

.field private map_btn:Landroid/widget/RelativeLayout;

.field private mcl:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

.field private parser:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;

.field public performMapCancled:Z

.field point:[I

.field private refresh_toast_display:Z

.field region:I

.field private searchEditText:Landroid/widget/AutoCompleteTextView;

.field searchHandler:Landroid/os/Handler;

.field syncstamp:J

.field private tempscale:I

.field public textcount:I

.field private toastHint:Landroid/widget/Toast;

.field private urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 99
    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    .line 114
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mClearTextBtn:Landroid/widget/ImageView;

    .line 122
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mHttpThreads:Ljava/util/ArrayList;

    .line 134
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mLoadingDialog:Landroid/app/Dialog;

    .line 136
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mNetworkErrorDialog:Landroid/app/Dialog;

    .line 138
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mPopupDialog:Landroid/app/Dialog;

    .line 140
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->refresh_toast_display:Z

    .line 144
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mIsVisible:Z

    .line 146
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->textcount:I

    .line 148
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->region:I

    .line 154
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->tempscale:I

    .line 156
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->COLLAB_MODE:Z

    .line 164
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    .line 166
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mTextHighlight:Z

    .line 168
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mIsKeyboardShown:Z

    .line 170
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mTopLayout:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;

    .line 173
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mSearchLayoutOutLine:Landroid/widget/RelativeLayout;

    .line 175
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mIsLanguageHindiorBengali:Z

    .line 586
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$18;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$18;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mapHandler:Landroid/os/Handler;

    .line 601
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->performMapCancled:Z

    .line 631
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$19;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$19;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mMapInfoPostProcHandler:Landroid/os/Handler;

    .line 664
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$20;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$20;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mMapInfoPostErrorHandler:Landroid/os/Handler;

    .line 789
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$21;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$21;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mBackKeyHandler:Landroid/os/Handler;

    .line 868
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$22;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$22;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mKeyboardHanlder:Landroid/os/Handler;

    .line 1105
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$27;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getLocDataHandler:Landroid/os/Handler;

    .line 1195
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$28;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$28;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchHandler:Landroid/os/Handler;

    .line 1249
    const/4 v0, -0x2

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mDropBoxHeight:I

    .line 1266
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$29;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$29;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mTextWatcher:Landroid/text/TextWatcher;

    .line 1452
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getDataHandler:Landroid/os/Handler;

    .line 1739
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mTopLayout:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->citydb:Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;
    .param p1, "x1"    # I

    .prologue
    .line 99
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mDropBoxHeight:I

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->performMapCities()V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;
    .param p1, "x1"    # Z

    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->setKeyboardVisible(Z)V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->enableEditField()V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;ZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Z

    .prologue
    .line 99
    invoke-direct {p0, p1, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->setKeyboardVisible(ZZ)V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mTextHighlight:Z

    return v0
.end method

.method static synthetic access$1600(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getSearchData()V

    return-void
.end method

.method static synthetic access$1700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mNoSearchText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getLoc()V

    return-void
.end method

.method static synthetic access$1900(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->performClickGpsBtn(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mSearchLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->hideLoadingDialog()V

    return-void
.end method

.method static synthetic access$2100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    return-object v0
.end method

.method static synthetic access$2102(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;
    .param p1, "x1"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    return-object p1
.end method

.method static synthetic access$2200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 99
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->tempscale:I

    return v0
.end method

.method static synthetic access$2300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->ctx:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$2402(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;
    .param p1, "x1"    # Z

    .prologue
    .line 99
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mIsKeyboardShown:Z

    return p1
.end method

.method static synthetic access$2500(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getLocationInfo()V

    return-void
.end method

.method static synthetic access$2600(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mRequestCanceled:Z

    return v0
.end method

.method static synthetic access$2602(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;
    .param p1, "x1"    # Z

    .prologue
    .line 99
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mRequestCanceled:Z

    return p1
.end method

.method static synthetic access$2700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mMapInfoPostProcHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mMapInfoPostErrorHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->stopHttpThread()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Landroid/widget/AutoCompleteTextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mcl:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->hideAllDialog()V

    return-void
.end method

.method static synthetic access$3200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->lstCity:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$3202(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->lstCity:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$3300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->cityListAdapter2:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;

    return-object v0
.end method

.method static synthetic access$3302(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;)Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;
    .param p1, "x1"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->cityListAdapter2:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;

    return-object p1
.end method

.method static synthetic access$3400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->parser:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->disableEditField()V

    return-void
.end method

.method static synthetic access$3600(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->cityListAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mClearTextBtn:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    return-object v0
.end method

.method static synthetic access$3900(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mIsLanguageHindiorBengali:Z

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->setDropdownMenuHeight()V

    return-void
.end method

.method static synthetic access$4000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->httpClient:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->toastHint:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;
    .param p1, "x1"    # Landroid/widget/Toast;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->toastHint:Landroid/widget/Toast;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->gps_btn:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->refresh_toast_display:Z

    return v0
.end method

.method static synthetic access$702(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;
    .param p1, "x1"    # Z

    .prologue
    .line 99
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->refresh_toast_display:Z

    return p1
.end method

.method static synthetic access$800(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->map_btn:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getMccCode()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private disableEditField()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1058
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;

    if-eqz v0, :cond_0

    .line 1059
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setFocusable(Z)V

    .line 1060
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setFocusableInTouchMode(Z)V

    .line 1062
    :cond_0
    return-void
.end method

.method private enableEditField()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1065
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;

    if-eqz v0, :cond_0

    .line 1066
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setFocusable(Z)V

    .line 1067
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setFocusableInTouchMode(Z)V

    .line 1069
    :cond_0
    return-void
.end method

.method private getData(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;)V
    .locals 1
    .param p1, "cityInfo"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    .prologue
    .line 1443
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->isNetWorkConnected(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1444
    const v0, 0x7f0d0020

    invoke-static {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    .line 1450
    :goto_0
    return-void

    .line 1448
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mRequestCanceled:Z

    .line 1449
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->performGetData(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;)V

    goto :goto_0
.end method

.method private getLoc()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1072
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->hideAllDialog()V

    .line 1074
    const-string v0, "location"

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/location/LocationManager;

    .line 1076
    .local v7, "locationManager":Landroid/location/LocationManager;
    const-string v0, "gps"

    invoke-virtual {v7, v0}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "network"

    .line 1077
    invoke-virtual {v7, v0}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1078
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->showLocatingDialog()V

    .line 1081
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mcl:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getLocDataHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    move v4, v2

    move v5, v2

    move v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->performGetCurrentLocation(Landroid/os/Handler;IZZZZ)V

    .line 1082
    return-void
.end method

.method private getLocationInfo()V
    .locals 2

    .prologue
    .line 1085
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090011

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1086
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->ctx:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->isAgreeUseLocation(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1087
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->ctx:Landroid/content/Context;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$26;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$26;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->showUseCurrentLocation(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)Landroid/app/Dialog;

    .line 1104
    :goto_0
    return-void

    .line 1098
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->gps_btn:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->performClickGpsBtn(Landroid/view/View;)V

    goto :goto_0

    .line 1101
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->gps_btn:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->performClickGpsBtn(Landroid/view/View;)V

    goto :goto_0
.end method

.method private getMccCode()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x3

    .line 681
    const-string v2, "mcc"

    const-string v3, "@@@"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 682
    const-string v2, "phone"

    .line 683
    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 684
    .local v0, "manager":Landroid/telephony/TelephonyManager;
    const-string v2, "mcc"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "code = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 685
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v1

    .line 686
    .local v1, "mcc":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v5, :cond_0

    .line 687
    const/4 v2, 0x0

    invoke-virtual {v1, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 688
    :cond_0
    return-object v1
.end method

.method private getSearchData()V
    .locals 1

    .prologue
    .line 1385
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->isNetWorkConnected(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1386
    const v0, 0x7f0d0020

    invoke-static {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    .line 1404
    :goto_0
    return-void

    .line 1389
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 1390
    const v0, 0x7f0d006e

    invoke-static {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    goto :goto_0

    .line 1396
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->performSearch()V

    goto :goto_0
.end method

.method private hideAllDialog()V
    .locals 1

    .prologue
    .line 941
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->hideLoadingDialog()V

    .line 942
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mNetworkErrorDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 944
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mNetworkErrorDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 949
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mPopupDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    .line 951
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mPopupDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 955
    :cond_1
    :goto_1
    return-void

    .line 952
    :catch_0
    move-exception v0

    goto :goto_1

    .line 945
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method private hideLoadingDialog()V
    .locals 3

    .prologue
    .line 1033
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->enableEditField()V

    .line 1034
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mLoadingDialog:Landroid/app/Dialog;

    if-eqz v1, :cond_0

    .line 1036
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1041
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mLoadingDialog:Landroid/app/Dialog;

    .line 1043
    :cond_0
    return-void

    .line 1037
    :catch_0
    move-exception v0

    .line 1039
    .local v0, "ex":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MenuAdd.hideLoadingDialog() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private performClickGpsBtn(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 553
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mPopupDialog:Landroid/app/Dialog;

    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$16;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$16;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    new-instance v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$17;

    invoke-direct {v4, p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$17;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;Landroid/view/View;)V

    const/4 v5, 0x0

    iget-boolean v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->COLLAB_MODE:Z

    move-object v0, p0

    move-object v1, p1

    invoke-static/range {v0 .. v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->performClickGpsBtn(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/LocationGetter;Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;Landroid/content/DialogInterface$OnDismissListener;Z)Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mPopupDialog:Landroid/app/Dialog;

    .line 584
    return-void
.end method

.method private performGetData(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;)V
    .locals 9
    .param p1, "cityInfo"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    .prologue
    .line 1455
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeHeader()Lorg/apache/http/message/HeaderGroup;

    move-result-object v4

    .line 1456
    .local v4, "headerGroup":Lorg/apache/http/message/HeaderGroup;
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getTempScaleSetting(Landroid/content/Context;)I

    move-result v8

    .line 1457
    .local v8, "tempScaleSetting":I
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v6

    .line 1458
    .local v6, "location":Ljava/lang/String;
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " performGD tempSS= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1459
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    .line 1460
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkLanguage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 1459
    invoke-virtual {v0, v6, v8, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeUrlForGetDetailData(Ljava/lang/String;ILjava/lang/String;)Ljava/net/URL;

    move-result-object v3

    .line 1462
    .local v3, "url":Ljava/net/URL;
    if-eqz v3, :cond_2

    .line 1463
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->isNetWorkConnected(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1464
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->showLoadingDialog()V

    .line 1467
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mHttpThreads:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 1468
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mHttpThreads:Ljava/util/ArrayList;

    .line 1470
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->httpClient:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$31;

    invoke-direct {v5, p0, v8, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$31;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;ILcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;)V

    move-object v2, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->get(ILandroid/content/Context;Ljava/net/URL;Lorg/apache/http/message/HeaderGroup;Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;)Ljava/lang/Thread;

    move-result-object v7

    .line 1601
    .local v7, "t":Ljava/lang/Thread;
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1603
    .end local v7    # "t":Ljava/lang/Thread;
    :cond_2
    return-void
.end method

.method private performMapCities()V
    .locals 2

    .prologue
    .line 608
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->isNetWorkConnected(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 609
    const v0, 0x7f0d0020

    invoke-static {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    .line 623
    :goto_0
    return-void

    .line 613
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mapHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 614
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mapHandler:Landroid/os/Handler;

    const v1, -0x13d30

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 616
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->performMapCancled:Z

    .line 618
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->stopHttpThread()V

    .line 619
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->showLoadingDialog()V

    .line 621
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->setupGetMapInfoManager()V

    .line 622
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->performMapCities()V

    goto :goto_0
.end method

.method private performSearch()V
    .locals 8

    .prologue
    .line 1407
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeHeader()Lorg/apache/http/message/HeaderGroup;

    move-result-object v4

    .line 1408
    .local v4, "headerGroup":Lorg/apache/http/message/HeaderGroup;
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1409
    .local v6, "parameter":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    .line 1410
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkLanguage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 1409
    invoke-virtual {v0, v6, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeUrlForGetTextSearching(Ljava/lang/String;Ljava/lang/String;)Ljava/net/URL;

    move-result-object v3

    .line 1412
    .local v3, "url":Ljava/net/URL;
    if-eqz v3, :cond_2

    .line 1413
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->isNetWorkConnected(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1414
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->showLoadingDialog()V

    .line 1417
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mHttpThreads:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 1418
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mHttpThreads:Ljava/util/ArrayList;

    .line 1420
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->httpClient:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$30;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$30;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    move-object v2, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->get(ILandroid/content/Context;Ljava/net/URL;Lorg/apache/http/message/HeaderGroup;Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;)Ljava/lang/Thread;

    move-result-object v7

    .line 1438
    .local v7, "t":Ljava/lang/Thread;
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1440
    .end local v7    # "t":Ljava/lang/Thread;
    :cond_2
    return-void
.end method

.method private setDropdownMenuHeight()V
    .locals 4

    .prologue
    .line 1252
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DBH = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mDropBoxHeight:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1253
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mDropBoxHeight:I

    if-gtz v1, :cond_0

    .line 1254
    const/4 v1, -0x2

    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mDropBoxHeight:I

    .line 1255
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mDropBoxHeight:I

    invoke-virtual {v1, v2}, Landroid/widget/AutoCompleteTextView;->setDropDownHeight(I)V

    .line 1264
    :goto_0
    return-void

    .line 1257
    :cond_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->ctx:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 1258
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mDropBoxHeight:I

    invoke-virtual {v1, v2}, Landroid/widget/AutoCompleteTextView;->setDropDownHeight(I)V

    goto :goto_0

    .line 1260
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->ctx:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f0b0000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v1

    double-to-int v0, v1

    .line 1261
    .local v0, "margin":I
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mDropBoxHeight:I

    sub-int/2addr v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/AutoCompleteTextView;->setDropDownHeight(I)V

    goto :goto_0
.end method

.method private setKeyboardVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 878
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->setKeyboardVisible(ZZ)V

    .line 879
    return-void
.end method

.method private setKeyboardVisible(ZZ)V
    .locals 4
    .param p1, "visible"    # Z
    .param p2, "isIgnoreDevice"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 882
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->inputManager:Landroid/view/inputmethod/InputMethodManager;

    invoke-static {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkKeyboard(Landroid/view/inputmethod/InputMethodManager;Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v3, :cond_0

    if-nez p2, :cond_0

    .line 883
    const/4 p1, 0x0

    .line 886
    :cond_0
    if-ne p1, v3, :cond_2

    .line 887
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;

    if-eqz v0, :cond_1

    .line 888
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->inputManager:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 889
    iput-boolean v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mIsKeyboardShown:Z

    .line 890
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mSearchText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a006d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHintTextColor(I)V

    .line 899
    :cond_1
    :goto_0
    return-void

    .line 893
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;

    if-eqz v0, :cond_1

    .line 894
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->inputManager:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 895
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mIsKeyboardShown:Z

    .line 896
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mSearchText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a006c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHintTextColor(I)V

    goto :goto_0
.end method

.method private setRTLEditText()V
    .locals 12

    .prologue
    const/16 v11, 0xb

    const/16 v10, 0xf

    const/4 v9, -0x1

    const/4 v8, -0x2

    .line 178
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Landroid/widget/AutoCompleteTextView;->setTextDirection(I)V

    .line 179
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;

    const/16 v7, 0x15

    invoke-virtual {v6, v7}, Landroid/widget/AutoCompleteTextView;->setGravity(I)V

    .line 180
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v6}, Landroid/widget/AutoCompleteTextView;->getPaddingLeft()I

    move-result v0

    .line 181
    .local v0, "left":I
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v6}, Landroid/widget/AutoCompleteTextView;->getPaddingRight()I

    move-result v4

    .line 184
    .local v4, "right":I
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v8, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 187
    .local v1, "params":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v1, v11, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 188
    invoke-virtual {v1, v10, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 189
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b0016

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v6

    iput v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 190
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b0015

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 191
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mSearchLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 194
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v8, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 197
    .local v3, "paramsIcon":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v3, v11, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 198
    invoke-virtual {v3, v10, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 199
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b0014

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v6

    iput v6, v3, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 201
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mSearchLayoutOutLine:Landroid/widget/RelativeLayout;

    const v7, 0x7f080109

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 202
    .local v5, "v":Landroid/widget/ImageView;
    invoke-virtual {v5, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 204
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mSearchLayoutOutLine:Landroid/widget/RelativeLayout;

    invoke-virtual {v6, v5}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 205
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mSearchLayoutOutLine:Landroid/widget/RelativeLayout;

    invoke-virtual {v6, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 207
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v8, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 210
    .local v2, "params2":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v6, 0x9

    invoke-virtual {v2, v6, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 211
    invoke-virtual {v2, v10, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 212
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mClearTextBtn:Landroid/widget/ImageView;

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 213
    return-void
.end method

.method private setSeachBtnController()V
    .locals 2

    .prologue
    .line 928
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->gps_btn:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$23;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$23;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 938
    return-void
.end method

.method private setupGetMapInfoManager()V
    .locals 2

    .prologue
    .line 626
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->ctx:Landroid/content/Context;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->tempscale:I

    invoke-static {v0, p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->getInstance(Landroid/content/Context;Landroid/app/Activity;I)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    .line 627
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mMapInfoPostProcHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->setPostProcHandler(Landroid/os/Handler;)V

    .line 628
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mMapInfoPostErrorHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->setErrorHandler(Landroid/os/Handler;)V

    .line 629
    return-void
.end method

.method private showLoadingDialog()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 958
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 960
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->disableEditField()V

    .line 961
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mLoadingDialog:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 962
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mRequestCanceled:Z

    .line 963
    const/16 v0, 0x3ef

    invoke-static {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->showDialog(Landroid/content/Context;I)Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mLoadingDialog:Landroid/app/Dialog;

    .line 965
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mLoadingDialog:Landroid/app/Dialog;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$24;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$24;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 998
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 999
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 1000
    return-void
.end method

.method private showSoftKeyboard(Z)V
    .locals 4
    .param p1, "isShowing"    # Z

    .prologue
    .line 536
    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->inputManager:Landroid/view/inputmethod/InputMethodManager;

    invoke-static {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkKeyboard(Landroid/view/inputmethod/InputMethodManager;Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 544
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 545
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 546
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mKeyboardHanlder:Landroid/os/Handler;

    const-wide/16 v2, 0x258

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 550
    .end local v0    # "msg":Landroid/os/Message;
    :goto_0
    return-void

    .line 548
    :cond_0
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->setKeyboardVisible(Z)V

    goto :goto_0
.end method

.method private stopHttpThread()V
    .locals 3

    .prologue
    .line 1046
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mHttpThreads:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 1047
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 1048
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Thread;

    .line 1049
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 1051
    .end local v0    # "t":Ljava/lang/Thread;
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mHttpThreads:Ljava/util/ArrayList;

    .line 1053
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mapHandler:Landroid/os/Handler;

    if-eqz v1, :cond_2

    .line 1054
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mapHandler:Landroid/os/Handler;

    const v2, -0x140b4

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1055
    :cond_2
    return-void
.end method


# virtual methods
.method public cleanResource()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 809
    const-string v0, ""

    const-string v1, "===stopHttpThread==="

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 810
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->stopHttpThread()V

    .line 812
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->lstCity:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 813
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->lstCity:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 814
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->lstCity:Ljava/util/ArrayList;

    .line 817
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->cityListAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;

    if-eqz v0, :cond_1

    .line 818
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->cityListAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;->clear()V

    .line 819
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->cityListAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;

    .line 822
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->cityListAdapter2:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;

    if-eqz v0, :cond_2

    .line 823
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->cityListAdapter2:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;->clear()V

    .line 824
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->cityListAdapter2:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;

    .line 827
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->citynames:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 828
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->citynames:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 829
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->citynames:Ljava/util/ArrayList;

    .line 832
    :cond_3
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->httpClient:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    .line 833
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->citydb:Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;

    .line 834
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->intent:Landroid/content/Intent;

    .line 835
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    .line 836
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->parser:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;

    .line 838
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mLoadingDialog:Landroid/app/Dialog;

    .line 839
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mNetworkErrorDialog:Landroid/app/Dialog;

    .line 840
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mPopupDialog:Landroid/app/Dialog;

    .line 842
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->gps_btn:Landroid/widget/RelativeLayout;

    .line 843
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->map_btn:Landroid/widget/RelativeLayout;

    .line 844
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mTextWatcher:Landroid/text/TextWatcher;

    .line 845
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;

    .line 846
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mSearchLayout:Landroid/widget/LinearLayout;

    .line 847
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mSearchText:Landroid/widget/TextView;

    .line 848
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mNoSearchText:Landroid/widget/TextView;

    .line 850
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mcl:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    .line 852
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    if-eqz v0, :cond_4

    .line 853
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->clearResource()V

    .line 854
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    .line 857
    :cond_4
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mTopLayout:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;

    if-eqz v0, :cond_5

    .line 858
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mTopLayout:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;->setHandler(Landroid/os/Handler;I)V

    .line 859
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mTopLayout:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;

    .line 862
    :cond_5
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mBackKeyHandler:Landroid/os/Handler;

    if-eqz v0, :cond_6

    .line 863
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mBackKeyHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 864
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mBackKeyHandler:Landroid/os/Handler;

    .line 866
    :cond_6
    return-void
.end method

.method public deleteMe(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 1686
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mHttpThreads:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 1687
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 1688
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1690
    :cond_0
    return-void
.end method

.method public isActivityVisible()Z
    .locals 1

    .prologue
    .line 692
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mIsVisible:Z

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 10
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x1

    const/16 v9, -0x3e7

    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v6, -0x1

    .line 1606
    invoke-super {p0, p1, p2, p3}, Landroid/app/ListActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1608
    const/16 v3, 0x3e7

    if-ne v3, p2, :cond_1

    .line 1609
    const/16 v3, 0x3e7

    invoke-virtual {p0, v3, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->setResult(ILandroid/content/Intent;)V

    .line 1610
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->finish()V

    .line 1683
    :cond_0
    :goto_0
    return-void

    .line 1614
    :cond_1
    const v3, 0xf22f

    if-ne p1, v3, :cond_6

    .line 1615
    const-string v3, "@@@"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "reqcode ="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1616
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getRegisteredCityCount(Landroid/content/Context;)I

    move-result v3

    if-nez v3, :cond_4

    .line 1617
    invoke-virtual {p0, v6, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->setResult(ILandroid/content/Intent;)V

    .line 1618
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->finish()V

    .line 1651
    :cond_2
    :goto_1
    const/16 v3, 0x12c

    if-ne p2, v3, :cond_0

    .line 1652
    const/4 v1, 0x0

    .line 1653
    .local v1, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    if-eqz p3, :cond_3

    .line 1654
    const-string v3, "cityinfo"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    .end local v1    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    check-cast v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    .line 1655
    .restart local v1    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    :cond_3
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    invoke-direct {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;-><init>()V

    .line 1657
    .local v0, "cityInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    if-eqz v1, :cond_0

    .line 1658
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLocation()Ljava/lang/String;

    move-result-object v2

    .line 1660
    .local v2, "location":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getCity()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setCity(Ljava/lang/String;)V

    .line 1661
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLatitude()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setLatitude(Ljava/lang/String;)V

    .line 1662
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLongitude()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setLongitude(Ljava/lang/String;)V

    .line 1663
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getState()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setState(Ljava/lang/String;)V

    .line 1665
    invoke-virtual {v0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setProvider(I)V

    .line 1666
    invoke-virtual {v0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setLocation(Ljava/lang/String;)V

    .line 1667
    invoke-virtual {v0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setRealLocation(Ljava/lang/String;)V

    .line 1669
    invoke-static {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->isRegisteredToCityList(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 1670
    const v3, 0x7f0d0022

    invoke-static {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    goto :goto_0

    .line 1620
    .end local v0    # "cityInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .end local v1    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    .end local v2    # "location":Ljava/lang/String;
    :cond_4
    if-ne p2, v6, :cond_5

    .line 1621
    invoke-virtual {p0, v6, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->setResult(ILandroid/content/Intent;)V

    .line 1622
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->finish()V

    goto :goto_1

    .line 1624
    :cond_5
    invoke-direct {p0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->setKeyboardVisible(Z)V

    goto :goto_1

    .line 1627
    :cond_6
    const/16 v3, 0x3070

    if-ne p1, v3, :cond_b

    .line 1628
    const-string v3, ""

    const-string v4, "come in "

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1629
    if-ne p2, v6, :cond_7

    .line 1630
    invoke-virtual {p0, v6, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->setResult(ILandroid/content/Intent;)V

    .line 1631
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->finish()V

    goto :goto_1

    .line 1633
    :cond_7
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->hideLoadingDialog()V

    .line 1634
    iput-boolean v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->performMapCancled:Z

    .line 1635
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    if-eqz v3, :cond_8

    .line 1636
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->clearResource()V

    .line 1637
    iput-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    .line 1639
    :cond_8
    iput-boolean v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mRequestCanceled:Z

    .line 1640
    if-nez p3, :cond_a

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->cityListAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;->getCount()I

    move-result v3

    if-nez v3, :cond_a

    .line 1641
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;

    if-eqz v3, :cond_9

    .line 1642
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v3, v5}, Landroid/widget/AutoCompleteTextView;->setFocusable(Z)V

    .line 1643
    :cond_9
    invoke-direct {p0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->showSoftKeyboard(Z)V

    goto/16 :goto_1

    .line 1645
    :cond_a
    invoke-direct {p0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->setKeyboardVisible(Z)V

    goto/16 :goto_1

    .line 1647
    :cond_b
    const/16 v3, 0x4b3

    if-ne v3, p1, :cond_2

    .line 1648
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->ctx:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getLocationservice(Landroid/content/Context;)I

    move-result v3

    const/16 v4, 0xbbe

    if-ne v3, v4, :cond_2

    .line 1649
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getLocationInfo()V

    goto/16 :goto_1

    .line 1672
    .restart local v0    # "cityInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .restart local v1    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    .restart local v2    # "location":Ljava/lang/String;
    :cond_c
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getIntent()Landroid/content/Intent;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->intent:Landroid/content/Intent;

    .line 1673
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->intent:Landroid/content/Intent;

    const-string v4, "flags"

    invoke-virtual {v3, v4, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const/16 v4, 0x7cf

    if-ne v3, v4, :cond_d

    .line 1674
    invoke-static {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateLastSelectedLocation(Landroid/content/Context;Ljava/lang/String;)I

    .line 1675
    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getData(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;)V

    goto/16 :goto_0

    .line 1676
    :cond_d
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->intent:Landroid/content/Intent;

    const-string v4, "flags"

    invoke-virtual {v3, v4, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const/16 v4, 0x2edf

    if-ne v3, v4, :cond_0

    .line 1677
    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getData(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;)V

    goto/16 :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 785
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 786
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "bundel"    # Landroid/os/Bundle;

    .prologue
    const v4, 0x7f030033

    const v8, 0x7f0d0090

    const v7, 0x7f0d0089

    const/4 v6, 0x1

    const v5, 0x7f0d008a

    .line 217
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 219
    const v2, 0x7f030032

    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->setContentView(I)V

    .line 220
    invoke-virtual {p0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->setActivityVisibleState(Z)V

    .line 221
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->setContext(Landroid/content/Context;)V

    .line 222
    iput-object p0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->ctx:Landroid/content/Context;

    .line 224
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->ctx:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09000f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mTextHighlight:Z

    .line 226
    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    invoke-direct {v2, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mcl:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    .line 227
    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    invoke-direct {v2, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    .line 228
    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;

    invoke-direct {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;-><init>()V

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->parser:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;

    .line 229
    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    invoke-direct {v2, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->httpClient:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    .line 231
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->ctx:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getTempScaleSetting(Landroid/content/Context;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->tempscale:I

    .line 233
    const v2, 0x7f08011a

    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mNoSearchText:Landroid/widget/TextView;

    .line 234
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->lstCity:Ljava/util/ArrayList;

    .line 235
    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->lstCity:Ljava/util/ArrayList;

    invoke-direct {v2, p0, v4, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->cityListAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;

    .line 236
    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->lstCity:Ljava/util/ArrayList;

    invoke-direct {v2, p0, v4, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->cityListAdapter2:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;

    .line 237
    const v2, 0x7f080116

    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/AutoCompleteTextView;

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;

    .line 238
    const v2, 0x7f08010b

    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mSearchLayout:Landroid/widget/LinearLayout;

    .line 239
    const v2, 0x7f08010c

    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mSearchText:Landroid/widget/TextView;

    .line 240
    const v2, 0x7f080118

    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->gps_btn:Landroid/widget/RelativeLayout;

    .line 241
    const v2, 0x7f080119

    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->map_btn:Landroid/widget/RelativeLayout;

    .line 242
    const v2, 0x7f080117

    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mClearTextBtn:Landroid/widget/ImageView;

    .line 245
    const v2, 0x7f08000c

    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mTopLayout:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;

    .line 246
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mTopLayout:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mBackKeyHandler:Landroid/os/Handler;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;->setHandler(Landroid/os/Handler;I)V

    .line 248
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mTopLayout:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$1;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    invoke-virtual {v2, v3}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 272
    const v2, 0x7f080115

    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mSearchLayoutOutLine:Landroid/widget/RelativeLayout;

    .line 274
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v2}, Landroid/widget/AutoCompleteTextView;->setSingleLine()V

    .line 275
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->cityListAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;

    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 276
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->setSeachBtnController()V

    .line 278
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v2, v3}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 281
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;

    const v3, 0x7f0d0025

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/AutoCompleteTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 283
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->ctx:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 285
    .local v0, "locale":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->ctx:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090016

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 286
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->gps_btn:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 287
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->map_btn:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 300
    :goto_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->gps_btn:Landroid/widget/RelativeLayout;

    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$2;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 311
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->map_btn:Landroid/widget/RelativeLayout;

    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$3;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$3;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 323
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->gps_btn:Landroid/widget/RelativeLayout;

    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$4;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$4;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 335
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->map_btn:Landroid/widget/RelativeLayout;

    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$5;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$5;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 347
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getListView()Landroid/widget/ListView;

    move-result-object v2

    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$6;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$6;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 371
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->map_btn:Landroid/widget/RelativeLayout;

    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$7;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$7;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 401
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->map_btn:Landroid/widget/RelativeLayout;

    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$8;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$8;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 412
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->gps_btn:Landroid/widget/RelativeLayout;

    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$9;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$9;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 423
    const-string v2, "input_method"

    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/inputmethod/InputMethodManager;

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->inputManager:Landroid/view/inputmethod/InputMethodManager;

    .line 425
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getSystemResolution(Landroid/content/Context;)I

    move-result v1

    .line 427
    .local v1, "resolution":I
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090004

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 428
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mSearchText:Landroid/widget/TextView;

    const v3, 0x7f0d0107

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setHint(I)V

    .line 435
    :goto_1
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mSearchText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a006b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setHintTextColor(I)V

    .line 436
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v2}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v2

    if-ne v2, v6, :cond_0

    .line 437
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->setRTLEditText()V

    .line 439
    :cond_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;

    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$10;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$10;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    invoke-virtual {v2, v3}, Landroid/widget/AutoCompleteTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 445
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;

    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$11;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$11;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    invoke-virtual {v2, v3}, Landroid/widget/AutoCompleteTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 461
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;

    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$12;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$12;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    invoke-virtual {v2, v3}, Landroid/widget/AutoCompleteTextView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 478
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;

    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$13;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$13;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    invoke-virtual {v2, v3}, Landroid/widget/AutoCompleteTextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 493
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;

    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$14;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$14;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    invoke-virtual {v2, v3}, Landroid/widget/AutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 513
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mClearTextBtn:Landroid/widget/ImageView;

    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$15;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$15;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 523
    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;

    invoke-direct {v2, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->citydb:Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;

    .line 525
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getRegisteredCityCount(Landroid/content/Context;)I

    move-result v2

    if-nez v2, :cond_1

    .line 526
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getLocationInfo()V

    .line 528
    :cond_1
    invoke-direct {p0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->showSoftKeyboard(Z)V

    .line 530
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->ctx:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->isLanguageHindiorBengali(Landroid/content/Context;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mIsLanguageHindiorBengali:Z

    .line 532
    return-void

    .line 289
    .end local v1    # "resolution":I
    :cond_2
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "fr"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 290
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->gps_btn:Landroid/widget/RelativeLayout;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 291
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->map_btn:Landroid/widget/RelativeLayout;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 294
    :cond_3
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->gps_btn:Landroid/widget/RelativeLayout;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 295
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->map_btn:Landroid/widget/RelativeLayout;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 430
    .restart local v1    # "resolution":I
    :cond_4
    const v2, 0xfa000

    if-ne v1, v2, :cond_5

    .line 431
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mSearchText:Landroid/widget/TextView;

    const-string v3, " "

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    .line 432
    :cond_5
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mSearchText:Landroid/widget/TextView;

    const v3, 0x7f0d0005

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setHint(I)V

    goto/16 :goto_1
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 772
    invoke-super {p0}, Landroid/app/ListActivity;->onDestroy()V

    .line 773
    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->setActivityVisibleState(Z)V

    .line 774
    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->setKeyboardVisible(Z)V

    .line 776
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 777
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 780
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->cleanResource()V

    .line 782
    return-void
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 8
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 902
    invoke-super/range {p0 .. p5}, Landroid/app/ListActivity;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    .line 903
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->cityListAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;

    invoke-virtual {v5, p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$MenuAddAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    .line 904
    .local v1, "cityInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v4

    .line 906
    .local v4, "location":Ljava/lang/String;
    const/4 v3, 0x0

    .line 907
    .local v3, "isResultByGPS":Z
    const v5, 0x7f08011c

    invoke-virtual {p0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 908
    .local v0, "addRowState":Landroid/widget/TextView;
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0d0033

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 910
    const/4 v3, 0x1

    .line 912
    :cond_0
    invoke-static {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->isRegisteredToCityList(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    if-nez v3, :cond_1

    .line 913
    new-instance v2, Landroid/content/Intent;

    const-class v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/WeatherClockDetail;

    invoke-direct {v2, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 914
    .local v2, "detailIntent":Landroid/content/Intent;
    const/high16 v5, 0x24000000

    invoke-virtual {v2, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 916
    const-string v5, "searchlocation"

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 918
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateLastSelectedLocation(Landroid/content/Context;Ljava/lang/String;)I

    .line 919
    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->startActivity(Landroid/content/Intent;)V

    .line 920
    const/4 v5, -0x1

    invoke-virtual {p0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->setResult(I)V

    .line 921
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->finish()V

    .line 925
    .end local v2    # "detailIntent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 923
    :cond_1
    invoke-direct {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getData(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 707
    invoke-super {p0}, Landroid/app/ListActivity;->onPause()V

    .line 708
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->stopHttpThread()V

    .line 709
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getDataHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 710
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getDataHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 713
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getLocDataHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 714
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getLocDataHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 717
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mapHandler:Landroid/os/Handler;

    if-eqz v0, :cond_2

    .line 718
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mapHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 721
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchHandler:Landroid/os/Handler;

    if-eqz v0, :cond_3

    .line 722
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 725
    :cond_3
    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->setActivityVisibleState(Z)V

    .line 727
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setSaveEnabled(Z)V

    .line 728
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->toastHint:Landroid/widget/Toast;

    if-eqz v0, :cond_4

    .line 729
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->toastHint:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 731
    :cond_4
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mPopupDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mPopupDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mPopupDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_6

    .line 732
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->hideAllDialog()V

    .line 735
    :cond_6
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->hideLoadingDialog()V

    .line 736
    return-void
.end method

.method protected onResume()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 739
    invoke-super {p0}, Landroid/app/ListActivity;->onResume()V

    .line 741
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "LAUNCH_MODE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 742
    .local v0, "LAUNCH_MODE":Ljava/lang/String;
    if-eqz v0, :cond_2

    const-string v1, "COLLAB_MODE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 743
    iput-boolean v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->COLLAB_MODE:Z

    .line 747
    :goto_0
    iget-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->COLLAB_MODE:Z

    if-eqz v1, :cond_0

    .line 748
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->map_btn:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_0

    .line 749
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->map_btn:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 753
    :cond_0
    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->citydb:Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;

    .line 755
    invoke-virtual {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->setActivityVisibleState(Z)V

    .line 757
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-lez v1, :cond_3

    .line 758
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mClearTextBtn:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 762
    :goto_1
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->setContext(Landroid/content/Context;)V

    .line 764
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    if-eqz v1, :cond_1

    .line 765
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->clearResource()V

    .line 766
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    .line 769
    :cond_1
    return-void

    .line 745
    :cond_2
    iput-boolean v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->COLLAB_MODE:Z

    goto :goto_0

    .line 760
    :cond_3
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mClearTextBtn:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method public onSearchRequested()Z
    .locals 2

    .prologue
    .line 701
    const-string v0, ""

    const-string v1, "MS_oSR"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 702
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->requestFocus()Z

    .line 703
    invoke-super {p0}, Landroid/app/ListActivity;->onSearchRequested()Z

    move-result v0

    return v0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 3
    .param p1, "hasFocus"    # Z

    .prologue
    .line 798
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onWindowFocusChanged(Z)V

    .line 800
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "oWFC : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v2}, Landroid/widget/AutoCompleteTextView;->isFocusable()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mIsKeyboardShown:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 802
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->isFocusable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mIsKeyboardShown:Z

    if-eqz v0, :cond_0

    .line 803
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->setKeyboardVisible(Z)V

    .line 805
    :cond_0
    return-void
.end method

.method public setActivityVisibleState(Z)V
    .locals 0
    .param p1, "onoff"    # Z

    .prologue
    .line 696
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mIsVisible:Z

    .line 697
    return-void
.end method

.method public showLocatingDialog()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1003
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 1004
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->disableEditField()V

    .line 1005
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mLoadingDialog:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 1006
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mRequestCanceled:Z

    .line 1007
    const/4 v0, 0x0

    const v1, 0x7f0d0038

    .line 1008
    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1007
    invoke-static {p0, v0, v1, v3, v3}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mLoadingDialog:Landroid/app/Dialog;

    .line 1010
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mLoadingDialog:Landroid/app/Dialog;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$25;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$25;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1028
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 1029
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 1030
    return-void
.end method
