.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/CustMaskView;
.super Landroid/widget/ImageView;
.source "CustMaskView.java"


# instance fields
.field private mMaskImg:Landroid/graphics/Bitmap;

.field private final mPaint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 19
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/CustMaskView;->mPaint:Landroid/graphics/Paint;

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/CustMaskView;->mMaskImg:Landroid/graphics/Bitmap;

    .line 27
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/CustMaskView;->mContext:Landroid/content/Context;

    .line 29
    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/CustMaskView;->init(Landroid/content/Context;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/CustMaskView;->mPaint:Landroid/graphics/Paint;

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/CustMaskView;->mMaskImg:Landroid/graphics/Bitmap;

    .line 45
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/CustMaskView;->mContext:Landroid/content/Context;

    .line 47
    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/CustMaskView;->init(Landroid/content/Context;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 19
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/CustMaskView;->mPaint:Landroid/graphics/Paint;

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/CustMaskView;->mMaskImg:Landroid/graphics/Bitmap;

    .line 36
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/CustMaskView;->mContext:Landroid/content/Context;

    .line 38
    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/CustMaskView;->init(Landroid/content/Context;)V

    .line 39
    return-void
.end method


# virtual methods
.method public clearView()V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/CustMaskView;->mMaskImg:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/CustMaskView;->mMaskImg:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 78
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/CustMaskView;->mMaskImg:Landroid/graphics/Bitmap;

    .line 80
    :cond_0
    return-void
.end method

.method public init(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/CustMaskView;->mPaint:Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/PorterDuffXfermode;

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v2, v3}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 53
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 54
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f020151

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/CustMaskView;->mMaskImg:Landroid/graphics/Bitmap;

    .line 58
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v4, 0x0

    .line 61
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 63
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/CustMaskView;->mMaskImg:Landroid/graphics/Bitmap;

    if-nez v1, :cond_0

    .line 73
    :goto_0
    return-void

    .line 66
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 69
    new-instance v0, Landroid/graphics/NinePatch;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/CustMaskView;->mMaskImg:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/CustMaskView;->mMaskImg:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getNinePatchChunk()[B

    move-result-object v2

    const-string v3, ""

    invoke-direct {v0, v1, v2, v3}, Landroid/graphics/NinePatch;-><init>(Landroid/graphics/Bitmap;[BLjava/lang/String;)V

    .line 70
    .local v0, "sc":Landroid/graphics/NinePatch;
    new-instance v1, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v3

    invoke-direct {v1, v4, v4, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/CustMaskView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, v1, v2}, Landroid/graphics/NinePatch;->draw(Landroid/graphics/Canvas;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 72
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0
.end method
