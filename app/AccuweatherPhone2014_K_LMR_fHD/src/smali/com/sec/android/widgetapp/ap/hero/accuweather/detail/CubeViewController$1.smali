.class Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$1;
.super Ljava/lang/Object;
.source "CubeViewController.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 93
    const-string v0, ""

    const-string v1, "AL scr end"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;

    invoke-interface {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;->onFinshAnimation()V

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mIsRunningAni:Z

    .line 99
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    .line 100
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->getMPState()I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    .line 101
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->getMPState()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 102
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->setMPState(I)V

    .line 103
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mMPController:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->startAfterFlick()V

    .line 105
    :cond_1
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 90
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->setIsRunningAni(Z)V

    .line 83
    const-string v0, ""

    const-string v1, "AL scr start"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;

    invoke-interface {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController$CubeViewUpdateListener;->onStartAnimation()V

    .line 87
    :cond_0
    return-void
.end method
