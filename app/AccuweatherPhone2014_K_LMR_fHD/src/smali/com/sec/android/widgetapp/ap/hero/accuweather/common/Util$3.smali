.class final Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util$3;
.super Ljava/lang/Object;
.source "Util.java"

# interfaces
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->showGPSonlyDialog(Landroid/app/Activity;Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/LocationGetter;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/app/Activity;

.field final synthetic val$lgetter:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/LocationGetter;


# direct methods
.method constructor <init>(Landroid/app/Activity;Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/LocationGetter;)V
    .locals 0

    .prologue
    .line 3388
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util$3;->val$context:Landroid/app/Activity;

    iput-object p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util$3;->val$lgetter:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/LocationGetter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public click(I)V
    .locals 5
    .param p1, "buttonType"    # I

    .prologue
    .line 3390
    const/16 v2, 0xc

    if-ne p1, v2, :cond_1

    .line 3391
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util$3;->val$context:Landroid/app/Activity;

    const-string v3, "config"

    const/4 v4, 0x4

    invoke-virtual {v2, v3, v4}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 3392
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 3393
    .local v0, "edit":Landroid/content/SharedPreferences$Editor;
    const-string v2, "CHECK_GPS_ONLY_DONOT_SHOW"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 3394
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 3402
    .end local v0    # "edit":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "prefs":Landroid/content/SharedPreferences;
    :cond_0
    :goto_0
    return-void

    .line 3395
    :cond_1
    const/16 v2, 0xb

    if-ne p1, v2, :cond_2

    .line 3396
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util$3;->val$context:Landroid/app/Activity;

    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->startLocationSettingActivity(Landroid/app/Activity;)V

    goto :goto_0

    .line 3397
    :cond_2
    const/16 v2, 0xa

    if-ne p1, v2, :cond_0

    .line 3398
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util$3;->val$lgetter:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/LocationGetter;

    if-eqz v2, :cond_0

    .line 3399
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util$3;->val$lgetter:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/LocationGetter;

    invoke-interface {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/LocationGetter;->getLocation()V

    goto :goto_0
.end method
