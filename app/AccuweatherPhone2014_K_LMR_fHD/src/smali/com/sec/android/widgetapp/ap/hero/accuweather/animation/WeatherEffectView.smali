.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;
.super Landroid/view/View;
.source "WeatherEffectView.java"


# static fields
.field private static final MAX_PARTICLEOBJ_CNT:I = 0x50

.field private static final PADDING:I = 0x64


# instance fields
.field private mIsStartAnim:Z

.field private mPaint:Landroid/graphics/Paint;

.field private mRVal:Ljava/util/Random;

.field private mRainParticleObj:[Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;

.field private mSnowParticleObj:[Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;

.field private mViewHeight:I

.field private mViewWidth:I

.field private mWeatherType:Ljava/lang/String;

.field parent:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v2, 0x50

    .line 24
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 17
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mPaint:Landroid/graphics/Paint;

    .line 38
    new-array v0, v2, [Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mRainParticleObj:[Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;

    .line 40
    new-array v0, v2, [Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mSnowParticleObj:[Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;

    .line 42
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mRVal:Ljava/util/Random;

    .line 55
    const-string v0, "none"

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mWeatherType:Ljava/lang/String;

    .line 65
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->parent:Landroid/view/View;

    .line 161
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mIsStartAnim:Z

    .line 25
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->initView()V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/16 v2, 0x50

    .line 34
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 17
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mPaint:Landroid/graphics/Paint;

    .line 38
    new-array v0, v2, [Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mRainParticleObj:[Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;

    .line 40
    new-array v0, v2, [Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mSnowParticleObj:[Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;

    .line 42
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mRVal:Ljava/util/Random;

    .line 55
    const-string v0, "none"

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mWeatherType:Ljava/lang/String;

    .line 65
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->parent:Landroid/view/View;

    .line 161
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mIsStartAnim:Z

    .line 35
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->initView()V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/16 v2, 0x50

    .line 29
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 17
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mPaint:Landroid/graphics/Paint;

    .line 38
    new-array v0, v2, [Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mRainParticleObj:[Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;

    .line 40
    new-array v0, v2, [Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mSnowParticleObj:[Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;

    .line 42
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mRVal:Ljava/util/Random;

    .line 55
    const-string v0, "none"

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mWeatherType:Ljava/lang/String;

    .line 65
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->parent:Landroid/view/View;

    .line 161
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mIsStartAnim:Z

    .line 30
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->initView()V

    .line 31
    return-void
.end method


# virtual methods
.method public getViewHeight()I
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mViewHeight:I

    return v0
.end method

.method public getViewWidth()I
    .locals 1

    .prologue
    .line 158
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mViewWidth:I

    return v0
.end method

.method public getWeatherType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mWeatherType:Ljava/lang/String;

    return-object v0
.end method

.method public initView()V
    .locals 3

    .prologue
    .line 49
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x50

    if-ge v0, v1, :cond_0

    .line 50
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mRainParticleObj:[Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;

    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;

    invoke-direct {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;-><init>()V

    aput-object v2, v1, v0

    .line 51
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mSnowParticleObj:[Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;

    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;

    invoke-direct {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;-><init>()V

    aput-object v2, v1, v0

    .line 49
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 53
    :cond_0
    return-void
.end method

.method public isRunningAni()Z
    .locals 1

    .prologue
    .line 173
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mIsStartAnim:Z

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 182
    iget-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mIsStartAnim:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mWeatherType:Ljava/lang/String;

    const-string v2, "none"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 197
    :cond_0
    :goto_0
    return-void

    .line 185
    :cond_1
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 187
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    const/16 v1, 0x50

    if-ge v0, v1, :cond_3

    .line 188
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mWeatherType:Ljava/lang/String;

    const-string v2, "rain"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 189
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mRainParticleObj:[Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, p1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->Animate(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    .line 187
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 191
    :cond_2
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mSnowParticleObj:[Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, p1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->Animate(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    goto :goto_2

    .line 195
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->invalidate()V

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 3
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 139
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 140
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mViewWidth:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mViewHeight:I

    if-nez v0, :cond_1

    .line 141
    :cond_0
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mViewWidth:I

    .line 142
    iput p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mViewHeight:I

    .line 143
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mWeatherType:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->setObject(Ljava/lang/String;)V

    .line 147
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSizeChanged mViewWidth = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mViewWidth:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mViewHeight = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mViewHeight:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    :cond_1
    return-void
.end method

.method public setColor()V
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mWeatherType:Ljava/lang/String;

    const-string v1, "none"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    :goto_0
    return-void

    .line 131
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mWeatherType:Ljava/lang/String;

    const-string v1, "rain"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 132
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mPaint:Landroid/graphics/Paint;

    sget v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/AnimUtils;->RAIN_EFFECT_NORMAL_COLOR:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0

    .line 134
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mPaint:Landroid/graphics/Paint;

    sget v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/AnimUtils;->RAIN_EFFECT_NORMAL_COLOR:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0
.end method

.method public setObject(Ljava/lang/String;)V
    .locals 10
    .param p1, "weatherType"    # Ljava/lang/String;

    .prologue
    const/16 v9, 0x50

    const/high16 v8, 0x40800000    # 4.0f

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 68
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mWeatherType:Ljava/lang/String;

    .line 70
    const-string v3, "none"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 124
    :cond_0
    return-void

    .line 74
    :cond_1
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setObject mViewWidth = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mViewWidth:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", mViewHeight = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mViewHeight:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mViewWidth:I

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mViewHeight:I

    if-eqz v3, :cond_0

    .line 79
    const-string v3, "rain"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 80
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 81
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v6}, Landroid/graphics/Paint;->setDither(Z)V

    .line 82
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->setColor()V

    .line 83
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 84
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v8}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 85
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v9, :cond_0

    .line 86
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mRVal:Ljava/util/Random;

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mViewHeight:I

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit8 v2, v3, -0x64

    .line 87
    .local v2, "y":I
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mRVal:Ljava/util/Random;

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mViewWidth:I

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit8 v1, v3, 0x64

    .line 88
    .local v1, "x":I
    rem-int/lit8 v3, v0, 0x3

    if-nez v3, :cond_2

    .line 89
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mRainParticleObj:[Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;

    aget-object v3, v3, v0

    int-to-float v4, v1

    int-to-float v5, v2

    const/high16 v6, 0x42400000    # 48.0f

    invoke-virtual {v3, v4, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->setPos(FFF)V

    .line 90
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mRainParticleObj:[Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;

    aget-object v3, v3, v0

    mul-int/lit8 v4, v0, 0x7

    invoke-virtual {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->setInterval(I)V

    .line 85
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 92
    :cond_2
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mRainParticleObj:[Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;

    aget-object v3, v3, v0

    int-to-float v4, v1

    int-to-float v5, v2

    invoke-virtual {v3, v4, v5, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->setPos(FFF)V

    .line 93
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mRainParticleObj:[Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;

    aget-object v3, v3, v0

    mul-int/lit8 v4, v0, 0x9

    invoke-virtual {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/RainParticleObj;->setInterval(I)V

    goto :goto_1

    .line 97
    .end local v0    # "i":I
    .end local v1    # "x":I
    .end local v2    # "y":I
    :cond_3
    const-string v3, "snow"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 98
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 99
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->setColor()V

    .line 100
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 101
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v8}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 102
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    if-ge v0, v9, :cond_0

    .line 103
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mRVal:Ljava/util/Random;

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mViewHeight:I

    add-int/lit8 v4, v4, -0x64

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit8 v2, v3, 0x32

    .line 104
    .restart local v2    # "y":I
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mRVal:Ljava/util/Random;

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mViewWidth:I

    add-int/lit8 v4, v4, -0x64

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit8 v1, v3, 0x32

    .line 105
    .restart local v1    # "x":I
    rem-int/lit8 v3, v0, 0x2

    if-nez v3, :cond_4

    .line 106
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mSnowParticleObj:[Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;

    aget-object v3, v3, v0

    int-to-float v4, v1

    int-to-float v5, v2

    const/high16 v6, 0x40c00000    # 6.0f

    invoke-virtual {v3, v4, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->setPos(FFF)V

    .line 107
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mSnowParticleObj:[Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;

    aget-object v3, v3, v0

    mul-int/lit8 v4, v0, 0x7

    invoke-virtual {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->setInterval(I)V

    .line 102
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 108
    :cond_4
    rem-int/lit8 v3, v0, 0x3

    if-nez v3, :cond_5

    .line 109
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mSnowParticleObj:[Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;

    aget-object v3, v3, v0

    int-to-float v4, v1

    int-to-float v5, v2

    invoke-virtual {v3, v4, v5, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->setPos(FFF)V

    .line 110
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mSnowParticleObj:[Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;

    aget-object v3, v3, v0

    mul-int/lit8 v4, v0, 0x9

    invoke-virtual {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->setInterval(I)V

    goto :goto_3

    .line 111
    :cond_5
    rem-int/lit8 v3, v0, 0x7

    if-nez v3, :cond_6

    .line 112
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mSnowParticleObj:[Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;

    aget-object v3, v3, v0

    int-to-float v4, v1

    int-to-float v5, v2

    const/high16 v6, 0x41100000    # 9.0f

    invoke-virtual {v3, v4, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->setPos(FFF)V

    .line 113
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mSnowParticleObj:[Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;

    aget-object v3, v3, v0

    mul-int/lit8 v4, v0, 0xb

    invoke-virtual {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->setInterval(I)V

    goto :goto_3

    .line 114
    :cond_6
    rem-int/lit8 v3, v0, 0xb

    if-nez v3, :cond_7

    .line 115
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mSnowParticleObj:[Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;

    aget-object v3, v3, v0

    int-to-float v4, v1

    int-to-float v5, v2

    const/high16 v6, 0x41400000    # 12.0f

    invoke-virtual {v3, v4, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->setPos(FFF)V

    .line 116
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mSnowParticleObj:[Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;

    aget-object v3, v3, v0

    mul-int/lit8 v4, v0, 0xd

    invoke-virtual {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->setInterval(I)V

    goto :goto_3

    .line 118
    :cond_7
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mSnowParticleObj:[Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;

    aget-object v3, v3, v0

    int-to-float v4, v1

    int-to-float v5, v2

    const/high16 v6, 0x41900000    # 18.0f

    invoke-virtual {v3, v4, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->setPos(FFF)V

    .line 119
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mSnowParticleObj:[Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;

    aget-object v3, v3, v0

    mul-int/lit8 v4, v0, 0xf

    invoke-virtual {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/object/SnowParticleObj;->setInterval(I)V

    goto :goto_3
.end method

.method public setWeatherType(Ljava/lang/String;)V
    .locals 0
    .param p1, "mWeatherType"    # Ljava/lang/String;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mWeatherType:Ljava/lang/String;

    .line 63
    return-void
.end method

.method public startAnimation()V
    .locals 2

    .prologue
    .line 164
    const-string v0, ""

    const-string v1, "start effect"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mWeatherType:Ljava/lang/String;

    const-string v1, "none"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 170
    :goto_0
    return-void

    .line 168
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mIsStartAnim:Z

    .line 169
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->invalidate()V

    goto :goto_0
.end method

.method public stopAnimation()V
    .locals 2

    .prologue
    .line 177
    const-string v0, ""

    const-string v1, "stop effect"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/WeatherEffectView;->mIsStartAnim:Z

    .line 179
    return-void
.end method
