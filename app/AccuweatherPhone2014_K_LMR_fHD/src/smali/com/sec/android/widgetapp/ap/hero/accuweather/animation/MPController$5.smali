.class Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$5;
.super Ljava/lang/Object;
.source "MPController.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    .prologue
    .line 692
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 5
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 694
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPView:Landroid/widget/VideoView;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)Landroid/widget/VideoView;

    move-result-object v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_2

    .line 695
    :cond_0
    const-string v0, ""

    const-string v1, "mp null~~"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 736
    :cond_1
    :goto_0
    return-void

    .line 699
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPState:I
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPState:I
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)I

    move-result v0

    if-ne v0, v4, :cond_4

    .line 700
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPState:I
    invoke-static {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$202(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;I)I

    goto :goto_0

    .line 704
    :cond_4
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    .line 705
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;->getCubeViewController()Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    .line 706
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;->getCubeViewController()Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/CubeViewController;->isRunningAni()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 707
    const-string v0, ""

    const-string v1, "running scroll ani "

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 711
    :cond_5
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mp play!! mMPState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPState:I
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 712
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$402(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer;

    .line 713
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;->getViEffectSettinginfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 714
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;->getViEffectSettinginfo()Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsViEffect;->getBackgroundVideoRepeat()I

    move-result v0

    if-ne v0, v4, :cond_6

    .line 715
    invoke-virtual {p1, v4}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 722
    :goto_1
    invoke-virtual {p1, v3}, Landroid/media/MediaPlayer;->setScreenOnWhilePlaying(Z)V

    .line 723
    invoke-virtual {p1, v4}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 724
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->start()V

    .line 727
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->playBGFadeOut()V

    .line 731
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    const/4 v1, 0x2

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mMPState:I
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$202(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;I)I

    .line 733
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 734
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$5;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->mListener:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController;)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/MPController$onMediaPlayerListener;->onPrepareComplete(Landroid/media/MediaPlayer;)V

    goto/16 :goto_0

    .line 717
    :cond_6
    invoke-virtual {p1, v3}, Landroid/media/MediaPlayer;->setLooping(Z)V

    goto :goto_1

    .line 720
    :cond_7
    invoke-virtual {p1, v3}, Landroid/media/MediaPlayer;->setLooping(Z)V

    goto :goto_1
.end method
