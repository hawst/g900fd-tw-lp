.class public final Lcom/sec/android/widgetapp/ap/hero/accuweather/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final actionbar_btn_selector:I = 0x7f020000

.field public static final actionbar_checkbox_selector:I = 0x7f020001

.field public static final actionbar_spinner_selector:I = 0x7f020002

.field public static final bg_2nd_forecast:I = 0x7f020003

.field public static final bg_2nd_forecast_press:I = 0x7f020004

.field public static final btn_main_focus:I = 0x7f020005

.field public static final btn_press_ripple_effect:I = 0x7f020006

.field public static final btn_search_dialog:I = 0x7f020007

.field public static final btn_search_dialog_default:I = 0x7f020008

.field public static final changeorder_icon_selector:I = 0x7f020009

.field public static final checkbox_selector:I = 0x7f02000a

.field public static final citylist_selector:I = 0x7f02000b

.field public static final cursor_drawable:I = 0x7f02000c

.field public static final detail_selector:I = 0x7f02000d

.field public static final dropdown_list_selector:I = 0x7f02000e

.field public static final easy_main_ic_accuweather:I = 0x7f02000f

.field public static final easy_temp_0:I = 0x7f020010

.field public static final easy_temp_1:I = 0x7f020011

.field public static final easy_temp_2:I = 0x7f020012

.field public static final easy_temp_3:I = 0x7f020013

.field public static final easy_temp_4:I = 0x7f020014

.field public static final easy_temp_5:I = 0x7f020015

.field public static final easy_temp_6:I = 0x7f020016

.field public static final easy_temp_7:I = 0x7f020017

.field public static final easy_temp_8:I = 0x7f020018

.field public static final easy_temp_9:I = 0x7f020019

.field public static final easy_temp_minus:I = 0x7f02001a

.field public static final easy_weather_clear_day_bg:I = 0x7f02001b

.field public static final easy_weather_clear_night_bg:I = 0x7f02001c

.field public static final easy_weather_cloudy_day_bg:I = 0x7f02001d

.field public static final easy_weather_cloudy_night_bg:I = 0x7f02001e

.field public static final easy_weather_cold_bg:I = 0x7f02001f

.field public static final easy_weather_fog_day_bg:I = 0x7f020020

.field public static final easy_weather_fog_night_bg:I = 0x7f020021

.field public static final easy_weather_hot_bg:I = 0x7f020022

.field public static final easy_weather_ic_clear:I = 0x7f020023

.field public static final easy_weather_ic_cold:I = 0x7f020024

.field public static final easy_weather_ic_flurries:I = 0x7f020025

.field public static final easy_weather_ic_fog:I = 0x7f020026

.field public static final easy_weather_ic_hot:I = 0x7f020027

.field public static final easy_weather_ic_ice:I = 0x7f020028

.field public static final easy_weather_ic_mostlyclear:I = 0x7f020029

.field public static final easy_weather_ic_mostlycloudy:I = 0x7f02002a

.field public static final easy_weather_ic_partlysunny:I = 0x7f02002b

.field public static final easy_weather_ic_partlysunnywithflurries:I = 0x7f02002c

.field public static final easy_weather_ic_partlysunnywithshowers:I = 0x7f02002d

.field public static final easy_weather_ic_partlysunnywiththundershowers:I = 0x7f02002e

.field public static final easy_weather_ic_rain:I = 0x7f02002f

.field public static final easy_weather_ic_rainandsnowmixed:I = 0x7f020030

.field public static final easy_weather_ic_shower:I = 0x7f020031

.field public static final easy_weather_ic_snow:I = 0x7f020032

.field public static final easy_weather_ic_sunny:I = 0x7f020033

.field public static final easy_weather_ic_thunderstorms:I = 0x7f020034

.field public static final easy_weather_ic_windy:I = 0x7f020035

.field public static final easy_weather_partlysunny_day_bg:I = 0x7f020036

.field public static final easy_weather_partlysunny_night_bg:I = 0x7f020037

.field public static final easy_weather_preview:I = 0x7f020038

.field public static final easy_weather_rain_day_bg:I = 0x7f020039

.field public static final easy_weather_rain_night_bg:I = 0x7f02003a

.field public static final easy_weather_snow_day_bg:I = 0x7f02003b

.field public static final easy_weather_snow_night_bg:I = 0x7f02003c

.field public static final easy_weather_temp_c:I = 0x7f02003d

.field public static final easy_weather_temp_f:I = 0x7f02003e

.field public static final easy_weather_thunderstorms_day_bg:I = 0x7f02003f

.field public static final easy_weather_thunderstorms_night_bg:I = 0x7f020040

.field public static final focast_weather_ic_clear:I = 0x7f020041

.field public static final focast_weather_ic_cold:I = 0x7f020042

.field public static final focast_weather_ic_flurries:I = 0x7f020043

.field public static final focast_weather_ic_fog:I = 0x7f020044

.field public static final focast_weather_ic_hot:I = 0x7f020045

.field public static final focast_weather_ic_ice:I = 0x7f020046

.field public static final focast_weather_ic_mostlyclear:I = 0x7f020047

.field public static final focast_weather_ic_mostlycloudy:I = 0x7f020048

.field public static final focast_weather_ic_partlysunny:I = 0x7f020049

.field public static final focast_weather_ic_partlysunnywithflurries:I = 0x7f02004a

.field public static final focast_weather_ic_partlysunnywithshowers:I = 0x7f02004b

.field public static final focast_weather_ic_partlysunnywiththundershowers:I = 0x7f02004c

.field public static final focast_weather_ic_rain:I = 0x7f02004d

.field public static final focast_weather_ic_rainandsnowmixed:I = 0x7f02004e

.field public static final focast_weather_ic_shower:I = 0x7f02004f

.field public static final focast_weather_ic_snow:I = 0x7f020050

.field public static final focast_weather_ic_sunny:I = 0x7f020051

.field public static final focast_weather_ic_thunderstorms:I = 0x7f020052

.field public static final focast_weather_ic_windy:I = 0x7f020053

.field public static final ic_btn_search:I = 0x7f020054

.field public static final map_icon_button:I = 0x7f020055

.field public static final map_weather_ic_clear:I = 0x7f020056

.field public static final map_weather_ic_cold:I = 0x7f020057

.field public static final map_weather_ic_flurries:I = 0x7f020058

.field public static final map_weather_ic_fog:I = 0x7f020059

.field public static final map_weather_ic_hot:I = 0x7f02005a

.field public static final map_weather_ic_ice:I = 0x7f02005b

.field public static final map_weather_ic_mostlyclear:I = 0x7f02005c

.field public static final map_weather_ic_mostlycloudy:I = 0x7f02005d

.field public static final map_weather_ic_partlysunny:I = 0x7f02005e

.field public static final map_weather_ic_partlysunnywithflurries:I = 0x7f02005f

.field public static final map_weather_ic_partlysunnywithshowers:I = 0x7f020060

.field public static final map_weather_ic_partlysunnywiththundershowers:I = 0x7f020061

.field public static final map_weather_ic_rain:I = 0x7f020062

.field public static final map_weather_ic_rainandsnowmixed:I = 0x7f020063

.field public static final map_weather_ic_shower:I = 0x7f020064

.field public static final map_weather_ic_snow:I = 0x7f020065

.field public static final map_weather_ic_sunny:I = 0x7f020066

.field public static final map_weather_ic_thunderstorms:I = 0x7f020067

.field public static final map_weather_ic_windy:I = 0x7f020068

.field public static final mapitem_selector:I = 0x7f020069

.field public static final more_weather_ic_clear:I = 0x7f02006a

.field public static final more_weather_ic_cold:I = 0x7f02006b

.field public static final more_weather_ic_flurries:I = 0x7f02006c

.field public static final more_weather_ic_fog:I = 0x7f02006d

.field public static final more_weather_ic_hot:I = 0x7f02006e

.field public static final more_weather_ic_ice:I = 0x7f02006f

.field public static final more_weather_ic_mostlyclear:I = 0x7f020070

.field public static final more_weather_ic_mostlycloudy:I = 0x7f020071

.field public static final more_weather_ic_partlysunny:I = 0x7f020072

.field public static final more_weather_ic_partlysunnywithflurries:I = 0x7f020073

.field public static final more_weather_ic_partlysunnywithshowers:I = 0x7f020074

.field public static final more_weather_ic_partlysunnywiththundershowers:I = 0x7f020075

.field public static final more_weather_ic_rain:I = 0x7f020076

.field public static final more_weather_ic_rainandsnowmixed:I = 0x7f020077

.field public static final more_weather_ic_shower:I = 0x7f020078

.field public static final more_weather_ic_snow:I = 0x7f020079

.field public static final more_weather_ic_sunny:I = 0x7f02007a

.field public static final more_weather_ic_thunderstorms:I = 0x7f02007b

.field public static final more_weather_ic_windy:I = 0x7f02007c

.field public static final popup_city_focus:I = 0x7f02007d

.field public static final popup_city_normal:I = 0x7f02007e

.field public static final popup_city_pressed:I = 0x7f02007f

.field public static final refresh_icon_detail:I = 0x7f020080

.field public static final refresh_icon_list:I = 0x7f020081

.field public static final refresh_selector:I = 0x7f020082

.field public static final searchfield_box:I = 0x7f020083

.field public static final searchfield_shape:I = 0x7f020084

.field public static final searchfield_textcolor:I = 0x7f020085

.field public static final send_weather_rain:I = 0x7f020086

.field public static final stat_notify_weather:I = 0x7f020087

.field public static final textfield_search:I = 0x7f020088

.field public static final textfield_search_pressed:I = 0x7f020089

.field public static final textfield_search_selected:I = 0x7f02008a

.field public static final textfield_searchwidget_default:I = 0x7f02008b

.field public static final trayicon:I = 0x7f02008c

.field public static final tw_ab_spinner_list_focused_holo_light:I = 0x7f02008d

.field public static final tw_ab_spinner_list_pressed_holo_light:I = 0x7f02008e

.field public static final tw_ab_transparent_light_holo:I = 0x7f02008f

.field public static final tw_action_bar_icon_add_reject_list:I = 0x7f020090

.field public static final tw_action_bar_icon_cancel_02_holo_dark:I = 0x7f020091

.field public static final tw_action_bar_icon_change_order_list_holo_light:I = 0x7f020092

.field public static final tw_action_bar_icon_check_holo_dark:I = 0x7f020093

.field public static final tw_action_bar_icon_delete_holo_light:I = 0x7f020094

.field public static final tw_action_bar_icon_list_holo_dark:I = 0x7f020095

.field public static final tw_action_bar_icon_new_add_holo_dark:I = 0x7f020096

.field public static final tw_action_bar_icon_search_holo_light:I = 0x7f020097

.field public static final tw_action_bar_icon_search_mtrl:I = 0x7f020098

.field public static final tw_action_bar_icon_setting_holo_light:I = 0x7f020099

.field public static final tw_action_bar_show_current_city_holo_dark:I = 0x7f02009a

.field public static final tw_action_item_background_focused_holo_light:I = 0x7f02009b

.field public static final tw_action_item_background_pressed_holo_light:I = 0x7f02009c

.field public static final tw_action_item_background_selected_holo_light:I = 0x7f02009d

.field public static final tw_btn_borderless_focused_holo_dark:I = 0x7f02009e

.field public static final tw_btn_borderless_pressed_holo_dark:I = 0x7f02009f

.field public static final tw_btn_borderless_selected_holo_dark:I = 0x7f0200a0

.field public static final tw_btn_borderless_selector:I = 0x7f0200a1

.field public static final tw_btn_check_to_off_mtrl_015:I = 0x7f0200a2

.field public static final tw_btn_check_to_on_mtrl_015:I = 0x7f0200a3

.field public static final tw_btn_logo_pressed_holo_dark:I = 0x7f0200a4

.field public static final tw_btn_logo_selector:I = 0x7f0200a5

.field public static final tw_cab_background_top_holo_light:I = 0x7f0200a6

.field public static final tw_divider_ab_holo_light:I = 0x7f0200a7

.field public static final tw_gallery_btn_check_off:I = 0x7f0200a8

.field public static final tw_gallery_btn_check_on:I = 0x7f0200a9

.field public static final tw_ic_ab_back_holo_light:I = 0x7f0200aa

.field public static final tw_ic_cab_done_holo_light:I = 0x7f0200ab

.field public static final tw_ic_clear_search_api_holo_light:I = 0x7f0200ac

.field public static final tw_ic_clear_search_api_mtrl:I = 0x7f0200ad

.field public static final tw_ic_menu_moreoverflow_normal_holo_dark:I = 0x7f0200ae

.field public static final tw_ic_menu_moreoverflow_pressed_dark:I = 0x7f0200af

.field public static final tw_ic_search:I = 0x7f0200b0

.field public static final tw_ic_search_01:I = 0x7f0200b1

.field public static final tw_list_disabled_holo_light:I = 0x7f0200b2

.field public static final tw_list_divider_holo_light:I = 0x7f0200b3

.field public static final tw_list_focused_holo_light:I = 0x7f0200b4

.field public static final tw_list_pressed_holo_light:I = 0x7f0200b5

.field public static final tw_list_selected_holo_light:I = 0x7f0200b6

.field public static final tw_menu_ab_dropdown_panel_holo_light:I = 0x7f0200b7

.field public static final tw_progress_bg_holo_light:I = 0x7f0200b8

.field public static final tw_progress_primary_holo_light:I = 0x7f0200b9

.field public static final tw_scrubber_control_focused_holo_light:I = 0x7f0200ba

.field public static final tw_scrubber_control_holo_light:I = 0x7f0200bb

.field public static final tw_scrubber_control_pressed_focused_holo_light:I = 0x7f0200bc

.field public static final tw_seekbar_progress_bg:I = 0x7f0200bd

.field public static final tw_seekbar_thum:I = 0x7f0200be

.field public static final tw_select_all_bg_holo_light:I = 0x7f0200bf

.field public static final tw_spinner_ab_default_holo_light_am:I = 0x7f0200c0

.field public static final tw_spinner_ab_disabled_holo_light_am:I = 0x7f0200c1

.field public static final tw_spinner_ab_focused_holo_light_am:I = 0x7f0200c2

.field public static final tw_spinner_ab_pressed_holo_light_am:I = 0x7f0200c3

.field public static final tw_spinner_mtrl_am_alpha:I = 0x7f0200c4

.field public static final tw_textfield_search_default_holo_light:I = 0x7f0200c5

.field public static final tw_textfield_search_focused_01_holo_light:I = 0x7f0200c6

.field public static final tw_textfield_search_selected_holo_light:I = 0x7f0200c7

.field public static final tw_toast_frame_mtrl:I = 0x7f0200c8

.field public static final weather_2depth_ic_rainprobability:I = 0x7f0200c9

.field public static final weather_2depth_ic_realfeel:I = 0x7f0200ca

.field public static final weather_2depth_ic_sunrise:I = 0x7f0200cb

.field public static final weather_2depth_ic_uvindex:I = 0x7f0200cc

.field public static final weather_2depth_line:I = 0x7f0200cd

.field public static final weather_2depth_location_ic:I = 0x7f0200ce

.field public static final weather_2depth_temp:I = 0x7f0200cf

.field public static final weather_2depth_temp_c:I = 0x7f0200d0

.field public static final weather_2depth_temp_f:I = 0x7f0200d1

.field public static final weather_2depth_temp_slash:I = 0x7f0200d2

.field public static final weather_2depth_update:I = 0x7f0200d3

.field public static final weather_2depth_update_bg:I = 0x7f0200d4

.field public static final weather_2depth_update_pre:I = 0x7f0200d5

.field public static final weather_2depth_week_line:I = 0x7f0200d6

.field public static final weather_2nd_bg_clear:I = 0x7f0200d7

.field public static final weather_2nd_bg_cloudy:I = 0x7f0200d8

.field public static final weather_2nd_bg_cloudy_n:I = 0x7f0200d9

.field public static final weather_2nd_bg_cold:I = 0x7f0200da

.field public static final weather_2nd_bg_fog:I = 0x7f0200db

.field public static final weather_2nd_bg_fog_n:I = 0x7f0200dc

.field public static final weather_2nd_bg_hot:I = 0x7f0200dd

.field public static final weather_2nd_bg_mostly_clear:I = 0x7f0200de

.field public static final weather_2nd_bg_partly_sunny:I = 0x7f0200df

.field public static final weather_2nd_bg_rain:I = 0x7f0200e0

.field public static final weather_2nd_bg_rain_n:I = 0x7f0200e1

.field public static final weather_2nd_bg_snow:I = 0x7f0200e2

.field public static final weather_2nd_bg_snow_n:I = 0x7f0200e3

.field public static final weather_2nd_bg_sunny:I = 0x7f0200e4

.field public static final weather_2nd_bg_thunderstorms:I = 0x7f0200e5

.field public static final weather_2nd_bg_thunderstorms_n:I = 0x7f0200e6

.field public static final weather_accuweather_logo:I = 0x7f0200e7

.field public static final weather_add:I = 0x7f0200e8

.field public static final weather_bg:I = 0x7f0200e9

.field public static final weather_current_location:I = 0x7f0200ea

.field public static final weather_global:I = 0x7f0200eb

.field public static final weather_global_disable:I = 0x7f0200ec

.field public static final weather_ic_clear:I = 0x7f0200ed

.field public static final weather_ic_cold:I = 0x7f0200ee

.field public static final weather_ic_flurries:I = 0x7f0200ef

.field public static final weather_ic_fog:I = 0x7f0200f0

.field public static final weather_ic_hot:I = 0x7f0200f1

.field public static final weather_ic_ice:I = 0x7f0200f2

.field public static final weather_ic_mostlyclear:I = 0x7f0200f3

.field public static final weather_ic_mostlycloudy:I = 0x7f0200f4

.field public static final weather_ic_pageindicator_off:I = 0x7f0200f5

.field public static final weather_ic_pageindicator_on:I = 0x7f0200f6

.field public static final weather_ic_partlysunny:I = 0x7f0200f7

.field public static final weather_ic_partlysunnywithflurries:I = 0x7f0200f8

.field public static final weather_ic_partlysunnywithshowers:I = 0x7f0200f9

.field public static final weather_ic_partlysunnywiththundershowers:I = 0x7f0200fa

.field public static final weather_ic_rain:I = 0x7f0200fb

.field public static final weather_ic_rain_percent:I = 0x7f0200fc

.field public static final weather_ic_rainandsnowmixed:I = 0x7f0200fd

.field public static final weather_ic_shower:I = 0x7f0200fe

.field public static final weather_ic_snow:I = 0x7f0200ff

.field public static final weather_ic_sun:I = 0x7f020100

.field public static final weather_ic_sunny:I = 0x7f020101

.field public static final weather_ic_temp:I = 0x7f020102

.field public static final weather_ic_temp_c:I = 0x7f020103

.field public static final weather_ic_temp_f:I = 0x7f020104

.field public static final weather_ic_thunderstorms:I = 0x7f020105

.field public static final weather_ic_windy:I = 0x7f020106

.field public static final weather_icon_map_01:I = 0x7f020107

.field public static final weather_icon_map_02:I = 0x7f020108

.field public static final weather_icon_map_03:I = 0x7f020109

.field public static final weather_icon_map_04:I = 0x7f02010a

.field public static final weather_icon_map_05:I = 0x7f02010b

.field public static final weather_icon_map_06:I = 0x7f02010c

.field public static final weather_icon_map_07:I = 0x7f02010d

.field public static final weather_icon_map_08:I = 0x7f02010e

.field public static final weather_icon_map_09:I = 0x7f02010f

.field public static final weather_icon_map_10:I = 0x7f020110

.field public static final weather_icon_map_11:I = 0x7f020111

.field public static final weather_icon_map_12:I = 0x7f020112

.field public static final weather_icon_map_13:I = 0x7f020113

.field public static final weather_icon_map_14:I = 0x7f020114

.field public static final weather_icon_map_15:I = 0x7f020115

.field public static final weather_icon_map_16:I = 0x7f020116

.field public static final weather_icon_map_17:I = 0x7f020117

.field public static final weather_icon_map_18:I = 0x7f020118

.field public static final weather_icon_map_19:I = 0x7f020119

.field public static final weather_icon_press:I = 0x7f02011a

.field public static final weather_info:I = 0x7f02011b

.field public static final weather_info_alpha:I = 0x7f02011c

.field public static final weather_list:I = 0x7f02011d

.field public static final weather_list_accuweather_logo:I = 0x7f02011e

.field public static final weather_list_cloudy_day_bg:I = 0x7f02011f

.field public static final weather_list_cloudy_night_bg:I = 0x7f020120

.field public static final weather_list_cold_bg:I = 0x7f020121

.field public static final weather_list_fog_day_bg:I = 0x7f020122

.field public static final weather_list_fog_night_bg:I = 0x7f020123

.field public static final weather_list_hot_bg:I = 0x7f020124

.field public static final weather_list_ic_location:I = 0x7f020125

.field public static final weather_list_ic_temp:I = 0x7f020126

.field public static final weather_list_ic_temp_c:I = 0x7f020127

.field public static final weather_list_ic_temp_f:I = 0x7f020128

.field public static final weather_list_ic_time:I = 0x7f020129

.field public static final weather_list_line:I = 0x7f02012a

.field public static final weather_list_mostlyclear_day_bg:I = 0x7f02012b

.field public static final weather_list_mostlyclear_night_bg:I = 0x7f02012c

.field public static final weather_list_rain_day_bg:I = 0x7f02012d

.field public static final weather_list_rain_night_bg:I = 0x7f02012e

.field public static final weather_list_snow_day_bg:I = 0x7f02012f

.field public static final weather_list_snow_night_bg:I = 0x7f020130

.field public static final weather_list_sunnyclear_day_bg:I = 0x7f020131

.field public static final weather_list_sunnyclear_night_bg:I = 0x7f020132

.field public static final weather_list_temp_c:I = 0x7f020133

.field public static final weather_list_temp_f:I = 0x7f020134

.field public static final weather_list_thunderstorm_day_bg:I = 0x7f020135

.field public static final weather_list_thunderstorm_night_bg:I = 0x7f020136

.field public static final weather_list_update:I = 0x7f020137

.field public static final weather_list_update_bg:I = 0x7f020138

.field public static final weather_location:I = 0x7f020139

.field public static final weather_location_ic:I = 0x7f02013a

.field public static final weather_map_popup_currentlocation:I = 0x7f02013b

.field public static final weather_map_popup_location:I = 0x7f02013c

.field public static final weather_map_temp_c:I = 0x7f02013d

.field public static final weather_map_temp_f:I = 0x7f02013e

.field public static final weather_menu:I = 0x7f02013f

.field public static final weather_more:I = 0x7f020140

.field public static final weather_realfeel_r:I = 0x7f020141

.field public static final weather_reorder:I = 0x7f020142

.field public static final weather_reorder_focused:I = 0x7f020143

.field public static final weather_reorder_pressed:I = 0x7f020144

.field public static final weather_reorder_stroke:I = 0x7f020145

.field public static final weather_temp:I = 0x7f020146

.field public static final weather_temp_realfeel:I = 0x7f020147

.field public static final weather_trans_ic_summertime:I = 0x7f020148

.field public static final weather_update:I = 0x7f020149

.field public static final widget_clear_bg:I = 0x7f02014a

.field public static final widget_cloudy_day_bg:I = 0x7f02014b

.field public static final widget_cloudy_night_bg:I = 0x7f02014c

.field public static final widget_cold_bg:I = 0x7f02014d

.field public static final widget_fog_day_bg:I = 0x7f02014e

.field public static final widget_fog_night_bg:I = 0x7f02014f

.field public static final widget_hot_bg:I = 0x7f020150

.field public static final widget_masking_bg:I = 0x7f020151

.field public static final widget_preview:I = 0x7f020152

.field public static final widget_rainny_day_bg:I = 0x7f020153

.field public static final widget_rainny_night_bg:I = 0x7f020154

.field public static final widget_shadow:I = 0x7f020155

.field public static final widget_snow_day_bg:I = 0x7f020156

.field public static final widget_snow_night_bg:I = 0x7f020157

.field public static final widget_sunny_bg:I = 0x7f020158

.field public static final widget_thunder_day_bg:I = 0x7f020159

.field public static final widget_thunder_night_bg:I = 0x7f02015a

.field public static final widget_windy_bg:I = 0x7f02015b


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 374
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
