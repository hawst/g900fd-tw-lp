.class Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2$6;
.super Ljava/lang/Object;
.source "MenuSettingsUseCurrentLocation.java"

# interfaces
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;->onCheckedChanged(Landroid/widget/CompoundButton;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;)V
    .locals 0
    .param p1, "this$1"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;

    .prologue
    .line 308
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2$6;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public click(I)V
    .locals 3
    .param p1, "buttonType"    # I

    .prologue
    const/4 v1, 0x0

    .line 310
    const/16 v0, 0xa

    if-ne p1, v0, :cond_1

    .line 311
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2$6;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateCheckCurrentCityLocation(Landroid/content/Context;I)I

    .line 312
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2$6;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setIsAgreeUseLocation(Landroid/content/Context;Z)V

    .line 313
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2$6;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->switchView:Landroid/widget/Switch;

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 314
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2$6;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getNotificationSettings(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 315
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2$6;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->unregisterNotiTime(Landroid/content/Context;)V

    .line 317
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2$6;->this$1:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation$2;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.widgetapp.ap.hero.accuweather.action.CHANGE_SETTING"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettingsUseCurrentLocation;->sendBroadcast(Landroid/content/Intent;)V

    .line 319
    :cond_1
    return-void
.end method
