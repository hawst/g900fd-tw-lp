.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;
.super Landroid/view/View;
.source "PartlySunnyFlurriesIconView.java"

# interfaces
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;


# instance fields
.field CloudTranslate:Landroid/animation/ValueAnimator;

.field DropCenterT1:Landroid/animation/ValueAnimator;

.field DropCenterT2:Landroid/animation/ValueAnimator;

.field DropCenterT3:Landroid/animation/ValueAnimator;

.field DropCenterT3_1:Landroid/animation/ValueAnimator;

.field DropScale1:Landroid/animation/ValueAnimator;

.field DropScale2:Landroid/animation/ValueAnimator;

.field DropScale3:Landroid/animation/ValueAnimator;

.field SunRotation:Landroid/animation/ValueAnimator;

.field SunScale1:Landroid/animation/ValueAnimator;

.field SunScale2:Landroid/animation/ValueAnimator;

.field isStop:Z

.field l:Landroid/animation/Animator$AnimatorListener;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mCenterDrop1:Landroid/graphics/Path;

.field private mCenterDrop2:Landroid/graphics/Path;

.field private mCenterDrop3:Landroid/graphics/Path;

.field private mCloud:Landroid/graphics/Path;

.field private mIsActiveAnimationThread:Z

.field private mLeftDrop1:Landroid/graphics/Path;

.field private mLeftDrop2:Landroid/graphics/Path;

.field private mMasking:Landroid/graphics/Path;

.field private mPaint:Landroid/graphics/Paint;

.field private mPaintColor:I

.field public mPreFixS1:Ljava/lang/String;

.field public mPreFixS2:Ljava/lang/String;

.field public mPreFixS3:Ljava/lang/String;

.field public mPreFixT1:Ljava/lang/String;

.field public mPreFixT2:Ljava/lang/String;

.field public mPreFixT3:Ljava/lang/String;

.field public mPreFixT3_1:Ljava/lang/String;

.field private mRightDrop1:Landroid/graphics/Path;

.field private mRightDrop2:Landroid/graphics/Path;

.field private mScale:F

.field private mSun:Landroid/graphics/Path;

.field private mSunOuter:Landroid/graphics/Path;

.field rotateOffset:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 101
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 102
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 97
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 98
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 92
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    .line 30
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunRotation:Landroid/animation/ValueAnimator;

    .line 32
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunScale1:Landroid/animation/ValueAnimator;

    .line 34
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunScale2:Landroid/animation/ValueAnimator;

    .line 36
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale1:Landroid/animation/ValueAnimator;

    .line 38
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale2:Landroid/animation/ValueAnimator;

    .line 40
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale3:Landroid/animation/ValueAnimator;

    .line 42
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    .line 44
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    .line 46
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT3:Landroid/animation/ValueAnimator;

    .line 48
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;

    .line 50
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->rotateOffset:F

    .line 52
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mIsActiveAnimationThread:Z

    .line 54
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPaint:Landroid/graphics/Paint;

    .line 56
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mMasking:Landroid/graphics/Path;

    .line 58
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    .line 60
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mSun:Landroid/graphics/Path;

    .line 62
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mSunOuter:Landroid/graphics/Path;

    .line 64
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mLeftDrop1:Landroid/graphics/Path;

    .line 66
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mLeftDrop2:Landroid/graphics/Path;

    .line 68
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCenterDrop1:Landroid/graphics/Path;

    .line 70
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCenterDrop2:Landroid/graphics/Path;

    .line 72
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCenterDrop3:Landroid/graphics/Path;

    .line 74
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mRightDrop1:Landroid/graphics/Path;

    .line 76
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mRightDrop2:Landroid/graphics/Path;

    .line 78
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    .line 81
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mBitmap:Landroid/graphics/Bitmap;

    .line 85
    const v0, -0xa0a0b

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPaintColor:I

    .line 583
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->l:Landroid/animation/Animator$AnimatorListener;

    .line 635
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPreFixS1:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPreFixS2:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPreFixS3:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPreFixT1:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPreFixT2:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPreFixT3:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPreFixT3_1:Ljava/lang/String;

    .line 853
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->isStop:Z

    .line 93
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->init()V

    .line 94
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mIsActiveAnimationThread:Z

    return v0
.end method

.method private drawCloud(Landroid/graphics/Canvas;Landroid/graphics/Paint;FF)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "paint"    # Landroid/graphics/Paint;
    .param p3, "dx"    # F
    .param p4, "dy"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 341
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPaintColor:I

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 342
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 343
    neg-float v0, p3

    const/high16 v1, 0x40a00000    # 5.0f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    add-float/2addr v1, p4

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 344
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    invoke-virtual {p1, v0, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 345
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 346
    return-void
.end method

.method private drawDrop(Landroid/graphics/Canvas;)V
    .locals 22
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 402
    const/4 v5, 0x0

    .line 403
    .local v5, "dcenterx1":F
    const/4 v9, 0x0

    .line 405
    .local v9, "dcentery1":F
    const/4 v7, 0x0

    .line 406
    .local v7, "dcenterx3":F
    const/4 v11, 0x0

    .line 408
    .local v11, "dcentery3":F
    const/4 v6, 0x0

    .line 409
    .local v6, "dcenterx2":F
    const/4 v10, 0x0

    .line 411
    .local v10, "dcentery2":F
    const/high16 v13, 0x3f800000    # 1.0f

    .line 412
    .local v13, "scale1_1":F
    const/high16 v14, 0x3f800000    # 1.0f

    .line 414
    .local v14, "scale1_2":F
    const/high16 v15, 0x3f800000    # 1.0f

    .line 415
    .local v15, "scale2_1":F
    const/high16 v16, 0x3f800000    # 1.0f

    .line 417
    .local v16, "scale2_2":F
    const/high16 v17, 0x3f800000    # 1.0f

    .line 418
    .local v17, "scale3_1":F
    const/high16 v18, 0x3f800000    # 1.0f

    .line 420
    .local v18, "scale3_2":F
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mIsActiveAnimationThread:Z

    move/from16 v19, v0

    if-eqz v19, :cond_2

    .line 421
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v19

    if-eqz v19, :cond_5

    .line 422
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    move-object/from16 v19, v0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPreFixT1:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "x"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Float;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Float;->floatValue()F

    move-result v5

    .line 423
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    move-object/from16 v19, v0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPreFixT1:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "y"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Float;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Float;->floatValue()F

    move-result v9

    .line 430
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v19

    if-eqz v19, :cond_6

    .line 431
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    move-object/from16 v19, v0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPreFixT2:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "x"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Float;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Float;->floatValue()F

    move-result v6

    .line 432
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    move-object/from16 v19, v0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPreFixT2:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "y"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Float;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Float;->floatValue()F

    move-result v10

    .line 438
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT3:Landroid/animation/ValueAnimator;

    move-object/from16 v19, v0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPreFixT3:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "x"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Float;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Float;->floatValue()F

    move-result v7

    .line 439
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT3:Landroid/animation/ValueAnimator;

    move-object/from16 v19, v0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPreFixT3:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "y"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Float;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Float;->floatValue()F

    move-result v11

    .line 441
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale1:Landroid/animation/ValueAnimator;

    move-object/from16 v19, v0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPreFixS1:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "scale"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Float;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Float;->floatValue()F

    move-result v13

    .line 442
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale1:Landroid/animation/ValueAnimator;

    move-object/from16 v19, v0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPreFixS1:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "scale_2"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Float;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Float;->floatValue()F

    move-result v14

    .line 444
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale2:Landroid/animation/ValueAnimator;

    move-object/from16 v19, v0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPreFixS2:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "scale"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Float;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Float;->floatValue()F

    move-result v15

    .line 445
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale2:Landroid/animation/ValueAnimator;

    move-object/from16 v19, v0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPreFixS2:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "scale_2"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Float;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Float;->floatValue()F

    move-result v16

    .line 447
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale3:Landroid/animation/ValueAnimator;

    move-object/from16 v19, v0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPreFixS3:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "scale"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Float;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Float;->floatValue()F

    move-result v17

    .line 448
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale3:Landroid/animation/ValueAnimator;

    move-object/from16 v19, v0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPreFixS3:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "scale_2"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Float;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Float;->floatValue()F

    move-result v18

    .line 451
    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 452
    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v9}, Landroid/graphics/Canvas;->translate(FF)V

    .line 453
    const v19, 0x42ce7ae1    # 103.24f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    const v20, 0x43257852    # 165.47f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v13, v13, v1, v2}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 454
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mLeftDrop1:Landroid/graphics/Path;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 455
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 457
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 458
    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v9}, Landroid/graphics/Canvas;->translate(FF)V

    .line 459
    const v19, 0x43087333    # 136.45f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    const v20, 0x43261c29    # 166.11f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v14, v14, v1, v2}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 460
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCenterDrop1:Landroid/graphics/Path;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 461
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 463
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 464
    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v9}, Landroid/graphics/Canvas;->translate(FF)V

    .line 465
    const v19, 0x43278f5c    # 167.56f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    const v20, 0x43257852    # 165.47f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v13, v13, v1, v2}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 466
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mRightDrop1:Landroid/graphics/Path;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 467
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 469
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 470
    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v10}, Landroid/graphics/Canvas;->translate(FF)V

    .line 471
    const v19, 0x42b547ae    # 90.64f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    const v20, 0x433adeb8    # 186.87f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, p1

    move/from16 v1, v16

    move/from16 v2, v16

    move/from16 v3, v19

    move/from16 v4, v20

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 472
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mLeftDrop2:Landroid/graphics/Path;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 473
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 475
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 476
    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v10}, Landroid/graphics/Canvas;->translate(FF)V

    .line 477
    const v19, 0x431b11ec    # 155.07f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    const/high16 v20, 0x433e0000    # 190.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, p1

    move/from16 v1, v16

    move/from16 v2, v16

    move/from16 v3, v19

    move/from16 v4, v20

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 478
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mRightDrop2:Landroid/graphics/Path;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 479
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 481
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 482
    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v10}, Landroid/graphics/Canvas;->translate(FF)V

    .line 483
    const v19, 0x42fb75c3    # 125.73f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    const v20, 0x4339a148    # 185.63f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v15, v15, v1, v2}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 484
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCenterDrop2:Landroid/graphics/Path;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 485
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 487
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 488
    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v11}, Landroid/graphics/Canvas;->translate(FF)V

    .line 489
    const v19, 0x42e75c29    # 115.68f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    const/high16 v20, 0x434d0000    # 205.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v18

    move/from16 v3, v19

    move/from16 v4, v20

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 490
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCenterDrop3:Landroid/graphics/Path;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 491
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 495
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mIsActiveAnimationThread:Z

    move/from16 v19, v0

    if-eqz v19, :cond_4

    .line 496
    const/4 v8, 0x0

    .line 497
    .local v8, "dcenterx_":F
    const/4 v12, 0x0

    .line 498
    .local v12, "dcentery_":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v19

    if-eqz v19, :cond_7

    .line 499
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;

    move-object/from16 v19, v0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPreFixT3_1:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "x"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Float;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Float;->floatValue()F

    move-result v8

    .line 500
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;

    move-object/from16 v19, v0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPreFixT3_1:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "y"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Float;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Float;->floatValue()F

    move-result v12

    .line 507
    :cond_3
    :goto_2
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 508
    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v12}, Landroid/graphics/Canvas;->translate(FF)V

    .line 509
    const v19, 0x42ce7ae1    # 103.24f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    const v20, 0x43257852    # 165.47f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v17

    move/from16 v3, v19

    move/from16 v4, v20

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 510
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mLeftDrop1:Landroid/graphics/Path;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 511
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 513
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 514
    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v12}, Landroid/graphics/Canvas;->translate(FF)V

    .line 515
    const v19, 0x43278f5c    # 167.56f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    const v20, 0x43257852    # 165.47f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v17

    move/from16 v3, v19

    move/from16 v4, v20

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 516
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mRightDrop1:Landroid/graphics/Path;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 517
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 521
    .end local v8    # "dcenterx_":F
    .end local v12    # "dcentery_":F
    :cond_4
    return-void

    .line 425
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->isStop:Z

    move/from16 v19, v0

    if-nez v19, :cond_0

    .line 426
    const/high16 v19, -0x3e380000    # -25.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v20, v0

    mul-float v9, v19, v20

    goto/16 :goto_0

    .line 434
    :cond_6
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->isStop:Z

    move/from16 v19, v0

    if-nez v19, :cond_1

    .line 435
    const/high16 v19, -0x3dcc0000    # -45.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v20, v0

    mul-float v10, v19, v20

    goto/16 :goto_1

    .line 502
    .restart local v8    # "dcenterx_":F
    .restart local v12    # "dcentery_":F
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->isStop:Z

    move/from16 v19, v0

    if-nez v19, :cond_3

    .line 503
    const/high16 v19, -0x3db80000    # -50.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v20, v0

    mul-float v12, v19, v20

    goto/16 :goto_2
.end method

.method private drawSun(Landroid/graphics/Canvas;Landroid/graphics/Paint;FFF)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "paint"    # Landroid/graphics/Paint;
    .param p3, "rotation"    # F
    .param p4, "s1"    # F
    .param p5, "s2"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const v4, 0x42a6dc29    # 83.43f

    const v3, 0x42a3999a    # 81.8f

    .line 350
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 351
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v0, v3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v4

    invoke-virtual {p1, p4, p4, v0, v1}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 352
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mSun:Landroid/graphics/Path;

    invoke-virtual {p1, v0, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 354
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 355
    const/4 v0, 0x0

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v4

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 356
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mSunOuter:Landroid/graphics/Path;

    invoke-virtual {p1, v0, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 357
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 359
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 360
    const/high16 v0, 0x42b40000    # 90.0f

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v4

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 361
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mSunOuter:Landroid/graphics/Path;

    invoke-virtual {p1, v0, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 362
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 364
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 365
    const/high16 v0, 0x43340000    # 180.0f

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v4

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 366
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mSunOuter:Landroid/graphics/Path;

    invoke-virtual {p1, v0, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 367
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 369
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 370
    const/high16 v0, 0x43870000    # 270.0f

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v4

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 371
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mSunOuter:Landroid/graphics/Path;

    invoke-virtual {p1, v0, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 372
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 373
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 375
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 376
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v0, v3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v4

    invoke-virtual {p1, p5, p5, v0, v1}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 378
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 379
    const/high16 v0, 0x42340000    # 45.0f

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v4

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 380
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mSunOuter:Landroid/graphics/Path;

    invoke-virtual {p1, v0, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 381
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 383
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 384
    const/high16 v0, 0x43070000    # 135.0f

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v4

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 385
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mSunOuter:Landroid/graphics/Path;

    invoke-virtual {p1, v0, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 386
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 388
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 389
    const/high16 v0, 0x43610000    # 225.0f

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v4

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 390
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mSunOuter:Landroid/graphics/Path;

    invoke-virtual {p1, v0, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 391
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 393
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 394
    const v0, 0x439d8000    # 315.0f

    add-float/2addr v0, p3

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v4

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 395
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mSunOuter:Landroid/graphics/Path;

    invoke-virtual {p1, v0, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 396
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 398
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 399
    return-void
.end method

.method private drawTransparentArea(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "paint"    # Landroid/graphics/Paint;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 335
    new-instance v0, Landroid/graphics/PorterDuffXfermode;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v1}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 336
    invoke-virtual {p1, p2, v2, v2, p3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 337
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 338
    return-void
.end method

.method private init()V
    .locals 4

    .prologue
    const/high16 v3, 0x43960000    # 300.0f

    .line 105
    invoke-virtual {p0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 106
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPaint:Landroid/graphics/Paint;

    .line 107
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->initPath()V

    .line 108
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPaintColor:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 109
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 110
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 111
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x40a00000    # 5.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 113
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mBitmap:Landroid/graphics/Bitmap;

    .line 115
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 116
    .local v0, "mMaskingCanvas":Landroid/graphics/Canvas;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mMasking:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 117
    return-void
.end method

.method private initPath()V
    .locals 13

    .prologue
    const v12, 0x428e1eb8    # 71.06f

    const v11, 0x4274b333    # 61.175f

    const v10, 0x43358fdf

    const v9, 0x43135df4

    const v8, 0x42a3e560

    .line 120
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mMasking:Landroid/graphics/Path;

    .line 121
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x433613f8

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42c44312

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 122
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x43435917

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42c4cfdf

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x434e045a

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42dad1ec    # 109.41f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x434e045a

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42f57ae1    # 122.74f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 124
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x434e045a

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43083e77

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x434311ec    # 195.07f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v9

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v10

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v9

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x42932f1b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v9

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 127
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x42857852    # 66.735f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v9

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v11

    const v4, 0x430da312

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v11

    const v6, 0x4306c7f0

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 129
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mMasking:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v11

    const v2, 0x42ffd917

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42857852    # 66.735f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42f48ed9    # 122.279f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42932f1b

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42f48ed9    # 122.279f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 131
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x4293ef1b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42f48ed9    # 122.279f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4294ac08

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42f4849c

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x429567f0

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42f494fe    # 122.291f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 133
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x4294d99a

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42f224dd

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42948c4a

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42ef91ec

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42948c4a

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42ecf5c3    # 118.48f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 135
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x42948c4a

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42da2c8b

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42a3c831

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42caea7f    # 101.458f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42b693f8

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42caea7f    # 101.458f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 137
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x42b6ab02    # 91.334f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42caea7f    # 101.458f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42b6c083    # 91.376f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42caeb02    # 101.459f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42b6d78d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42caeb02    # 101.459f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 139
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x42ce8fdf

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42b413f8

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 140
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x42d8b0a4

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a42b02    # 82.084f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42e8276d

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4298020c

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42fa6e98

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4292170a

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 142
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x4300dc6a

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x428fcdd3    # 71.902f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43052ed9

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v12

    const v5, 0x4309d2f2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v12

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 144
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x4318ad50

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v12

    const v3, 0x4326c625

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x429f52f2

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x432dbae1    # 173.73f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42b8b2b0    # 92.349f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 146
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mMasking:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 148
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    .line 149
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v10

    const v2, 0x431a2f9e

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 150
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x4346ce56    # 198.806f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x431a2f9e

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4354d5c3

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x430bfc29    # 139.985f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4354d5c3

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42f57b64

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 152
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x4354d5c3

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x42d30000    # 105.5f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4346ce56    # 198.806f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42b6f3b6

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v10

    const v6, 0x42b6f3b6

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 154
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x433554fe    # 181.332f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42b6f3b6

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43351a5e

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42b6c937

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4334df3b

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42b6cb44

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 156
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x432cfe77

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4296028f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431c51ec    # 156.32f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x428092f2

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4309d2f2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x428092f2

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 158
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x430776c9

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x428092f2

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43051e35

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4280e7f0

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4302cd0e

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4281999a    # 64.8f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 160
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42f04106

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42847efa

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42dd072b    # 110.514f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x428d8a3d    # 70.77f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42ce61cb

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x429d5810

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 162
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42c77cee

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a4c937

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42c1e354    # 96.944f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42ad4dd3    # 86.652f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42bda666

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42b691ec

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 164
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42bc8f5c    # 94.28f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42b8d375

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42bb87ae    # 93.765f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42bb1a1d

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42ba9db2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42bd7021

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 166
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42b9472b    # 92.639f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42bd53f8

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42b7ee98

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42bd4419

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42b693f8

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42bd4419

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 168
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x429db74c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42bd4419

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42893d71    # 68.62f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42d064dd

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42871893

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42e8b3b6

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 170
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x426f9375

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42edc7ae    # 118.89f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42596e98

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42fc91ec

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42596e98

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4306c873

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 172
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42596e98

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43116354    # 145.388f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x427bf2b0    # 62.987f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x431a2fdf

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42932f9e

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x431a2fdf

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 174
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v10

    const v2, 0x431a2fdf

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 175
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v10

    const v2, 0x431a2f9e

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 176
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 177
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v10

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v9

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 178
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42932f1b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v9

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 179
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42857852    # 66.735f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v9

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v11

    const v4, 0x430da312

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v11

    const v6, 0x4306c7f0

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 181
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v11

    const v2, 0x42ffd917

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42857852    # 66.735f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42f48ed9    # 122.279f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42932f1b

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42f48ed9    # 122.279f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 183
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x4293ef1b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42f48ed9    # 122.279f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4294ac08

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42f4849c

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x429567f0

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42f494fe    # 122.291f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 185
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x4294d99a

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42f224dd

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42948c4a

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42ef91ec

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42948c4a

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42ecf5c3    # 118.48f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 187
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42948c4a

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42da2c8b

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42a3c831

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42caea7f    # 101.458f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42b693f8

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42caea7f    # 101.458f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 189
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42b6ab02    # 91.334f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42caea7f    # 101.458f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42b6c083    # 91.376f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42caeb02    # 101.459f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42b6d78d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42caeb02    # 101.459f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 191
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42b52b85    # 90.585f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42d27be7

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42b44ccd    # 90.15f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42da578d

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42b44ccd    # 90.15f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42e26042

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 193
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42b44ccd    # 90.15f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42e6245a    # 115.071f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42b75a1d

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42e93021

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42bb1db2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42e93021

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 195
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42bee1cb

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42e93021

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42c1ef1b

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42e6224e

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42c1ef1b

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42e25e35

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 197
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42c1ef1b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42d0d4fe    # 104.416f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42c688b4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42c1170a

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42ce8fdf

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42b413f8

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 199
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x42d8b0a4

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a42b02    # 82.084f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42e8276d

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4298020c

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42fa6e98

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4292170a

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 201
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x4300dc6a

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x428fcdd3    # 71.902f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43052ed9

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v12

    const v5, 0x4309d2f2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v12

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 203
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x4318ad50

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v12

    const v3, 0x4326c625

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x429f52f2

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x432dbae1    # 173.73f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42b8b2b0    # 92.349f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 205
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x4325d021

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42bcbbe7

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431ee20c

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42c6a45a    # 99.321f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431a5e35

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42d4deb8

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 207
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x43195ba6

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42d80c4a

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4319d3b6

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42dc428f    # 110.13f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431b6ac1

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42de4625

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 209
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x431bfc29    # 155.985f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42defdf4

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431c9db2

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42df5604    # 111.668f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431d3df4

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42df5604    # 111.668f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 211
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x431e5eb8    # 158.37f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42df5604    # 111.668f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431f7917

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42de374c

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43201f7d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42dc2d91

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 213
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x4324dd2f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42cd3c6a

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x432bf581    # 171.959f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42c44312

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x433613f8

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42c44312

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 215
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x43435917

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42c4cfdf

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x434e045a

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42dad1ec    # 109.41f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x434e045a

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42f57ae1    # 122.74f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 217
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    const v1, 0x434e045a

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43083e77

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x434311ec    # 195.07f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v9

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v10

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v9

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 219
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 221
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mSunOuter:Landroid/graphics/Path;

    .line 222
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mSunOuter:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v8

    const v2, 0x41fc76c9    # 31.558f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 223
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mSunOuter:Landroid/graphics/Path;

    const v1, 0x42a01aa0

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41fc76c9    # 31.558f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x429d07ae    # 78.515f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x41f02b02    # 30.021f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x429d07ae    # 78.515f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x41e1020c    # 28.126f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 225
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mSunOuter:Landroid/graphics/Path;

    const v1, 0x429d07ae    # 78.515f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x413f78d5    # 11.967f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 226
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mSunOuter:Landroid/graphics/Path;

    const v1, 0x429d07ae    # 78.515f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x412122d1    # 10.071f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42a01aa0

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x41088b44    # 8.534f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v8

    const v6, 0x41088b44    # 8.534f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 228
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mSunOuter:Landroid/graphics/Path;

    const v1, 0x42a7b021

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41088b44    # 8.534f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42aac312

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x412122d1    # 10.071f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42aac312

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x413f78d5    # 11.967f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 230
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mSunOuter:Landroid/graphics/Path;

    const v1, 0x42aac312

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41e1020c    # 28.126f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 231
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mSunOuter:Landroid/graphics/Path;

    const v1, 0x42aac28f    # 85.38f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41f02b02    # 30.021f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42a7af9e

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x41fc76c9    # 31.558f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v8

    const v6, 0x41fc76c9    # 31.558f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 234
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mSun:Landroid/graphics/Path;

    .line 235
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mSun:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v8

    const v2, 0x42f5353f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 236
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mSun:Landroid/graphics/Path;

    const v1, 0x4271c8b4    # 60.446f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42f5353f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x422bced9    # 42.952f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42d23a5e

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x422bced9    # 42.952f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42a73cee

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 238
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mSun:Landroid/graphics/Path;

    const v1, 0x422bced9    # 42.952f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x427877cf    # 62.117f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4271c7ae    # 60.445f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42327be7

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v8

    const v6, 0x42327be7

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 240
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mSun:Landroid/graphics/Path;

    const v1, 0x42cee6e9

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42327be7

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42f1e979

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x427875c3

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42f1e979

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42a73cee

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 242
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mSun:Landroid/graphics/Path;

    const v1, 0x42f1e979

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42d23ae1

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42cee6e9

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42f5353f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v8

    const v6, 0x42f5353f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 244
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mSun:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 245
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mSun:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v8

    const v2, 0x424df1aa    # 51.486f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 246
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mSun:Landroid/graphics/Path;

    const v1, 0x42807646

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x424df1aa    # 51.486f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4247449c

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4283cd50    # 65.901f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4247449c

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42a73c6a

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 248
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mSun:Landroid/graphics/Path;

    const v1, 0x4247449c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42caa76d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42807646

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42e77ae1    # 115.74f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v8

    const v6, 0x42e77ae1    # 115.74f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 250
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mSun:Landroid/graphics/Path;

    const v1, 0x42c753f8

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42e77ae1    # 115.74f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42e42e14    # 114.09f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42caa7f0

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42e42e14    # 114.09f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42a73c6a

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 252
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mSun:Landroid/graphics/Path;

    const v1, 0x42e42e14    # 114.09f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4283cd50    # 65.901f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42c753f8

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x424df1aa    # 51.486f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v8

    const v6, 0x424df1aa    # 51.486f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 255
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mLeftDrop1:Landroid/graphics/Path;

    .line 256
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mLeftDrop1:Landroid/graphics/Path;

    const v1, 0x42cde24e

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x431fa5e3

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 257
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mLeftDrop1:Landroid/graphics/Path;

    const v1, 0x42c7126f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x431fa5e3

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42c187ae    # 96.765f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43226b44

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42c187ae    # 96.765f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4325d375

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 259
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mLeftDrop1:Landroid/graphics/Path;

    const v1, 0x42c187ae    # 96.765f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43293b64

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42c7126f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x432c0083

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42cde24e

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x432c0083

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 261
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mLeftDrop1:Landroid/graphics/Path;

    const v1, 0x42d4b2b0    # 106.349f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432c0083

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42da3df4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43293b64

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42da3df4

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4325d375

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 263
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mLeftDrop1:Landroid/graphics/Path;

    const v1, 0x42da3df4

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43226b85    # 162.42f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42d4b333    # 106.35f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x431fa5e3

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42cde24e

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x431fa5e3

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 266
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mLeftDrop2:Landroid/graphics/Path;

    .line 267
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mLeftDrop2:Landroid/graphics/Path;

    const v1, 0x42b4820c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43338189

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 268
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mLeftDrop2:Landroid/graphics/Path;

    const v1, 0x42ab9b23

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43338189

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42a45d2f    # 82.182f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43372083

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42a45d2f    # 82.182f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x433b93f8

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 270
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mLeftDrop2:Landroid/graphics/Path;

    const v1, 0x42a45d2f    # 82.182f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x434006e9

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42ab9b23

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4343a560

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42b4820c

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4343a560

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 272
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mLeftDrop2:Landroid/graphics/Path;

    const v1, 0x42bd6873

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4343a560

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42c4a666

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x434006e9

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42c4a666

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x433b93f8

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 274
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mLeftDrop2:Landroid/graphics/Path;

    const v1, 0x42c4a666

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43372083

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42bd6873

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43338189

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42b4820c

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43338189

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 277
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCenterDrop1:Landroid/graphics/Path;

    .line 278
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCenterDrop1:Landroid/graphics/Path;

    const v1, 0x43083f7d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x431dc189

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 279
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCenterDrop1:Landroid/graphics/Path;

    const v1, 0x4303cccd    # 131.8f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x431dc189

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43002e98

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43216083

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43002e98

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4325d375

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 281
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCenterDrop1:Landroid/graphics/Path;

    const v1, 0x43002e98

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432a4666

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4303cccd    # 131.8f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x432de4dd

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43083f7d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x432de4dd

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 283
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCenterDrop1:Landroid/graphics/Path;

    const v1, 0x430cb1ec

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432de4dd

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43105062

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x432a4666

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43105062

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4325d375

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 285
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCenterDrop1:Landroid/graphics/Path;

    const v1, 0x43105062

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43216083

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x430cb1ec

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x431dc189

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43083f7d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x431dc189

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 288
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCenterDrop2:Landroid/graphics/Path;

    .line 289
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCenterDrop2:Landroid/graphics/Path;

    const v1, 0x42fb6d91

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4333c76d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 290
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCenterDrop2:Landroid/graphics/Path;

    const v1, 0x42f50937

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4333c76d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42efd4fe    # 119.916f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43366189

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42efd4fe    # 119.916f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x433993f8

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 292
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCenterDrop2:Landroid/graphics/Path;

    const v1, 0x42efd4fe    # 119.916f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x433cc666

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42f50937

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x433f6083

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42fb6d91

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x433f6083

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 294
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCenterDrop2:Landroid/graphics/Path;

    const v1, 0x4300e937

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x433f6083

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43038396

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x433cc666

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43038396

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x433993f8

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 296
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCenterDrop2:Landroid/graphics/Path;

    const v1, 0x43038396

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43366189

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4300e937

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4333c76d

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42fb6d91

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4333c76d

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 299
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCenterDrop3:Landroid/graphics/Path;

    .line 300
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCenterDrop3:Landroid/graphics/Path;

    const v1, 0x42e75f3b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4345a5e3

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 301
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCenterDrop3:Landroid/graphics/Path;

    const v1, 0x42df4e56    # 111.653f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4345a5e3

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42d8be77

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4348edd3    # 200.929f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42d8be77

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x434cf604

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 303
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCenterDrop3:Landroid/graphics/Path;

    const v1, 0x42d8be77

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4350fdf4

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42df4e56    # 111.653f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x435445e3

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42e75f3b

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x435445e3

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 305
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCenterDrop3:Landroid/graphics/Path;

    const v1, 0x42ef6f9e

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x435445e3

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42f5ff7d    # 122.999f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4350fe77

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42f5ff7d    # 122.999f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x434cf604

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 307
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCenterDrop3:Landroid/graphics/Path;

    const v1, 0x42f5ff7d    # 122.999f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4348ed91

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42ef6f9e

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4345a5e3

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42e75f3b

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4345a5e3

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 310
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mRightDrop1:Landroid/graphics/Path;

    .line 311
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mRightDrop1:Landroid/graphics/Path;

    const v1, 0x432747f0

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4320676d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 312
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mRightDrop1:Landroid/graphics/Path;

    const v1, 0x43244b44

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4320676d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4321dcee

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4322d687

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4321dcee

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4325d375

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 314
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mRightDrop1:Landroid/graphics/Path;

    const v1, 0x4321dcee

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4328d062

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43244b44

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x432b3efa

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x432747f0

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x432b3efa

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 316
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mRightDrop1:Landroid/graphics/Path;

    const v1, 0x432a445a

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432b3efa

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x432cb2f2

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4328d062

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x432cb2f2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4325d375

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 318
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mRightDrop1:Landroid/graphics/Path;

    const v1, 0x432cb2f2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4322d687

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x432a451f    # 170.27f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4320676d

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x432747f0

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4320676d

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 321
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mRightDrop2:Landroid/graphics/Path;

    .line 322
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mRightDrop2:Landroid/graphics/Path;

    const v1, 0x431b3893

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43358189

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 323
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mRightDrop2:Landroid/graphics/Path;

    const v1, 0x43165b23

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43358189

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431265a2

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4339778d

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431265a2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x433e5581    # 190.334f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 325
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mRightDrop2:Landroid/graphics/Path;

    const v1, 0x431265a2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x434332f2

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43165b23

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43472873

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431b3893

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43472873

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 327
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mRightDrop2:Landroid/graphics/Path;

    const v1, 0x43201581    # 160.084f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43472873

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43240ac1

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x434332f2

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43240ac1

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x433e5581    # 190.334f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 329
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mRightDrop2:Landroid/graphics/Path;

    const v1, 0x43240ac1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4339778d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43201581    # 160.084f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43358189

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431b3893

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43358189

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 332
    return-void
.end method


# virtual methods
.method public cancelAnimation()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 810
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 811
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 813
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunRotation:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunRotation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 814
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunRotation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 816
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunScale1:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunScale1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 817
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunScale1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 819
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunScale2:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunScale2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 820
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunScale2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 823
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale1:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 824
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 826
    :cond_4
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale2:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 827
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 830
    :cond_5
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale3:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 831
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 834
    :cond_6
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 835
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 838
    :cond_7
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 839
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 842
    :cond_8
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT3:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 843
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 846
    :cond_9
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 847
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 850
    :cond_a
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mIsActiveAnimationThread:Z

    .line 851
    return-void
.end method

.method public getScale()F
    .locals 1

    .prologue
    .line 892
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 580
    return-object p0
.end method

.method public isRunning()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 859
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunRotation:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunScale1:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunScale2:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale1:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale2:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale3:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT3:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;

    if-nez v1, :cond_1

    .line 883
    :cond_0
    :goto_0
    return v0

    .line 873
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunRotation:Landroid/animation/ValueAnimator;

    .line 874
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunScale1:Landroid/animation/ValueAnimator;

    .line 875
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunScale2:Landroid/animation/ValueAnimator;

    .line 876
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale1:Landroid/animation/ValueAnimator;

    .line 877
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale2:Landroid/animation/ValueAnimator;

    .line 878
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale3:Landroid/animation/ValueAnimator;

    .line 879
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    .line 880
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    .line 881
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT3:Landroid/animation/ValueAnimator;

    .line 882
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;

    .line 883
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v4, 0x43960000    # 300.0f

    const/4 v2, 0x0

    .line 525
    :try_start_0
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 527
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    cmpg-float v0, v0, v2

    if-gtz v0, :cond_1

    .line 528
    const-string v0, ""

    const-string v1, "scale is less then 0"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 577
    :cond_0
    :goto_0
    return-void

    .line 532
    :cond_1
    const/4 v7, 0x0

    .line 533
    .local v7, "dx":F
    const/4 v8, 0x0

    .line 534
    .local v8, "dy":F
    const/4 v10, 0x0

    .line 535
    .local v10, "rotation":F
    const/high16 v11, 0x3f800000    # 1.0f

    .line 536
    .local v11, "scale1":F
    const/high16 v12, 0x3f800000    # 1.0f

    .line 538
    .local v12, "scale2":F
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mIsActiveAnimationThread:Z

    if-eqz v0, :cond_2

    .line 539
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    const-string v1, "x"

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v7

    .line 540
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    const-string v1, "y"

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v8

    .line 542
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunRotation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->rotateOffset:F

    add-float v10, v0, v1

    .line 544
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunScale1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v11

    .line 545
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunScale2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 546
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunScale2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v12

    .line 550
    :cond_2
    const/16 v6, 0x1f

    .line 552
    .local v6, "saveFlag":I
    const/16 v6, 0xf

    .line 555
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float v1, v2, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v2, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float v3, v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v4, v0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->saveLayer(FFFFLandroid/graphics/Paint;I)I

    move-result v13

    .line 558
    .local v13, "transparentSaveLevel":I
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPaint:Landroid/graphics/Paint;

    move-object v0, p0

    move-object v1, p1

    move v3, v10

    move v4, v11

    move v5, v12

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->drawSun(Landroid/graphics/Canvas;Landroid/graphics/Paint;FFF)V

    .line 560
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->drawDrop(Landroid/graphics/Canvas;)V

    .line 562
    neg-float v0, v7

    const/high16 v1, 0x40a00000    # 5.0f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    add-float/2addr v1, v8

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 563
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPaint:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->drawTransparentArea(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V

    .line 565
    invoke-virtual {p1, v13}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 567
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPaint:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0, v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->drawCloud(Landroid/graphics/Canvas;Landroid/graphics/Paint;FF)V

    .line 569
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 570
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->cancelAnimation()V

    .line 571
    const-string v0, ""

    const-string v1, "cancelA"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 574
    .end local v6    # "saveFlag":I
    .end local v7    # "dx":F
    .end local v8    # "dy":F
    .end local v10    # "rotation":F
    .end local v11    # "scale1":F
    .end local v12    # "scale2":F
    .end local v13    # "transparentSaveLevel":I
    :catch_0
    move-exception v9

    .line 575
    .local v9, "e":Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0
.end method

.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 984
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 901
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onViewDetachedFromWindow : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 902
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 903
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 904
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 906
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunRotation:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_1

    .line 907
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunRotation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 908
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunRotation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 910
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunScale1:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_2

    .line 911
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunScale1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 912
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunScale1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 914
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunScale2:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_3

    .line 915
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunScale2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 916
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunScale2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 919
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale1:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_4

    .line 920
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 921
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 923
    :cond_4
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale2:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_5

    .line 924
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 925
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 928
    :cond_5
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale3:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_6

    .line 929
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 930
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 933
    :cond_6
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_7

    .line 934
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 935
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 938
    :cond_7
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_8

    .line 939
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 940
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 943
    :cond_8
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT3:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_9

    .line 944
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 945
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 948
    :cond_9
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_a

    .line 949
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 950
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 953
    :cond_a
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    .line 954
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunRotation:Landroid/animation/ValueAnimator;

    .line 955
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunScale1:Landroid/animation/ValueAnimator;

    .line 956
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunScale2:Landroid/animation/ValueAnimator;

    .line 957
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale1:Landroid/animation/ValueAnimator;

    .line 958
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale2:Landroid/animation/ValueAnimator;

    .line 959
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale3:Landroid/animation/ValueAnimator;

    .line 960
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    .line 961
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    .line 962
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT3:Landroid/animation/ValueAnimator;

    .line 963
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;

    .line 964
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPaint:Landroid/graphics/Paint;

    .line 965
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mMasking:Landroid/graphics/Path;

    .line 966
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCloud:Landroid/graphics/Path;

    .line 967
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mSun:Landroid/graphics/Path;

    .line 968
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mSunOuter:Landroid/graphics/Path;

    .line 969
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mLeftDrop1:Landroid/graphics/Path;

    .line 970
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mLeftDrop2:Landroid/graphics/Path;

    .line 971
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCenterDrop1:Landroid/graphics/Path;

    .line 972
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCenterDrop2:Landroid/graphics/Path;

    .line 973
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mCenterDrop3:Landroid/graphics/Path;

    .line 974
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mRightDrop1:Landroid/graphics/Path;

    .line 975
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mRightDrop2:Landroid/graphics/Path;

    .line 976
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_b

    .line 977
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 979
    :cond_b
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mBitmap:Landroid/graphics/Bitmap;

    .line 980
    return-void
.end method

.method public setPaintColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 88
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPaintColor:I

    .line 89
    return-void
.end method

.method public setScale(F)V
    .locals 0
    .param p1, "scale"    # F

    .prologue
    .line 887
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    .line 888
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->init()V

    .line 889
    return-void
.end method

.method public setWidthAndHeight(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 896
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, p1, p2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 897
    .local v0, "l":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 899
    return-void
.end method

.method public startAnimation()V
    .locals 27

    .prologue
    .line 645
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->isStop:Z

    .line 646
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->rotateOffset:F

    .line 647
    const-string v20, ""

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPreFixS1:Ljava/lang/String;

    .line 648
    const-string v20, ""

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPreFixS2:Ljava/lang/String;

    .line 649
    const-string v20, ""

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPreFixS3:Ljava/lang/String;

    .line 650
    const-string v20, ""

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPreFixT1:Ljava/lang/String;

    .line 651
    const-string v20, ""

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPreFixT2:Ljava/lang/String;

    .line 652
    const-string v20, ""

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPreFixT3:Ljava/lang/String;

    .line 653
    const-string v20, ""

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mPreFixT3_1:Ljava/lang/String;

    .line 656
    const-wide/16 v4, 0x5dc

    .line 657
    .local v4, "duration":J
    const-wide/16 v20, 0x3

    div-long v2, v4, v20

    .line 659
    .local v2, "delayGap":J
    const/16 v20, 0x0

    const/high16 v21, 0x3f800000    # 1.0f

    invoke-static/range {v20 .. v21}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v7

    .line 660
    .local v7, "key1":Landroid/animation/Keyframe;
    const v20, 0x3e99999a    # 0.3f

    const/high16 v21, 0x3fc00000    # 1.5f

    invoke-static/range {v20 .. v21}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v9

    .line 661
    .local v9, "key2":Landroid/animation/Keyframe;
    const v20, 0x3f19999a    # 0.6f

    const v21, 0x3f4ccccd    # 0.8f

    invoke-static/range {v20 .. v21}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v11

    .line 662
    .local v11, "key3":Landroid/animation/Keyframe;
    const v20, 0x3f666666    # 0.9f

    const/high16 v21, 0x3fc00000    # 1.5f

    invoke-static/range {v20 .. v21}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v13

    .line 663
    .local v13, "key4":Landroid/animation/Keyframe;
    const v20, 0x3f733333    # 0.95f

    const/16 v21, 0x0

    invoke-static/range {v20 .. v21}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v15

    .line 664
    .local v15, "key5":Landroid/animation/Keyframe;
    const/high16 v20, 0x3f800000    # 1.0f

    const/16 v21, 0x0

    invoke-static/range {v20 .. v21}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v17

    .line 666
    .local v17, "key6":Landroid/animation/Keyframe;
    const/16 v20, 0x0

    const/high16 v21, 0x3f800000    # 1.0f

    invoke-static/range {v20 .. v21}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v8

    .line 667
    .local v8, "key1_":Landroid/animation/Keyframe;
    const v20, 0x3e99999a    # 0.3f

    const/high16 v21, 0x3f000000    # 0.5f

    invoke-static/range {v20 .. v21}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v10

    .line 668
    .local v10, "key2_":Landroid/animation/Keyframe;
    const v20, 0x3f19999a    # 0.6f

    const v21, 0x3f99999a    # 1.2f

    invoke-static/range {v20 .. v21}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v12

    .line 669
    .local v12, "key3_":Landroid/animation/Keyframe;
    const v20, 0x3f666666    # 0.9f

    const/high16 v21, 0x3f000000    # 0.5f

    invoke-static/range {v20 .. v21}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v14

    .line 670
    .local v14, "key4_":Landroid/animation/Keyframe;
    const v20, 0x3f733333    # 0.95f

    const/16 v21, 0x0

    invoke-static/range {v20 .. v21}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v16

    .line 671
    .local v16, "key5_":Landroid/animation/Keyframe;
    const/high16 v20, 0x3f800000    # 1.0f

    const/16 v21, 0x0

    invoke-static/range {v20 .. v21}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v18

    .line 673
    .local v18, "key6_":Landroid/animation/Keyframe;
    const v20, 0x3e99999a    # 0.3f

    const/high16 v21, 0x3f800000    # 1.0f

    invoke-static/range {v20 .. v21}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v6

    .line 674
    .local v6, "endkey":Landroid/animation/Keyframe;
    const/16 v20, 0x4

    move/from16 v0, v20

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const-string v22, "scale"

    const/16 v23, 0x6

    move/from16 v0, v23

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aput-object v7, v23, v24

    const/16 v24, 0x1

    aput-object v9, v23, v24

    const/16 v24, 0x2

    aput-object v11, v23, v24

    const/16 v24, 0x3

    aput-object v13, v23, v24

    const/16 v24, 0x4

    aput-object v15, v23, v24

    const/16 v24, 0x5

    aput-object v17, v23, v24

    .line 675
    invoke-static/range {v22 .. v23}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x1

    const-string v22, "scale_2"

    const/16 v23, 0x6

    move/from16 v0, v23

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aput-object v8, v23, v24

    const/16 v24, 0x1

    aput-object v10, v23, v24

    const/16 v24, 0x2

    aput-object v12, v23, v24

    const/16 v24, 0x3

    aput-object v14, v23, v24

    const/16 v24, 0x4

    aput-object v16, v23, v24

    const/16 v24, 0x5

    aput-object v18, v23, v24

    .line 676
    invoke-static/range {v22 .. v23}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x2

    const-string v22, "e_scale"

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aput-object v7, v23, v24

    const/16 v24, 0x1

    aput-object v6, v23, v24

    .line 677
    invoke-static/range {v22 .. v23}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x3

    const-string v22, "e_scale_2"

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aput-object v8, v23, v24

    const/16 v24, 0x1

    aput-object v6, v23, v24

    .line 678
    invoke-static/range {v22 .. v23}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v22

    aput-object v22, v20, v21

    .line 674
    invoke-static/range {v20 .. v20}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale1:Landroid/animation/ValueAnimator;

    .line 679
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale1:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    const/16 v21, -0x1

    invoke-virtual/range {v20 .. v21}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 680
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale1:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 681
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale1:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    const-wide/16 v21, 0x2

    mul-long v21, v21, v2

    invoke-virtual/range {v20 .. v22}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 682
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale1:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    new-instance v21, Landroid/view/animation/LinearInterpolator;

    invoke-direct/range {v21 .. v21}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual/range {v20 .. v21}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 683
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale1:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 684
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale1:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/animation/ValueAnimator;->start()V

    .line 686
    const/16 v20, 0x4

    move/from16 v0, v20

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const-string v22, "scale"

    const/16 v23, 0x6

    move/from16 v0, v23

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aput-object v7, v23, v24

    const/16 v24, 0x1

    aput-object v9, v23, v24

    const/16 v24, 0x2

    aput-object v11, v23, v24

    const/16 v24, 0x3

    aput-object v13, v23, v24

    const/16 v24, 0x4

    aput-object v15, v23, v24

    const/16 v24, 0x5

    aput-object v17, v23, v24

    .line 687
    invoke-static/range {v22 .. v23}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x1

    const-string v22, "scale_2"

    const/16 v23, 0x6

    move/from16 v0, v23

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aput-object v8, v23, v24

    const/16 v24, 0x1

    aput-object v10, v23, v24

    const/16 v24, 0x2

    aput-object v12, v23, v24

    const/16 v24, 0x3

    aput-object v14, v23, v24

    const/16 v24, 0x4

    aput-object v16, v23, v24

    const/16 v24, 0x5

    aput-object v18, v23, v24

    .line 688
    invoke-static/range {v22 .. v23}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x2

    const-string v22, "e_scale"

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aput-object v7, v23, v24

    const/16 v24, 0x1

    aput-object v6, v23, v24

    .line 689
    invoke-static/range {v22 .. v23}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x3

    const-string v22, "e_scale_2"

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aput-object v8, v23, v24

    const/16 v24, 0x1

    aput-object v6, v23, v24

    .line 690
    invoke-static/range {v22 .. v23}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v22

    aput-object v22, v20, v21

    .line 686
    invoke-static/range {v20 .. v20}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale2:Landroid/animation/ValueAnimator;

    .line 691
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale2:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    const/16 v21, -0x1

    invoke-virtual/range {v20 .. v21}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 692
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale2:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 693
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale2:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 694
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale2:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    new-instance v21, Landroid/view/animation/LinearInterpolator;

    invoke-direct/range {v21 .. v21}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual/range {v20 .. v21}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 695
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale2:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 696
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale2:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/animation/ValueAnimator;->start()V

    .line 698
    const/16 v20, 0x4

    move/from16 v0, v20

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const-string v22, "scale"

    const/16 v23, 0x6

    move/from16 v0, v23

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aput-object v7, v23, v24

    const/16 v24, 0x1

    aput-object v9, v23, v24

    const/16 v24, 0x2

    aput-object v11, v23, v24

    const/16 v24, 0x3

    aput-object v13, v23, v24

    const/16 v24, 0x4

    aput-object v15, v23, v24

    const/16 v24, 0x5

    aput-object v17, v23, v24

    .line 699
    invoke-static/range {v22 .. v23}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x1

    const-string v22, "scale_2"

    const/16 v23, 0x6

    move/from16 v0, v23

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aput-object v8, v23, v24

    const/16 v24, 0x1

    aput-object v10, v23, v24

    const/16 v24, 0x2

    aput-object v12, v23, v24

    const/16 v24, 0x3

    aput-object v14, v23, v24

    const/16 v24, 0x4

    aput-object v16, v23, v24

    const/16 v24, 0x5

    aput-object v18, v23, v24

    .line 700
    invoke-static/range {v22 .. v23}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x2

    const-string v22, "e_scale"

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aput-object v7, v23, v24

    const/16 v24, 0x1

    aput-object v6, v23, v24

    .line 701
    invoke-static/range {v22 .. v23}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x3

    const-string v22, "e_scale_2"

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aput-object v8, v23, v24

    const/16 v24, 0x1

    aput-object v6, v23, v24

    .line 702
    invoke-static/range {v22 .. v23}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v22

    aput-object v22, v20, v21

    .line 698
    invoke-static/range {v20 .. v20}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale3:Landroid/animation/ValueAnimator;

    .line 703
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale3:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    const/16 v21, -0x1

    invoke-virtual/range {v20 .. v21}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 704
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale3:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 705
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale3:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    new-instance v21, Landroid/view/animation/LinearInterpolator;

    invoke-direct/range {v21 .. v21}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual/range {v20 .. v21}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 706
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale3:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 707
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropScale3:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/animation/ValueAnimator;->start()V

    .line 709
    const/16 v20, 0x4

    move/from16 v0, v20

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const-string v22, "y"

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [F

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const/high16 v25, -0x3e380000    # -25.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v26, v0

    mul-float v25, v25, v26

    aput v25, v23, v24

    const/16 v24, 0x1

    const/high16 v25, 0x42700000    # 60.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v26, v0

    mul-float v25, v25, v26

    aput v25, v23, v24

    .line 710
    invoke-static/range {v22 .. v23}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x1

    const-string v22, "x"

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [F

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const/high16 v25, 0x41200000    # 10.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v26, v0

    mul-float v25, v25, v26

    aput v25, v23, v24

    const/16 v24, 0x1

    const/high16 v25, -0x3de00000    # -40.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v26, v0

    mul-float v25, v25, v26

    aput v25, v23, v24

    .line 711
    invoke-static/range {v22 .. v23}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x2

    const-string v22, "e_y"

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [F

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const/high16 v25, -0x3e380000    # -25.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v26, v0

    mul-float v25, v25, v26

    aput v25, v23, v24

    const/16 v24, 0x1

    const/16 v25, 0x0

    aput v25, v23, v24

    .line 712
    invoke-static/range {v22 .. v23}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x3

    const-string v22, "e_x"

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [F

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const/high16 v25, 0x41200000    # 10.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v26, v0

    mul-float v25, v25, v26

    aput v25, v23, v24

    const/16 v24, 0x1

    const/16 v25, 0x0

    aput v25, v23, v24

    .line 713
    invoke-static/range {v22 .. v23}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v22

    aput-object v22, v20, v21

    .line 709
    invoke-static/range {v20 .. v20}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    .line 715
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    const/16 v21, -0x1

    invoke-virtual/range {v20 .. v21}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 716
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 717
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    const-wide/16 v21, 0x2

    mul-long v21, v21, v2

    invoke-virtual/range {v20 .. v22}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 718
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    new-instance v21, Landroid/view/animation/LinearInterpolator;

    invoke-direct/range {v21 .. v21}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual/range {v20 .. v21}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 719
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 720
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT1:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/animation/ValueAnimator;->start()V

    .line 722
    const/16 v20, 0x4

    move/from16 v0, v20

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const-string v22, "y"

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [F

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const/high16 v25, -0x3dcc0000    # -45.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v26, v0

    mul-float v25, v25, v26

    aput v25, v23, v24

    const/16 v24, 0x1

    const/high16 v25, 0x42200000    # 40.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v26, v0

    mul-float v25, v25, v26

    aput v25, v23, v24

    .line 723
    invoke-static/range {v22 .. v23}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x1

    const-string v22, "x"

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [F

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const/high16 v25, 0x41a00000    # 20.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v26, v0

    mul-float v25, v25, v26

    aput v25, v23, v24

    const/16 v24, 0x1

    const/high16 v25, -0x3e100000    # -30.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v26, v0

    mul-float v25, v25, v26

    aput v25, v23, v24

    .line 724
    invoke-static/range {v22 .. v23}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x2

    const-string v22, "e_y"

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [F

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const/high16 v25, -0x3dcc0000    # -45.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v26, v0

    mul-float v25, v25, v26

    aput v25, v23, v24

    const/16 v24, 0x1

    const/16 v25, 0x0

    aput v25, v23, v24

    .line 725
    invoke-static/range {v22 .. v23}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x3

    const-string v22, "e_x"

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [F

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const/high16 v25, 0x41a00000    # 20.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v26, v0

    mul-float v25, v25, v26

    aput v25, v23, v24

    const/16 v24, 0x1

    const/16 v25, 0x0

    aput v25, v23, v24

    .line 726
    invoke-static/range {v22 .. v23}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v22

    aput-object v22, v20, v21

    .line 722
    invoke-static/range {v20 .. v20}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    .line 728
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    const/16 v21, -0x1

    invoke-virtual/range {v20 .. v21}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 729
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 730
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 731
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    new-instance v21, Landroid/view/animation/LinearInterpolator;

    invoke-direct/range {v21 .. v21}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual/range {v20 .. v21}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 732
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 733
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT2:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/animation/ValueAnimator;->start()V

    .line 735
    const/16 v20, 0x4

    move/from16 v0, v20

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const-string v22, "y"

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [F

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const/high16 v25, -0x3d900000    # -60.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v26, v0

    mul-float v25, v25, v26

    aput v25, v23, v24

    const/16 v24, 0x1

    const/high16 v25, 0x41c80000    # 25.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v26, v0

    mul-float v25, v25, v26

    aput v25, v23, v24

    .line 736
    invoke-static/range {v22 .. v23}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x1

    const-string v22, "x"

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [F

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const/high16 v25, 0x41a00000    # 20.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v26, v0

    mul-float v25, v25, v26

    aput v25, v23, v24

    const/16 v24, 0x1

    const/high16 v25, -0x3e600000    # -20.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v26, v0

    mul-float v25, v25, v26

    aput v25, v23, v24

    .line 737
    invoke-static/range {v22 .. v23}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x2

    const-string v22, "e_y"

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [F

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const/high16 v25, -0x3dcc0000    # -45.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v26, v0

    mul-float v25, v25, v26

    aput v25, v23, v24

    const/16 v24, 0x1

    const/16 v25, 0x0

    aput v25, v23, v24

    .line 738
    invoke-static/range {v22 .. v23}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x3

    const-string v22, "e_x"

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [F

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const/high16 v25, 0x41a00000    # 20.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v26, v0

    mul-float v25, v25, v26

    aput v25, v23, v24

    const/16 v24, 0x1

    const/16 v25, 0x0

    aput v25, v23, v24

    .line 739
    invoke-static/range {v22 .. v23}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v22

    aput-object v22, v20, v21

    .line 735
    invoke-static/range {v20 .. v20}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT3:Landroid/animation/ValueAnimator;

    .line 741
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT3:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    const/16 v21, -0x1

    invoke-virtual/range {v20 .. v21}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 742
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT3:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 743
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT3:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    new-instance v21, Landroid/view/animation/LinearInterpolator;

    invoke-direct/range {v21 .. v21}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual/range {v20 .. v21}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 744
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT3:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 745
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT3:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/animation/ValueAnimator;->start()V

    .line 747
    const/16 v20, 0x4

    move/from16 v0, v20

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const-string v22, "y"

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [F

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const/high16 v25, -0x3e380000    # -25.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v26, v0

    mul-float v25, v25, v26

    aput v25, v23, v24

    const/16 v24, 0x1

    const/high16 v25, 0x42700000    # 60.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v26, v0

    mul-float v25, v25, v26

    aput v25, v23, v24

    .line 748
    invoke-static/range {v22 .. v23}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x1

    const-string v22, "x"

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [F

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const/high16 v25, 0x41200000    # 10.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v26, v0

    mul-float v25, v25, v26

    aput v25, v23, v24

    const/16 v24, 0x1

    const/high16 v25, -0x3e100000    # -30.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v26, v0

    mul-float v25, v25, v26

    aput v25, v23, v24

    .line 749
    invoke-static/range {v22 .. v23}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x2

    const-string v22, "e_y"

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [F

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const/high16 v25, -0x3e380000    # -25.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v26, v0

    mul-float v25, v25, v26

    aput v25, v23, v24

    const/16 v24, 0x1

    const/16 v25, 0x0

    aput v25, v23, v24

    .line 750
    invoke-static/range {v22 .. v23}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x3

    const-string v22, "e_x"

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [F

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const/high16 v25, 0x41200000    # 10.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v26, v0

    mul-float v25, v25, v26

    aput v25, v23, v24

    const/16 v24, 0x1

    const/16 v25, 0x0

    aput v25, v23, v24

    .line 751
    invoke-static/range {v22 .. v23}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v22

    aput-object v22, v20, v21

    .line 747
    invoke-static/range {v20 .. v20}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;

    .line 753
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    const/16 v21, -0x1

    invoke-virtual/range {v20 .. v21}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 754
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 755
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 756
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    new-instance v21, Landroid/view/animation/LinearInterpolator;

    invoke-direct/range {v21 .. v21}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual/range {v20 .. v21}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 757
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 758
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->DropCenterT3_1:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/animation/ValueAnimator;->start()V

    .line 760
    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const-string v22, "y"

    const/16 v23, 0x3

    move/from16 v0, v23

    new-array v0, v0, [F

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const/16 v25, 0x0

    aput v25, v23, v24

    const/16 v24, 0x1

    const v25, -0x3fd33333    # -2.7f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v26, v0

    mul-float v25, v25, v26

    aput v25, v23, v24

    const/16 v24, 0x2

    const/16 v25, 0x0

    aput v25, v23, v24

    .line 761
    invoke-static/range {v22 .. v23}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x1

    const-string v22, "x"

    const/16 v23, 0x5

    move/from16 v0, v23

    new-array v0, v0, [F

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const/16 v25, 0x0

    aput v25, v23, v24

    const/16 v24, 0x1

    const v25, -0x3f933333    # -3.7f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v26, v0

    mul-float v25, v25, v26

    aput v25, v23, v24

    const/16 v24, 0x2

    const/16 v25, 0x0

    aput v25, v23, v24

    const/16 v24, 0x3

    const/high16 v25, 0x40c00000    # 6.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mScale:F

    move/from16 v26, v0

    mul-float v25, v25, v26

    aput v25, v23, v24

    const/16 v24, 0x4

    const/16 v25, 0x0

    aput v25, v23, v24

    .line 762
    invoke-static/range {v22 .. v23}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v22

    aput-object v22, v20, v21

    .line 760
    invoke-static/range {v20 .. v20}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    .line 764
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    const/16 v21, -0x1

    invoke-virtual/range {v20 .. v21}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 765
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    const-wide/16 v21, 0x7d0

    invoke-virtual/range {v20 .. v22}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 766
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 767
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->CloudTranslate:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/animation/ValueAnimator;->start()V

    .line 769
    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [F

    move-object/from16 v20, v0

    fill-array-data v20, :array_0

    invoke-static/range {v20 .. v20}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunRotation:Landroid/animation/ValueAnimator;

    .line 770
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunRotation:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    const/16 v21, -0x1

    invoke-virtual/range {v20 .. v21}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 771
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunRotation:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    const-wide/16 v21, 0x7d0

    invoke-virtual/range {v20 .. v22}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 772
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunRotation:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 773
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunRotation:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    new-instance v21, Landroid/view/animation/LinearInterpolator;

    invoke-direct/range {v21 .. v21}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual/range {v20 .. v21}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 774
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunRotation:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/animation/ValueAnimator;->start()V

    .line 776
    const/16 v20, 0x3

    move/from16 v0, v20

    new-array v0, v0, [F

    move-object/from16 v20, v0

    fill-array-data v20, :array_1

    invoke-static/range {v20 .. v20}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunScale1:Landroid/animation/ValueAnimator;

    .line 777
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunScale1:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    const/16 v21, -0x1

    invoke-virtual/range {v20 .. v21}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 778
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunScale1:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    const-wide/16 v21, 0x514

    invoke-virtual/range {v20 .. v22}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 779
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunScale1:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 780
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunScale1:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    new-instance v21, Landroid/view/animation/LinearInterpolator;

    invoke-direct/range {v21 .. v21}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual/range {v20 .. v21}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 781
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunScale1:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/animation/ValueAnimator;->start()V

    .line 783
    const/16 v20, 0x3

    move/from16 v0, v20

    new-array v0, v0, [F

    move-object/from16 v20, v0

    fill-array-data v20, :array_2

    invoke-static/range {v20 .. v20}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunScale2:Landroid/animation/ValueAnimator;

    .line 784
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunScale2:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    const/16 v21, -0x1

    invoke-virtual/range {v20 .. v21}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 785
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunScale2:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    const-wide/16 v21, 0x514

    invoke-virtual/range {v20 .. v22}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 786
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunScale2:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    const-wide/16 v21, 0x190

    invoke-virtual/range {v20 .. v22}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 787
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunScale2:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    new-instance v21, Landroid/view/animation/LinearInterpolator;

    invoke-direct/range {v21 .. v21}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual/range {v20 .. v21}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 788
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunScale2:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 789
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->SunScale2:Landroid/animation/ValueAnimator;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/animation/ValueAnimator;->start()V

    .line 791
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->mIsActiveAnimationThread:Z

    .line 792
    new-instance v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView$2;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;)V

    .line 805
    .local v19, "t":Ljava/lang/Thread;
    const/16 v20, 0xa

    invoke-virtual/range {v19 .. v20}, Ljava/lang/Thread;->setPriority(I)V

    .line 806
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Thread;->start()V

    .line 807
    return-void

    .line 769
    :array_0
    .array-data 4
        0x0
        0x42340000    # 45.0f
    .end array-data

    .line 776
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x3f8ccccd    # 1.1f
        0x3f800000    # 1.0f
    .end array-data

    .line 783
    :array_2
    .array-data 4
        0x3f800000    # 1.0f
        0x3f8ccccd    # 1.1f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public stopAnimation()V
    .locals 1

    .prologue
    .line 855
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/PartlySunnyFlurriesIconView;->isStop:Z

    .line 856
    return-void
.end method
