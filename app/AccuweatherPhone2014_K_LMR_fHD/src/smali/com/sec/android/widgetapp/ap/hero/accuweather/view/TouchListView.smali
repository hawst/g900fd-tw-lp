.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;
.super Landroid/widget/ListView;
.source "TouchListView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView$list;
    }
.end annotation


# instance fields
.field action:I

.field center_vertical:F

.field holding:Z

.field movecounts:I

.field points:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 23
    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 12
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;->center_vertical:F

    .line 33
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;->action:I

    .line 35
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;->points:I

    .line 37
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;->movecounts:I

    .line 39
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;->holding:Z

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 19
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 12
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;->center_vertical:F

    .line 33
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;->action:I

    .line 35
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;->points:I

    .line 37
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;->movecounts:I

    .line 39
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;->holding:Z

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    .line 15
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 12
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;->center_vertical:F

    .line 33
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;->action:I

    .line 35
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;->points:I

    .line 37
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;->movecounts:I

    .line 39
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;->holding:Z

    .line 16
    return-void
.end method


# virtual methods
.method protected layoutChildren()V
    .locals 2

    .prologue
    .line 27
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;->center_vertical:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 28
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;->getHeight()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;->center_vertical:F

    .line 30
    :cond_0
    invoke-super {p0}, Landroid/widget/ListView;->layoutChildren()V

    .line 31
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 42
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    and-int/lit16 v1, v1, 0xff

    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;->action:I

    .line 43
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;->action:I

    packed-switch v1, :pswitch_data_0

    .line 63
    :cond_0
    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_1
    return v0

    .line 45
    :pswitch_1
    iput v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;->movecounts:I

    goto :goto_0

    .line 48
    :pswitch_2
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;->movecounts:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;->movecounts:I

    goto :goto_0

    .line 51
    :pswitch_3
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;->points:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;->points:I

    .line 52
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;->movecounts:I

    const/16 v2, 0xa

    if-ge v1, v2, :cond_0

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;->points:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 53
    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;->points:I

    .line 55
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;->center_vertical:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_1

    .line 56
    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;->setSelection(I)V

    .line 59
    :goto_2
    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;->holding:Z

    goto :goto_1

    .line 58
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;->setSelection(I)V

    goto :goto_2

    .line 43
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/widget/AdapterView$OnItemClickListener;

    .prologue
    .line 67
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView$list;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView$list;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/view/TouchListView;Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-super {p0, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 68
    return-void
.end method
