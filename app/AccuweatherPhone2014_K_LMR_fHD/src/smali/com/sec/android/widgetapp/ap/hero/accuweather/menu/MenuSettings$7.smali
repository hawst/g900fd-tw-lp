.class Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$7;
.super Ljava/lang/Object;
.source "MenuSettings.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

.field final synthetic val$confirm:Landroid/app/Dialog;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;Landroid/app/Dialog;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    .prologue
    .line 1474
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$7;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    iput-object p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$7;->val$confirm:Landroid/app/Dialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 1476
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$7;->val$confirm:Landroid/app/Dialog;

    const v2, 0x7f08003c

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultCheckBox;

    check-cast v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultCheckBox;

    .line 1477
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultCheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$7;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    .line 1478
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getButtenTypeCheck()I

    move-result v1

    const/16 v2, 0xa

    if-ne v1, v2, :cond_0

    .line 1479
    const-string v1, ""

    const-string v2, "on dismiss"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1480
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings$7;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;

    const-string v2, "config"

    const/4 v3, 0x4

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSettings;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1481
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "CONFIRM_AUTOREFRESH"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1482
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1484
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    return-void
.end method
