.class Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$3;
.super Ljava/lang/Object;
.source "MenuDefaultLocation.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->onCreateOptionsMenu(Landroid/view/Menu;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;

    .prologue
    .line 154
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 156
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->defaultLocation:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateSelectDefaultdLocation(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    .line 157
    .local v0, "result":I
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 158
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->defaultLocation:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateLastSelectedLocation(Landroid/content/Context;Ljava/lang/String;)I

    .line 160
    :cond_0
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 161
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;

    invoke-virtual {v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->setResult(I)V

    .line 163
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation$3;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuDefaultLocation;->finish()V

    .line 192
    return-void
.end method
