.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;
.super Landroid/view/View;
.source "FlurriesIconView.java"

# interfaces
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;


# static fields
.field private static final ANI_TYPE_CLOUD_TRANSLATE:I = 0x11

.field private static final ANI_TYPE_FIRST:I = 0x22

.field private static final ANI_TYPE_SECOND:I = 0x25


# instance fields
.field TranslateAni:Landroid/animation/ValueAnimator;

.field isStop:Z

.field private l:Landroid/animation/Animator$AnimatorListener;

.field private mAnimators:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/animation/ValueAnimator;",
            ">;"
        }
    .end annotation
.end field

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mCenterDrop1:Landroid/graphics/Path;

.field private mCenterDrop2:Landroid/graphics/Path;

.field private mCenterDrop3:Landroid/graphics/Path;

.field private mCloudLeft:Landroid/graphics/Path;

.field private mCloudRight:Landroid/graphics/Path;

.field private mCloudTop:Landroid/graphics/Path;

.field private mIsActiveAnimationThread:Z

.field private mLeftDrop1:Landroid/graphics/Path;

.field private mLeftDrop2:Landroid/graphics/Path;

.field private mMasking:Landroid/graphics/Path;

.field private mPaint:Landroid/graphics/Paint;

.field private mPaintColor:I

.field public mPreFixProperty1:Ljava/lang/String;

.field private mRightDrop:Landroid/graphics/Path;

.field private mRightFlower:Landroid/graphics/Path;

.field private mScale:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 92
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 93
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 88
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 89
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 83
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mAnimators:Ljava/util/HashMap;

    .line 42
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    .line 44
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mIsActiveAnimationThread:Z

    .line 46
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPaint:Landroid/graphics/Paint;

    .line 48
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mMasking:Landroid/graphics/Path;

    .line 50
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudLeft:Landroid/graphics/Path;

    .line 52
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudRight:Landroid/graphics/Path;

    .line 54
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudTop:Landroid/graphics/Path;

    .line 56
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mLeftDrop2:Landroid/graphics/Path;

    .line 58
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mRightFlower:Landroid/graphics/Path;

    .line 60
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mLeftDrop1:Landroid/graphics/Path;

    .line 62
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mRightDrop:Landroid/graphics/Path;

    .line 64
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCenterDrop1:Landroid/graphics/Path;

    .line 66
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCenterDrop2:Landroid/graphics/Path;

    .line 68
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCenterDrop3:Landroid/graphics/Path;

    .line 70
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    .line 73
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mBitmap:Landroid/graphics/Bitmap;

    .line 76
    const v0, -0xa0a0b

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPaintColor:I

    .line 554
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->l:Landroid/animation/Animator$AnimatorListener;

    .line 585
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPreFixProperty1:Ljava/lang/String;

    .line 772
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->isStop:Z

    .line 84
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->init()V

    .line 85
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mAnimators:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mIsActiveAnimationThread:Z

    return v0
.end method

.method private drawCloud(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "paint"    # Landroid/graphics/Paint;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 354
    const/4 v0, 0x0

    .line 355
    .local v0, "dx":F
    iget-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mIsActiveAnimationThread:Z

    if-eqz v1, :cond_0

    .line 356
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 358
    :cond_0
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPaintColor:I

    invoke-virtual {p2, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 360
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 361
    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 362
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudLeft:Landroid/graphics/Path;

    invoke-virtual {p1, v1, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 363
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 365
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 366
    invoke-virtual {p1, v2, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 367
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudTop:Landroid/graphics/Path;

    invoke-virtual {p1, v1, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 368
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 370
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 371
    neg-float v1, v0

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 372
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudRight:Landroid/graphics/Path;

    invoke-virtual {p1, v1, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 373
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 374
    return-void
.end method

.method private drawTransparentArea(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "paint"    # Landroid/graphics/Paint;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 348
    new-instance v0, Landroid/graphics/PorterDuffXfermode;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v1}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 349
    invoke-virtual {p1, p2, v2, v2, p3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 350
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 351
    return-void
.end method

.method private init()V
    .locals 4

    .prologue
    const/high16 v3, 0x43960000    # 300.0f

    .line 96
    invoke-virtual {p0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 97
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPaint:Landroid/graphics/Paint;

    .line 98
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->initPath()V

    .line 99
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPaintColor:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 100
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 101
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 102
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x40a00000    # 5.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 104
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mBitmap:Landroid/graphics/Bitmap;

    .line 106
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 107
    .local v0, "mMaskingCanvas":Landroid/graphics/Canvas;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mMasking:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 108
    return-void
.end method

.method private initPath()V
    .locals 13

    .prologue
    const v12, 0x424f20c5

    const v11, 0x42fa774c

    const/high16 v10, 0x42ec0000    # 118.0f

    const v9, 0x42ea8937

    const v8, 0x43061604

    .line 111
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mMasking:Landroid/graphics/Path;

    .line 112
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x432647f0

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v10

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 113
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x421e5f3b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v10

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 114
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x41fc6042    # 31.547f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v10

    const v3, 0x41c82f1b    # 25.023f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42ded0e5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x41c82f1b    # 25.023f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42ceb8d5    # 103.361f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x41c82f1b    # 25.023f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42bea1cb

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x41fc6042    # 31.547f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42b18419

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x421e5f3b

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42b18419

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 118
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x422020c5

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42b18419

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4221dd2f    # 40.466f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42b18937

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42239581    # 40.896f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42b19c29    # 88.805f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 120
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x422247ae    # 40.57f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42aec000    # 87.375f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42219168

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42abc28f    # 85.88f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42219168

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42a8b3b6

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 122
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x42219168

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4292a6e9

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x424551ec    # 49.33f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4280c28f    # 64.38f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42716e98

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4280c28f    # 64.38f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 124
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x4271a3d7    # 60.41f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4280c28f    # 64.38f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4271d70a    # 60.46f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4280c5a2

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42720c4a

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4280c5a2

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x426e1fbe

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4289a5e3

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x426c1375

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4292dfbe

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x426c1375

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x429c4e56    # 78.153f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 128
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x426c1375

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a0b958    # 80.362f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42733d71    # 60.81f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42a44e56    # 82.153f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x427c1375

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42a44e56    # 82.153f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 130
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x428274bc

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a44e56    # 82.153f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x428609ba

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42a0b8d5    # 80.361f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x428609ba

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x429c4dd3    # 78.152f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 132
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x428609ba

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4287b74c

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x428b70a4    # 69.72f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x426a7ae1    # 58.62f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4294dcac    # 74.431f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x424bf3b6

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 134
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x42a0bcee

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4226ac08    # 41.668f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42b2dc29    # 89.43f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x420a27f0

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42c848b4

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x41f88937    # 31.067f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 136
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x42d0d917

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41edc49c    # 29.721f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42d9fa5e

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x41e76a7f    # 28.927f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42e3872b    # 113.764f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x41e62f1b    # 28.773f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 138
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x42e3a560

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41e62b02    # 28.771f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42e3c000    # 113.875f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x41e60831    # 28.754f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42e3dd2f    # 113.932f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x41e6020c    # 28.751f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 140
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x42e48d50    # 114.276f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41e5f3b6    # 28.744f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42e53c6a

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x41e5d4fe    # 28.729f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42e5ec08

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x41e5d4fe    # 28.729f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 142
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x4305220c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41e5d4fe    # 28.729f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4314ee14    # 148.93f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x421b4bc7

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431d1810

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4256cfdf    # 53.703f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 144
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x4313cd91

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x426046a8    # 56.069f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x430bac08

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4277872b    # 61.882f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43065f7d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x428c753f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 146
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x43053062

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42903021

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4305bd71    # 133.74f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4295224e

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43079ae1

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42978000    # 75.75f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 148
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x43084560

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42985810

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x430902d1

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4298befa

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4309befa

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4298befa

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 150
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x430b11ec    # 139.07f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4298befa

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x430c5d71

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42976f1b

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x430d2083

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42950831

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 152
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x4312b0a4    # 146.69f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42838000    # 65.75f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431c3375

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4272126f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43269168

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4272126f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 154
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x4326accd

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4272126f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4326c6e9

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4271fae1

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4326e1cb

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4271f2b0    # 60.487f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 156
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x43367439

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42733a5e    # 60.807f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4342f9db

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42932d91

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4342f9db

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42b27852    # 89.235f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 158
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mMasking:Landroid/graphics/Path;

    const v1, 0x4342fa1d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42d228f6    # 105.08f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4336220c

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v10

    const v5, 0x432647f0

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v10

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 160
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mMasking:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 162
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudTop:Landroid/graphics/Path;

    .line 163
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudTop:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 164
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x42733646

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x427eef9e

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 165
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x426f5a1d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42883efa

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x426d52f2

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42915b23

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x426d52f2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x429aac8b

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 167
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x426d52f2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x429f12f2

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42747df4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42a2a24e

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x427d52f2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42a2a24e

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 169
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x4283147b    # 65.54f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a2a24e

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4286a979

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x429f10e5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4286a979

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x429aaa7f    # 77.333f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 171
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x4286a979

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42862c8b

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x428c0f5c    # 70.03f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4267926f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42957a5e

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42492b02    # 50.292f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 173
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x42a15d2f    # 80.682f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42241581    # 41.021f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42b3820c

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4207ac08    # 33.918f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42c8f021

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x41f3b852    # 30.465f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 175
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x42d18831

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41e90419    # 29.127f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42dbb958    # 109.862f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x41e124dd    # 28.143f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42e6a2d1    # 115.318f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x41e124dd    # 28.143f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 177
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x4304bf3b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41e124dd    # 28.143f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431549ba

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4218c6a8    # 38.194f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431d72f2

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4253f7cf    # 52.992f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 179
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x431d6000    # 157.375f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42540b44

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431efd2f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4262b74c    # 56.679f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431eea3d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4262cac1

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 181
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x43277e35

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x425e51ec    # 55.58f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 182
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x4325c042

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x424ee873

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 183
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x431c79db

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4202c083    # 32.688f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4308f687

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x41a1d70a    # 20.23f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42e6a2d1    # 115.318f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x41a1d70a    # 20.23f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 185
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x42e11810

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41a1d70a    # 20.23f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42db92f2

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x41a3645a    # 20.424f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42d61c29    # 107.055f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x41a6a5e3    # 20.831f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 187
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x42bcfbe7

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x41b42d0e    # 22.522f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42a67333

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x41de70a4    # 27.805f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42954419

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x421425e3

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 189
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x428d2d0e

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4225820c

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42869b23

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x423954fe    # 46.333f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4281a24e

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x424efcee

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 191
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x42805b23

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42543f7d    # 53.062f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x427c0419

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x425ef8d5    # 55.743f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const/high16 v5, 0x427c0000    # 63.0f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x425f051f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 193
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudTop:Landroid/graphics/Path;

    const v1, 0x4277ee98

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x426b6560    # 58.849f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42733646

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x427eef9e

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42733646

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x427eef9e

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 195
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudTop:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 197
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudLeft:Landroid/graphics/Path;

    .line 198
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudLeft:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 199
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x421f9fbe

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v9

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 200
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x41fee148    # 31.86f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v9

    const v3, 0x41cab021    # 25.336f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42dd25e3

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x41cab021    # 25.336f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42cd2042

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 202
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x41cab021    # 25.336f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42bd1ba6    # 94.554f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x41fee148    # 31.86f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42afeb02    # 87.959f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x421f9fbe

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42afeb02    # 87.959f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 204
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x42216148

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42afeb02    # 87.959f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42231db2    # 40.779f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42afdf3b

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4224d604    # 41.209f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42aff333

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 206
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x4223872b    # 40.882f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42ad199a    # 86.55f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4222d1ec

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42aa178d

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4222d1ec

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42a70ac1

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 208
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x4222d1ec

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4291178d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42469168

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x427e872b    # 63.632f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4272ae14    # 60.67f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x427e872b    # 63.632f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 210
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x4272e354    # 60.722f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x427e872b    # 63.632f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4282e148    # 65.44f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42805f3b

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4282e148    # 65.44f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42805f3b

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 212
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x4286ad0e

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42606f9e

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 213
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x42880ccd

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42606042

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4275ced9    # 61.452f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x425ea1cb    # 55.658f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4272af1b

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x425ea1cb    # 55.658f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 215
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x4238570a    # 46.085f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x425ea1cb    # 55.658f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42084ac1

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4285a8f6    # 66.83f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42034396

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42a21062

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 217
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x41beae14    # 23.835f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42a7fdf4

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x418ab439    # 17.338f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42b946a8    # 92.638f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x418ab439    # 17.338f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42cd1fbe

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 219
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x418ab439    # 17.338f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42e5e76d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x41dbb22d    # 27.462f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v11

    const v5, 0x421fa1cb    # 39.908f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v11

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 221
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x431c374c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v11

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 222
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x431c374c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42ea88b4

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 223
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudLeft:Landroid/graphics/Path;

    const v1, 0x421f9fbe

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42ea88b4

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 224
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudLeft:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 226
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudRight:Landroid/graphics/Path;

    .line 227
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudRight:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 228
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x43267ba6

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v12

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 229
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x43263687

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v12

    const v3, 0x4321d5c3

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x424f51ec    # 51.83f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x431d3aa0

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42543646

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 231
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x43140b02    # 148.043f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x425df6c9

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x430bfc6a

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4274cbc7

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4306bae1    # 134.73f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x428ae3d7    # 69.445f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 233
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x43058b44

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x428e9aa0

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43061852

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4293872b    # 73.764f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4307f5c3    # 135.96f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4295e24e

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 235
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x4308a083

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4296b9db

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43095df4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x429720c5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x430a19db

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x429720c5

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 237
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x430b6ccd

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x429720c5

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x430cb810

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4295d168

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x430d7be7

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42936d91

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 239
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x43130c08

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4281f852    # 64.985f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x431b5efa

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x426efae1

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x43273e35

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x426efae1

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 241
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x4336cfdf

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42704396

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4343553f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4291d687

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4343553f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42b0fe77

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 243
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x4343553f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42d08bc7

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43367cee

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v9

    const v5, 0x4326a354    # 166.638f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v9

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 245
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x42775a1d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v9

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 246
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x42775a1d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42fa77cf

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 247
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x4326a354    # 166.638f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x42fa77cf

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 248
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x4326a354    # 166.638f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v11

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 249
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x433adf3b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v11

    const v3, 0x434b54bc

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x42d949ba

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x434b54bc

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x42b0fefa

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 251
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudRight:Landroid/graphics/Path;

    const v1, 0x434b54bc

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4288b439

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x433ab78d

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v12

    const v5, 0x43267ba6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v12

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 253
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudRight:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 255
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mLeftDrop2:Landroid/graphics/Path;

    .line 256
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mLeftDrop2:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 257
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mLeftDrop2:Landroid/graphics/Path;

    const v1, 0x426dee98

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x431f4b85

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 258
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mLeftDrop2:Landroid/graphics/Path;

    const v1, 0x425977cf    # 54.367f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x431f4b85

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4248d1ec

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x432374fe    # 163.457f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4248d1ec

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4328926f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 260
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mLeftDrop2:Landroid/graphics/Path;

    const v1, 0x4248d1ec

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432dafdf

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x425977cf    # 54.367f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4331d8d5    # 177.847f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x426dee98

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4331d8d5    # 177.847f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 262
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mLeftDrop2:Landroid/graphics/Path;

    const v1, 0x4281322d    # 64.598f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4331d8d5    # 177.847f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4289851f    # 68.76f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x432dafdf

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4289851f    # 68.76f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4328926f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 264
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mLeftDrop2:Landroid/graphics/Path;

    const v1, 0x4289851f    # 68.76f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432374fe    # 163.457f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4281322d    # 64.598f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x431f4b85

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x426dee98

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x431f4b85

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 266
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mLeftDrop2:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 268
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mRightFlower:Landroid/graphics/Path;

    .line 269
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mRightFlower:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 270
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mRightFlower:Landroid/graphics/Path;

    const v1, 0x4302f78d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43201f7d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 271
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mRightFlower:Landroid/graphics/Path;

    const v1, 0x42facc4a

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43201f7d

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42f1bbe7

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4324a7f0

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42f1bbe7

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x432a3a5e

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 273
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mRightFlower:Landroid/graphics/Path;

    const v1, 0x42f1bbe7

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432fcc08

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42facbc7

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4334547b    # 180.33f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4302f78d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4334547b    # 180.33f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 275
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mRightFlower:Landroid/graphics/Path;

    const v1, 0x43088979

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4334547b    # 180.33f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x430d11aa    # 141.069f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x432fcc8b

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x430d11aa    # 141.069f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x432a3a5e

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 277
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mRightFlower:Landroid/graphics/Path;

    const v1, 0x430d1168

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4324a7f0

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43088979

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43201f7d

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4302f78d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43201f7d

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 279
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mRightFlower:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 281
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mLeftDrop1:Landroid/graphics/Path;

    .line 282
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mLeftDrop1:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 283
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mLeftDrop1:Landroid/graphics/Path;

    const v1, 0x428e6042

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 284
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mLeftDrop1:Landroid/graphics/Path;

    const v1, 0x42866d91

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v8

    const v3, 0x427fec8b

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x430951ec    # 137.32f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x427fec8b

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x430d4b85

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 286
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mLeftDrop1:Landroid/graphics/Path;

    const v1, 0x427fec8b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4311449c

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42866d91

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43148000    # 148.5f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x428e6042

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43148000    # 148.5f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 288
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mLeftDrop1:Landroid/graphics/Path;

    const v1, 0x42965375

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43148000    # 148.5f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x429ccb44

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4311449c

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x429ccb44

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x430d4b85

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 290
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mLeftDrop1:Landroid/graphics/Path;

    const v1, 0x429ccb44

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x430951ec    # 137.32f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42965375

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v8

    const v5, 0x428e6042

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v8

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 292
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mLeftDrop1:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 294
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mRightDrop:Landroid/graphics/Path;

    .line 295
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mRightDrop:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 296
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mRightDrop:Landroid/graphics/Path;

    const v1, 0x430d88f6

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4312d893

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 297
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mRightDrop:Landroid/graphics/Path;

    const v1, 0x43110d0e

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4312d893

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x4313e8f6    # 147.91f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x430ffc29    # 143.985f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x4313e8f6    # 147.91f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x430c778d

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 299
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mRightDrop:Landroid/graphics/Path;

    const v1, 0x4313e8f6    # 147.91f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4308f2f2

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x43110d0e

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v8

    const v5, 0x430d88f6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v8

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 301
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mRightDrop:Landroid/graphics/Path;

    const v1, 0x430a045a

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v8

    const v3, 0x430727f0

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4308f2f2

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x430727f0

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x430c778d

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 303
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mRightDrop:Landroid/graphics/Path;

    const v1, 0x430727f0

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x430ffc29    # 143.985f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x430a049c

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4312d893

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x430d88f6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4312d893

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 305
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mRightDrop:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 307
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCenterDrop1:Landroid/graphics/Path;

    .line 308
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCenterDrop1:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 309
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCenterDrop1:Landroid/graphics/Path;

    const v1, 0x42d7353f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4304d893

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 310
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCenterDrop1:Landroid/graphics/Path;

    const v1, 0x42ccfb64

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4304d893

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42c4a979

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4309020c

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42c4a979

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x430e1f7d

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 312
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCenterDrop1:Landroid/graphics/Path;

    const v1, 0x42c4a979

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43133c6a

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42ccfb64

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43176560

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42d7353f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43176560

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 314
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCenterDrop1:Landroid/graphics/Path;

    const v1, 0x42e16f1b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43176560

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42e9c106

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43133c6a

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42e9c106

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x430e1f7d

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 316
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCenterDrop1:Landroid/graphics/Path;

    const v1, 0x42e9c106

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4309020c

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42e16f1b

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4304d893

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42d7353f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4304d893

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 318
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCenterDrop1:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 320
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCenterDrop2:Landroid/graphics/Path;

    .line 321
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCenterDrop2:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 322
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCenterDrop2:Landroid/graphics/Path;

    const v1, 0x42c35810

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x431d9ae1

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 323
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCenterDrop2:Landroid/graphics/Path;

    const v1, 0x42bbda1d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x431d9ae1

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42b5c20c

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4320a6e9

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42b5c20c

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x432465e3

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 325
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCenterDrop2:Landroid/graphics/Path;

    const v1, 0x42b5c20c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432824dd

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42bbda1d

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x432b30e5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42c35810

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x432b30e5

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 327
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCenterDrop2:Landroid/graphics/Path;

    const v1, 0x42cad604    # 101.418f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x432b30e5

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42d0ee14

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x432824dd

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42d0ee14

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x432465e3

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 329
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCenterDrop2:Landroid/graphics/Path;

    const v1, 0x42d0ee14

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4320a6e9

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42cad604    # 101.418f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x431d9ae1

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42c35810

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x431d9ae1

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 331
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCenterDrop2:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 333
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCenterDrop3:Landroid/graphics/Path;

    .line 334
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCenterDrop3:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 335
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCenterDrop3:Landroid/graphics/Path;

    const v1, 0x42af38d5    # 87.611f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4332a6e9

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 336
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCenterDrop3:Landroid/graphics/Path;

    const v1, 0x42a5e6e9

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x4332a6e9

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x429e5168

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x433670e5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x429e5168

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x433b19db

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 338
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCenterDrop3:Landroid/graphics/Path;

    const v1, 0x429e5168

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x433fc24e

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42a5e6e9

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x43438c4a

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42af38d5    # 87.611f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x43438c4a

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 340
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCenterDrop3:Landroid/graphics/Path;

    const v1, 0x42b88a3d    # 92.27f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x43438c4a

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42c01f3b

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x433fc24e

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42c01f3b

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x433b19db

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 342
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCenterDrop3:Landroid/graphics/Path;

    const v1, 0x42c01f3b

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v1, v2

    const v2, 0x433670e5

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v2, v3

    const v3, 0x42b88a3d    # 92.27f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4332a6e9

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    const v5, 0x42af38d5    # 87.611f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v6

    const v6, 0x4332a6e9

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 344
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCenterDrop3:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 345
    return-void
.end method


# virtual methods
.method public cancelAnimation()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 754
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mAnimators:Ljava/util/HashMap;

    if-eqz v3, :cond_1

    .line 755
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 756
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 757
    .local v2, "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 758
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 759
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    .line 760
    .local v0, "ani":Landroid/animation/ValueAnimator;
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 761
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    goto :goto_0

    .line 766
    .end local v0    # "ani":Landroid/animation/ValueAnimator;
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    .end local v2    # "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_1
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 767
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->cancel()V

    .line 769
    :cond_2
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mIsActiveAnimationThread:Z

    .line 770
    return-void
.end method

.method public getScale()F
    .locals 1

    .prologue
    .line 805
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 551
    return-object p0
.end method

.method public isRunning()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 778
    const/4 v3, 0x0

    .line 780
    .local v3, "ret":Z
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v4}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 781
    const/4 v3, 0x1

    .line 783
    :cond_0
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mAnimators:Ljava/util/HashMap;

    if-eqz v4, :cond_2

    .line 784
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 785
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 786
    .local v2, "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 787
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 788
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    .line 789
    .local v0, "ani":Landroid/animation/ValueAnimator;
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 790
    const/4 v3, 0x1

    .line 796
    .end local v0    # "ani":Landroid/animation/ValueAnimator;
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    .end local v2    # "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_2
    return v3
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 48
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 378
    :try_start_0
    invoke-super/range {p0 .. p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 380
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    const/4 v4, 0x0

    cmpg-float v3, v3, v4

    if-gtz v3, :cond_1

    .line 381
    const-string v3, ""

    const-string v4, "scale is less then 0"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 548
    :cond_0
    :goto_0
    return-void

    .line 385
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mAnimators:Ljava/util/HashMap;

    const/16 v4, 0x11

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/animation/ValueAnimator;

    .line 386
    .local v10, "ani":Landroid/animation/ValueAnimator;
    const/16 v22, 0x0

    .line 387
    .local v22, "cloudTrX":F
    const/16 v23, 0x0

    .line 388
    .local v23, "cloudTrY":F
    if-eqz v10, :cond_2

    .line 389
    const-string v3, "transX"

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v22

    .line 390
    const-string v3, "transY"

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v23

    .line 393
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mAnimators:Ljava/util/HashMap;

    const/16 v4, 0x22

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "ani":Landroid/animation/ValueAnimator;
    check-cast v10, Landroid/animation/ValueAnimator;

    .line 395
    .restart local v10    # "ani":Landroid/animation/ValueAnimator;
    const/16 v37, 0x0

    .line 396
    .local v37, "leftFlowserTrX":F
    const/16 v39, 0x0

    .line 397
    .local v39, "leftFlowserTrY":F
    const/high16 v35, 0x3f800000    # 1.0f

    .line 398
    .local v35, "leftFlowserSc":F
    const/16 v29, 0x0

    .line 399
    .local v29, "leftDropTrX":F
    const/16 v19, 0x0

    .line 400
    .local v19, "centerDropTrX":F
    const/16 v31, 0x0

    .line 401
    .local v31, "leftDropTrY":F
    const/high16 v27, 0x3f800000    # 1.0f

    .line 402
    .local v27, "leftDropSc":F
    const/high16 v43, 0x3f800000    # 1.0f

    .line 403
    .local v43, "rightDropSc":F
    const/16 v20, 0x0

    .line 404
    .local v20, "centerDropTrY":F
    const/high16 v12, 0x3f800000    # 1.0f

    .line 405
    .local v12, "centerDrop1Sc":F
    const/high16 v14, 0x3f800000    # 1.0f

    .line 406
    .local v14, "centerDrop2Sc":F
    const/high16 v17, 0x3f800000    # 1.0f

    .line 408
    .local v17, "centerDrop3Sc":F
    if-eqz v10, :cond_3

    .line 409
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPreFixProperty1:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "leftFlowerTrX"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v37

    .line 410
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPreFixProperty1:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "leftFlowerTrY"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v39

    .line 411
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPreFixProperty1:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "leftFlowerSc"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v35

    .line 412
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPreFixProperty1:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "leftDropTrX"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v29

    .line 413
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPreFixProperty1:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "centerDropTrX"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v19

    .line 414
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPreFixProperty1:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "leftDropTrY"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v31

    .line 415
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPreFixProperty1:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "leftDropSc"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v27

    .line 416
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPreFixProperty1:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "rightDropSc"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v43

    .line 417
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPreFixProperty1:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "centerDropTrY2"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v20

    .line 418
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPreFixProperty1:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "centerDrop1Sc"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v12

    .line 419
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPreFixProperty1:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "centerDrop2Sc"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v14

    .line 420
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPreFixProperty1:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "centerDrop3Sc"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v17

    .line 424
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mAnimators:Ljava/util/HashMap;

    const/16 v4, 0x25

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "ani":Landroid/animation/ValueAnimator;
    check-cast v10, Landroid/animation/ValueAnimator;

    .line 426
    .restart local v10    # "ani":Landroid/animation/ValueAnimator;
    const/16 v38, 0x0

    .line 427
    .local v38, "leftFlowserTrX1":F
    const/16 v40, 0x0

    .line 428
    .local v40, "leftFlowserTrY1":F
    const/high16 v36, 0x3f800000    # 1.0f

    .line 429
    .local v36, "leftFlowserSc1":F
    const/16 v30, 0x0

    .line 430
    .local v30, "leftDropTrX1":F
    const/16 v32, 0x0

    .line 431
    .local v32, "leftDropTrY1":F
    const/high16 v28, 0x3f800000    # 1.0f

    .line 432
    .local v28, "leftDropSc1":F
    const/high16 v44, 0x3f800000    # 1.0f

    .line 433
    .local v44, "rightDropSc1":F
    const/high16 v18, 0x3f800000    # 1.0f

    .line 435
    .local v18, "centerDrop3Sc1":F
    if-eqz v10, :cond_4

    .line 436
    const-string v3, "leftFlowerTrX1"

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v38

    .line 437
    const-string v3, "leftFlowerTrY1"

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v40

    .line 438
    const-string v3, "leftFlowerSc1"

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v36

    .line 439
    const-string v3, "leftDropTrX"

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v30

    .line 440
    const-string v3, "leftDropTrY1"

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v32

    .line 441
    const-string v3, "leftDropSc1"

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v28

    .line 442
    const-string v3, "rightDropSc1"

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v44

    .line 443
    const-string v3, "centerDrop1Sc1"

    invoke-virtual {v10, v3}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v18

    .line 447
    :cond_4
    const/16 v9, 0x1f

    .line 449
    .local v9, "saveFlag":I
    const/16 v9, 0xf

    .line 452
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v3

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v5, v3

    const/high16 v3, 0x43960000    # 300.0f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v6, v3

    const/high16 v3, 0x43960000    # 300.0f

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v7, v3

    const/4 v8, 0x0

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v9}, Landroid/graphics/Canvas;->saveLayer(FFFFLandroid/graphics/Paint;I)I

    move-result v47

    .line 455
    .local v47, "transparentSaveLevel":I
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v33

    .line 456
    .local v33, "leftFlowerLevel":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float v3, v3, v37

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float v4, v4, v39

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 457
    const/high16 v3, 0x422c0000    # 43.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const/high16 v4, 0x43290000    # 169.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    move-object/from16 v0, p1

    move/from16 v1, v35

    move/from16 v2, v35

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 458
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mLeftDrop2:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 459
    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 461
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v45

    .line 462
    .local v45, "rightFlowerLevel":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float v3, v3, v37

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float v4, v4, v39

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 463
    const/high16 v3, 0x43180000    # 152.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const/high16 v4, 0x43320000    # 178.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    move-object/from16 v0, p1

    move/from16 v1, v35

    move/from16 v2, v35

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 464
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mRightFlower:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 465
    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 467
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v25

    .line 468
    .local v25, "leftDropLevel":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float v3, v3, v29

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float v4, v4, v31

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 469
    const v3, 0x4276ae14    # 61.67f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x430d1eb8    # 141.12f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 470
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mLeftDrop1:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 471
    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 473
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v41

    .line 474
    .local v41, "rightDropLevel":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float v3, v3, v29

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float v4, v4, v31

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 475
    const v3, 0x43177d71    # 151.49f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x430c6148    # 140.38f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    move-object/from16 v0, p1

    move/from16 v1, v43

    move/from16 v2, v43

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 476
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mRightDrop:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 477
    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 479
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v11

    .line 480
    .local v11, "centerDrop1Level":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float v3, v3, v29

    const/high16 v4, 0x40400000    # 3.0f

    sub-float v4, v31, v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 481
    const v3, 0x42d7bd71    # 107.87f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x430ddc29    # 141.86f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v12, v3, v4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 482
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCenterDrop1:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 483
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 485
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v13

    .line 486
    .local v13, "centerDrop2Level":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float v3, v3, v19

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float v4, v4, v20

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 487
    const v3, 0x42c3cccd    # 97.9f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x4324051f    # 164.02f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v14, v3, v4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 488
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCenterDrop2:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 489
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 491
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v15

    .line 492
    .local v15, "centerDrop3Level":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float v3, v3, v37

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float v4, v4, v39

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 493
    const v3, 0x42af199a    # 87.55f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x433aee14    # 186.93f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 494
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCenterDrop3:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 495
    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 498
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mIsActiveAnimationThread:Z

    if-eqz v3, :cond_5

    .line 499
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v34

    .line 500
    .local v34, "leftFlowerLevel1":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float v3, v3, v38

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float v4, v4, v40

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 501
    const/high16 v3, 0x422c0000    # 43.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const/high16 v4, 0x43290000    # 169.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    move-object/from16 v0, p1

    move/from16 v1, v36

    move/from16 v2, v36

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 502
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mLeftDrop2:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 503
    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 505
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v46

    .line 506
    .local v46, "rightFlowerLevel1":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float v3, v3, v38

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float v4, v4, v40

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 507
    const/high16 v3, 0x42f00000    # 120.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const/high16 v4, 0x43320000    # 178.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    move-object/from16 v0, p1

    move/from16 v1, v36

    move/from16 v2, v36

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 508
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mRightFlower:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 509
    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 511
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v26

    .line 512
    .local v26, "leftDropLevel1":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float v3, v3, v30

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float v4, v4, v32

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 513
    const v3, 0x4276ae14    # 61.67f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x430d1eb8    # 141.12f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    move-object/from16 v0, p1

    move/from16 v1, v28

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 514
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mLeftDrop1:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 515
    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 517
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v42

    .line 518
    .local v42, "rightDropLevel1":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float v3, v3, v30

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float v4, v4, v32

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 519
    const v3, 0x42fafae1    # 125.49f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x430c6148    # 140.38f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    move-object/from16 v0, p1

    move/from16 v1, v44

    move/from16 v2, v44

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 520
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mRightDrop:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 521
    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 523
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v16

    .line 524
    .local v16, "centerDrop3Level1":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float v3, v3, v38

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float v4, v4, v40

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 525
    const v3, 0x42af199a    # 87.55f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v3, v4

    const v4, 0x433aee14    # 186.93f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float/2addr v4, v5

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 526
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCenterDrop3:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 527
    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 531
    .end local v16    # "centerDrop3Level1":I
    .end local v26    # "leftDropLevel1":I
    .end local v34    # "leftFlowerLevel1":I
    .end local v42    # "rightDropLevel1":I
    .end local v46    # "rightFlowerLevel1":I
    :cond_5
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float v3, v3, v22

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float v4, v4, v23

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 532
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->drawTransparentArea(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V

    .line 534
    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 536
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-result v21

    .line 537
    .local v21, "cloudSaveLevel":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float v3, v3, v22

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    mul-float v4, v4, v23

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 538
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->drawCloud(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    .line 540
    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 541
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->isRunning()Z

    move-result v3

    if-nez v3, :cond_0

    .line 542
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->cancelAnimation()V

    .line 543
    const-string v3, ""

    const-string v4, "cancelA"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 545
    .end local v9    # "saveFlag":I
    .end local v10    # "ani":Landroid/animation/ValueAnimator;
    .end local v11    # "centerDrop1Level":I
    .end local v12    # "centerDrop1Sc":F
    .end local v13    # "centerDrop2Level":I
    .end local v14    # "centerDrop2Sc":F
    .end local v15    # "centerDrop3Level":I
    .end local v17    # "centerDrop3Sc":F
    .end local v18    # "centerDrop3Sc1":F
    .end local v19    # "centerDropTrX":F
    .end local v20    # "centerDropTrY":F
    .end local v21    # "cloudSaveLevel":I
    .end local v22    # "cloudTrX":F
    .end local v23    # "cloudTrY":F
    .end local v25    # "leftDropLevel":I
    .end local v27    # "leftDropSc":F
    .end local v28    # "leftDropSc1":F
    .end local v29    # "leftDropTrX":F
    .end local v30    # "leftDropTrX1":F
    .end local v31    # "leftDropTrY":F
    .end local v32    # "leftDropTrY1":F
    .end local v33    # "leftFlowerLevel":I
    .end local v35    # "leftFlowserSc":F
    .end local v36    # "leftFlowserSc1":F
    .end local v37    # "leftFlowserTrX":F
    .end local v38    # "leftFlowserTrX1":F
    .end local v39    # "leftFlowserTrY":F
    .end local v40    # "leftFlowserTrY1":F
    .end local v41    # "rightDropLevel":I
    .end local v43    # "rightDropSc":F
    .end local v44    # "rightDropSc1":F
    .end local v45    # "rightFlowerLevel":I
    .end local v47    # "transparentSaveLevel":I
    :catch_0
    move-exception v24

    .line 546
    .local v24, "e":Ljava/lang/Exception;
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0
.end method

.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 849
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x0

    .line 813
    const-string v3, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onViewDetachedFromWindow : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 814
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mAnimators:Ljava/util/HashMap;

    if-eqz v3, :cond_2

    .line 815
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 816
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 817
    .local v2, "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 818
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 819
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    .line 820
    .local v0, "ani":Landroid/animation/ValueAnimator;
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 821
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 822
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    goto :goto_0

    .line 825
    .end local v0    # "ani":Landroid/animation/ValueAnimator;
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    :cond_1
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mAnimators:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    .line 828
    .end local v2    # "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_2
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mAnimators:Ljava/util/HashMap;

    .line 829
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    .line 830
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPaint:Landroid/graphics/Paint;

    .line 831
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mMasking:Landroid/graphics/Path;

    .line 832
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudLeft:Landroid/graphics/Path;

    .line 833
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudRight:Landroid/graphics/Path;

    .line 834
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCloudTop:Landroid/graphics/Path;

    .line 835
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mLeftDrop2:Landroid/graphics/Path;

    .line 836
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mRightFlower:Landroid/graphics/Path;

    .line 837
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mLeftDrop1:Landroid/graphics/Path;

    .line 838
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mRightDrop:Landroid/graphics/Path;

    .line 839
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCenterDrop1:Landroid/graphics/Path;

    .line 840
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCenterDrop2:Landroid/graphics/Path;

    .line 841
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mCenterDrop3:Landroid/graphics/Path;

    .line 842
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_3

    .line 843
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 845
    :cond_3
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mBitmap:Landroid/graphics/Bitmap;

    .line 846
    return-void
.end method

.method public setPaintColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 79
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPaintColor:I

    .line 80
    return-void
.end method

.method public setScale(F)V
    .locals 0
    .param p1, "scale"    # F

    .prologue
    .line 800
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    .line 801
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->init()V

    .line 802
    return-void
.end method

.method public setWidthAndHeight(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 809
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, p1, p2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 810
    .local v0, "l":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 811
    return-void
.end method

.method public startAnimation()V
    .locals 60

    .prologue
    .line 587
    const/16 v55, 0x0

    move/from16 v0, v55

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->isStop:Z

    .line 588
    const-string v55, ""

    move-object/from16 v0, v55

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mPreFixProperty1:Ljava/lang/String;

    .line 590
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mAnimators:Ljava/util/HashMap;

    move-object/from16 v55, v0

    invoke-virtual/range {v55 .. v55}, Ljava/util/HashMap;->size()I

    move-result v55

    if-lez v55, :cond_2

    .line 591
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mAnimators:Ljava/util/HashMap;

    move-object/from16 v55, v0

    invoke-virtual/range {v55 .. v55}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v55

    invoke-interface/range {v55 .. v55}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 592
    .local v6, "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v55

    if-eqz v55, :cond_1

    .line 593
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 594
    .local v5, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/animation/ValueAnimator;

    .line 595
    .local v2, "ani":Landroid/animation/ValueAnimator;
    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v55

    if-eqz v55, :cond_0

    .line 596
    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->end()V

    goto :goto_0

    .line 600
    .end local v2    # "ani":Landroid/animation/ValueAnimator;
    .end local v5    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;"
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mAnimators:Ljava/util/HashMap;

    move-object/from16 v55, v0

    invoke-virtual/range {v55 .. v55}, Ljava/util/HashMap;->clear()V

    .line 603
    .end local v6    # "entryIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Landroid/animation/ValueAnimator;>;>;"
    :cond_2
    const/4 v2, 0x0

    .line 605
    .restart local v2    # "ani":Landroid/animation/ValueAnimator;
    const/16 v55, 0x2

    move/from16 v0, v55

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v55, v0

    const/16 v56, 0x0

    const-string v57, "transX"

    const/16 v58, 0x5

    move/from16 v0, v58

    new-array v0, v0, [F

    move-object/from16 v58, v0

    fill-array-data v58, :array_0

    .line 606
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x1

    const-string v57, "transY"

    const/16 v58, 0x3

    move/from16 v0, v58

    new-array v0, v0, [F

    move-object/from16 v58, v0

    fill-array-data v58, :array_1

    .line 607
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    .line 605
    invoke-static/range {v55 .. v55}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v2

    .line 608
    const-wide/16 v55, 0xbb8

    move-wide/from16 v0, v55

    invoke-virtual {v2, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 609
    const/16 v55, -0x1

    move/from16 v0, v55

    invoke-virtual {v2, v0}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 610
    new-instance v55, Landroid/view/animation/LinearInterpolator;

    invoke-direct/range {v55 .. v55}, Landroid/view/animation/LinearInterpolator;-><init>()V

    move-object/from16 v0, v55

    invoke-virtual {v2, v0}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 611
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v55, v0

    move-object/from16 v0, v55

    invoke-virtual {v2, v0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 612
    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->start()V

    .line 613
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mAnimators:Ljava/util/HashMap;

    move-object/from16 v55, v0

    const/16 v56, 0x11

    invoke-static/range {v56 .. v56}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v56

    move-object/from16 v0, v55

    move-object/from16 v1, v56

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 615
    const/16 v55, 0x0

    const/high16 v56, 0x41700000    # 15.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v40

    .line 616
    .local v40, "kFlowerTrX1":Landroid/animation/Keyframe;
    const v55, 0x3f4ccccd    # 0.8f

    const/high16 v56, -0x3e600000    # -20.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v41

    .line 617
    .local v41, "kFlowerTrX2":Landroid/animation/Keyframe;
    const/high16 v55, 0x3f800000    # 1.0f

    const/high16 v56, -0x3e600000    # -20.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v42

    .line 619
    .local v42, "kFlowerTrX3":Landroid/animation/Keyframe;
    const v55, 0x3dcccccd    # 0.1f

    const/16 v56, 0x0

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v4

    .line 620
    .local v4, "ekTransValue1":Landroid/animation/Keyframe;
    const v55, 0x3d4ccccd    # 0.05f

    const/high16 v56, 0x3f800000    # 1.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v3

    .line 622
    .local v3, "ekScaleValue1":Landroid/animation/Keyframe;
    const/16 v55, 0x0

    const/high16 v56, -0x3d7e0000    # -65.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v43

    .line 623
    .local v43, "kFlowerTrY1":Landroid/animation/Keyframe;
    const v55, 0x3f4ccccd    # 0.8f

    const/high16 v56, 0x41f00000    # 30.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v44

    .line 624
    .local v44, "kFlowerTrY2":Landroid/animation/Keyframe;
    const/high16 v55, 0x3f800000    # 1.0f

    const/high16 v56, 0x41f00000    # 30.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v45

    .line 626
    .local v45, "kFlowerTrY3":Landroid/animation/Keyframe;
    const/16 v55, 0x0

    const/high16 v56, 0x3f800000    # 1.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v36

    .line 627
    .local v36, "kFlowerSc1":Landroid/animation/Keyframe;
    const v55, 0x3f19999a    # 0.6f

    const/high16 v56, 0x3f800000    # 1.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v37

    .line 628
    .local v37, "kFlowerSc2":Landroid/animation/Keyframe;
    const v55, 0x3f4ccccd    # 0.8f

    const/16 v56, 0x0

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v38

    .line 629
    .local v38, "kFlowerSc3":Landroid/animation/Keyframe;
    const/high16 v55, 0x3f800000    # 1.0f

    const/16 v56, 0x0

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v39

    .line 631
    .local v39, "kFlowerSc4":Landroid/animation/Keyframe;
    const/16 v55, 0x0

    const/high16 v56, 0x41700000    # 15.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v30

    .line 632
    .local v30, "kDropTrX1":Landroid/animation/Keyframe;
    const v55, 0x3e4ccccd    # 0.2f

    const/high16 v56, 0x41700000    # 15.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v31

    .line 633
    .local v31, "kDropTrX2":Landroid/animation/Keyframe;
    const/high16 v55, 0x3f800000    # 1.0f

    const/high16 v56, -0x3e600000    # -20.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v32

    .line 635
    .local v32, "kDropTrX3":Landroid/animation/Keyframe;
    const/16 v55, 0x0

    const/high16 v56, 0x41700000    # 15.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v22

    .line 636
    .local v22, "kCenterTrX1":Landroid/animation/Keyframe;
    const v55, 0x3dcccccd    # 0.1f

    const/high16 v56, 0x41700000    # 15.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v23

    .line 637
    .local v23, "kCenterTrX2":Landroid/animation/Keyframe;
    const v55, 0x3f666666    # 0.9f

    const/high16 v56, -0x3e600000    # -20.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v24

    .line 638
    .local v24, "kCenterTrX3":Landroid/animation/Keyframe;
    const/high16 v55, 0x3f800000    # 1.0f

    const/high16 v56, -0x3e600000    # -20.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v25

    .line 640
    .local v25, "kCenterTrX4":Landroid/animation/Keyframe;
    const/16 v55, 0x0

    const/high16 v56, -0x3df40000    # -35.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v33

    .line 641
    .local v33, "kDropTrY1":Landroid/animation/Keyframe;
    const v55, 0x3ecccccd    # 0.4f

    const/high16 v56, -0x3df40000    # -35.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v34

    .line 642
    .local v34, "kDropTrY2":Landroid/animation/Keyframe;
    const/high16 v55, 0x3f800000    # 1.0f

    const/high16 v56, 0x42700000    # 60.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v35

    .line 644
    .local v35, "kDropTrY3":Landroid/animation/Keyframe;
    const/16 v55, 0x0

    const/high16 v56, 0x3f800000    # 1.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v46

    .line 645
    .local v46, "kLeftDropSc1":Landroid/animation/Keyframe;
    const v55, 0x3e4ccccd    # 0.2f

    const/high16 v56, 0x3f800000    # 1.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v47

    .line 646
    .local v47, "kLeftDropSc2":Landroid/animation/Keyframe;
    const v55, 0x3f333333    # 0.7f

    const/high16 v56, 0x3fc00000    # 1.5f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v48

    .line 647
    .local v48, "kLeftDropSc3":Landroid/animation/Keyframe;
    const v55, 0x3f4ccccd    # 0.8f

    const/high16 v56, 0x3fc00000    # 1.5f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v49

    .line 648
    .local v49, "kLeftDropSc4":Landroid/animation/Keyframe;
    const/high16 v55, 0x3f800000    # 1.0f

    const/16 v56, 0x0

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v50

    .line 650
    .local v50, "kLeftDropSc5":Landroid/animation/Keyframe;
    const/16 v55, 0x0

    const/high16 v56, 0x3f800000    # 1.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v51

    .line 651
    .local v51, "kRightDropSc1":Landroid/animation/Keyframe;
    const v55, 0x3f4ccccd    # 0.8f

    const/high16 v56, 0x3f800000    # 1.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v52

    .line 652
    .local v52, "kRightDropSc2":Landroid/animation/Keyframe;
    const/high16 v55, 0x3f800000    # 1.0f

    const/16 v56, 0x0

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v53

    .line 654
    .local v53, "kRightDropSc3":Landroid/animation/Keyframe;
    const/16 v55, 0x0

    const/high16 v56, -0x3da40000    # -55.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v26

    .line 655
    .local v26, "kCenterTrY1":Landroid/animation/Keyframe;
    const v55, 0x3e4ccccd    # 0.2f

    const/high16 v56, -0x3da40000    # -55.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v27

    .line 656
    .local v27, "kCenterTrY2":Landroid/animation/Keyframe;
    const v55, 0x3f4ccccd    # 0.8f

    const/high16 v56, 0x42200000    # 40.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v28

    .line 657
    .local v28, "kCenterTrY3":Landroid/animation/Keyframe;
    const/high16 v55, 0x3f800000    # 1.0f

    const/high16 v56, 0x42200000    # 40.0f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v29

    .line 659
    .local v29, "kCenterTrY4":Landroid/animation/Keyframe;
    const/16 v55, 0x0

    const v56, 0x3f99999a    # 1.2f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v7

    .line 660
    .local v7, "kCenterSc11":Landroid/animation/Keyframe;
    const v55, 0x3e99999a    # 0.3f

    const/high16 v56, 0x3f000000    # 0.5f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v8

    .line 661
    .local v8, "kCenterSc12":Landroid/animation/Keyframe;
    const v55, 0x3f19999a    # 0.6f

    const v56, 0x3f99999a    # 1.2f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v9

    .line 662
    .local v9, "kCenterSc13":Landroid/animation/Keyframe;
    const v55, 0x3f666666    # 0.9f

    const/high16 v56, 0x3f000000    # 0.5f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v10

    .line 663
    .local v10, "kCenterSc14":Landroid/animation/Keyframe;
    const/high16 v55, 0x3f800000    # 1.0f

    const/16 v56, 0x0

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v11

    .line 664
    .local v11, "kCenterSc15":Landroid/animation/Keyframe;
    const/16 v55, 0x0

    const/high16 v56, 0x3f000000    # 0.5f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v12

    .line 665
    .local v12, "kCenterSc21":Landroid/animation/Keyframe;
    const v55, 0x3e99999a    # 0.3f

    const v56, 0x3f99999a    # 1.2f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v13

    .line 666
    .local v13, "kCenterSc22":Landroid/animation/Keyframe;
    const v55, 0x3f19999a    # 0.6f

    const/high16 v56, 0x3f000000    # 0.5f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v14

    .line 667
    .local v14, "kCenterSc23":Landroid/animation/Keyframe;
    const v55, 0x3f666666    # 0.9f

    const/16 v56, 0x0

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v15

    .line 668
    .local v15, "kCenterSc24":Landroid/animation/Keyframe;
    const/high16 v55, 0x3f800000    # 1.0f

    const/16 v56, 0x0

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v16

    .line 670
    .local v16, "kCenterSc25":Landroid/animation/Keyframe;
    const/16 v55, 0x0

    const v56, 0x3f99999a    # 1.2f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v17

    .line 671
    .local v17, "kCenterSc31":Landroid/animation/Keyframe;
    const v55, 0x3e99999a    # 0.3f

    const/high16 v56, 0x3f000000    # 0.5f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v18

    .line 672
    .local v18, "kCenterSc32":Landroid/animation/Keyframe;
    const v55, 0x3f19999a    # 0.6f

    const v56, 0x3f99999a    # 1.2f

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v19

    .line 673
    .local v19, "kCenterSc33":Landroid/animation/Keyframe;
    const v55, 0x3f666666    # 0.9f

    const/16 v56, 0x0

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v20

    .line 674
    .local v20, "kCenterSc34":Landroid/animation/Keyframe;
    const/high16 v55, 0x3f800000    # 1.0f

    const/16 v56, 0x0

    invoke-static/range {v55 .. v56}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v21

    .line 676
    .local v21, "kCenterSc35":Landroid/animation/Keyframe;
    const/16 v55, 0x18

    move/from16 v0, v55

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v55, v0

    const/16 v56, 0x0

    const-string v57, "leftFlowerTrX"

    const/16 v58, 0x3

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v40, v58, v59

    const/16 v59, 0x1

    aput-object v41, v58, v59

    const/16 v59, 0x2

    aput-object v42, v58, v59

    .line 677
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x1

    const-string v57, "leftFlowerTrY"

    const/16 v58, 0x3

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v43, v58, v59

    const/16 v59, 0x1

    aput-object v44, v58, v59

    const/16 v59, 0x2

    aput-object v45, v58, v59

    .line 678
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x2

    const-string v57, "leftFlowerSc"

    const/16 v58, 0x4

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v36, v58, v59

    const/16 v59, 0x1

    aput-object v37, v58, v59

    const/16 v59, 0x2

    aput-object v38, v58, v59

    const/16 v59, 0x3

    aput-object v39, v58, v59

    .line 679
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x3

    const-string v57, "leftDropTrX"

    const/16 v58, 0x3

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v30, v58, v59

    const/16 v59, 0x1

    aput-object v31, v58, v59

    const/16 v59, 0x2

    aput-object v32, v58, v59

    .line 680
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x4

    const-string v57, "centerDropTrX"

    const/16 v58, 0x4

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v22, v58, v59

    const/16 v59, 0x1

    aput-object v23, v58, v59

    const/16 v59, 0x2

    aput-object v24, v58, v59

    const/16 v59, 0x3

    aput-object v25, v58, v59

    .line 681
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x5

    const-string v57, "leftDropTrY"

    const/16 v58, 0x3

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v33, v58, v59

    const/16 v59, 0x1

    aput-object v34, v58, v59

    const/16 v59, 0x2

    aput-object v35, v58, v59

    .line 682
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x6

    const-string v57, "leftDropSc"

    const/16 v58, 0x5

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v46, v58, v59

    const/16 v59, 0x1

    aput-object v47, v58, v59

    const/16 v59, 0x2

    aput-object v48, v58, v59

    const/16 v59, 0x3

    aput-object v49, v58, v59

    const/16 v59, 0x4

    aput-object v50, v58, v59

    .line 683
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x7

    const-string v57, "rightDropSc"

    const/16 v58, 0x3

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v51, v58, v59

    const/16 v59, 0x1

    aput-object v52, v58, v59

    const/16 v59, 0x2

    aput-object v53, v58, v59

    .line 684
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x8

    const-string v57, "centerDropTrY2"

    const/16 v58, 0x4

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v26, v58, v59

    const/16 v59, 0x1

    aput-object v27, v58, v59

    const/16 v59, 0x2

    aput-object v28, v58, v59

    const/16 v59, 0x3

    aput-object v29, v58, v59

    .line 685
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x9

    const-string v57, "centerDrop1Sc"

    const/16 v58, 0x5

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v7, v58, v59

    const/16 v59, 0x1

    aput-object v8, v58, v59

    const/16 v59, 0x2

    aput-object v9, v58, v59

    const/16 v59, 0x3

    aput-object v10, v58, v59

    const/16 v59, 0x4

    aput-object v11, v58, v59

    .line 686
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0xa

    const-string v57, "centerDrop2Sc"

    const/16 v58, 0x5

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v12, v58, v59

    const/16 v59, 0x1

    aput-object v13, v58, v59

    const/16 v59, 0x2

    aput-object v14, v58, v59

    const/16 v59, 0x3

    aput-object v15, v58, v59

    const/16 v59, 0x4

    aput-object v16, v58, v59

    .line 687
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0xb

    const-string v57, "centerDrop3Sc"

    const/16 v58, 0x5

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v17, v58, v59

    const/16 v59, 0x1

    aput-object v18, v58, v59

    const/16 v59, 0x2

    aput-object v19, v58, v59

    const/16 v59, 0x3

    aput-object v20, v58, v59

    const/16 v59, 0x4

    aput-object v21, v58, v59

    .line 688
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0xc

    const-string v57, "e_leftFlowerTrX"

    const/16 v58, 0x2

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v40, v58, v59

    const/16 v59, 0x1

    aput-object v4, v58, v59

    .line 690
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0xd

    const-string v57, "e_leftFlowerTrY"

    const/16 v58, 0x2

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v43, v58, v59

    const/16 v59, 0x1

    aput-object v4, v58, v59

    .line 691
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0xe

    const-string v57, "e_leftFlowerSc"

    const/16 v58, 0x2

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v36, v58, v59

    const/16 v59, 0x1

    aput-object v3, v58, v59

    .line 692
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0xf

    const-string v57, "e_leftDropTrX"

    const/16 v58, 0x2

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v30, v58, v59

    const/16 v59, 0x1

    aput-object v4, v58, v59

    .line 693
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x10

    const-string v57, "e_centerDropTrX"

    const/16 v58, 0x2

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v22, v58, v59

    const/16 v59, 0x1

    aput-object v4, v58, v59

    .line 694
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x11

    const-string v57, "e_leftDropTrY"

    const/16 v58, 0x2

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v33, v58, v59

    const/16 v59, 0x1

    aput-object v4, v58, v59

    .line 695
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x12

    const-string v57, "e_leftDropSc"

    const/16 v58, 0x2

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v46, v58, v59

    const/16 v59, 0x1

    aput-object v3, v58, v59

    .line 696
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x13

    const-string v57, "e_rightDropSc"

    const/16 v58, 0x2

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v51, v58, v59

    const/16 v59, 0x1

    aput-object v3, v58, v59

    .line 697
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x14

    const-string v57, "e_centerDropTrY2"

    const/16 v58, 0x2

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v26, v58, v59

    const/16 v59, 0x1

    aput-object v4, v58, v59

    .line 698
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x15

    const-string v57, "e_centerDrop1Sc"

    const/16 v58, 0x2

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v7, v58, v59

    const/16 v59, 0x1

    aput-object v3, v58, v59

    .line 699
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x16

    const-string v57, "e_centerDrop2Sc"

    const/16 v58, 0x2

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v12, v58, v59

    const/16 v59, 0x1

    aput-object v3, v58, v59

    .line 700
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x17

    const-string v57, "e_centerDrop3Sc"

    const/16 v58, 0x2

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v17, v58, v59

    const/16 v59, 0x1

    aput-object v3, v58, v59

    .line 701
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    .line 676
    invoke-static/range {v55 .. v55}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v2

    .line 703
    const-wide/16 v55, 0xbb8

    move-wide/from16 v0, v55

    invoke-virtual {v2, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 704
    const/16 v55, -0x1

    move/from16 v0, v55

    invoke-virtual {v2, v0}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 705
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v55, v0

    move-object/from16 v0, v55

    invoke-virtual {v2, v0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 706
    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->start()V

    .line 707
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mAnimators:Ljava/util/HashMap;

    move-object/from16 v55, v0

    const/16 v56, 0x22

    invoke-static/range {v56 .. v56}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v56

    move-object/from16 v0, v55

    move-object/from16 v1, v56

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 710
    const/16 v55, 0x8

    move/from16 v0, v55

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v55, v0

    const/16 v56, 0x0

    const-string v57, "leftFlowerTrX1"

    const/16 v58, 0x3

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v40, v58, v59

    const/16 v59, 0x1

    aput-object v41, v58, v59

    const/16 v59, 0x2

    aput-object v42, v58, v59

    .line 711
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x1

    const-string v57, "leftFlowerTrY1"

    const/16 v58, 0x3

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v43, v58, v59

    const/16 v59, 0x1

    aput-object v44, v58, v59

    const/16 v59, 0x2

    aput-object v45, v58, v59

    .line 712
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x2

    const-string v57, "leftFlowerSc1"

    const/16 v58, 0x3

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v36, v58, v59

    const/16 v59, 0x1

    aput-object v37, v58, v59

    const/16 v59, 0x2

    aput-object v38, v58, v59

    .line 713
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x3

    const-string v57, "leftDropTrX"

    const/16 v58, 0x3

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v30, v58, v59

    const/16 v59, 0x1

    aput-object v31, v58, v59

    const/16 v59, 0x2

    aput-object v32, v58, v59

    .line 714
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x4

    const-string v57, "leftDropTrY1"

    const/16 v58, 0x3

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v33, v58, v59

    const/16 v59, 0x1

    aput-object v34, v58, v59

    const/16 v59, 0x2

    aput-object v35, v58, v59

    .line 715
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x5

    const-string v57, "leftDropSc1"

    const/16 v58, 0x5

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v46, v58, v59

    const/16 v59, 0x1

    aput-object v47, v58, v59

    const/16 v59, 0x2

    aput-object v48, v58, v59

    const/16 v59, 0x3

    aput-object v49, v58, v59

    const/16 v59, 0x4

    aput-object v50, v58, v59

    .line 716
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x6

    const-string v57, "rightDropSc1"

    const/16 v58, 0x3

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v51, v58, v59

    const/16 v59, 0x1

    aput-object v52, v58, v59

    const/16 v59, 0x2

    aput-object v53, v58, v59

    .line 717
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    const/16 v56, 0x7

    const-string v57, "centerDrop1Sc1"

    const/16 v58, 0x5

    move/from16 v0, v58

    new-array v0, v0, [Landroid/animation/Keyframe;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v12, v58, v59

    const/16 v59, 0x1

    aput-object v13, v58, v59

    const/16 v59, 0x2

    aput-object v14, v58, v59

    const/16 v59, 0x3

    aput-object v15, v58, v59

    const/16 v59, 0x4

    aput-object v16, v58, v59

    .line 718
    invoke-static/range {v57 .. v58}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Ljava/lang/String;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v57

    aput-object v57, v55, v56

    .line 710
    invoke-static/range {v55 .. v55}, Landroid/animation/ValueAnimator;->ofPropertyValuesHolder([Landroid/animation/PropertyValuesHolder;)Landroid/animation/ValueAnimator;

    move-result-object v2

    .line 720
    const-wide/16 v55, 0xbb8

    move-wide/from16 v0, v55

    invoke-virtual {v2, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 721
    const-wide/16 v55, 0x5dc

    move-wide/from16 v0, v55

    invoke-virtual {v2, v0, v1}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 722
    const/16 v55, -0x1

    move/from16 v0, v55

    invoke-virtual {v2, v0}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 723
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v55, v0

    move-object/from16 v0, v55

    invoke-virtual {v2, v0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 724
    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->start()V

    .line 725
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mAnimators:Ljava/util/HashMap;

    move-object/from16 v55, v0

    const/16 v56, 0x25

    invoke-static/range {v56 .. v56}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v56

    move-object/from16 v0, v55

    move-object/from16 v1, v56

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 729
    const/16 v55, 0x3

    move/from16 v0, v55

    new-array v0, v0, [F

    move-object/from16 v55, v0

    const/16 v56, 0x0

    const/16 v57, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    move/from16 v58, v0

    mul-float v57, v57, v58

    aput v57, v55, v56

    const/16 v56, 0x1

    const v57, -0x3fd33333    # -2.7f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    move/from16 v58, v0

    mul-float v57, v57, v58

    aput v57, v55, v56

    const/16 v56, 0x2

    const/16 v57, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mScale:F

    move/from16 v58, v0

    mul-float v57, v57, v58

    aput v57, v55, v56

    invoke-static/range {v55 .. v55}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v55

    move-object/from16 v0, v55

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    .line 730
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    move-object/from16 v55, v0

    const/16 v56, -0x1

    invoke-virtual/range {v55 .. v56}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 731
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    move-object/from16 v55, v0

    const-wide/16 v56, 0x5dc

    invoke-virtual/range {v55 .. v57}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 732
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    move-object/from16 v55, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->l:Landroid/animation/Animator$AnimatorListener;

    move-object/from16 v56, v0

    invoke-virtual/range {v55 .. v56}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 733
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    move-object/from16 v55, v0

    invoke-virtual/range {v55 .. v55}, Landroid/animation/ValueAnimator;->start()V

    .line 735
    const/16 v55, 0x1

    move/from16 v0, v55

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->mIsActiveAnimationThread:Z

    .line 736
    new-instance v54, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView$2;

    move-object/from16 v0, v54

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;)V

    .line 749
    .local v54, "t":Ljava/lang/Thread;
    const/16 v55, 0xa

    invoke-virtual/range {v54 .. v55}, Ljava/lang/Thread;->setPriority(I)V

    .line 750
    invoke-virtual/range {v54 .. v54}, Ljava/lang/Thread;->start()V

    .line 751
    return-void

    .line 605
    nop

    :array_0
    .array-data 4
        0x0
        -0x3ee00000    # -10.0f
        0x0
        0x41200000    # 10.0f
        0x0
    .end array-data

    .line 606
    :array_1
    .array-data 4
        0x0
        -0x3fc00000    # -3.0f
        0x0
    .end array-data
.end method

.method public stopAnimation()V
    .locals 1

    .prologue
    .line 774
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/FlurriesIconView;->isStop:Z

    .line 775
    return-void
.end method
