.class public final Lcom/sec/android/widgetapp/ap/hero/accuweather/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final CustTextView:[I

.field public static final CustTextView_textStroke:I = 0x0

.field public static final CustTextView_textStrokeColor:I = 0x2

.field public static final CustTextView_textStrokeWidth:I = 0x1

.field public static final NotiSwitchPreference:[I

.field public static final TwSoftkeyItem:[I

.field public static final TwSoftkeyItem_twSoftkeyItemImage:I = 0x2

.field public static final TwSoftkeyItem_twSoftkeyItemText:I = 0x1

.field public static final TwSoftkeyItem_twSoftkeyItemType:I

.field public static final TwTheme:[I

.field public static final TwTheme_twSoftkeyItemStyle:I

.field public static final default_gallery:[I

.field public static final default_gallery_android_galleryItemBackground:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x3

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 21462
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/R$styleable;->CustTextView:[I

    .line 21510
    new-array v0, v2, [I

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/R$styleable;->NotiSwitchPreference:[I

    .line 21527
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/R$styleable;->TwSoftkeyItem:[I

    .line 21595
    new-array v0, v3, [I

    const/high16 v1, 0x7f010000

    aput v1, v0, v2

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/R$styleable;->TwTheme:[I

    .line 21625
    new-array v0, v3, [I

    const v1, 0x101004c

    aput v1, v0, v2

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/R$styleable;->default_gallery:[I

    return-void

    .line 21462
    nop

    :array_0
    .array-data 4
        0x7f010004
        0x7f010005
        0x7f010006
    .end array-data

    .line 21527
    :array_1
    .array-data 4
        0x7f010001
        0x7f010002
        0x7f010003
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21447
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
