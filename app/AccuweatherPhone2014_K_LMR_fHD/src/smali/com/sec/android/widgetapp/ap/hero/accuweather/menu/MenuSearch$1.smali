.class Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$1;
.super Ljava/lang/Object;
.source "MenuSearch.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    .prologue
    .line 248
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 7

    .prologue
    .line 250
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 252
    .local v0, "decorViewHeight":I
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 255
    .local v3, "r":Landroid/graphics/Rect;
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mTopLayout:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 256
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mTopLayout:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;->getHeight()I

    move-result v4

    iget v5, v3, Landroid/graphics/Rect;->bottom:I

    iget v6, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v5, v6

    sub-int v2, v4, v5

    .line 257
    .local v2, "keypadHeight":I
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    sub-int v5, v0, v2

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mSearchLayout:Landroid/widget/LinearLayout;
    invoke-static {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Landroid/widget/LinearLayout;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v6

    sub-int/2addr v5, v6

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->mDropBoxHeight:I
    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$102(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;I)I

    .line 258
    const-string v4, ""

    const-string v5, "VTO lstn "

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Landroid/widget/AutoCompleteTextView;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 261
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Landroid/widget/AutoCompleteTextView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/AutoCompleteTextView;->isFocusable()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 262
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->setDropdownMenuHeight()V
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)V

    .line 263
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->searchEditText:Landroid/widget/AutoCompleteTextView;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuSearch;)Landroid/widget/AutoCompleteTextView;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/AutoCompleteTextView;->ensureImeVisible(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 269
    .end local v2    # "keypadHeight":I
    :cond_0
    :goto_0
    return-void

    .line 266
    :catch_0
    move-exception v1

    .line 267
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
