.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;
.super Landroid/view/View;
.source "MostClearIconView.java"

# interfaces
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/IconAnimation;


# instance fields
.field RotationAni:Landroid/animation/ValueAnimator;

.field TranslateAni:Landroid/animation/ValueAnimator;

.field TranslateAni2:Landroid/animation/ValueAnimator;

.field TranslateAni3:Landroid/animation/ValueAnimator;

.field isStop:Z

.field l:Landroid/animation/Animator$AnimatorListener;

.field private mCloudPaint1:Landroid/graphics/Paint;

.field private mCloudPath_left:Landroid/graphics/Path;

.field private mCloudPath_right:Landroid/graphics/Path;

.field private mCloudPath_top:Landroid/graphics/Path;

.field private mIsActiveAnimationThread:Z

.field private mMoonPaint:Landroid/graphics/Paint;

.field private mMoonPath:Landroid/graphics/Path;

.field private mPaintColor:I

.field private mScaleFactor:F

.field private mTempBitmap:Landroid/graphics/Bitmap;

.field private mTempCanvas:Landroid/graphics/Canvas;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attributeset"    # Landroid/util/AttributeSet;

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attributeset"    # Landroid/util/AttributeSet;
    .param p3, "i"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 36
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    .line 54
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mIsActiveAnimationThread:Z

    .line 60
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    .line 62
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    .line 64
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni3:Landroid/animation/ValueAnimator;

    .line 66
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->RotationAni:Landroid/animation/ValueAnimator;

    .line 68
    const v0, -0xa0a0b

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mPaintColor:I

    .line 267
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->isStop:Z

    .line 273
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->l:Landroid/animation/Animator$AnimatorListener;

    .line 37
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->init()V

    .line 38
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mIsActiveAnimationThread:Z

    return v0
.end method

.method private drawMaskingArea(Landroid/graphics/Canvas;FLandroid/graphics/Paint;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "maskingMove"    # F
    .param p3, "paint"    # Landroid/graphics/Paint;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 353
    invoke-virtual {p1, p2, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 354
    new-instance v0, Landroid/graphics/PorterDuffXfermode;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v1}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 355
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mTempBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v2, v2, p3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 356
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 357
    return-void
.end method

.method private init()V
    .locals 13

    .prologue
    const v12, 0x4330f687

    const v11, 0x43112831

    const v10, 0x42f06a7f    # 120.208f

    const v9, 0x4282f1aa    # 65.472f

    const v8, 0x4337ddf4

    .line 75
    invoke-virtual {p0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 76
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->initPaint()V

    .line 78
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mTempBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mTempBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 80
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mTempBitmap:Landroid/graphics/Bitmap;

    .line 82
    :cond_0
    const/high16 v0, 0x43960000    # 300.0f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    const/high16 v1, 0x43960000    # 300.0f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mTempBitmap:Landroid/graphics/Bitmap;

    .line 84
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mTempBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mTempCanvas:Landroid/graphics/Canvas;

    .line 85
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mTempCanvas:Landroid/graphics/Canvas;

    const/high16 v1, 0x43060000    # 134.0f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x43340000    # 180.0f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const/high16 v3, 0x42a40000    # 82.0f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mMoonPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 88
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_left:Landroid/graphics/Path;

    .line 89
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_left:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 90
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_left:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v9

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v12

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 91
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x426a54fe    # 58.583f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v12

    const v3, 0x4253fdf4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x432b2937

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x4253fdf4

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x432437cf

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 94
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x4253fdf4

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x431d472b    # 157.278f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x426a54fe    # 58.583f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x43178fdf

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v9

    const v6, 0x43178fdf

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 97
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x4283b22d    # 65.848f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x43178fdf

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x42847021

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x43178a7f    # 151.541f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x42852d0e

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x43179375

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 100
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x42849db2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x4316574c

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x42844fdf

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x431509ba

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x42844fdf

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x4313b74c

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 103
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x42844fdf

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x430a3439

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x42939db2

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x43027aa0

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x42a67f7d    # 83.249f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x43027aa0

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 106
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x42a69687

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x43027aa0

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x42aeaa7f    # 87.333f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x4302f581    # 130.959f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x42aeaa7f    # 87.333f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x4302f581    # 130.959f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 109
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x42b1ea7f    # 88.958f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42f7eb02    # 123.959f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 110
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x42b3178d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42f7e45a    # 123.946f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x42a7d604    # 83.918f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42f722d1    # 123.568f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x42a67f7d    # 83.249f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42f722d1    # 123.568f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 113
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x428d85a2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42f722d1    # 123.568f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x4271e76d

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x43054042

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x426d999a    # 59.4f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x43118f1b

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x424ed810

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x431420c5

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x4238978d

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x431b9e35

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x4238978d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x432437cf

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 119
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_left:Landroid/graphics/Path;

    const v1, 0x4238978d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x432ef4fe    # 174.957f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x425b4396

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v8

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v9

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v8

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 122
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_left:Landroid/graphics/Path;

    const/high16 v1, 0x43250000    # 165.0f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 123
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_left:Landroid/graphics/Path;

    const/high16 v1, 0x43250000    # 165.0f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v12

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 124
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_left:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v9

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v12

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 126
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_right:Landroid/graphics/Path;

    .line 127
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_right:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 128
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x432dcac1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v10

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 129
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x432d8fdf

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v10

    const v3, 0x4329d021

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42f08000    # 120.25f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x4325de77

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42f29eb8    # 121.31f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 132
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x431e0148

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42f6d893

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x43171b23

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x43005eb8    # 128.37f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x43129ae1

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x43078419

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 135
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x431196c9

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x43092000    # 137.125f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x43120fdf

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x430b424e

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x4313a8b4

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x430c47f0

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 138
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x43143ae1    # 148.23f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x430ca51f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x4314dd2f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x430cd1aa    # 140.819f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x43157df4

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x430cd1aa    # 140.819f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 141
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x4316a042

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x430cd1aa    # 140.819f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x4317bbe7

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x430c4083

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x43186354    # 152.388f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x430b374c

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 144
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x431d26e9

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x4303a6e9

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x4324472b    # 164.278f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42fe3852    # 127.11f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x432e7127

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42fe3852    # 127.11f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 147
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x433bc5a2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42fec6a8    # 127.388f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x43467df4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x430a872b    # 138.528f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x43467df4

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x4318076d

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 150
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x43467df4

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x4325b3b6

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x433b7eb8

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x4330f6c9

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x432deccd

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x4330f6c9

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 153
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x42a88000    # 84.25f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x4330f6c9

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 154
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x42a88000    # 84.25f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x4337de35

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 155
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x432deccd

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x4337de35

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 156
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x432deccd

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 157
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x433f3fbe

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v8

    const v3, 0x434d574c

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x43297d2f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x434d574c

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x4318076d

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 160
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_right:Landroid/graphics/Path;

    const v1, 0x434d574c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x430691aa    # 134.569f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x433f1db2

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v10

    const v5, 0x432dcac1

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v10

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 164
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_top:Landroid/graphics/Path;

    .line 166
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_top:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 168
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x42a6ba5e

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x430290e5

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 169
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x42a51375

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x43065eb8    # 134.37f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x42a4353f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x430a5127

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x42a4353f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x430e5b23

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 172
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x42a4353f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x43104312

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x42a746a8    # 83.638f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x4311ce14

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x42ab0ed9    # 85.529f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x4311ce14

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 175
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x42aed78d

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x4311ce14

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x42b1e7f0

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x4310420c

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x42b1e7f0

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x430e5a1d

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 178
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x42b1e7f0

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x430578d5    # 133.472f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x42b6872b    # 91.264f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42fb0189

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x42be978d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42edd4fe    # 118.916f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 181
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x42c8c49c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42ddc312

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x42d84d50    # 108.151f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42d173b6

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x42eaa354    # 117.319f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42cb76c9

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 184
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x42f1f5c3    # 120.98f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42c92560

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x42faa4dd

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42c770a4    # 99.72f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x4301fc29    # 129.985f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42c770a4    # 99.72f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 187
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x4310e831

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42c770a4    # 99.72f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x431f11aa    # 159.069f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42d8dcac    # 108.431f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x43260e98

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42f28312

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 190
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x4325fe77

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42f28bc7

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x43276000    # 167.375f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42f8e6e9

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x43274fdf

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42f8ef9e

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 193
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x432ea7f0

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42f6ff7d    # 123.499f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 194
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x432d2a7f    # 173.166f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42f05168

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 195
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x43253958    # 165.224f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42cf5168

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x4314849c

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42b9b9db

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x4301fc29    # 129.985f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42b9b9db

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 198
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x42ff3a5e

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42b9b9db

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x42fa8419

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42ba0fdf

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x42f5dc29    # 122.93f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42bac419

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 201
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x42e069fc    # 112.207f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42bdb2b0    # 94.849f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x42cd1f3b

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42c6da1d

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x42be6873

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42d6db23

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 204
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x42b77b64

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42de6148    # 111.19f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x42b1dba6    # 88.929f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42e6f852    # 115.485f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x42ad999a    # 86.8f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42f05aa0

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 207
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x42ac8189

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42f2a24e

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x42aa7efa

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42f747ae    # 123.64f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x42aa7cee

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42f74d50    # 123.651f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 210
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_top:Landroid/graphics/Path;

    const v1, 0x42a8c000    # 84.375f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42fcaa7f    # 126.333f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x42a6ba5e

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x430290e5

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x42a6ba5e

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x430290e5

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 214
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mMoonPath:Landroid/graphics/Path;

    .line 215
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mMoonPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 216
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mMoonPath:Landroid/graphics/Path;

    const v1, 0x42aa5cac    # 85.181f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x420fab02    # 35.917f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 217
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mMoonPath:Landroid/graphics/Path;

    const v1, 0x42a69e35

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x420e8312

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x42a22979

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42169168

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x42a2df3b

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x4220947b    # 40.145f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 219
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mMoonPath:Landroid/graphics/Path;

    const v1, 0x42a4bb64

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x423ad3f8    # 46.707f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x42ab774c

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x425371aa    # 52.861f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x42ab774c

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x426fa4dd    # 59.911f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 221
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mMoonPath:Landroid/graphics/Path;

    const v1, 0x42ab774c

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42b1eb85    # 88.96f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x428371aa    # 65.722f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42d7c20c

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x42156e98

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42d7c20c

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 223
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mMoonPath:Landroid/graphics/Path;

    const v1, 0x41f50c4a    # 30.631f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42d7c20c

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x41c4d917    # 24.606f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42d29a1d

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x41934fdf    # 18.414f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42d01604    # 104.043f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 225
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mMoonPath:Landroid/graphics/Path;

    const v1, 0x416bdf3b    # 14.742f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42ce9810

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x414a2d0e    # 12.636f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42d5353f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x4154d917    # 13.303f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42da2e14    # 109.09f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 227
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mMoonPath:Landroid/graphics/Path;

    const v1, 0x41a15604    # 20.167f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x4306ab02    # 134.668f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x422d6b85    # 43.355f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x43187ae1    # 152.48f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x428d978d

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x43187ae1    # 152.48f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 229
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mMoonPath:Landroid/graphics/Path;

    const v1, 0x42cf7f7d    # 103.749f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x43187ae1    # 152.48f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x4302770a

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42fbfb64

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x4302770a

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42baa148

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 231
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mMoonPath:Landroid/graphics/Path;

    const v1, 0x4302770a

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42831fbe

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x42d6aa7f    # 107.333f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x421d54fe    # 39.333f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x42aa5cac    # 85.181f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x420fab02    # 35.917f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 234
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mMoonPath:Landroid/graphics/Path;

    const v1, 0x428e3127    # 71.096f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v11

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 235
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mMoonPath:Landroid/graphics/Path;

    const v1, 0x424773b6

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v11

    const v3, 0x41fce560    # 31.612f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x43046ed9

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x41bc26e9    # 23.519f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42e466e9

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 237
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mMoonPath:Landroid/graphics/Path;

    const v1, 0x41e1fbe7    # 28.248f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42e70419

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x4204e45a    # 33.223f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42e870a4    # 116.22f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x421977cf    # 38.367f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42e870a4    # 116.22f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 239
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mMoonPath:Landroid/graphics/Path;

    const v1, 0x428a38d5    # 69.111f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42e870a4    # 116.22f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x42bc1168

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x42b69687

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x42bc1168

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42723333    # 60.55f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 241
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mMoonPath:Landroid/graphics/Path;

    const v1, 0x42bc1168

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x425d3852    # 55.305f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x42ba9604    # 93.293f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x4248f1aa    # 50.236f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x42b7dfbe

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x4235b333    # 45.425f

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 243
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mMoonPath:Landroid/graphics/Path;

    const v1, 0x42dc9b23

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x4255d917

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x42f647ae    # 123.14f

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    const v4, 0x428f9168

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v5

    const v5, 0x42f647ae    # 123.14f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    const v6, 0x42ba39db

    iget v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v7

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 245
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mMoonPath:Landroid/graphics/Path;

    const v1, 0x42f6472b    # 123.139f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    const v2, 0x42f3b6c9

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    const v3, 0x42c7ad0e

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v11

    const v5, 0x428e3127    # 71.096f

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v6, v11

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 251
    return-void
.end method

.method private initPaint()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 254
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mMoonPaint:Landroid/graphics/Paint;

    .line 255
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mMoonPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 256
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mMoonPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mPaintColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 257
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mMoonPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 258
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mMoonPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 259
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mMoonPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->BUTT:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 261
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPaint1:Landroid/graphics/Paint;

    .line 262
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPaint1:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 263
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPaint1:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 264
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPaint1:Landroid/graphics/Paint;

    const/high16 v1, 0x41400000    # 12.0f

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 265
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPaint1:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mPaintColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 266
    return-void
.end method


# virtual methods
.method public cancelAnimation()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 432
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 433
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 435
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 436
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 438
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni3:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 439
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 441
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->RotationAni:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->RotationAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 442
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->RotationAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 445
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mIsActiveAnimationThread:Z

    .line 446
    return-void
.end method

.method public getScale()F
    .locals 1

    .prologue
    .line 470
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 428
    return-object p0
.end method

.method public isRunning()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 450
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni3:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->RotationAni:Landroid/animation/ValueAnimator;

    if-nez v1, :cond_1

    .line 460
    :cond_0
    :goto_0
    return v0

    .line 457
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    .line 458
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni3:Landroid/animation/ValueAnimator;

    .line 459
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->RotationAni:Landroid/animation/ValueAnimator;

    .line 460
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 13
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v4, 0x43960000    # 300.0f

    const/4 v2, 0x0

    .line 362
    :try_start_0
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    cmpg-float v0, v0, v2

    if-gtz v0, :cond_1

    .line 363
    const-string v0, ""

    const-string v1, "scale is less then 0"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    :cond_0
    :goto_0
    return-void

    .line 367
    :cond_1
    const/4 v9, 0x0

    .line 368
    .local v9, "dx":F
    const/4 v8, 0x0

    .line 369
    .local v8, "cloudyX":F
    const/4 v12, 0x0

    .line 370
    .local v12, "moonXY":F
    const/4 v7, 0x0

    .line 372
    .local v7, "angle":F
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mIsActiveAnimationThread:Z

    if-eqz v0, :cond_2

    .line 373
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v9

    .line 374
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v8

    .line 375
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v12

    .line 376
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->RotationAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v7

    .line 379
    :cond_2
    const/16 v6, 0x1f

    .line 381
    .local v6, "saveFlag":I
    const/16 v6, 0xf

    .line 384
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float v1, v2, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float v3, v4, v0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v4, v0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->saveLayer(FFFFLandroid/graphics/Paint;I)I

    move-result v11

    .line 387
    .local v11, "maskingLayer":I
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 389
    const v0, 0x42905b8f

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v0, v1

    const v1, 0x42b939de

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v1, v2

    invoke-virtual {p1, v7, v0, v1}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 390
    neg-float v0, v12

    invoke-virtual {p1, v0, v12}, Landroid/graphics/Canvas;->translate(FF)V

    .line 391
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mMoonPath:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mMoonPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 393
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 394
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mMoonPaint:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v8, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->drawMaskingArea(Landroid/graphics/Canvas;FLandroid/graphics/Paint;)V

    .line 396
    invoke-virtual {p1, v11}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 398
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 400
    const/4 v0, 0x0

    invoke-virtual {p1, v8, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 401
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 403
    neg-float v0, v9

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 404
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_left:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mMoonPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 406
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 407
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_top:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mMoonPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 408
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 410
    const/4 v0, 0x0

    invoke-virtual {p1, v9, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 411
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_right:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mMoonPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 413
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 415
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 417
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    .line 418
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->cancelAnimation()V

    .line 419
    const-string v0, ""

    const-string v1, "cancelA"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 422
    .end local v6    # "saveFlag":I
    .end local v7    # "angle":F
    .end local v8    # "cloudyX":F
    .end local v9    # "dx":F
    .end local v11    # "maskingLayer":I
    .end local v12    # "moonXY":F
    :catch_0
    move-exception v10

    .line 423
    .local v10, "e":Ljava/lang/Exception;
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0
.end method

.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 518
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 479
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onViewDetachedFromWindow : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 480
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mMoonPaint:Landroid/graphics/Paint;

    .line 481
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPaint1:Landroid/graphics/Paint;

    .line 482
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_left:Landroid/graphics/Path;

    .line 483
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_right:Landroid/graphics/Path;

    .line 484
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mCloudPath_top:Landroid/graphics/Path;

    .line 485
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mMoonPath:Landroid/graphics/Path;

    .line 486
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mTempCanvas:Landroid/graphics/Canvas;

    .line 488
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 489
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 490
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 492
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_1

    .line 493
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 494
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 496
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni3:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_2

    .line 497
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 498
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni3:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 500
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->RotationAni:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_3

    .line 501
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->RotationAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 502
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->RotationAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    .line 506
    :cond_3
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    .line 507
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    .line 508
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni3:Landroid/animation/ValueAnimator;

    .line 509
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->RotationAni:Landroid/animation/ValueAnimator;

    .line 510
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mTempBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_4

    .line 511
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mTempBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 513
    :cond_4
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mTempBitmap:Landroid/graphics/Bitmap;

    .line 514
    return-void
.end method

.method public setPaintColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 71
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mPaintColor:I

    .line 72
    return-void
.end method

.method public setScale(F)V
    .locals 0
    .param p1, "scale"    # F

    .prologue
    .line 465
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    .line 466
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->init()V

    .line 467
    return-void
.end method

.method public setWidthAndHeight(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 474
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, p1, p2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 475
    .local v0, "l":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 477
    return-void
.end method

.method public startAnimation()V
    .locals 0

    .prologue
    .line 349
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->startAnimationThread()V

    .line 350
    return-void
.end method

.method public startAnimationThread()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, -0x1

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 297
    iput-boolean v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->isStop:Z

    .line 298
    new-array v1, v8, [F

    aput v4, v1, v5

    const v2, -0x3fd33333    # -2.7f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    aput v2, v1, v6

    const/4 v2, 0x2

    aput v4, v1, v2

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    .line 299
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v1, v7}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 300
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x5dc

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 301
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 302
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    .line 304
    const/4 v1, 0x5

    new-array v1, v1, [F

    aput v4, v1, v5

    const/high16 v2, -0x3ef80000    # -8.5f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    aput v2, v1, v6

    const/4 v2, 0x2

    aput v4, v1, v2

    const/high16 v2, 0x41080000    # 8.5f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    aput v2, v1, v8

    const/4 v2, 0x4

    aput v4, v1, v2

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    .line 305
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    invoke-virtual {v1, v7}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 306
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x1194

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 307
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 308
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni2:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    .line 310
    const/4 v1, 0x5

    new-array v1, v1, [F

    aput v4, v1, v5

    const/high16 v2, 0x40a00000    # 5.0f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    aput v2, v1, v6

    const/4 v2, 0x2

    aput v4, v1, v2

    const/high16 v2, -0x3ee00000    # -10.0f

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mScaleFactor:F

    mul-float/2addr v2, v3

    aput v2, v1, v8

    const/4 v2, 0x4

    aput v4, v1, v2

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni3:Landroid/animation/ValueAnimator;

    .line 312
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni3:Landroid/animation/ValueAnimator;

    invoke-virtual {v1, v7}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 313
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni3:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0xfa0

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 314
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni3:Landroid/animation/ValueAnimator;

    new-instance v2, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v2}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 315
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni3:Landroid/animation/ValueAnimator;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 316
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->TranslateAni3:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    .line 318
    new-array v1, v8, [F

    fill-array-data v1, :array_0

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->RotationAni:Landroid/animation/ValueAnimator;

    .line 319
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->RotationAni:Landroid/animation/ValueAnimator;

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setObjectValues([Ljava/lang/Object;)V

    .line 320
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->RotationAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v1, v7}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 321
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->RotationAni:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 322
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->RotationAni:Landroid/animation/ValueAnimator;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 323
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->RotationAni:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    .line 325
    iput-boolean v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->mIsActiveAnimationThread:Z

    .line 326
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;)V

    .line 340
    .local v0, "t":Ljava/lang/Thread;
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 341
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 342
    return-void

    .line 318
    :array_0
    .array-data 4
        0x0
        0x40a00000    # 5.0f
        0x0
    .end array-data
.end method

.method public stopAnimation()V
    .locals 1

    .prologue
    .line 269
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->isStop:Z

    .line 270
    return-void
.end method

.method public update(I)V
    .locals 0
    .param p1, "delta"    # I

    .prologue
    .line 345
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/MostClearIconView;->postInvalidate()V

    .line 346
    return-void
.end method
