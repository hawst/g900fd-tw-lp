.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;
.super Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
.source "HourWeatherInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field mCurrentTemp:F

.field mDate:I

.field mHour:I

.field mIconNum:I

.field mRainForecast:I

.field mTempScale:I

.field mUpdateDate:Ljava/lang/String;

.field mWindSpeed:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 128
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo$1;

    invoke-direct {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo$1;-><init>()V

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;-><init>()V

    .line 22
    return-void
.end method

.method public constructor <init>(IFIIIILjava/lang/String;I)V
    .locals 0
    .param p1, "tempScale"    # I
    .param p2, "currentTemp"    # F
    .param p3, "hour"    # I
    .param p4, "RainForecast"    # I
    .param p5, "windSpeed"    # I
    .param p6, "iconNum"    # I
    .param p7, "updateDate"    # Ljava/lang/String;
    .param p8, "date"    # I

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;-><init>()V

    .line 27
    iput p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mCurrentTemp:F

    .line 28
    iput p3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mHour:I

    .line 29
    iput p4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mRainForecast:I

    .line 30
    iput p5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mWindSpeed:I

    .line 31
    iput p6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mIconNum:I

    .line 32
    iput-object p7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mUpdateDate:Ljava/lang/String;

    .line 33
    iput p8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mDate:I

    .line 34
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mTempScale:I

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 117
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;-><init>()V

    .line 118
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mCurrentTemp:F

    .line 119
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mDate:I

    .line 120
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mHour:I

    .line 121
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mRainForecast:I

    .line 122
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mWindSpeed:I

    .line 123
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mIconNum:I

    .line 124
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mUpdateDate:Ljava/lang/String;

    .line 125
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mTempScale:I

    .line 126
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 102
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public getCurrentTemp()F
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mCurrentTemp:F

    return v0
.end method

.method public getHour()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mHour:I

    return v0
.end method

.method public getHourDate()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mDate:I

    return v0
.end method

.method public getIconNum()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mIconNum:I

    return v0
.end method

.method public getRainForecast()I
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mRainForecast:I

    return v0
.end method

.method public getTempScale()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mTempScale:I

    return v0
.end method

.method public getUpdateDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mUpdateDate:Ljava/lang/String;

    return-object v0
.end method

.method public getWindSpeed()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mWindSpeed:I

    return v0
.end method

.method public setCurrentTemp(F)V
    .locals 0
    .param p1, "temperature"    # F

    .prologue
    .line 58
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mCurrentTemp:F

    .line 59
    return-void
.end method

.method public setHour(I)V
    .locals 0
    .param p1, "mHour"    # I

    .prologue
    .line 66
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mHour:I

    .line 67
    return-void
.end method

.method public setHourDate(I)V
    .locals 0
    .param p1, "date"    # I

    .prologue
    .line 42
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mDate:I

    .line 43
    return-void
.end method

.method public setIconNum(I)V
    .locals 0
    .param p1, "mIconNum"    # I

    .prologue
    .line 50
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mIconNum:I

    .line 51
    return-void
.end method

.method public setRainForecast(I)V
    .locals 0
    .param p1, "mRainForecast"    # I

    .prologue
    .line 74
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mRainForecast:I

    .line 75
    return-void
.end method

.method public setTempScale(I)V
    .locals 0
    .param p1, "mTempScale"    # I

    .prologue
    .line 98
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mTempScale:I

    .line 99
    return-void
.end method

.method public setUpdateDate(Ljava/lang/String;)V
    .locals 0
    .param p1, "mUpdateDate"    # Ljava/lang/String;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mUpdateDate:Ljava/lang/String;

    .line 91
    return-void
.end method

.method public setWindSpeed(I)V
    .locals 0
    .param p1, "mWindSpeed"    # I

    .prologue
    .line 82
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mWindSpeed:I

    .line 83
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 106
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mCurrentTemp:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 107
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mDate:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 108
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mHour:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 109
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mRainForecast:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 110
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mWindSpeed:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 111
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mIconNum:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 112
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mUpdateDate:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 113
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;->mTempScale:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 114
    return-void
.end method
