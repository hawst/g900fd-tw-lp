.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;
.super Landroid/app/ListActivity;
.source "MenuAdd.java"

# interfaces
.implements Lcom/sec/android/widgetapp/ap/hero/accuweather/model/IActivityVisibleState;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;,
        Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$SearchListAdapter;
    }
.end annotation


# instance fields
.field private COLLAB_MODE:Z

.field cities:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;",
            ">;"
        }
    .end annotation
.end field

.field private cityListAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;

.field private cityListAdapter2:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;

.field private citydb:Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;

.field citynames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private ctx:Landroid/content/Context;

.field private getDataHandler:Landroid/os/Handler;

.field getLocDataHandler:Landroid/os/Handler;

.field private gps_btn:Landroid/widget/RelativeLayout;

.field private httpClient:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

.field private inputManager:Landroid/view/inputmethod/InputMethodManager;

.field private intent:Landroid/content/Intent;

.field private lstCity:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;",
            ">;"
        }
    .end annotation
.end field

.field mBackKeyHandler:Landroid/os/Handler;

.field private mClearTextBtn:Landroid/widget/ImageView;

.field private mDropBoxHeight:I

.field private mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

.field public mHttpThreads:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Thread;",
            ">;"
        }
    .end annotation
.end field

.field private mIsKeyboardShown:Z

.field private mIsLanguageHindiorBengali:Z

.field mIsVisible:Z

.field public mKeyboardHanlder:Landroid/os/Handler;

.field public mLoadingDialog:Landroid/app/Dialog;

.field private mMapInfoPostErrorHandler:Landroid/os/Handler;

.field private mMapInfoPostProcHandler:Landroid/os/Handler;

.field public mNetworkErrorDialog:Landroid/app/Dialog;

.field private mNoSearchText:Landroid/widget/TextView;

.field public mPopupDialog:Landroid/app/Dialog;

.field private mPushDialogButtonType:I

.field private volatile mRequestCanceled:Z

.field private mRootView:Landroid/view/View;

.field private mSearchLayout:Landroid/widget/LinearLayout;

.field private mSearchLayoutOutLine:Landroid/widget/RelativeLayout;

.field private mSearchText:Landroid/widget/TextView;

.field private mTextHighlight:Z

.field private mTextWatcher:Landroid/text/TextWatcher;

.field private mTimer:Ljava/util/Timer;

.field private mTopLayout:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;

.field mapHandler:Landroid/os/Handler;

.field private map_btn:Landroid/widget/RelativeLayout;

.field private mcl:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

.field private parser:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;

.field public performMapCancled:Z

.field point:[I

.field private refresh_toast_display:Z

.field region:I

.field private searchEditText:Landroid/widget/AutoCompleteTextView;

.field searchHandler:Landroid/os/Handler;

.field syncstamp:J

.field private tempscale:I

.field public textcount:I

.field private toastHint:Landroid/widget/Toast;

.field private urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 105
    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mHttpThreads:Ljava/util/ArrayList;

    .line 136
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mLoadingDialog:Landroid/app/Dialog;

    .line 138
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mNetworkErrorDialog:Landroid/app/Dialog;

    .line 140
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mPopupDialog:Landroid/app/Dialog;

    .line 144
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->refresh_toast_display:Z

    .line 148
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mIsVisible:Z

    .line 150
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->textcount:I

    .line 152
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->region:I

    .line 158
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mClearTextBtn:Landroid/widget/ImageView;

    .line 160
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->tempscale:I

    .line 162
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->COLLAB_MODE:Z

    .line 172
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    .line 176
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mTextHighlight:Z

    .line 178
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mIsKeyboardShown:Z

    .line 180
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mTopLayout:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;

    .line 181
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mRootView:Landroid/view/View;

    .line 184
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mSearchLayoutOutLine:Landroid/widget/RelativeLayout;

    .line 186
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mIsLanguageHindiorBengali:Z

    .line 744
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$20;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$20;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mapHandler:Landroid/os/Handler;

    .line 761
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->performMapCancled:Z

    .line 789
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$21;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$21;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mMapInfoPostProcHandler:Landroid/os/Handler;

    .line 825
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$22;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$22;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mMapInfoPostErrorHandler:Landroid/os/Handler;

    .line 954
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$23;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$23;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mBackKeyHandler:Landroid/os/Handler;

    .line 1039
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$24;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$24;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mKeyboardHanlder:Landroid/os/Handler;

    .line 1268
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mPushDialogButtonType:I

    .line 1311
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$30;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getLocDataHandler:Landroid/os/Handler;

    .line 1413
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$31;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$31;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchHandler:Landroid/os/Handler;

    .line 1466
    const/4 v0, -0x2

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mDropBoxHeight:I

    .line 1483
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$32;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$32;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mTextWatcher:Landroid/text/TextWatcher;

    .line 1672
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getDataHandler:Landroid/os/Handler;

    .line 2005
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mRootView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->citydb:Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;
    .param p1, "x1"    # I

    .prologue
    .line 105
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mDropBoxHeight:I

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->performMapCities()V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;
    .param p1, "x1"    # Z

    .prologue
    .line 105
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->setKeyboardVisible(Z)V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->enableEditField()V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;ZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Z

    .prologue
    .line 105
    invoke-direct {p0, p1, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->setKeyboardVisible(ZZ)V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 105
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mTextHighlight:Z

    return v0
.end method

.method static synthetic access$1600(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getSearchData()V

    return-void
.end method

.method static synthetic access$1700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mNoSearchText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getLoc()V

    return-void
.end method

.method static synthetic access$1900(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 105
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mPushDialogButtonType:I

    return v0
.end method

.method static synthetic access$1902(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;
    .param p1, "x1"    # I

    .prologue
    .line 105
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mPushDialogButtonType:I

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mSearchLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 105
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->performClickGpsBtn(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$2100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->hideLoadingDialog()V

    return-void
.end method

.method static synthetic access$2200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;
    .param p1, "x1"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    return-object p1
.end method

.method static synthetic access$2300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 105
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->tempscale:I

    return v0
.end method

.method static synthetic access$2400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$2502(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;
    .param p1, "x1"    # Z

    .prologue
    .line 105
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mIsKeyboardShown:Z

    return p1
.end method

.method static synthetic access$2600(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getLocationInfo()V

    return-void
.end method

.method static synthetic access$2700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 105
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mRequestCanceled:Z

    return v0
.end method

.method static synthetic access$2702(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;
    .param p1, "x1"    # Z

    .prologue
    .line 105
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mRequestCanceled:Z

    return p1
.end method

.method static synthetic access$2800(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->stopHttpThread()V

    return-void
.end method

.method static synthetic access$2900(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mcl:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/widget/AutoCompleteTextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->hideAllDialog()V

    return-void
.end method

.method static synthetic access$3100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 105
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->COLLAB_MODE:Z

    return v0
.end method

.method static synthetic access$3200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->lstCity:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$3202(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->lstCity:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$3300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->cityListAdapter2:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;

    return-object v0
.end method

.method static synthetic access$3302(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;)Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;
    .param p1, "x1"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->cityListAdapter2:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;

    return-object p1
.end method

.method static synthetic access$3400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->parser:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->disableEditField()V

    return-void
.end method

.method static synthetic access$3600(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->cityListAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mClearTextBtn:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    return-object v0
.end method

.method static synthetic access$3900(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 105
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mIsLanguageHindiorBengali:Z

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->setDropdownMenuHeight()V

    return-void
.end method

.method static synthetic access$4000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->httpClient:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    return-object v0
.end method

.method static synthetic access$4100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getDataHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$4200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;
    .param p1, "x1"    # I

    .prologue
    .line 105
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->checkResultCode(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->toastHint:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;
    .param p1, "x1"    # Landroid/widget/Toast;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->toastHint:Landroid/widget/Toast;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->gps_btn:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 105
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->refresh_toast_display:Z

    return v0
.end method

.method static synthetic access$702(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;
    .param p1, "x1"    # Z

    .prologue
    .line 105
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->refresh_toast_display:Z

    return p1
.end method

.method static synthetic access$800(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->map_btn:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getMccCode()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private checkResultCode(I)I
    .locals 2
    .param p1, "resultCode"    # I

    .prologue
    .line 1950
    const/4 v0, 0x0

    .line 1951
    .local v0, "result":I
    const/4 v1, -0x1

    if-ne v1, p1, :cond_1

    .line 1952
    const/16 v0, 0x3e7

    .line 1956
    :cond_0
    :goto_0
    return v0

    .line 1953
    :cond_1
    if-nez p1, :cond_0

    .line 1954
    const/16 v0, 0x3e7

    goto :goto_0
.end method

.method private disableEditField()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1238
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;

    if-eqz v0, :cond_0

    .line 1239
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setFocusable(Z)V

    .line 1240
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setFocusableInTouchMode(Z)V

    .line 1242
    :cond_0
    return-void
.end method

.method private enableEditField()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1245
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;

    if-eqz v0, :cond_0

    .line 1246
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setFocusable(Z)V

    .line 1247
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setFocusableInTouchMode(Z)V

    .line 1249
    :cond_0
    return-void
.end method

.method private getData(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;)V
    .locals 1
    .param p1, "cityInfo"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    .prologue
    .line 1663
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->isNetWorkConnected(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1664
    const v0, 0x7f0d0020

    invoke-static {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    .line 1670
    :goto_0
    return-void

    .line 1668
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mRequestCanceled:Z

    .line 1669
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->performGetData(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;)V

    goto :goto_0
.end method

.method private getLoc()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1252
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->isNetWorkConnected(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1253
    const v0, 0x7f0d0020

    invoke-static {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    .line 1266
    :goto_0
    return-void

    .line 1256
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->hideAllDialog()V

    .line 1258
    const-string v0, "location"

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/location/LocationManager;

    .line 1260
    .local v7, "locationManager":Landroid/location/LocationManager;
    const-string v0, "gps"

    invoke-virtual {v7, v0}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "network"

    .line 1261
    invoke-virtual {v7, v0}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1262
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->showLocatingDialog()V

    .line 1264
    :cond_2
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "gLI Gbtn "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1265
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mcl:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getLocDataHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    move v4, v2

    move v5, v2

    move v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;->performGetCurrentLocation(Landroid/os/Handler;IZZZZ)V

    goto :goto_0
.end method

.method private getLocationInfo()V
    .locals 3

    .prologue
    .line 1270
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090011

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1271
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->isAgreeUseLocation(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1272
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->disableEditField()V

    .line 1273
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mPushDialogButtonType:I

    .line 1274
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$28;

    invoke-direct {v2, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$28;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->showUseCurrentLocation(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)Landroid/app/Dialog;

    move-result-object v0

    .line 1289
    .local v0, "dialog":Landroid/app/Dialog;
    if-eqz v0, :cond_0

    .line 1290
    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$29;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$29;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1309
    .end local v0    # "dialog":Landroid/app/Dialog;
    :cond_0
    :goto_0
    return-void

    .line 1303
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->gps_btn:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->performClickGpsBtn(Landroid/view/View;)V

    goto :goto_0

    .line 1306
    :cond_2
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->gps_btn:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->performClickGpsBtn(Landroid/view/View;)V

    goto :goto_0
.end method

.method private getMccCode()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x3

    .line 844
    const-string v2, "mcc"

    const-string v3, "@@@"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 845
    const-string v2, "phone"

    .line 846
    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 847
    .local v0, "manager":Landroid/telephony/TelephonyManager;
    const-string v2, "mcc"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "code = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 848
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v1

    .line 849
    .local v1, "mcc":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v5, :cond_0

    .line 850
    const/4 v2, 0x0

    invoke-virtual {v1, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 851
    :cond_0
    return-object v1
.end method

.method private getSearchData()V
    .locals 1

    .prologue
    .line 1605
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->isNetWorkConnected(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1606
    const v0, 0x7f0d0020

    invoke-static {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    .line 1624
    :goto_0
    return-void

    .line 1609
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 1610
    const v0, 0x7f0d006e

    invoke-static {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    goto :goto_0

    .line 1616
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->performSearch()V

    goto :goto_0
.end method

.method private hideAllDialog()V
    .locals 1

    .prologue
    .line 1128
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->hideLoadingDialog()V

    .line 1129
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mNetworkErrorDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 1131
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mNetworkErrorDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 1136
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mPopupDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    .line 1138
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mPopupDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1142
    :cond_1
    :goto_1
    return-void

    .line 1139
    :catch_0
    move-exception v0

    goto :goto_1

    .line 1132
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method private hideLoadingDialog()V
    .locals 3

    .prologue
    .line 1213
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->enableEditField()V

    .line 1214
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mLoadingDialog:Landroid/app/Dialog;

    if-eqz v1, :cond_0

    .line 1216
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1221
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mLoadingDialog:Landroid/app/Dialog;

    .line 1223
    :cond_0
    return-void

    .line 1217
    :catch_0
    move-exception v0

    .line 1219
    .local v0, "ex":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MenuAdd.hideLoadingDialog() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private performClickGpsBtn(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 601
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "G Btn Clicked "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 602
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->disableEditField()V

    .line 603
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mPushDialogButtonType:I

    .line 604
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mPopupDialog:Landroid/app/Dialog;

    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$16;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$16;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    new-instance v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$17;

    invoke-direct {v4, p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$17;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;Landroid/view/View;)V

    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$18;

    invoke-direct {v5, p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$18;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;Landroid/view/View;)V

    iget-boolean v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->COLLAB_MODE:Z

    move-object v0, p0

    move-object v1, p1

    invoke-static/range {v0 .. v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->performClickGpsBtn(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/LocationGetter;Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;Landroid/content/DialogInterface$OnDismissListener;Z)Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mPopupDialog:Landroid/app/Dialog;

    .line 649
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mPopupDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 650
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mPopupDialog:Landroid/app/Dialog;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$19;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$19;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 662
    :cond_0
    return-void
.end method

.method private performGetData(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;)V
    .locals 11
    .param p1, "cityInfo"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    .prologue
    .line 1675
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeHeader()Lorg/apache/http/message/HeaderGroup;

    move-result-object v4

    .line 1676
    .local v4, "headerGroup":Lorg/apache/http/message/HeaderGroup;
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getTempScaleSetting(Landroid/content/Context;)I

    move-result v8

    .line 1677
    .local v8, "tempScaleSetting":I
    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v6

    .line 1678
    .local v6, "location":Ljava/lang/String;
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "performGD tempSC= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1679
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    .line 1680
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkLanguage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 1679
    invoke-virtual {v0, v6, v8, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeUrlForGetDetailData(Ljava/lang/String;ILjava/lang/String;)Ljava/net/URL;

    move-result-object v3

    .line 1682
    .local v3, "url":Ljava/net/URL;
    if-eqz v3, :cond_2

    .line 1686
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->syncstamp:J

    .line 1687
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->isNetWorkConnected(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1688
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->showLoadingDialog()V

    .line 1691
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mHttpThreads:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 1692
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mHttpThreads:Ljava/util/ArrayList;

    .line 1694
    :cond_1
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    const/4 v1, 0x1

    iget-wide v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->syncstamp:J

    invoke-direct {v0, p0, v1, v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;-><init>(Landroid/content/Context;ZJ)V

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;

    invoke-direct {v5, p0, v8, p1, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$34;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;ILcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;Ljava/lang/String;)V

    move-object v2, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->get(ILandroid/content/Context;Ljava/net/URL;Lorg/apache/http/message/HeaderGroup;Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;)Ljava/lang/Thread;

    move-result-object v7

    .line 1936
    .local v7, "t":Ljava/lang/Thread;
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1940
    .end local v7    # "t":Ljava/lang/Thread;
    :goto_0
    return-void

    .line 1938
    :cond_2
    const-string v0, ""

    const-string v1, "pGD url is n"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private performMapCities()V
    .locals 2

    .prologue
    .line 766
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->isNetWorkConnected(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 767
    const v0, 0x7f0d0020

    invoke-static {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    .line 781
    :goto_0
    return-void

    .line 771
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mapHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 772
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mapHandler:Landroid/os/Handler;

    const v1, -0x13d30

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 774
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->performMapCancled:Z

    .line 776
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->stopHttpThread()V

    .line 777
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->showLoadingDialog()V

    .line 779
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->setupGetMapInfoManager()V

    .line 780
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->performMapCities()V

    goto :goto_0
.end method

.method private performSearch()V
    .locals 8

    .prologue
    .line 1627
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeHeader()Lorg/apache/http/message/HeaderGroup;

    move-result-object v4

    .line 1628
    .local v4, "headerGroup":Lorg/apache/http/message/HeaderGroup;
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1629
    .local v6, "parameter":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    .line 1630
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkLanguage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 1629
    invoke-virtual {v0, v6, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeUrlForGetTextSearching(Ljava/lang/String;Ljava/lang/String;)Ljava/net/URL;

    move-result-object v3

    .line 1631
    .local v3, "url":Ljava/net/URL;
    if-eqz v3, :cond_2

    .line 1634
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->isNetWorkConnected(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1635
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->showLoadingDialog()V

    .line 1638
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mHttpThreads:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 1639
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mHttpThreads:Ljava/util/ArrayList;

    .line 1641
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->httpClient:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$33;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$33;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    move-object v2, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->get(ILandroid/content/Context;Ljava/net/URL;Lorg/apache/http/message/HeaderGroup;Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;)Ljava/lang/Thread;

    move-result-object v7

    .line 1658
    .local v7, "t":Ljava/lang/Thread;
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1660
    .end local v7    # "t":Ljava/lang/Thread;
    :cond_2
    return-void
.end method

.method private setDropdownMenuHeight()V
    .locals 4

    .prologue
    .line 1469
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DBH = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mDropBoxHeight:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1470
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mDropBoxHeight:I

    if-gtz v1, :cond_0

    .line 1471
    const/4 v1, -0x2

    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mDropBoxHeight:I

    .line 1472
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mDropBoxHeight:I

    invoke-virtual {v1, v2}, Landroid/widget/AutoCompleteTextView;->setDropDownHeight(I)V

    .line 1481
    :goto_0
    return-void

    .line 1474
    :cond_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 1475
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mDropBoxHeight:I

    invoke-virtual {v1, v2}, Landroid/widget/AutoCompleteTextView;->setDropDownHeight(I)V

    goto :goto_0

    .line 1477
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f0b0000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v1

    double-to-int v0, v1

    .line 1478
    .local v0, "margin":I
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mDropBoxHeight:I

    sub-int/2addr v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/AutoCompleteTextView;->setDropDownHeight(I)V

    goto :goto_0
.end method

.method private setKeyboardVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 1049
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->setKeyboardVisible(ZZ)V

    .line 1050
    return-void
.end method

.method private setKeyboardVisible(ZZ)V
    .locals 4
    .param p1, "visible"    # Z
    .param p2, "isIgnoreDevice"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1053
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->inputManager:Landroid/view/inputmethod/InputMethodManager;

    invoke-static {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkKeyboard(Landroid/view/inputmethod/InputMethodManager;Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v3, :cond_0

    if-nez p2, :cond_0

    .line 1054
    const/4 p1, 0x0

    .line 1056
    :cond_0
    if-ne p1, v3, :cond_1

    .line 1058
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->inputManager:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 1059
    iput-boolean v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mIsKeyboardShown:Z

    .line 1061
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mSearchText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a006d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHintTextColor(I)V

    .line 1072
    :goto_0
    return-void

    .line 1065
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;

    if-eqz v0, :cond_2

    .line 1066
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->inputManager:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1067
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mIsKeyboardShown:Z

    .line 1070
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mSearchText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a006c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHintTextColor(I)V

    goto :goto_0
.end method

.method private setRTLEditText()V
    .locals 12

    .prologue
    const/16 v11, 0xb

    const/16 v10, 0xf

    const/4 v9, -0x1

    const/4 v8, -0x2

    .line 189
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Landroid/widget/AutoCompleteTextView;->setTextDirection(I)V

    .line 190
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;

    const/16 v7, 0x15

    invoke-virtual {v6, v7}, Landroid/widget/AutoCompleteTextView;->setGravity(I)V

    .line 191
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v6}, Landroid/widget/AutoCompleteTextView;->getPaddingLeft()I

    move-result v0

    .line 192
    .local v0, "left":I
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v6}, Landroid/widget/AutoCompleteTextView;->getPaddingRight()I

    move-result v4

    .line 195
    .local v4, "right":I
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v8, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 198
    .local v1, "params":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v1, v11, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 199
    invoke-virtual {v1, v10, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 200
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b0016

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v6

    iput v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 201
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b0015

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 202
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mSearchLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 205
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v8, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 208
    .local v3, "paramsIcon":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v3, v11, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 209
    invoke-virtual {v3, v10, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 210
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b0014

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v6

    iput v6, v3, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 212
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mSearchLayoutOutLine:Landroid/widget/RelativeLayout;

    const v7, 0x7f080109

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 213
    .local v5, "v":Landroid/widget/ImageView;
    invoke-virtual {v5, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 215
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mSearchLayoutOutLine:Landroid/widget/RelativeLayout;

    invoke-virtual {v6, v5}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 216
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mSearchLayoutOutLine:Landroid/widget/RelativeLayout;

    invoke-virtual {v6, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 218
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v8, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 221
    .local v2, "params2":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v6, 0x9

    invoke-virtual {v2, v6, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 222
    invoke-virtual {v2, v10, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 223
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mClearTextBtn:Landroid/widget/ImageView;

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 224
    return-void
.end method

.method private setSeachBtnController()V
    .locals 2

    .prologue
    .line 1115
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->gps_btn:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$25;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$25;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1125
    return-void
.end method

.method private setupGetMapInfoManager()V
    .locals 2

    .prologue
    .line 784
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->tempscale:I

    invoke-static {v0, p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->getInstance(Landroid/content/Context;Landroid/app/Activity;I)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    .line 785
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mMapInfoPostProcHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->setPostProcHandler(Landroid/os/Handler;)V

    .line 786
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mMapInfoPostErrorHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->setErrorHandler(Landroid/os/Handler;)V

    .line 787
    return-void
.end method

.method private showLoadingDialog()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1145
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 1147
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->disableEditField()V

    .line 1148
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mLoadingDialog:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 1149
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mRequestCanceled:Z

    .line 1150
    const/16 v0, 0x3ef

    invoke-static {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->showDialog(Landroid/content/Context;I)Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mLoadingDialog:Landroid/app/Dialog;

    .line 1152
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mLoadingDialog:Landroid/app/Dialog;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$26;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$26;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1178
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 1179
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 1180
    return-void
.end method

.method private showSoftKeyboard(Z)V
    .locals 4
    .param p1, "isShowing"    # Z

    .prologue
    .line 583
    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->inputManager:Landroid/view/inputmethod/InputMethodManager;

    invoke-static {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkKeyboard(Landroid/view/inputmethod/InputMethodManager;Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 591
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 592
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 593
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mKeyboardHanlder:Landroid/os/Handler;

    const-wide/16 v2, 0x258

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 598
    .end local v0    # "msg":Landroid/os/Message;
    :goto_0
    return-void

    .line 596
    :cond_0
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->setKeyboardVisible(Z)V

    goto :goto_0
.end method

.method private stopHttpThread()V
    .locals 3

    .prologue
    .line 1226
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mHttpThreads:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 1227
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 1228
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Thread;

    .line 1229
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 1231
    .end local v0    # "t":Ljava/lang/Thread;
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mHttpThreads:Ljava/util/ArrayList;

    .line 1233
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mapHandler:Landroid/os/Handler;

    if-eqz v1, :cond_2

    .line 1234
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mapHandler:Landroid/os/Handler;

    const v2, -0x140b4

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1235
    :cond_2
    return-void
.end method


# virtual methods
.method public cleanResource()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 975
    const-string v0, ""

    const-string v1, "===stopHttpThread==="

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 976
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->stopHttpThread()V

    .line 978
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->lstCity:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 979
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->lstCity:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 980
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->lstCity:Ljava/util/ArrayList;

    .line 983
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->cityListAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;

    if-eqz v0, :cond_1

    .line 984
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->cityListAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;->clear()V

    .line 985
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->cityListAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;

    .line 988
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->cityListAdapter2:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;

    if-eqz v0, :cond_2

    .line 989
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->cityListAdapter2:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;->clear()V

    .line 990
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->cityListAdapter2:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;

    .line 993
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->cities:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 994
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->cities:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 995
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->cities:Ljava/util/ArrayList;

    .line 998
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->citynames:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    .line 999
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->citynames:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1000
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->citynames:Ljava/util/ArrayList;

    .line 1003
    :cond_4
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->httpClient:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    .line 1004
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->citydb:Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;

    .line 1005
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->intent:Landroid/content/Intent;

    .line 1006
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    .line 1007
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->parser:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;

    .line 1009
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mLoadingDialog:Landroid/app/Dialog;

    .line 1010
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mNetworkErrorDialog:Landroid/app/Dialog;

    .line 1011
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mPopupDialog:Landroid/app/Dialog;

    .line 1013
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->gps_btn:Landroid/widget/RelativeLayout;

    .line 1014
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->map_btn:Landroid/widget/RelativeLayout;

    .line 1015
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mTextWatcher:Landroid/text/TextWatcher;

    .line 1016
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;

    .line 1017
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mSearchLayout:Landroid/widget/LinearLayout;

    .line 1018
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mSearchText:Landroid/widget/TextView;

    .line 1019
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mNoSearchText:Landroid/widget/TextView;

    .line 1021
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mcl:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    .line 1023
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    if-eqz v0, :cond_5

    .line 1024
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->clearResource()V

    .line 1025
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    .line 1028
    :cond_5
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mTopLayout:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;

    if-eqz v0, :cond_6

    .line 1029
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mTopLayout:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;->setHandler(Landroid/os/Handler;I)V

    .line 1030
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mTopLayout:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;

    .line 1033
    :cond_6
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mBackKeyHandler:Landroid/os/Handler;

    if-eqz v0, :cond_7

    .line 1034
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mBackKeyHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1035
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mBackKeyHandler:Landroid/os/Handler;

    .line 1037
    :cond_7
    return-void
.end method

.method public deleteMe(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 1943
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mHttpThreads:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 1944
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 1945
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1947
    :cond_0
    return-void
.end method

.method public isActivityVisible()Z
    .locals 1

    .prologue
    .line 855
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mIsVisible:Z

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 11
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/16 v10, 0x61a7

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/16 v7, -0x3e7

    .line 665
    const-string v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "@@@@@@@@@@@ "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 666
    if-ne p1, v10, :cond_a

    if-eqz p3, :cond_a

    .line 667
    const-string v4, "cityinfo"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    .line 668
    .local v1, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    if-eqz v1, :cond_1

    .line 669
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLocation()Ljava/lang/String;

    move-result-object v2

    .line 670
    .local v2, "location":Ljava/lang/String;
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    invoke-direct {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;-><init>()V

    .line 671
    .local v0, "cityInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getCity()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setCity(Ljava/lang/String;)V

    .line 672
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLatitude()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setLatitude(Ljava/lang/String;)V

    .line 673
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLongitude()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setLongitude(Ljava/lang/String;)V

    .line 674
    invoke-virtual {v0, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setProvider(I)V

    .line 675
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getState()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setState(Ljava/lang/String;)V

    .line 676
    invoke-virtual {v0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setLocation(Ljava/lang/String;)V

    .line 677
    invoke-virtual {v0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setRealLocation(Ljava/lang/String;)V

    .line 679
    iget-boolean v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->COLLAB_MODE:Z

    if-eqz v4, :cond_0

    .line 681
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 682
    .local v3, "result":Landroid/content/Intent;
    const-string v4, "CITY_NAME"

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getCity()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 683
    const-string v4, "CITY_STATE"

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getState()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 684
    const-string v4, "CITY_ID"

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 686
    const/4 v4, -0x1

    invoke-virtual {p0, v4, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->setResult(ILandroid/content/Intent;)V

    .line 687
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->finish()V

    .line 742
    .end local v0    # "cityInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .end local v1    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    .end local v2    # "location":Ljava/lang/String;
    .end local v3    # "result":Landroid/content/Intent;
    :goto_0
    return-void

    .line 691
    .restart local v0    # "cityInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .restart local v1    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    .restart local v2    # "location":Ljava/lang/String;
    :cond_0
    const/16 v4, 0x12c

    if-ne p2, v4, :cond_9

    .line 692
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;

    invoke-static {v4, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->isRegisteredToCityList(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 693
    const v4, 0x7f0d0022

    invoke-static {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    .line 741
    .end local v0    # "cityInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .end local v1    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    .end local v2    # "location":Ljava/lang/String;
    :cond_1
    :goto_1
    invoke-super {p0, p1, p2, p3}, Landroid/app/ListActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0

    .line 695
    .restart local v0    # "cityInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .restart local v1    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    .restart local v2    # "location":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getIntent()Landroid/content/Intent;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->intent:Landroid/content/Intent;

    .line 696
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->lstCity:Ljava/util/ArrayList;

    if-eqz v4, :cond_3

    .line 697
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->lstCity:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 698
    :cond_3
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;

    if-eqz v4, :cond_4

    .line 699
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;

    const-string v5, ""

    invoke-virtual {v4, v5}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 700
    :cond_4
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->cityListAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;

    if-eqz v4, :cond_5

    .line 701
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->cityListAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;->clear()V

    .line 702
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->cityListAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;->notifyDataSetChanged()V

    .line 704
    :cond_5
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->cityListAdapter2:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;

    if-eqz v4, :cond_6

    .line 705
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->cityListAdapter2:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;->clear()V

    .line 706
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->cityListAdapter2:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;->notifyDataSetChanged()V

    .line 708
    :cond_6
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->intent:Landroid/content/Intent;

    const-string v5, "flags"

    invoke-virtual {v4, v5, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    const/16 v5, 0x7cf

    if-ne v4, v5, :cond_7

    .line 709
    invoke-static {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateLastSelectedLocation(Landroid/content/Context;Ljava/lang/String;)I

    .line 710
    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getData(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;)V

    goto :goto_1

    .line 711
    :cond_7
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->intent:Landroid/content/Intent;

    const-string v5, "flags"

    invoke-virtual {v4, v5, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    const/16 v5, 0x2edf

    if-ne v4, v5, :cond_8

    .line 712
    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getData(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;)V

    goto :goto_1

    .line 713
    :cond_8
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->intent:Landroid/content/Intent;

    const-string v5, "flags"

    invoke-virtual {v4, v5, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    const/16 v5, 0x6977

    if-ne v4, v5, :cond_1

    .line 721
    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getData(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;)V

    goto :goto_1

    .line 725
    :cond_9
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->hideLoadingDialog()V

    .line 726
    iput-boolean v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mRequestCanceled:Z

    goto :goto_1

    .line 730
    .end local v0    # "cityInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .end local v1    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    .end local v2    # "location":Ljava/lang/String;
    :cond_a
    if-ne p1, v10, :cond_b

    if-nez p3, :cond_b

    .line 731
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->cityListAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;->getCount()I

    move-result v4

    if-nez v4, :cond_1

    .line 732
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v4, v9}, Landroid/widget/AutoCompleteTextView;->setFocusable(Z)V

    .line 733
    invoke-direct {p0, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->showSoftKeyboard(Z)V

    goto/16 :goto_1

    .line 735
    :cond_b
    const/16 v4, 0x4b3

    if-ne v4, p1, :cond_1

    .line 736
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getLocationservice(Landroid/content/Context;)I

    move-result v4

    const/16 v5, 0xbbe

    if-ne v4, v5, :cond_1

    .line 737
    const-string v4, ""

    const-string v5, "[oAR] try to find CL"

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 738
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getLocationInfo()V

    goto/16 :goto_1
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 950
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 951
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "bundel"    # Landroid/os/Bundle;

    .prologue
    const v6, 0x7f03002e

    const v10, 0x7f0d0090

    const v9, 0x7f0d0089

    const/4 v8, 0x1

    const v7, 0x7f0d008a

    .line 228
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 230
    const v4, 0x7f03002d

    invoke-virtual {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->setContentView(I)V

    .line 231
    invoke-virtual {p0, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->setActivityVisibleState(Z)V

    .line 232
    iput-object p0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;

    .line 234
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09000f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v4

    iput-boolean v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mTextHighlight:Z

    .line 236
    new-instance v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    invoke-direct {v4, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mcl:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/MyCurrentLocation;

    .line 237
    new-instance v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    invoke-direct {v4, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    .line 238
    new-instance v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;

    invoke-direct {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;-><init>()V

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->parser:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;

    .line 239
    new-instance v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    invoke-direct {v4, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->httpClient:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    .line 241
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getTempScaleSetting(Landroid/content/Context;)I

    move-result v4

    iput v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->tempscale:I

    .line 256
    const v4, 0x7f080110

    invoke-virtual {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mNoSearchText:Landroid/widget/TextView;

    .line 257
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->lstCity:Ljava/util/ArrayList;

    .line 258
    new-instance v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->lstCity:Ljava/util/ArrayList;

    invoke-direct {v4, p0, v6, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->cityListAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;

    .line 259
    new-instance v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->lstCity:Ljava/util/ArrayList;

    invoke-direct {v4, p0, v6, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->cityListAdapter2:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;

    .line 260
    const v4, 0x7f08010a

    invoke-virtual {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/AutoCompleteTextView;

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;

    .line 261
    const v4, 0x7f08010b

    invoke-virtual {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mSearchLayout:Landroid/widget/LinearLayout;

    .line 262
    const v4, 0x7f08010c

    invoke-virtual {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mSearchText:Landroid/widget/TextView;

    .line 263
    const v4, 0x7f08010e

    invoke-virtual {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->gps_btn:Landroid/widget/RelativeLayout;

    .line 264
    const v4, 0x7f08010f

    invoke-virtual {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->map_btn:Landroid/widget/RelativeLayout;

    .line 265
    const v4, 0x7f08010d

    invoke-virtual {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mClearTextBtn:Landroid/widget/ImageView;

    .line 267
    const v4, 0x7f08000c

    invoke-virtual {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mRootView:Landroid/view/View;

    .line 268
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mRootView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v4

    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$1;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    invoke-virtual {v4, v5}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 292
    const v4, 0x7f080108

    invoke-virtual {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mSearchLayoutOutLine:Landroid/widget/RelativeLayout;

    .line 295
    const v4, 0x7f08000c

    invoke-virtual {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mTopLayout:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;

    .line 296
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mTopLayout:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mBackKeyHandler:Landroid/os/Handler;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;->setHandler(Landroid/os/Handler;I)V

    .line 298
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v4}, Landroid/widget/AutoCompleteTextView;->setSingleLine()V

    .line 299
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->cityListAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;

    invoke-virtual {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 300
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->setSeachBtnController()V

    .line 302
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v4, v5}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 305
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;

    const v5, 0x7f0d0025

    invoke-virtual {p0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/AutoCompleteTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 307
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget-object v4, v4, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    .line 309
    .local v2, "locale":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090016

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 310
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->gps_btn:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 311
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->map_btn:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 324
    :goto_0
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->gps_btn:Landroid/widget/RelativeLayout;

    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$2;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 335
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->map_btn:Landroid/widget/RelativeLayout;

    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$3;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$3;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 347
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->gps_btn:Landroid/widget/RelativeLayout;

    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$4;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$4;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 359
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->map_btn:Landroid/widget/RelativeLayout;

    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$5;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$5;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 372
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->map_btn:Landroid/widget/RelativeLayout;

    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$6;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$6;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 383
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->gps_btn:Landroid/widget/RelativeLayout;

    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$7;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$7;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 394
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getListView()Landroid/widget/ListView;

    move-result-object v4

    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$8;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$8;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 418
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->map_btn:Landroid/widget/RelativeLayout;

    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$9;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$9;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 448
    const-string v4, "input_method"

    invoke-virtual {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/inputmethod/InputMethodManager;

    iput-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->inputManager:Landroid/view/inputmethod/InputMethodManager;

    .line 450
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getSystemResolution(Landroid/content/Context;)I

    move-result v3

    .line 452
    .local v3, "resolution":I
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090004

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 453
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mSearchText:Landroid/widget/TextView;

    const v5, 0x7f0d0107

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setHint(I)V

    .line 460
    :goto_1
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mSearchText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a006b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setHintTextColor(I)V

    .line 461
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget-object v4, v4, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v4}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v4

    if-ne v4, v8, :cond_0

    .line 462
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->setRTLEditText()V

    .line 464
    :cond_0
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;

    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$10;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$10;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    invoke-virtual {v4, v5}, Landroid/widget/AutoCompleteTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 471
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;

    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$11;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$11;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    invoke-virtual {v4, v5}, Landroid/widget/AutoCompleteTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 486
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;

    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$12;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$12;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    invoke-virtual {v4, v5}, Landroid/widget/AutoCompleteTextView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 505
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;

    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$13;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$13;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    invoke-virtual {v4, v5}, Landroid/widget/AutoCompleteTextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 521
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;

    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$14;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$14;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    invoke-virtual {v4, v5}, Landroid/widget/AutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 541
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mClearTextBtn:Landroid/widget/ImageView;

    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$15;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$15;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 551
    sget-boolean v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isAutoCaptureToolSupportMode:Z

    if-nez v4, :cond_2

    .line 552
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 553
    .local v1, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_1

    .line 554
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 556
    .local v0, "extras":Landroid/os/Bundle;
    if-eqz v0, :cond_1

    .line 557
    const-string v4, "widget_mode"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_1

    .line 558
    const-string v4, "widget_mode"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setWidgetMode(Landroid/content/Context;I)Z

    .line 563
    .end local v0    # "extras":Landroid/os/Bundle;
    :cond_1
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getRegisteredCityCount(Landroid/content/Context;)I

    move-result v4

    if-nez v4, :cond_2

    iget-boolean v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->COLLAB_MODE:Z

    if-nez v4, :cond_2

    .line 566
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getLocationInfo()V

    .line 570
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_2
    invoke-direct {p0, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->showSoftKeyboard(Z)V

    .line 572
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->isLanguageHindiorBengali(Landroid/content/Context;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mIsLanguageHindiorBengali:Z

    .line 573
    return-void

    .line 313
    .end local v3    # "resolution":I
    :cond_3
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, "fr"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 314
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->gps_btn:Landroid/widget/RelativeLayout;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 315
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->map_btn:Landroid/widget/RelativeLayout;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 318
    :cond_4
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->gps_btn:Landroid/widget/RelativeLayout;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 319
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->map_btn:Landroid/widget/RelativeLayout;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 455
    .restart local v3    # "resolution":I
    :cond_5
    const v4, 0xfa000

    if-ne v3, v4, :cond_6

    .line 456
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mSearchText:Landroid/widget/TextView;

    const-string v5, " "

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    .line 457
    :cond_6
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mSearchText:Landroid/widget/TextView;

    const v5, 0x7f0d0005

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setHint(I)V

    goto/16 :goto_1
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 938
    invoke-super {p0}, Landroid/app/ListActivity;->onDestroy()V

    .line 939
    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->setActivityVisibleState(Z)V

    .line 940
    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->setKeyboardVisible(Z)V

    .line 942
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 943
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 946
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->cleanResource()V

    .line 947
    return-void
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 9
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    const/16 v8, -0x3e7

    .line 1075
    invoke-super/range {p0 .. p5}, Landroid/app/ListActivity;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    .line 1077
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->cityListAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;

    invoke-virtual {v5, p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$MenuAddAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    .line 1078
    .local v1, "cityInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v3

    .line 1080
    .local v3, "location":Ljava/lang/String;
    iget-boolean v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->COLLAB_MODE:Z

    if-eqz v5, :cond_1

    .line 1081
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 1082
    .local v4, "result":Landroid/content/Intent;
    const-string v5, "CITY_NAME"

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getCity()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1083
    const-string v5, "CITY_STATE"

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getState()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1084
    const-string v5, "CITY_ID"

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1086
    const/4 v5, -0x1

    invoke-virtual {p0, v5, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->setResult(ILandroid/content/Intent;)V

    .line 1087
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->finish()V

    .line 1112
    .end local v4    # "result":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 1090
    :cond_1
    const v5, 0x7f080112

    invoke-virtual {p0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1091
    .local v0, "addRowState":Landroid/widget/TextView;
    const/4 v2, 0x0

    .line 1092
    .local v2, "isResultByGPS":Z
    if-eqz v0, :cond_2

    .line 1093
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0d0033

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1094
    const/4 v2, 0x1

    .line 1098
    :cond_2
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->ctx:Landroid/content/Context;

    invoke-static {v5, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->isRegisteredToCityList(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    if-nez v2, :cond_3

    .line 1099
    const v5, 0x7f0d0022

    invoke-static {p0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    goto :goto_0

    .line 1101
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getIntent()Landroid/content/Intent;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->intent:Landroid/content/Intent;

    .line 1102
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->intent:Landroid/content/Intent;

    const-string v6, "flags"

    invoke-virtual {v5, v6, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    const/16 v6, 0x7cf

    if-ne v5, v6, :cond_4

    .line 1103
    invoke-static {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateLastSelectedLocation(Landroid/content/Context;Ljava/lang/String;)I

    .line 1104
    invoke-direct {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getData(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;)V

    goto :goto_0

    .line 1105
    :cond_4
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->intent:Landroid/content/Intent;

    const-string v6, "flags"

    invoke-virtual {v5, v6, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    const/16 v6, 0x2edf

    if-ne v5, v6, :cond_5

    .line 1106
    invoke-direct {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getData(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;)V

    goto :goto_0

    .line 1107
    :cond_5
    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->intent:Landroid/content/Intent;

    const-string v6, "flags"

    invoke-virtual {v5, v6, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    const/16 v6, 0x6977

    if-ne v5, v6, :cond_0

    .line 1109
    invoke-direct {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getData(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 863
    invoke-super {p0}, Landroid/app/ListActivity;->onPause()V

    .line 864
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->stopHttpThread()V

    .line 865
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getDataHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 866
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getDataHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 869
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getLocDataHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 870
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getLocDataHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 873
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mapHandler:Landroid/os/Handler;

    if-eqz v0, :cond_2

    .line 874
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mapHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 877
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchHandler:Landroid/os/Handler;

    if-eqz v0, :cond_3

    .line 878
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 880
    :cond_3
    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->setActivityVisibleState(Z)V

    .line 882
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setSaveEnabled(Z)V

    .line 883
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->toastHint:Landroid/widget/Toast;

    if-eqz v0, :cond_4

    .line 884
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->toastHint:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 886
    :cond_4
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mPopupDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mPopupDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_5

    .line 887
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->hideAllDialog()V

    .line 890
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->hideLoadingDialog()V

    .line 892
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 893
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.widgetapp.ap.hero.accuweather.widget.action.APPWIDGET_UPDATE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->sendBroadcast(Landroid/content/Intent;)V

    .line 895
    :cond_6
    return-void
.end method

.method protected onResume()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 905
    invoke-super {p0}, Landroid/app/ListActivity;->onResume()V

    .line 907
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "LAUNCH_MODE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 908
    .local v0, "LAUNCH_MODE":Ljava/lang/String;
    if-eqz v0, :cond_2

    const-string v1, "COLLAB_MODE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 909
    iput-boolean v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->COLLAB_MODE:Z

    .line 913
    :goto_0
    iget-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->COLLAB_MODE:Z

    if-eqz v1, :cond_0

    .line 914
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->map_btn:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_0

    .line 915
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->map_btn:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 919
    :cond_0
    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->citydb:Lcom/sec/android/widgetapp/ap/hero/accuweather/db/CityDBHelper;

    .line 921
    invoke-virtual {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->setActivityVisibleState(Z)V

    .line 923
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-lez v1, :cond_3

    .line 924
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mClearTextBtn:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 928
    :goto_1
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->setContext(Landroid/content/Context;)V

    .line 930
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    if-eqz v1, :cond_1

    .line 931
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;->clearResource()V

    .line 932
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mGetMapInfoManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/GetMapInfoManager;

    .line 935
    :cond_1
    return-void

    .line 911
    :cond_2
    iput-boolean v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->COLLAB_MODE:Z

    goto :goto_0

    .line 926
    :cond_3
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mClearTextBtn:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method public onSearchRequested()Z
    .locals 2

    .prologue
    .line 576
    const-string v0, ""

    const-string v1, "MA_oSR"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 577
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->requestFocus()Z

    .line 578
    invoke-super {p0}, Landroid/app/ListActivity;->onSearchRequested()Z

    move-result v0

    return v0
.end method

.method protected onUserLeaveHint()V
    .locals 2

    .prologue
    .line 899
    invoke-super {p0}, Landroid/app/ListActivity;->onUserLeaveHint()V

    .line 900
    const-string v0, ""

    const-string v1, "Press Home key!!"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 901
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.widgetapp.ap.hero.accuweather.widget.action.APPWIDGET_UPDATE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->sendBroadcast(Landroid/content/Intent;)V

    .line 902
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 3
    .param p1, "hasFocus"    # Z

    .prologue
    .line 963
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onWindowFocusChanged(Z)V

    .line 965
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "oWFC : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v2}, Landroid/widget/AutoCompleteTextView;->isFocusable()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mIsKeyboardShown:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 967
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->isFocusable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mIsKeyboardShown:Z

    if-eqz v0, :cond_0

    .line 968
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->setKeyboardVisible(Z)V

    .line 970
    :cond_0
    return-void
.end method

.method public setActivityVisibleState(Z)V
    .locals 0
    .param p1, "onoff"    # Z

    .prologue
    .line 859
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mIsVisible:Z

    .line 860
    return-void
.end method

.method public showLocatingDialog()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1183
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 1184
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->disableEditField()V

    .line 1185
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mLoadingDialog:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 1186
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mRequestCanceled:Z

    .line 1187
    const/4 v0, 0x0

    const v1, 0x7f0d0038

    .line 1188
    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1187
    invoke-static {p0, v0, v1, v3, v3}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mLoadingDialog:Landroid/app/Dialog;

    .line 1190
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mLoadingDialog:Landroid/app/Dialog;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$27;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$27;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1208
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 1209
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 1210
    return-void
.end method
