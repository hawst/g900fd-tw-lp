.class Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$4;
.super Ljava/lang/Object;
.source "AdvancedHttpClient.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->callReceive(Landroid/content/Context;Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

.field final synthetic val$httpResponseHandler:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;

.field final synthetic val$msg:Landroid/os/Message;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;Landroid/os/Message;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    .prologue
    .line 221
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    iput-object p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$4;->val$httpResponseHandler:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;

    iput-object p3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$4;->val$msg:Landroid/os/Message;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v2, -0x1

    .line 223
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->stamp:J
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;)J

    move-result-wide v0

    const-wide/16 v3, -0x1

    cmp-long v0, v0, v3

    if-nez v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$4;->val$httpResponseHandler:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$4;->val$msg:Landroid/os/Message;

    iget v1, v1, Landroid/os/Message;->arg1:I

    const-string v3, "HTTP Error"

    const-string v4, ""

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;->onReceive(IILjava/lang/String;Ljava/lang/String;)V

    .line 229
    :goto_0
    return-void

    .line 226
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$4;->val$httpResponseHandler:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$4;->val$msg:Landroid/os/Message;

    iget v1, v1, Landroid/os/Message;->arg1:I

    const-string v3, "HTTP Error"

    const-string v4, ""

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient$4;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    .line 227
    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->stamp:J
    invoke-static {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;)J

    move-result-wide v5

    .line 226
    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;->onReceive(IILjava/lang/String;Ljava/lang/String;J)V

    goto :goto_0
.end method
