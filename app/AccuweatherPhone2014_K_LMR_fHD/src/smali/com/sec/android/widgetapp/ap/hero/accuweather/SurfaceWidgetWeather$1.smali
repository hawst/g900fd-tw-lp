.class Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$1;
.super Landroid/content/BroadcastReceiver;
.source "SurfaceWidgetWeather.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, -0x1

    const/4 v2, 0x1

    .line 61
    if-nez p2, :cond_1

    .line 62
    :try_start_0
    const-string v4, ""

    const-string v5, "==============action : intent = null"

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    :cond_0
    :goto_0
    return-void

    .line 65
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 66
    .local v0, "action":Ljava/lang/String;
    const-string v5, ""

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "action : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    const-string v5, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "android.intent.action.DATE_CHANGED"

    .line 68
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "clock.date_format_changed"

    .line 69
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "android.intent.action.TIME_SET"

    .line 70
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "android.intent.action.NETWORK_SET_TIMEZONE"

    .line 71
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 72
    :cond_2
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->doChangeDayOrNight()Z
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 73
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    const/4 v5, 0x1

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->updateClock(Z)V
    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 121
    .end local v0    # "action":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 122
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 75
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "action":Ljava/lang/String;
    :cond_3
    :try_start_1
    const-string v5, "com.sec.android.daemonapp.action.DAEMON_AUTOREFRESH_START"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 78
    const-string v5, "com.sec.android.widgetapp.ap.hero.accuweather.action.GET_CURRENT_LOCATION_OK"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    const-string v5, "com.sec.android.daemonapp.action.REMOTE_WIDGET_MANUAL_REFRESH_END"

    .line 79
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    const-string v5, "com.sec.android.daemonapp.action.DAEMON_AUTOREFRESH_END"

    .line 80
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 85
    :cond_4
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    const/4 v5, 0x1

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->updateView(Z)V
    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;Z)V

    goto :goto_0

    .line 86
    :cond_5
    const-string v5, "com.sec.android.widgetapp.ap.hero.accuweather.action.GET_CURRENT_LOCATION_START"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 87
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    const v5, 0x7f0d0038

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->showMsg(I)V
    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;I)V

    goto/16 :goto_0

    .line 88
    :cond_6
    const-string v5, "com.sec.android.widgetapp.ap.hero.accuweather.action.GET_CURRENT_LOCATION_ERROR"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 89
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    const v5, 0x7f0d0021

    const-wide/16 v6, 0x5dc

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->showMsg(IJ)V
    invoke-static {v4, v5, v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;IJ)V

    goto/16 :goto_0

    .line 90
    :cond_7
    const-string v5, "com.sec.android.widgetapp.ap.hero.accuweather.action.GET_DATA_NETWORK_ERROR"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 91
    const-string v4, "type"

    const/4 v5, 0x0

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 92
    .local v3, "type":I
    const-string v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "type : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    if-ne v3, v2, :cond_8

    .line 95
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    const v5, 0x7f0d001f

    const-wide/16 v6, 0x5dc

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->showMsg(IJ)V
    invoke-static {v4, v5, v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;IJ)V

    goto/16 :goto_0

    .line 97
    :cond_8
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    const v5, 0x7f0d0020

    const-wide/16 v6, 0x5dc

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->showMsg(IJ)V
    invoke-static {v4, v5, v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;IJ)V

    goto/16 :goto_0

    .line 99
    .end local v3    # "type":I
    :cond_9
    const-string v5, "com.sec.android.widgetapp.ap.hero.accuweather.action.DISABLE_LOCATION_SERVICES"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 100
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    const v5, 0x7f0d00ba

    const-wide/16 v6, 0x5dc

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->showMsg(IJ)V
    invoke-static {v4, v5, v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;IJ)V

    goto/16 :goto_0

    .line 101
    :cond_a
    const-string v5, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 102
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->stopRefreshIcon()V
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;)V

    goto/16 :goto_0

    .line 103
    :cond_b
    const-string v5, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 104
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->doChangeDayOrNight()Z
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 105
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    const/4 v5, 0x0

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->updateClock(Z)V
    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;Z)V

    goto/16 :goto_0

    .line 109
    :cond_c
    const-string v5, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "networkType"

    const/4 v6, -0x1

    .line 110
    invoke-virtual {p2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    if-nez v5, :cond_0

    .line 111
    invoke-static {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->handleConnectivityUpdate(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 113
    .local v2, "newRoaming":I
    :goto_1
    const-string v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "connectivity nR : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mR : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mRoaming:I
    invoke-static {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mRoaming:I
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;)I

    move-result v4

    if-eq v2, v4, :cond_0

    .line 116
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->mRoaming:I
    invoke-static {v4, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->access$602(Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;I)I

    .line 117
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;

    invoke-virtual {v4, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/SurfaceWidgetWeather;->startRefreshingNetworkChange(Landroid/content/Context;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .end local v2    # "newRoaming":I
    :cond_d
    move v2, v4

    .line 111
    goto :goto_1
.end method
