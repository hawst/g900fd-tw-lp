.class public abstract Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/Animation;
.super Ljava/lang/Object;
.source "Animation.java"


# instance fields
.field protected mListener:Landroid/animation/Animator$AnimatorListener;

.field protected mViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/Animation;->mListener:Landroid/animation/Animator$AnimatorListener;

    .line 12
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/Animation;->mViews:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public abstract animate()V
.end method

.method public abstract cancelAnimation()V
.end method

.method public abstract setDirection(I)V
.end method

.method public setOnListener(Landroid/animation/Animator$AnimatorListener;)V
    .locals 0
    .param p1, "l"    # Landroid/animation/Animator$AnimatorListener;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/Animation;->mListener:Landroid/animation/Animator$AnimatorListener;

    .line 33
    return-void
.end method

.method public setViews(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 41
    .local p1, "views":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/surface/view/animator/Animation;->mViews:Ljava/util/ArrayList;

    .line 42
    return-void
.end method
