.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;
.super Ljava/lang/Object;
.source "AccuWeatherJsonParser.java"


# instance fields
.field info:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

.field locitems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;",
            ">;"
        }
    .end annotation
.end field

.field lstResult:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;",
            ">;"
        }
    .end annotation
.end field

.field results:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->results:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    .line 31
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->info:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    .line 33
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->lstResult:Ljava/util/ArrayList;

    .line 35
    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->locitems:Ljava/util/ArrayList;

    return-void
.end method

.method private getName(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 2
    .param p1, "object"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 38
    const-string v1, "LocalizedName"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 39
    .local v0, "ret":Ljava/lang/String;
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 40
    const-string v1, "EnglishName"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 42
    :cond_0
    return-object v0
.end method


# virtual methods
.method getHourlyDateValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1037
    if-nez p1, :cond_0

    .line 1038
    const/4 v4, 0x0

    .line 1050
    :goto_0
    return-object v4

    .line 1040
    :cond_0
    const/4 v4, 0x0

    const/4 v5, 0x4

    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 1041
    .local v3, "year":Ljava/lang/String;
    const/4 v4, 0x5

    const/4 v5, 0x7

    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 1042
    .local v1, "month":Ljava/lang/String;
    const/16 v4, 0x8

    const/16 v5, 0xa

    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1044
    .local v0, "day":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 1046
    .local v2, "result":Ljava/lang/StringBuffer;
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1047
    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1048
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1050
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method getHourlyTimeValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1026
    if-nez p1, :cond_0

    .line 1027
    const/4 v2, 0x0

    .line 1031
    :goto_0
    return-object v2

    .line 1028
    :cond_0
    const/16 v2, 0xb

    const/16 v3, 0xd

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1029
    .local v0, "Time":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 1030
    .local v1, "result":Ljava/lang/StringBuffer;
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1031
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method getTimeValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 996
    if-nez p1, :cond_0

    .line 997
    const/4 v5, 0x0

    .line 1022
    :goto_0
    return-object v5

    .line 999
    :cond_0
    const/16 v5, 0xb

    const/16 v6, 0xd

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1000
    .local v1, "Time":Ljava/lang/String;
    const/16 v5, 0xe

    const/16 v6, 0x10

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1002
    .local v0, "Min":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 1004
    .local v3, "result":Ljava/lang/StringBuffer;
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    const/16 v6, 0xc

    if-le v5, v6, :cond_2

    .line 1005
    const-string v2, "PM"

    .line 1006
    .local v2, "ampm":Ljava/lang/String;
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v4, v5, -0xc

    .line 1007
    .local v4, "temp":I
    const/16 v5, 0xa

    if-ge v4, v5, :cond_1

    .line 1008
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "0"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1016
    .end local v4    # "temp":I
    :goto_1
    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1017
    const-string v5, ":"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1018
    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1019
    const-string v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1020
    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1022
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 1010
    .restart local v4    # "temp":I
    :cond_1
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 1013
    .end local v2    # "ampm":Ljava/lang/String;
    .end local v4    # "temp":I
    :cond_2
    const-string v2, "AM"

    .restart local v2    # "ampm":Ljava/lang/String;
    goto :goto_1
.end method

.method public parseAutoComplete(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 13
    .param p1, "responseBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 759
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 762
    .local v5, "localArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez p1, :cond_0

    .line 763
    :try_start_0
    const-string v9, ""

    const-string v10, "========>>> error !!!!"

    invoke-static {v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v5, v8

    .line 790
    .end local v5    # "localArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_0
    return-object v5

    .line 767
    .restart local v5    # "localArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_0
    new-instance v7, Lorg/json/JSONArray;

    invoke-direct {v7, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 769
    .local v7, "mMain":Lorg/json/JSONArray;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v4, v9, :cond_2

    .line 770
    invoke-virtual {v7, v4}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/json/JSONObject;

    .line 772
    .local v6, "mLocation":Lorg/json/JSONObject;
    const-string v9, "LocalizedName"

    invoke-virtual {v6, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 773
    .local v0, "LocalizedName":Ljava/lang/String;
    if-eqz v0, :cond_1

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 774
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 769
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 778
    .end local v0    # "LocalizedName":Ljava/lang/String;
    .end local v6    # "mLocation":Lorg/json/JSONObject;
    :cond_2
    const-string v9, ""

    const-string v10, "/// parseAutoComplete ///"

    invoke-static {v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 782
    .end local v4    # "i":I
    .end local v7    # "mMain":Lorg/json/JSONArray;
    :catch_0
    move-exception v3

    .line 784
    .local v3, "ex":Ljava/lang/Exception;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "parseAutoComplete() exception "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V

    .line 785
    invoke-virtual {v3}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    .line 786
    .local v2, "ele":[Ljava/lang/StackTraceElement;
    array-length v10, v2

    const/4 v9, 0x0

    :goto_2
    if-ge v9, v10, :cond_3

    aget-object v1, v2, v9

    .line 787
    .local v1, "e":Ljava/lang/StackTraceElement;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v1}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ""

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V

    .line 786
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 789
    .end local v1    # "e":Ljava/lang/StackTraceElement;
    :cond_3
    const-string v9, ""

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Exception : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v5, v8

    .line 790
    goto/16 :goto_0
.end method

.method public parseCityList(Ljava/lang/String;Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 27
    .param p1, "responseBody"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    if-nez p1, :cond_1

    .line 56
    :try_start_0
    const-string v23, ""

    const-string v24, "========>>> error !!!!"

    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    const/16 v21, 0x0

    .line 149
    :cond_0
    :goto_0
    return-object v21

    .line 59
    :cond_1
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 61
    .local v21, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;>;"
    new-instance v20, Lorg/json/JSONArray;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 62
    .local v20, "mMain":Lorg/json/JSONArray;
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_1
    invoke-virtual/range {v20 .. v20}, Lorg/json/JSONArray;->length()I

    move-result v23

    move/from16 v0, v23

    if-ge v12, v0, :cond_0

    .line 63
    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lorg/json/JSONObject;

    .line 64
    .local v19, "mLocation":Lorg/json/JSONObject;
    const-string v23, "Key"

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 65
    .local v14, "location":Ljava/lang/String;
    const-string v23, "LocalizedName"

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 66
    .local v3, "city":Ljava/lang/String;
    const-string v23, "Country"

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v17

    .line 67
    .local v17, "mCountry":Lorg/json/JSONObject;
    const-string v23, "AdministrativeArea"

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v16

    .line 69
    .local v16, "mAdministrativeArea":Lorg/json/JSONObject;
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->getName(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v2

    .line 70
    .local v2, "adminArea":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->getName(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v6

    .line 72
    .local v6, "countryArea":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isMEA()Z

    move-result v23

    if-eqz v23, :cond_9

    .line 73
    const-string v23, "ID"

    move-object/from16 v0, v16

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 74
    .local v4, "cityID":Ljava/lang/String;
    const-string v23, "ID"

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 75
    .local v7, "countryID":Ljava/lang/String;
    const-string v23, "213225"

    move-object/from16 v0, v23

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_2

    const-string v23, "IL"

    move-object/from16 v0, v23

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_3

    :cond_2
    const-string v23, "JM"

    .line 76
    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_6

    const-string v23, "IL"

    move-object/from16 v0, v23

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_6

    .line 77
    :cond_3
    if-eqz v2, :cond_4

    const-string v23, ""

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_5

    .line 78
    :cond_4
    const-string v23, ""

    const-string v24, "adminArea is null"

    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    const-string v22, ""

    .line 114
    .end local v4    # "cityID":Ljava/lang/String;
    .end local v7    # "countryID":Ljava/lang/String;
    .local v22, "state":Ljava/lang/String;
    :goto_2
    const-string v23, "GeoPosition"

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v18

    .line 115
    .local v18, "mGeoPosition":Lorg/json/JSONObject;
    const-string v23, "Latitude"

    move-object/from16 v0, v18

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 116
    .local v13, "latitude":Ljava/lang/String;
    const-string v23, "Longitude"

    move-object/from16 v0, v18

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 118
    .local v15, "longitude":Ljava/lang/String;
    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    invoke-direct {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;-><init>()V

    .line 119
    .local v5, "cityInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    invoke-virtual {v5, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setCity(Ljava/lang/String;)V

    .line 120
    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setState(Ljava/lang/String;)V

    .line 121
    invoke-virtual {v5, v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setLocation(Ljava/lang/String;)V

    .line 122
    invoke-virtual {v5, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setLatitude(Ljava/lang/String;)V

    .line 123
    invoke-virtual {v5, v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setLongitude(Ljava/lang/String;)V

    .line 125
    const-string v23, ""

    const-string v24, "//// parseCityList ///"

    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    const-string v23, ""

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "loc : "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 62
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_1

    .line 81
    .end local v5    # "cityInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .end local v13    # "latitude":Ljava/lang/String;
    .end local v15    # "longitude":Ljava/lang/String;
    .end local v18    # "mGeoPosition":Lorg/json/JSONObject;
    .end local v22    # "state":Ljava/lang/String;
    .restart local v4    # "cityID":Ljava/lang/String;
    .restart local v7    # "countryID":Ljava/lang/String;
    :cond_5
    move-object/from16 v22, v2

    .restart local v22    # "state":Ljava/lang/String;
    goto :goto_2

    .line 84
    .end local v22    # "state":Ljava/lang/String;
    :cond_6
    const-string v23, ""

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_7

    .line 85
    move-object/from16 v22, v6

    .restart local v22    # "state":Ljava/lang/String;
    goto :goto_2

    .line 87
    .end local v22    # "state":Ljava/lang/String;
    :cond_7
    invoke-static/range {p2 .. p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkLanguage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    .line 88
    .local v8, "cpLang":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    .line 89
    invoke-virtual/range {v23 .. v23}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v23

    move-object/from16 v0, v23

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    move-object/from16 v23, v0

    .line 88
    invoke-static/range {v23 .. v23}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v23

    const/16 v24, 0x1

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_8

    const-string v23, "he"

    .line 90
    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_8

    const-string v23, "en"

    .line 91
    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_8

    .line 92
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, "\u060c "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .restart local v22    # "state":Ljava/lang/String;
    goto/16 :goto_2

    .line 94
    .end local v22    # "state":Ljava/lang/String;
    :cond_8
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ", "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .restart local v22    # "state":Ljava/lang/String;
    goto/16 :goto_2

    .line 99
    .end local v4    # "cityID":Ljava/lang/String;
    .end local v7    # "countryID":Ljava/lang/String;
    .end local v8    # "cpLang":Ljava/lang/String;
    .end local v22    # "state":Ljava/lang/String;
    :cond_9
    const-string v23, ""

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_a

    .line 100
    move-object/from16 v22, v6

    .restart local v22    # "state":Ljava/lang/String;
    goto/16 :goto_2

    .line 102
    .end local v22    # "state":Ljava/lang/String;
    :cond_a
    invoke-static/range {p2 .. p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkLanguage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    .line 103
    .restart local v8    # "cpLang":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    .line 104
    invoke-virtual/range {v23 .. v23}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v23

    move-object/from16 v0, v23

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    move-object/from16 v23, v0

    .line 103
    invoke-static/range {v23 .. v23}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v23

    const/16 v24, 0x1

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_b

    const-string v23, "he"

    .line 105
    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_b

    const-string v23, "en"

    .line 106
    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_b

    .line 107
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, "\u060c "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .restart local v22    # "state":Ljava/lang/String;
    goto/16 :goto_2

    .line 109
    .end local v22    # "state":Ljava/lang/String;
    :cond_b
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ", "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v22

    .restart local v22    # "state":Ljava/lang/String;
    goto/16 :goto_2

    .line 141
    .end local v2    # "adminArea":Ljava/lang/String;
    .end local v3    # "city":Ljava/lang/String;
    .end local v6    # "countryArea":Ljava/lang/String;
    .end local v8    # "cpLang":Ljava/lang/String;
    .end local v12    # "i":I
    .end local v14    # "location":Ljava/lang/String;
    .end local v16    # "mAdministrativeArea":Lorg/json/JSONObject;
    .end local v17    # "mCountry":Lorg/json/JSONObject;
    .end local v19    # "mLocation":Lorg/json/JSONObject;
    .end local v20    # "mMain":Lorg/json/JSONArray;
    .end local v21    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;>;"
    .end local v22    # "state":Ljava/lang/String;
    :catch_0
    move-exception v11

    .line 143
    .local v11, "ex":Ljava/lang/Exception;
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "parseCityList() exception "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v11}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V

    .line 144
    invoke-virtual {v11}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v10

    .line 145
    .local v10, "ele":[Ljava/lang/StackTraceElement;
    array-length v0, v10

    move/from16 v24, v0

    const/16 v23, 0x0

    :goto_3
    move/from16 v0, v23

    move/from16 v1, v24

    if-ge v0, v1, :cond_c

    aget-object v9, v10, v23

    .line 146
    .local v9, "e":Ljava/lang/StackTraceElement;
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual {v9}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, ""

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v25 .. v25}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V

    .line 145
    add-int/lit8 v23, v23, 0x1

    goto :goto_3

    .line 148
    .end local v9    # "e":Ljava/lang/StackTraceElement;
    :cond_c
    const-string v23, ""

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "Exception : "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    const/16 v21, 0x0

    goto/16 :goto_0
.end method

.method public parseDetailWeather(ILjava/lang/String;Ljava/lang/String;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    .locals 104
    .param p1, "tempScale"    # I
    .param p2, "responseBody"    # Ljava/lang/String;
    .param p3, "updateDate"    # Ljava/lang/String;

    .prologue
    .line 219
    if-nez p2, :cond_0

    .line 220
    :try_start_0
    const-string v4, ""

    const-string v5, "========>>> error !!!!"

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    const/4 v4, 0x0

    .line 563
    :goto_0
    return-object v4

    .line 224
    :cond_0
    new-instance v60, Lorg/json/JSONObject;

    move-object/from16 v0, v60

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 225
    .local v60, "mMain":Lorg/json/JSONObject;
    const-string v4, "Location"

    move-object/from16 v0, v60

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "TimeZone"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v61

    .line 226
    .local v61, "mTimeZone":Lorg/json/JSONObject;
    const-string v4, "GmtOffset"

    move-object/from16 v0, v61

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 229
    .local v14, "mGmtOffset":Ljava/lang/String;
    new-instance v71, Ljava/lang/StringBuffer;

    invoke-direct/range {v71 .. v71}, Ljava/lang/StringBuffer;-><init>()V

    .line 230
    .local v71, "timeZoneStr":Ljava/lang/StringBuffer;
    invoke-virtual {v14}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v45

    .local v45, "dotIndex":I
    const/16 v62, 0x0

    .line 231
    .local v62, "min":I
    invoke-virtual {v14}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 232
    const-string v4, "GMT"

    move-object/from16 v0, v71

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 234
    invoke-virtual {v14}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move/from16 v0, v45

    invoke-virtual {v4, v5, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v71

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 235
    const/high16 v4, 0x42700000    # 60.0f

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "0"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v14}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v6

    move/from16 v0, v45

    invoke-virtual {v6, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    mul-float/2addr v4, v5

    float-to-int v0, v4

    move/from16 v62, v0

    .line 236
    if-lez v62, :cond_1

    .line 237
    const-string v4, ":"

    move-object/from16 v0, v71

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 238
    move-object/from16 v0, v71

    move/from16 v1, v62

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 257
    :cond_1
    :goto_1
    invoke-virtual/range {v71 .. v71}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v14

    .line 259
    const-string v4, "IsDaylightSaving"

    move-object/from16 v0, v61

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    .line 261
    .local v30, "mIsDaylightSaving":Ljava/lang/String;
    const-string v4, "CurrentConditions"

    move-object/from16 v0, v60

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v57

    .line 262
    .local v57, "mCurrentConditions":Lorg/json/JSONObject;
    const-string v4, "WeatherText"

    move-object/from16 v0, v57

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 263
    .local v16, "todayWeatherText":Ljava/lang/String;
    const-string v4, "WeatherIcon"

    move-object/from16 v0, v57

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v103

    .line 264
    .local v103, "todayWeatherIcon":Ljava/lang/String;
    const-string v4, "Temperature"

    move-object/from16 v0, v57

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Value"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v100

    .line 265
    .local v100, "todayTemperature":Ljava/lang/String;
    const-string v4, "RealFeelTemperature"

    move-object/from16 v0, v57

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Value"

    .line 266
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v98

    .line 267
    .local v98, "todayRealFeelTemperature":Ljava/lang/String;
    const-string v4, "MobileLink"

    move-object/from16 v0, v57

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 268
    .local v17, "todayUrl":Ljava/lang/String;
    const-string v4, "RelativeHumidity"

    move-object/from16 v0, v57

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v99

    .line 269
    .local v99, "todayRelativeHumidity":Ljava/lang/String;
    const-string v4, "UVIndex"

    move-object/from16 v0, v57

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v101

    .line 270
    .local v101, "todayUVIndex":I
    const-string v4, "UVIndexText"

    move-object/from16 v0, v57

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v102

    .line 272
    .local v102, "todayUVIndexText":Ljava/lang/String;
    const-string v4, "ForecastSummary"

    move-object/from16 v0, v60

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "DailyForecasts"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v58

    .line 273
    .local v58, "mDailyForecasts":Lorg/json/JSONArray;
    const/4 v4, 0x0

    move-object/from16 v0, v58

    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Temperature"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Minimum"

    .line 274
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Value"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v52

    .line 275
    .local v52, "forecastLowtemp":Ljava/lang/String;
    const/4 v4, 0x0

    move-object/from16 v0, v58

    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Temperature"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Maximum"

    .line 276
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Value"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v51

    .line 277
    .local v51, "forecastHightemp":Ljava/lang/String;
    const/4 v4, 0x0

    move-object/from16 v0, v58

    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Day"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Icon"

    .line 278
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v53

    .line 279
    .local v53, "forecastWeatherIcon":Ljava/lang/String;
    const/4 v4, 0x0

    move-object/from16 v0, v58

    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "MobileLink"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 281
    .local v8, "ForecastUrl":Ljava/lang/String;
    const/4 v4, 0x0

    move-object/from16 v0, v58

    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Sun"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v4

    const/4 v5, 0x1

    if-eq v4, v5, :cond_6

    .line 282
    const/4 v4, 0x0

    move-object/from16 v0, v58

    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Sun"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Rise"

    .line 283
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    const/4 v4, 0x0

    move-object/from16 v0, v58

    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Sun"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Set"

    .line 284
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 285
    :cond_2
    const-string v4, ""

    const-string v5, " ===>>>>> error not get Rise or Set"

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    const/16 v42, 0x0

    .line 287
    .local v42, "ForecastSunRiseTime":Ljava/lang/String;
    const/16 v43, 0x0

    .line 303
    .local v43, "ForecastSunSetTime":Ljava/lang/String;
    :goto_2
    const/4 v4, 0x0

    move-object/from16 v0, v58

    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Day"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Rain"

    .line 304
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Value"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v79

    .line 305
    .local v79, "todayDayRainAmount":D
    const/4 v4, 0x0

    move-object/from16 v0, v58

    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Day"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Snow"

    .line 306
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Value"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v82

    .line 307
    .local v82, "todayDaySnowAmount":D
    const/4 v4, 0x0

    move-object/from16 v0, v58

    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Day"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Ice"

    .line 308
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Value"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v72

    .line 309
    .local v72, "todayDayHailAmount":D
    const/4 v4, 0x0

    move-object/from16 v0, v58

    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Day"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "TotalLiquid"

    .line 310
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Value"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v76

    .line 312
    .local v76, "todayDayPrecifitationAmount":D
    const/4 v4, 0x0

    move-object/from16 v0, v58

    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Day"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "RainProbability"

    .line 313
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v81

    .line 314
    .local v81, "todayDayRainProb":I
    const/4 v4, 0x0

    move-object/from16 v0, v58

    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Day"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "SnowProbability"

    .line 315
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v84

    .line 316
    .local v84, "todayDaySnowProb":I
    const/4 v4, 0x0

    move-object/from16 v0, v58

    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Day"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "IceProbability"

    .line 317
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v74

    .line 318
    .local v74, "todayDayHailProb":I
    const/4 v4, 0x0

    move-object/from16 v0, v58

    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Day"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "PrecipitationProbability"

    .line 319
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v78

    .line 321
    .local v78, "todayDayPrecifitationProb":I
    const/4 v4, 0x0

    move-object/from16 v0, v58

    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Night"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Rain"

    .line 322
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Value"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v92

    .line 323
    .local v92, "todayNightRainAmount":D
    const/4 v4, 0x0

    move-object/from16 v0, v58

    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Night"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Snow"

    .line 324
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Value"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v95

    .line 325
    .local v95, "todayNightSnowAmount":D
    const/4 v4, 0x0

    move-object/from16 v0, v58

    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Night"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Ice"

    .line 326
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Value"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v85

    .line 327
    .local v85, "todayNightHailAmount":D
    const/4 v4, 0x0

    move-object/from16 v0, v58

    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Night"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "TotalLiquid"

    .line 328
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Value"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v89

    .line 330
    .local v89, "todayNightPrecifitationAmount":D
    const/4 v4, 0x0

    move-object/from16 v0, v58

    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Night"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "RainProbability"

    .line 331
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v94

    .line 332
    .local v94, "todayNightRainProb":I
    const/4 v4, 0x0

    move-object/from16 v0, v58

    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Night"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "SnowProbability"

    .line 333
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v97

    .line 334
    .local v97, "todayNightSnowProb":I
    const/4 v4, 0x0

    move-object/from16 v0, v58

    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Night"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "IceProbability"

    .line 335
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v87

    .line 336
    .local v87, "todayNightHailProb":I
    const/4 v4, 0x0

    move-object/from16 v0, v58

    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Night"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "PrecipitationProbability"

    .line 337
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v91

    .line 339
    .local v91, "todayNightPrecifitationProb":I
    const/4 v4, 0x0

    move-object/from16 v0, v58

    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Day"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Icon"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v75

    .line 340
    .local v75, "todayDayIcon":I
    const/4 v4, 0x0

    move-object/from16 v0, v58

    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Night"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Icon"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v88

    .line 341
    .local v88, "todayNightIcon":I
    const/4 v4, 0x1

    move-object/from16 v0, v58

    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Day"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Icon"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v63

    .line 342
    .local v63, "onedayDayIcon":I
    const/4 v4, 0x1

    move-object/from16 v0, v58

    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Night"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Icon"

    .line 343
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v64

    .line 345
    .local v64, "onedayNightIcon":I
    const-string v4, "True"

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    const-string v9, "1"

    .line 346
    .local v9, "strDaylightsaving":Ljava/lang/String;
    :goto_3
    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    .line 347
    invoke-static/range {v52 .. v52}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    invoke-static/range {v51 .. v51}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v6

    .line 348
    invoke-static/range {v53 .. v53}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    move/from16 v4, p1

    invoke-direct/range {v3 .. v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;-><init>(IFFILjava/lang/String;Ljava/lang/String;)V

    .line 351
    .local v3, "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    new-instance v10, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    .line 352
    invoke-static/range {v100 .. v100}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v13

    move/from16 v11, p1

    move-object v12, v3

    move-object v15, v9

    invoke-direct/range {v10 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;-><init>(ILcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;FLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    .local v10, "todayInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    invoke-static {v14}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v44

    .line 356
    .local v44, "cal":Ljava/util/Calendar;
    if-eqz v44, :cond_3

    .line 357
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object/from16 v0, v44

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 359
    :cond_3
    invoke-virtual {v10, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setSummerTime(Ljava/lang/String;)V

    .line 360
    invoke-static/range {v103 .. v103}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v10, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setIconNum(I)V

    .line 361
    move-object/from16 v0, p3

    invoke-virtual {v10, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setUpdateDate(Ljava/lang/String;)V

    .line 362
    invoke-virtual/range {v44 .. v44}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v10, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDate(Ljava/lang/String;)V

    .line 365
    if-eqz v42, :cond_8

    .line 366
    invoke-virtual/range {v42 .. v42}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v10, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setSunRiseTime(Ljava/lang/String;)V

    .line 370
    :goto_4
    if-eqz v43, :cond_9

    .line 371
    invoke-virtual/range {v43 .. v43}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v10, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setSunSetTime(Ljava/lang/String;)V

    .line 376
    :goto_5
    invoke-static/range {v98 .. v98}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v4

    invoke-virtual {v10, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setRealFeel(F)V

    .line 378
    move-object/from16 v0, v99

    invoke-virtual {v10, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setRelativeHumidity(Ljava/lang/String;)V

    .line 379
    move/from16 v0, v101

    invoke-virtual {v10, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setUVIndex(I)V

    .line 380
    move-object/from16 v0, v102

    invoke-virtual {v10, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setUVIndexText(Ljava/lang/String;)V

    .line 381
    move/from16 v0, v75

    invoke-virtual {v10, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDayIcon(I)V

    .line 382
    move/from16 v0, v88

    invoke-virtual {v10, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightIcon(I)V

    .line 383
    move/from16 v0, v63

    invoke-virtual {v10, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setOnedayDayIcon(I)V

    .line 384
    move/from16 v0, v64

    invoke-virtual {v10, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setOnedayNightIcon(I)V

    .line 387
    move-wide/from16 v0, v79

    invoke-virtual {v10, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDayRainAmount(D)V

    .line 388
    move-wide/from16 v0, v82

    invoke-virtual {v10, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDaySnowAmount(D)V

    .line 389
    move-wide/from16 v0, v72

    invoke-virtual {v10, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDayHailAmount(D)V

    .line 390
    move-wide/from16 v0, v76

    invoke-virtual {v10, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDayPrecipitationAmount(D)V

    .line 391
    move/from16 v0, v81

    invoke-virtual {v10, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDayRainProbability(I)V

    .line 392
    move/from16 v0, v84

    invoke-virtual {v10, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDaySnowProbability(I)V

    .line 393
    move/from16 v0, v74

    invoke-virtual {v10, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDayHailProbability(I)V

    .line 394
    move/from16 v0, v78

    invoke-virtual {v10, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setDayPrecipitationProbability(I)V

    .line 396
    move-wide/from16 v0, v92

    invoke-virtual {v10, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightRainAmount(D)V

    .line 397
    move-wide/from16 v0, v95

    invoke-virtual {v10, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightSnowAmount(D)V

    .line 398
    move-wide/from16 v0, v85

    invoke-virtual {v10, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightHailAmount(D)V

    .line 399
    move-wide/from16 v0, v89

    invoke-virtual {v10, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightPrecipitationAmount(D)V

    .line 400
    move/from16 v0, v94

    invoke-virtual {v10, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightRainProbability(I)V

    .line 401
    move/from16 v0, v97

    invoke-virtual {v10, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightSnowProbability(I)V

    .line 402
    move/from16 v0, v87

    invoke-virtual {v10, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightHailProbability(I)V

    .line 403
    move/from16 v0, v91

    invoke-virtual {v10, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setNightPrecipitationProbability(I)V

    .line 405
    new-instance v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    invoke-direct {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->info:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    .line 407
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->info:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    invoke-virtual {v4, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->setTodayWeatherInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;)V

    .line 409
    const-string v4, "Photos"

    move-object/from16 v0, v57

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v66

    .line 410
    .local v66, "photosArray":Lorg/json/JSONArray;
    invoke-virtual/range {v66 .. v66}, Lorg/json/JSONArray;->length()I

    move-result v67

    .line 412
    .local v67, "size":I
    const-string v4, "Location"

    move-object/from16 v0, v60

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Key"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 414
    .local v19, "cityId":Ljava/lang/String;
    const/16 v56, 0x0

    .local v56, "i":I
    :goto_6
    move/from16 v0, v56

    move/from16 v1, v67

    if-ge v0, v1, :cond_a

    .line 416
    move-object/from16 v0, v66

    move/from16 v1, v56

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v65

    .line 417
    .local v65, "photos":Lorg/json/JSONObject;
    const-string v4, "Source"

    move-object/from16 v0, v65

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 418
    .local v20, "source":Ljava/lang/String;
    const-string v4, "Description"

    move-object/from16 v0, v65

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 419
    .local v21, "description":Ljava/lang/String;
    const-string v4, "PortraitLink"

    move-object/from16 v0, v65

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 420
    .local v22, "portraitLink":Ljava/lang/String;
    const-string v4, "LandscapeLink"

    move-object/from16 v0, v65

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 422
    .local v23, "landscapeLink":Ljava/lang/String;
    new-instance v18, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;

    invoke-direct/range {v18 .. v23}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    .local v18, "photosInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->info:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addPhotosInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;)V

    .line 414
    add-int/lit8 v56, v56, 0x1

    goto :goto_6

    .line 245
    .end local v3    # "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .end local v8    # "ForecastUrl":Ljava/lang/String;
    .end local v9    # "strDaylightsaving":Ljava/lang/String;
    .end local v10    # "todayInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    .end local v16    # "todayWeatherText":Ljava/lang/String;
    .end local v17    # "todayUrl":Ljava/lang/String;
    .end local v18    # "photosInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/PhotosInfo;
    .end local v19    # "cityId":Ljava/lang/String;
    .end local v20    # "source":Ljava/lang/String;
    .end local v21    # "description":Ljava/lang/String;
    .end local v22    # "portraitLink":Ljava/lang/String;
    .end local v23    # "landscapeLink":Ljava/lang/String;
    .end local v30    # "mIsDaylightSaving":Ljava/lang/String;
    .end local v42    # "ForecastSunRiseTime":Ljava/lang/String;
    .end local v43    # "ForecastSunSetTime":Ljava/lang/String;
    .end local v44    # "cal":Ljava/util/Calendar;
    .end local v51    # "forecastHightemp":Ljava/lang/String;
    .end local v52    # "forecastLowtemp":Ljava/lang/String;
    .end local v53    # "forecastWeatherIcon":Ljava/lang/String;
    .end local v56    # "i":I
    .end local v57    # "mCurrentConditions":Lorg/json/JSONObject;
    .end local v58    # "mDailyForecasts":Lorg/json/JSONArray;
    .end local v63    # "onedayDayIcon":I
    .end local v64    # "onedayNightIcon":I
    .end local v65    # "photos":Lorg/json/JSONObject;
    .end local v66    # "photosArray":Lorg/json/JSONArray;
    .end local v67    # "size":I
    .end local v72    # "todayDayHailAmount":D
    .end local v74    # "todayDayHailProb":I
    .end local v75    # "todayDayIcon":I
    .end local v76    # "todayDayPrecifitationAmount":D
    .end local v78    # "todayDayPrecifitationProb":I
    .end local v79    # "todayDayRainAmount":D
    .end local v81    # "todayDayRainProb":I
    .end local v82    # "todayDaySnowAmount":D
    .end local v84    # "todayDaySnowProb":I
    .end local v85    # "todayNightHailAmount":D
    .end local v87    # "todayNightHailProb":I
    .end local v88    # "todayNightIcon":I
    .end local v89    # "todayNightPrecifitationAmount":D
    .end local v91    # "todayNightPrecifitationProb":I
    .end local v92    # "todayNightRainAmount":D
    .end local v94    # "todayNightRainProb":I
    .end local v95    # "todayNightSnowAmount":D
    .end local v97    # "todayNightSnowProb":I
    .end local v98    # "todayRealFeelTemperature":Ljava/lang/String;
    .end local v99    # "todayRelativeHumidity":Ljava/lang/String;
    .end local v100    # "todayTemperature":Ljava/lang/String;
    .end local v101    # "todayUVIndex":I
    .end local v102    # "todayUVIndexText":Ljava/lang/String;
    .end local v103    # "todayWeatherIcon":Ljava/lang/String;
    :cond_4
    const-string v4, "GMT+"

    move-object/from16 v0, v71

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 247
    invoke-virtual {v14}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move/from16 v0, v45

    invoke-virtual {v4, v5, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v71

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 248
    const/high16 v4, 0x42700000    # 60.0f

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "0"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v14}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v6

    move/from16 v0, v45

    invoke-virtual {v6, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    mul-float/2addr v4, v5

    float-to-int v0, v4

    move/from16 v62, v0

    .line 249
    if-lez v62, :cond_1

    .line 250
    const-string v4, ":"

    move-object/from16 v0, v71

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 251
    move-object/from16 v0, v71

    move/from16 v1, v62

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 555
    .end local v14    # "mGmtOffset":Ljava/lang/String;
    .end local v45    # "dotIndex":I
    .end local v60    # "mMain":Lorg/json/JSONObject;
    .end local v61    # "mTimeZone":Lorg/json/JSONObject;
    .end local v62    # "min":I
    .end local v71    # "timeZoneStr":Ljava/lang/StringBuffer;
    :catch_0
    move-exception v50

    .line 557
    .local v50, "ex":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "parseDetailWeather() exception "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v50 .. v50}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V

    .line 558
    invoke-virtual/range {v50 .. v50}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v47

    .line 559
    .local v47, "ele":[Ljava/lang/StackTraceElement;
    move-object/from16 v0, v47

    array-length v5, v0

    const/4 v4, 0x0

    :goto_7
    if-ge v4, v5, :cond_12

    aget-object v46, v47, v4

    .line 560
    .local v46, "e":Ljava/lang/StackTraceElement;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v46 .. v46}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v46 .. v46}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V

    .line 559
    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    .line 289
    .end local v46    # "e":Ljava/lang/StackTraceElement;
    .end local v47    # "ele":[Ljava/lang/StackTraceElement;
    .end local v50    # "ex":Ljava/lang/Exception;
    .restart local v8    # "ForecastUrl":Ljava/lang/String;
    .restart local v14    # "mGmtOffset":Ljava/lang/String;
    .restart local v16    # "todayWeatherText":Ljava/lang/String;
    .restart local v17    # "todayUrl":Ljava/lang/String;
    .restart local v30    # "mIsDaylightSaving":Ljava/lang/String;
    .restart local v45    # "dotIndex":I
    .restart local v51    # "forecastHightemp":Ljava/lang/String;
    .restart local v52    # "forecastLowtemp":Ljava/lang/String;
    .restart local v53    # "forecastWeatherIcon":Ljava/lang/String;
    .restart local v57    # "mCurrentConditions":Lorg/json/JSONObject;
    .restart local v58    # "mDailyForecasts":Lorg/json/JSONArray;
    .restart local v60    # "mMain":Lorg/json/JSONObject;
    .restart local v61    # "mTimeZone":Lorg/json/JSONObject;
    .restart local v62    # "min":I
    .restart local v71    # "timeZoneStr":Ljava/lang/StringBuffer;
    .restart local v98    # "todayRealFeelTemperature":Ljava/lang/String;
    .restart local v99    # "todayRelativeHumidity":Ljava/lang/String;
    .restart local v100    # "todayTemperature":Ljava/lang/String;
    .restart local v101    # "todayUVIndex":I
    .restart local v102    # "todayUVIndexText":Ljava/lang/String;
    .restart local v103    # "todayWeatherIcon":Ljava/lang/String;
    :cond_5
    const/4 v4, 0x0

    :try_start_1
    move-object/from16 v0, v58

    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Sun"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Rise"

    .line 290
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v40

    .line 291
    .local v40, "EpochRise":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->getTimeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    .line 292
    .restart local v42    # "ForecastSunRiseTime":Ljava/lang/String;
    const/4 v4, 0x0

    move-object/from16 v0, v58

    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Sun"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Set"

    .line 293
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v41

    .line 294
    .local v41, "EpochSet":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->getTimeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v43

    .line 295
    .restart local v43    # "ForecastSunSetTime":Ljava/lang/String;
    goto/16 :goto_2

    .line 298
    .end local v40    # "EpochRise":Ljava/lang/String;
    .end local v41    # "EpochSet":Ljava/lang/String;
    .end local v42    # "ForecastSunRiseTime":Ljava/lang/String;
    .end local v43    # "ForecastSunSetTime":Ljava/lang/String;
    :cond_6
    const-string v4, ""

    const-string v5, " ===>>>>> error not get Sun:"

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    const/16 v42, 0x0

    .line 300
    .restart local v42    # "ForecastSunRiseTime":Ljava/lang/String;
    const/16 v43, 0x0

    .restart local v43    # "ForecastSunSetTime":Ljava/lang/String;
    goto/16 :goto_2

    .line 345
    .restart local v63    # "onedayDayIcon":I
    .restart local v64    # "onedayNightIcon":I
    .restart local v72    # "todayDayHailAmount":D
    .restart local v74    # "todayDayHailProb":I
    .restart local v75    # "todayDayIcon":I
    .restart local v76    # "todayDayPrecifitationAmount":D
    .restart local v78    # "todayDayPrecifitationProb":I
    .restart local v79    # "todayDayRainAmount":D
    .restart local v81    # "todayDayRainProb":I
    .restart local v82    # "todayDaySnowAmount":D
    .restart local v84    # "todayDaySnowProb":I
    .restart local v85    # "todayNightHailAmount":D
    .restart local v87    # "todayNightHailProb":I
    .restart local v88    # "todayNightIcon":I
    .restart local v89    # "todayNightPrecifitationAmount":D
    .restart local v91    # "todayNightPrecifitationProb":I
    .restart local v92    # "todayNightRainAmount":D
    .restart local v94    # "todayNightRainProb":I
    .restart local v95    # "todayNightSnowAmount":D
    .restart local v97    # "todayNightSnowProb":I
    :cond_7
    const-string v9, "0"

    goto/16 :goto_3

    .line 368
    .restart local v3    # "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .restart local v9    # "strDaylightsaving":Ljava/lang/String;
    .restart local v10    # "todayInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    .restart local v44    # "cal":Ljava/util/Calendar;
    :cond_8
    const/4 v4, 0x0

    invoke-virtual {v10, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setSunRiseTime(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 373
    :cond_9
    const/4 v4, 0x0

    invoke-virtual {v10, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->setSunSetTime(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 427
    .restart local v19    # "cityId":Ljava/lang/String;
    .restart local v56    # "i":I
    .restart local v66    # "photosArray":Lorg/json/JSONArray;
    .restart local v67    # "size":I
    :cond_a
    const/16 v56, 0x1

    :goto_8
    const/4 v4, 0x7

    move/from16 v0, v56

    if-ge v0, v4, :cond_10

    .line 428
    const/16 v24, 0x0

    .line 429
    .local v24, "temp":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    const-string v4, "ForecastSummary"

    move-object/from16 v0, v60

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "DailyForecasts"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v58

    .line 431
    move-object/from16 v0, v58

    move/from16 v1, v56

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Temperature"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Minimum"

    .line 432
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Value"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v52

    .line 433
    move-object/from16 v0, v58

    move/from16 v1, v56

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Temperature"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Maximum"

    .line 434
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Value"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v51

    .line 435
    move-object/from16 v0, v58

    move/from16 v1, v56

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Day"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Icon"

    .line 436
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v53

    .line 437
    move-object/from16 v0, v58

    move/from16 v1, v56

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "MobileLink"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 439
    new-instance v24, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;

    .end local v24    # "temp":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    invoke-static/range {v52 .. v52}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v26

    .line 440
    invoke-static/range {v51 .. v51}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v27

    invoke-static/range {v53 .. v53}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v28

    move/from16 v25, p1

    move-object/from16 v29, v8

    invoke-direct/range {v24 .. v30}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;-><init>(IFFILjava/lang/String;Ljava/lang/String;)V

    .line 444
    .restart local v24    # "temp":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    move-object/from16 v0, v58

    move/from16 v1, v56

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Day"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Rain"

    .line 445
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Value"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v79

    .line 446
    move-object/from16 v0, v58

    move/from16 v1, v56

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Day"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Snow"

    .line 447
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Value"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v82

    .line 448
    move-object/from16 v0, v58

    move/from16 v1, v56

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Day"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Ice"

    .line 449
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Value"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v72

    .line 450
    move-object/from16 v0, v58

    move/from16 v1, v56

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Day"

    .line 451
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "TotalLiquid"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Value"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v76

    .line 453
    move-object/from16 v0, v58

    move/from16 v1, v56

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Day"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "RainProbability"

    .line 454
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v81

    .line 455
    move-object/from16 v0, v58

    move/from16 v1, v56

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Day"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "SnowProbability"

    .line 456
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v84

    .line 457
    move-object/from16 v0, v58

    move/from16 v1, v56

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Day"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "IceProbability"

    .line 458
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v74

    .line 459
    move-object/from16 v0, v58

    move/from16 v1, v56

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Day"

    .line 460
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "PrecipitationProbability"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v78

    .line 462
    move-object/from16 v0, v58

    move/from16 v1, v56

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Night"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Rain"

    .line 463
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Value"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v92

    .line 464
    move-object/from16 v0, v58

    move/from16 v1, v56

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Night"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Snow"

    .line 465
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Value"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v95

    .line 466
    move-object/from16 v0, v58

    move/from16 v1, v56

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Night"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Ice"

    .line 467
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Value"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v85

    .line 468
    move-object/from16 v0, v58

    move/from16 v1, v56

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Night"

    .line 469
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "TotalLiquid"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Value"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v89

    .line 471
    move-object/from16 v0, v58

    move/from16 v1, v56

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Night"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "RainProbability"

    .line 472
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v94

    .line 473
    move-object/from16 v0, v58

    move/from16 v1, v56

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Night"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "SnowProbability"

    .line 474
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v97

    .line 475
    move-object/from16 v0, v58

    move/from16 v1, v56

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Night"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "IceProbability"

    .line 476
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v87

    .line 477
    move-object/from16 v0, v58

    move/from16 v1, v56

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Night"

    .line 478
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "PrecipitationProbability"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v91

    .line 480
    move-object/from16 v0, v58

    move/from16 v1, v56

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Sun"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v4

    const/4 v5, 0x1

    if-eq v4, v5, :cond_d

    .line 481
    move-object/from16 v0, v58

    move/from16 v1, v56

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Sun"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Rise"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_b

    .line 482
    move-object/from16 v0, v58

    move/from16 v1, v56

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Sun"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Set"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 483
    :cond_b
    const-string v4, ""

    const-string v5, " ===>>>>>forecast error not get Rise or Set:"

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 484
    const/16 v42, 0x0

    .line 485
    const/16 v43, 0x0

    .line 498
    :goto_9
    move-object/from16 v0, v24

    move-wide/from16 v1, v79

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDayRainAmount(D)V

    .line 499
    move-object/from16 v0, v24

    move-wide/from16 v1, v82

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDaySnowAmount(D)V

    .line 500
    move-object/from16 v0, v24

    move-wide/from16 v1, v72

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDayHailAmount(D)V

    .line 501
    move-object/from16 v0, v24

    move-wide/from16 v1, v76

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDayPrecipitationAmount(D)V

    .line 502
    move-object/from16 v0, v24

    move/from16 v1, v81

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDayRainProbability(I)V

    .line 503
    move-object/from16 v0, v24

    move/from16 v1, v84

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDaySnowProbability(I)V

    .line 504
    move-object/from16 v0, v24

    move/from16 v1, v74

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDayHailProbability(I)V

    .line 505
    move-object/from16 v0, v24

    move/from16 v1, v78

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setDayPrecipitationProbability(I)V

    .line 507
    move-object/from16 v0, v24

    move-wide/from16 v1, v92

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightRainAmount(D)V

    .line 508
    move-object/from16 v0, v24

    move-wide/from16 v1, v95

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightSnowAmount(D)V

    .line 509
    move-object/from16 v0, v24

    move-wide/from16 v1, v85

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightHailAmount(D)V

    .line 510
    move-object/from16 v0, v24

    move-wide/from16 v1, v89

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightPrecipitationAmount(D)V

    .line 511
    move-object/from16 v0, v24

    move/from16 v1, v94

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightRainProbability(I)V

    .line 512
    move-object/from16 v0, v24

    move/from16 v1, v97

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightSnowProbability(I)V

    .line 513
    move-object/from16 v0, v24

    move/from16 v1, v87

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightHailProbability(I)V

    .line 514
    move-object/from16 v0, v24

    move/from16 v1, v91

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setNightPrecipitationProbability(I)V

    .line 516
    if-eqz v42, :cond_e

    .line 517
    invoke-virtual/range {v42 .. v42}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setSunRiseTime(Ljava/lang/String;)V

    .line 522
    :goto_a
    if-eqz v43, :cond_f

    .line 523
    invoke-virtual/range {v43 .. v43}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setSunSetTime(Ljava/lang/String;)V

    .line 529
    :goto_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->info:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addForecastInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;)V

    .line 427
    add-int/lit8 v56, v56, 0x1

    goto/16 :goto_8

    .line 487
    :cond_c
    move-object/from16 v0, v58

    move/from16 v1, v56

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Sun"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Rise"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    .line 488
    .local v48, "epochRise":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v48

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->getTimeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    .line 489
    move-object/from16 v0, v58

    move/from16 v1, v56

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Sun"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Set"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v49

    .line 490
    .local v49, "epochSet":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->getTimeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v43

    .line 491
    goto/16 :goto_9

    .line 493
    .end local v48    # "epochRise":Ljava/lang/String;
    .end local v49    # "epochSet":Ljava/lang/String;
    :cond_d
    const-string v4, ""

    const-string v5, " ===>>>>>forecast error not get Sun:"

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    const/16 v42, 0x0

    .line 495
    const/16 v43, 0x0

    goto/16 :goto_9

    .line 519
    :cond_e
    const/4 v4, 0x0

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setSunRiseTime(Ljava/lang/String;)V

    goto :goto_a

    .line 525
    :cond_f
    const/4 v4, 0x0

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;->setSunSetTime(Ljava/lang/String;)V

    goto :goto_b

    .line 531
    .end local v24    # "temp":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    :cond_10
    const/16 v56, 0x0

    :goto_c
    const/16 v4, 0xc

    move/from16 v0, v56

    if-ge v0, v4, :cond_11

    .line 533
    const-string v4, "ForecastSummary"

    move-object/from16 v0, v60

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "HourlyForecasts"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v59

    .line 536
    .local v59, "mHourlyForecasts":Lorg/json/JSONArray;
    move-object/from16 v0, v59

    move/from16 v1, v56

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Temperature"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Value"

    .line 537
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    long-to-float v0, v4

    move/from16 v33, v0

    .line 538
    .local v33, "hourlyTemp":F
    move-object/from16 v0, v59

    move/from16 v1, v56

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "WeatherIcon"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v37

    .line 539
    .local v37, "hourlyWeatherIcon":I
    move-object/from16 v0, v59

    move/from16 v1, v56

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "DateTime"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v68

    .line 540
    .local v68, "tempTime":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v68

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->getHourlyTimeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v55

    .line 541
    .local v55, "hourlyTime":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v68

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->getHourlyDateValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v54

    .line 542
    .local v54, "hourlyDate":Ljava/lang/String;
    move-object/from16 v0, v59

    move/from16 v1, v56

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Wind"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Speed"

    .line 543
    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "Value"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v69

    .line 544
    .local v69, "tempWindSpeed":D
    move-wide/from16 v0, v69

    double-to-int v0, v0

    move/from16 v36, v0

    .line 545
    .local v36, "hourlyWindSpeed":I
    move-object/from16 v0, v59

    move/from16 v1, v56

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "PrecipitationProbability"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v35

    .line 546
    .local v35, "hourlyPrecipitation":I
    new-instance v31, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;

    invoke-static/range {v55 .. v55}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v34

    .line 547
    invoke-static/range {v54 .. v54}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v39

    move/from16 v32, p1

    move-object/from16 v38, p3

    invoke-direct/range {v31 .. v39}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;-><init>(IFIIIILjava/lang/String;I)V

    .line 548
    .local v31, "hWeatherInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->info:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;

    move-object/from16 v0, v31

    invoke-virtual {v4, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;->addHourInfo(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;)V

    .line 531
    add-int/lit8 v56, v56, 0x1

    goto/16 :goto_c

    .line 551
    .end local v31    # "hWeatherInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/HourWeatherInfo;
    .end local v33    # "hourlyTemp":F
    .end local v35    # "hourlyPrecipitation":I
    .end local v36    # "hourlyWindSpeed":I
    .end local v37    # "hourlyWeatherIcon":I
    .end local v54    # "hourlyDate":Ljava/lang/String;
    .end local v55    # "hourlyTime":Ljava/lang/String;
    .end local v59    # "mHourlyForecasts":Lorg/json/JSONArray;
    .end local v68    # "tempTime":Ljava/lang/String;
    .end local v69    # "tempWindSpeed":D
    :cond_11
    const-string v4, ""

    const-string v5, "/// parseDetailWeather ///"

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 553
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->info:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/DetailWeatherInfo;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 562
    .end local v3    # "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/GeneralWeatherInfo;
    .end local v8    # "ForecastUrl":Ljava/lang/String;
    .end local v9    # "strDaylightsaving":Ljava/lang/String;
    .end local v10    # "todayInfo":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    .end local v14    # "mGmtOffset":Ljava/lang/String;
    .end local v16    # "todayWeatherText":Ljava/lang/String;
    .end local v17    # "todayUrl":Ljava/lang/String;
    .end local v19    # "cityId":Ljava/lang/String;
    .end local v30    # "mIsDaylightSaving":Ljava/lang/String;
    .end local v42    # "ForecastSunRiseTime":Ljava/lang/String;
    .end local v43    # "ForecastSunSetTime":Ljava/lang/String;
    .end local v44    # "cal":Ljava/util/Calendar;
    .end local v45    # "dotIndex":I
    .end local v51    # "forecastHightemp":Ljava/lang/String;
    .end local v52    # "forecastLowtemp":Ljava/lang/String;
    .end local v53    # "forecastWeatherIcon":Ljava/lang/String;
    .end local v56    # "i":I
    .end local v57    # "mCurrentConditions":Lorg/json/JSONObject;
    .end local v58    # "mDailyForecasts":Lorg/json/JSONArray;
    .end local v60    # "mMain":Lorg/json/JSONObject;
    .end local v61    # "mTimeZone":Lorg/json/JSONObject;
    .end local v62    # "min":I
    .end local v63    # "onedayDayIcon":I
    .end local v64    # "onedayNightIcon":I
    .end local v66    # "photosArray":Lorg/json/JSONArray;
    .end local v67    # "size":I
    .end local v71    # "timeZoneStr":Ljava/lang/StringBuffer;
    .end local v72    # "todayDayHailAmount":D
    .end local v74    # "todayDayHailProb":I
    .end local v75    # "todayDayIcon":I
    .end local v76    # "todayDayPrecifitationAmount":D
    .end local v78    # "todayDayPrecifitationProb":I
    .end local v79    # "todayDayRainAmount":D
    .end local v81    # "todayDayRainProb":I
    .end local v82    # "todayDaySnowAmount":D
    .end local v84    # "todayDaySnowProb":I
    .end local v85    # "todayNightHailAmount":D
    .end local v87    # "todayNightHailProb":I
    .end local v88    # "todayNightIcon":I
    .end local v89    # "todayNightPrecifitationAmount":D
    .end local v91    # "todayNightPrecifitationProb":I
    .end local v92    # "todayNightRainAmount":D
    .end local v94    # "todayNightRainProb":I
    .end local v95    # "todayNightSnowAmount":D
    .end local v97    # "todayNightSnowProb":I
    .end local v98    # "todayRealFeelTemperature":Ljava/lang/String;
    .end local v99    # "todayRelativeHumidity":Ljava/lang/String;
    .end local v100    # "todayTemperature":Ljava/lang/String;
    .end local v101    # "todayUVIndex":I
    .end local v102    # "todayUVIndexText":Ljava/lang/String;
    .end local v103    # "todayWeatherIcon":Ljava/lang/String;
    .restart local v47    # "ele":[Ljava/lang/StackTraceElement;
    .restart local v50    # "ex":Ljava/lang/Exception;
    :cond_12
    const-string v4, ""

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v50

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 563
    const/4 v4, 0x0

    goto/16 :goto_0
.end method

.method public parseDetailWeatherLocation(Ljava/lang/String;Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .locals 19
    .param p1, "responseBody"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 803
    if-nez p1, :cond_0

    .line 804
    :try_start_0
    const-string v15, ""

    const-string v16, "========>>> error !!!!"

    invoke-static/range {v15 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 805
    const/4 v15, 0x0

    .line 885
    :goto_0
    return-object v15

    .line 808
    :cond_0
    new-instance v13, Lorg/json/JSONObject;

    move-object/from16 v0, p1

    invoke-direct {v13, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 810
    .local v13, "mMain":Lorg/json/JSONObject;
    const-string v15, "Key"

    invoke-virtual {v13, v15}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 811
    .local v10, "location":Ljava/lang/String;
    const-string v15, "LocalizedName"

    invoke-virtual {v13, v15}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 812
    .local v2, "city":Ljava/lang/String;
    const-string v15, ""

    invoke-virtual {v2, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 813
    const-string v15, "EnglishName"

    invoke-virtual {v13, v15}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 815
    :cond_1
    const-string v15, "Country"

    invoke-virtual {v13, v15}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v12

    .line 816
    .local v12, "mCountry":Lorg/json/JSONObject;
    const-string v15, "AdministrativeArea"

    invoke-virtual {v13, v15}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v11

    .line 817
    .local v11, "mAdministrativeArea":Lorg/json/JSONObject;
    const-string v15, "LocalizedName"

    invoke-virtual {v11, v15}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 818
    .local v1, "adminArea":Ljava/lang/String;
    const-string v15, ""

    invoke-virtual {v1, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 819
    const-string v15, "EnglishName"

    invoke-virtual {v11, v15}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 821
    :cond_2
    const-string v15, "LocalizedName"

    invoke-virtual {v12, v15}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 822
    .local v4, "countryArea":Ljava/lang/String;
    const-string v15, ""

    invoke-virtual {v4, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 823
    const-string v15, "EnglishName"

    invoke-virtual {v12, v15}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 825
    :cond_3
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isMEA()Z

    move-result v15

    if-eqz v15, :cond_b

    .line 826
    const-string v15, "ID"

    invoke-virtual {v11, v15}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 827
    .local v3, "cityID":Ljava/lang/String;
    const-string v15, "ID"

    invoke-virtual {v12, v15}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 828
    .local v5, "countryID":Ljava/lang/String;
    const-string v15, "213225"

    invoke-virtual {v15, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    const-string v15, "IL"

    invoke-virtual {v15, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_5

    :cond_4
    const-string v15, "JM"

    .line 829
    invoke-virtual {v15, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    const-string v15, "IL"

    invoke-virtual {v15, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 830
    :cond_5
    if-eqz v1, :cond_6

    const-string v15, ""

    invoke-virtual {v1, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 831
    :cond_6
    const-string v15, ""

    const-string v16, "adminArea is null"

    invoke-static/range {v15 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 832
    const-string v14, ""

    .line 867
    .end local v3    # "cityID":Ljava/lang/String;
    .end local v5    # "countryID":Ljava/lang/String;
    .local v14, "state":Ljava/lang/String;
    :goto_1
    new-instance v15, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    invoke-direct {v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;-><init>()V

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->results:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    .line 868
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->results:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    invoke-virtual {v15, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setCity(Ljava/lang/String;)V

    .line 869
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->results:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    invoke-virtual {v15, v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setState(Ljava/lang/String;)V

    .line 870
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->results:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    invoke-virtual {v15, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setLocation(Ljava/lang/String;)V

    .line 872
    const-string v15, ""

    const-string v16, "/// parseDetailWeatherLocation ///"

    invoke-static/range {v15 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 873
    const-string v15, ""

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "loc :  "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 875
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->results:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    goto/16 :goto_0

    .line 834
    .end local v14    # "state":Ljava/lang/String;
    .restart local v3    # "cityID":Ljava/lang/String;
    .restart local v5    # "countryID":Ljava/lang/String;
    :cond_7
    move-object v14, v1

    .restart local v14    # "state":Ljava/lang/String;
    goto :goto_1

    .line 837
    .end local v14    # "state":Ljava/lang/String;
    :cond_8
    const-string v15, ""

    invoke-virtual {v1, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_9

    .line 838
    move-object v14, v4

    .restart local v14    # "state":Ljava/lang/String;
    goto :goto_1

    .line 840
    .end local v14    # "state":Ljava/lang/String;
    :cond_9
    invoke-static/range {p2 .. p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkLanguage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    .line 841
    .local v6, "cpLang":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    .line 842
    invoke-virtual {v15}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v15

    iget-object v15, v15, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 841
    invoke-static {v15}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v15

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_a

    const-string v15, "he"

    .line 843
    invoke-virtual {v6, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_a

    const-string v15, "en"

    .line 844
    invoke-virtual {v6, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_a

    .line 845
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\u060c "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .restart local v14    # "state":Ljava/lang/String;
    goto/16 :goto_1

    .line 847
    .end local v14    # "state":Ljava/lang/String;
    :cond_a
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .restart local v14    # "state":Ljava/lang/String;
    goto/16 :goto_1

    .line 852
    .end local v3    # "cityID":Ljava/lang/String;
    .end local v5    # "countryID":Ljava/lang/String;
    .end local v6    # "cpLang":Ljava/lang/String;
    .end local v14    # "state":Ljava/lang/String;
    :cond_b
    const-string v15, ""

    invoke-virtual {v1, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_c

    .line 853
    move-object v14, v4

    .restart local v14    # "state":Ljava/lang/String;
    goto/16 :goto_1

    .line 855
    .end local v14    # "state":Ljava/lang/String;
    :cond_c
    invoke-static/range {p2 .. p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkLanguage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    .line 856
    .restart local v6    # "cpLang":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    .line 857
    invoke-virtual {v15}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v15

    iget-object v15, v15, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 856
    invoke-static {v15}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v15

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_d

    const-string v15, "he"

    .line 858
    invoke-virtual {v6, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_d

    const-string v15, "en"

    .line 859
    invoke-virtual {v6, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_d

    .line 860
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\u060c "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .restart local v14    # "state":Ljava/lang/String;
    goto/16 :goto_1

    .line 862
    .end local v14    # "state":Ljava/lang/String;
    :cond_d
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v14

    .restart local v14    # "state":Ljava/lang/String;
    goto/16 :goto_1

    .line 877
    .end local v1    # "adminArea":Ljava/lang/String;
    .end local v2    # "city":Ljava/lang/String;
    .end local v4    # "countryArea":Ljava/lang/String;
    .end local v6    # "cpLang":Ljava/lang/String;
    .end local v10    # "location":Ljava/lang/String;
    .end local v11    # "mAdministrativeArea":Lorg/json/JSONObject;
    .end local v12    # "mCountry":Lorg/json/JSONObject;
    .end local v13    # "mMain":Lorg/json/JSONObject;
    .end local v14    # "state":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 879
    .local v9, "ex":Ljava/lang/Exception;
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "parseDetailWeatherLocation() exception "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v9}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V

    .line 880
    invoke-virtual {v9}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v8

    .line 881
    .local v8, "ele":[Ljava/lang/StackTraceElement;
    array-length v0, v8

    move/from16 v16, v0

    const/4 v15, 0x0

    :goto_2
    move/from16 v0, v16

    if-ge v15, v0, :cond_e

    aget-object v7, v8, v15

    .line 882
    .local v7, "e":Ljava/lang/StackTraceElement;
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v7}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, ""

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V

    .line 881
    add-int/lit8 v15, v15, 0x1

    goto :goto_2

    .line 884
    .end local v7    # "e":Ljava/lang/StackTraceElement;
    :cond_e
    const-string v15, ""

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Exception : "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 885
    const/4 v15, 0x0

    goto/16 :goto_0
.end method

.method public parseDetailWeatherLocationCity(Ljava/lang/String;Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;
    .locals 22
    .param p1, "responseBody"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 898
    if-nez p1, :cond_0

    .line 899
    :try_start_0
    const-string v18, ""

    const-string v19, "========>>> error !!!!"

    invoke-static/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 900
    const/16 v18, 0x0

    .line 988
    :goto_0
    return-object v18

    .line 903
    :cond_0
    new-instance v16, Lorg/json/JSONObject;

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 905
    .local v16, "mMain":Lorg/json/JSONObject;
    const-string v18, "Location"

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v18

    const-string v19, "Key"

    invoke-virtual/range {v18 .. v19}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 906
    .local v12, "location":Ljava/lang/String;
    const-string v18, "Location"

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v18

    const-string v19, "LocalizedName"

    invoke-virtual/range {v18 .. v19}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 907
    .local v3, "city":Ljava/lang/String;
    const-string v18, ""

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_1

    .line 908
    const-string v18, "Location"

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v18

    const-string v19, "EnglishName"

    invoke-virtual/range {v18 .. v19}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 910
    :cond_1
    const-string v18, "Location"

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v18

    const-string v19, "Country"

    invoke-virtual/range {v18 .. v19}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v15

    .line 911
    .local v15, "mCountry":Lorg/json/JSONObject;
    const-string v18, "Location"

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v18

    const-string v19, "AdministrativeArea"

    invoke-virtual/range {v18 .. v19}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v14

    .line 913
    .local v14, "mAdministrativeArea":Lorg/json/JSONObject;
    const-string v18, "LocalizedName"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 914
    .local v2, "adminArea":Ljava/lang/String;
    const-string v18, ""

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_2

    .line 915
    const-string v18, "EnglishName"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 917
    :cond_2
    const-string v18, "LocalizedName"

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 918
    .local v5, "countryArea":Ljava/lang/String;
    const-string v18, ""

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 919
    const-string v18, "EnglishName"

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 921
    :cond_3
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isMEA()Z

    move-result v18

    if-eqz v18, :cond_b

    .line 922
    const-string v18, "ID"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 923
    .local v4, "cityID":Ljava/lang/String;
    const-string v18, "ID"

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 924
    .local v6, "countryID":Ljava/lang/String;
    const-string v18, "213225"

    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_4

    const-string v18, "IL"

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_5

    :cond_4
    const-string v18, "JM"

    .line 925
    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_8

    const-string v18, "IL"

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_8

    .line 926
    :cond_5
    if-eqz v2, :cond_6

    const-string v18, ""

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_7

    .line 927
    :cond_6
    const-string v18, ""

    const-string v19, "adminArea is null"

    invoke-static/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 928
    const-string v17, ""

    .line 963
    .end local v4    # "cityID":Ljava/lang/String;
    .end local v6    # "countryID":Ljava/lang/String;
    .local v17, "state":Ljava/lang/String;
    :goto_1
    const-string v18, "Location"

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v18

    const-string v19, "GeoPosition"

    invoke-virtual/range {v18 .. v19}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v18

    const-string v19, "Latitude"

    .line 964
    invoke-virtual/range {v18 .. v19}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 965
    .local v11, "latitude":Ljava/lang/String;
    const-string v18, "Location"

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v18

    const-string v19, "GeoPosition"

    invoke-virtual/range {v18 .. v19}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v18

    const-string v19, "Longitude"

    .line 966
    invoke-virtual/range {v18 .. v19}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 968
    .local v13, "longitude":Ljava/lang/String;
    new-instance v18, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    invoke-direct/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->results:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    .line 969
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->results:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setCity(Ljava/lang/String;)V

    .line 970
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->results:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setState(Ljava/lang/String;)V

    .line 971
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->results:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setLocation(Ljava/lang/String;)V

    .line 972
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->results:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setLatitude(Ljava/lang/String;)V

    .line 973
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->results:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->setLongitude(Ljava/lang/String;)V

    .line 975
    const-string v18, ""

    const-string v19, "/// parseDetailWeatherLocationCity ///"

    invoke-static/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 976
    const-string v18, ""

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "loc :  "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 978
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;->results:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    move-object/from16 v18, v0

    goto/16 :goto_0

    .line 930
    .end local v11    # "latitude":Ljava/lang/String;
    .end local v13    # "longitude":Ljava/lang/String;
    .end local v17    # "state":Ljava/lang/String;
    .restart local v4    # "cityID":Ljava/lang/String;
    .restart local v6    # "countryID":Ljava/lang/String;
    :cond_7
    move-object/from16 v17, v2

    .restart local v17    # "state":Ljava/lang/String;
    goto/16 :goto_1

    .line 933
    .end local v17    # "state":Ljava/lang/String;
    :cond_8
    const-string v18, ""

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_9

    .line 934
    move-object/from16 v17, v5

    .restart local v17    # "state":Ljava/lang/String;
    goto/16 :goto_1

    .line 936
    .end local v17    # "state":Ljava/lang/String;
    :cond_9
    invoke-static/range {p2 .. p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkLanguage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    .line 937
    .local v7, "cpLang":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    .line 938
    invoke-virtual/range {v18 .. v18}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v18

    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    move-object/from16 v18, v0

    .line 937
    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_a

    const-string v18, "he"

    .line 939
    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_a

    const-string v18, "en"

    .line 940
    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_a

    .line 941
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "\u060c "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .restart local v17    # "state":Ljava/lang/String;
    goto/16 :goto_1

    .line 943
    .end local v17    # "state":Ljava/lang/String;
    :cond_a
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .restart local v17    # "state":Ljava/lang/String;
    goto/16 :goto_1

    .line 948
    .end local v4    # "cityID":Ljava/lang/String;
    .end local v6    # "countryID":Ljava/lang/String;
    .end local v7    # "cpLang":Ljava/lang/String;
    .end local v17    # "state":Ljava/lang/String;
    :cond_b
    const-string v18, ""

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_c

    .line 949
    move-object/from16 v17, v5

    .restart local v17    # "state":Ljava/lang/String;
    goto/16 :goto_1

    .line 951
    .end local v17    # "state":Ljava/lang/String;
    :cond_c
    invoke-static/range {p2 .. p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkLanguage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    .line 952
    .restart local v7    # "cpLang":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    .line 953
    invoke-virtual/range {v18 .. v18}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v18

    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    move-object/from16 v18, v0

    .line 952
    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_d

    const-string v18, "he"

    .line 954
    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_d

    const-string v18, "en"

    .line 955
    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_d

    .line 956
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "\u060c "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .restart local v17    # "state":Ljava/lang/String;
    goto/16 :goto_1

    .line 958
    .end local v17    # "state":Ljava/lang/String;
    :cond_d
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v17

    .restart local v17    # "state":Ljava/lang/String;
    goto/16 :goto_1

    .line 980
    .end local v2    # "adminArea":Ljava/lang/String;
    .end local v3    # "city":Ljava/lang/String;
    .end local v5    # "countryArea":Ljava/lang/String;
    .end local v7    # "cpLang":Ljava/lang/String;
    .end local v12    # "location":Ljava/lang/String;
    .end local v14    # "mAdministrativeArea":Lorg/json/JSONObject;
    .end local v15    # "mCountry":Lorg/json/JSONObject;
    .end local v16    # "mMain":Lorg/json/JSONObject;
    .end local v17    # "state":Ljava/lang/String;
    :catch_0
    move-exception v10

    .line 982
    .local v10, "ex":Ljava/lang/Exception;
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "parseDetailWeatherLocation() exception "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v10}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V

    .line 983
    invoke-virtual {v10}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v9

    .line 984
    .local v9, "ele":[Ljava/lang/StackTraceElement;
    array-length v0, v9

    move/from16 v19, v0

    const/16 v18, 0x0

    :goto_2
    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_e

    aget-object v8, v9, v18

    .line 985
    .local v8, "e":Ljava/lang/StackTraceElement;
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v8}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ""

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V

    .line 984
    add-int/lit8 v18, v18, 0x1

    goto :goto_2

    .line 987
    .end local v8    # "e":Ljava/lang/StackTraceElement;
    :cond_e
    const-string v18, ""

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Exception : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 988
    const/16 v18, 0x0

    goto/16 :goto_0
.end method

.method public parseDetailWeather_LocCities(ILjava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 24
    .param p1, "tempScale"    # I
    .param p2, "updateDate"    # Ljava/lang/String;
    .param p3, "responseBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 692
    if-nez p3, :cond_0

    .line 693
    :try_start_0
    const-string v20, ""

    const-string v21, "========>>> error !!!!"

    invoke-static/range {v20 .. v21}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 694
    const/4 v15, 0x0

    .line 753
    :goto_0
    return-object v15

    .line 697
    :cond_0
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 699
    .local v15, "locitems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;>;"
    new-instance v18, Lorg/json/JSONArray;

    move-object/from16 v0, v18

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 700
    .local v18, "mMain":Lorg/json/JSONArray;
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    invoke-virtual/range {v18 .. v18}, Lorg/json/JSONArray;->length()I

    move-result v20

    move/from16 v0, v20

    if-ge v10, v0, :cond_2

    .line 702
    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lorg/json/JSONObject;

    .line 704
    .local v17, "mLocation":Lorg/json/JSONObject;
    new-instance v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    invoke-direct {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;-><init>()V

    .line 706
    .local v4, "ci":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    const-string v20, "Key"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 707
    .local v14, "location":Ljava/lang/String;
    const-string v20, "LocalizedName"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 708
    .local v5, "city":Ljava/lang/String;
    const-string v20, "Country"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v20

    const-string v21, "LocalizedName"

    invoke-virtual/range {v20 .. v21}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 709
    .local v19, "state":Ljava/lang/String;
    const-string v20, "GeoPosition"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v20

    const-string v21, "Latitude"

    invoke-virtual/range {v20 .. v21}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 710
    .local v13, "latitude":Ljava/lang/String;
    const-string v20, "GeoPosition"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v20

    const-string v21, "Longitude"

    invoke-virtual/range {v20 .. v21}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 711
    .local v16, "longitude":Ljava/lang/String;
    const-string v20, "IsDayTime"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 712
    .local v12, "isDayTime":Ljava/lang/String;
    const-string v20, "WeatherIcon"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 713
    .local v11, "icon":Ljava/lang/String;
    if-nez p1, :cond_1

    .line 714
    const-string v20, "Temperature"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v20

    const-string v21, "Imperial"

    invoke-virtual/range {v20 .. v21}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v20

    const-string v21, "Value"

    .line 715
    invoke-virtual/range {v20 .. v21}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 720
    .local v3, "Temperature":Ljava/lang/String;
    :goto_2
    invoke-virtual {v4, v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->setLocation(Ljava/lang/String;)V

    .line 721
    invoke-virtual {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->setCity(Ljava/lang/String;)V

    .line 722
    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->setState(Ljava/lang/String;)V

    .line 723
    invoke-virtual {v4, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->setLatitude(Ljava/lang/String;)V

    .line 724
    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->setLongitude(Ljava/lang/String;)V

    .line 725
    invoke-virtual {v4, v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->setIcon(Ljava/lang/String;)V

    .line 726
    invoke-virtual {v4, v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->setIsDayTime(Ljava/lang/String;)V

    .line 729
    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v20

    .line 728
    move/from16 v0, p1

    move/from16 v1, p1

    move/from16 v2, v20

    invoke-static {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v6

    .line 731
    .local v6, "currentTemp":I
    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->setTemp(Ljava/lang/String;)V

    .line 740
    invoke-virtual {v15, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 700
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_1

    .line 717
    .end local v3    # "Temperature":Ljava/lang/String;
    .end local v6    # "currentTemp":I
    :cond_1
    const-string v20, "Temperature"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v20

    const-string v21, "Metric"

    invoke-virtual/range {v20 .. v21}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v20

    const-string v21, "Value"

    .line 718
    invoke-virtual/range {v20 .. v21}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "Temperature":Ljava/lang/String;
    goto :goto_2

    .line 743
    .end local v3    # "Temperature":Ljava/lang/String;
    .end local v4    # "ci":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    .end local v5    # "city":Ljava/lang/String;
    .end local v11    # "icon":Ljava/lang/String;
    .end local v12    # "isDayTime":Ljava/lang/String;
    .end local v13    # "latitude":Ljava/lang/String;
    .end local v14    # "location":Ljava/lang/String;
    .end local v16    # "longitude":Ljava/lang/String;
    .end local v17    # "mLocation":Lorg/json/JSONObject;
    .end local v19    # "state":Ljava/lang/String;
    :cond_2
    const-string v20, ""

    const-string v21, "/// parseDetailWeather_LocCities ///"

    invoke-static/range {v20 .. v21}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 745
    .end local v10    # "i":I
    .end local v15    # "locitems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;>;"
    .end local v18    # "mMain":Lorg/json/JSONArray;
    :catch_0
    move-exception v9

    .line 746
    .local v9, "ex":Ljava/lang/Exception;
    const-string v20, ""

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "parseDetailWeather_LocCities() exception "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v9}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 747
    invoke-virtual {v9}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v8

    .line 748
    .local v8, "ele":[Ljava/lang/StackTraceElement;
    array-length v0, v8

    move/from16 v21, v0

    const/16 v20, 0x0

    :goto_3
    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_3

    aget-object v7, v8, v20

    .line 749
    .local v7, "e":Ljava/lang/StackTraceElement;
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v7}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ""

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V

    .line 748
    add-int/lit8 v20, v20, 0x1

    goto :goto_3

    .line 751
    .end local v7    # "e":Ljava/lang/StackTraceElement;
    :cond_3
    const/4 v15, 0x0

    goto/16 :goto_0
.end method

.method public parseDetailWeather_SearchMapCity(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 27
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "tempScale"    # I
    .param p3, "updateDate"    # Ljava/lang/String;
    .param p4, "responseBody"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 579
    if-nez p4, :cond_0

    .line 580
    :try_start_0
    const-string v23, ""

    const-string v24, "========>>> error !!!!"

    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 581
    const/16 v17, 0x0

    .line 679
    :goto_0
    return-object v17

    .line 584
    :cond_0
    new-instance v14, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    invoke-direct {v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;-><init>()V

    .line 585
    .local v14, "item":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 586
    .local v17, "locitems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;>;"
    new-instance v21, Lorg/json/JSONObject;

    move-object/from16 v0, v21

    move-object/from16 v1, p4

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 587
    .local v21, "mMain":Lorg/json/JSONObject;
    const-string v23, "Location"

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v23

    const-string v24, "Key"

    invoke-virtual/range {v23 .. v24}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 588
    .local v16, "location":Ljava/lang/String;
    const-string v23, "Location"

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v23

    const-string v24, "LocalizedName"

    invoke-virtual/range {v23 .. v24}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 590
    .local v5, "city":Ljava/lang/String;
    const-string v23, "Location"

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v23

    const-string v24, "Country"

    invoke-virtual/range {v23 .. v24}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v20

    .line 591
    .local v20, "mCountry":Lorg/json/JSONObject;
    const-string v23, "Location"

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v23

    const-string v24, "AdministrativeArea"

    invoke-virtual/range {v23 .. v24}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v19

    .line 594
    .local v19, "mAdministrativeArea":Lorg/json/JSONObject;
    invoke-static {}, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isMEA()Z

    move-result v23

    if-eqz v23, :cond_9

    .line 595
    const-string v23, "ID"

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 596
    .local v6, "cityID":Ljava/lang/String;
    const-string v23, "ID"

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 597
    .local v7, "countryID":Ljava/lang/String;
    const-string v23, "213225"

    move-object/from16 v0, v23

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_1

    const-string v23, "IL"

    move-object/from16 v0, v23

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_2

    :cond_1
    const-string v23, "JM"

    .line 598
    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_5

    const-string v23, "IL"

    move-object/from16 v0, v23

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_5

    .line 599
    :cond_2
    const-string v23, "LocalizedName"

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 600
    .local v4, "adminArea":Ljava/lang/String;
    if-eqz v4, :cond_3

    const-string v23, ""

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_4

    .line 601
    :cond_3
    const-string v23, ""

    const-string v24, "adminArea is null"

    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 602
    const-string v22, ""

    .line 644
    .end local v4    # "adminArea":Ljava/lang/String;
    .end local v6    # "cityID":Ljava/lang/String;
    .end local v7    # "countryID":Ljava/lang/String;
    .local v22, "state":Ljava/lang/String;
    :goto_1
    const-string v23, "Location"

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v23

    const-string v24, "GeoPosition"

    invoke-virtual/range {v23 .. v24}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v23

    const-string v24, "Latitude"

    .line 645
    invoke-virtual/range {v23 .. v24}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 646
    .local v15, "latitude":Ljava/lang/String;
    const-string v23, "Location"

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v23

    const-string v24, "GeoPosition"

    invoke-virtual/range {v23 .. v24}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v23

    const-string v24, "Longitude"

    .line 647
    invoke-virtual/range {v23 .. v24}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 648
    .local v18, "longitude":Ljava/lang/String;
    const-string v23, "CurrentConditions"

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v23

    const-string v24, "WeatherIcon"

    invoke-virtual/range {v23 .. v24}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 649
    .local v13, "icon":Ljava/lang/String;
    if-nez p2, :cond_d

    .line 650
    const-string v23, "CurrentConditions"

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v23

    const-string v24, "Temperature"

    invoke-virtual/range {v23 .. v24}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v23

    const-string v24, "Value"

    .line 651
    invoke-virtual/range {v23 .. v24}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 656
    .local v3, "Temperature":Ljava/lang/String;
    :goto_2
    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->setLocation(Ljava/lang/String;)V

    .line 657
    invoke-virtual {v14, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->setCity(Ljava/lang/String;)V

    .line 658
    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->setState(Ljava/lang/String;)V

    .line 659
    invoke-virtual {v14, v15}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->setLatitude(Ljava/lang/String;)V

    .line 660
    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->setLongitude(Ljava/lang/String;)V

    .line 661
    invoke-virtual {v14, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->setIcon(Ljava/lang/String;)V

    .line 663
    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v23

    move/from16 v0, p2

    move/from16 v1, p2

    move/from16 v2, v23

    invoke-static {v0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v9

    .line 665
    .local v9, "currentTemp":I
    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v14, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->setTemp(Ljava/lang/String;)V

    .line 667
    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 669
    const-string v23, ""

    const-string v24, "/// parseDetailWeather_SearchMapCity ///"

    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 671
    .end local v3    # "Temperature":Ljava/lang/String;
    .end local v5    # "city":Ljava/lang/String;
    .end local v9    # "currentTemp":I
    .end local v13    # "icon":Ljava/lang/String;
    .end local v14    # "item":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    .end local v15    # "latitude":Ljava/lang/String;
    .end local v16    # "location":Ljava/lang/String;
    .end local v17    # "locitems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;>;"
    .end local v18    # "longitude":Ljava/lang/String;
    .end local v19    # "mAdministrativeArea":Lorg/json/JSONObject;
    .end local v20    # "mCountry":Lorg/json/JSONObject;
    .end local v21    # "mMain":Lorg/json/JSONObject;
    .end local v22    # "state":Ljava/lang/String;
    :catch_0
    move-exception v12

    .line 672
    .local v12, "ex":Ljava/lang/Exception;
    const-string v23, ""

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "parseDetailWeather_LocCities() exception "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual {v12}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 673
    invoke-virtual {v12}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v11

    .line 674
    .local v11, "ele":[Ljava/lang/StackTraceElement;
    array-length v0, v11

    move/from16 v24, v0

    const/16 v23, 0x0

    :goto_3
    move/from16 v0, v23

    move/from16 v1, v24

    if-ge v0, v1, :cond_e

    aget-object v10, v11, v23

    .line 675
    .local v10, "e":Ljava/lang/StackTraceElement;
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual {v10}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, ""

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v25 .. v25}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V

    .line 674
    add-int/lit8 v23, v23, 0x1

    goto :goto_3

    .line 604
    .end local v10    # "e":Ljava/lang/StackTraceElement;
    .end local v11    # "ele":[Ljava/lang/StackTraceElement;
    .end local v12    # "ex":Ljava/lang/Exception;
    .restart local v4    # "adminArea":Ljava/lang/String;
    .restart local v5    # "city":Ljava/lang/String;
    .restart local v6    # "cityID":Ljava/lang/String;
    .restart local v7    # "countryID":Ljava/lang/String;
    .restart local v14    # "item":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    .restart local v16    # "location":Ljava/lang/String;
    .restart local v17    # "locitems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;>;"
    .restart local v19    # "mAdministrativeArea":Lorg/json/JSONObject;
    .restart local v20    # "mCountry":Lorg/json/JSONObject;
    .restart local v21    # "mMain":Lorg/json/JSONObject;
    :cond_4
    move-object/from16 v22, v4

    .restart local v22    # "state":Ljava/lang/String;
    goto/16 :goto_1

    .line 607
    .end local v4    # "adminArea":Ljava/lang/String;
    .end local v22    # "state":Ljava/lang/String;
    :cond_5
    :try_start_1
    const-string v23, "LocalizedName"

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    const-string v24, ""

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_6

    const-string v23, "LocalizedName"

    .line 608
    move-object/from16 v0, v19

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    if-nez v23, :cond_7

    .line 609
    :cond_6
    const-string v23, "LocalizedName"

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .restart local v22    # "state":Ljava/lang/String;
    goto/16 :goto_1

    .line 611
    .end local v22    # "state":Ljava/lang/String;
    :cond_7
    invoke-static/range {p1 .. p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkLanguage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    .line 612
    .local v8, "cpLang":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    .line 613
    invoke-virtual/range {v23 .. v23}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v23

    move-object/from16 v0, v23

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    move-object/from16 v23, v0

    .line 612
    invoke-static/range {v23 .. v23}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v23

    const/16 v24, 0x1

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_8

    const-string v23, "he"

    .line 614
    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_8

    const-string v23, "en"

    .line 615
    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_8

    .line 616
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "LocalizedName"

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, "\u060c "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, "LocalizedName"

    .line 617
    move-object/from16 v0, v20

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .restart local v22    # "state":Ljava/lang/String;
    goto/16 :goto_1

    .line 619
    .end local v22    # "state":Ljava/lang/String;
    :cond_8
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "LocalizedName"

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ", "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, "LocalizedName"

    .line 620
    move-object/from16 v0, v20

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .restart local v22    # "state":Ljava/lang/String;
    goto/16 :goto_1

    .line 625
    .end local v6    # "cityID":Ljava/lang/String;
    .end local v7    # "countryID":Ljava/lang/String;
    .end local v8    # "cpLang":Ljava/lang/String;
    .end local v22    # "state":Ljava/lang/String;
    :cond_9
    const-string v23, "LocalizedName"

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    const-string v24, ""

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_a

    const-string v23, "LocalizedName"

    .line 626
    move-object/from16 v0, v19

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    if-eqz v23, :cond_a

    const-string v23, "LocalizedName"

    .line 627
    move-object/from16 v0, v19

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    if-nez v23, :cond_b

    .line 628
    :cond_a
    const-string v23, "LocalizedName"

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .restart local v22    # "state":Ljava/lang/String;
    goto/16 :goto_1

    .line 630
    .end local v22    # "state":Ljava/lang/String;
    :cond_b
    invoke-static/range {p1 .. p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkLanguage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    .line 631
    .restart local v8    # "cpLang":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    .line 632
    invoke-virtual/range {v23 .. v23}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v23

    move-object/from16 v0, v23

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    move-object/from16 v23, v0

    .line 631
    invoke-static/range {v23 .. v23}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v23

    const/16 v24, 0x1

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_c

    const-string v23, "he"

    .line 633
    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_c

    const-string v23, "en"

    .line 634
    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_c

    .line 635
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "LocalizedName"

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, "\u060c "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, "LocalizedName"

    .line 636
    move-object/from16 v0, v20

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .restart local v22    # "state":Ljava/lang/String;
    goto/16 :goto_1

    .line 638
    .end local v22    # "state":Ljava/lang/String;
    :cond_c
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "LocalizedName"

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ", "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, "LocalizedName"

    .line 639
    move-object/from16 v0, v20

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .restart local v22    # "state":Ljava/lang/String;
    goto/16 :goto_1

    .line 653
    .end local v8    # "cpLang":Ljava/lang/String;
    .restart local v13    # "icon":Ljava/lang/String;
    .restart local v15    # "latitude":Ljava/lang/String;
    .restart local v18    # "longitude":Ljava/lang/String;
    :cond_d
    const-string v23, "CurrentConditions"

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v23

    const-string v24, "Temperature"

    invoke-virtual/range {v23 .. v24}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v23

    const-string v24, "Value"

    .line 654
    invoke-virtual/range {v23 .. v24}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v3

    .restart local v3    # "Temperature":Ljava/lang/String;
    goto/16 :goto_2

    .line 677
    .end local v3    # "Temperature":Ljava/lang/String;
    .end local v5    # "city":Ljava/lang/String;
    .end local v13    # "icon":Ljava/lang/String;
    .end local v14    # "item":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    .end local v15    # "latitude":Ljava/lang/String;
    .end local v16    # "location":Ljava/lang/String;
    .end local v17    # "locitems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;>;"
    .end local v18    # "longitude":Ljava/lang/String;
    .end local v19    # "mAdministrativeArea":Lorg/json/JSONObject;
    .end local v20    # "mCountry":Lorg/json/JSONObject;
    .end local v21    # "mMain":Lorg/json/JSONObject;
    .end local v22    # "state":Ljava/lang/String;
    .restart local v11    # "ele":[Ljava/lang/StackTraceElement;
    .restart local v12    # "ex":Ljava/lang/Exception;
    :cond_e
    const/16 v17, 0x0

    goto/16 :goto_0
.end method
