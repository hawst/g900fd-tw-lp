.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;
.super Ljava/lang/Object;
.source "WindyIconView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PathAnimator"
.end annotation


# instance fields
.field delta:F

.field end:F

.field frameNum:I

.field isFrameEnd:Z

.field isIconStop:Z

.field private mCurStep:I

.field private mDelay:I

.field private mFSegmentLen:F

.field private mIsActivate:Z

.field private mPathMeasure:Landroid/graphics/PathMeasure;

.field private mTotalFrame:I

.field start:F

.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;


# direct methods
.method public constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;II)V
    .locals 2
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;
    .param p2, "delay"    # I
    .param p3, "totalstep"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 459
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 474
    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->frameNum:I

    .line 476
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->start:F

    .line 478
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->delta:F

    .line 480
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->end:F

    .line 482
    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->isIconStop:Z

    .line 483
    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->isFrameEnd:Z

    .line 460
    iput p3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->mTotalFrame:I

    .line 461
    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->mIsActivate:Z

    .line 462
    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->mCurStep:I

    .line 463
    iput p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->mDelay:I

    .line 464
    return-void
.end method


# virtual methods
.method public animate2(I)Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$ResultPathAlpha;
    .locals 9
    .param p1, "d"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 488
    iget-boolean v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->mIsActivate:Z

    if-nez v4, :cond_0

    .line 527
    :goto_0
    return-object v2

    .line 491
    :cond_0
    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->mCurStep:I

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->mDelay:I

    if-le v4, v5, :cond_4

    .line 492
    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->mCurStep:I

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->mDelay:I

    sub-int/2addr v4, v5

    iput v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->frameNum:I

    .line 494
    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    .line 495
    .local v3, "result":Landroid/graphics/Path;
    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->mFSegmentLen:F

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->frameNum:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    iput v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->start:F

    .line 496
    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->mFSegmentLen:F

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->frameNum:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    const/high16 v5, 0x40000000    # 2.0f

    mul-float/2addr v4, v5

    iput v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->delta:F

    .line 497
    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->start:F

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->delta:F

    add-float/2addr v4, v5

    iput v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->end:F

    .line 499
    const/high16 v4, 0x3f800000    # 1.0f

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->frameNum:I

    int-to-float v5, v5

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->mTotalFrame:I

    int-to-float v6, v6

    div-float/2addr v5, v6

    sub-float v0, v4, v5

    .line 500
    .local v0, "alpha":F
    const/high16 v4, 0x437f0000    # 255.0f

    mul-float v1, v4, v0

    .line 502
    .local v1, "alpha2":F
    iget-boolean v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->isFrameEnd:Z

    if-eqz v4, :cond_3

    .line 503
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->mPathMeasure:Landroid/graphics/PathMeasure;

    const/4 v5, 0x0

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->end:F

    invoke-virtual {v4, v5, v6, v3, v8}, Landroid/graphics/PathMeasure;->getSegment(FFLandroid/graphics/Path;Z)Z

    .line 504
    const/high16 v1, 0x437f0000    # 255.0f

    .line 509
    :goto_1
    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->frameNum:I

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->mTotalFrame:I

    if-lt v4, v5, :cond_2

    .line 510
    iput v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->frameNum:I

    .line 511
    iput v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->mCurStep:I

    .line 512
    iget-boolean v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->isFrameEnd:Z

    if-eqz v4, :cond_1

    .line 513
    iput-boolean v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->mIsActivate:Z

    .line 515
    :cond_1
    iget-boolean v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->isIconStop:Z

    if-eqz v4, :cond_2

    .line 516
    iput-boolean v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->isFrameEnd:Z

    .line 520
    :cond_2
    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$ResultPathAlpha;

    invoke-direct {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$ResultPathAlpha;-><init>()V

    .line 521
    .local v2, "r":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$ResultPathAlpha;
    iput-object v3, v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$ResultPathAlpha;->result:Landroid/graphics/Path;

    .line 522
    float-to-int v4, v1

    iput v4, v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$ResultPathAlpha;->alpha:I

    .line 523
    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->mCurStep:I

    add-int/2addr v4, p1

    iput v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->mCurStep:I

    goto :goto_0

    .line 506
    .end local v2    # "r":Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$ResultPathAlpha;
    :cond_3
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->mPathMeasure:Landroid/graphics/PathMeasure;

    iget v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->start:F

    iget v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->end:F

    invoke-virtual {v4, v5, v6, v3, v8}, Landroid/graphics/PathMeasure;->getSegment(FFLandroid/graphics/Path;Z)Z

    goto :goto_1

    .line 526
    .end local v0    # "alpha":F
    .end local v1    # "alpha2":F
    .end local v3    # "result":Landroid/graphics/Path;
    :cond_4
    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->mCurStep:I

    add-int/2addr v4, p1

    iput v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->mCurStep:I

    goto :goto_0
.end method

.method public getIsActivate()Z
    .locals 1

    .prologue
    .line 471
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->mIsActivate:Z

    return v0
.end method

.method public restartAnimation()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 531
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->mIsActivate:Z

    .line 532
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->mCurStep:I

    .line 533
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->frameNum:I

    .line 534
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->isIconStop:Z

    .line 535
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->isFrameEnd:Z

    .line 536
    return-void
.end method

.method public setIconStop()V
    .locals 1

    .prologue
    .line 485
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->isIconStop:Z

    .line 486
    return-void
.end method

.method public setIsActivate(Z)V
    .locals 0
    .param p1, "isact"    # Z

    .prologue
    .line 467
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->mIsActivate:Z

    .line 468
    return-void
.end method

.method public setPath(Landroid/graphics/Path;)V
    .locals 3
    .param p1, "path"    # Landroid/graphics/Path;

    .prologue
    .line 539
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0, p1}, Landroid/graphics/Path;-><init>(Landroid/graphics/Path;)V

    .line 540
    .local v0, "p":Landroid/graphics/Path;
    new-instance v1, Landroid/graphics/PathMeasure;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Landroid/graphics/PathMeasure;-><init>(Landroid/graphics/Path;Z)V

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->mPathMeasure:Landroid/graphics/PathMeasure;

    .line 541
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->mPathMeasure:Landroid/graphics/PathMeasure;

    invoke-virtual {v1}, Landroid/graphics/PathMeasure;->getLength()F

    move-result v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->mTotalFrame:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/WindyIconView$PathAnimator;->mFSegmentLen:F

    .line 542
    return-void
.end method
