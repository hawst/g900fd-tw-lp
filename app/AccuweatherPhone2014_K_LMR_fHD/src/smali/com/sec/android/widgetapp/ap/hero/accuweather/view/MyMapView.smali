.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;
.super Lcom/google/android/maps/MapView;
.source "MyMapView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView$Call;
    }
.end annotation


# static fields
.field private static final NONE:I = 0x0

.field private static final ZOOM:I = 0x3


# instance fields
.field call:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView$Call;

.field private mode:I

.field movecount:I

.field private oldSpacing:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "arg1"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 18
    invoke-direct {p0, p1, p2}, Lcom/google/android/maps/MapView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->oldSpacing:F

    .line 40
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->mode:I

    .line 106
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->movecount:I

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "arg1"    # Landroid/util/AttributeSet;
    .param p3, "arg2"    # I

    .prologue
    const/4 v1, 0x0

    .line 14
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/maps/MapView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->oldSpacing:F

    .line 40
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->mode:I

    .line 106
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->movecount:I

    .line 15
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "arg1"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 22
    invoke-direct {p0, p1, p2}, Lcom/google/android/maps/MapView;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 37
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->oldSpacing:F

    .line 40
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->mode:I

    .line 106
    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->movecount:I

    .line 23
    return-void
.end method

.method private getTouchSpacing(Landroid/view/MotionEvent;)F
    .locals 6
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 44
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 45
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    sub-float v0, v2, v3

    .line 46
    .local v0, "x":F
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    sub-float v1, v2, v3

    .line 47
    .local v1, "y":F
    mul-float v2, v0, v0

    mul-float v3, v1, v1

    add-float/2addr v2, v3

    invoke-static {v2}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v2

    .line 49
    .end local v0    # "x":F
    .end local v1    # "y":F
    :goto_0
    return v2

    :cond_0
    const/high16 v2, -0x40800000    # -1.0f

    goto :goto_0
.end method


# virtual methods
.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x3

    const/4 v3, 0x0

    .line 53
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    and-int/lit16 v4, v4, 0xff

    packed-switch v4, :pswitch_data_0

    .line 99
    :cond_0
    :goto_0
    :pswitch_0
    :try_start_0
    invoke-super {p0, p1}, Lcom/google/android/maps/MapView;->onTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 102
    :goto_1
    return v2

    .line 55
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->call:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView$Call;

    if-eqz v2, :cond_0

    .line 56
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setEnterOnTap(Z)V

    .line 57
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->call:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView$Call;

    invoke-interface {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView$Call;->run()V

    goto :goto_0

    .line 61
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->call:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView$Call;

    if-eqz v2, :cond_0

    .line 62
    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->movecount:I

    const/16 v4, 0xc

    if-ge v2, v4, :cond_1

    .line 63
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setEnterOnTap(Z)V

    .line 66
    :cond_1
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->call:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView$Call;

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->movecount:I

    invoke-interface {v2, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView$Call;->up(I)V

    .line 67
    iput v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->movecount:I

    goto :goto_0

    .line 72
    :pswitch_3
    iput v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->mode:I

    goto :goto_0

    .line 75
    :pswitch_4
    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setEnterOnTap(Z)V

    .line 76
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getTouchSpacing(Landroid/view/MotionEvent;)F

    move-result v2

    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->oldSpacing:F

    .line 77
    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->oldSpacing:F

    const/high16 v4, 0x42c80000    # 100.0f

    cmpl-float v2, v2, v4

    if-lez v2, :cond_0

    .line 78
    iput v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->mode:I

    goto :goto_0

    .line 82
    :pswitch_5
    invoke-static {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setEnterOnTap(Z)V

    .line 84
    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->movecount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->movecount:I

    .line 85
    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->mode:I

    if-ne v4, v5, :cond_0

    .line 86
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getTouchSpacing(Landroid/view/MotionEvent;)F

    move-result v1

    .line 87
    .local v1, "newSpacing":F
    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->oldSpacing:F

    cmpl-float v4, v4, v1

    if-lez v4, :cond_0

    .line 88
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getZoomLevel()I

    move-result v4

    if-gt v4, v5, :cond_0

    goto :goto_1

    .line 100
    .end local v1    # "newSpacing":F
    :catch_0
    move-exception v0

    .line 101
    .local v0, "ex":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move v2, v3

    .line 102
    goto :goto_1

    .line 53
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public setInterface(Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView$Call;)V
    .locals 0
    .param p1, "call"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView$Call;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->call:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView$Call;

    .line 35
    return-void
.end method
