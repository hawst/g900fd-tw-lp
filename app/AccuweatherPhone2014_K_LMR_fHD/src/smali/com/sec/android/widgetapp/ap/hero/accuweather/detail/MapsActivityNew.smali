.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;
.super Lcom/google/android/maps/MapActivity;
.source "MapsActivityNew.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;,
        Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MyLocationListener;
    }
.end annotation


# static fields
.field public static mFontSamsungNumber3L:Landroid/graphics/Typeface;


# instance fields
.field private createMapItemsHandler:Landroid/os/Handler;

.field dataHandler:Landroid/os/Handler;

.field detailAddHandler:Landroid/os/Handler;

.field detailHandler:Landroid/os/Handler;

.field private detailitem:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

.field private height:I

.field loadingHandler:Landroid/os/Handler;

.field locHandler:Landroid/os/Handler;

.field private mAddedMapItem:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;",
            ">;"
        }
    .end annotation
.end field

.field mBackKeyHandler:Landroid/os/Handler;

.field private mClearTextBtn:Landroid/widget/ImageView;

.field private mDialog:Landroid/app/Dialog;

.field private mDialogSelected:I

.field private mGpsBtn:Landroid/widget/RelativeLayout;

.field private mHttpClient:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

.field public mHttpThreads:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Thread;",
            ">;"
        }
    .end annotation
.end field

.field private mInitGeoPoint:Lcom/google/android/maps/GeoPoint;

.field private mInputManager:Landroid/view/inputmethod/InputMethodManager;

.field private mIsKeyboardShown:Z

.field private mIsLocaleRTL:Z

.field private mIsWQHD:Z

.field mKoreaCities:[Ljava/lang/String;

.field mKoreaCityIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mKoreaLocation:[Lcom/google/android/maps/GeoPoint;

.field private mLatitude:I

.field private mLauncher:I

.field private mListBtn:Landroid/widget/RelativeLayout;

.field private mListZoom5:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mListZoom6:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mListZoom7:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mLoadingDialog:Landroid/app/Dialog;

.field private mLocationListener:Landroid/location/LocationListener;

.field private mLocationManager:Landroid/location/LocationManager;

.field private mLongitude:I

.field private mMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

.field private mMapItemsList5:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

.field private mMapItemsList6:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

.field private mMapItemsList7:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

.field private mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

.field private mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

.field private mParser:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;

.field private mPreLocation:Ljava/lang/String;

.field private mPreLongitutde:D

.field private mPreTapTime:J

.field private mPrelatitude:D

.field protected mPreviousZoomLevel:I

.field private mRefreshToastDisplay:Z

.field private mRetry:Z

.field private mRunning:Z

.field private mSearchLayout:Landroid/widget/LinearLayout;

.field private mSearchLayoutOutLine:Landroid/widget/RelativeLayout;

.field private mSearchMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

.field private mSearchText:Landroid/widget/TextView;

.field private mTempImage:I

.field private mTempScale:I

.field public mTextCount:I

.field private mToastHint:Landroid/widget/Toast;

.field private mTopLayout:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;

.field private mUpdateText:Landroid/widget/TextView;

.field private mUrlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

.field private mZoomlevel:I

.field private moveHandler:Landroid/os/Handler;

.field private myMapController:Lcom/google/android/maps/MapController;

.field positive:Z

.field private searchEditTextAccessibilityDelegate:Landroid/view/View$AccessibilityDelegate;

.field seletedInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

.field setKeyboardVisibleDelayHandler:Landroid/os/Handler;

.field private width:I

.field zoomHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 216
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mFontSamsungNumber3L:Landroid/graphics/Typeface;

    return-void
.end method

.method public constructor <init>()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 98
    invoke-direct {p0}, Lcom/google/android/maps/MapActivity;-><init>()V

    .line 99
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    .line 101
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListZoom5:Ljava/util/ArrayList;

    .line 103
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListZoom6:Ljava/util/ArrayList;

    .line 105
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListZoom7:Ljava/util/ArrayList;

    .line 107
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mAddedMapItem:Ljava/util/ArrayList;

    .line 121
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mClearTextBtn:Landroid/widget/ImageView;

    .line 141
    iput v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mTextCount:I

    .line 143
    iput v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLauncher:I

    .line 145
    iput v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mDialogSelected:I

    .line 147
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mHttpThreads:Ljava/util/ArrayList;

    .line 161
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mZoomlevel:I

    .line 163
    iput-boolean v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mRetry:Z

    .line 165
    iput v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mPreviousZoomLevel:I

    .line 167
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mKoreaCityIds:Ljava/util/ArrayList;

    .line 169
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "cityId:224032"

    aput-object v1, v0, v5

    const-string v1, "cityId:223347"

    aput-object v1, v0, v4

    const-string v1, "cityId:223682"

    aput-object v1, v0, v7

    const-string v1, "cityId:223656"

    aput-object v1, v0, v8

    const/4 v1, 0x4

    const-string v2, "cityId:223146"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mKoreaCities:[Ljava/lang/String;

    .line 177
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/maps/GeoPoint;

    new-instance v1, Lcom/google/android/maps/GeoPoint;

    const v2, 0x233dd1a

    const v3, 0x78994a8

    invoke-direct {v1, v2, v3}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    aput-object v1, v0, v5

    new-instance v1, Lcom/google/android/maps/GeoPoint;

    const v2, 0x2233ffa

    const v3, 0x7af2830

    invoke-direct {v1, v2, v3}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    aput-object v1, v0, v4

    new-instance v1, Lcom/google/android/maps/GeoPoint;

    const v2, 0x225dcb0

    const v3, 0x7bab1b4

    invoke-direct {v1, v2, v3}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    aput-object v1, v0, v7

    new-instance v1, Lcom/google/android/maps/GeoPoint;

    const v2, 0x238eed6

    const v3, 0x7a049f0

    invoke-direct {v1, v2, v3}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    aput-object v1, v0, v8

    const/4 v1, 0x4

    new-instance v2, Lcom/google/android/maps/GeoPoint;

    const v3, 0x238ca28

    const v4, 0x784ec78    # 2.0000136E-34f

    invoke-direct {v2, v3, v4}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mKoreaLocation:[Lcom/google/android/maps/GeoPoint;

    .line 197
    iput-boolean v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mRefreshToastDisplay:Z

    .line 199
    iput-boolean v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mRunning:Z

    .line 206
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mPreTapTime:J

    .line 208
    iput-boolean v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mIsKeyboardShown:Z

    .line 210
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mTopLayout:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;

    .line 212
    iput-boolean v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mIsWQHD:Z

    .line 214
    iput-boolean v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mIsLocaleRTL:Z

    .line 219
    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchLayoutOutLine:Landroid/widget/RelativeLayout;

    .line 270
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->createMapItemsHandler:Landroid/os/Handler;

    .line 609
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$4;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$4;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mBackKeyHandler:Landroid/os/Handler;

    .line 615
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$5;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$5;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->setKeyboardVisibleDelayHandler:Landroid/os/Handler;

    .line 632
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$6;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$6;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->zoomHandler:Landroid/os/Handler;

    .line 721
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$7;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$7;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->moveHandler:Landroid/os/Handler;

    .line 739
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$8;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$8;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->locHandler:Landroid/os/Handler;

    .line 1229
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$31;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$31;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->loadingHandler:Landroid/os/Handler;

    .line 1326
    iput-boolean v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->positive:Z

    .line 1358
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$37;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$37;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->dataHandler:Landroid/os/Handler;

    .line 1631
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$45;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->detailHandler:Landroid/os/Handler;

    .line 1765
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$46;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->detailAddHandler:Landroid/os/Handler;

    .line 2203
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$50;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$50;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->searchEditTextAccessibilityDelegate:Landroid/view/View$AccessibilityDelegate;

    .line 2219
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->setupMapView()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->removeAllMapList()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;
    .param p1, "x1"    # Z

    .prologue
    .line 98
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->setKeyboardVisible(Z)V

    return-void
.end method

.method static synthetic access$1102(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;
    .param p1, "x1"    # Z

    .prologue
    .line 98
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mIsKeyboardShown:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListZoom5:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListZoom6:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListZoom7:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;
    .param p1, "x1"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mZoomlevel:I

    return v0
.end method

.method static synthetic access$1602(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;
    .param p1, "x1"    # I

    .prologue
    .line 98
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mZoomlevel:I

    return p1
.end method

.method static synthetic access$1700(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->createMapItemsHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/google/android/maps/GeoPoint;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mInitGeoPoint:Lcom/google/android/maps/GeoPoint;

    return-object v0
.end method

.method static synthetic access$1802(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Lcom/google/android/maps/GeoPoint;)Lcom/google/android/maps/GeoPoint;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;
    .param p1, "x1"    # Lcom/google/android/maps/GeoPoint;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mInitGeoPoint:Lcom/google/android/maps/GeoPoint;

    return-object p1
.end method

.method static synthetic access$1900(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Lcom/google/android/maps/GeoPoint;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;
    .param p1, "x1"    # Lcom/google/android/maps/GeoPoint;

    .prologue
    .line 98
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->centerLocation(Lcom/google/android/maps/GeoPoint;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;
    .param p1, "x1"    # I

    .prologue
    .line 98
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->createMapItems(I)V

    return-void
.end method

.method static synthetic access$2000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->moveHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Landroid/location/LocationListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLocationListener:Landroid/location/LocationListener;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Landroid/location/LocationManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLocationManager:Landroid/location/LocationManager;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->hideLoadingDialog()V

    return-void
.end method

.method static synthetic access$2400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mRetry:Z

    return v0
.end method

.method static synthetic access$2402(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;
    .param p1, "x1"    # Z

    .prologue
    .line 98
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mRetry:Z

    return p1
.end method

.method static synthetic access$2500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mGpsBtn:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 98
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->performClickGpsBtn(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$2700(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Landroid/widget/AutoCompleteTextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mClearTextBtn:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItemsList5:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->doSearch()V

    return-void
.end method

.method static synthetic access$3200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mRefreshToastDisplay:Z

    return v0
.end method

.method static synthetic access$3202(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;
    .param p1, "x1"    # Z

    .prologue
    .line 98
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mRefreshToastDisplay:Z

    return p1
.end method

.method static synthetic access$3300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLauncher:I

    return v0
.end method

.method static synthetic access$3400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mToastHint:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$3402(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;
    .param p1, "x1"    # Landroid/widget/Toast;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mToastHint:Landroid/widget/Toast;

    return-object p1
.end method

.method static synthetic access$3500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListBtn:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->performLocation()V

    return-void
.end method

.method static synthetic access$3700(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getLoc()V

    return-void
.end method

.method static synthetic access$3800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mParser:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;

    return-object v0
.end method

.method static synthetic access$3900(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Ljava/util/ArrayList;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 98
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->showCities(Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mDialogSelected:I

    return v0
.end method

.method static synthetic access$4002(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;
    .param p1, "x1"    # I

    .prologue
    .line 98
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mDialogSelected:I

    return p1
.end method

.method static synthetic access$4100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mTempScale:I

    return v0
.end method

.method static synthetic access$4102(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;
    .param p1, "x1"    # I

    .prologue
    .line 98
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mTempScale:I

    return p1
.end method

.method static synthetic access$4200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;DD)Lcom/google/android/maps/GeoPoint;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;
    .param p1, "x1"    # D
    .param p3, "x2"    # D

    .prologue
    .line 98
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getPoint(DD)Lcom/google/android/maps/GeoPoint;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4302(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Landroid/app/Dialog;)Landroid/app/Dialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;
    .param p1, "x1"    # Landroid/app/Dialog;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLoadingDialog:Landroid/app/Dialog;

    return-object p1
.end method

.method static synthetic access$4400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->stopHttpThread()V

    return-void
.end method

.method static synthetic access$4500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->detailitem:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    return-object v0
.end method

.method static synthetic access$4600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLatitude:I

    return v0
.end method

.method static synthetic access$4700(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLongitude:I

    return v0
.end method

.method static synthetic access$4800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)D
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget-wide v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mPrelatitude:D

    return-wide v0
.end method

.method static synthetic access$4900(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)D
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget-wide v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mPreLongitutde:D

    return-wide v0
.end method

.method static synthetic access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    return-object v0
.end method

.method static synthetic access$5000(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;
    .param p1, "x1"    # I

    .prologue
    .line 98
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->checkResultCode(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$5100(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mUpdateText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$5200(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mTempImage:I

    return v0
.end method

.method static synthetic access$5202(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;
    .param p1, "x1"    # I

    .prologue
    .line 98
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mTempImage:I

    return p1
.end method

.method static synthetic access$5300(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mPreLocation:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5400(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget-wide v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mPreTapTime:J

    return-wide v0
.end method

.method static synthetic access$5402(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;J)J
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;
    .param p1, "x1"    # J

    .prologue
    .line 98
    iput-wide p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mPreTapTime:J

    return-wide p1
.end method

.method static synthetic access$5500(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mIsWQHD:Z

    return v0
.end method

.method static synthetic access$5600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mIsLocaleRTL:Z

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItemsList6:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItemsList7:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mRunning:Z

    return v0
.end method

.method static synthetic access$802(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;
    .param p1, "x1"    # Z

    .prologue
    .line 98
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mRunning:Z

    return p1
.end method

.method private addMapCityInfoInList(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 4
    .param p3, "cityName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1108
    .local p1, "targetList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;>;"
    .local p2, "tempList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;>;"
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_2

    .line 1109
    :cond_0
    const-string v2, ""

    const-string v3, "fMCIIL n"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1121
    :cond_1
    return-void

    .line 1113
    :cond_2
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 1115
    .local v1, "targetListSize":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 1116
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getCity()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1117
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1115
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private centerLocation(Lcom/google/android/maps/GeoPoint;)V
    .locals 1
    .param p1, "centerGeoPoint"    # Lcom/google/android/maps/GeoPoint;

    .prologue
    .line 222
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->hideLoadingDialog()V

    .line 223
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->myMapController:Lcom/google/android/maps/MapController;

    if-eqz v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->myMapController:Lcom/google/android/maps/MapController;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/MapController;->animateTo(Lcom/google/android/maps/GeoPoint;)V

    .line 225
    :cond_0
    return-void
.end method

.method private checkResultCode(I)I
    .locals 2
    .param p1, "resultCode"    # I

    .prologue
    .line 1868
    const/4 v0, 0x0

    .line 1870
    .local v0, "result":I
    const/4 v1, -0x1

    if-ne v1, p1, :cond_1

    .line 1871
    const/16 v0, 0x3e7

    .line 1875
    :cond_0
    :goto_0
    return v0

    .line 1872
    :cond_1
    if-nez p1, :cond_0

    .line 1873
    const/16 v0, 0x3e7

    goto :goto_0
.end method

.method private cleanResource()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2048
    const-string v0, ""

    const-string v1, "===stopHttpThread==="

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2049
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->stopHttpThread()V

    .line 2051
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mKoreaCityIds:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 2052
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mKoreaCityIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2053
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mKoreaCityIds:Ljava/util/ArrayList;

    .line 2056
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListBtn:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 2057
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListBtn:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2058
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListBtn:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2059
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListBtn:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2060
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListBtn:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2061
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListBtn:Landroid/widget/RelativeLayout;

    .line 2064
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mGpsBtn:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_2

    .line 2065
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mGpsBtn:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2066
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mGpsBtn:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2067
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mGpsBtn:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2068
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mGpsBtn:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2069
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mGpsBtn:Landroid/widget/RelativeLayout;

    .line 2072
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

    if-eqz v0, :cond_3

    .line 2073
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2074
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 2075
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2076
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 2077
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2078
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2079
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

    .line 2082
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_4

    .line 2083
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 2084
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mDialog:Landroid/app/Dialog;

    .line 2087
    :cond_4
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLoadingDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_5

    .line 2088
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 2089
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLoadingDialog:Landroid/app/Dialog;

    .line 2092
    :cond_5
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLocationManager:Landroid/location/LocationManager;

    .line 2093
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLocationListener:Landroid/location/LocationListener;

    .line 2095
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mUrlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    .line 2096
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mInputManager:Landroid/view/inputmethod/InputMethodManager;

    .line 2097
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mHttpClient:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    .line 2098
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mParser:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;

    .line 2099
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->myMapController:Lcom/google/android/maps/MapController;

    .line 2100
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mInitGeoPoint:Lcom/google/android/maps/GeoPoint;

    .line 2101
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mUpdateText:Landroid/widget/TextView;

    if-eqz v0, :cond_6

    .line 2102
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mUpdateText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2103
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mUpdateText:Landroid/widget/TextView;

    .line 2105
    :cond_6
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchLayout:Landroid/widget/LinearLayout;

    .line 2106
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchText:Landroid/widget/TextView;

    .line 2108
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->seletedInfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    .line 2110
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mTopLayout:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;

    if-eqz v0, :cond_7

    .line 2111
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mTopLayout:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;->setHandler(Landroid/os/Handler;I)V

    .line 2112
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mTopLayout:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;

    .line 2115
    :cond_7
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mBackKeyHandler:Landroid/os/Handler;

    if-eqz v0, :cond_8

    .line 2116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mBackKeyHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 2117
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mBackKeyHandler:Landroid/os/Handler;

    .line 2120
    :cond_8
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->setKeyboardVisibleDelayHandler:Landroid/os/Handler;

    if-eqz v0, :cond_9

    .line 2121
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->setKeyboardVisibleDelayHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 2122
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->setKeyboardVisibleDelayHandler:Landroid/os/Handler;

    .line 2125
    :cond_9
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mFontSamsungNumber3L:Landroid/graphics/Typeface;

    if-eqz v0, :cond_a

    .line 2126
    sput-object v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mFontSamsungNumber3L:Landroid/graphics/Typeface;

    .line 2128
    :cond_a
    return-void
.end method

.method private createFont()V
    .locals 3

    .prologue
    .line 251
    :try_start_0
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mFontSamsungNumber3L:Landroid/graphics/Typeface;

    if-nez v1, :cond_0

    .line 252
    const-string v1, "/system/fonts/SamsungSans-Num3L.ttf"

    invoke-static {v1}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    sput-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mFontSamsungNumber3L:Landroid/graphics/Typeface;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 258
    :cond_0
    :goto_0
    return-void

    .line 254
    :catch_0
    move-exception v0

    .line 255
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    .line 256
    const-string v1, ""

    const-string v2, "font is not enable."

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private createMapItems(I)V
    .locals 4
    .param p1, "listType"    # I

    .prologue
    .line 567
    packed-switch p1, :pswitch_data_0

    .line 592
    :cond_0
    :goto_0
    return-void

    .line 569
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListZoom5:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItemsList5:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    if-nez v0, :cond_0

    .line 570
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListZoom5:Ljava/util/ArrayList;

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mTempScale:I

    const/4 v3, 0x5

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Ljava/util/ArrayList;II)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItemsList5:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    goto :goto_0

    .line 575
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListZoom6:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItemsList6:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    if-nez v0, :cond_0

    .line 576
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListZoom6:Ljava/util/ArrayList;

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mTempScale:I

    const/4 v3, 0x6

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Ljava/util/ArrayList;II)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItemsList6:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    goto :goto_0

    .line 581
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListZoom7:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItemsList7:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    if-nez v0, :cond_0

    .line 582
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListZoom7:Ljava/util/ArrayList;

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mTempScale:I

    const/4 v3, 0x7

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Ljava/util/ArrayList;II)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItemsList7:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    goto :goto_0

    .line 587
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mAddedMapItem:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    if-nez v0, :cond_0

    .line 588
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mAddedMapItem:Ljava/util/ArrayList;

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mTempScale:I

    const/16 v3, 0x8

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Ljava/util/ArrayList;II)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    goto :goto_0

    .line 567
    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private doSearch()V
    .locals 10

    .prologue
    const/4 v9, 0x5

    const/4 v4, 0x1

    const/4 v8, 0x0

    .line 1138
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v3}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 1140
    .local v1, "strCityName":Ljava/lang/String;
    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_6

    .line 1144
    invoke-direct {p0, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->setKeyboardVisible(Z)V

    .line 1145
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1153
    .local v2, "templist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;>;"
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListZoom5:Ljava/util/ArrayList;

    invoke-direct {p0, v3, v2, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->addMapCityInfoInList(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 1154
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListZoom6:Ljava/util/ArrayList;

    invoke-direct {p0, v3, v2, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->addMapCityInfoInList(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 1155
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListZoom7:Ljava/util/ArrayList;

    invoke-direct {p0, v3, v2, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->addMapCityInfoInList(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 1156
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mAddedMapItem:Ljava/util/ArrayList;

    invoke-direct {p0, v3, v2, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->addMapCityInfoInList(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 1159
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v3, v4, :cond_4

    .line 1160
    iput v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mZoomlevel:I

    .line 1161
    invoke-direct {p0, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->setKeyboardVisible(Z)V

    .line 1162
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getOverlays()Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    invoke-interface {v3, v4}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1164
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    if-eqz v3, :cond_0

    .line 1165
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->clearItems()V

    .line 1166
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    .line 1169
    :cond_0
    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mTempScale:I

    invoke-direct {v3, p0, v2, v4, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Ljava/util/ArrayList;II)V

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    .line 1171
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->removeAllMapList()V

    .line 1173
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getOverlays()Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1174
    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    .line 1176
    .local v0, "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mKoreaCityIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLocation()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getZoomLevel()I

    move-result v3

    const/16 v4, 0x9

    if-le v3, v4, :cond_3

    .line 1177
    :cond_1
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getController()Lcom/google/android/maps/MapController;

    move-result-object v3

    .line 1178
    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLatitude()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 1179
    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLongitude()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    .line 1178
    invoke-direct {p0, v4, v5, v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getPoint(DD)Lcom/google/android/maps/GeoPoint;

    move-result-object v4

    .line 1177
    invoke-virtual {v3, v4}, Lcom/google/android/maps/MapController;->animateTo(Lcom/google/android/maps/GeoPoint;)V

    .line 1184
    :goto_0
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getController()Lcom/google/android/maps/MapController;

    move-result-object v3

    invoke-virtual {v3, v9}, Lcom/google/android/maps/MapController;->setZoom(I)I

    .line 1190
    .end local v0    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    :cond_2
    :goto_1
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v3, v8}, Landroid/widget/AutoCompleteTextView;->setFocusable(Z)V

    .line 1196
    .end local v2    # "templist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;>;"
    :goto_2
    return-void

    .line 1181
    .restart local v0    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    .restart local v2    # "templist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;>;"
    :cond_3
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getController()Lcom/google/android/maps/MapController;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mKoreaLocation:[Lcom/google/android/maps/GeoPoint;

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mKoreaCityIds:Ljava/util/ArrayList;

    .line 1182
    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLocation()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v5

    aget-object v4, v4, v5

    .line 1181
    invoke-virtual {v3, v4}, Lcom/google/android/maps/MapController;->animateTo(Lcom/google/android/maps/GeoPoint;)V

    goto :goto_0

    .line 1185
    .end local v0    # "info":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;
    :cond_4
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-le v3, v4, :cond_5

    .line 1186
    invoke-direct {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->showResult(Ljava/util/ArrayList;)V

    goto :goto_1

    .line 1187
    :cond_5
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_2

    .line 1188
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->performSearch()V

    goto :goto_1

    .line 1194
    .end local v2    # "templist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;>;"
    :cond_6
    const v3, 0x7f0d006e

    invoke-static {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    goto :goto_2
.end method

.method private getLoc()V
    .locals 10

    .prologue
    .line 772
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLocationManager:Landroid/location/LocationManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getProviders(Z)Ljava/util/List;

    move-result-object v8

    .line 774
    .local v8, "providers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v8, :cond_0

    .line 775
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 776
    .local v9, "s":Ljava/lang/String;
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pv "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 780
    .end local v9    # "s":Ljava/lang/String;
    :cond_0
    const-string v0, "location"

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/location/LocationManager;

    .line 782
    .local v7, "locationManager":Landroid/location/LocationManager;
    if-eqz v8, :cond_1

    .line 783
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "gps"

    .line 784
    invoke-virtual {v7, v0}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "network"

    .line 785
    invoke-virtual {v7, v0}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 786
    :cond_1
    sget-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isVerizon:Z

    if-eqz v0, :cond_3

    .line 787
    const v0, 0x7f0d002e

    invoke-static {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    .line 814
    :cond_2
    :goto_1
    return-void

    .line 789
    :cond_3
    const v0, 0x7f0d002d

    invoke-static {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    goto :goto_1

    .line 790
    :cond_4
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLocationListener:Landroid/location/LocationListener;

    if-eqz v0, :cond_2

    .line 792
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->locHandler:Landroid/os/Handler;

    if-eqz v0, :cond_5

    .line 793
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->locHandler:Landroid/os/Handler;

    const v1, -0x13d30

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 795
    :cond_5
    const v0, 0x7f0d0038

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->showLoadingDialog(Ljava/lang/String;)V

    .line 797
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mInitGeoPoint:Lcom/google/android/maps/GeoPoint;

    .line 800
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLocationListener:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 802
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "network"

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLocationListener:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 810
    :goto_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mInitGeoPoint:Lcom/google/android/maps/GeoPoint;

    if-eqz v0, :cond_2

    .line 811
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mInitGeoPoint:Lcom/google/android/maps/GeoPoint;

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->centerLocation(Lcom/google/android/maps/GeoPoint;)V

    goto :goto_1

    .line 804
    :catch_0
    move-exception v6

    .line 806
    .local v6, "ex":Ljava/lang/IllegalArgumentException;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "performLocation "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v6}, Ljava/lang/IllegalArgumentException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V

    goto :goto_2
.end method

.method private getPoint(DD)Lcom/google/android/maps/GeoPoint;
    .locals 5
    .param p1, "lat"    # D
    .param p3, "lon"    # D

    .prologue
    const-wide v3, 0x412e848000000000L    # 1000000.0

    .line 2150
    new-instance v0, Lcom/google/android/maps/GeoPoint;

    mul-double v1, p1, v3

    double-to-int v1, v1

    mul-double v2, p3, v3

    double-to-int v2, v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    return-object v0
.end method

.method private hideLoadingDialog()V
    .locals 3

    .prologue
    .line 1561
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLoadingDialog:Landroid/app/Dialog;

    if-eqz v1, :cond_0

    .line 1563
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 1564
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLoadingDialog:Landroid/app/Dialog;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1571
    :cond_0
    :goto_0
    return-void

    .line 1565
    :catch_0
    move-exception v0

    .line 1567
    .local v0, "ex":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MenuAdd.hideLoadingDialog() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private initViews()V
    .locals 7

    .prologue
    const v6, 0x7f0d0090

    const v5, 0x7f0d0025

    const v4, 0x7f0d008a

    .line 357
    const v1, 0x7f08010e

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mGpsBtn:Landroid/widget/RelativeLayout;

    .line 358
    const v1, 0x7f080120

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListBtn:Landroid/widget/RelativeLayout;

    .line 362
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 364
    .local v0, "locale":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090016

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 365
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mGpsBtn:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 366
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListBtn:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 378
    :goto_0
    const v1, 0x7f08011e

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchLayoutOutLine:Landroid/widget/RelativeLayout;

    .line 381
    const v1, 0x7f08010a

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/AutoCompleteTextView;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

    .line 382
    const v1, 0x7f08010b

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchLayout:Landroid/widget/LinearLayout;

    .line 383
    const v1, 0x7f08010c

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchText:Landroid/widget/TextView;

    .line 384
    const v1, 0x7f08011f

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mClearTextBtn:Landroid/widget/ImageView;

    .line 385
    const v1, 0x7f080122

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mUpdateText:Landroid/widget/TextView;

    .line 387
    return-void

    .line 368
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "fr"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 369
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mGpsBtn:Landroid/widget/RelativeLayout;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 370
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListBtn:Landroid/widget/RelativeLayout;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 373
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mGpsBtn:Landroid/widget/RelativeLayout;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 374
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListBtn:Landroid/widget/RelativeLayout;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method private isLocaleRTL()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 261
    .line 262
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 261
    invoke-static {v1}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 267
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private performClickGpsBtn(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1085
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "G Btn "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1087
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mDialog:Landroid/app/Dialog;

    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$26;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$26;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    new-instance v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$27;

    invoke-direct {v4, p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$27;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Landroid/view/View;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-static/range {v0 .. v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->performClickGpsBtn(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/LocationGetter;Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;Landroid/content/DialogInterface$OnDismissListener;Z)Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mDialog:Landroid/app/Dialog;

    .line 1105
    return-void
.end method

.method private performLocation()V
    .locals 2

    .prologue
    .line 816
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090011

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 817
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->isAgreeUseLocation(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 818
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$9;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$9;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    invoke-static {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->showUseCurrentLocation(Landroid/content/Context;Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)Landroid/app/Dialog;

    .line 834
    :goto_0
    return-void

    .line 827
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mGpsBtn:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->performClickGpsBtn(Landroid/view/View;)V

    goto :goto_0

    .line 830
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mGpsBtn:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->performClickGpsBtn(Landroid/view/View;)V

    goto :goto_0
.end method

.method private performSearch()V
    .locals 8

    .prologue
    .line 2158
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->isNetWorkConnected(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2159
    const v0, 0x7f0d0020

    invoke-static {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    .line 2201
    :cond_0
    :goto_0
    return-void

    .line 2163
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 2164
    .local v6, "strCityName":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mUrlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    .line 2165
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkLanguage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 2164
    invoke-virtual {v0, v6, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeUrlForGetCityList(Ljava/lang/String;Ljava/lang/String;)Ljava/net/URL;

    move-result-object v3

    .line 2167
    .local v3, "url":Ljava/net/URL;
    if-eqz v3, :cond_0

    .line 2170
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->showLoadingDialog()V

    .line 2172
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->loadingHandler:Landroid/os/Handler;

    if-eqz v0, :cond_2

    .line 2173
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->loadingHandler:Landroid/os/Handler;

    const v1, -0x13d30

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2176
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mHttpThreads:Ljava/util/ArrayList;

    if-nez v0, :cond_3

    .line 2177
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mHttpThreads:Ljava/util/ArrayList;

    .line 2180
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mUrlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeHeader()Lorg/apache/http/message/HeaderGroup;

    move-result-object v4

    .line 2182
    .local v4, "headerGroup":Lorg/apache/http/message/HeaderGroup;
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mHttpClient:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$49;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$49;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    move-object v2, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->get(ILandroid/content/Context;Ljava/net/URL;Lorg/apache/http/message/HeaderGroup;Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;)Ljava/lang/Thread;

    move-result-object v7

    .line 2199
    .local v7, "t":Ljava/lang/Thread;
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private removeAllMapList()V
    .locals 2

    .prologue
    .line 668
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getOverlays()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 669
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getOverlays()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItemsList5:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 670
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getOverlays()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItemsList6:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 671
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getOverlays()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItemsList7:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 672
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getOverlays()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 673
    return-void
.end method

.method private removeMapCityInfoInList(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 3
    .param p2, "stateName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1124
    .local p1, "targetList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;>;"
    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    .line 1125
    :cond_0
    const-string v1, ""

    const-string v2, "fMCIIL n"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1135
    :cond_1
    return-void

    .line 1129
    :cond_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 1130
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getState()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1131
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1129
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private setEventListener()V
    .locals 2

    .prologue
    .line 837
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$10;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$10;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 851
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$11;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$11;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 868
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$12;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$12;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 912
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$13;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$13;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 931
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$14;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$14;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 937
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$15;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$15;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 947
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListBtn:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$16;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$16;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 988
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListBtn:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$17;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$17;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1000
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListBtn:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$18;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$18;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1012
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListBtn:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$19;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$19;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1023
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mGpsBtn:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$20;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$20;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1035
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mGpsBtn:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$21;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$21;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1047
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mGpsBtn:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$22;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$22;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1058
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mGpsBtn:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$23;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$23;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1070
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mClearTextBtn:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$24;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$24;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1078
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mUpdateText:Landroid/widget/TextView;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$25;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$25;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1083
    return-void
.end method

.method private setKeyboardVisible(Z)V
    .locals 5
    .param p1, "visible"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1532
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mInputManager:Landroid/view/inputmethod/InputMethodManager;

    if-nez v1, :cond_0

    .line 1550
    :goto_0
    return-void

    .line 1535
    :cond_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mInputManager:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodManager;->isAccessoryKeyboardState()I

    move-result v0

    .line 1536
    .local v0, "mKeyboard":I
    and-int/lit8 v1, v0, 0x1

    if-eq v1, v4, :cond_1

    and-int/lit8 v1, v0, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 1540
    :cond_1
    const/4 p1, 0x0

    .line 1543
    :cond_2
    if-ne p1, v4, :cond_3

    .line 1544
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mInputManager:Landroid/view/inputmethod/InputMethodManager;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 1545
    iput-boolean v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mIsKeyboardShown:Z

    goto :goto_0

    .line 1547
    :cond_3
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mInputManager:Landroid/view/inputmethod/InputMethodManager;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v2}, Landroid/widget/AutoCompleteTextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1548
    iput-boolean v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mIsKeyboardShown:Z

    goto :goto_0
.end method

.method private setRTLEditText()V
    .locals 12

    .prologue
    const/16 v11, 0xb

    const/16 v10, 0xf

    const/4 v9, -0x1

    const/4 v8, -0x2

    .line 390
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Landroid/widget/AutoCompleteTextView;->setTextDirection(I)V

    .line 391
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

    const/16 v7, 0x15

    invoke-virtual {v6, v7}, Landroid/widget/AutoCompleteTextView;->setGravity(I)V

    .line 392
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v6}, Landroid/widget/AutoCompleteTextView;->getPaddingLeft()I

    move-result v0

    .line 393
    .local v0, "left":I
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v6}, Landroid/widget/AutoCompleteTextView;->getPaddingRight()I

    move-result v4

    .line 396
    .local v4, "right":I
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v8, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 399
    .local v1, "params":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v1, v11, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 400
    invoke-virtual {v1, v10, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 401
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b0016

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v6

    iput v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 402
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b0015

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 403
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 406
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v8, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 409
    .local v3, "paramsIcon":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v3, v11, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 410
    invoke-virtual {v3, v10, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 411
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b0014

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v6

    iput v6, v3, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 413
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchLayoutOutLine:Landroid/widget/RelativeLayout;

    const v7, 0x7f080109

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 414
    .local v5, "v":Landroid/widget/ImageView;
    invoke-virtual {v5, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 416
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchLayoutOutLine:Landroid/widget/RelativeLayout;

    invoke-virtual {v6, v5}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 417
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchLayoutOutLine:Landroid/widget/RelativeLayout;

    invoke-virtual {v6, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 419
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v8, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 422
    .local v2, "params2":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v6, 0x9

    invoke-virtual {v2, v6, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 423
    invoke-virtual {v2, v10, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 424
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mClearTextBtn:Landroid/widget/ImageView;

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 425
    return-void
.end method

.method private setVisibility()V
    .locals 2

    .prologue
    .line 595
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

    .line 596
    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 597
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mClearTextBtn:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 602
    :goto_0
    return-void

    .line 599
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mClearTextBtn:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private setupMapView()V
    .locals 5

    .prologue
    .line 492
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    if-nez v0, :cond_0

    .line 493
    const v0, 0x7f080121

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    .line 494
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    sget-object v1, Lcom/google/android/maps/MapView$ReticleDrawMode;->DRAW_RETICLE_NEVER:Lcom/google/android/maps/MapView$ReticleDrawMode;

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->setReticleDrawMode(Lcom/google/android/maps/MapView$ReticleDrawMode;)V

    .line 495
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getController()Lcom/google/android/maps/MapController;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->myMapController:Lcom/google/android/maps/MapController;

    .line 497
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getController()Lcom/google/android/maps/MapController;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/android/maps/MapController;->setZoom(I)I

    .line 498
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->setBuiltInZoomControls(Z)V

    .line 500
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLatitude:I

    if-nez v0, :cond_3

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLongitude:I

    if-nez v0, :cond_3

    .line 501
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getController()Lcom/google/android/maps/MapController;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mPrelatitude:D

    iget-wide v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mPreLongitutde:D

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getPoint(DD)Lcom/google/android/maps/GeoPoint;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/MapController;->setCenter(Lcom/google/android/maps/GeoPoint;)V

    .line 506
    :goto_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$2;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->setInterface(Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView$Call;)V

    .line 518
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getOverlays()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getOverlays()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 519
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getOverlays()Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$3;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$3;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 564
    :cond_2
    return-void

    .line 503
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getController()Lcom/google/android/maps/MapController;

    move-result-object v0

    new-instance v1, Lcom/google/android/maps/GeoPoint;

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLatitude:I

    iget v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLongitude:I

    invoke-direct {v1, v2, v3}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/MapController;->setCenter(Lcom/google/android/maps/GeoPoint;)V

    goto :goto_0
.end method

.method private setupViews()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const-wide/16 v7, 0x0

    const/4 v3, 0x0

    .line 428
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mKoreaCities:[Ljava/lang/String;

    array-length v5, v4

    move v2, v3

    :goto_0
    if-ge v2, v5, :cond_0

    aget-object v1, v4, v2

    .line 429
    .local v1, "s":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mKoreaCityIds:Ljava/util/ArrayList;

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 428
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 432
    .end local v1    # "s":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v2}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v2

    if-ne v2, v9, :cond_1

    .line 433
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->setRTLEditText()V

    .line 436
    :cond_1
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v2}, Landroid/widget/AutoCompleteTextView;->setSingleLine()V

    .line 437
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v2, v3}, Landroid/widget/AutoCompleteTextView;->setCursorVisible(Z)V

    .line 438
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v2, v3}, Landroid/widget/AutoCompleteTextView;->setFocusable(Z)V

    .line 439
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->searchEditTextAccessibilityDelegate:Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {v2, v4}, Landroid/widget/AutoCompleteTextView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 441
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

    const v4, 0x7f0d0025

    invoke-virtual {p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/AutoCompleteTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 443
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getSystemResolution(Landroid/content/Context;)I

    move-result v0

    .line 445
    .local v0, "resolution":I
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f090004

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 446
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchText:Landroid/widget/TextView;

    const v4, 0x7f0d0107

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setHint(I)V

    .line 453
    :goto_1
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a006c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setHintTextColor(I)V

    .line 455
    const-string v2, "input_method"

    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/inputmethod/InputMethodManager;

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mInputManager:Landroid/view/inputmethod/InputMethodManager;

    .line 457
    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    invoke-direct {v2, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mUrlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    .line 458
    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    invoke-direct {v2, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mHttpClient:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    .line 459
    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;

    invoke-direct {v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;-><init>()V

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mParser:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherJsonParser;

    .line 461
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v4, "mapweatherzoom5"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListZoom5:Ljava/util/ArrayList;

    .line 462
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v4, "mapweatherzoom6"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListZoom6:Ljava/util/ArrayList;

    .line 463
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v4, "mapweatherzoom7"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListZoom7:Ljava/util/ArrayList;

    .line 464
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v4, "mapitem"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mAddedMapItem:Ljava/util/ArrayList;

    .line 466
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListZoom5:Ljava/util/ArrayList;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListZoom6:Ljava/util/ArrayList;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListZoom7:Ljava/util/ArrayList;

    if-eqz v2, :cond_2

    .line 467
    const-string v2, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "zl5: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListZoom5:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " zl6: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListZoom6:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " zl7: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListZoom7:Ljava/util/ArrayList;

    .line 468
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 467
    invoke-static {v2, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 471
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v4, "latitude"

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLatitude:I

    .line 472
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v4, "longitude"

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLongitude:I

    .line 473
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v4, "pre_lat"

    invoke-virtual {v2, v4, v7, v8}, Landroid/content/Intent;->getDoubleExtra(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mPrelatitude:D

    .line 474
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v4, "pre_lon"

    invoke-virtual {v2, v4, v7, v8}, Landroid/content/Intent;->getDoubleExtra(Ljava/lang/String;D)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mPreLongitutde:D

    .line 475
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v4, "tempscale"

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mTempScale:I

    .line 476
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v4, "launcher"

    const/16 v5, -0x526c

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLauncher:I

    .line 479
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v4, "pre_loc"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mPreLocation:Ljava/lang/String;

    .line 481
    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mTempScale:I

    if-ne v2, v9, :cond_5

    const v2, 0x7f02013d

    :goto_2
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mTempImage:I

    .line 484
    const-string v2, "location"

    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/location/LocationManager;

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLocationManager:Landroid/location/LocationManager;

    .line 485
    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MyLocationListener;

    const/4 v4, 0x0

    invoke-direct {v2, p0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MyLocationListener;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$1;)V

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLocationListener:Landroid/location/LocationListener;

    .line 487
    const v2, 0x7f08000c

    invoke-virtual {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mTopLayout:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;

    .line 488
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mTopLayout:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mBackKeyHandler:Landroid/os/Handler;

    invoke-virtual {v2, v4, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;->setHandler(Landroid/os/Handler;I)V

    .line 489
    return-void

    .line 448
    :cond_3
    const v2, 0xfa000

    if-ne v0, v2, :cond_4

    .line 449
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchText:Landroid/widget/TextView;

    const-string v4, " "

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    .line 450
    :cond_4
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchText:Landroid/widget/TextView;

    const v4, 0x7f0d0005

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setHint(I)V

    goto/16 :goto_1

    .line 481
    :cond_5
    const v2, 0x7f02013e

    goto :goto_2
.end method

.method private showCities(Ljava/util/ArrayList;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "citylist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;>;"
    const/4 v9, 0x0

    .line 1271
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->hideLoadingDialog()V

    .line 1273
    iput-boolean v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->positive:Z

    .line 1274
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1275
    .local v0, "b":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v2, v6, [Ljava/lang/String;

    .line 1277
    .local v2, "citynames":[Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v3, v6, :cond_0

    .line 1278
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getCity()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getState()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v3

    .line 1277
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1282
    :cond_0
    const v6, 0x7f03001a

    const/4 v7, 0x0

    invoke-static {p0, v6, v7}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 1283
    .local v5, "v":Landroid/view/View;
    const v6, 0x7f080059

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 1284
    .local v4, "title":Landroid/widget/TextView;
    const v6, 0x7f08005a

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1285
    .local v1, "cityListSize":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0d0039

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1286
    const v6, 0x7f0d003a

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {p0, v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1288
    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 1289
    iput v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mDialogSelected:I

    .line 1291
    new-instance v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$32;

    invoke-direct {v6, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$32;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    invoke-virtual {v0, v2, v9, v6}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1297
    const v6, 0x7f0d006d

    new-instance v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$33;

    invoke-direct {v7, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$33;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    invoke-virtual {v0, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1303
    const v6, 0x7f0d0009

    new-instance v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$34;

    invoke-direct {v7, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$34;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    invoke-virtual {v0, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1309
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    .line 1310
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mDialog:Landroid/app/Dialog;

    .line 1311
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mDialog:Landroid/app/Dialog;

    new-instance v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$35;

    invoke-direct {v7, p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$35;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Ljava/util/ArrayList;)V

    invoke-virtual {v6, v7}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1324
    return-void
.end method

.method private showResult(Ljava/util/ArrayList;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "citylist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;>;"
    const/4 v10, 0x0

    .line 1398
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1399
    .local v0, "b":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v7

    new-array v2, v7, [Ljava/lang/String;

    .line 1401
    .local v2, "citynames":[Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v3, v7, :cond_0

    .line 1402
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 1403
    .local v4, "sb":Ljava/lang/StringBuffer;
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getCity()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1404
    const-string v7, ", "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1405
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getState()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1406
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v2, v3

    .line 1401
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1409
    .end local v4    # "sb":Ljava/lang/StringBuffer;
    :cond_0
    iput v10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mDialogSelected:I

    .line 1411
    const v7, 0x7f03001a

    const/4 v8, 0x0

    invoke-static {p0, v7, v8}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 1412
    .local v6, "v":Landroid/view/View;
    const v7, 0x7f080059

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 1413
    .local v5, "title":Landroid/widget/TextView;
    const v7, 0x7f08005a

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1414
    .local v1, "cityListSize":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0d0039

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1415
    const v7, 0x7f0d003a

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {p0, v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1417
    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 1419
    new-instance v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$38;

    invoke-direct {v7, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$38;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    invoke-virtual {v0, v2, v10, v7}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1425
    const v7, 0x7f0d006d

    new-instance v8, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$39;

    invoke-direct {v8, p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$39;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1454
    const v7, 0x7f0d0009

    new-instance v8, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$40;

    invoke-direct {v8, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$40;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    invoke-virtual {v0, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1459
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    .line 1460
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mDialog:Landroid/app/Dialog;

    .line 1461
    return-void
.end method

.method private stopHttpThread()V
    .locals 3

    .prologue
    .line 1574
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mHttpThreads:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 1575
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 1576
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Thread;

    .line 1577
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 1580
    .end local v0    # "t":Ljava/lang/Thread;
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mHttpThreads:Ljava/util/ArrayList;

    .line 1582
    :cond_1
    return-void
.end method


# virtual methods
.method public deleteMe(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 1553
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mHttpThreads:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 1554
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 1555
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1558
    :cond_0
    return-void
.end method

.method protected isRouteDisplayed()Z
    .locals 1

    .prologue
    .line 2154
    const/4 v0, 0x0

    return v0
.end method

.method public makeDetail(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;)V
    .locals 7
    .param p1, "info"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    .prologue
    .line 1881
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->showLoadingDialog()V

    .line 1883
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->detailHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 1884
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->detailHandler:Landroid/os/Handler;

    const v1, -0x13d30

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1887
    :cond_0
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->detailitem:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    .line 1888
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mUrlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLocation()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mTempScale:I

    .line 1889
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkLanguage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 1888
    invoke-virtual {v0, v1, v2, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeUrlForGetDetailData(Ljava/lang/String;ILjava/lang/String;)Ljava/net/URL;

    move-result-object v3

    .line 1891
    .local v3, "url":Ljava/net/URL;
    if-eqz v3, :cond_2

    .line 1892
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mHttpThreads:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 1893
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mHttpThreads:Ljava/util/ArrayList;

    .line 1895
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mUrlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeHeader()Lorg/apache/http/message/HeaderGroup;

    move-result-object v4

    .line 1897
    .local v4, "headerGroup":Lorg/apache/http/message/HeaderGroup;
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mHttpClient:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$47;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$47;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    move-object v2, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->get(ILandroid/content/Context;Ljava/net/URL;Lorg/apache/http/message/HeaderGroup;Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;)Ljava/lang/Thread;

    move-result-object v6

    .line 1914
    .local v6, "t":Ljava/lang/Thread;
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1916
    .end local v4    # "headerGroup":Lorg/apache/http/message/HeaderGroup;
    .end local v6    # "t":Ljava/lang/Thread;
    :cond_2
    return-void
.end method

.method public makeDetailAdd(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;)V
    .locals 7
    .param p1, "info"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    .prologue
    .line 1919
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->showLoadingDialog()V

    .line 1921
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->detailAddHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 1922
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->detailAddHandler:Landroid/os/Handler;

    const v1, -0x13d30

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1925
    :cond_0
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->detailitem:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    .line 1926
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mUrlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLocation()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mTempScale:I

    .line 1927
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkLanguage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 1926
    invoke-virtual {v0, v1, v2, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeUrlForGetDetailData(Ljava/lang/String;ILjava/lang/String;)Ljava/net/URL;

    move-result-object v3

    .line 1929
    .local v3, "url":Ljava/net/URL;
    if-eqz v3, :cond_2

    .line 1930
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mHttpThreads:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 1931
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mHttpThreads:Ljava/util/ArrayList;

    .line 1934
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mUrlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeHeader()Lorg/apache/http/message/HeaderGroup;

    move-result-object v4

    .line 1936
    .local v4, "headerGroup":Lorg/apache/http/message/HeaderGroup;
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mHttpClient:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$48;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$48;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    move-object v2, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->get(ILandroid/content/Context;Ljava/net/URL;Lorg/apache/http/message/HeaderGroup;Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;)Ljava/lang/Thread;

    move-result-object v6

    .line 1952
    .local v6, "t":Ljava/lang/Thread;
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1954
    .end local v4    # "headerGroup":Lorg/apache/http/message/HeaderGroup;
    .end local v6    # "t":Ljava/lang/Thread;
    :cond_2
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v1, -0x1

    .line 1611
    const v0, -0x9bdc

    if-ne p1, v0, :cond_2

    .line 1612
    if-ne p2, v1, :cond_1

    .line 1613
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->detailitem:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MapCityInfo;->getLocation()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateLastSelectedLocation(Landroid/content/Context;Ljava/lang/String;)I

    .line 1614
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->setResult(ILandroid/content/Intent;)V

    .line 1615
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->finish()V

    .line 1628
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/maps/MapActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1629
    return-void

    .line 1617
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->hideLoadingDialog()V

    goto :goto_0

    .line 1619
    :cond_2
    const/16 v0, 0x4b3

    if-ne v0, p1, :cond_3

    .line 1620
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getLocationservice(Landroid/content/Context;)I

    move-result v0

    const/16 v1, 0xbbe

    if-ne v0, v1, :cond_0

    .line 1621
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->performLocation()V

    goto :goto_0

    .line 1622
    :cond_3
    const v0, 0xf22f

    if-ne p1, v0, :cond_0

    .line 1623
    if-ne p2, v1, :cond_0

    .line 1624
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->setResult(ILandroid/content/Intent;)V

    .line 1625
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->finish()V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 1979
    invoke-super {p0}, Lcom/google/android/maps/MapActivity;->onBackPressed()V

    .line 1980
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 605
    invoke-super {p0, p1}, Lcom/google/android/maps/MapActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 606
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v3, 0x780

    const/16 v2, 0x438

    .line 228
    invoke-super {p0, p1}, Lcom/google/android/maps/MapActivity;->onCreate(Landroid/os/Bundle;)V

    .line 229
    const v1, 0x7f030034

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->setContentView(I)V

    .line 231
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->createFont()V

    .line 233
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->initViews()V

    .line 234
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->setupViews()V

    .line 236
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getWindowDisplay(Landroid/content/Context;)Landroid/view/Display;

    move-result-object v0

    .line 237
    .local v0, "display":Landroid/view/Display;
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v1

    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->width:I

    .line 238
    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v1

    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->height:I

    .line 240
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->width:I

    if-le v1, v2, :cond_0

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->height:I

    if-gt v1, v3, :cond_1

    :cond_0
    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->width:I

    if-le v1, v3, :cond_2

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->height:I

    if-le v1, v2, :cond_2

    .line 241
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mIsWQHD:Z

    .line 246
    :goto_0
    invoke-super {p0}, Lcom/google/android/maps/MapActivity;->onPause()V

    .line 247
    return-void

    .line 243
    :cond_2
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mIsWQHD:Z

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1983
    const-string v0, ""

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1984
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->loadingHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1985
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->locHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1986
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->zoomHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1987
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->dataHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1988
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->detailHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1989
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->detailAddHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1990
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->moveHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1992
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    if-eqz v0, :cond_0

    .line 1993
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->removeAllMapList()V

    .line 1994
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getOverlays()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1997
    :cond_0
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    .line 1999
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    if-eqz v0, :cond_1

    .line 2000
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->clearItems()V

    .line 2002
    :cond_1
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    .line 2004
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    if-eqz v0, :cond_2

    .line 2005
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->clearItems()V

    .line 2007
    :cond_2
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mSearchMapItems:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    .line 2009
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItemsList5:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    if-eqz v0, :cond_3

    .line 2010
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItemsList5:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->clearItems()V

    .line 2012
    :cond_3
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItemsList5:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    .line 2014
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItemsList6:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    if-eqz v0, :cond_4

    .line 2015
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItemsList6:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->clearItems()V

    .line 2017
    :cond_4
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItemsList6:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    .line 2019
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItemsList7:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    if-eqz v0, :cond_5

    .line 2020
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItemsList7:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;->clearItems()V

    .line 2022
    :cond_5
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapItemsList7:Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$MapItems;

    .line 2024
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListZoom5:Ljava/util/ArrayList;

    if-eqz v0, :cond_6

    .line 2025
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListZoom5:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2027
    :cond_6
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListZoom5:Ljava/util/ArrayList;

    .line 2029
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListZoom6:Ljava/util/ArrayList;

    if-eqz v0, :cond_7

    .line 2030
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListZoom6:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2032
    :cond_7
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListZoom6:Ljava/util/ArrayList;

    .line 2034
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListZoom7:Ljava/util/ArrayList;

    if-eqz v0, :cond_8

    .line 2035
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListZoom7:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2037
    :cond_8
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mListZoom7:Ljava/util/ArrayList;

    .line 2039
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLocationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLocationListener:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 2041
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->setKeyboardVisible(Z)V

    .line 2042
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->cleanResource()V

    .line 2044
    invoke-super {p0}, Lcom/google/android/maps/MapActivity;->onDestroy()V

    .line 2045
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 2131
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v2}, Landroid/widget/AutoCompleteTextView;->isFocusable()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2132
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v2, v1}, Landroid/widget/AutoCompleteTextView;->setFocusable(Z)V

    .line 2133
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v2, v1}, Landroid/widget/AutoCompleteTextView;->setFocusableInTouchMode(Z)V

    .line 2135
    :cond_0
    const/16 v2, 0x2f

    if-ne p1, v2, :cond_2

    .line 2136
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->isSatellite()Z

    move-result v3

    if-nez v3, :cond_1

    move v0, v1

    :cond_1
    invoke-virtual {v2, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->setSatellite(Z)V

    .line 2146
    :goto_0
    return v1

    .line 2138
    :cond_2
    const/16 v2, 0x36

    if-ne p1, v2, :cond_3

    .line 2139
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->displayZoomControls(Z)V

    goto :goto_0

    .line 2143
    :cond_3
    const/4 v1, 0x4

    if-ne p1, v1, :cond_4

    .line 2144
    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->setKeyboardVisible(Z)V

    .line 2146
    :cond_4
    invoke-super {p0, p1, p2}, Lcom/google/android/maps/MapActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method public onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1957
    invoke-super {p0}, Lcom/google/android/maps/MapActivity;->onPause()V

    .line 1958
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    if-eqz v0, :cond_0

    .line 1959
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;->getZoomButtonsController()Landroid/widget/ZoomButtonsController;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ZoomButtonsController;->setVisible(Z)V

    .line 1964
    :cond_0
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mPreviousZoomLevel:I

    .line 1965
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->stopHttpThread()V

    .line 1966
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->loadingHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1967
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->locHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1968
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->zoomHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1969
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->dataHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1970
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->detailHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1971
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->detailAddHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1972
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->hideLoadingDialog()V

    .line 1973
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mToastHint:Landroid/widget/Toast;

    if-eqz v0, :cond_1

    .line 1974
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mToastHint:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 1976
    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 1593
    invoke-super {p0}, Lcom/google/android/maps/MapActivity;->onResume()V

    .line 1595
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->isLocaleRTL()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mIsLocaleRTL:Z

    .line 1597
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMapView:Lcom/sec/android/widgetapp/ap/hero/accuweather/view/MyMapView;

    if-nez v0, :cond_0

    .line 1602
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->createMapItemsHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1606
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->setEventListener()V

    .line 1607
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->setVisibility()V

    .line 1608
    return-void
.end method

.method public onSearchRequested()Z
    .locals 2

    .prologue
    .line 1587
    const-string v0, ""

    const-string v1, "MA_oSR"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1588
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->requestFocus()Z

    .line 1589
    invoke-super {p0}, Lcom/google/android/maps/MapActivity;->onSearchRequested()Z

    move-result v0

    return v0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 4
    .param p1, "hasFocus"    # Z

    .prologue
    .line 622
    invoke-super {p0, p1}, Lcom/google/android/maps/MapActivity;->onWindowFocusChanged(Z)V

    .line 624
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "oWFC : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v2}, Landroid/widget/AutoCompleteTextView;->isFocusable()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mIsKeyboardShown:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 626
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mMenuAddSearchEditText:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->isFocusable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mIsKeyboardShown:Z

    if-eqz v0, :cond_0

    .line 627
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->setKeyboardVisibleDelayHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 629
    :cond_0
    return-void
.end method

.method protected perform_Acity(Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;)V
    .locals 7
    .param p1, "i"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;

    .prologue
    .line 1329
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mUrlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    invoke-virtual {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityInfo;->getLocation()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mTempScale:I

    .line 1330
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkLanguage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 1329
    invoke-virtual {v0, v1, v2, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeUrlForGetDetailData(Ljava/lang/String;ILjava/lang/String;)Ljava/net/URL;

    move-result-object v3

    .line 1331
    .local v3, "url":Ljava/net/URL;
    if-eqz v3, :cond_1

    .line 1332
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mHttpThreads:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 1333
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mHttpThreads:Ljava/util/ArrayList;

    .line 1336
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mUrlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeHeader()Lorg/apache/http/message/HeaderGroup;

    move-result-object v4

    .line 1338
    .local v4, "headerGroup":Lorg/apache/http/message/HeaderGroup;
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mHttpClient:Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$36;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$36;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    move-object v2, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->get(ILandroid/content/Context;Ljava/net/URL;Lorg/apache/http/message/HeaderGroup;Lcom/sec/android/widgetapp/ap/hero/accuweather/http/HttpResponseHandler;)Ljava/lang/Thread;

    move-result-object v6

    .line 1354
    .local v6, "t":Ljava/lang/Thread;
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mHttpThreads:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1356
    .end local v4    # "headerGroup":Lorg/apache/http/message/HeaderGroup;
    .end local v6    # "t":Ljava/lang/Thread;
    :cond_1
    return-void
.end method

.method protected showConfirmDialog()V
    .locals 3

    .prologue
    .line 1199
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1200
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f0d0033

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 1201
    const v1, 0x7f0d002f

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 1203
    const v1, 0x7f0d006c

    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$28;

    invoke-direct {v2, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$28;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1211
    const v1, 0x7f0d0009

    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$29;

    invoke-direct {v2, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$29;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1218
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->loadingHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 1219
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mDialog:Landroid/app/Dialog;

    .line 1222
    :cond_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mDialog:Landroid/app/Dialog;

    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$30;

    invoke-direct {v2, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$30;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1227
    return-void
.end method

.method public showLoadingDialog()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 1464
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 1466
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLoadingDialog:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 1467
    const/4 v1, 0x0

    const v0, 0x7f0d001e

    .line 1468
    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$41;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$41;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    move-object v0, p0

    move v4, v3

    .line 1467
    invoke-static/range {v0 .. v5}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZLandroid/content/DialogInterface$OnCancelListener;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLoadingDialog:Landroid/app/Dialog;

    .line 1477
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLoadingDialog:Landroid/app/Dialog;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$42;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$42;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1486
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLoadingDialog:Landroid/app/Dialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 1487
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 1488
    return-void
.end method

.method public showLoadingDialog(Ljava/lang/String;)V
    .locals 6
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 1491
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 1492
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLoadingDialog:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 1493
    const/4 v1, 0x0

    new-instance v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$43;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$43;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    move-object v0, p0

    move-object v2, p1

    move v4, v3

    invoke-static/range {v0 .. v5}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZLandroid/content/DialogInterface$OnCancelListener;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLoadingDialog:Landroid/app/Dialog;

    .line 1510
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLoadingDialog:Landroid/app/Dialog;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$44;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew$44;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1527
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLoadingDialog:Landroid/app/Dialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 1528
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/detail/MapsActivityNew;->mLoadingDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 1529
    return-void
.end method
