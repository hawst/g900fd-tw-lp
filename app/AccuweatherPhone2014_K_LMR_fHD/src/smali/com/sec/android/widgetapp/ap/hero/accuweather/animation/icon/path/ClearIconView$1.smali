.class Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView$1;
.super Ljava/lang/Object;
.source "ClearIconView.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;

    .prologue
    .line 170
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 175
    invoke-virtual {p1}, Landroid/animation/Animator;->removeAllListeners()V

    .line 176
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 172
    invoke-virtual {p1}, Landroid/animation/Animator;->removeAllListeners()V

    .line 173
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 3
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    const/4 v2, 0x0

    .line 179
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->isStop:Z

    if-eqz v0, :cond_3

    .line 180
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->TranslateAni:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->TranslateAni:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->TranslateAni:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 183
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni1:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni1:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 184
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni1:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 185
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;

    const-string v1, "e_"

    iput-object v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mPreFixProperty1:Ljava/lang/String;

    .line 187
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni2:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni2:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 188
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni2:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 189
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;

    const-string v1, "e_"

    iput-object v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mPreFixProperty2:Ljava/lang/String;

    .line 191
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni3:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni3:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 192
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->UnderLineAlphaAni3:Landroid/animation/ValueAnimator;
    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 193
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;

    const-string v1, "e_"

    iput-object v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/animation/icon/path/ClearIconView;->mPreFixProperty3:Ljava/lang/String;

    .line 196
    :cond_3
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 177
    return-void
.end method
