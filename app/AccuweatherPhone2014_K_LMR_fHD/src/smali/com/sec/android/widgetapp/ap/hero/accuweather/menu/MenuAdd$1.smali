.class Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$1;
.super Ljava/lang/Object;
.source "MenuAdd.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V
    .locals 0
    .param p1, "this$0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    .prologue
    .line 268
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 7

    .prologue
    .line 270
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 272
    .local v0, "decorViewHeight":I
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 275
    .local v3, "r":Landroid/graphics/Rect;
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mRootView:Landroid/view/View;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 276
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mRootView:Landroid/view/View;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    iget v5, v3, Landroid/graphics/Rect;->bottom:I

    iget v6, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v5, v6

    sub-int v2, v4, v5

    .line 277
    .local v2, "keypadHeight":I
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    sub-int v5, v0, v2

    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mSearchLayout:Landroid/widget/LinearLayout;
    invoke-static {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/widget/LinearLayout;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v6

    sub-int/2addr v5, v6

    # setter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->mDropBoxHeight:I
    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$102(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;I)I

    .line 278
    const-string v4, ""

    const-string v5, "vto lstn "

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/widget/AutoCompleteTextView;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 281
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/widget/AutoCompleteTextView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/AutoCompleteTextView;->isFocusable()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 282
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # invokes: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->setDropdownMenuHeight()V
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)V

    .line 283
    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd$1;->this$0:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;

    # getter for: Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->searchEditText:Landroid/widget/AutoCompleteTextView;
    invoke-static {v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;->access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuAdd;)Landroid/widget/AutoCompleteTextView;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/AutoCompleteTextView;->ensureImeVisible(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 289
    .end local v2    # "keypadHeight":I
    :cond_0
    :goto_0
    return-void

    .line 286
    :catch_0
    move-exception v1

    .line 287
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
