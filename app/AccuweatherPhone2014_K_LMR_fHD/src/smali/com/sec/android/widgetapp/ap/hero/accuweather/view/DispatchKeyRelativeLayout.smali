.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;
.super Landroid/widget/RelativeLayout;
.source "DispatchKeyRelativeLayout.java"


# instance fields
.field private mBackkeyMsgCode:I

.field private mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;->mHandler:Landroid/os/Handler;

    .line 16
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;->mBackkeyMsgCode:I

    .line 20
    return-void
.end method


# virtual methods
.method public dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 28
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 29
    const-string v1, ""

    const-string v2, "========================= back key"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    .line 37
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 38
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;->mHandler:Landroid/os/Handler;

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;->mBackkeyMsgCode:I

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 39
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;->mHandler:Landroid/os/Handler;

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;->mBackkeyMsgCode:I

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 40
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 44
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z

    move-result v1

    return v1
.end method

.method public setHandler(Landroid/os/Handler;I)V
    .locals 0
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "msgCode"    # I

    .prologue
    .line 23
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;->mHandler:Landroid/os/Handler;

    .line 24
    iput p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/view/DispatchKeyRelativeLayout;->mBackkeyMsgCode:I

    .line 25
    return-void
.end method
