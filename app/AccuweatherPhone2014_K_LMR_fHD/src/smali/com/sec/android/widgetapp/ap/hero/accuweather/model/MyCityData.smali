.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;
.super Ljava/lang/Object;
.source "MyCityData.java"


# instance fields
.field protected checked:Z

.field protected date:Ljava/lang/String;

.field protected dateForTTS:Ljava/lang/String;

.field protected id:I

.field protected isCurrentLocation:Z

.field protected latitude:Ljava/lang/String;

.field protected location:Ljava/lang/String;

.field protected longitude:Ljava/lang/String;

.field protected mDst:I

.field protected name:Ljava/lang/String;

.field protected realLocation:Ljava/lang/String;

.field protected state:Ljava/lang/String;

.field protected summerTime:Ljava/lang/String;

.field protected sunRaiseTime:Ljava/lang/String;

.field protected sunSetTime:Ljava/lang/String;

.field protected tempCurrent:Ljava/lang/String;

.field protected tempHigh:Ljava/lang/String;

.field protected tempLow:Ljava/lang/String;

.field protected time:Ljava/lang/String;

.field protected timeZone:Ljava/lang/String;

.field protected weatherDesc:Ljava/lang/String;

.field protected weatherIcon:I

.field protected xmldetailinfo:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "state"    # Ljava/lang/String;
    .param p3, "location"    # Ljava/lang/String;
    .param p4, "time"    # Ljava/lang/String;
    .param p5, "date"    # Ljava/lang/String;
    .param p6, "weatherDesc"    # Ljava/lang/String;
    .param p7, "tempCurrent"    # Ljava/lang/String;
    .param p8, "tempLow"    # Ljava/lang/String;
    .param p9, "tempHigh"    # Ljava/lang/String;
    .param p10, "isCurrentLocation"    # Z
    .param p11, "weatherIcon"    # I
    .param p12, "dst"    # I
    .param p13, "realLocation"    # Ljava/lang/String;
    .param p14, "summerTime"    # Ljava/lang/String;
    .param p15, "sunRaiseTime"    # Ljava/lang/String;
    .param p16, "sunSetTime"    # Ljava/lang/String;
    .param p17, "timeZone"    # Ljava/lang/String;
    .param p18, "latitude"    # Ljava/lang/String;
    .param p19, "longitude"    # Ljava/lang/String;
    .param p20, "xmldetailinfo"    # Ljava/lang/String;

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->xmldetailinfo:Ljava/lang/String;

    .line 56
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->name:Ljava/lang/String;

    .line 57
    iput-object p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->state:Ljava/lang/String;

    .line 58
    iput-object p3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->location:Ljava/lang/String;

    .line 59
    iput-object p4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->time:Ljava/lang/String;

    .line 60
    iput-object p5, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->date:Ljava/lang/String;

    .line 61
    iput-object p6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->weatherDesc:Ljava/lang/String;

    .line 62
    iput-object p7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->tempCurrent:Ljava/lang/String;

    .line 63
    iput-object p8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->tempLow:Ljava/lang/String;

    .line 64
    iput-object p9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->tempHigh:Ljava/lang/String;

    .line 65
    iput-boolean p10, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->isCurrentLocation:Z

    .line 66
    iput p11, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->weatherIcon:I

    .line 67
    iput p12, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->mDst:I

    .line 68
    iput-object p13, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->realLocation:Ljava/lang/String;

    .line 69
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->summerTime:Ljava/lang/String;

    .line 70
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->sunRaiseTime:Ljava/lang/String;

    .line 71
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->sunSetTime:Ljava/lang/String;

    .line 72
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->timeZone:Ljava/lang/String;

    .line 73
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->latitude:Ljava/lang/String;

    .line 74
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->longitude:Ljava/lang/String;

    .line 75
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->xmldetailinfo:Ljava/lang/String;

    .line 76
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->id:I

    .line 77
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->checked:Z

    .line 78
    return-void
.end method


# virtual methods
.method public getDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->date:Ljava/lang/String;

    return-object v0
.end method

.method public getDateForTTS()Ljava/lang/String;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->dateForTTS:Ljava/lang/String;

    return-object v0
.end method

.method public getDst()I
    .locals 1

    .prologue
    .line 117
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->mDst:I

    return v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 237
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->id:I

    return v0
.end method

.method public getIsCurrentLocation()Z
    .locals 1

    .prologue
    .line 177
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->isCurrentLocation:Z

    return v0
.end method

.method public getLatitude()Ljava/lang/String;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->latitude:Ljava/lang/String;

    return-object v0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->location:Ljava/lang/String;

    return-object v0
.end method

.method public getLongitude()Ljava/lang/String;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->longitude:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getRealLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->realLocation:Ljava/lang/String;

    return-object v0
.end method

.method public getState()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->state:Ljava/lang/String;

    return-object v0
.end method

.method public getSummerTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->summerTime:Ljava/lang/String;

    return-object v0
.end method

.method public getSunRaiseTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->sunRaiseTime:Ljava/lang/String;

    return-object v0
.end method

.method public getSunSetTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->sunSetTime:Ljava/lang/String;

    return-object v0
.end method

.method public getTempCurrent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->tempCurrent:Ljava/lang/String;

    return-object v0
.end method

.method public getTempHigh()Ljava/lang/String;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->tempHigh:Ljava/lang/String;

    return-object v0
.end method

.method public getTempLow()Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->tempLow:Ljava/lang/String;

    return-object v0
.end method

.method public getTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->time:Ljava/lang/String;

    return-object v0
.end method

.method public getTimeZone()Ljava/lang/String;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->timeZone:Ljava/lang/String;

    return-object v0
.end method

.method public getWeatherDesc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->weatherDesc:Ljava/lang/String;

    return-object v0
.end method

.method public getWeatherIcon()I
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->weatherIcon:I

    return v0
.end method

.method public getXmlDetailInfo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->xmldetailinfo:Ljava/lang/String;

    return-object v0
.end method

.method public isChecked()Z
    .locals 1

    .prologue
    .line 245
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->checked:Z

    return v0
.end method

.method public setChecked(Z)V
    .locals 0
    .param p1, "checked"    # Z

    .prologue
    .line 249
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->checked:Z

    .line 250
    return-void
.end method

.method public setDate(Ljava/lang/String;)V
    .locals 0
    .param p1, "date"    # Ljava/lang/String;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->date:Ljava/lang/String;

    .line 134
    return-void
.end method

.method public setDateForTTS(Ljava/lang/String;)V
    .locals 0
    .param p1, "dateForTTS"    # Ljava/lang/String;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->dateForTTS:Ljava/lang/String;

    .line 142
    return-void
.end method

.method public setDst(I)V
    .locals 0
    .param p1, "dst"    # I

    .prologue
    .line 113
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->mDst:I

    .line 114
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 241
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->id:I

    .line 242
    return-void
.end method

.method public setIsCurrentLocation(Z)V
    .locals 0
    .param p1, "isCurrentLocation"    # Z

    .prologue
    .line 181
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->isCurrentLocation:Z

    .line 182
    return-void
.end method

.method public setLatitude(Ljava/lang/String;)V
    .locals 0
    .param p1, "latitude"    # Ljava/lang/String;

    .prologue
    .line 217
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->latitude:Ljava/lang/String;

    .line 218
    return-void
.end method

.method public setLocation(Ljava/lang/String;)V
    .locals 0
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->location:Ljava/lang/String;

    .line 102
    return-void
.end method

.method public setLongitude(Ljava/lang/String;)V
    .locals 0
    .param p1, "longitude"    # Ljava/lang/String;

    .prologue
    .line 225
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->longitude:Ljava/lang/String;

    .line 226
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->name:Ljava/lang/String;

    .line 86
    return-void
.end method

.method public setRealLocation(Ljava/lang/String;)V
    .locals 0
    .param p1, "realLocation"    # Ljava/lang/String;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->realLocation:Ljava/lang/String;

    .line 110
    return-void
.end method

.method public setState(Ljava/lang/String;)V
    .locals 0
    .param p1, "state"    # Ljava/lang/String;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->state:Ljava/lang/String;

    .line 94
    return-void
.end method

.method public setSummerTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "summerTime"    # Ljava/lang/String;

    .prologue
    .line 197
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->summerTime:Ljava/lang/String;

    .line 198
    return-void
.end method

.method public setTempCurrent(Ljava/lang/String;)V
    .locals 0
    .param p1, "tempCurrent"    # Ljava/lang/String;

    .prologue
    .line 157
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->tempCurrent:Ljava/lang/String;

    .line 158
    return-void
.end method

.method public setTempHigh(Ljava/lang/String;)V
    .locals 0
    .param p1, "tempHigh"    # Ljava/lang/String;

    .prologue
    .line 173
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->tempHigh:Ljava/lang/String;

    .line 174
    return-void
.end method

.method public setTempLow(Ljava/lang/String;)V
    .locals 0
    .param p1, "tempLow"    # Ljava/lang/String;

    .prologue
    .line 165
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->tempLow:Ljava/lang/String;

    .line 166
    return-void
.end method

.method public setTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "time"    # Ljava/lang/String;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->time:Ljava/lang/String;

    .line 126
    return-void
.end method

.method public setWeatherDesc(Ljava/lang/String;)V
    .locals 0
    .param p1, "weatherDesc"    # Ljava/lang/String;

    .prologue
    .line 149
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->weatherDesc:Ljava/lang/String;

    .line 150
    return-void
.end method

.method public setWeatherIcon(I)V
    .locals 0
    .param p1, "weatherIcon"    # I

    .prologue
    .line 189
    iput p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->weatherIcon:I

    .line 190
    return-void
.end method

.method public setXmlDetailInfo(Ljava/lang/String;)V
    .locals 0
    .param p1, "xmldetailinfo"    # Ljava/lang/String;

    .prologue
    .line 229
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->xmldetailinfo:Ljava/lang/String;

    .line 230
    return-void
.end method
