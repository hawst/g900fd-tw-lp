.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;
.super Ljava/lang/Object;
.source "UIManagerEasyMode.java"


# static fields
.field static final DATE_FORMAT_CHANGED:Ljava/lang/String; = "clock.date_format_changed"

.field private static mUIManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

.field private static sTempStringImage:[I


# instance fields
.field private isErrorMsg:Z

.field private isShowLoading:Z

.field private mCityList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;",
            ">;"
        }
    .end annotation
.end field

.field private mCtx:Landroid/content/Context;

.field private mIsOnRefreshing:Z

.field private mIsScreenOn:Z

.field private mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

.field private mResponseHandler:Landroid/os/Handler;

.field private mRoaming:I

.field private mScreenOnOffCnt:I

.field private mSelectedLocation:Ljava/lang/String;

.field private mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

.field private mTTSinfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;

.field private mUiHandler:Landroid/os/Handler;

.field private mViews:Landroid/widget/RemoteViews;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mUIManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    .line 939
    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->sTempStringImage:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f020010
        0x7f020011
        0x7f020012
        0x7f020013
        0x7f020014
        0x7f020015
        0x7f020016
        0x7f020017
        0x7f020018
        0x7f020019
        0x7f02001a
    .end array-data
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    .line 45
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSelectedLocation:Ljava/lang/String;

    .line 47
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    .line 49
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->isShowLoading:Z

    .line 51
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->isErrorMsg:Z

    .line 55
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mTTSinfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;

    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mIsScreenOn:Z

    .line 59
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    .line 61
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    .line 66
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mIsOnRefreshing:Z

    .line 67
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRoaming:I

    .line 70
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mScreenOnOffCnt:I

    .line 469
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode$1;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mUiHandler:Landroid/os/Handler;

    .line 706
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode$3;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode$3;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mResponseHandler:Landroid/os/Handler;

    .line 78
    const-string v0, ""

    const-string v1, "UIManager : create ui manager"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    .line 80
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    invoke-direct {v0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    .line 81
    return-void
.end method

.method private SetUIWeatherData(Landroid/widget/RemoteViews;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;)V
    .locals 16
    .param p1, "views"    # Landroid/widget/RemoteViews;
    .param p2, "today"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    .prologue
    .line 989
    const-string v11, ""

    const-string v12, " cUI : cnt = not handler"

    invoke-static {v11, v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 991
    if-eqz p2, :cond_8

    .line 993
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    if-eqz v11, :cond_4

    .line 994
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getTempScaleSetting()I

    move-result v10

    .line 1004
    .local v10, "toScaleSetting":I
    :goto_0
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v11

    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getCurrentTemp()F

    move-result v12

    invoke-static {v11, v10, v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v1

    .line 1005
    .local v1, "currentTemp":I
    const v11, 0x7f080043

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1006
    const v11, 0x7f080043

    const-string v12, "%d"

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 1007
    const v11, 0x7f080043

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    const/16 v13, 0x50

    const/4 v14, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v11, v12, v13, v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setOutterGlowNOutterShadowForWidget(Landroid/widget/RemoteViews;ILandroid/content/Context;IZ)V

    .line 1023
    const/4 v11, 0x1

    if-ne v10, v11, :cond_5

    .line 1024
    const v11, 0x7f080042

    const v12, 0x7f02003d

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 1031
    :goto_1
    const v11, 0x7f080042

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1034
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mTTSinfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;

    if-nez v11, :cond_0

    .line 1035
    new-instance v11, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;

    invoke-direct {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;-><init>()V

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mTTSinfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;

    .line 1037
    :cond_0
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v12

    .line 1038
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v13

    .line 1037
    invoke-static {v11, v12, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkDayOrNight(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    .line 1039
    .local v4, "isDay":Z
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mTTSinfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;

    invoke-virtual {v11, v10}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;->setmTTStoScaleSetting(I)V

    .line 1040
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mTTSinfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;

    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v12

    .line 1041
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getCurrentTemp()F

    move-result v13

    .line 1040
    invoke-static {v12, v10, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->convertTemp(IIF)I

    move-result v12

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;->setmTTStemp(Ljava/lang/String;)V

    .line 1042
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mTTSinfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;

    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getWeatherText()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;->setmTTSweatherText(Ljava/lang/String;)V

    .line 1044
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCityList:Ljava/util/ArrayList;

    if-eqz v11, :cond_1

    .line 1045
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-ge v2, v11, :cond_1

    .line 1046
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    invoke-virtual {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSelectedLocation:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 1047
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    .line 1048
    .local v5, "item":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;
    const v11, 0x7f080045

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getCityName()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 1049
    const v11, 0x7f080045

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    const/16 v13, 0x50

    const/4 v14, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v11, v12, v13, v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setOutterGlowNOutterShadowForWidget(Landroid/widget/RemoteViews;ILandroid/content/Context;IZ)V

    .line 1050
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getCurrentTemp()F

    move-result v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->setTemperature(Ljava/lang/String;)V

    .line 1051
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTimeZone()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->setTimeZone(Ljava/lang/String;)V

    .line 1052
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getTempScale()I

    move-result v11

    invoke-virtual {v5, v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->setTempScale(I)V

    .line 1053
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunRiseTime()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->setSunRiseTime(Ljava/lang/String;)V

    .line 1054
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getSunSetTime()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->setSunSetTime(Ljava/lang/String;)V

    .line 1060
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mTTSinfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getCityName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;->setmTTScityname(Ljava/lang/String;)V

    .line 1062
    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v11

    const-string v12, "cityId:current"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 1063
    const-string v11, ""

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "mSUDH(): current loc id = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getRealLocation()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1072
    .end local v2    # "i":I
    .end local v5    # "item":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;
    :cond_1
    :goto_3
    const v11, 0x7f080040

    .line 1073
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getIconNum()I

    move-result v12

    const/4 v13, 0x6

    .line 1072
    invoke-static {v12, v13, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setTodayWeatherIcon(IIZ)I

    move-result v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 1074
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getIconNum()I

    move-result v12

    const/16 v13, 0x104

    invoke-static {v11, v4, v12, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getTodayBG(Landroid/content/Context;ZII)I

    move-result v9

    .line 1075
    .local v9, "res":I
    const/4 v11, -0x1

    if-le v9, v11, :cond_2

    .line 1076
    const v11, 0x7f08003d

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v9}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 1079
    :cond_2
    const v11, 0x7f080047

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    .line 1080
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getUpdateDate()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getDateTimeStringForWidget(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 1079
    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 1081
    const v11, 0x7f080047

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    const/16 v13, 0x50

    const/4 v14, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v11, v12, v13, v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setOutterGlowNOutterShadowForWidget(Landroid/widget/RemoteViews;ILandroid/content/Context;IZ)V

    .line 1082
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mTTSinfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;->getUpdateDate()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getDateTimeStringForWidget(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;->setmTTSUpdateTime(Ljava/lang/String;)V

    .line 1084
    new-instance v3, Landroid/content/Intent;

    const-string v11, "com.sec.android.widgetapp.ap.hero.accuweather.easy.widget.action.ACTIVITY"

    invoke-direct {v3, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1085
    .local v3, "intent":Landroid/content/Intent;
    const-string v11, "cls"

    const-string v12, "com.samsung.sec.android.widgetapp.intent.action.MENU_DETAIL_GL"

    invoke-virtual {v3, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1086
    const-string v11, "flags"

    const/16 v12, -0x7530

    invoke-virtual {v3, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1087
    const-string v11, "searchlocation"

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSelectedLocation:Ljava/lang/String;

    invoke-virtual {v3, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1088
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    const/4 v12, 0x0

    const/high16 v13, 0x8000000

    invoke-static {v11, v12, v3, v13}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 1090
    .local v6, "pendingIntent":Landroid/app/PendingIntent;
    const v11, 0x7f080033

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v6}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 1092
    new-instance v7, Landroid/content/Intent;

    const-string v11, "com.sec.android.widgetapp.ap.hero.accuweather.widget.action.MANUAL_REFRESH_FOR_EASY"

    invoke-direct {v7, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1094
    .local v7, "refresh_intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    const/4 v12, 0x0

    const/high16 v13, 0x8000000

    invoke-static {v11, v12, v7, v13}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v8

    .line 1096
    .local v8, "refresh_pending":Landroid/app/PendingIntent;
    const v11, 0x7f080032

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v8}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 1097
    const v11, 0x7f080032

    const/16 v12, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1099
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->isShowLoading:Z

    if-nez v11, :cond_3

    .line 1100
    const v11, 0x7f080032

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1103
    :cond_3
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->setTTSInfo(Landroid/widget/RemoteViews;)V

    .line 1107
    .end local v1    # "currentTemp":I
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v4    # "isDay":Z
    .end local v6    # "pendingIntent":Landroid/app/PendingIntent;
    .end local v7    # "refresh_intent":Landroid/content/Intent;
    .end local v8    # "refresh_pending":Landroid/app/PendingIntent;
    .end local v9    # "res":I
    .end local v10    # "toScaleSetting":I
    :goto_4
    return-void

    .line 997
    :cond_4
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    invoke-static {v11}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getTempScaleSetting(Landroid/content/Context;)I

    move-result v10

    .restart local v10    # "toScaleSetting":I
    goto/16 :goto_0

    .line 1027
    .restart local v1    # "currentTemp":I
    :cond_5
    const v11, 0x7f080042

    const v12, 0x7f02003e

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto/16 :goto_1

    .line 1065
    .restart local v2    # "i":I
    .restart local v4    # "isDay":Z
    .restart local v5    # "item":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;
    :cond_6
    const-string v11, ""

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "mSUDH(): id = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 1045
    .end local v5    # "item":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2

    .line 1105
    .end local v1    # "currentTemp":I
    .end local v2    # "i":I
    .end local v4    # "isDay":Z
    .end local v10    # "toScaleSetting":I
    :cond_8
    const-string v11, ""

    const-string v12, "setUiWithDatabaseData(): today is null"

    invoke-static {v11, v12}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->isShowLoading:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->showLoading()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->stopLoading()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mIsOnRefreshing:Z

    return p1
.end method

.method static synthetic access$602(Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->isErrorMsg:Z

    return p1
.end method

.method private clearHandler()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 762
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mResponseHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 763
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mResponseHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 764
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mResponseHandler:Landroid/os/Handler;

    .line 766
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mUiHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 767
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 768
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mUiHandler:Landroid/os/Handler;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 769
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mUiHandler:Landroid/os/Handler;

    .line 771
    :cond_1
    return-void
.end method

.method private clearResource()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 742
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCityList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 743
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 744
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCityList:Ljava/util/ArrayList;

    .line 747
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    if-eqz v0, :cond_1

    .line 748
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    .line 751
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSelectedLocation:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 752
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSelectedLocation:Ljava/lang/String;

    .line 755
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->clearHandler()V

    .line 756
    return-void
.end method

.method private composeEmptyCityUI(ZLandroid/widget/RemoteViews;)V
    .locals 10
    .param p1, "tabtoaddcity"    # Z
    .param p2, "views"    # Landroid/widget/RemoteViews;

    .prologue
    const v9, 0x7f08004a

    const/16 v8, 0x50

    const/4 v7, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x4

    .line 816
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " composeEmptyCityUI : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 817
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCityList:Ljava/util/ArrayList;

    .line 818
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSelectedLocation:Ljava/lang/String;

    .line 820
    const v2, 0x7f080032

    invoke-virtual {p2, v2, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 821
    const v2, 0x7f080047

    invoke-virtual {p2, v2, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 822
    const v2, 0x7f080051

    invoke-virtual {p2, v2, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 823
    const v2, 0x7f080041

    invoke-virtual {p2, v2, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 825
    if-eqz p1, :cond_0

    .line 826
    const v2, 0x7f08004b

    invoke-virtual {p2, v2, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 827
    const v2, 0x7f08004b

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    invoke-static {p2, v2, v3, v8, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setOutterGlowNOutterShadowForWidget(Landroid/widget/RemoteViews;ILandroid/content/Context;IZ)V

    .line 828
    invoke-virtual {p2, v9, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 829
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    invoke-static {p2, v9, v2, v8, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setOutterGlowNOutterShadowForWidget(Landroid/widget/RemoteViews;ILandroid/content/Context;IZ)V

    .line 830
    const v2, 0x7f08004c

    invoke-virtual {p2, v2, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 831
    const v2, 0x7f08004c

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    invoke-static {p2, v2, v3, v8, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setOutterGlowNOutterShadowForWidget(Landroid/widget/RemoteViews;ILandroid/content/Context;IZ)V

    .line 832
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    invoke-virtual {v2, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->setRefreshCanceled(Z)V

    .line 834
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.widgetapp.ap.hero.accuweather.action.CITY_CNT_ZERO_FOR_EASY"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 835
    .local v0, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    invoke-static {v2, v5, v0, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 836
    .local v1, "pendingIntent":Landroid/app/PendingIntent;
    const v2, 0x7f080033

    invoke-virtual {p2, v2, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 843
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "pendingIntent":Landroid/app/PendingIntent;
    :goto_0
    const v2, 0x7f08003d

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    const/4 v4, -0x1

    const/16 v5, 0x104

    .line 844
    invoke-static {v3, v7, v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getTodayBG(Landroid/content/Context;ZII)I

    move-result v3

    .line 843
    invoke-virtual {p2, v2, v3}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 845
    const v2, 0x7f080050

    invoke-virtual {p2, v2, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 846
    const v2, 0x7f080045

    invoke-virtual {p2, v2, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 847
    const v2, 0x7f080040

    invoke-virtual {p2, v2, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 848
    const v2, 0x7f080048

    invoke-virtual {p2, v2, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 850
    invoke-direct {p0, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->setTTSInfo(Landroid/widget/RemoteViews;)V

    .line 851
    return-void

    .line 839
    :cond_0
    const v2, 0x7f08004b

    invoke-virtual {p2, v2, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 840
    invoke-virtual {p2, v9, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 841
    const v2, 0x7f08004c

    invoke-virtual {p2, v2, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_0
.end method

.method private composeNotEmptyCityUI(Ljava/lang/String;Ljava/util/ArrayList;Landroid/widget/RemoteViews;)V
    .locals 5
    .param p1, "location"    # Ljava/lang/String;
    .param p3, "views"    # Landroid/widget/RemoteViews;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;",
            ">;",
            "Landroid/widget/RemoteViews;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "cityList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;>;"
    const/4 v4, 0x0

    .line 863
    const-string v1, ""

    const-string v2, " cUI : cnt = not empty"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 864
    const/4 v0, 0x0

    .line 866
    .local v0, "needRefresh":Z
    const v1, 0x7f08003e

    invoke-virtual {p3, v1, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 867
    const v1, 0x7f080040

    invoke-virtual {p3, v1, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 868
    const v1, 0x7f080047

    invoke-virtual {p3, v1, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 869
    const v1, 0x7f080051

    invoke-virtual {p3, v1, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 870
    const v1, 0x7f080032

    invoke-virtual {p3, v1, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 872
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " cUI : cnt = not empty nr :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 874
    iput-object p2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCityList:Ljava/util/ArrayList;

    .line 876
    if-nez p1, :cond_0

    .line 877
    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSelectedLocation:Ljava/lang/String;

    .line 882
    :goto_0
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " mSelectedLocation :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSelectedLocation:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 883
    invoke-virtual {p0, p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->setUiWithDatabaseData(Landroid/widget/RemoteViews;)V

    .line 884
    return-void

    .line 879
    :cond_0
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSelectedLocation:Ljava/lang/String;

    goto :goto_0
.end method

.method private composeUi(Ljava/util/ArrayList;Landroid/widget/RemoteViews;)V
    .locals 5
    .param p2, "view"    # Landroid/widget/RemoteViews;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;",
            ">;",
            "Landroid/widget/RemoteViews;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "cityList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;>;"
    const/4 v4, 0x4

    .line 783
    if-eqz p1, :cond_1

    .line 784
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 788
    .local v0, "cityCount":I
    :goto_0
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " cUI : cnt = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->isRefreshCompleted()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 789
    if-nez v0, :cond_2

    .line 791
    const/4 v1, 0x1

    invoke-direct {p0, v1, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->composeEmptyCityUI(ZLandroid/widget/RemoteViews;)V

    .line 792
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->isRefreshCompleted()Z

    move-result v1

    if-nez v1, :cond_0

    .line 793
    const v1, 0x7f080041

    invoke-virtual {p2, v1, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 794
    const v1, 0x7f08004b

    invoke-virtual {p2, v1, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 795
    const v1, 0x7f08004a

    invoke-virtual {p2, v1, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 797
    const v1, 0x7f080045

    invoke-virtual {p2, v1, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 798
    const v1, 0x7f080050

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 799
    const v1, 0x7f08004c

    invoke-virtual {p2, v1, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 800
    const v1, 0x7f080048

    invoke-virtual {p2, v1, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 806
    :cond_0
    :goto_1
    return-void

    .line 786
    .end local v0    # "cityCount":I
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "cityCount":I
    goto :goto_0

    .line 803
    :cond_2
    if-lez v0, :cond_0

    .line 804
    const/4 v1, 0x0

    invoke-direct {p0, v1, p1, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->composeNotEmptyCityUI(Ljava/lang/String;Ljava/util/ArrayList;Landroid/widget/RemoteViews;)V

    goto :goto_1
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 90
    const-string v0, ""

    const-string v1, "getInstance"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mUIManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    if-nez v0, :cond_0

    .line 92
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mUIManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    .line 95
    :cond_0
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mUIManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    return-object v0
.end method

.method private hideError(Landroid/widget/RemoteViews;)V
    .locals 2
    .param p1, "views"    # Landroid/widget/RemoteViews;

    .prologue
    .line 699
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->isRefreshCompleted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 700
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->setRefreshCompleted(Z)V

    .line 702
    :cond_0
    const v0, 0x7f08004d

    const/16 v1, 0x8

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 703
    const v0, 0x7f08003e

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 704
    return-void
.end method

.method private makeTempStringImage(I)[I
    .locals 8
    .param p1, "temp"    # I

    .prologue
    .line 961
    const/4 v0, 0x0

    .line 962
    .local v0, "isMinus":Z
    if-gez p1, :cond_0

    .line 963
    const/4 v0, 0x1

    .line 964
    const-string v6, ""

    const-string v7, "value = 10"

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 966
    :cond_0
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    .line 967
    .local v4, "tempString":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    new-array v1, v6, [I

    .line 968
    .local v1, "result":[I
    const/4 v3, 0x0

    .line 969
    .local v3, "startIndex":I
    if-eqz v0, :cond_1

    .line 970
    const/4 v3, 0x1

    .line 971
    const/4 v6, 0x0

    const/16 v7, 0xa

    aput v7, v1, v6

    .line 974
    :cond_1
    :goto_0
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v3, v6, :cond_2

    .line 975
    add-int/lit8 v6, v3, 0x1

    invoke-virtual {v4, v3, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 976
    .local v2, "s":Ljava/lang/String;
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 977
    .local v5, "value":I
    aput v5, v1, v3

    .line 974
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 979
    .end local v2    # "s":Ljava/lang/String;
    .end local v5    # "value":I
    :cond_2
    return-object v1
.end method

.method private makeViews(Z)V
    .locals 3
    .param p1, "bForceMake"    # Z

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    .line 129
    :cond_0
    const-string v0, ""

    const-string v1, "make widget view!!! "

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f030015

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    .line 135
    :cond_1
    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRoaming:I

    if-nez v0, :cond_2

    .line 136
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->handleConnectivityUpdate(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRoaming:I

    .line 137
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "uV mIR : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRoaming:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    :cond_2
    return-void

    .line 136
    :cond_3
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private setTTSInfo(Landroid/widget/RemoteViews;)V
    .locals 5
    .param p1, "views"    # Landroid/widget/RemoteViews;

    .prologue
    .line 1194
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mTTSinfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;

    if-nez v3, :cond_0

    .line 1195
    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;

    invoke-direct {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;-><init>()V

    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mTTSinfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;

    .line 1197
    :cond_0
    if-eqz p1, :cond_3

    .line 1198
    sget v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;->DESCRIPTION_TYPE_EASY:I

    .line 1200
    .local v0, "descType":I
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCityList:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ge v3, v4, :cond_2

    .line 1201
    :cond_1
    sget v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;->DESCRIPTION_TYPE_EMPTY_CITY:I

    or-int/2addr v0, v3

    .line 1204
    :cond_2
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mTTSinfo:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    invoke-virtual {v3, v4, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TTSinfo;->getDescription(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 1205
    .local v2, "sb":Ljava/lang/String;
    const v3, 0x7f080033

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1210
    .end local v0    # "descType":I
    .end local v2    # "sb":Ljava/lang/String;
    :cond_3
    :goto_0
    return-void

    .line 1206
    .restart local v0    # "descType":I
    :catch_0
    move-exception v1

    .line 1207
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method private setTTSInfo(Landroid/widget/RemoteViews;I)V
    .locals 2
    .param p1, "views"    # Landroid/widget/RemoteViews;
    .param p2, "res"    # I

    .prologue
    .line 1178
    if-eqz p1, :cond_1

    .line 1179
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCityList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_2

    :cond_0
    if-lez p2, :cond_2

    .line 1180
    const v0, 0x7f080033

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    .line 1181
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1180
    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    .line 1186
    :cond_1
    :goto_0
    return-void

    .line 1183
    :cond_2
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->setTTSInfo(Landroid/widget/RemoteViews;)V

    goto :goto_0
.end method

.method private showError(Landroid/widget/RemoteViews;IZ)V
    .locals 7
    .param p1, "views"    # Landroid/widget/RemoteViews;
    .param p2, "res"    # I
    .param p3, "gpsOffMessage"    # Z

    .prologue
    const v6, 0x7f08004f

    const v5, 0x7f08004e

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 657
    const v0, 0x7f080041

    invoke-virtual {p1, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 658
    const v0, 0x7f08003e

    invoke-virtual {p1, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 659
    const v0, 0x7f08004a

    invoke-virtual {p1, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 660
    const v0, 0x7f08004b

    invoke-virtual {p1, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 661
    const v0, 0x7f08004c

    const/4 v1, 0x4

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 662
    const v0, 0x7f080045

    invoke-virtual {p1, v0, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 663
    const v0, 0x7f080050

    invoke-virtual {p1, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 664
    const v0, 0x7f080040

    invoke-virtual {p1, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 665
    const v0, 0x7f080032

    invoke-virtual {p1, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 667
    const v0, 0x7f08004d

    invoke-virtual {p1, v0, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 669
    if-ne p3, v4, :cond_1

    .line 670
    invoke-virtual {p1, v6, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 671
    invoke-virtual {p1, v5, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 672
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    invoke-virtual {v0, p2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v6, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 673
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    const/16 v1, 0x50

    invoke-static {p1, v6, v0, v1, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setOutterGlowNOutterShadowForWidget(Landroid/widget/RemoteViews;ILandroid/content/Context;IZ)V

    .line 680
    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->setTTSInfo(Landroid/widget/RemoteViews;I)V

    .line 681
    iput-boolean v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->isErrorMsg:Z

    .line 683
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mResponseHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 684
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mResponseHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode$2;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;)V

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 691
    :cond_0
    return-void

    .line 675
    :cond_1
    invoke-virtual {p1, v5, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 676
    invoke-virtual {p1, v6, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 677
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    invoke-virtual {v0, p2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v5, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 678
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    const/16 v1, 0x50

    invoke-static {p1, v5, v0, v1, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setOutterGlowNOutterShadowForWidget(Landroid/widget/RemoteViews;ILandroid/content/Context;IZ)V

    goto :goto_0
.end method

.method private showLoading()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1113
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->isShowLoading:Z

    .line 1114
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    invoke-static {v2}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 1115
    .local v0, "appWidgetManager":Landroid/appwidget/AppWidgetManager;
    new-instance v2, Landroid/content/ComponentName;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    const-class v4, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/WeatherClock;

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v2}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v1

    .line 1116
    .local v1, "ids":[I
    invoke-direct {p0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->makeViews(Z)V

    .line 1117
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    const v3, 0x7f080032

    const/16 v4, 0x8

    invoke-virtual {v2, v3, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1118
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    const v3, 0x7f080034

    invoke-virtual {v2, v3, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1119
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    invoke-virtual {v0, v1, v2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    .line 1121
    return-void
.end method

.method private showLocating(Landroid/widget/RemoteViews;)V
    .locals 8
    .param p1, "views"    # Landroid/widget/RemoteViews;

    .prologue
    const v7, 0x7f0d0038

    const v6, 0x7f080050

    const/4 v5, 0x0

    const/16 v2, 0x8

    .line 719
    const v0, 0x7f080041

    invoke-virtual {p1, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 720
    const v0, 0x7f08003e

    invoke-virtual {p1, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 721
    const v0, 0x7f080049

    invoke-virtual {p1, v0, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 722
    const v0, 0x7f08004a

    invoke-virtual {p1, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 723
    const v0, 0x7f08004b

    invoke-virtual {p1, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 724
    const v0, 0x7f08004c

    const/4 v1, 0x4

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 725
    const v0, 0x7f080045

    invoke-virtual {p1, v0, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 726
    invoke-virtual {p1, v6, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 727
    const v0, 0x7f080032

    invoke-virtual {p1, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 730
    const v0, 0x7f08003d

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    const/4 v2, 0x1

    const/4 v3, -0x1

    const/16 v4, 0x104

    .line 731
    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getTodayBG(Landroid/content/Context;ZII)I

    move-result v1

    .line 730
    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 732
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    .line 733
    invoke-virtual {v0, v7}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 732
    invoke-virtual {p1, v6, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 734
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    invoke-virtual {v0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->setRefreshCompleted(Z)V

    .line 735
    invoke-direct {p0, p1, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->setTTSInfo(Landroid/widget/RemoteViews;I)V

    .line 736
    return-void
.end method

.method private startActivity(Landroid/content/Intent;)V
    .locals 11
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 508
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    invoke-static {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getCurrentCity(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-nez v8, :cond_0

    .line 509
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    new-instance v9, Landroid/content/Intent;

    const-string v10, "com.sec.android.widgetapp.ap.hero.accuweather.widget.action.APPWIDGET_UPDATE"

    invoke-direct {v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v9}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 556
    :goto_0
    return-void

    .line 511
    :cond_0
    const/4 v4, 0x0

    .line 512
    .local v4, "result":I
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    if-nez v8, :cond_1

    .line 513
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    invoke-static {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSettingsForMain(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    .line 514
    :cond_1
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    if-eqz v8, :cond_2

    .line 515
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getAutoRefreshTime()I

    move-result v4

    .line 517
    :cond_2
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    invoke-static {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->isShowChargerPopup(Landroid/content/Context;)Z

    move-result v3

    .line 518
    .local v3, "isShowCharger":Z
    const-string v8, ""

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "result:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " isShowCharger:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 520
    if-eqz v4, :cond_3

    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    .line 521
    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f090005

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v8

    if-nez v8, :cond_3

    if-eqz v3, :cond_3

    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    .line 522
    invoke-static {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->isWifiOnly(Landroid/content/Context;)Z

    move-result v8

    if-nez v8, :cond_3

    sget-boolean v8, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isVerizon:Z

    if-nez v8, :cond_3

    .line 524
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "searchlocation"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 525
    .local v5, "searchlocation":Ljava/lang/String;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 526
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v8, "ACCOUNTING_DIALOG_FROM"

    const-string v9, "FROM_TOUCH_WIDGET_EASY"

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 528
    const-string v8, "AUTOREFRESH_SETTING"

    invoke-virtual {v0, v8, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 529
    const-string v8, "SELECTED_INDEX_EASY"

    invoke-virtual {v0, v8, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 530
    new-instance v6, Landroid/content/Intent;

    const-string v8, "android.intent.action.MAIN"

    invoke-direct {v6, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 531
    .local v6, "sintent":Landroid/content/Intent;
    invoke-virtual {v6, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 532
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    const-class v9, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;

    invoke-virtual {v6, v8, v9}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 533
    const/high16 v8, 0x10000000

    invoke-virtual {v6, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 535
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    invoke-virtual {v8, v6}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 539
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v5    # "searchlocation":Ljava/lang/String;
    .end local v6    # "sintent":Landroid/content/Intent;
    :cond_3
    if-nez v4, :cond_4

    if-eqz v3, :cond_4

    .line 540
    const-string v8, ""

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "TOUCH_WIDGET : r = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", isShow = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 541
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    const/4 v9, 0x0

    invoke-static {v8, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setShowChargerPopup(Landroid/content/Context;Z)V

    .line 543
    :cond_4
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "cls"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 544
    .local v1, "cls":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "flags"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 545
    .local v2, "flags":I
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "searchlocation"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 546
    .restart local v5    # "searchlocation":Ljava/lang/String;
    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 547
    .local v7, "start_activity":Landroid/content/Intent;
    const-string v8, "flags"

    invoke-virtual {v7, v8, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 548
    const-string v8, "widget_mode"

    const/16 v9, 0xfa1

    invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 549
    const-string v8, "searchlocation"

    invoke-virtual {v7, v8, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 550
    const v8, 0x14008000

    invoke-virtual {v7, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 553
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    invoke-virtual {v8, v7}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method private stopLoading()V
    .locals 10

    .prologue
    const v9, 0x7f080032

    const v8, 0x7f080050

    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 1127
    iput-boolean v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->isShowLoading:Z

    .line 1128
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    invoke-static {v3}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 1129
    .local v0, "appWidgetManager":Landroid/appwidget/AppWidgetManager;
    new-instance v3, Landroid/content/ComponentName;

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    const-class v5, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/WeatherClock;

    invoke-direct {v3, v4, v5}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v3}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v1

    .line 1130
    .local v1, "ids":[I
    const/4 v2, -0x1

    .line 1131
    .local v2, "mWeatherCount":I
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getAllCityList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_0

    .line 1132
    const/4 v2, 0x0

    .line 1137
    :goto_0
    invoke-direct {p0, v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->makeViews(Z)V

    .line 1139
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    const v4, 0x7f080034

    invoke-virtual {v3, v4, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1141
    if-lez v2, :cond_1

    .line 1142
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    invoke-virtual {v3, v9, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1143
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    invoke-virtual {v3, v8, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1152
    :goto_1
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    invoke-virtual {v0, v1, v3}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    .line 1153
    return-void

    .line 1135
    :cond_0
    const/4 v2, 0x1

    goto :goto_0

    .line 1145
    :cond_1
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    invoke-virtual {v3, v9, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1146
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->isRefreshCompleted()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1147
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    invoke-virtual {v3, v8, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_1

    .line 1149
    :cond_2
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    invoke-virtual {v3, v8, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_1
.end method

.method private stopLoading(Landroid/widget/RemoteViews;I)V
    .locals 3
    .param p1, "views"    # Landroid/widget/RemoteViews;
    .param p2, "weatherCount"    # I

    .prologue
    const v2, 0x7f080032

    const/16 v1, 0x8

    .line 1162
    const v0, 0x7f080034

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1163
    const v0, 0x7f080050

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1164
    if-lez p2, :cond_0

    .line 1165
    const/4 v0, 0x0

    invoke-virtual {p1, v2, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1170
    :goto_0
    return-void

    .line 1167
    :cond_0
    invoke-virtual {p1, v2, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_0
.end method


# virtual methods
.method public isExistWidget([I)Z
    .locals 2
    .param p1, "ids"    # [I

    .prologue
    .line 105
    array-length v0, p1

    if-gtz v0, :cond_0

    .line 106
    const-string v0, ""

    const-string v1, "The widget does not exist in idle!!"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    const/4 v0, 0x0

    .line 109
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onDisableWidget()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 628
    const-string v0, ""

    const-string v1, "onDisableWidget"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 629
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->clearResource()V

    .line 631
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    invoke-static {v0, v2, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;->setWidgetAtDaemon(Landroid/content/Context;ZZ)V

    .line 633
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->isShowLoading:Z

    .line 634
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    if-eqz v0, :cond_0

    .line 635
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->onDestroy()V

    .line 636
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    .line 639
    :cond_0
    sget-object v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mUIManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    if-eqz v0, :cond_1

    .line 640
    sput-object v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mUIManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;

    .line 643
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    if-eqz v0, :cond_2

    .line 644
    iput-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    .line 647
    :cond_2
    return-void
.end method

.method public onEnableWidget()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 116
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->setContext(Landroid/content/Context;)V

    .line 117
    invoke-direct {p0, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->makeViews(Z)V

    .line 118
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    invoke-static {v0, v2, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;->setWidgetAtDaemon(Landroid/content/Context;ZZ)V

    .line 120
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mIsOnRefreshing:Z

    .line 122
    return-void
.end method

.method public onRecieveWidget(Landroid/content/Intent;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 22
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "awm"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "ids"    # [I

    .prologue
    .line 150
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    .line 151
    .local v7, "action":Ljava/lang/String;
    const-string v18, "easymode"

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    .line 153
    .local v6, "LanucherMode":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkLanucherMode(Landroid/content/Context;)Z

    move-result v18

    if-nez v18, :cond_0

    .line 154
    const-string v18, ""

    const-string v19, "Lanucher is not easy!!"

    invoke-static/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    :cond_0
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->makeViews(Z)V

    .line 160
    :try_start_0
    const-string v18, "com.sec.android.widgetapp.ap.hero.accuweather.widget.action.APPWIDGET_UPDATE"

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_1

    const-string v18, "com.android.launcher.action.EASY_MODE_CHANGE"

    .line 161
    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_1

    const-string v18, "android.intent.action.TIME_SET"

    .line 162
    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_1

    const-string v18, "com.sec.android.widgetapp.ap.hero.accuweather.action.CONFIGURATION_CHANGED"

    .line 163
    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_7

    .line 165
    :cond_1
    const-string v18, "com.sec.android.widgetapp.ap.hero.accuweather.widget.action.APPWIDGET_UPDATE"

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 166
    const-string v18, "FROMSURFACE"

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v12

    .line 167
    .local v12, "isRecievedUpdateEvtFromSurface":Z
    if-nez v12, :cond_2

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->isErrorMsg:Z

    move/from16 v18, v0

    if-eqz v18, :cond_4

    .line 168
    :cond_2
    const-string v18, ""

    const-string v19, "update evt from surface"

    invoke-static/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    .end local v12    # "isRecievedUpdateEvtFromSurface":Z
    :cond_3
    :goto_0
    return-void

    .line 173
    :cond_4
    const-string v18, "com.android.launcher.action.EASY_MODE_CHANGE"

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_5

    if-eqz v6, :cond_5

    .line 174
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    move-object/from16 v18, v0

    new-instance v19, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    move-object/from16 v20, v0

    const-class v21, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockService;

    invoke-direct/range {v19 .. v21}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual/range {v18 .. v19}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 177
    :cond_5
    const/16 v18, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->makeViews(Z)V

    .line 178
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getCurrentCity(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v9

    .line 179
    .local v9, "cityList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;>;"
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v18

    if-lez v18, :cond_6

    .line 180
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSelectedLocation:Ljava/lang/String;

    .line 182
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v9, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->composeUi(Ljava/util/ArrayList;Landroid/widget/RemoteViews;)V

    .line 183
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    move-object/from16 v18, v0

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    .line 184
    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCityList:Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 464
    .end local v9    # "cityList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;>;"
    :catch_0
    move-exception v10

    .line 465
    .local v10, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v10}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 185
    .end local v10    # "e":Ljava/lang/NullPointerException;
    :cond_7
    :try_start_1
    const-string v18, "com.sec.android.widgetapp.ap.hero.accuweather.action.CHANGE_SETTING"

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_9

    .line 186
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSettingsForMain(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    .line 187
    const/16 v18, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->makeViews(Z)V

    .line 188
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->isShowLoading:Z

    move/from16 v18, v0

    if-eqz v18, :cond_8

    .line 189
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->stopLoading()V

    .line 191
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getCurrentCity(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v13

    .line 192
    .local v13, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v13, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->composeUi(Ljava/util/ArrayList;Landroid/widget/RemoteViews;)V

    .line 193
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    move-object/from16 v18, v0

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    goto/16 :goto_0

    .line 194
    .end local v13    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;>;"
    :cond_9
    const-string v18, "com.sec.android.widgetapp.ap.hero.accuweather.widget.action.MANUAL_REFRESH_FOR_EASY"

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_d

    .line 195
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getAllCityList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCityList:Ljava/util/ArrayList;

    .line 196
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCityList:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    if-nez v18, :cond_a

    .line 197
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    move-object/from16 v18, v0

    new-instance v19, Landroid/content/Intent;

    const-string v20, "com.sec.android.widgetapp.ap.hero.accuweather.widget.action.APPWIDGET_UPDATE"

    invoke-direct/range {v19 .. v20}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v19}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 199
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCityList:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v18

    if-lez v18, :cond_c

    .line 200
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->isShowLoading:Z

    move/from16 v18, v0

    if-nez v18, :cond_b

    .line 201
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->showLoading()V

    .line 203
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v18, v0

    if-eqz v18, :cond_3

    .line 204
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mUiHandler:Landroid/os/Handler;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v18 .. v21}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->doManualRefresh(ILandroid/os/Handler;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 208
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    move-object/from16 v18, v0

    new-instance v19, Landroid/content/Intent;

    const-string v20, "com.sec.android.widgetapp.ap.hero.accuweather.widget.action.APPWIDGET_UPDATE"

    invoke-direct/range {v19 .. v20}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v19}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 211
    :cond_d
    const-string v18, "com.sec.android.widgetapp.ap.hero.accuweather.widget.action.WEATHER_SCREEN_OFF"

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_e

    .line 212
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mIsScreenOn:Z

    .line 213
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->stopLoading()V

    .line 214
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mScreenOnOffCnt:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mScreenOnOffCnt:I

    rem-int/lit8 v18, v18, 0xa

    if-nez v18, :cond_3

    .line 215
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    move-object/from16 v18, v0

    new-instance v19, Landroid/content/Intent;

    const-string v20, "com.sec.android.widgetapp.ap.hero.accuweather.widget.action.APPWIDGET_UPDATE"

    invoke-direct/range {v19 .. v20}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v19}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 216
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mScreenOnOffCnt:I

    goto/16 :goto_0

    .line 218
    :cond_e
    const-string v18, "com.sec.android.widgetapp.ap.hero.accuweather.widget.action.WEATHER_SCREEN_ON"

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_10

    .line 219
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mIsScreenOn:Z

    .line 220
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-object/from16 v18, v0

    if-nez v18, :cond_f

    .line 221
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSettingsForMain(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    .line 223
    :cond_f
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mScreenOnOffCnt:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mScreenOnOffCnt:I

    rem-int/lit8 v18, v18, 0xa

    if-nez v18, :cond_3

    .line 224
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    move-object/from16 v18, v0

    new-instance v19, Landroid/content/Intent;

    const-string v20, "com.sec.android.widgetapp.ap.hero.accuweather.widget.action.APPWIDGET_UPDATE"

    invoke-direct/range {v19 .. v20}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v19}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 225
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mScreenOnOffCnt:I

    goto/16 :goto_0

    .line 227
    :cond_10
    const-string v18, "com.sec.android.widgetapp.ap.hero.accuweather.action.GET_CURRENT_LOCATION_START"

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_12

    .line 228
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v18, v0

    if-eqz v18, :cond_11

    .line 229
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->showLocating(Landroid/widget/RemoteViews;)V

    .line 230
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    move-object/from16 v18, v0

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    goto/16 :goto_0

    .line 232
    :cond_11
    const-string v18, ""

    const-string v19, "weather clock is null"

    invoke-static/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 234
    :cond_12
    const-string v18, "com.sec.android.widgetapp.ap.hero.accuweather.action.GET_CURRENT_LOCATION_OK"

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_18

    .line 235
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v18, v0

    if-eqz v18, :cond_16

    .line 236
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSettingsForMain(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    .line 237
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getRefreshCnt()I

    move-result v18

    if-gtz v18, :cond_15

    .line 238
    const/16 v18, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->makeViews(Z)V

    .line 239
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->isShowLoading:Z

    move/from16 v18, v0

    if-eqz v18, :cond_13

    .line 240
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->stopLoading()V

    .line 241
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->isRefreshCompleted()Z

    move-result v18

    if-nez v18, :cond_14

    .line 242
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->setRefreshCompleted(Z)V

    .line 244
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getCurrentCity(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v9

    .line 245
    .restart local v9    # "cityList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v9, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->composeUi(Ljava/util/ArrayList;Landroid/widget/RemoteViews;)V

    .line 246
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    move-object/from16 v18, v0

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    goto/16 :goto_0

    .line 248
    .end local v9    # "cityList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;>;"
    :cond_15
    const-string v18, ""

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "ACTION_GET_CURRENT_LOCATION_OK!! : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v20, v0

    .line 250
    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getRefreshCnt()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 248
    invoke-static/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 253
    :cond_16
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->isShowLoading:Z

    move/from16 v18, v0

    if-eqz v18, :cond_17

    .line 254
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->stopLoading()V

    .line 256
    :cond_17
    const-string v18, ""

    const-string v19, "weather clock is null"

    invoke-static/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 259
    :cond_18
    const-string v18, "com.sec.android.widgetapp.ap.hero.accuweather.action.GET_CURRENT_LOCATION_EXCEPTION"

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_19

    const-string v18, "com.sec.android.widgetapp.ap.hero.accuweather.action.GET_CURRENT_LOCATION_ERROR"

    .line 260
    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_19

    const-string v18, "com.sec.android.widgetapp.ap.hero.accuweather.action.GET_CURRENT_LOCATION_PROVIDER_ERR"

    .line 261
    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_1c

    .line 262
    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v18, v0

    if-eqz v18, :cond_1a

    .line 263
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-object/from16 v18, v0

    if-nez v18, :cond_1a

    .line 264
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSettingsForMain(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    .line 268
    :cond_1a
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->isShowLoading:Z

    move/from16 v18, v0

    if-eqz v18, :cond_1b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v18, v0

    if-eqz v18, :cond_1b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v18, v0

    .line 269
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getRefreshCnt()I

    move-result v18

    if-gtz v18, :cond_1b

    .line 270
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->stopLoading()V

    .line 272
    :cond_1b
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->isErrorMsg:Z

    move/from16 v18, v0

    if-nez v18, :cond_3

    .line 273
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    move-object/from16 v18, v0

    const v19, 0x7f0d0021

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->showError(Landroid/widget/RemoteViews;IZ)V

    .line 274
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    move-object/from16 v18, v0

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    goto/16 :goto_0

    .line 276
    :cond_1c
    const-string v18, "com.sec.android.widgetapp.ap.hero.accuweather.action.DISABLE_LOCATION_SERVICES"

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_1f

    .line 277
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v18, v0

    if-eqz v18, :cond_1d

    .line 278
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v18, v0

    const/16 v19, 0x300

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->setRefreshCnt(I)V

    .line 280
    :cond_1d
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->isShowLoading:Z

    move/from16 v18, v0

    if-eqz v18, :cond_1e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v18, v0

    if-eqz v18, :cond_1e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v18, v0

    .line 281
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getRefreshCnt()I

    move-result v18

    if-gtz v18, :cond_1e

    .line 282
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->stopLoading()V

    .line 284
    :cond_1e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    move-object/from16 v18, v0

    const v19, 0x7f0d00ba

    const/16 v20, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->showError(Landroid/widget/RemoteViews;IZ)V

    .line 285
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    move-object/from16 v18, v0

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    goto/16 :goto_0

    .line 286
    :cond_1f
    const-string v18, "com.sec.android.widgetapp.ap.hero.accuweather.action.GET_CURRENT_LOCATION_NETWORK_FAILED"

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_24

    .line 287
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v18, v0

    if-eqz v18, :cond_22

    .line 288
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v18, v0

    const/16 v19, 0x300

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->setRefreshCnt(I)V

    .line 289
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-object/from16 v18, v0

    if-nez v18, :cond_20

    .line 290
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSettingsForMain(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    .line 293
    :cond_20
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->isShowLoading:Z

    move/from16 v18, v0

    if-eqz v18, :cond_21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getRefreshCnt()I

    move-result v18

    if-gtz v18, :cond_21

    .line 294
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->stopLoading()V

    .line 296
    :cond_21
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->setRefreshCompleted(Z)V

    .line 297
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    move-object/from16 v18, v0

    const v19, 0x7f0d001f

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->showError(Landroid/widget/RemoteViews;IZ)V

    .line 298
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    move-object/from16 v18, v0

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    goto/16 :goto_0

    .line 300
    :cond_22
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->isShowLoading:Z

    move/from16 v18, v0

    if-eqz v18, :cond_23

    .line 301
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->stopLoading()V

    .line 303
    :cond_23
    const-string v18, ""

    const-string v19, "weather clock is null"

    invoke-static/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 305
    :cond_24
    const-string v18, "com.sec.android.widgetapp.ap.hero.accuweather.action.ACTION_STOP_ERROR_MSG"

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_26

    .line 306
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSettingsForMain(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    .line 307
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getCurrentCity(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v9

    .line 308
    .restart local v9    # "cityList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;>;"
    const/16 v18, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->makeViews(Z)V

    .line 309
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->isShowLoading:Z

    move/from16 v18, v0

    if-eqz v18, :cond_25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v18, v0

    if-eqz v18, :cond_25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v18, v0

    .line 310
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getRefreshCnt()I

    move-result v18

    if-gtz v18, :cond_25

    .line 311
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->stopLoading()V

    .line 313
    :cond_25
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->hideError(Landroid/widget/RemoteViews;)V

    .line 314
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v9, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->composeUi(Ljava/util/ArrayList;Landroid/widget/RemoteViews;)V

    .line 315
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    move-object/from16 v18, v0

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    goto/16 :goto_0

    .line 316
    .end local v9    # "cityList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;>;"
    :cond_26
    const-string v18, "com.sec.android.widgetapp.ap.hero.accuweather.action.GET_DATA_NETWORK_ERROR"

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_2c

    .line 317
    const-string v18, "type"

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v17

    .line 318
    .local v17, "type":I
    const-string v18, ""

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "type : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v18, v0

    if-eqz v18, :cond_2a

    .line 320
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->isShowLoading:Z

    move/from16 v18, v0

    if-eqz v18, :cond_27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getRefreshCnt()I

    move-result v18

    if-gtz v18, :cond_27

    .line 321
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->stopLoading()V

    .line 323
    :cond_27
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-object/from16 v18, v0

    if-nez v18, :cond_28

    .line 324
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSettingsForMain(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    .line 326
    :cond_28
    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_29

    .line 327
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    move-object/from16 v18, v0

    const v19, 0x7f0d001f

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->showError(Landroid/widget/RemoteViews;IZ)V

    .line 332
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    move-object/from16 v18, v0

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    goto/16 :goto_0

    .line 329
    :cond_29
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    move-object/from16 v18, v0

    const v19, 0x7f0d0020

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->showError(Landroid/widget/RemoteViews;IZ)V

    goto :goto_1

    .line 334
    :cond_2a
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->isShowLoading:Z

    move/from16 v18, v0

    if-eqz v18, :cond_2b

    .line 335
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->stopLoading()V

    .line 337
    :cond_2b
    const-string v18, ""

    const-string v19, "weather clock is null"

    invoke-static/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 339
    .end local v17    # "type":I
    :cond_2c
    const-string v18, "com.sec.android.widgetapp.ap.hero.accuweatherdaemon.action.SYNC_DATA_WITH_DAEMON"

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_2f

    .line 340
    const-string v18, "MENUNUM"

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v14

    .line 341
    .local v14, "menunum":I
    const/16 v18, 0x2

    move/from16 v0, v18

    if-ne v14, v0, :cond_2e

    .line 342
    const-string v18, "AUTOREFRESH"

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 343
    .local v4, "DaemonRefresh":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-static {v0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;->setDaemonAccuRefresh(Landroid/content/Context;I)V

    .line 348
    .end local v4    # "DaemonRefresh":I
    :cond_2d
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    move-object/from16 v18, v0

    new-instance v19, Landroid/content/Intent;

    const-string v20, "com.sec.android.widgetapp.ap.hero.accuweather.widget.action.APPWIDGET_UPDATE"

    invoke-direct/range {v19 .. v20}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v19}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 344
    :cond_2e
    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v14, v0, :cond_2d

    .line 345
    const-string v18, "TEMP"

    const/16 v19, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 346
    .local v5, "DaemonTemp":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-static {v0, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/DaemonInterface;->setDaemonTemp(Landroid/content/Context;I)V

    goto :goto_2

    .line 349
    .end local v5    # "DaemonTemp":I
    .end local v14    # "menunum":I
    :cond_2f
    const-string v18, "com.sec.android.widgetapp.ap.hero.accuweather.easy.widget.action.ACTIVITY"

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_30

    .line 350
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 351
    :cond_30
    const-string v18, "com.sec.android.widgetapp.ap.hero.accuweather.action.CITY_CNT_ZERO_FOR_EASY"

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_37

    .line 352
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->isErrorMsg:Z

    move/from16 v18, v0

    if-eqz v18, :cond_31

    .line 353
    const-string v18, ""

    const-string v19, "showing msg"

    invoke-static/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 356
    :cond_31
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f090011

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v18

    if-eqz v18, :cond_32

    .line 357
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getCurrentCity(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v18

    if-nez v18, :cond_3

    .line 359
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v18, v0

    if-eqz v18, :cond_3

    .line 360
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->setRefreshCompleted(Z)V

    .line 361
    const-string v18, ""

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "cnt : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getRefreshCnt()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 362
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getRefreshCnt()I

    move-result v18

    if-gtz v18, :cond_3

    .line 363
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getLocation()V

    goto/16 :goto_0

    .line 369
    :cond_32
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->isNetWorkConnected(Landroid/content/Context;)Z

    move-result v18

    if-nez v18, :cond_33

    .line 370
    const-string v18, ""

    const-string v19, "no connect"

    invoke-static/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    move-object/from16 v18, v0

    const v19, 0x7f0d0020

    invoke-static/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    goto/16 :goto_0

    .line 374
    :cond_33
    const-string v18, "easy"

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    .line 375
    .local v11, "easy":I
    const/16 v18, 0x3e7

    move/from16 v0, v18

    if-ne v11, v0, :cond_34

    .line 376
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    invoke-static/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setIsAgreeUseLocation(Landroid/content/Context;Z)V

    .line 379
    :cond_34
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->isAgreeUseLocation(Landroid/content/Context;)Z

    move-result v18

    if-nez v18, :cond_36

    .line 380
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-object/from16 v18, v0

    if-nez v18, :cond_35

    .line 381
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSettingsForMain(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    .line 383
    :cond_35
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 384
    .local v8, "bundle":Landroid/os/Bundle;
    const-string v18, "ACCOUNTING_DIALOG_FROM"

    const-string v19, "FROM_TOUCH_WIDGET_EASY"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    const-string v18, "AUTOREFRESH_SETTING"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;->getAutoRefreshTime()I

    move-result v19

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 387
    new-instance v16, Landroid/content/Intent;

    const-string v18, "android.intent.action.MAIN"

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 388
    .local v16, "sintent":Landroid/content/Intent;
    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 389
    const-string v18, "easy"

    const/16 v19, 0x3e7

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 390
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    move-object/from16 v18, v0

    const-class v19, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/AccountingDialogActivity;

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 391
    const v18, 0x10008000

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 393
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 395
    .end local v8    # "bundle":Landroid/os/Bundle;
    .end local v16    # "sintent":Landroid/content/Intent;
    :cond_36
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getCurrentCity(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v18

    if-nez v18, :cond_3

    .line 397
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v18, v0

    if-eqz v18, :cond_3

    .line 398
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->setRefreshCompleted(Z)V

    .line 399
    const-string v18, ""

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "cnt : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getRefreshCnt()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 400
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getRefreshCnt()I

    move-result v18

    if-gtz v18, :cond_3

    .line 401
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getLocation()V

    goto/16 :goto_0

    .line 411
    .end local v11    # "easy":I
    :cond_37
    const-string v18, "com.sec.android.widgetapp.ap.hero.accuweatherdaemon.action.SYNC_CURRENT_LOCATION_WEATHER_DATA"

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_38

    .line 412
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    move-object/from16 v18, v0

    new-instance v19, Landroid/content/Intent;

    const-string v20, "com.sec.android.widgetapp.ap.hero.accuweather.widget.action.APPWIDGET_UPDATE"

    invoke-direct/range {v19 .. v20}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v19}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 414
    :cond_38
    const-string v18, "com.sec.android.daemonapp.action.DAEMON_AUTOREFRESH_START"

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_39

    .line 415
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v18, v0

    if-eqz v18, :cond_3

    .line 416
    const-string v18, ""

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "ACTION_DAEMON_AUTOREFRESH_START : cnt = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v20, v0

    .line 418
    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->getRefreshCnt()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", showLoading = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->isShowLoading:Z

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 416
    invoke-static/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 430
    :cond_39
    const-string v18, "com.sec.android.daemonapp.action.DAEMON_AUTOREFRESH_END"

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_3c

    .line 431
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    move-object/from16 v18, v0

    if-eqz v18, :cond_3b

    .line 432
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-object/from16 v18, v0

    if-nez v18, :cond_3a

    .line 433
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSettingsForMain(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    .line 435
    :cond_3a
    const/16 v18, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->makeViews(Z)V

    .line 440
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getAllCityList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v9

    .line 441
    .restart local v9    # "cityList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v9, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->composeUi(Ljava/util/ArrayList;Landroid/widget/RemoteViews;)V

    .line 442
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    move-object/from16 v18, v0

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    goto/16 :goto_0

    .line 447
    .end local v9    # "cityList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;>;"
    :cond_3b
    const-string v18, ""

    const-string v19, "refresh manager is null"

    invoke-static/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 451
    :cond_3c
    const-string v18, "android.net.conn.CONNECTIVITY_CHANGE"

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_3

    const-string v18, "networkType"

    const/16 v19, -0x1

    .line 452
    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v18

    if-nez v18, :cond_3

    .line 453
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->handleConnectivityUpdate(Landroid/content/Context;)Z

    move-result v18

    if-eqz v18, :cond_3d

    const/4 v15, 0x1

    .line 455
    .local v15, "newRoaming":I
    :goto_3
    const-string v18, ""

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "connectivity nR : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " mR : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRoaming:I

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRoaming:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-eq v15, v0, :cond_3

    .line 458
    move-object/from16 v0, p0

    iput v15, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRoaming:I

    .line 459
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->startRefreshingNetworkChange(Landroid/content/Context;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 453
    .end local v15    # "newRoaming":I
    :cond_3d
    const/4 v15, -0x1

    goto :goto_3
.end method

.method public onUpdateWidget(Landroid/appwidget/AppWidgetManager;[IZ)V
    .locals 10
    .param p1, "awm"    # Landroid/appwidget/AppWidgetManager;
    .param p2, "ids"    # [I
    .param p3, "isRotation"    # Z

    .prologue
    .line 566
    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    .line 567
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getCurrentCity(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCityList:Ljava/util/ArrayList;

    .line 569
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCityList:Ljava/util/ArrayList;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_0

    .line 570
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getSettingsForMain(Landroid/content/Context;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSetting:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/SettingsInfoForMain;

    .line 571
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSelectedLocation:Ljava/lang/String;

    if-nez v7, :cond_0

    .line 572
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCityList:Ljava/util/ArrayList;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/CityListItem;->getLocation()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSelectedLocation:Ljava/lang/String;

    .line 576
    :cond_0
    const/4 v5, 0x0

    .line 577
    .local v5, "isFirstLanch":Z
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCityList:Ljava/util/ArrayList;

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-nez v7, :cond_1

    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getFirstLaunch(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 578
    const/4 v5, 0x1

    .line 580
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    if-eqz v7, :cond_1

    .line 581
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->setRefreshCompleted(Z)V

    .line 582
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    const/4 v8, 0x0

    const/16 v9, 0xfa1

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->onFirstUpdate(II)Z

    move-result v7

    if-nez v7, :cond_1

    .line 584
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->setRefreshCompleted(Z)V

    .line 585
    const/4 v5, 0x0

    .line 590
    :cond_1
    const/4 v6, 0x0

    .line 591
    .local v6, "resolutionInfo":[Ljava/lang/String;
    array-length v0, p2

    .line 592
    .local v0, "N":I
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    if-eqz v7, :cond_2

    .line 593
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f070001

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    .line 595
    :cond_2
    if-eqz v6, :cond_4

    const/4 v7, 0x0

    aget-object v7, v6, v7

    if-eqz v7, :cond_4

    const/4 v7, 0x0

    aget-object v7, v6, v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_4

    .line 596
    const-string v7, ""

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onUpdate() : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x0

    aget-object v9, v6, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 600
    :goto_0
    const/4 v7, 0x1

    invoke-direct {p0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->makeViews(Z)V

    .line 601
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v0, :cond_6

    .line 602
    aget v3, p2, v2

    .line 603
    .local v3, "id":I
    const-string v7, ""

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onUpdate() id : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 605
    invoke-virtual {p1, v3}, Landroid/appwidget/AppWidgetManager;->getAppWidgetInfo(I)Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v4

    .line 607
    .local v4, "info":Landroid/appwidget/AppWidgetProviderInfo;
    if-nez v4, :cond_5

    .line 622
    .end local v0    # "N":I
    .end local v2    # "i":I
    .end local v3    # "id":I
    .end local v4    # "info":Landroid/appwidget/AppWidgetProviderInfo;
    .end local v5    # "isFirstLanch":Z
    .end local v6    # "resolutionInfo":[Ljava/lang/String;
    :cond_3
    :goto_2
    return-void

    .line 598
    .restart local v0    # "N":I
    .restart local v5    # "isFirstLanch":Z
    .restart local v6    # "resolutionInfo":[Ljava/lang/String;
    :cond_4
    const-string v7, ""

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onUpdate() : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 619
    .end local v0    # "N":I
    .end local v5    # "isFirstLanch":Z
    .end local v6    # "resolutionInfo":[Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 620
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_2

    .line 601
    .end local v1    # "e":Ljava/lang/NullPointerException;
    .restart local v0    # "N":I
    .restart local v2    # "i":I
    .restart local v3    # "id":I
    .restart local v4    # "info":Landroid/appwidget/AppWidgetProviderInfo;
    .restart local v5    # "isFirstLanch":Z
    .restart local v6    # "resolutionInfo":[Ljava/lang/String;
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 612
    .end local v3    # "id":I
    .end local v4    # "info":Landroid/appwidget/AppWidgetProviderInfo;
    :cond_6
    if-eqz v5, :cond_7

    .line 613
    const/4 v7, 0x0

    :try_start_1
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    invoke-direct {p0, v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->composeEmptyCityUI(ZLandroid/widget/RemoteViews;)V

    .line 617
    :goto_3
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    invoke-virtual {p1, p2, v7}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    goto :goto_2

    .line 615
    :cond_7
    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCityList:Ljava/util/ArrayList;

    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mViews:Landroid/widget/RemoteViews;

    invoke-direct {p0, v7, v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->composeUi(Ljava/util/ArrayList;Landroid/widget/RemoteViews;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3
.end method

.method public setUiWithDatabaseData(Landroid/widget/RemoteViews;)V
    .locals 8
    .param p1, "views"    # Landroid/widget/RemoteViews;

    .prologue
    const v7, 0x7f080048

    const v6, 0x7f080045

    const v5, 0x7f080041

    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 893
    const-string v1, ""

    const-string v2, " cUI : cnt = setui"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 894
    const v1, 0x7f08004d

    invoke-virtual {p1, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 895
    const v1, 0x7f08004e

    invoke-virtual {p1, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 897
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCityList:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 899
    invoke-virtual {p1, v5, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 900
    const v1, 0x7f08004b

    invoke-virtual {p1, v1, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 901
    const v1, 0x7f08004a

    invoke-virtual {p1, v1, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 902
    invoke-virtual {p1, v6, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 903
    const v1, 0x7f080050

    invoke-virtual {p1, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 904
    const v1, 0x7f08004c

    invoke-virtual {p1, v1, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 905
    invoke-virtual {p1, v7, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 937
    :goto_0
    return-void

    .line 910
    :cond_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 911
    invoke-virtual {p1, v5, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 916
    :goto_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCityList:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCityList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-lt v1, v2, :cond_2

    .line 917
    const v1, 0x7f08004b

    invoke-virtual {p1, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 918
    const v1, 0x7f08004a

    invoke-virtual {p1, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 919
    invoke-virtual {p1, v6, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 920
    const v1, 0x7f080050

    invoke-virtual {p1, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 921
    const v1, 0x7f08004c

    invoke-virtual {p1, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 922
    invoke-virtual {p1, v7, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 933
    :goto_2
    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setUiWithDatabaseData(): mSelectedL = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSelectedLocation:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 935
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mCtx:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mSelectedLocation:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getCurrentLocationTodayInfo(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;

    move-result-object v0

    .line 936
    .local v0, "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    invoke-direct {p0, p1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->SetUIWeatherData(Landroid/widget/RemoteViews;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;)V

    goto :goto_0

    .line 913
    .end local v0    # "today":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/TodayWeatherInfo;
    :cond_1
    invoke-virtual {p1, v5, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 914
    const v1, 0x7f080040

    invoke-virtual {p1, v1, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_1

    .line 924
    :cond_2
    const v1, 0x7f08004b

    invoke-virtual {p1, v1, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 925
    const v1, 0x7f08004a

    invoke-virtual {p1, v1, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 926
    invoke-virtual {p1, v6, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 927
    const v1, 0x7f080050

    invoke-virtual {p1, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 928
    const v1, 0x7f08004c

    invoke-virtual {p1, v1, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 929
    invoke-virtual {p1, v7, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_2
.end method

.method public startRefreshingNetworkChange(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x1

    .line 1214
    if-nez p1, :cond_0

    .line 1215
    const-string v2, ""

    const-string v3, "sRNC context n"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1245
    :goto_0
    return-void

    .line 1219
    :cond_0
    iget-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mIsOnRefreshing:Z

    if-nez v2, :cond_4

    .line 1220
    invoke-static {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getSettingDataRoaming(Landroid/content/Context;)I

    move-result v1

    .line 1221
    .local v1, "settingDataRoaming":I
    invoke-static {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getRefreshRoamingSettings(Landroid/content/Context;)I

    move-result v0

    .line 1223
    .local v0, "refreshRoamingSetting":I
    if-ne v1, v6, :cond_3

    if-ne v0, v6, :cond_3

    .line 1225
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    if-eqz v2, :cond_2

    .line 1226
    invoke-static {p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getLocation(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1227
    const-string v2, ""

    const-string v3, "sRNC refresh roaming"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1231
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mUiHandler:Landroid/os/Handler;

    const-string v5, "cityId:current"

    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->doManualRefresh(ILandroid/os/Handler;Ljava/lang/String;)V

    .line 1232
    iput-boolean v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/easy/widget/UIManagerEasyMode;->mIsOnRefreshing:Z

    goto :goto_0

    .line 1234
    :cond_1
    const-string v2, ""

    const-string v3, "sRNC no cur city"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1237
    :cond_2
    const-string v2, ""

    const-string v3, "sRNC mRM n"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1240
    :cond_3
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sRNC sDR : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " rRS : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1243
    .end local v0    # "refreshRoamingSetting":I
    .end local v1    # "settingDataRoaming":I
    :cond_4
    const-string v2, ""

    const-string v3, "sRNC mIsOnRefreshing true"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
