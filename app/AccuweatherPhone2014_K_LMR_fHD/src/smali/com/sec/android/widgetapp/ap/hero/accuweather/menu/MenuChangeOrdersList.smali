.class public Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;
.super Landroid/app/Activity;
.source "MenuChangeOrdersList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;,
        Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;
    }
.end annotation


# instance fields
.field private actionbar:Landroid/app/ActionBar;

.field private actionbarBack:Landroid/widget/LinearLayout;

.field private actionbarLayout:Landroid/widget/RelativeLayout;

.field private actionbarTitle:Landroid/widget/TextView;

.field private citylist_logo:Landroid/widget/RelativeLayout;

.field private mAccessibilityDelegateCompat:Landroid/support/v4/view/AccessibilityDelegateCompat;

.field private mCheckCurrentCitySetting:I

.field private mCtx:Landroid/content/Context;

.field private mCurrentCityData:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;

.field private mCurrentCityLayout:Landroid/view/View;

.field mCurrentY:I

.field mHandler:Landroid/os/Handler;

.field private mHasCurrentCity:Z

.field mIsLastScroll:Z

.field private mIsPressedHomeKey:Z

.field private mIsShownDst:Z

.field private mIsTModel:Z

.field private mIsVisible:Z

.field mLastY:I

.field private mListView:Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;

.field private mOrderAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;

.field private mOrgItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;",
            ">;"
        }
    .end annotation
.end field

.field private mPopupDialog:Landroid/app/Dialog;

.field private mRefreshDialog:Landroid/app/Dialog;

.field private mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

.field private mTouchHandle:Landroid/os/Handler;

.field private mUiHandler:Landroid/os/Handler;

.field private mUpdateBtn:Landroid/widget/ImageView;

.field private mUpdateFlipper:Landroid/widget/ProgressBar;

.field private mUpdateLayout:Landroid/widget/RelativeLayout;

.field private mWorkingArrayList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;",
            ">;"
        }
    .end annotation
.end field

.field private tempScale:I

.field private toastHint:Landroid/widget/Toast;

.field private urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 59
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 72
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mIsLastScroll:Z

    .line 77
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->actionbar:Landroid/app/ActionBar;

    .line 79
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->actionbarLayout:Landroid/widget/RelativeLayout;

    .line 80
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->actionbarBack:Landroid/widget/LinearLayout;

    .line 82
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->actionbarTitle:Landroid/widget/TextView;

    .line 94
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mHandler:Landroid/os/Handler;

    .line 98
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mIsShownDst:Z

    .line 102
    iput v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCheckCurrentCitySetting:I

    .line 104
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mHasCurrentCity:Z

    .line 106
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCurrentCityData:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;

    .line 107
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCurrentCityLayout:Landroid/view/View;

    .line 109
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mUpdateBtn:Landroid/widget/ImageView;

    .line 111
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mUpdateFlipper:Landroid/widget/ProgressBar;

    .line 112
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    .line 113
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    .line 115
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mPopupDialog:Landroid/app/Dialog;

    .line 116
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mIsVisible:Z

    .line 117
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mRefreshDialog:Landroid/app/Dialog;

    .line 119
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mIsPressedHomeKey:Z

    .line 121
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mUpdateLayout:Landroid/widget/RelativeLayout;

    .line 123
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mIsTModel:Z

    .line 435
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$5;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$5;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mUiHandler:Landroid/os/Handler;

    .line 881
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$8;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$8;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mAccessibilityDelegateCompat:Landroid/support/v4/view/AccessibilityDelegateCompat;

    .line 1135
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mWorkingArrayList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mOrgItemList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->stopRefreshAnimation()V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->changeListViewRowLayoutParams(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;Landroid/view/View;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;
    .param p1, "x1"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;
    .param p2, "x2"    # Landroid/view/View;
    .param p3, "x3"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->setMyCityData(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;Landroid/view/View;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCtx:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)Landroid/app/Dialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mPopupDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->startUpdateAnimation()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->showRefreshDialog()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mUiHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->hideRefreshDialog()V

    return-void
.end method

.method static synthetic access$902(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;Landroid/app/Dialog;)Landroid/app/Dialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;
    .param p1, "x1"    # Landroid/app/Dialog;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mRefreshDialog:Landroid/app/Dialog;

    return-object p1
.end method

.method private changeListViewParamsBeforDrag(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 911
    if-nez p1, :cond_1

    .line 912
    const-string v1, ""

    const-string v2, "view is null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 927
    :cond_0
    :goto_0
    return-void

    .line 915
    :cond_1
    const v1, 0x7f080070

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 918
    .local v0, "selectorLayout":Landroid/widget/FrameLayout;
    if-eqz v0, :cond_0

    .line 919
    const-string v1, "#50000000"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    goto :goto_0
.end method

.method private changeListViewRowLayoutParams(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 939
    const v3, 0x7f080074

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 942
    .local v0, "mycitydatanameLayout":Landroid/widget/LinearLayout;
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 944
    .local v1, "mycitydatanameLayoutParam":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCtx:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v2, v3, Landroid/content/res/Configuration;->orientation:I

    .line 946
    .local v2, "orientation":I
    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 947
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCtx:Landroid/content/Context;

    const/16 v4, 0x2e

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getPixelFromDP(Landroid/content/Context;I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginStart(I)V

    .line 948
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCtx:Landroid/content/Context;

    const/16 v4, 0xe2

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getPixelFromDP(Landroid/content/Context;I)I

    move-result v3

    iput v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 954
    :cond_0
    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 955
    return-void

    .line 950
    :cond_1
    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 951
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCtx:Landroid/content/Context;

    const/16 v4, 0x32

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getPixelFromDP(Landroid/content/Context;I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginStart(I)V

    .line 952
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCtx:Landroid/content/Context;

    const/16 v4, 0x178

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getPixelFromDP(Landroid/content/Context;I)I

    move-result v3

    iput v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    goto :goto_0
.end method

.method private changeUpdateLayoutHeight(I)V
    .locals 3
    .param p1, "orientation"    # I

    .prologue
    .line 1089
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mUpdateLayout:Landroid/widget/RelativeLayout;

    .line 1090
    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1092
    .local v0, "changeUpdateLayoutParam":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    .line 1093
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCtx:Landroid/content/Context;

    const/16 v2, 0x24

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getPixelFromDP(Landroid/content/Context;I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 1098
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mUpdateLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1099
    return-void

    .line 1094
    :cond_1
    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    .line 1095
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCtx:Landroid/content/Context;

    const/16 v2, 0x23

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getPixelFromDP(Landroid/content/Context;I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    goto :goto_0
.end method

.method private getCurrentData()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 299
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mOrgItemList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mOrgItemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 300
    :cond_0
    const-string v0, ""

    const-string v1, "OIL null or size 0"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    :goto_0
    return-void

    .line 303
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCtx:Landroid/content/Context;

    const v1, 0x7f030028

    invoke-static {v0, v1, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCurrentCityLayout:Landroid/view/View;

    .line 305
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCurrentCityLayout:Landroid/view/View;

    if-nez v0, :cond_2

    .line 306
    const-string v0, ""

    const-string v1, "CCL is null"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 309
    :cond_2
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCurrentCityLayout:Landroid/view/View;

    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mOrgItemList:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;

    invoke-direct {p0, v3, v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->setMyCityData(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;Landroid/view/View;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;)V

    .line 310
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCurrentCityLayout:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->changeListViewParamsBeforDrag(Landroid/view/View;)V

    .line 311
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mListView:Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCurrentCityLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;->addHeaderView(Landroid/view/View;)V

    .line 312
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mOrgItemList:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCurrentCityData:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;

    .line 313
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mOrgItemList:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0
.end method

.method private getWorkingDataList()V
    .locals 2

    .prologue
    .line 317
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mWorkingArrayList:Ljava/util/ArrayList;

    .line 318
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mWorkingArrayList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mOrgItemList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 320
    return-void
.end method

.method private hideRefreshDialog()V
    .locals 1

    .prologue
    .line 564
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mRefreshDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 566
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mRefreshDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 570
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mRefreshDialog:Landroid/app/Dialog;

    .line 572
    :cond_0
    return-void

    .line 567
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private initViews()V
    .locals 2

    .prologue
    .line 204
    const v0, 0x7f080113

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mListView:Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;

    .line 206
    const v0, 0x7f080068

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->citylist_logo:Landroid/widget/RelativeLayout;

    .line 207
    const v0, 0x7f080069

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mUpdateBtn:Landroid/widget/ImageView;

    .line 208
    const v0, 0x7f08006a

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mUpdateFlipper:Landroid/widget/ProgressBar;

    .line 209
    const v0, 0x7f080067

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mUpdateLayout:Landroid/widget/RelativeLayout;

    .line 211
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCtx:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    .line 212
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCtx:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    .line 213
    return-void
.end method

.method private makemOrgItemListOrderSameWithWorkingList()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 252
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mOrgItemList:Ljava/util/ArrayList;

    if-eqz v8, :cond_6

    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mWorkingArrayList:Ljava/util/ArrayList;

    if-eqz v8, :cond_6

    .line 253
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mOrgItemList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 254
    .local v3, "orgItemSize":I
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mWorkingArrayList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 256
    .local v7, "workingItemSize":I
    if-ne v3, v7, :cond_4

    .line 257
    new-array v4, v7, [Ljava/lang/String;

    .line 259
    .local v4, "refLocationArray":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v7, :cond_0

    .line 260
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mWorkingArrayList:Ljava/util/ArrayList;

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getLocation()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v0

    .line 259
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 263
    :cond_0
    array-length v5, v4

    .line 264
    .local v5, "refLocationArraySize":I
    const/4 v6, 0x0

    .line 266
    .local v6, "tempLoc":Ljava/lang/String;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 268
    .local v2, "newOrgItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;>;"
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v5, :cond_3

    .line 269
    aget-object v6, v4, v0

    .line 271
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_2
    if-ge v1, v3, :cond_1

    .line 272
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mOrgItemList:Ljava/util/ArrayList;

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getLocation()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 273
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mOrgItemList:Ljava/util/ArrayList;

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 268
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 271
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 279
    .end local v1    # "j":I
    :cond_3
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mWorkingArrayList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    .line 280
    iput-object v9, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mWorkingArrayList:Ljava/util/ArrayList;

    .line 282
    iget-object v8, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mOrgItemList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    .line 283
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mOrgItemList:Ljava/util/ArrayList;

    .line 285
    const/4 v0, 0x0

    :goto_3
    if-ge v0, v5, :cond_5

    .line 286
    aput-object v9, v4, v0

    .line 285
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 291
    .end local v0    # "i":I
    .end local v2    # "newOrgItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;>;"
    .end local v4    # "refLocationArray":[Ljava/lang/String;
    .end local v5    # "refLocationArraySize":I
    .end local v6    # "tempLoc":Ljava/lang/String;
    :cond_4
    const-string v8, ""

    const-string v9, "mOS size invalid"

    invoke-static {v8, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    .end local v3    # "orgItemSize":I
    .end local v7    # "workingItemSize":I
    :cond_5
    :goto_4
    return-void

    .line 294
    :cond_6
    const-string v8, ""

    const-string v9, "mOS invalid arg"

    invoke-static {v8, v9}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4
.end method

.method private setDragGrabHandlePadding()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 583
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mListView:Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;

    if-eqz v1, :cond_0

    .line 584
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCtx:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v0, v1, Landroid/content/res/Configuration;->orientation:I

    .line 586
    .local v0, "orientation":I
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mListView:Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;

    const/16 v2, 0x13

    invoke-virtual {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;->setDragGrabHandlePositionGravity(I)V

    .line 588
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 589
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mListView:Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCtx:Landroid/content/Context;

    const/16 v3, 0xc

    .line 590
    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getPixelFromDP(Landroid/content/Context;I)I

    move-result v2

    .line 589
    invoke-virtual {v1, v2, v4, v4, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;->setDragGrabHandlePadding(IIII)V

    .line 602
    .end local v0    # "orientation":I
    :cond_0
    :goto_0
    return-void

    .line 594
    .restart local v0    # "orientation":I
    :cond_1
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 595
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mListView:Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCtx:Landroid/content/Context;

    const/16 v3, 0x10

    .line 596
    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getPixelFromDP(Landroid/content/Context;I)I

    move-result v2

    .line 595
    invoke-virtual {v1, v2, v4, v4, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;->setDragGrabHandlePadding(IIII)V

    goto :goto_0
.end method

.method private setEventListener()V
    .locals 2

    .prologue
    .line 340
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mUpdateBtn:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mUpdateBtn:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$2;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$2;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 348
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->citylist_logo:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 349
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->citylist_logo:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$3;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$3;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 365
    :cond_1
    return-void
.end method

.method private setIsTModel()V
    .locals 2

    .prologue
    .line 1102
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090012

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mIsTModel:Z

    .line 1103
    return-void
.end method

.method private setMyCityData(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;Landroid/view/View;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;)V
    .locals 25
    .param p1, "viewHolder"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;
    .param p2, "currentCityLayoutView"    # Landroid/view/View;
    .param p3, "myCityData"    # Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;

    .prologue
    .line 959
    if-eqz p3, :cond_0

    if-nez p1, :cond_1

    if-nez p2, :cond_1

    .line 960
    :cond_0
    const-string v1, ""

    const-string v2, "sWD arg n"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1086
    :goto_0
    return-void

    .line 964
    :cond_1
    const/4 v9, 0x0

    .line 965
    .local v9, "contentsLayout":Landroid/widget/RelativeLayout;
    const/16 v18, 0x0

    .line 966
    .local v18, "selector":Landroid/widget/FrameLayout;
    const/16 v20, 0x0

    .line 967
    .local v20, "stroke":Landroid/widget/FrameLayout;
    const/16 v16, 0x0

    .line 968
    .local v16, "name":Landroid/widget/TextView;
    const/16 v19, 0x0

    .line 969
    .local v19, "state_name":Landroid/widget/TextView;
    const/16 v21, 0x0

    .line 970
    .local v21, "time":Landroid/widget/TextView;
    const/16 v22, 0x0

    .line 971
    .local v22, "time_ampm":Landroid/widget/TextView;
    const/4 v11, 0x0

    .line 972
    .local v11, "currentTemp":Landroid/widget/TextView;
    const/4 v13, 0x0

    .line 973
    .local v13, "highTemp":Landroid/widget/TextView;
    const/4 v15, 0x0

    .line 974
    .local v15, "lowTemp":Landroid/widget/TextView;
    const/16 v23, 0x0

    .line 975
    .local v23, "weathericon":Landroid/widget/ImageView;
    const/4 v8, 0x0

    .line 976
    .local v8, "centorfahren":Landroid/widget/ImageView;
    const/4 v10, 0x0

    .line 977
    .local v10, "current":Landroid/widget/ImageView;
    const/4 v12, 0x0

    .line 979
    .local v12, "dst":Landroid/widget/ImageView;
    if-eqz p1, :cond_5

    .line 981
    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;->tv_contentsLayout:Landroid/widget/RelativeLayout;

    .line 982
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;->tv_selector:Landroid/widget/FrameLayout;

    move-object/from16 v18, v0

    .line 983
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;->tv_stroke:Landroid/widget/FrameLayout;

    move-object/from16 v20, v0

    .line 984
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;->tv_name:Landroid/widget/TextView;

    move-object/from16 v16, v0

    .line 985
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;->tv_state_name:Landroid/widget/TextView;

    move-object/from16 v19, v0

    .line 986
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;->tv_time:Landroid/widget/TextView;

    move-object/from16 v21, v0

    .line 987
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;->tv_time_ampm:Landroid/widget/TextView;

    move-object/from16 v22, v0

    .line 988
    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;->tv_currentTemp:Landroid/widget/TextView;

    .line 989
    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;->tv_highTemp:Landroid/widget/TextView;

    .line 990
    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;->tv_lowTemp:Landroid/widget/TextView;

    .line 991
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;->tv_weathericon:Landroid/widget/ImageView;

    move-object/from16 v23, v0

    .line 992
    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;->tv_centorfahren:Landroid/widget/ImageView;

    .line 993
    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;->tv_current:Landroid/widget/ImageView;

    .line 994
    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;->tv_dst:Landroid/widget/ImageView;

    .line 996
    const-string v1, "#00000000"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    .line 1017
    :goto_1
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getTimeZone()Ljava/lang/String;

    move-result-object v1

    .line 1018
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getSunRaiseTime()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getSunSetTime()Ljava/lang/String;

    move-result-object v3

    .line 1017
    invoke-static {v1, v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->checkDayOrNight(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v14

    .line 1020
    .local v14, "isDay":Z
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getName()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1021
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getState()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1022
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getTime()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1023
    const/16 v1, 0x8

    move-object/from16 v0, v22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1024
    const/4 v1, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1025
    const-string v1, "%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getTempCurrent()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1026
    const-string v1, "%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getTempHigh()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v13, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1027
    const-string v1, "%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getTempLow()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v15, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1029
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->SamsungNumber3L:Landroid/graphics/Typeface;

    if-eqz v1, :cond_2

    .line 1030
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->SamsungNumber3L:Landroid/graphics/Typeface;

    invoke-virtual {v11, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1031
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->SamsungNumber3L:Landroid/graphics/Typeface;

    invoke-virtual {v13, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1032
    sget-object v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->SamsungNumber3L:Landroid/graphics/Typeface;

    invoke-virtual {v15, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1036
    :cond_2
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getWeatherIcon()I

    move-result v1

    const/4 v2, 0x3

    .line 1035
    invoke-static {v1, v2, v14}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->setTodayWeatherIcon(IIZ)I

    move-result v1

    move-object/from16 v0, v23

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1039
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCtx:Landroid/content/Context;

    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getWeatherIcon()I

    move-result v2

    const/16 v3, 0x102

    invoke-static {v1, v14, v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getTodayBG(Landroid/content/Context;ZII)I

    move-result v17

    .line 1041
    .local v17, "res":I
    const/4 v1, -0x1

    move/from16 v0, v17

    if-le v0, v1, :cond_3

    .line 1042
    move/from16 v0, v17

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 1044
    :cond_3
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->tempScale:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_7

    .line 1045
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mIsTModel:Z

    if-eqz v1, :cond_6

    .line 1046
    const v1, 0x7f020127

    invoke-virtual {v8, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1057
    :goto_2
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mIsShownDst:Z

    if-eqz v1, :cond_4

    .line 1058
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getDst()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lez v1, :cond_9

    .line 1060
    const/4 v1, 0x0

    invoke-virtual {v12, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1066
    :cond_4
    :goto_3
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getIsCurrentLocation()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_a

    .line 1067
    const/4 v1, 0x0

    invoke-virtual {v10, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1072
    :goto_4
    const/4 v1, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1074
    invoke-virtual/range {v20 .. v20}, Landroid/widget/FrameLayout;->bringToFront()V

    .line 1075
    invoke-virtual/range {v18 .. v18}, Landroid/widget/FrameLayout;->bringToFront()V

    .line 1078
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getIsCurrentLocation()Z

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_b

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCtx:Landroid/content/Context;

    .line 1079
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d0033

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_5
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCtx:Landroid/content/Context;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->tempScale:I

    .line 1080
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getName()Ljava/lang/String;

    move-result-object v3

    .line 1081
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getState()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getTime()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getTempCurrent()Ljava/lang/String;

    move-result-object v6

    .line 1082
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getWeatherDesc()Ljava/lang/String;

    move-result-object v7

    .line 1080
    invoke-static/range {v1 .. v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getGridCityListTTS(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1078
    invoke-virtual {v9, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1084
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mAccessibilityDelegateCompat:Landroid/support/v4/view/AccessibilityDelegateCompat;

    invoke-static {v9, v1}, Landroid/support/v4/view/ViewCompat;->setAccessibilityDelegate(Landroid/view/View;Landroid/support/v4/view/AccessibilityDelegateCompat;)V

    goto/16 :goto_0

    .line 999
    .end local v14    # "isDay":Z
    .end local v17    # "res":I
    :cond_5
    const v1, 0x7f080072

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .end local v9    # "contentsLayout":Landroid/widget/RelativeLayout;
    check-cast v9, Landroid/widget/RelativeLayout;

    .line 1000
    .restart local v9    # "contentsLayout":Landroid/widget/RelativeLayout;
    const v1, 0x7f080070

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    .end local v18    # "selector":Landroid/widget/FrameLayout;
    check-cast v18, Landroid/widget/FrameLayout;

    .line 1001
    .restart local v18    # "selector":Landroid/widget/FrameLayout;
    const v1, 0x7f080071

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    .end local v20    # "stroke":Landroid/widget/FrameLayout;
    check-cast v20, Landroid/widget/FrameLayout;

    .line 1002
    .restart local v20    # "stroke":Landroid/widget/FrameLayout;
    const v1, 0x7f080076

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    .end local v16    # "name":Landroid/widget/TextView;
    check-cast v16, Landroid/widget/TextView;

    .line 1003
    .restart local v16    # "name":Landroid/widget/TextView;
    const v1, 0x7f080077

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    .end local v19    # "state_name":Landroid/widget/TextView;
    check-cast v19, Landroid/widget/TextView;

    .line 1004
    .restart local v19    # "state_name":Landroid/widget/TextView;
    const v1, 0x7f080079

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    .end local v21    # "time":Landroid/widget/TextView;
    check-cast v21, Landroid/widget/TextView;

    .line 1005
    .restart local v21    # "time":Landroid/widget/TextView;
    const v1, 0x7f08007a

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    .end local v22    # "time_ampm":Landroid/widget/TextView;
    check-cast v22, Landroid/widget/TextView;

    .line 1006
    .restart local v22    # "time_ampm":Landroid/widget/TextView;
    const v1, 0x7f08007d

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    .end local v11    # "currentTemp":Landroid/widget/TextView;
    check-cast v11, Landroid/widget/TextView;

    .line 1007
    .restart local v11    # "currentTemp":Landroid/widget/TextView;
    const v1, 0x7f08002c

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    .end local v13    # "highTemp":Landroid/widget/TextView;
    check-cast v13, Landroid/widget/TextView;

    .line 1008
    .restart local v13    # "highTemp":Landroid/widget/TextView;
    const v1, 0x7f08002d

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    .end local v15    # "lowTemp":Landroid/widget/TextView;
    check-cast v15, Landroid/widget/TextView;

    .line 1009
    .restart local v15    # "lowTemp":Landroid/widget/TextView;
    const v1, 0x7f08007c

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v23

    .end local v23    # "weathericon":Landroid/widget/ImageView;
    check-cast v23, Landroid/widget/ImageView;

    .line 1010
    .restart local v23    # "weathericon":Landroid/widget/ImageView;
    const v1, 0x7f08007e

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .end local v8    # "centorfahren":Landroid/widget/ImageView;
    check-cast v8, Landroid/widget/ImageView;

    .line 1011
    .restart local v8    # "centorfahren":Landroid/widget/ImageView;
    const v1, 0x7f080075

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    .end local v10    # "current":Landroid/widget/ImageView;
    check-cast v10, Landroid/widget/ImageView;

    .line 1012
    .restart local v10    # "current":Landroid/widget/ImageView;
    const v1, 0x7f080020

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    .end local v12    # "dst":Landroid/widget/ImageView;
    check-cast v12, Landroid/widget/ImageView;

    .line 1014
    .restart local v12    # "dst":Landroid/widget/ImageView;
    const-string v1, "#50000000"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 1048
    .restart local v14    # "isDay":Z
    .restart local v17    # "res":I
    :cond_6
    const v1, 0x7f020133

    invoke-virtual {v8, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_2

    .line 1051
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mIsTModel:Z

    if-eqz v1, :cond_8

    .line 1052
    const v1, 0x7f020128

    invoke-virtual {v8, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_2

    .line 1054
    :cond_8
    const v1, 0x7f020134

    invoke-virtual {v8, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_2

    .line 1062
    :cond_9
    const/16 v1, 0x8

    invoke-virtual {v12, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 1069
    :cond_a
    const/16 v1, 0x8

    invoke-virtual {v10, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_4

    .line 1079
    :cond_b
    const-string v1, ""

    goto/16 :goto_5
.end method

.method private setupActionbar()V
    .locals 8

    .prologue
    const v7, 0x7f0d0003

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 153
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->actionbar:Landroid/app/ActionBar;

    if-nez v2, :cond_0

    .line 154
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->actionbar:Landroid/app/ActionBar;

    .line 156
    :cond_0
    sget-boolean v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isLollipopOS:Z

    if-nez v2, :cond_2

    .line 158
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->actionbar:Landroid/app/ActionBar;

    if-eqz v2, :cond_1

    .line 159
    const v2, 0x7f030005

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->actionbarLayout:Landroid/widget/RelativeLayout;

    .line 161
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02008f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 164
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->actionbar:Landroid/app/ActionBar;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->actionbarLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 165
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->actionbarLayout:Landroid/widget/RelativeLayout;

    const v3, 0x7f080008

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->actionbarTitle:Landroid/widget/TextView;

    .line 166
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->actionbarLayout:Landroid/widget/RelativeLayout;

    const v3, 0x7f080006

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->actionbarBack:Landroid/widget/LinearLayout;

    .line 168
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->actionbarBack:Landroid/widget/LinearLayout;

    const v3, 0x7f080009

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 169
    .local v1, "v":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200aa

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 170
    .local v0, "backIcon":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0, v6}, Landroid/graphics/drawable/Drawable;->setAutoMirrored(Z)V

    .line 171
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 174
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v5}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 175
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v5}, Landroid/app/ActionBar;->setDisplayUseLogoEnabled(Z)V

    .line 176
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v5}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 177
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v6}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 179
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->actionbarTitle:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setText(I)V

    .line 181
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->actionbarBack:Landroid/widget/LinearLayout;

    new-instance v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$1;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$1;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)V

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 187
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->actionbarBack:Landroid/widget/LinearLayout;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCtx:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d0096

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCtx:Landroid/content/Context;

    .line 189
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d0097

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 187
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 201
    .end local v0    # "backIcon":Landroid/graphics/drawable/Drawable;
    .end local v1    # "v":Landroid/widget/ImageView;
    :cond_1
    :goto_0
    return-void

    .line 192
    :cond_2
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->actionbar:Landroid/app/ActionBar;

    if-eqz v2, :cond_1

    .line 193
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v5}, Landroid/app/ActionBar;->setDisplayUseLogoEnabled(Z)V

    .line 194
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v6}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 195
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v5}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 197
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v7}, Landroid/app/ActionBar;->setTitle(I)V

    .line 198
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->actionbar:Landroid/app/ActionBar;

    invoke-virtual {v2, v6}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    goto :goto_0
.end method

.method private setupAdapter()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 323
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;

    const v1, 0x7f030028

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mWorkingArrayList:Ljava/util/ArrayList;

    invoke-direct {v0, p0, p0, v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mOrderAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;

    .line 326
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mListView:Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;->setCacheColorHint(I)V

    .line 328
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mListView:Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;

    invoke-virtual {v0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;->setHapticFeedbackEnabled(Z)V

    .line 329
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mListView:Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mOrderAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;->setOrderItemAdapter(Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrderAdapter;)V

    .line 330
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mListView:Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;

    const v1, 0x7f020009

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;->setDragGrabHandleDrawable(I)V

    .line 331
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mListView:Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;->setDndMode(Z)V

    .line 333
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mListView:Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;

    const/16 v1, 0x33

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;->setDragGrabHandlePositionGravity(I)V

    .line 335
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->setDragGrabHandlePadding()V

    .line 336
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mListView:Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;->setDndListener()V

    .line 337
    return-void
.end method

.method private setupListView(Z)V
    .locals 2
    .param p1, "isRefreshed"    # Z

    .prologue
    .line 223
    const-string v0, "cityId:current"

    invoke-static {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->isRegisteredToCityList(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mHasCurrentCity:Z

    .line 224
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getCheckCurrentCityLocation(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCheckCurrentCitySetting:I

    .line 226
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCtx:Landroid/content/Context;

    iget v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCheckCurrentCitySetting:I

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getAllCityListForChangeOrder(Landroid/content/Context;I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mOrgItemList:Ljava/util/ArrayList;

    .line 228
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCtx:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getTempScaleSetting(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->tempScale:I

    .line 230
    sget-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isCurCityAtTopInCityList:Z

    if-eqz v0, :cond_0

    .line 232
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mHasCurrentCity:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCheckCurrentCitySetting:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 233
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->getCurrentData()V

    .line 237
    :cond_0
    if-eqz p1, :cond_1

    .line 239
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->makemOrgItemListOrderSameWithWorkingList()V

    .line 242
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->getWorkingDataList()V

    .line 244
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->setupAdapter()V

    .line 245
    return-void
.end method

.method private setupViews()V
    .locals 2

    .prologue
    .line 216
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mIsShownDst:Z

    .line 217
    sget-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isVerizon:Z

    if-eqz v0, :cond_0

    .line 218
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mIsShownDst:Z

    .line 220
    :cond_0
    return-void
.end method

.method private showRefreshDialog()V
    .locals 2

    .prologue
    .line 497
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 499
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->isActivityVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 500
    const-string v0, ""

    const-string v1, " RefreshDialog Show! "

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 502
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mRefreshDialog:Landroid/app/Dialog;

    if-nez v0, :cond_0

    .line 503
    const/16 v0, 0x3ef

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$6;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$6;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)V

    invoke-static {p0, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/CommonDialog;->showDialog(Landroid/content/Context;ILcom/sec/android/widgetapp/ap/hero/accuweather/common/DialogInteraction;)Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mRefreshDialog:Landroid/app/Dialog;

    .line 515
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mRefreshDialog:Landroid/app/Dialog;

    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$7;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$7;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 531
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mRefreshDialog:Landroid/app/Dialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 532
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mRefreshDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 535
    :cond_0
    return-void
.end method

.method private startUpdateAnimation()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 463
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mUpdateBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 464
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mUpdateBtn:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 465
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mUpdateFlipper:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 466
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mUpdateFlipper:Landroid/widget/ProgressBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setFocusable(Z)V

    .line 467
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mUpdateFlipper:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->requestFocus()Z

    .line 468
    return-void
.end method

.method private stopRefreshAnimation()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 471
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mUpdateFlipper:Landroid/widget/ProgressBar;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mUpdateBtn:Landroid/widget/ImageView;

    if-nez v1, :cond_2

    .line 472
    :cond_0
    const/4 v0, 0x5

    .line 474
    .local v0, "temp_seq":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mUpdateFlipper:Landroid/widget/ProgressBar;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mUpdateBtn:Landroid/widget/ImageView;

    if-nez v1, :cond_2

    .line 475
    :cond_1
    add-int/lit8 v0, v0, -0x1

    .line 477
    if-gtz v0, :cond_4

    .line 486
    .end local v0    # "temp_seq":I
    :cond_2
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mUpdateFlipper:Landroid/widget/ProgressBar;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mUpdateBtn:Landroid/widget/ImageView;

    if-eqz v1, :cond_3

    .line 487
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mUpdateBtn:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 488
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mUpdateBtn:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 489
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mUpdateFlipper:Landroid/widget/ProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 490
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mUpdateBtn:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 491
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mUpdateBtn:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->requestFocus()Z

    .line 494
    :cond_3
    return-void

    .line 481
    .restart local v0    # "temp_seq":I
    :cond_4
    const v1, 0x7f08006a

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mUpdateFlipper:Landroid/widget/ProgressBar;

    .line 482
    const v1, 0x7f080069

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mUpdateBtn:Landroid/widget/ImageView;

    goto :goto_0
.end method


# virtual methods
.method public cleanResource()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 771
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mListView:Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;

    .line 773
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mOrderAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;

    if-eqz v0, :cond_0

    .line 774
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mOrderAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->clear()V

    .line 775
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mOrderAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;

    .line 786
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mOrgItemList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 787
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mOrgItemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 788
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mOrgItemList:Ljava/util/ArrayList;

    .line 791
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mWorkingArrayList:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 792
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mWorkingArrayList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 793
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mWorkingArrayList:Ljava/util/ArrayList;

    .line 796
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mTouchHandle:Landroid/os/Handler;

    if-eqz v0, :cond_3

    .line 797
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mTouchHandle:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 798
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mTouchHandle:Landroid/os/Handler;

    .line 801
    :cond_3
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_4

    .line 802
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 803
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mHandler:Landroid/os/Handler;

    .line 806
    :cond_4
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mListView:Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;

    .line 807
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->actionbar:Landroid/app/ActionBar;

    .line 809
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mOrderAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;

    .line 811
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->actionbar:Landroid/app/ActionBar;

    .line 812
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->actionbarLayout:Landroid/widget/RelativeLayout;

    .line 813
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->actionbarBack:Landroid/widget/LinearLayout;

    .line 814
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->actionbarTitle:Landroid/widget/TextView;

    .line 822
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCurrentCityData:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;

    if-eqz v0, :cond_5

    .line 823
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCurrentCityData:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;

    .line 826
    :cond_5
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCurrentCityLayout:Landroid/view/View;

    if-eqz v0, :cond_6

    .line 827
    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCurrentCityLayout:Landroid/view/View;

    .line 830
    :cond_6
    return-void
.end method

.method public doneRun()V
    .locals 9

    .prologue
    const/4 v8, -0x1

    .line 605
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 606
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v8, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->setResult(ILandroid/content/Intent;)V

    .line 607
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mWorkingArrayList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_2

    .line 608
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 609
    .local v0, "calendar":Ljava/util/Calendar;
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCtx:Landroid/content/Context;

    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCtx:Landroid/content/Context;

    .line 610
    invoke-static {v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getTempScaleSetting(Landroid/content/Context;)I

    move-result v7

    .line 609
    invoke-static {v6, v0, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->getAllCityListIncludedCurrentLocation(Landroid/content/Context;Ljava/util/Calendar;I)Ljava/util/ArrayList;

    move-result-object v4

    .line 612
    .local v4, "mCLItemArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;>;"
    sget-boolean v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isCurCityAtTopInCityList:Z

    if-eqz v6, :cond_1

    .line 613
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v1, v6, :cond_1

    .line 614
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;->getLocation()Ljava/lang/String;

    move-result-object v6

    const-string v7, "cityId:current"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 615
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;

    .line 616
    .local v3, "mCLItem":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mWorkingArrayList:Ljava/util/ArrayList;

    const/4 v7, 0x0

    invoke-virtual {v6, v7, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 613
    .end local v3    # "mCLItem":Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 621
    .end local v1    # "i":I
    :cond_1
    iget-object v6, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCtx:Landroid/content/Context;

    iget-object v7, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mWorkingArrayList:Ljava/util/ArrayList;

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/db/DBHelper;->updateChangeOrderForGrid(Landroid/content/Context;Ljava/util/ArrayList;)I

    move-result v5

    .line 623
    .local v5, "result":I
    if-ne v8, v5, :cond_2

    .line 624
    const/16 v6, 0x3e7

    const/4 v7, 0x0

    invoke-virtual {p0, v6, v7}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->setResult(ILandroid/content/Intent;)V

    .line 639
    .end local v0    # "calendar":Ljava/util/Calendar;
    .end local v4    # "mCLItemArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;>;"
    .end local v5    # "result":I
    :cond_2
    return-void
.end method

.method public getDataRefresh(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "cityDataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;>;"
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 383
    const-string v1, ""

    const-string v2, "CL DRf Start"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 385
    if-nez p1, :cond_1

    .line 433
    :cond_0
    :goto_0
    return-void

    .line 389
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    invoke-virtual {v1, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->setRefreshCanceled(Z)V

    .line 391
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/http/AdvancedHttpClient;->isNetWorkConnected(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 392
    const v1, 0x7f0d0020

    invoke-static {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->toast(Landroid/content/Context;I)V

    goto :goto_0

    .line 396
    :cond_2
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCurrentCityData:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;

    if-eqz v1, :cond_5

    .line 397
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCtx:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getLocationCheck(Landroid/content/Context;)I

    move-result v0

    .line 398
    .local v0, "locationSetting":I
    const/16 v1, 0xbc0

    if-ne v0, v1, :cond_3

    .line 399
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->getPreferenceGPSOnlyDoNotShow(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_3

    sget-boolean v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isVerizon:Z

    if-eq v1, v4, :cond_3

    .line 402
    new-instance v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$4;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$4;-><init>(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;Ljava/util/ArrayList;)V

    invoke-static {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->showGPSonlyDialog(Landroid/app/Activity;Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/LocationGetter;)Landroid/app/Dialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mPopupDialog:Landroid/app/Dialog;

    goto :goto_0

    .line 415
    :cond_3
    const/16 v1, 0xbbb

    if-ne v0, v1, :cond_4

    sget-boolean v1, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->isVerizon:Z

    if-ne v1, v4, :cond_4

    .line 417
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->showGPSoffDialog(Landroid/app/Activity;)Landroid/app/Dialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mPopupDialog:Landroid/app/Dialog;

    goto :goto_0

    .line 420
    :cond_4
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCtx:Landroid/content/Context;

    invoke-static {v1, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->isLocationAvailable(Landroid/content/Context;Z)Z

    move-result v1

    if-nez v1, :cond_5

    .line 421
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/common/Util;->showGPSoffDialog(Landroid/app/Activity;)Landroid/app/Dialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mPopupDialog:Landroid/app/Dialog;

    goto :goto_0

    .line 427
    .end local v0    # "locationSetting":I
    :cond_5
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 428
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->startUpdateAnimation()V

    .line 429
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->showRefreshDialog()V

    .line 430
    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mRefreshManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;

    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mUiHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v1, v4, v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/appservice/RefreshManager;->doManualRefresh(ILandroid/os/Handler;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public isActivityVisible()Z
    .locals 1

    .prologue
    .line 579
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mIsVisible:Z

    return v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 5
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const v4, 0x7f030028

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 833
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 835
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->setupActionbar()V

    .line 836
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v3, :cond_1

    .line 838
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->toastHint:Landroid/widget/Toast;

    if-eqz v0, :cond_1

    .line 839
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->toastHint:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 840
    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->toastHint:Landroid/widget/Toast;

    .line 844
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mOrderAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;

    invoke-virtual {v0, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;->setViewResourceId(I)V

    .line 845
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mListView:Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mOrderAdapter:Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$OrderItemAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;->setOrderItemAdapter(Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrderAdapter;)V

    .line 858
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mHasCurrentCity:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCheckCurrentCitySetting:I

    if-ne v0, v3, :cond_2

    .line 859
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mListView:Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCurrentCityLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;->removeHeaderView(Landroid/view/View;)Z

    .line 860
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCtx:Landroid/content/Context;

    invoke-static {v0, v4, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCurrentCityLayout:Landroid/view/View;

    .line 861
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCurrentCityLayout:Landroid/view/View;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCurrentCityData:Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;

    invoke-direct {p0, v2, v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->setMyCityData(Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList$ViewHolder;Landroid/view/View;Lcom/sec/android/widgetapp/ap/hero/accuweather/model/MyCityData;)V

    .line 862
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mListView:Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;

    iget-object v1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCurrentCityLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;->addHeaderView(Landroid/view/View;)V

    .line 866
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->setDragGrabHandlePadding()V

    .line 869
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCurrentCityLayout:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 870
    iget-object v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCurrentCityLayout:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->changeListViewParamsBeforDrag(Landroid/view/View;)V

    .line 874
    :cond_3
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->changeUpdateLayoutHeight(I)V

    .line 876
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v2, 0x400

    const/4 v1, 0x0

    .line 126
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 128
    iput-object p0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCtx:Landroid/content/Context;

    .line 129
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setFlags(II)V

    .line 131
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutDirection(I)V

    .line 132
    const v0, 0x7f03002f

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->setContentView(I)V

    .line 134
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->setupActionbar()V

    .line 136
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->setContext(Landroid/content/Context;)V

    .line 138
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->setActivityVisibleState(Z)V

    .line 140
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->initViews()V

    .line 141
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->setupViews()V

    .line 143
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->setIsTModel()V

    .line 145
    invoke-direct {p0, v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->setupListView(Z)V

    .line 147
    invoke-direct {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->setEventListener()V

    .line 149
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->changeUpdateLayoutHeight(I)V

    .line 150
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menus"    # Landroid/view/Menu;

    .prologue
    .line 673
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 724
    const/4 v0, 0x0

    return v0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 764
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 766
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->setActivityVisibleState(Z)V

    .line 767
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->cleanResource()V

    .line 768
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 903
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 904
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mIsPressedHomeKey:Z

    .line 907
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v0, 0x1

    .line 728
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 729
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 740
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 731
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->doneRun()V

    goto :goto_0

    .line 734
    :sswitch_1
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->finish()V

    goto :goto_0

    .line 737
    :sswitch_2
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->finish()V

    goto :goto_0

    .line 729
    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_2
        0x7f08013b -> :sswitch_1
        0x7f08013c -> :sswitch_0
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 663
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 665
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 666
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->doneRun()V

    .line 668
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 744
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 746
    invoke-static {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/slog/SLog;->setContext(Landroid/content/Context;)V

    .line 748
    sget-boolean v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/WeatherClockApplication;->HARDKEY_USE:Z

    if-eqz v0, :cond_0

    .line 749
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->invalidateOptionsMenu()V

    .line 752
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->setActivityVisibleState(Z)V

    .line 761
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 1

    .prologue
    .line 895
    invoke-super {p0}, Landroid/app/Activity;->onUserLeaveHint()V

    .line 896
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mIsPressedHomeKey:Z

    if-eqz v0, :cond_0

    .line 897
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->doneRun()V

    .line 898
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mIsPressedHomeKey:Z

    .line 900
    :cond_0
    return-void
.end method

.method public refreshListView()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 538
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->isActivityVisible()Z

    move-result v3

    if-nez v3, :cond_0

    .line 561
    :goto_0
    return-void

    .line 542
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->doneRun()V

    .line 546
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mListView:Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;->getFirstVisiblePosition()I

    move-result v1

    .line 547
    .local v1, "selectedItem":I
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mListView:Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;

    invoke-virtual {v3, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 548
    .local v2, "v":Landroid/view/View;
    if-nez v2, :cond_2

    .line 550
    .local v0, "offsetY":I
    :goto_1
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCurrentCityLayout:Landroid/view/View;

    if-eqz v3, :cond_1

    .line 551
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mListView:Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCurrentCityLayout:Landroid/view/View;

    invoke-virtual {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;->addHeaderView(Landroid/view/View;)V

    .line 552
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mListView:Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;

    iget-object v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCurrentCityLayout:Landroid/view/View;

    invoke-virtual {v3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;->removeHeaderView(Landroid/view/View;)Z

    .line 555
    :cond_1
    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->setupListView(Z)V

    .line 558
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mListView:Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;

    invoke-virtual {v3, v1, v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;->setSelectionFromTop(II)V

    .line 559
    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mListView:Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/widget/DeviceDefaultChangeOrder;->invalidate()V

    goto :goto_0

    .line 548
    .end local v0    # "offsetY":I
    :cond_2
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v0

    goto :goto_1
.end method

.method public setActivityVisibleState(Z)V
    .locals 0
    .param p1, "onoff"    # Z

    .prologue
    .line 575
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mIsVisible:Z

    .line 576
    return-void
.end method

.method setViewUri(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 5
    .param p1, "Ctx"    # Landroid/content/Context;
    .param p2, "postion"    # Ljava/lang/String;
    .param p3, "location"    # Ljava/lang/String;

    .prologue
    .line 368
    new-instance v0, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuweatherUiUtil;

    invoke-direct {v0}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuweatherUiUtil;-><init>()V

    .line 369
    .local v0, "mUiUtil":Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuweatherUiUtil;
    const/4 v1, 0x0

    .line 371
    .local v1, "uri":Landroid/net/Uri;
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    if-nez v2, :cond_0

    .line 372
    new-instance v2, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    iget-object v3, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->mCtx:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    .line 375
    :cond_0
    const-string v2, "DETAIL_HOME"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 376
    iget-object v2, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->urlManager:Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuweatherUiUtil;->getLanguageString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/widgetapp/ap/hero/accuweather/menu/MenuChangeOrdersList;->tempScale:I

    invoke-virtual {v2, v3, p3, v4}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->makeQuickViewUri(Ljava/lang/String;Ljava/lang/String;I)Landroid/net/Uri;

    move-result-object v1

    .line 377
    invoke-static {v1}, Lcom/sec/android/widgetapp/ap/hero/accuweather/provider/accuweather/AccuWeatherUrlManager;->appendPartnerCodeAt(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    .line 379
    :cond_1
    return-object v1
.end method
